//
// Created by caduffm on 3/28/22.
//

#ifndef ATLAS_TMCMCTESTS_H
#define ATLAS_TMCMCTESTS_H

#include "coretools/Types/TStringHash.h"
#include "stattools/DAG/TDAGBuilder.h"
#include "stattools/ParametersObservations/TObservation.h"
#include "stattools/ParametersObservations/TParameter.h"
#include "stattools/commonTestCases/TBernoulliTest.h"
#include "stattools/commonTestCases/TBetaTest.h"
#include "stattools/commonTestCases/TBinomialTest.h"
#include "stattools/commonTestCases/TCategoricalTest.h"
#include "stattools/commonTestCases/TDirichletTest.h"
#include "stattools/commonTestCases/TExponentialTest.h"
#include "stattools/commonTestCases/TGammaTest.h"
#include "stattools/commonTestCases/THMMBoolGeneratingTest.h"
#include "stattools/commonTestCases/THMMBoolTest.h"
#include "stattools/commonTestCases/THMMCategoricalTest.h"
#include "stattools/commonTestCases/THMMCombinedScaledLaddersTest.h"
#include "stattools/commonTestCases/THMMLadderTest.h"
#include "stattools/commonTestCases/TLinearModelTest.h"
#include "stattools/commonTestCases/TLinkFunctionsTest.h"
#include "stattools/commonTestCases/TMultivariateNormalTest.h"
#include "stattools/commonTestCases/TNegativeBinomialTest.h"
#include "stattools/commonTestCases/TNormalTest.h"
#include "stattools/commonTestCases/TPoissonTest.h"
#include "stattools/commonTestCases/TTwoMultivariateNormalMixedModelsTest.h"
#include "stattools/commonTestCases/TTwoNormalMixedModelsTest.h"

namespace stattools {

template<typename Type, size_t NumDim, typename TypeBoxAbove>
void fillValuesIntoObservation(const std::string &Filename, TObservation<Type, NumDim, TypeBoxAbove> &Observation) {
	coretools::TInputFile simulatedFile(Filename + "_observations_simulated.txt", coretools::FileType::Header);

	// prepare
	auto dimensions = Observation.dimensions();
	std::array<size_t, NumDim - 1> allButFirstDimension;
	for (size_t d = 0; d < NumDim - 1; d++) { allButFirstDimension[d] = dimensions[d + 1]; }
	Observation.storage().prepareFillData(dimensions[0], allButFirstDimension);

	// read file and fill observation
	size_t c = 0;
	for (; !simulatedFile.empty(); simulatedFile.popFront()) {
		Observation.storage().emplace_back(simulatedFile.get<Type>(1));
		++c;
	}
	if (c != coretools::containerProduct(dimensions)) {
		DEVERROR("Number of lines in file ", simulatedFile.name(), " (", c,
		         ") does not match expected size of observation (", coretools::containerProduct(dimensions), ")!");
	}

	Observation.storage().finalizeFillData();
}

class TTest_Normal {
private:
	TNormalTest<> _test;
	TNormalTest<>::SpecObs _observation;

public:
	TTest_Normal(const std::string &Filename, bool Simulate);

	static std::string name();
	void fillIndistinguishableParameters(std::map<std::string, std::vector<std::vector<std::string>>> &){};
	void fillSwitchableParameters(std::vector<std::string> &, std::map<std::string, size_t> &){};
};

class TTest_Exponential {
private:
	TExponentialTest _test;
	TExponentialTest::SpecObs _observation;

public:
	TTest_Exponential(const std::string &Filename, bool Simulate);

	static std::string name();
	void fillIndistinguishableParameters(std::map<std::string, std::vector<std::vector<std::string>>> &){};
	void fillSwitchableParameters(std::vector<std::string> &, std::map<std::string, size_t> &){};
};

class TTest_Poisson {
private:
	TPoissonTest _test;
	TPoissonTest::SpecObs _observation;

public:
	TTest_Poisson(const std::string &Filename, bool Simulate);

	static std::string name();
	void fillIndistinguishableParameters(std::map<std::string, std::vector<std::vector<std::string>>> &){};
	void fillSwitchableParameters(std::vector<std::string> &, std::map<std::string, size_t> &){};
};

class TTest_Gamma {
private:
	TGammaTest _test;
	TGammaTest::SpecObs _observation;

public:
	TTest_Gamma(const std::string &Filename, bool Simulate);

	static std::string name();
	void fillIndistinguishableParameters(std::map<std::string, std::vector<std::vector<std::string>>> &){};
	void fillSwitchableParameters(std::vector<std::string> &, std::map<std::string, size_t> &){};
};

class TTest_Beta {
private:
	using TypeAlpha                     = coretools::StrictlyPositive;
	using TypeBeta                      = coretools::StrictlyPositive;
	constexpr static size_t NumDimAlpha = 1;
	constexpr static size_t NumDimBeta  = 1;
	using BoxOnAlpha                    = prior::TExponentialFixed<TParameterBase, TypeAlpha, NumDimAlpha>;
	using BoxOnBeta                     = prior::TExponentialFixed<TParameterBase, TypeBeta, NumDimBeta>;
	using Beta                          = TBetaTest<TypeAlpha, TypeBeta, BoxOnAlpha, BoxOnBeta>;
	BoxOnAlpha _boxOnAlpha;
	BoxOnAlpha _boxOnBeta;

	Beta _test;
	Beta::SpecObs _observation;

public:
	TTest_Beta(const std::string &Filename, bool Simulate);

	static std::string name();
	void fillIndistinguishableParameters(std::map<std::string, std::vector<std::vector<std::string>>> &){};
	void fillSwitchableParameters(std::vector<std::string> &, std::map<std::string, size_t> &){};
};

class TTest_Beta_UpdateLog {
private:
	using TypeLogAlpha = coretools::Unbounded;
	using TypeLogBeta  = coretools::Unbounded;
	using TypeAlpha    = coretools::StrictlyPositive;
	using TypeBeta     = coretools::StrictlyPositive;

	using LinkAlpha = TLinkFunctionsTest<TypeAlpha, TypeLogAlpha, det::TExp>;
	using LinkBeta  = TLinkFunctionsTest<TypeBeta, TypeLogBeta, det::TExp>;
	using Beta      = TBetaTest<TypeAlpha, TypeBeta, LinkAlpha::BoxOnBelow, LinkBeta::BoxOnBelow>;

	LinkAlpha _linkAlpha;
	LinkBeta _linkBeta;

	Beta _test;
	Beta::SpecObs _observation;

public:
	TTest_Beta_UpdateLog(const std::string &Filename, bool Simulate);

	static std::string name();
	void fillIndistinguishableParameters(std::map<std::string, std::vector<std::vector<std::string>>> &){};
	void fillSwitchableParameters(std::vector<std::string> &, std::map<std::string, size_t> &){};
};

class TTest_SymmetricBeta {
private:
	using TypeAlpha                     = coretools::StrictlyPositive;
	constexpr static size_t NumDimAlpha = 1;
	using BoxOnAlpha                    = prior::TExponentialFixed<TParameterBase, TypeAlpha, NumDimAlpha>;
	using Beta                          = TSymmetricBetaTest<TypeAlpha, BoxOnAlpha>;
	BoxOnAlpha _boxOnAlpha;

	Beta _test;
	Beta::SpecObs _observation;

public:
	TTest_SymmetricBeta(const std::string &Filename, bool Simulate);

	static std::string name();
	void fillIndistinguishableParameters(std::map<std::string, std::vector<std::vector<std::string>>> &){};
	void fillSwitchableParameters(std::vector<std::string> &, std::map<std::string, size_t> &){};
};

class TTest_SymmetricBeta_UpdateLog {
private:
	using TypeLogAlpha = coretools::Unbounded;
	using TypeAlpha    = coretools::StrictlyPositive;
	using Link         = TLinkFunctionsTest<TypeAlpha, TypeLogAlpha, det::TExp>;
	using Beta         = TSymmetricBetaTest<TypeAlpha, Link::BoxOnBelow>;
	Link _link;

	Beta _test;
	Beta::SpecObs _observation;

public:
	TTest_SymmetricBeta_UpdateLog(const std::string &Filename, bool Simulate);

	static std::string name();
	void fillIndistinguishableParameters(std::map<std::string, std::vector<std::vector<std::string>>> &){};
	void fillSwitchableParameters(std::vector<std::string> &, std::map<std::string, size_t> &){};
};

class TTest_Binomial {
private:
	TBinomialTest _test;
	TBinomialTest::SpecObs _observation;

public:
	TTest_Binomial(const std::string &Filename, bool Simulate);

	static std::string name();
	void fillIndistinguishableParameters(std::map<std::string, std::vector<std::vector<std::string>>> &){};
	void fillSwitchableParameters(std::vector<std::string> &, std::map<std::string, size_t> &){};
};

class TTest_NegativeBinomial {
private:
	TNegativeBinomialTest _test;
	TNegativeBinomialTest::SpecObs _observation;

public:
	TTest_NegativeBinomial(const std::string &Filename, bool Simulate);

	static std::string name();
	void fillIndistinguishableParameters(std::map<std::string, std::vector<std::vector<std::string>>> &){};
	void fillSwitchableParameters(std::vector<std::string> &, std::map<std::string, size_t> &){};
};

class TTest_Bernoulli {
private:
	using TypeBern = TBernoulliTest<true, 1>;
	TypeBern _test;
	TypeBern::SpecZ _observation;

public:
	TTest_Bernoulli(const std::string &Filename, bool Simulate);

	static std::string name();
	void fillIndistinguishableParameters(std::map<std::string, std::vector<std::vector<std::string>>> &){};
	void fillSwitchableParameters(std::vector<std::string> &, std::map<std::string, size_t> &){};
};

class TTest_GibbsBetaBernoulli {
private:
	TGibbsBetaBernoulliTest _test;
	TGibbsBetaBernoulliTest::SpecObs _observation;

public:
	TTest_GibbsBetaBernoulli(const std::string &Filename, bool Simulate);

	static std::string name();
	void fillIndistinguishableParameters(std::map<std::string, std::vector<std::vector<std::string>>> &){};
	void fillSwitchableParameters(std::vector<std::string> &, std::map<std::string, size_t> &){};
};

class TTest_Categorical {
private:
	TCategoricalTest _test;
	TCategoricalTest::SpecObs _observation;

public:
	TTest_Categorical(const std::string &Filename, bool Simulate);

	static std::string name();
	void fillIndistinguishableParameters(std::map<std::string, std::vector<std::vector<std::string>>> &){};
	void fillSwitchableParameters(std::vector<std::string> &, std::map<std::string, size_t> &){};
};

class TTest_DirichletVar {
private:
	TDirichletTest _test;
	TDirichletTest::SpecObs _observation;

public:
	TTest_DirichletVar(const std::string &Filename, bool Simulate);

	static std::string name();
	void fillIndistinguishableParameters(std::map<std::string, std::vector<std::vector<std::string>>> &){};
	void fillSwitchableParameters(std::vector<std::string> &, std::map<std::string, size_t> &){};
};

class TTest_DirichletMeanVar {
private:
	TDirichletMeanVarTest _test;
	TDirichletMeanVarTest::SpecObs _observation;

public:
	TTest_DirichletMeanVar(const std::string &Filename, bool Simulate);

	static std::string name();
	void fillIndistinguishableParameters(std::map<std::string, std::vector<std::vector<std::string>>> &){};
	void fillSwitchableParameters(std::vector<std::string> &, std::map<std::string, size_t> &){};
};

class TTest_HMMBool {
private:
	using TypeHMM = THMMBoolTest<true>;
	genometools::TDistancesBinned<uint8_t> _distances;
	TypeHMM _test;
	TypeHMM::SpecZ _observation;

public:
	TTest_HMMBool(const std::string &Filename, bool Simulate);

	static std::string name();
	void fillIndistinguishableParameters(std::map<std::string, std::vector<std::vector<std::string>>> &){};
	void fillSwitchableParameters(std::vector<std::string> &, std::map<std::string, size_t> &){};
};

class TTest_HMMBoolGeneratingMatrix {
private:
	genometools::TDistancesBinned<uint8_t> _distances;
	THMMBoolGeneratingTest _test;
	THMMBoolGeneratingTest::SpecObs _observation;

public:
	TTest_HMMBoolGeneratingMatrix(const std::string &Filename, bool Simulate);

	static std::string name();
	void fillIndistinguishableParameters(std::map<std::string, std::vector<std::vector<std::string>>> &){};
	void fillSwitchableParameters(std::vector<std::string> &, std::map<std::string, size_t> &){};
};

class TTest_HMMCategorical {
private:
	size_t _D = 5;
	genometools::TDistancesBinned<uint8_t> _distances;
	THMMCategoricalTest _test;
	THMMCategoricalTest::SpecObs _observation;

public:
	TTest_HMMCategorical(const std::string &Filename, bool Simulate);

	static std::string name();
	void fillIndistinguishableParameters(std::map<std::string, std::vector<std::vector<std::string>>> &){};
	void fillSwitchableParameters(std::vector<std::string> &, std::map<std::string, size_t> &){};
};

class TTest_HMMLadder {
private:
	size_t _K = 5;
	genometools::TDistancesBinned<uint8_t> _distances;
	THMMLadderTest _test;
	THMMLadderTest::SpecObs _observation;

public:
	TTest_HMMLadder(const std::string &Filename, bool Simulate);

	static std::string name();
	void fillIndistinguishableParameters(std::map<std::string, std::vector<std::vector<std::string>>> &){};
	void fillSwitchableParameters(std::vector<std::string> &, std::map<std::string, size_t> &){};
};

class TTest_HMMScaledLadderCombined {
private:
	const std::vector<size_t> _numStatesPerChain = {3, 5};
	constexpr static size_t _numStates           = 15;
	using Type                                   = coretools::UnsignedInt8WithMax<0>;

	using TypeHMM = THMMCombinedScaledLaddersTest<true, Type, TTransitionMatrixScaledLadder>;
	genometools::TDistancesBinned<uint8_t> _distances;
	TypeHMM _test;
	TypeHMM::SpecObs _observation;

public:
	TTest_HMMScaledLadderCombined(const std::string &Filename, bool Simulate);

	static std::string name();
	void fillIndistinguishableParameters(std::map<std::string, std::vector<std::vector<std::string>>> &){};
	void fillSwitchableParameters(std::vector<std::string> &, std::map<std::string, size_t> &){};
};

class TTest_MultivariateNormal {
private:
	TMultivariateNormalTest _test;
	TMultivariateNormalTest::SpecObs _observation;

public:
	TTest_MultivariateNormal(const std::string &Filename, bool Simulate);

	static std::string name();
	void fillIndistinguishableParameters(
	    std::map<std::string, std::vector<std::vector<std::string>>> &OperatorToJointParametersMap);
	void fillSwitchableParameters(std::vector<std::string> &, std::map<std::string, size_t> &){};
};

/*
class TTest_NormalMixedModel {
private:
    constexpr static size_t _K = 3;

    using Type              = coretools::Unbounded;
    using SpecZ             = ParamSpec<coretools::UnsignedInt8WithMax<0>, coretools::toHash("z"), 1>;
    using SpecMus           = ParamSpec<coretools::Unbounded, coretools::toHash("mus"), 1>;
    using SpecVars          = ParamSpec<coretools::StrictlyPositive, coretools::toHash("vars"), 1>;
    using SpecPis           = ParamSpec<coretools::ZeroOpenOneClosed, coretools::toHash("pis"), 1>;
    using TypeK             = coretools::UnsignedInt8;
    using BoxTypeMixedModel = prior::TNormalMixedModelInferred<TObservationBase, Type, 1, SpecZ, SpecMus, SpecVars, _K>;
    using BoxTypeZ          = prior::TCategoricalInferred<TParameterBase, SpecZ::value_type, SpecZ::numDim, SpecPis>;

    TParameter<SpecMus, BoxTypeMixedModel> _mean_0;
    TParameter<SpecMus, BoxTypeMixedModel> _mean_1;
    TParameter<SpecMus, BoxTypeMixedModel> _mean_2;
    TParameter<SpecVars, BoxTypeMixedModel> _var_0;
    TParameter<SpecVars, BoxTypeMixedModel> _var_1;
    TParameter<SpecVars, BoxTypeMixedModel> _var_2;
    TParameter<SpecPis, BoxTypeZ> _pi;
    TParameter<SpecZ, BoxTypeMixedModel> _z;
    TObservation<Type, 1> _observation;

public:
    TTest_NormalMixedModel(const std::string &Filename, bool Simulate);

    static std::string name();
    void fillIndistinguishableParameters(std::map<std::string, std::vector<std::vector<std::string>>> &){};
    void fillSwitchableParameters(std::vector<std::string> &ParamNamesMixedModels,
                                  std::map<std::string, size_t> &ParamNamesAndNumStatesForSCTest);
};
*/

/*
class TTest_NormalMixedModel_HMM {
private:
    constexpr static size_t _K = 3;

    using Type              = coretools::Unbounded;
    using SpecZ             = ParamSpec<coretools::UnsignedInt8WithMax<0>, coretools::toHash("z"), 1>;
    using SpecMus           = ParamSpec<coretools::Unbounded, coretools::toHash("mus"), 1>;
    using SpecVars          = ParamSpec<coretools::StrictlyPositive, coretools::toHash("vars"), 1>;
    using SpecPi            = ParamSpec<coretools::ZeroOneOpen, coretools::toHash("pi"), 1>;
    using SpecGamma         = ParamSpec<coretools::Positive, coretools::toHash("gamma"), 1>;
    using SpecRho           = ParamSpec<coretools::ZeroOpenOneClosed, coretools::toHash("rho"), 1>;
    using TypeK             = coretools::UnsignedInt8;
    using BoxTypeMixedModel = prior::TNormalMixedModelInferred<TObservationBase, Type, 1, SpecZ, SpecMus, SpecVars, _K>;
    using BoxTypeZ = prior::THMMCategoricalInferred<TParameterBase, SpecZ::value_type, 1, SpecPi, SpecGamma, SpecRho>;

    TParameter<SpecMus, BoxTypeMixedModel> _mean_0;
    TParameter<SpecMus, BoxTypeMixedModel> _mean_1;
    TParameter<SpecMus, BoxTypeMixedModel> _mean_2;
    TParameter<SpecVars, BoxTypeMixedModel> _var_0;
    TParameter<SpecVars, BoxTypeMixedModel> _var_1;
    TParameter<SpecVars, BoxTypeMixedModel> _var_2;
    TParameter<SpecPi, BoxTypeZ> _pi;
    TParameter<SpecGamma, BoxTypeZ> _gamma;
    TParameter<SpecRho, BoxTypeZ> _rhos;
    genometools::TDistancesBinned<uint8_t> _distances;
    TParameter<SpecZ, BoxTypeMixedModel> _z;
    TObservation<Type, 1> _observation;

public:
    TTest_NormalMixedModel_HMM(const std::string &Filename, bool Simulate);

    static std::string name();
    void fillIndistinguishableParameters(std::map<std::string, std::vector<std::vector<std::string>>> &){};
    void fillSwitchableParameters(std::vector<std::string> &ParamNamesMixedModels,
                                  std::map<std::string, size_t> &ParamNamesAndNumStatesForSCTest);
};
*/

class TTest_NormalMixedModel_Stretch {
private:
	using TypeBern = TBernoulliTest<false, 1>;
	using TypeMM   = TTwoNormalMixedModelsTest<TypeBern::BoxOnZ, true, 1>;

	TypeBern _bernoulli;
	TypeMM _test;
	TypeMM::SpecObs _observation;

public:
	TTest_NormalMixedModel_Stretch(const std::string &Filename, bool Simulate);

	static std::string name();
	void fillIndistinguishableParameters(std::map<std::string, std::vector<std::vector<std::string>>> &){};
	void fillSwitchableParameters(std::vector<std::string> &ParamNamesMixedModels,
	                              std::map<std::string, size_t> &ParamNamesAndNumStatesForSCTest);
};

class TTest_NormalMixedModel_Stretch_HMM {
private:
	using TypeHMM = THMMBoolTest<false>;
	using TypeMM  = TTwoNormalMixedModelsTest<TypeHMM::BoxOnZ, true, 1>;

	genometools::TDistancesBinned<uint8_t> _distances;
	TypeHMM _hmm;
	TypeMM _test;
	TypeMM::SpecObs _observation;

public:
	TTest_NormalMixedModel_Stretch_HMM(const std::string &Filename, bool Simulate);

	static std::string name();
	void fillIndistinguishableParameters(std::map<std::string, std::vector<std::vector<std::string>>> &){};
	void fillSwitchableParameters(std::vector<std::string> &ParamNamesMixedModels,
	                              std::map<std::string, size_t> &ParamNamesAndNumStatesForSCTest);
};

/*
class TTest_MultivariateNormalMixedModel {
private:
    constexpr static size_t _K = 3;

    using Type              = coretools::Unbounded;
    using SpecZ             = ParamSpec<coretools::UnsignedInt8WithMax<0>, coretools::toHash("z"), 1>;
    using SpecMus           = ParamSpec<coretools::Unbounded, coretools::toHash("mu"), 1>;
    using SpecM             = ParamSpec<coretools::StrictlyPositive, coretools::toHash("m"), 1>;
    using SpecMrr           = ParamSpec<coretools::StrictlyPositive, coretools::toHash("mrr"), 1>;
    using SpecMrs           = ParamSpec<coretools::Unbounded, coretools::toHash("mrs"), 1>;
    using SpecPis           = ParamSpec<coretools::ZeroOpenOneClosed, coretools::toHash("pis"), 1>;
    using TypeK             = coretools::UnsignedInt8;
    using BoxTypeMixedModel = prior::TMultivariateNormalMixedModelInferred<TObservationBase, Type, 2, SpecZ, SpecMus,
                                                                           SpecM, SpecMrr, SpecMrs, _K>;
    using BoxTypeZ          = prior::TCategoricalInferred<TParameterBase, SpecZ::value_type, SpecZ::numDim, SpecPis>;

    TParameter<SpecMus, BoxTypeMixedModel> _mu_0;
    TParameter<SpecMus, BoxTypeMixedModel> _mu_1;
    TParameter<SpecMus, BoxTypeMixedModel> _mu_2;
    TParameter<SpecM, BoxTypeMixedModel> _m_0;
    TParameter<SpecM, BoxTypeMixedModel> _m_1;
    TParameter<SpecM, BoxTypeMixedModel> _m_2;
    TParameter<SpecMrr, BoxTypeMixedModel> _Mrr_0;
    TParameter<SpecMrr, BoxTypeMixedModel> _Mrr_1;
    TParameter<SpecMrr, BoxTypeMixedModel> _Mrr_2;
    TParameter<SpecMrs, BoxTypeMixedModel> _Mrs_0;
    TParameter<SpecMrs, BoxTypeMixedModel> _Mrs_1;
    TParameter<SpecMrs, BoxTypeMixedModel> _Mrs_2;
    TParameter<SpecPis, BoxTypeZ> _pi;
    TParameter<SpecZ, BoxTypeMixedModel> _z;
    TObservation<Type, 2> _observation;

public:
    TTest_MultivariateNormalMixedModel(const std::string &Filename, bool Simulate);

    static std::string name();
    void fillIndistinguishableParameters(
        std::map<std::string, std::vector<std::vector<std::string>>> &OperatorToJointParametersMap);
    void fillSwitchableParameters(std::vector<std::string> &ParamNamesMixedModels,
                                  std::map<std::string, size_t> &ParamNamesAndNumStatesForSCTest);
};
*/

class TTest_MultivariateNormalMixedModel_Stretch {
private:
	using TypeBern = TBernoulliTest<false, 1>;
	using TypeMM   = TTwoMulativariateNormalMixedModelsTest<TypeBern::BoxOnZ>;

	TypeBern _bernoulli;
	TypeMM _test;
	TypeMM::SpecObs _observation;

public:
	TTest_MultivariateNormalMixedModel_Stretch(const std::string &Filename, bool Simulate);

	static std::string name();
	void fillIndistinguishableParameters(
	    std::map<std::string, std::vector<std::vector<std::string>>> &OperatorToJointParametersMap);
	void fillSwitchableParameters(std::vector<std::string> &ParamNamesMixedModels,
	                              std::map<std::string, size_t> &ParamNamesAndNumStatesForSCTest);
};

class TTest_MultivariateNormalMixedModel_Stretch_HMM {
private:
	using TypeHMM = THMMBoolTest<false>;
	using TypeMM  = TTwoMulativariateNormalMixedModelsTest<TypeHMM::BoxOnZ>;

	genometools::TDistancesBinned<uint8_t> _distances;
	TypeHMM _hmm;
	TypeMM _test;
	TypeMM::SpecObs _observation;

public:
	TTest_MultivariateNormalMixedModel_Stretch_HMM(const std::string &Filename, bool Simulate);

	static std::string name();
	void fillIndistinguishableParameters(
	    std::map<std::string, std::vector<std::vector<std::string>>> &OperatorToJointParametersMap);
	void fillSwitchableParameters(std::vector<std::string> &ParamNamesMixedModels,
	                              std::map<std::string, size_t> &ParamNamesAndNumStatesForSCTest);
};

class TTest_LinearModel_Intercept {
private:
	using TypeBern        = TBernoulliTest<false, 2>;
	using TypeMM          = TTwoNormalMixedModelsTest<TypeBern::BoxOnZ, false, 2>;
	using TypeLinearModel = TLinearModelTest<true, true, TypeMM::BoxOnObs>;

	TypeBern _bernoulli;
	TypeMM _mixedModel;

	TypeLinearModel _test;
	TypeLinearModel::SpecObs _observation;

public:
	TTest_LinearModel_Intercept(const std::string &Filename, bool Simulate);

	static std::string name();
	void fillIndistinguishableParameters(std::map<std::string, std::vector<std::vector<std::string>>> &){};
	void fillSwitchableParameters(std::vector<std::string> &, std::map<std::string, size_t> &){};
};

} // namespace stattools
#endif // ATLAS_TMCMCTESTS_H
