//
// Created by Madleina Caduff on 01.05.20.
//

#ifndef MCMCFRAMEWORK_TRUNMCMC_H
#define MCMCFRAMEWORK_TRUNMCMC_H

#include <algorithm> // for max
#include <cstddef>   // for size_t
#include <cstdint>   // for uint8_t
#include <map>       // for map
#include <memory>    // for allocator, shared_ptr, make_shared
#include <stdexcept> // for runtime_error
#include <string>    // for string, operator+, char_traits
#include <vector>    // for vector, vector<>::value_type

#include "TKolmogorovSmirnovTest.h"                 // for TKolmogorovSmirnovTest
#include "TSandersCalibrationTest.h"                // for TSandersCalibrationTest
#include "coretools/Files/TInputFile.h"             // for TInputFile::read
#include "coretools/IntegrationTests/TTest.h"       // for TTest
#include "coretools/Main/TRandomGenerator.h"        // for TRandomGenerator
#include "coretools/algorithms.h"                   // for containerProduct
#include "genometools/GenomePositions/TDistances.h" // for TDistancesBinned::TDistancesBin...
#include "stattools/MCMC/TMCMC.h"

namespace stattools {

template<typename Test> class TMCMCTest : public coretools::TTest {
protected:
	// pointer to MCMC model
	std::unique_ptr<Test> _model;

	// parameters that need SC test
	std::map<std::string, size_t> _paramNamesAndNumStatesForSCTest;

	// vector with mixed-model parameters (than can possibly switch)
	std::vector<std::string> _paramNamesMixedModels;

	// tests
	TKolmogorovSmirnovTest KSTest;
	TSandersCalibrationTest SCTest;
	bool _runSCAfterEachRep;

	// files
	std::string _testingPrefix;
	std::string _filename;

	size_t _numReps;

	void _initializeRandomGenerator() {
		coretools::instances::logfile().listFlush("Initializing random generator ...");

		if (coretools::instances::parameters().exists("fixedSeed")) {
			coretools::instances::randomGenerator().setSeed(coretools::instances::parameters().get<long>("fixedSeed"),
			                                                true);
		} else if (coretools::instances::parameters().exists("addToSeed")) {
			coretools::instances::randomGenerator().setSeed(coretools::instances::parameters().get<long>("addToSeed"),
			                                                false);
		}
		coretools::instances::logfile().write(
		    " done with seed " + coretools::str::toString(coretools::instances::randomGenerator().getSeed()) + "!");
	};

	// prepare
	void _setUp(bool Simulate) {
		instances::dagBuilder().clear();

		// define parameters & observations
		_model = nullptr; // reset after simulation: make sure all settings from simulation do not apply for MCMC
		_model = std::make_unique<Test>(_filename, Simulate);
		_model->fillSwitchableParameters(_paramNamesMixedModels, _paramNamesAndNumStatesForSCTest);
	};

	void _prepare() {
		// define number of replicates and cutoff values for significance
		_numReps = coretools::instances::parameters().get("numReplicates", 2000);

		// define number of bins for Sander's Calibration
		size_t numBins = coretools::instances::parameters().get("numBins", _numReps / 100);
		if (numBins < 2) { numBins = 2; }
		SCTest.setNumBins(numBins);

		_initializeRandomGenerator();

		// run SC test after each replicate?
		if (coretools::instances::parameters().exists("runSCAfterEachRep")) { _runSCAfterEachRep = true; }

		double cutoffSignificance_KSTest = coretools::instances::parameters().get("cutoff_KS_test", 0.05);
		KSTest.setCutoffSignificance(cutoffSignificance_KSTest);
		double cutoffSignificance_SCTest = coretools::instances::parameters().get("cutoff_SC_test", 0.05);
		SCTest.setCutoffSignificance(cutoffSignificance_SCTest);
	};

	void _prepareMaps(const std::vector<TParameterBase *> &Parameters) {
		for (auto &param : Parameters) {
			if (param->isUpdated()) { // only add updated parameters to map
				std::vector<std::string> header;
				param->fillNames(header);
				for (auto &it : header) { _addToMap(it, _paramNeedsSCTest(param)); }
			}
		}
	};

	void _addToMap(const std::string &Name, bool SC) {
		if (SC) { // add to Sander-Calibration map
			SCTest.setNumStates(_paramNamesAndNumStatesForSCTest[(std::string)coretools::str::readBefore(Name, "_")]);
			SCTest.prepareMaps(Name, _numReps);
		} else { // add to Kolmogorov-Smirnov map
			KSTest.prepareMaps(Name, _numReps);
		}
	};

	bool _paramNeedsSCTest(const TParameterBase *parameter) {
		auto it = _paramNamesAndNumStatesForSCTest.find(parameter->name());
		if (it != _paramNamesAndNumStatesForSCTest.end()) {
			return true;
		} else {
			return false;
		}
	};

	// evaluate MCMC
	virtual void _runReplicate(size_t rep) {
		// simulate
		_setUp(true);
		_initializeRandomGenerator();
		TSimulator simulator;
		simulator.simulate();

		if (rep == 0) { _prepareMaps(instances::dagBuilder().getAllParameters()); }

		// run MCMC
		_setUp(false);
		_initializeRandomGenerator();
		TMCMC mcmc;
		mcmc.runMCMC(_filename);

		// evaluate result of single run
		_evaluateOneReplicate(rep);
	};

	void _openAndCheckFiles(coretools::TInputFile &simulatedFile, coretools::TInputFile &traceFile) {
		// open simulated file
		simulatedFile.open(_filename + "_simulated.txt", coretools::FileType::Header);

		// open trace file
		traceFile.open(_filename + "_trace.txt", coretools::FileType::Header);
	};

	void _evaluateOneReplicate(const size_t &Replicate) {
		// open simulated and trace file and check if they have same format
		coretools::TInputFile simulatedFile;
		coretools::TInputFile traceFile;
		_openAndCheckFiles(simulatedFile, traceFile);

		// read simulated values into vector
		std::vector<double> simulatedValues;
		for (; !simulatedFile.empty(); simulatedFile.popFront()) {
			simulatedValues.emplace_back(simulatedFile.get<double>(1));
		}

		// evaluate trace file
		_evaluateTraceFile(simulatedValues, traceFile, Replicate);

		// run SC test for checking convergence
		if (_runSCAfterEachRep &&
		    Replicate < _numReps - 1) { // don't run the very last one - this is still done above, together with KS
			SCTest.passSandersCalibrationTest(false, Replicate);
		}
	};

	void _evaluateTraceFile(std::vector<double> &SimulatedValues, coretools::TInputFile &TraceFile,
	                        const size_t &Replicate) {
		// assign column names: which columns should run a SC test and which should run a KS test?
		if (Replicate == 0) {
			KSTest.fillRelevantColumns(TraceFile.header());
			SCTest.fillRelevantColumns(TraceFile.header());

			// give KS-test information about parameters that can not be distinguished
			// e.g. m and Mrr in multivariate normal prior are estimated as a compound effect,
			// we only want to find out whether we estimate Mrr/m correctly
			std::map<std::string, std::vector<std::vector<std::string>>> operatorToJointParametersMap;
			_model->fillIndistinguishableParameters(operatorToJointParametersMap);
			KSTest.addMapOfIndistinguishableParameters(operatorToJointParametersMap);
		}

		// parse trace file for SC test
		if (SCTest.numParameters() > 0) {
			SCTest.evaluateTraceFile(SimulatedValues, TraceFile, Replicate);
			_accountForSwitchedModels(SimulatedValues, TraceFile.header());
		}
		// parse trace file again for KS test. Close and reopen to re-start at beginning of file (there is probably a
		// better way)
		std::string filename = TraceFile.name();
		TraceFile.close();

		coretools::TInputFile traceFile2;
		traceFile2.open(filename, coretools::FileType::Header);
		KSTest.evaluateTraceFile(SimulatedValues, traceFile2, Replicate);
	};

	void _accountForSwitchedModels(std::vector<double> &SimulatedValues, coretools::TConstView<std::string> Header) {
		if (SCTest.modelsSwitched()) {
			std::vector<size_t> newOrder = SCTest.getSwitchedModelOrder();
			KSTest.accountForModelSwitch(newOrder, Header, SimulatedValues, _paramNamesMixedModels);
		}
	};

	void _writeMapToFile(std::string_view Filename, const std::map<std::string, std::vector<double>> &Map) {
		coretools::TOutputFile file(Filename);

		// header
		std::vector<std::string> header;
		for (auto &it : Map) { header.push_back(it.first); }
		file.writeHeader(header);

		// write rest
		for (size_t rep = 0; rep < _numReps; rep++) {
			for (auto &it : Map) { file << it.second[rep]; }
			file.endln();
		}

		file.close();
	};

	void _writeSCAndPValsToFile() {
		// write observed SC values to file
		std::string filename                              = _filename + "_observedSandersAllReps.txt";
		std::map<std::string, std::vector<double>> SCVals = SCTest.getObservedSCsAllReps();
		_writeMapToFile(filename, SCVals);

		// write p-values to file
		filename                                         = _filename + "_pValsAllReps.txt";
		std::map<std::string, std::vector<double>> pVals = SCTest.getPValuesAllReps();
		_writeMapToFile(filename, pVals);
	};

public:
	TMCMCTest() {
		_name              = Test::name();
		_testingPrefix     = "stattools_test";
		_numReps           = 0;
		_runSCAfterEachRep = false;
		_filename          = _testingPrefix + "_" + _name;
	};

	bool run(coretools::TTaskList *) override {
		// read parameters, prepare maps etc.
		_prepare();

		bool writePosteriorFilesSCTest = false;
		if (coretools::instances::parameters().exists("writePosteriorProbsFile")) { writePosteriorFilesSCTest = true; }

		// run simulation-MCMC replicates!
		coretools::instances::logfile().startNumbering("Running " + coretools::str::toString(_numReps) +
		                                               " replicates:");
		for (size_t rep = 0; rep < _numReps; rep++) {
			coretools::instances::logfile().number("Replicate " + coretools::str::toString(rep + 1) + ":");
			coretools::instances::logfile().addIndent(2);
			_runReplicate(rep);
			coretools::instances::logfile().removeIndent(2);
		}
		coretools::instances::logfile().endNumbering();

		// run Kolmogorov-Smirnov test, evaluate all t-tests and run Sanders calibration test
		// return true if all passed
		bool KSTestPasses = KSTest.passKolmogorovSmirnovTest();
		bool SCTestPasses = SCTest.passSandersCalibrationTest(writePosteriorFilesSCTest, _numReps - 1);

		if (_runSCAfterEachRep) { _writeSCAndPValsToFile(); }

		return KSTestPasses && SCTestPasses;
	};
};

} // end namespace stattools

#endif // MCMCFRAMEWORK_TRUNMCMC_H
