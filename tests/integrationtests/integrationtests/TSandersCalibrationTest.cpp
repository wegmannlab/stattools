//
// Created by madleina on 05.03.21.
//

#include "TSandersCalibrationTest.h"

#include <algorithm> // for max, find, max_element
#include <assert.h>  // for assert
#include <limits>    // for numeric_limits
#include <stdexcept> // for runtime_error
#include <type_traits>

#include "TSandersCalibration.h"               // for TSandersCalibration
#include "coretools/Files/TInputFile.h"        // for TOutputFile::operator<<, TInputFile...
#include "coretools/Files/TOutputFile.h"       // for TOutputFile::operator<<, TInputFile...
#include "coretools/Main/TLog.h"               // for TLog

namespace coretools {
class TRandomGenerator;
} // namespace coretools

namespace stattools {

TSandersCalibrationTest::TSandersCalibrationTest() {
	_cutoffSignificance_SCTest = 0.;
	_modelsAreSwitched         = false;
	_numStates                 = 0;
	_numStatesInitialized      = false;
	_numBins                   = 0;
	_numReps                   = 0;
}

void TSandersCalibrationTest::setCutoffSignificance(double Cutoff) { _cutoffSignificance_SCTest = Cutoff; }

void TSandersCalibrationTest::setNumBins(uint16_t NumBins) { _numBins = NumBins; }

void TSandersCalibrationTest::setNumStates(const size_t &NumStates) {
	if (_numStatesInitialized && NumStates != _numStates) {
		DEVERROR("Tried to set another numStates (", NumStates, ") than previously initialized (", _numStates, "!");
	}
	_numStates = NumStates; // store number of states (Assume this is the same for all parameters that need SC test - if
	                        // not, make map!)
	_numStatesInitialized = true;
}

void TSandersCalibrationTest::prepareMaps(const std::string &Name, const size_t &NumReps) {
	_numReps = NumReps;

	_simulatedValues[Name].resize(_numReps, 0.);
	_posteriorProbabilities[Name].resize(_numReps, std::vector<double>(_numStates, 0.));

	_sc_afterEachRep[Name].resize(_numReps, 0.);
	_pValue_afterEachRep[Name].resize(_numReps, 0.);
}

size_t TSandersCalibrationTest::numParameters() const {
	assert(_posteriorProbabilities.size() == _simulatedValues.size());
	return _posteriorProbabilities.size();
}

bool TSandersCalibrationTest::modelsSwitched() const { return _modelsAreSwitched; }

uint16_t TSandersCalibrationTest::numBins() const { return _numBins; }

const std::vector<size_t> &TSandersCalibrationTest::getSwitchedModelOrder() const { return _switchedModels; }

const std::map<std::string, std::vector<double>> &TSandersCalibrationTest::getPValuesAllReps() const {
	return _pValue_afterEachRep;
}

const std::map<std::string, std::vector<double>> &TSandersCalibrationTest::getObservedSCsAllReps() const {
	return _sc_afterEachRep;
}

void TSandersCalibrationTest::fillRelevantColumns(coretools::TConstView<std::string> Header) {
	// fill vector that indicates which columns from a file are relevant for this test!
	_relevantCols.clear();
	for (size_t col = 0; col < Header.size(); col++) {
		auto it = _posteriorProbabilities.find(Header[col]);
		if (it != _posteriorProbabilities.end()) { // found
			_relevantCols.push_back(col);
		}
	}
}

void TSandersCalibrationTest::_calculateMeanPosteriorProbability_PerModel(
    const size_t &ModelIndex, const std::vector<double> &SimulatedValues,
    const std::vector<std::vector<double>> &PosteriorProbabilitiesPerModel, std::vector<double> &MeanPerModel) {
	MeanPerModel.clear();
	MeanPerModel.resize(_numStates, 0.);
	std::vector<size_t> counterPerModel(_numStates);

	// go over all relevant parameters
	for (auto &relevantCol : _relevantCols) {
		auto simulatedValue = static_cast<size_t>(SimulatedValues[relevantCol]);
		MeanPerModel[simulatedValue] += PosteriorProbabilitiesPerModel[relevantCol][ModelIndex];
		counterPerModel[simulatedValue]++;
	}

	// calculate mean of all posteriors per simulated value
	for (size_t value = 0; value < _numStates; value++) {
		if (counterPerModel[value] == 0) {
			MeanPerModel[value] = 0.;
		} else {
			MeanPerModel[value] /= counterPerModel[value];
		}
	}
}

void TSandersCalibrationTest::_assignSwitchedModels(
    const std::vector<double> &SimulatedValues,
    const std::vector<std::vector<double>> &PosteriorProbabilitiesPerModel) {
	// first clear (from previous run)
	_switchedModels.clear();

	// prepare storage to calculate mean of all posteriors per simulated value
	for (size_t model = 0; model < _numStates;
	     model++) { // go over all states (columns of PosteriorProbabilitiesPerModel)
		std::vector<double> meanPerModel;
		_calculateMeanPosteriorProbability_PerModel(model, SimulatedValues, PosteriorProbabilitiesPerModel,
		                                            meanPerModel);

		// keep the one with the highest index
		auto it      = std::max_element(meanPerModel.begin(), meanPerModel.end());
		size_t index = it - meanPerModel.begin();
		while (std::find(_switchedModels.begin(), _switchedModels.end(), index) !=
		       _switchedModels.end()) { // take next-best index in case the best index is already used
			*it   = std::numeric_limits<double>::lowest(); // set it to super small value
			it    = std::max_element(meanPerModel.begin(), meanPerModel.end());
			index = it - meanPerModel.begin();
		}
		// _switchedModels: maps new indices (= index in vector) to old indices (= value in vector)
		// e.g. _switchedModels = {2, 0, 1}
		// _switchedModels[0] = 2 means that model 0 corresponds to model 2 in simulations
		// _switchedModels[1] = 0 means that model 1 corresponds to model 0 in simulations
		_switchedModels.push_back(index);
	}

	// check if models are indeed switched -> set bool accordingly
	_modelsAreSwitched = false;
	for (size_t value = 0; value < _numStates; value++) {
		if (_switchedModels[value] != value) {
			_modelsAreSwitched = true;
			break;
		}
	}
}

void TSandersCalibrationTest::_parseTraceFile(coretools::TInputFile &TraceFile,
                                              std::vector<std::vector<double>> &SumOverIterationsPerParam) {
	// SumOverIterationsPerParam stores the sum of all iterations per parameter and model -> used to calculate posterior
	// probability per model afterwards
	SumOverIterationsPerParam.resize(TraceFile.numCols(), std::vector<double>(_numStates, 0.));

	// parse trace file
	for (; !TraceFile.empty(); TraceFile.popFront()) {
		// go over all relevant columns and add to sum
		for (auto &relevantCol : _relevantCols) {
			auto state = TraceFile.get<size_t>(relevantCol);
			SumOverIterationsPerParam[relevantCol][state]++;
		}
	}
}

void TSandersCalibrationTest::evaluateTraceFile(std::vector<double> &SimulatedValues, coretools::TInputFile &TraceFile,
                                                const size_t &Replicate) {
	// this function evaluates a trace file with respect to all Sanders calibration parameters
	// need to do this first to account for model switching!

	// parse trace file
	std::vector<std::vector<double>> sumOverIterationsPerParam;
	_parseTraceFile(TraceFile, sumOverIterationsPerParam);

	// assign values from trace file to parameter in map
	for (auto &relevantCol : _relevantCols) {
		// calculate posterior probabilities per model: divide each model-counter by the total number of counts (= line
		// number)
		for (auto &model : sumOverIterationsPerParam[relevantCol]) {
			model /= static_cast<double>(TraceFile.curLine() - 1); // -1 for header
		}

		// add to map
		std::string paramName = TraceFile.header()[relevantCol];
		auto it               = _posteriorProbabilities.find(paramName);
		if (it != _posteriorProbabilities.end()) { // found
			it->second[Replicate] = sumOverIterationsPerParam[relevantCol];
		} else {
			DEVERROR("Could not find parameter ", paramName, " in map _posteriorProbabilities!");
		}
	}

	// assign switched models: will need to modify simulated values accordingly
	_assignSwitchedModels(SimulatedValues, sumOverIterationsPerParam);

	// save simulated values (needed for test later). Account for model switch!
	_saveSimulatedValues(SimulatedValues, TraceFile.header(), Replicate);
}

void TSandersCalibrationTest::_reAssignSimulatedValues(std::vector<double> &SimulatedValues) {
	if (_modelsAreSwitched) {
		std::vector<double> simulatedValues_Old = SimulatedValues;
		for (auto &relevantCol : _relevantCols) {
			auto it = std::find(_switchedModels.begin(), _switchedModels.end(), simulatedValues_Old[relevantCol]);
			size_t newModel              = it - _switchedModels.begin(); // get index
			SimulatedValues[relevantCol] = newModel;
		}
	}
}

void TSandersCalibrationTest::_saveSimulatedValues(std::vector<double> &SimulatedValues,
                                                   coretools::TConstView<std::string> Header, const size_t &Replicate) {
	// first switch models
	_reAssignSimulatedValues(SimulatedValues);

	// now save
	for (auto &relevantCol : _relevantCols) {
		std::string paramName = Header[relevantCol];
		auto it               = _simulatedValues.find(paramName);
		if (it != _simulatedValues.end()) { // found
			it->second[Replicate] = SimulatedValues[relevantCol];
		}
	}
}

void TSandersCalibrationTest::_prepareVectorsForSandersTest(
    const size_t &Model, const std::vector<std::vector<double>> &PosteriorProbabilitiesAllModels,
    const std::vector<double> &SimulatedValuesAllModels, std::vector<double> &PosteriorProbabilityThisModel,
    std::vector<double> &SimulatedThisModel, const size_t &CurRep) {
	// for one specific model: convert to binary data (this model vs all other models)
	// get all posterior probabilities for this specific model into one vector -> PosteriorProbabilityThisModel
	// get all simulated values for this specific model into one vector (1 if simulated this model, else 0) ->
	// SimulatedThisModel
	PosteriorProbabilityThisModel.resize(CurRep, 0.);
	SimulatedThisModel.resize(CurRep, 0.);

	for (size_t rep = 0; rep < CurRep; rep++) {
		PosteriorProbabilityThisModel[rep] = PosteriorProbabilitiesAllModels[rep][Model];
		if (SimulatedValuesAllModels[rep] == Model) {
			SimulatedThisModel[rep] = 1;
		} else {
			SimulatedThisModel[rep] = 0;
		}
	}
}

void TSandersCalibrationTest::_addToPValMap(const std::string &Name, const size_t &Rep, double PValue) {
	auto paramPVal = _pValue_afterEachRep.find(Name);
	if (paramPVal == _pValue_afterEachRep.end()) { // not found
		DEVERROR("Could not find parameter ", Name, " in map _pValue_afterEachRep!");
	}
	paramPVal->second[Rep] = PValue;
}

void TSandersCalibrationTest::_addToSCMap(const std::string &Name, const size_t &Rep, double ObservedSC) {
	auto paramSC = _sc_afterEachRep.find(Name);
	if (paramSC == _sc_afterEachRep.end()) { // not found
		DEVERROR("Could not find parameter ", Name, " in map _sc_afterEachRep!");
	}
	paramSC->second[Rep] = ObservedSC;
}

bool TSandersCalibrationTest::passSandersCalibrationTest(bool WriteFile, const size_t &Rep) {
	if (Rep <= _numBins) { // not enough data points to calculate SC yet
		return true;
	}
	// go over all parameters in map
	size_t counter_paramFailed = 0;
	for (auto &param : _posteriorProbabilities) {
		// find that parameter in simulated map
		auto paramSimVal = _simulatedValues.find(param.first);
		if (paramSimVal == _simulatedValues.end()) { // not found
			DEVERROR("Could not find parameter ", param.first, " in map _simulatedValuesSCTest!");
		}

		// write posterior probabilities to file (for debugging?)
		if (WriteFile) { _writePosteriorProbsToFile(param, *paramSimVal); }

		for (size_t model = 0; model < _numStates; model++) {
			std::vector<double> posteriorProbabilityThisModel;
			std::vector<double> simulatedThisModel;
			_prepareVectorsForSandersTest(model, param.second, paramSimVal->second, posteriorProbabilityThisModel,
			                              simulatedThisModel, Rep);

			// run Sanders Calibration Test
			TSandersCalibration sandersCalibration;
			sandersCalibration.setNumBins(_numBins);
			double observedSC;
			double pValue = sandersCalibration.calculatePValueSandersCalibration(posteriorProbabilityThisModel,
			                                                                     simulatedThisModel, observedSC);

			// add to maps: just for 1-model -> TODO: maybe change later, if needed
			if (model == 1) {
				_addToPValMap(param.first, Rep, pValue);
				_addToSCMap(param.first, Rep, observedSC);
			}

			if (Rep == _numReps - 1) { // only print to logfile for very last replicate
				if (pValue <=
				    _cutoffSignificance_SCTest) { // reject null hypothesis that distribution is well calibrated
					coretools::instances::logfile().warning(
					    "Parameter " + param.first + " does not pass Sanders-Calibration-Test for model " +
					    coretools::str::toString(model) + ": rejected null hypothesis with p-value " +
					    coretools::str::toString(pValue) + ".");
					counter_paramFailed++;
				} else {
					coretools::instances::logfile().list(
					    "Parameter " + param.first + " successfully passed Sanders-Calibration-Test for model " +
					    coretools::str::toString(model) + ": did not reject null hypothesis with p-value " +
					    coretools::str::toString(pValue) + ".");
				}
			}
		}
	}

	// decide if overall test passes or fails
	if (counter_paramFailed > 0) {
		return false;
	} else {
		return true;
	}
}

void TSandersCalibrationTest::_writePosteriorProbsToFile(
    const std::pair<std::string, std::vector<std::vector<double>>> &PosteriorProbsParam,
    const std::pair<std::string, std::vector<double>> &SimulatedValues) const {
	coretools::TOutputFile file("posteriorProbabilitiesSander_" + PosteriorProbsParam.first + ".txt");
	// header
	std::vector<std::string> header = {"simulated"};
	for (size_t i = 0; i < _numStates; i++) { header.push_back("state_" + coretools::str::toString(i)); }
	file.writeHeader(header);

	assert(SimulatedValues.second.size() == PosteriorProbsParam.second.size());

	for (size_t i = 0; i < SimulatedValues.second.size(); i++) {
		file << SimulatedValues.second[i];
		for (size_t j = 0; j < _numStates; j++) { file << PosteriorProbsParam.second[i][j]; }
		file.endln();
	}

	file.close();
}

}; // end namespace stattools
