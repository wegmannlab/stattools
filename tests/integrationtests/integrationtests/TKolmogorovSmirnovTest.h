//
// Created by madleina on 05.03.21.
//

#ifndef EMPTY_TKOLMOGOROVSMIRNOVTEST_H
#define EMPTY_TKOLMOGOROVSMIRNOVTEST_H

#include <functional> // for function
#include <map>        // for map
#include <stddef.h>   // for size_t
#include <string>     // for string, basic_string
#include <utility>    // for pair
#include <vector>     // for vector

#include "coretools/Files/TInputFile.h"
#include "coretools/Main/TError.h"
#include "coretools/Main/TLog.h"
#include "coretools/Math/mathFunctions.h"

namespace stattools {

using ArithmeticFunction = std::function<double(const std::vector<double> &)>;

class TKolmogorovSmirnovTest {
	// Kolmogorov-Smirnov test for evaluating posterior sampling

protected:
	// store values for Kolmogorov-Smirnov test in map
	double _cutoffSignificance_KSTest;
	std::map<std::string, std::vector<double>> _valueInCDF;

	// relevant columns
	std::vector<size_t> _relevantCols;
	std::vector<std::string> _paramNameOfRelevantCols;
	size_t _numRedundantParameters;

	std::map<size_t, std::pair<std::vector<size_t>, ArithmeticFunction>> _columnToJointColumnAndOperatorMap;

	void _parseTraceFile(std::vector<double> &SimulatedValues, coretools::TInputFile &TraceFile,
	                     std::vector<double> &CountersCDF);
	static double _cumulativeDistrFuncUniform01(double x);

	// arithmetic functions that are needed for calculating compound effects of parameters
	static double _add(const std::vector<double> &x);
	static double _sub(const std::vector<double> &x);
	static double _mul(const std::vector<double> &x);
	static double _div(const std::vector<double> &x);
	static double _dotProd(const std::vector<double> &x);
	static ArithmeticFunction _getArithmeticFunction(std::string_view op);
	void _combineIndistinguishableParameters(std::vector<double> &Values);

public:
	TKolmogorovSmirnovTest();

	// setters
	void setCutoffSignificance(double Cutoff);
	void prepareMaps(const std::string &Name, const size_t &NumReps);
	void fillRelevantColumns(coretools::TConstView<std::string> Header);
	void addMapOfIndistinguishableParameters(
	    std::map<std::string, std::vector<std::vector<std::string>>> &OperatorToJointParametersMap);

	// getters
	size_t numParameters();

	// evaluate files
	void evaluateTraceFile(std::vector<double> &SimulatedValues, coretools::TInputFile &traceFile,
	                       const size_t &Replicate);
	void accountForModelSwitch(const std::vector<size_t> &switchedModels, coretools::TConstView<std::string> Header,
	                           std::vector<double> &SimulatedValues,
	                           const std::vector<std::string> &MixedModelParamNames);

	// run!
	bool passKolmogorovSmirnovTest();
};

}; // end namespace stattools

#endif // EMPTY_TKOLMOGOROVSMIRNOVTEST_H
