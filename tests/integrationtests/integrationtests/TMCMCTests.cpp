//
// Created by caduffm on 3/28/22.
//

#include "TMCMCTests.h"

namespace stattools {

std::string simulateInitValsMrr(const std::vector<double> &Alphas) {
	// small hack: very small values of Mrr can cause crazily high variance-covariance matrices
	// want to avoid this in simulation, but don't want complicated boundary-thingy -> just simulate in here
	std::vector<std::string> initVals;
	for (double Alpha : Alphas) {
		double value = sqrt(coretools::instances::randomGenerator().getChisqRand(Alpha));
		while (value < 0.001) { value = sqrt(coretools::instances::randomGenerator().getChisqRand(Alpha)); }
		initVals.push_back(coretools::str::toString(value));
	}
	return coretools::str::concatenateString(initVals, ",");
}

void fillDistancesFromFile(const std::string &Filename, genometools::TDistancesBinned<uint8_t> &Distances) {
	coretools::TInputFile simulatedFile(Filename + "_distances_simulated.txt", coretools::FileType::Header);

	// read distances from file
	for (; !simulatedFile.empty(); simulatedFile.popFront()) {
		Distances.add(simulatedFile.get<size_t>(1), simulatedFile.get(0));
	}
	Distances.finalizeFilling();
}

//--------------------------------------------
// TTest_Normal
//--------------------------------------------

TTest_Normal::TTest_Normal(const std::string &Filename, bool Simulate)
    : _test(Filename),
      _observation("observation", &_test.boxOnObs, coretools::TMultiDimensionalStorage<TNormalTest<>::Type, 1>({10000}),
                   {Filename + "_observations"}) {

	if (!Simulate) fillValuesIntoObservation(Filename, _observation);
}

std::string TTest_Normal::name() { return "normal"; }

//--------------------------------------------
// TTest_Exponential
//--------------------------------------------

TTest_Exponential::TTest_Exponential(const std::string &Filename, bool Simulate)
    : _test(Filename), _observation("observation", &_test.boxOnObs,
                                    coretools::TMultiDimensionalStorage<TExponentialTest::Type, 1>({1000}),
                                    {Filename + "_observations"}) {

	if (!Simulate) fillValuesIntoObservation(Filename, _observation);
}

std::string TTest_Exponential::name() { return "exponential"; }

//--------------------------------------------
// TTest_Poisson
//--------------------------------------------

TTest_Poisson::TTest_Poisson(const std::string &Filename, bool Simulate)
    : _test(Filename),
      _observation("observation", &_test.boxOnObs, coretools::TMultiDimensionalStorage<TPoissonTest::Type, 1>({1000}),
                   {Filename + "_observations"}) {

	if (!Simulate) fillValuesIntoObservation(Filename, _observation);
}

std::string TTest_Poisson::name() { return "poisson"; }

//--------------------------------------------
// TTest_Gamma
//--------------------------------------------

TTest_Gamma::TTest_Gamma(const std::string &Filename, bool Simulate)
    : _test(Filename),
      _observation("observation", &_test.boxOnObs, coretools::TMultiDimensionalStorage<TGammaTest::Type, 1>({1000}),
                   {Filename + "_observations"}) {

	if (!Simulate) fillValuesIntoObservation(Filename, _observation);
}

std::string TTest_Gamma::name() { return "gamma"; }

//--------------------------------------------
// TTest_Beta
//--------------------------------------------

TTest_Beta::TTest_Beta(const std::string &Filename, bool Simulate)
    : _boxOnAlpha(), _boxOnBeta(), _test(Filename, "1", &_boxOnAlpha, &_boxOnBeta),
      _observation("observation", &_test.boxOnObs, coretools::TMultiDimensionalStorage<Beta::Type, 1>({1000}),
                   {Filename + "_observations", 20}) {

	if (!Simulate) fillValuesIntoObservation(Filename, _observation);
}

std::string TTest_Beta::name() { return "beta"; }

//--------------------------------------------
// TTest_Beta_UpdateLog
//--------------------------------------------

TTest_Beta_UpdateLog::TTest_Beta_UpdateLog(const std::string &Filename, bool Simulate)
    : _linkAlpha(Filename, "logAlpha"), _linkBeta(Filename, "logBeta"),
      _test(Filename, "", &_linkAlpha.boxOnBelow, &_linkBeta.boxOnBelow),
      _observation("observation", &_test.boxOnObs, coretools::TMultiDimensionalStorage<Beta::Type, 1>({1000}),
                   {Filename + "_observations", 20}) {
	if (Simulate) { // set values for simulation: uniform prior will set crazy values
		auto r_a = coretools::str::toString(coretools::instances::randomGenerator().getRand(-3., 3.));
		auto r_b = coretools::str::toString(coretools::instances::randomGenerator().getRand(-3., 3.));
		_linkAlpha.param.getDefinition().setInitVal(r_a);
		_linkBeta.param.getDefinition().setInitVal(r_b);
	} else {
		fillValuesIntoObservation(Filename, _observation);
	}
}

std::string TTest_Beta_UpdateLog::name() { return "beta_updateInLog"; }

//--------------------------------------------
// TTest_SymmetricBeta
//--------------------------------------------

TTest_SymmetricBeta::TTest_SymmetricBeta(const std::string &Filename, bool Simulate)
    : _boxOnAlpha(), _test(Filename, "1", &_boxOnAlpha),
      _observation("observation", &_test.boxOnObs, coretools::TMultiDimensionalStorage<Beta::Type, 1>({1000}),
                   {Filename + "_observations", 20}) {

	if (!Simulate) fillValuesIntoObservation(Filename, _observation);
}

std::string TTest_SymmetricBeta::name() { return "symmetricBeta"; }

//--------------------------------------------
// TTest_SymmetricBeta_UpdateLog
//--------------------------------------------

TTest_SymmetricBeta_UpdateLog::TTest_SymmetricBeta_UpdateLog(const std::string &Filename, bool Simulate)
    : _link(Filename, "logAlpha"), _test(Filename, "", &_link.boxOnBelow),
      _observation("observation", &_test.boxOnObs, coretools::TMultiDimensionalStorage<Beta::Type, 1>({1000}),
                   {Filename + "_observations", 20}) {

	if (Simulate) { // set values for simulation: uniform prior will set crazy values
		auto r_a = coretools::str::toString(coretools::instances::randomGenerator().getRand(-3., 3.));
		_link.param.getDefinition().setInitVal(r_a);
	} else {
		fillValuesIntoObservation(Filename, _observation);
	}
}

std::string TTest_SymmetricBeta_UpdateLog::name() { return "symmetricBeta_updateInLog"; }

//--------------------------------------------
// TTest_Binomial
//--------------------------------------------

TTest_Binomial::TTest_Binomial(const std::string &Filename, bool Simulate)
    : _test(Filename), _observation("observation", &_test.boxOnObs,
                                    coretools::TMultiDimensionalStorage<TBinomialTest::Type, 2>({1000, 2}),
                                    {Filename + "_observations"}) {

	TBinomialTest::Type::setMax(5);

	if (!Simulate) fillValuesIntoObservation(Filename, _observation);
}

std::string TTest_Binomial::name() { return "binomial"; }

//--------------------------------------------
// TTest_NegativeBinomial
//--------------------------------------------

TTest_NegativeBinomial::TTest_NegativeBinomial(const std::string &Filename, bool Simulate)
    : _test(Filename), _observation("observation", &_test.boxOnObs,
                                    coretools::TMultiDimensionalStorage<TNegativeBinomialTest::Type, 2>({1000, 2}),
                                    {Filename + "_observations"}) {

	if (!Simulate) fillValuesIntoObservation(Filename, _observation);
}

std::string TTest_NegativeBinomial::name() { return "negativeBinomial"; }

//--------------------------------------------
// TTest_Bernoulli
//--------------------------------------------

TTest_Bernoulli::TTest_Bernoulli(const std::string &Filename, bool Simulate)
    : _test(Filename),
      _observation("observation", &_test.boxOnZ, coretools::TMultiDimensionalStorage<TypeBern::Type, 1>({1000}),
                   {Filename + "_observations"}) {

	if (!Simulate) fillValuesIntoObservation(Filename, _observation);
}

std::string TTest_Bernoulli::name() { return "bernoulli"; }

//--------------------------------------------
// TTest_GibbsBetaBernoulli
//--------------------------------------------

TTest_GibbsBetaBernoulli::TTest_GibbsBetaBernoulli(const std::string &Filename, bool Simulate)
    : _test(Filename), _observation("observation", &_test.boxOnObs,
                                    coretools::TMultiDimensionalStorage<TGibbsBetaBernoulliTest::Type, 1>({1000}),
                                    {Filename + "_observations"}) {

	if (!Simulate) fillValuesIntoObservation(Filename, _observation);
}

std::string TTest_GibbsBetaBernoulli::name() { return "gibbsBetaBernoulli"; }

//--------------------------------------------
// TTest_Categorical
//--------------------------------------------

TTest_Categorical::TTest_Categorical(const std::string &Filename, bool Simulate)
    : _test(Filename, 3, "100,90,10"),
      _observation("observation", &_test.boxOnObs,
                   coretools::TMultiDimensionalStorage<TCategoricalTest::Type, 1>({1000}),
                   {Filename + "_observations"}) {

	if (!Simulate) fillValuesIntoObservation(Filename, _observation);
}

std::string TTest_Categorical::name() { return "categorical"; }

//--------------------------------------------
// TTest_DirichletVar
//--------------------------------------------

TTest_DirichletVar::TTest_DirichletVar(const std::string &Filename, bool Simulate)
    : _test(Filename),
      _observation(
          "observation", &_test.boxOnObs, coretools::TMultiDimensionalStorage<TDirichletTest::Type, 1>({100}),
          {Filename + "_observations",
           "0.0157760798734837,0.00917464797648681,0.0171470090774331,0.00118831542002226,0.00796622938050616,0."
           "0170548077279624,0.00672241090116172,0.00494355747911681,0.00606524916613936,0.0133161741155921,0."
           "00140246104712483,0.0074816709367426,0.0157393078443198,0.0211913090420122,0.00279889975960017,0."
           "0111243592080313,0.0108781852033783,0.0037078344934618,0.00441950534889095,0.00593439018911383,0."
           "00384588003141828,0.00466236959946761,0.0101011150076666,0.0209777899189876,0.00574961150370952,0."
           "0112201657827379,0.0120690666761236,0.0199608093991444,0.0146857445662868,0.00725976555976456,0."
           "00187133095900992,0.0116186834079468,0.00749892493116446,0.0112002843339226,0.0196693862791097,0."
           "0141959987815788,0.0140325323669424,0.011247409150814,0.00291384151128006,0.0047829106553927,0."
           "0121429030900188,0.0128874723033533,0.00521750545269588,0.00620480384093356,0.0162060910345871,0."
           "01633193814402,0.00768326149253541,0.00474867371806704,0.0117001158455985,0.00406230272131625,0."
           "00741615675463234,0.0156740231608297,0.0078721684575911,0.0187001407471314,0.00163079830659406,0."
           "000511827711762423,0.0138168105002481,0.0182954948271335,0.00610933340511604,0.0118932135821832,0."
           "000175297618362548,0.0110756147583005,0.00526618105434093,0.0104995445811397,0.0165675892136061,0."
           "00913128558470974,0.0207155456949098,0.00153326833932676,0.00943137723234897,0.000320138477888132,0."
           "00681336913935756,0.0163482779596863,0.00581413665283685,0.0177173852623516,0.0192238979062929,0."
           "00827700940965827,0.0144171375428482,0.0107299878458919,0.0101030261459117,0.015513021764713,0."
           "0153619272715755,0.0118942665923261,0.0025641740708988,0.0201083817428361,0.00593406799287483,0."
           "00793830509801039,0.00309013630917393,0.0133063216029506,0.00763971289036582,0.00847765527089544,0."
           "0151699127358719,0.00243891098246927,0.00264067106548367,0.00519568492532498,0.00120873006518079,0."
           "00619143009402025,0.0193122732235597,0.0153953910119416,0.0191084924364753,0.0146494027238873"}) {

	if (!Simulate) fillValuesIntoObservation(Filename, _observation);
}

std::string TTest_DirichletVar::name() { return "dirichletVar"; }

//--------------------------------------------
// TTest_DirichletMeanVar
//--------------------------------------------

TTest_DirichletMeanVar::TTest_DirichletMeanVar(const std::string &Filename, bool Simulate)
    : _test(Filename, "200,100,50,20,10"),
      _observation("observation", &_test.boxOnObs,
                   coretools::TMultiDimensionalStorage<TDirichletMeanVarTest::Type, 2>({100, 5}),
                   {Filename + "_observations"}) {

	if (!Simulate) fillValuesIntoObservation(Filename, _observation);
}

std::string TTest_DirichletMeanVar::name() { return "dirichletMeanVar"; }

//--------------------------------------------
// TTest_HMMBool
//--------------------------------------------

TTest_HMMBool::TTest_HMMBool(const std::string &Filename, bool Simulate)
    : _distances(1000000), _test(Filename, &_distances),
      _observation("observation", &_test.boxOnZ, coretools::TMultiDimensionalStorage<TypeHMM::Type, 1>({1000}),
                   {Filename + "_observations"}) {

	if (Simulate) {
		_test.pi.getDefinition().setInitVal("0.1");
		_test.gamma.getDefinition().setInitVal("5");
		coretools::instances::parameters().add("fixedDistances", 1);
		coretools::instances::parameters().add("writeSimulatedDistances", Filename + "_distances_simulated.txt");
	} else {
		fillValuesIntoObservation(Filename, _observation);
		fillDistancesFromFile(Filename, _distances);
	}
}

std::string TTest_HMMBool::name() { return "hmm_bool"; }

//--------------------------------------------
// TTest_HMMBoolGeneratingMatrix
//--------------------------------------------

TTest_HMMBoolGeneratingMatrix::TTest_HMMBoolGeneratingMatrix(const std::string &Filename, bool Simulate)
    : _distances(1000000), _test(Filename, &_distances),
      _observation("observation", &_test.boxOnObs,
                   coretools::TMultiDimensionalStorage<THMMBoolGeneratingTest::Type, 1>({5000}),
                   {Filename + "_observations"}) {

	if (Simulate) {
		// = log(5.182507e-05) -> results in expected peak width of 20 (for distance 10, which is lambda for poisson)
		_test.log_lambda_1.getDefinition().setInitVal("-9.867637");
		// = log(0.005130682) -> results in expected peak width of 20 (for distance 10, which is lambda for poisson)
		_test.log_lambda_2.getDefinition().setInitVal("-5.272517");
		coretools::instances::parameters().add("writeSimulatedDistances", Filename + "_distances_simulated.txt");
	} else {
		fillValuesIntoObservation(Filename, _observation);
		fillDistancesFromFile(Filename, _distances);
	}
}

std::string TTest_HMMBoolGeneratingMatrix::name() { return "hmm_bool_generating_matrix"; }

//--------------------------------------------
// TTest_HMMCategorical
//--------------------------------------------

TTest_HMMCategorical::TTest_HMMCategorical(const std::string &Filename, bool Simulate)
    : _distances(1000000), _test(Filename, _D, &_distances),
      _observation("observation", &_test.boxOnObs,
                   coretools::TMultiDimensionalStorage<THMMCategoricalTest::Type, 1>({1000}),
                   {Filename + "_observations"}) {

	if (Simulate) {
		_test.pi.getDefinition().setInitVal("0.1");
		_test.gamma.getDefinition().setInitVal("5");
		_test.rhos.getDefinition().setInitVal("0.1, 0.2, 0.3, 0.25, 0.15");
		coretools::instances::parameters().add("fixedDistances", 1);
		coretools::instances::parameters().add("writeSimulatedDistances", Filename + "_distances_simulated.txt");
	} else {
		fillValuesIntoObservation(Filename, _observation);
		fillDistancesFromFile(Filename, _distances);
	}
}

std::string TTest_HMMCategorical::name() { return "hmm_categorical"; }

//--------------------------------------------
// TTest_HMMLadder
//--------------------------------------------

TTest_HMMLadder::TTest_HMMLadder(const std::string &Filename, bool Simulate)
    : _distances(1000000), _test(Filename, _K, &_distances),
      _observation("observation", &_test.boxOnObs, coretools::TMultiDimensionalStorage<THMMLadderTest::Type, 1>({1000}),
                   {Filename + "_observations"}) {

	if (Simulate) {
		coretools::instances::parameters().add("writeSimulatedDistances", Filename + "_distances_simulated.txt");
	} else {
		fillValuesIntoObservation(Filename, _observation);
		fillDistancesFromFile(Filename, _distances);
	}
}

std::string TTest_HMMLadder::name() { return "hmm_ladder"; }

//--------------------------------------------
// TTest_HMMScaledLadderCombined
//--------------------------------------------

TTest_HMMScaledLadderCombined::TTest_HMMScaledLadderCombined(const std::string &Filename, bool Simulate)
    : _distances(1000000), _test(Filename, _numStatesPerChain, &_distances),
      _observation("observation", &_test.boxOnObs, coretools::TMultiDimensionalStorage<Type, 1>({1000}),
                   {Filename + "_observations"}) {

	if (Simulate) {
		coretools::instances::parameters().add("writeSimulatedDistances", Filename + "_distances_simulated.txt");
	} else {
		fillValuesIntoObservation(Filename, _observation);
		fillDistancesFromFile(Filename, _distances);
	}
}

std::string TTest_HMMScaledLadderCombined::name() { return "hmm_scaledLadderCombined"; }

//--------------------------------------------
// TTest_MultivariateNormal
//--------------------------------------------

TTest_MultivariateNormal::TTest_MultivariateNormal(const std::string &Filename, bool Simulate)
    : _test(Filename), _observation("observation", &_test.boxOnObs,
                                    coretools::TMultiDimensionalStorage<TMultivariateNormalTest::Type, 2>({100, 3}),
                                    {Filename + "_observations"}) {

	if (Simulate) {
		_test.Mrr.getDefinition().setInitVal(
		    simulateInitValsMrr({3, 2, 1})); // simulate an initial value for Mrr that is > 0.001
	} else {
		fillValuesIntoObservation(Filename, _observation);
	}
}

std::string TTest_MultivariateNormal::name() { return "multivariateNormal"; }

void TTest_MultivariateNormal::fillIndistinguishableParameters(
    std::map<std::string, std::vector<std::vector<std::string>>> &OperatorToJointParametersMap) {
	// parameters m and Mrr / Mrs can not be distinguished
	for (size_t i = 1; i <= 3; i++) {
		OperatorToJointParametersMap["/"].push_back({"Mrr_" + coretools::str::toString(i), "m"});
		for (size_t j = 1; j < i; j++) {
			OperatorToJointParametersMap["/"].push_back(
			    {"Mrs_" + coretools::str::toString(i) + "_" + coretools::str::toString(j), "m"});
		}
	}
}

//--------------------------------------------
// TTest_NormalMixedModel
//--------------------------------------------
/*
TTest_NormalMixedModel::TTest_NormalMixedModel(const std::string &Filename, bool Simulate)
    : _mean_0("mu_0", std::make_shared<prior::TNormalFixed<TParameterBase, SpecMus ::value_type, SpecMus ::numDim>>(),
              {Filename, "2, 0.1"}),
      _mean_1("mu_1", std::make_shared<prior::TNormalFixed<TParameterBase, SpecMus ::value_type, SpecMus ::numDim>>(),
              {Filename, "-2, 0.1"}),
      _mean_2("mu_2", std::make_shared<prior::TNormalFixed<TParameterBase, SpecMus ::value_type, SpecMus ::numDim>>(),
              {Filename, "0§, 0.1"}),
      _var_0("var_0",
             std::make_shared<prior::TExponentialFixed<TParameterBase, SpecVars ::value_type, SpecVars ::numDim>>(),
             {Filename, "5"}),
      _var_1("var_1",
             std::make_shared<prior::TExponentialFixed<TParameterBase, SpecVars ::value_type, SpecVars ::numDim>>(),
             {Filename, "5"}),
      _var_2("var_2",
             std::make_shared<prior::TExponentialFixed<TParameterBase, SpecVars ::value_type, SpecVars ::numDim>>(),
             {Filename, "5"}),
      _pi("pis", std::make_shared<prior::TDirichletFixed<TParameterBase, SpecPis ::value_type, SpecPis ::numDim>>(),
          {Filename, "100,86,14", 0}), // start dimension name indices at zero, in order to match mixed model indices
      _z("z",
         std::make_shared<prior::TCategoricalInferred<TParameterBase, SpecZ::value_type, SpecZ::numDim, SpecPis>>(&_pi,
                                                                                                                  _K),
         {Filename}),
      _observation("observation",
                   std::make_shared<BoxTypeMixedModel>(
                       &_z, std::array<TParameter<SpecMus, BoxTypeMixedModel> *, _K>{&_mean_0, &_mean_1, &_mean_2},
                       std::array<TParameter<SpecVars, BoxTypeMixedModel> *, _K>{&_var_0, &_var_1, &_var_2}),
                   coretools::TMultiDimensionalStorage<Type, 1>({1000}), {Filename + "_observations"}) {

    SpecZ::value_type::setMax(_K - 1);

    if (!Simulate) fillValuesIntoObservation(Filename, _observation);
}

std::string TTest_NormalMixedModel::name() { return "normalMixedModel"; }

void TTest_NormalMixedModel::fillSwitchableParameters(std::vector<std::string> &ParamNamesMixedModels,
                                                      std::map<std::string, size_t> &ParamNamesAndNumStatesForSCTest) {
    ParamNamesMixedModels                = {"mu", "var", "pis"}; // these parameters can potentially be switched
    ParamNamesAndNumStatesForSCTest["z"] = 3;
}
*/

//--------------------------------------------
// TTest_NormalMixedModel_HMM
//--------------------------------------------

/*
TTest_NormalMixedModel_HMM::TTest_NormalMixedModel_HMM(const std::string &Filename, TDAGBuilder *DAGBuilder,
                                                       bool Simulate)
    : _mean_0("mu_0", std::make_shared<prior::TNormalFixed<TParameterBase, SpecMus ::value_type, SpecMus ::numDim>>(),
              {Filename, "2, 0.1"}),
      _mean_1("mu_1", std::make_shared<prior::TNormalFixed<TParameterBase, SpecMus ::value_type, SpecMus ::numDim>>(),
              {Filename, "-2, 0.1"}),
      _mean_2("mu_2", std::make_shared<prior::TNormalFixed<TParameterBase, SpecMus ::value_type, SpecMus ::numDim>>(),
              {Filename, "0, 0.1"}),
      _var_0("var_0",
             std::make_shared<prior::TExponentialFixed<TParameterBase, SpecVars ::value_type, SpecVars ::numDim>>(),
             {Filename, "5"}),
      _var_1("var_1",
             std::make_shared<prior::TExponentialFixed<TParameterBase, SpecVars ::value_type, SpecVars ::numDim>>(),
             {Filename, "5"}),
      _var_2("var_2",
             std::make_shared<prior::TExponentialFixed<TParameterBase, SpecVars ::value_type, SpecVars ::numDim>>(),
             {Filename, "5"}),
      _pi("pi", std::make_shared<prior::TBetaFixed<TParameterBase, SpecPi ::value_type, SpecPi ::numDim>>(),
          {Filename, "0.8, 7.2"}),
      _gamma("gamma",
             std::make_shared<prior::TExponentialFixed<TParameterBase, SpecGamma ::value_type, SpecGamma ::numDim>>(),
             {Filename, "0.2", ProposalKernel::MCMCProposalKernel::scaleLogNormal}),
      _rhos("rhos", std::make_shared<prior::TUniformFixed<TParameterBase, SpecRho ::value_type, SpecRho ::numDim>>(),
            {Filename}),
      _distances(1000000),
      _z("z",
         std::make_shared<prior::THMMCategoricalInferred<TParameterBase, SpecZ::value_type, SpecZ::numDim, SpecPi,
                                                         SpecGamma, SpecRho>>(&_pi, &_gamma, &_rhos, _K - 1,
                                                                              &_distances),
         {Filename}),
      _observation("observation",
                   std::make_shared<BoxTypeMixedModel>(
                       &_z, std::array<TParameter<SpecMus, BoxTypeMixedModel> *, _K>{&_mean_0, &_mean_1, &_mean_2},
                       std::array<TParameter<SpecVars, BoxTypeMixedModel> *, _K>{&_var_0, &_var_1, &_var_2}),
                   coretools::TMultiDimensionalStorage<Type, 1>({1000}), {Filename + "_observations"}) {
    SpecZ::value_type ::setMax(_K - 1);

    if (Simulate) {
        _pi.getDefinition().setInitVal("0.1");
        _gamma.getDefinition().setInitVal("5");
        _rhos.getDefinition().setInitVal("0.3,0.7");
        coretools::instances::parameters().add("fixedDistances", 1);
        coretools::instances::parameters().add("writeSimulatedDistances", Filename + "_distances_simulated.txt");
    } else {
        fillValuesIntoObservation(Filename, _observation);
        fillDistancesFromFile(Filename, _distances);
    }
}

std::string TTest_NormalMixedModel_HMM::name() { return "normalMixedModel_HMM"; }

void TTest_NormalMixedModel_HMM::fillSwitchableParameters(
    std::vector<std::string> &ParamNamesMixedModels, std::map<std::string, size_t> &ParamNamesAndNumStatesForSCTest) {
    ParamNamesMixedModels                = {"mu", "var"}; // these parameters can potentially be switched
    ParamNamesAndNumStatesForSCTest["z"] = 3;
}
*/

//--------------------------------------------
// TTest_NormalMixedModel_Stretch
//--------------------------------------------

TTest_NormalMixedModel_Stretch::TTest_NormalMixedModel_Stretch(const std::string &Filename, bool Simulate)
    : _bernoulli(Filename), _test(Filename, &_bernoulli.boxOnZ),
      _observation("observation", &_test.boxOnObs, coretools::TMultiDimensionalStorage<TypeMM::Type, 1>({1000}),
                   {Filename + "_observations"}) {

	if (!Simulate) fillValuesIntoObservation(Filename, _observation);
}

std::string TTest_NormalMixedModel_Stretch::name() { return "normalMixedModelStretch"; }

void TTest_NormalMixedModel_Stretch::fillSwitchableParameters(
    std::vector<std::string> &ParamNamesMixedModels, std::map<std::string, size_t> &ParamNamesAndNumStatesForSCTest) {
	ParamNamesMixedModels                = {"var"}; // these parameters can potentially be switched
	ParamNamesAndNumStatesForSCTest["z"] = 2;
}

//------------------------------------------------
// TTest_NormalMixedModel_Stretch_HMM
//------------------------------------------------

TTest_NormalMixedModel_Stretch_HMM::TTest_NormalMixedModel_Stretch_HMM(const std::string &Filename, bool Simulate)
    : _distances(1000000), _hmm(Filename, &_distances), _test(Filename, &_hmm.boxOnZ),
      _observation("observation", &_test.boxOnObs, coretools::TMultiDimensionalStorage<TypeMM::Type, 1>({5000}),
                   {Filename + "_observations"}) {

	if (Simulate) {
		_hmm.pi.getDefinition().setInitVal("0.1");
		_hmm.gamma.getDefinition().setInitVal("5");
		coretools::instances::parameters().add("fixedDistances", 1);
		coretools::instances::parameters().add("writeSimulatedDistances", Filename + "_distances_simulated.txt");
	} else {
		fillValuesIntoObservation(Filename, _observation);
		fillDistancesFromFile(Filename, _distances);
	}
}

std::string TTest_NormalMixedModel_Stretch_HMM::name() { return "normalMixedModelStretch_HMM"; }

void TTest_NormalMixedModel_Stretch_HMM::fillSwitchableParameters(
    std::vector<std::string> &ParamNamesMixedModels, std::map<std::string, size_t> &ParamNamesAndNumStatesForSCTest) {
	ParamNamesMixedModels                = {"var"}; // these parameters can potentially be switched
	ParamNamesAndNumStatesForSCTest["z"] = 2;
}

//--------------------------------------------
// TTest_MultivariateNormalMixedModel
//--------------------------------------------

/*
TTest_MultivariateNormalMixedModel::TTest_MultivariateNormalMixedModel(const std::string &Filename,
                                                                       bool Simulate)
    : _mu_0("mu_0", std::make_shared<prior::TNormalFixed<TParameterBase, SpecMus ::value_type, SpecMus ::numDim>>(),
            {Filename, "2, 0.1"}),
      _mu_1("mu_1", std::make_shared<prior::TNormalFixed<TParameterBase, SpecMus ::value_type, SpecMus ::numDim>>(),
            {Filename, "-2, 0.1"}),
      _mu_2("mu_2", std::make_shared<prior::TNormalFixed<TParameterBase, SpecMus ::value_type, SpecMus ::numDim>>(),
            {Filename, "0, 0.1"}),
      _m_0("m_0", std::make_shared<prior::TExponentialFixed<TParameterBase, SpecM ::value_type, SpecM ::numDim>>(),
           {Filename, "5"}),
      _m_1("m_1", std::make_shared<prior::TExponentialFixed<TParameterBase, SpecM ::value_type, SpecM ::numDim>>(),
           {Filename, "5"}),
      _m_2("m_2", std::make_shared<prior::TExponentialFixed<TParameterBase, SpecM ::value_type, SpecM ::numDim>>(),
           {Filename, "5"}),
      _Mrr_0("mrr_0",
             std::make_shared<prior::TMultivariateChiFixed<TParameterBase, SpecMrr ::value_type, SpecMrr ::numDim>>(),
             {Filename, "3,2,1"}),
      _Mrr_1("mrr_1",
             std::make_shared<prior::TMultivariateChiFixed<TParameterBase, SpecMrr ::value_type, SpecMrr ::numDim>>(),
             {Filename, "3,2,1"}),
      _Mrr_2("mrr_2",
             std::make_shared<prior::TMultivariateChiFixed<TParameterBase, SpecMrr ::value_type, SpecMrr ::numDim>>(),
             {Filename, "3,2,1"}),
      _Mrs_0("mrs_0", std::make_shared<prior::TNormalFixed<TParameterBase, SpecMrs ::value_type, SpecMrs ::numDim>>(),
             {Filename, "0,1"}),
      _Mrs_1("mrs_1", std::make_shared<prior::TNormalFixed<TParameterBase, SpecMrs ::value_type, SpecMrs ::numDim>>(),
             {Filename, "0,1"}),
      _Mrs_2("mrs_2", std::make_shared<prior::TNormalFixed<TParameterBase, SpecMrs ::value_type, SpecMrs ::numDim>>(),
             {Filename, "0,1"}),
      _pi("pis", std::make_shared<prior::TDirichletFixed<TParameterBase, SpecPis ::value_type, SpecPis ::numDim>>(),
          {Filename, "100,86,14", 0}), // start dimension name indices at zero, in order to match mixed model indices
      _z("z",
         std::make_shared<prior::TCategoricalInferred<TParameterBase, SpecZ::value_type, SpecZ::numDim, SpecPis>>(&_pi,
                                                                                                                  _K),
         {Filename}),
      _observation("observation",
                   std::make_shared<BoxTypeMixedModel>(
                       &_z, std::array<TParameter<SpecMus, BoxTypeMixedModel> *, _K>{&_mu_0, &_mu_1, &_mu_2},
                       std::array<TParameter<SpecM, BoxTypeMixedModel> *, _K>{&_m_0, &_m_1, &_m_2},
                       std::array<TParameter<SpecMrr, BoxTypeMixedModel> *, _K>{&_Mrr_0, &_Mrr_1, &_Mrr_2},
                       std::array<TParameter<SpecMrs, BoxTypeMixedModel> *, _K>{&_Mrs_0, &_Mrs_1, &_Mrs_2}),
                   coretools::TMultiDimensionalStorage<Type, 2>({1000, 3}), {Filename + "_observations"}) {
    SpecZ::value_type::setMax(_K - 1);

    DAGBuilder->addToDAG(&_mu_0);
    DAGBuilder->addToDAG(&_mu_1);
    DAGBuilder->addToDAG(&_mu_2);
    DAGBuilder->addToDAG(&_m_0);
    DAGBuilder->addToDAG(&_m_1);
    DAGBuilder->addToDAG(&_m_2);
    DAGBuilder->addToDAG(&_Mrr_0);
    DAGBuilder->addToDAG(&_Mrr_1);
    DAGBuilder->addToDAG(&_Mrr_2);
    DAGBuilder->addToDAG(&_Mrs_0);
    DAGBuilder->addToDAG(&_Mrs_1);
    DAGBuilder->addToDAG(&_Mrs_2);
    DAGBuilder->addToDAG(&_pi);
    DAGBuilder->addToDAG(&_z);
    DAGBuilder->addToDAG(&_observation);

    if (Simulate) {
        _Mrr_0.getDefinition().setInitVal(simulateInitValsMrr({3, 2, 1}));
        _Mrr_1.getDefinition().setInitVal(simulateInitValsMrr({3, 2, 1}));
        _Mrr_2.getDefinition().setInitVal(simulateInitValsMrr({3, 2, 1}));
    } else {
        fillValuesIntoObservation(Filename, _observation);
    }
}

std::string TTest_MultivariateNormalMixedModel::name() { return "multivariateNormalMixedModel"; }

void TTest_MultivariateNormalMixedModel::fillIndistinguishableParameters(
    std::map<std::string, std::vector<std::vector<std::string>>> &OperatorToJointParametersMap) {

    // parameters m and Mrr / Mrs can not be distinguished
    for (size_t i = 1; i <= 3; i++) {
        OperatorToJointParametersMap["/"].push_back({"mrr_0_" + coretools::str::toString(i), "m_0"});
        OperatorToJointParametersMap["/"].push_back({"mrr_1_" + coretools::str::toString(i), "m_1"});
        OperatorToJointParametersMap["/"].push_back({"mrr_2_" + coretools::str::toString(i), "m_2"});
        for (size_t j = 1; j < i; j++) {
            OperatorToJointParametersMap["/"].push_back(
                {"mrs_0_" + coretools::str::toString(i) + "_" + coretools::str::toString(j), "m_0"});
            OperatorToJointParametersMap["/"].push_back(
                {"mrs_1_" + coretools::str::toString(i) + "_" + coretools::str::toString(j), "m_1"});
            OperatorToJointParametersMap["/"].push_back(
                {"mrs_2_" + coretools::str::toString(i) + "_" + coretools::str::toString(j), "m_2"});
        }
    }
}

void TTest_MultivariateNormalMixedModel::fillSwitchableParameters(
    std::vector<std::string> &ParamNamesMixedModels, std::map<std::string, size_t> &ParamNamesAndNumStatesForSCTest) {
    ParamNamesMixedModels = {"mu", "m", "mrr", "mrs", "pis"}; // these parameters can potentially be switched
    ParamNamesAndNumStatesForSCTest["z"] = 3;
}
*/

//--------------------------------------------
// TTest_MultivariateNormalMixedModel
//--------------------------------------------

TTest_MultivariateNormalMixedModel_Stretch::TTest_MultivariateNormalMixedModel_Stretch(const std::string &Filename,
                                                                                       bool Simulate)
    : _bernoulli(Filename), _test(Filename, &_bernoulli.boxOnZ),
      _observation("observation", &_test.boxOnObs,
                   coretools::TMultiDimensionalStorage<TypeMM::Type, TypeMM ::NumDimObs>({10000, 3}),
                   {Filename + "_observations"}) {

	if (Simulate) {
		_test.Mrr.getDefinition().setInitVal(simulateInitValsMrr({3, 2, 1}));
	} else {
		fillValuesIntoObservation(Filename, _observation);
	}
}

std::string TTest_MultivariateNormalMixedModel_Stretch::name() { return "multivariateNormalMixedModelStretch"; }

void TTest_MultivariateNormalMixedModel_Stretch::fillIndistinguishableParameters(
    std::map<std::string, std::vector<std::vector<std::string>>> &OperatorToJointParametersMap) {
	// parameters m and Mrr / Mrs can not be distinguished
	for (size_t i = 1; i <= 3; i++) {
		OperatorToJointParametersMap["/"].push_back({"Mrr_" + coretools::str::toString(i), "m"});
		for (size_t j = 1; j < i; j++) {
			OperatorToJointParametersMap["/"].push_back(
			    {"Mrs_" + coretools::str::toString(i) + "_" + coretools::str::toString(j), "m"});
		}
	}
}

void TTest_MultivariateNormalMixedModel_Stretch::fillSwitchableParameters(
    std::vector<std::string> &, std::map<std::string, size_t> &ParamNamesAndNumStatesForSCTest) {
	ParamNamesAndNumStatesForSCTest["z"] = 2;
}

//--------------------------------------------
// TTest_MultivariateNormalMixedModel_Stretch_HMM
//--------------------------------------------

TTest_MultivariateNormalMixedModel_Stretch_HMM::TTest_MultivariateNormalMixedModel_Stretch_HMM(
    const std::string &Filename, bool Simulate)
    : _distances(1000000), _hmm(Filename, &_distances), _test(Filename, &_hmm.boxOnZ),
      _observation("observation", &_test.boxOnObs,
                   coretools::TMultiDimensionalStorage<TypeMM::Type, TypeMM::NumDimObs>({10000, 3}),
                   {Filename + "_observations"}) {

	if (Simulate) {
		_hmm.pi.getDefinition().setInitVal("0.1");
		_hmm.gamma.getDefinition().setInitVal("5");
		coretools::instances::parameters().add("fixedDistances", 100);
		coretools::instances::parameters().add("writeSimulatedDistances", Filename + "_distances_simulated.txt");
	} else {
		fillValuesIntoObservation(Filename, _observation);
		fillDistancesFromFile(Filename, _distances);
	}
}

std::string TTest_MultivariateNormalMixedModel_Stretch_HMM::name() { return "multivariateNormalMixedModelStretch_HMM"; }

void TTest_MultivariateNormalMixedModel_Stretch_HMM::fillIndistinguishableParameters(
    std::map<std::string, std::vector<std::vector<std::string>>> &OperatorToJointParametersMap) {
	// parameters m and Mrr / Mrs can not be distinguished
	for (size_t i = 1; i <= 3; i++) {
		OperatorToJointParametersMap["/"].push_back({"Mrr_" + coretools::str::toString(i), "m"});
		for (size_t j = 1; j < i; j++) {
			OperatorToJointParametersMap["/"].push_back(
			    {"Mrs_" + coretools::str::toString(i) + "_" + coretools::str::toString(j), "m"});
		}
	}
}

void TTest_MultivariateNormalMixedModel_Stretch_HMM::fillSwitchableParameters(
    std::vector<std::string> &, std::map<std::string, size_t> &ParamNamesAndNumStatesForSCTest) {
	ParamNamesAndNumStatesForSCTest["z"] = 2;
}

//--------------------------------------------
// TTest_LinearModel_Intercept
//--------------------------------------------

TTest_LinearModel_Intercept::TTest_LinearModel_Intercept(const std::string &Filename, bool Simulate)
    : _bernoulli(Filename), _mixedModel(Filename, &_bernoulli.boxOnZ),
      _test(Filename, {1000, 1}, &_mixedModel.boxOnObs),
      _observation("observation", &_test.boxOnBelow,
                   coretools::TMultiDimensionalStorage<TypeLinearModel::TypeBelow, 2>({1000, 7}),
                   {Filename + "_observations", 20}) {

	_test.beta.getDefinition().setJumpSizeForAll(false);

	// fill values into x
	if (Simulate) {
		coretools::TOutputFile file("x.txt");
		for (auto &i : _test.x) {
			i = coretools::instances::randomGenerator().getNormalRandom(0.0, 1.0);
			file << i;
		}
		file.endln();
	} else {
		// read
		coretools::TInputFile file("x.txt", coretools::FileType::NoHeader);
		for (; !file.empty(); file.popFront()) {
			for (size_t i = 0; i < _test.x.size(); ++i) { _test.x[i] = file.get<double>(i); }
		}
	}

	if (!Simulate) fillValuesIntoObservation(Filename, _observation);
}

std::string TTest_LinearModel_Intercept::name() { return "linearModelWithIntercept"; }

} // namespace stattools
