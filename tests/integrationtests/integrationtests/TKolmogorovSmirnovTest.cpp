//
// Created by madleina on 05.03.21.
//

#include "TKolmogorovSmirnovTest.h"

#include <algorithm> // for find, max
#include <assert.h>  // for assert
#include <iterator>  // for distance
#include <stdexcept> // for runtime_error

#include "coretools/Main/TLog.h"          // for TLog
#include "coretools/Math/mathFunctions.h" // for runOneSampleKolmogorovSmirnovTest
#include "coretools/Strings/stringProperties.h"
#include "coretools/Types/probability.h" // for Probability

namespace stattools {
using namespace coretools::str;

TKolmogorovSmirnovTest::TKolmogorovSmirnovTest() {
	_cutoffSignificance_KSTest = 0.;
	_numRedundantParameters    = 0;
}

void TKolmogorovSmirnovTest::setCutoffSignificance(double Cutoff) { _cutoffSignificance_KSTest = Cutoff; }

void TKolmogorovSmirnovTest::prepareMaps(const std::string &Name, const size_t &NumReps) {
	_valueInCDF[Name].resize(NumReps);
}

void TKolmogorovSmirnovTest::fillRelevantColumns(coretools::TConstView<std::string> Header) {
	// fill vector that indicates which columns from a file are relevant for this test!
	_relevantCols.clear();
	for (size_t col = 0; col < Header.size(); col++) {
		auto it = _valueInCDF.find(Header[col]);
		if (it != _valueInCDF.end()) { // found
			_relevantCols.push_back(col);
			_paramNameOfRelevantCols.push_back(Header[col]);
		}
	}
}

double TKolmogorovSmirnovTest::_add(const std::vector<double> &x) {
	assert(x.size() == 2);
	return x[0] + x[1];
}

double TKolmogorovSmirnovTest::_sub(const std::vector<double> &x) {
	assert(x.size() == 2);
	return x[0] - x[1];
}

double TKolmogorovSmirnovTest::_mul(const std::vector<double> &x) {
	assert(x.size() == 2);
	return x[0] * x[1];
}

double TKolmogorovSmirnovTest::_div(const std::vector<double> &x) {
	assert(x.size() == 2);
	return x[0] / x[1];
}

double TKolmogorovSmirnovTest::_dotProd(const std::vector<double> &x) {
	// IMPORTANT: in order for this to work, I expect that two consecutive elements of x must be multiplied!
	// e.g. sum_k v_k * u_k  --> what I want here, is: x = {v_1, u_1, v_2, u_2, v_3, u_3, ..., v_K, u_K}
	double result = 0.;
	for (size_t i = 0; i < x.size(); i += 2) { result += x[i] * x[i + 1]; }
	return result;
}

ArithmeticFunction TKolmogorovSmirnovTest::_getArithmeticFunction(std::string_view op) {
	if (op == "+") {
		return _add;
	} else if (op == "-") {
		return _sub;
	} else if (op == "*") {
		return _mul;
	} else if (op == "/") {
		return _div;
	} else if (op == "%*%") {
		return _dotProd;
	} else
		DEVERROR("Unknown operator ", op, "!");
}

void TKolmogorovSmirnovTest::addMapOfIndistinguishableParameters(
    std::map<std::string, std::vector<std::vector<std::string>>> &OperatorToJointParametersMap) {

	std::vector<size_t> colsToRemove;
	for (auto &op : OperatorToJointParametersMap) {
		for (auto &combinedParams : op.second) {
			std::vector<size_t> columnOfParameters;
			for (auto &param : combinedParams) {
				// find name of parameter in vector
				auto it = std::find(_paramNameOfRelevantCols.begin(), _paramNameOfRelevantCols.end(), param);
				if (it != _paramNameOfRelevantCols.end()) { // found
					auto positionInVec = std::distance(_paramNameOfRelevantCols.begin(), it);
					columnOfParameters.push_back(_relevantCols[positionInVec]);
				} else {
					DEVERROR("Could not find parameter ", param, " in map _paramNameOfRelevantCols!");
				}
			}

			// get relevant operator
			ArithmeticFunction funcPtr = _getArithmeticFunction(op.first);

			// add to map
			_columnToJointColumnAndOperatorMap[columnOfParameters[0]] = std::make_pair(columnOfParameters, funcPtr);

			// add all but first parameters to vector of columns that will be removed from relevant cols
			for (auto it = columnOfParameters.begin() + 1; it != columnOfParameters.end(); it++) {
				colsToRemove.push_back(*it);
			}
		}
	}

	// remove compound parameters from all vectors and maps
	for (auto &colToRemove : colsToRemove) {
		auto it = std::find(_relevantCols.begin(), _relevantCols.end(), colToRemove);
		if (it != _relevantCols.end()) {
			_numRedundantParameters++;
			auto positionInVec = std::distance(_relevantCols.begin(), it);
			_relevantCols.erase(it);

			std::string name = _paramNameOfRelevantCols[positionInVec];
			_paramNameOfRelevantCols.erase(_paramNameOfRelevantCols.begin() + positionInVec);
			auto it2 = _valueInCDF.find(name);
			if (it2 != _valueInCDF.end()) { // found
				_valueInCDF.erase(it2);
			}
		}
	}
}

void TKolmogorovSmirnovTest::_combineIndistinguishableParameters(std::vector<double> &Values) {
	std::vector<double> newValues = Values;
	for (auto &relevantCol : _relevantCols) {
		auto it = _columnToJointColumnAndOperatorMap.find(relevantCol);
		if (it != _columnToJointColumnAndOperatorMap.end()) { // found
			// gather function that should be applied
			auto arithmeticFunction = it->second.second;
			// gather columns that are affected
			auto cols               = it->second.first;
			std::vector<double> relevantValues(cols.size());
			size_t c = 0;
			for (auto &col : cols) {
				relevantValues[c] = Values[col];
				c++;
			}
			// apply function
			newValues[relevantCol] = arithmeticFunction(relevantValues);
		} // else do nothing, column does not need to be combined
	}
	Values = newValues;
}

void TKolmogorovSmirnovTest::_parseTraceFile(std::vector<double> &SimulatedValues, coretools::TInputFile &TraceFile,
                                             std::vector<double> &CountersCDF) {
	// CountersCDF stores how many times a trace value was smaller or equal than the true value
	// -> corresponds to the position of the true value in the empirical cumulative distribution function
	CountersCDF.resize(TraceFile.numCols(), 0.);

	// first combine indistinguishable parameters inside SimulatedValues
	_combineIndistinguishableParameters(SimulatedValues);

	// parse trace file
	for (; !TraceFile.empty(); TraceFile.popFront()) {
		// first combine indistinguishable parameters inside trace_OneIteration
		std::vector<double> line(TraceFile.numCols());
		for (size_t i = 0; i < TraceFile.numCols(); ++i) { line[i] = TraceFile.get<double>(i); }
		_combineIndistinguishableParameters(line);

		// go over all relevant columns
		for (auto &relevantCol : _relevantCols) {
			if (TraceFile.get<double>(relevantCol) <= SimulatedValues[relevantCol]) {
				// if trace <= true: add +1 to counter
				CountersCDF[relevantCol]++;
			}
		}
	}
}

void TKolmogorovSmirnovTest::evaluateTraceFile(std::vector<double> &SimulatedValues, coretools::TInputFile &TraceFile,
                                               const size_t &Replicate) {
	// parse trace file
	std::vector<double> countersCDF;
	_parseTraceFile(SimulatedValues, TraceFile, countersCDF);

	// assign values from trace file to parameter in map
	for (auto &relevantCol : _relevantCols) {
		// calculate mean
		countersCDF[relevantCol] /= static_cast<double>(TraceFile.curLine());

		// add to map
		std::string paramName = TraceFile.header()[relevantCol];
		auto it               = _valueInCDF.find(paramName);
		if (it != _valueInCDF.end()) { // found
			it->second[Replicate] = countersCDF[relevantCol];
		} else {
			DEVERROR("Could not find parameter ", paramName, " in map _valueInCDF!");
		}
	}
}

void TKolmogorovSmirnovTest::accountForModelSwitch(const std::vector<size_t> &switchedModels,
                                                   coretools::TConstView<std::string> Header,
                                                   std::vector<double> &SimulatedValues,
                                                   const std::vector<std::string> &MixedModelParamNames) {
	// account for model switch: switch simulated values of mixture parameters

	std::vector<double> simulatedValues_Old = SimulatedValues;
	// go over all relevant parameters
	for (auto &relevantCol : _relevantCols) {
		// get name of current parameter
		std::string paramName = Header[relevantCol];
		// check if this is potentially a mixed-model parameter
		bool found            = false;
		for (auto &mixedModelParamName : MixedModelParamNames) {
			if (stringStartsWith(paramName, mixedModelParamName)) { found = true; }
		}
		if (found) {
			// this is the previous model -> now get the new one
			TSplitter splitter(paramName, '_');
			// first before _ is the parameterName
			auto splitIt                           = splitter.begin();
			std::string_view paramNameWithoutModel = *splitIt;
			// next comes the model
			++splitIt;
			auto previousModel = fromString<size_t>(*splitIt);
			// all others are dimensions
			++splitIt;
			std::string dimensions;
			for (; splitIt != splitter.end(); splitIt++) { dimensions += "_" + (std::string)*splitIt; }

			// find new model number
			auto it = std::find(switchedModels.begin(), switchedModels.end(), previousModel);
			if (it != switchedModels.end()) {
				size_t newModel         = it - switchedModels.begin();
				// find parameter with that name
				std::string nameOfOther = (std::string)paramNameWithoutModel + "_" + toString(newModel) + dimensions;
				auto it2                = std::find(Header.begin(), Header.end(), nameOfOther);
				if (it2 != Header.end()) {
					size_t columnOfOther         = it2 - Header.begin();
					SimulatedValues[relevantCol] = simulatedValues_Old[columnOfOther];
				} else {
					DEVERROR("Could not find parameter ", nameOfOther, " inside Header!");
				}
			} else {
				DEVERROR("Could not find model ", previousModel, " inside _switchedModels!");
			}
		}
	}
}

double TKolmogorovSmirnovTest::_cumulativeDistrFuncUniform01(double x) {
	// we expect that position of true values inside cumulative distribution of posterior is uniform (between 0 and 1)
	// this function is used by the Kolmogorov-Smirnov-Test to check if samples are indeed uniform

	// cumulative distribution function of uniform distribution with min = 0 and max = 1
	if (x < 0.) {
		return 0.;
	} else if (x > 1.) {
		return 1.;
	} else {      // 0 <= x <= 1
		return x; // (x-min)/(max-min) = x/1 = x
	}
}

bool TKolmogorovSmirnovTest::passKolmogorovSmirnovTest() {
	// go over all parameters in map
	size_t counter_paramFailed = 0;
	for (auto &param : _valueInCDF) {
		// run Kolmogorov-Smirnov-Test
		double maxDistance;
		coretools::Probability pValue;
		coretools::runOneSampleKolmogorovSmirnovTest(param.second, _cumulativeDistrFuncUniform01, maxDistance, pValue);
		if (pValue <= _cutoffSignificance_KSTest) { // reject null hypothesis that distributions are the same
			coretools::instances::logfile().warning(
			    "Parameter " + param.first +
			    " does not pass Kolmogorov-Smirnov-Test: rejected null hypothesis with p-value " + toString(pValue) +
			    " (d = " + toString(maxDistance) + ").");
			counter_paramFailed++;
		} else {
			coretools::instances::logfile().list(
			    "Parameter " + param.first +
			    " successfully passed Kolmogorov-Smirnov-Test: did not reject null hypothesis with p-value " +
			    toString(pValue) + " (d = " + toString(maxDistance) + ").");
		}
	}

	// decide if overall test passes or fails
	if (counter_paramFailed > 0) {
		return false;
	} else {
		return true;
	}
}

size_t TKolmogorovSmirnovTest::numParameters() { return _valueInCDF.size() + _numRedundantParameters; }

}; // end namespace stattools
