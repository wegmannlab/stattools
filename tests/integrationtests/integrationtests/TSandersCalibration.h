//
// Created by madleina on 04.03.21.
//

#ifndef EMPTY_TSANDERSCALIBRATION_H
#define EMPTY_TSANDERSCALIBRATION_H

#include <cassert>
#include <cmath>
#include <cstdint> // for uint16_t
#include <cstdlib> // for size_t
#include <vector>  // for vector

#include "coretools/Main/TRandomGenerator.h"
#include "coretools/algorithms.h"
#include "coretools/Main/TError.h"

namespace stattools {

class TSandersCalibration {
protected:
	// bins
	uint16_t _numBins;
	std::vector<uint16_t> _bins;

	// p-value
	size_t _numRep;

	// bins
	void _fillBins_forSandersCalibration(const std::vector<double> &PosteriorProbabilities);
	// p-value
	double _getPValueSandersCalibration(std::vector<double> &MeanPosteriorProbPerBin,
	                                    std::vector<size_t> &numSamplesPerBin,
	                                    const std::vector<double> &PosteriorProbabilities, double ObservedSC);
	double _getObservedSandersCalibration(std::vector<double> &MeanPosteriorProbPerBin,
	                                      std::vector<size_t> &NumSamplesPerBin,
	                                      const std::vector<double> &PosteriorProbabilities,
	                                      const std::vector<double> &SimulatedValues);
	// sander's calibration
	double _calculateSandersCalibration(const std::vector<double> &YBar,
	                                    const std::vector<double> &PosteriorProbabilities_PerBin,
	                                    const std::vector<size_t> &NumSamples_PerBin) const;

public:
	// constructors
	TSandersCalibration();
	virtual ~TSandersCalibration() = default;

	void setNumBins(uint16_t NumBins);
	void setNumRep(const size_t &NumRep);

	// run!
	double calculatePValueSandersCalibration(const std::vector<double> &PosteriorProbabilities,
	                                         const std::vector<double> &SimulatedValues, double &ObservedSC);
};

}; // end namespace stattools

#endif // EMPTY_TSANDERSCALIBRATION_H
