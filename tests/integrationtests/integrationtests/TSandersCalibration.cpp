//
// Created by madleina on 04.03.21.
//

#include "TSandersCalibration.h"

#include <algorithm> // for stable_sort, max
#include <cmath>     // for floor
#include <stdexcept> // for runtime_error
#include <string>    // for char_traits, operator+

#include "coretools/Main/TRandomGenerator.h" // for TRandomGenerator
#include "coretools/algorithms.h"            // for rankSort

namespace stattools {

TSandersCalibration::TSandersCalibration() {
	// default values
	_numBins = 100;
	_numRep  = 10000;
}

void TSandersCalibration::setNumBins(uint16_t NumBins) { _numBins = NumBins; }

void TSandersCalibration::setNumRep(const size_t &NumRep) { _numRep = NumRep; }

double TSandersCalibration::calculatePValueSandersCalibration(const std::vector<double> &PosteriorProbabilities,
                                                              const std::vector<double> &SimulatedValues,
                                                              double &ObservedSC) {
	// run Sander's calibration (SC) to check if discrete values are well calibrated
	// calculate p-value where we compare observed SC to expected distribution --> H0: come from same distribution (i.e.
	// observed is well-calibrated) returns true if test is not significant (not reject H0) and false if test is
	// significant (reject H0)

	// fill bins
	if (_numBins >= PosteriorProbabilities.size()) {
		DEVERROR("More bins (", _numBins, ") than posterior probabilities (", PosteriorProbabilities.size(),
		         ") in Sander's Calibration test!");
	}
	_fillBins_forSandersCalibration(PosteriorProbabilities);

	// calculate observed Sander's calibration
	std::vector<double> meanPosteriorProbPerBin;
	std::vector<size_t> numSamplesPerBin;
	ObservedSC = _getObservedSandersCalibration(meanPosteriorProbPerBin, numSamplesPerBin, PosteriorProbabilities,
	                                            SimulatedValues);

	// calculate and return p-value
	return _getPValueSandersCalibration(meanPosteriorProbPerBin, numSamplesPerBin, PosteriorProbabilities, ObservedSC);
}

double TSandersCalibration::_calculateSandersCalibration(const std::vector<double> &YBar,
                                                         const std::vector<double> &PosteriorProbabilities_PerBin,
                                                         const std::vector<size_t> &NumSamples_PerBin) const {
	// calculate Sanders calibration
	double sum  = 0.;
	double sumN = 0.;
	for (uint16_t bin = 0; bin < _numBins; bin++) {
		double tmp = YBar[bin] - PosteriorProbabilities_PerBin[bin];
		sum += NumSamples_PerBin[bin] * tmp * tmp;
		sumN += NumSamples_PerBin[bin];
	}
	return sum / sumN;
}

void TSandersCalibration::_fillBins_forSandersCalibration(const std::vector<double> &PosteriorProbabilities) {
	// group posterior probabilities into bins
	// fills vector "_bins": for each element in PosteriorProbabilities, to which bin does it belong to?

	// first sort pi. Get a vector where each entry represents the index in pi
	// i.e. sorted[0] = 3 means that the entry with index 3 of pi has the smallest value
	std::vector<size_t> sorted;
	coretools::rankSort(PosteriorProbabilities, sorted);

	// define number of samples per bin
	size_t N                   = PosteriorProbabilities.size();
	size_t minNumSamplesPerBin = std::floor(static_cast<double>(N) / static_cast<double>(_numBins));
	std::vector<size_t> numSamplesPerBin(_numBins, minNumSamplesPerBin);
	size_t extraSamples = N % _numBins;
	for (size_t i = 0; i < extraSamples; i++) { numSamplesPerBin[i]++; }

	// assign bin to each element of PosteriorProbabilities
	_bins.resize(N, 0);
	uint16_t curBin = 0;
	size_t counter  = 0;

	for (size_t i = 0; i < N; i++) {
		size_t position = sorted[i];
		_bins[position] = curBin;
		counter++;

		if (counter >= numSamplesPerBin[curBin]) {
			curBin++;
			counter = 0;
		}
	}
}

double TSandersCalibration::_getObservedSandersCalibration(std::vector<double> &MeanPosteriorProbPerBin,
                                                           std::vector<size_t> &NumSamplesPerBin,
                                                           const std::vector<double> &PosteriorProbabilities,
                                                           const std::vector<double> &SimulatedValues) {
	// will fill the vectors "MeanPosteriorProbPerBin" and "numSamplesPerBin" with the mean posterior probability per
	// bin and the number of samples per bin (only want to calculate this once, will be the same for all p-value
	// replicates as well) then, it calculates the sander's calibration for these observed data
	MeanPosteriorProbPerBin.resize(_numBins, 0.);
	NumSamplesPerBin.resize(_numBins, 0.);
	std::vector<double> meanSimulatedValuePerBin(_numBins);

	// calculate average of posterior probabilities and simulated values per bin
	for (size_t i = 0; i < PosteriorProbabilities.size(); i++) {
		uint16_t bin = _bins[i];
		MeanPosteriorProbPerBin[bin] += PosteriorProbabilities[i];
		meanSimulatedValuePerBin[bin] += SimulatedValues[i];
		NumSamplesPerBin[bin]++;
	}
	for (uint16_t bin = 0; bin < _numBins; bin++) {
		MeanPosteriorProbPerBin[bin] /= NumSamplesPerBin[bin];
		meanSimulatedValuePerBin[bin] /= NumSamplesPerBin[bin];
	}

	// calculate sander's calibration
	return _calculateSandersCalibration(meanSimulatedValuePerBin, MeanPosteriorProbPerBin, NumSamplesPerBin);
}

double TSandersCalibration::_getPValueSandersCalibration(std::vector<double> &MeanPosteriorProbPerBin,
                                                         std::vector<size_t> &numSamplesPerBin,
                                                         const std::vector<double> &PosteriorProbabilities,
                                                         double ObservedSC) {
	size_t counter = 0;
	for (size_t rep = 0; rep < _numRep; rep++) {
		// sample y from pi
		std::vector<size_t> y(PosteriorProbabilities.size());
		for (size_t i = 0; i < PosteriorProbabilities.size(); i++) {
			y[i] = coretools::instances::randomGenerator().getBinomialRand(
			    coretools::Probability(PosteriorProbabilities[i]), 1); // SC only works for binary data -> pick 0 or 1
		}

		// calculate average of y
		std::vector<double> meanYPerBin(_numBins);
		for (size_t i = 0; i < PosteriorProbabilities.size(); i++) {
			uint16_t bin = _bins[i];
			meanYPerBin[bin] += y[i];
		}
		for (uint16_t bin = 0; bin < _numBins; bin++) { meanYPerBin[bin] /= numSamplesPerBin[bin]; }

		// calculate sander's calibration
		double SC = _calculateSandersCalibration(meanYPerBin, MeanPosteriorProbPerBin, numSamplesPerBin);

		if (SC >= ObservedSC) { counter++; }
	}
	return static_cast<double>(counter) /
	       static_cast<double>(_numRep); // this is the p-value: how many times was observed smaller than expected?
}

}; // end namespace stattools
