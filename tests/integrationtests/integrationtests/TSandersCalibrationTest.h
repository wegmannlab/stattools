//
// Created by madleina on 05.03.21.
//

#ifndef EMPTY_TSANDERSCALIBRATIONTEST_H
#define EMPTY_TSANDERSCALIBRATIONTEST_H

#include <map>      // for map
#include <stddef.h> // for size_t
#include <stdint.h> // for uint16_t
#include <string>   // for string
#include <utility>  // for pair
#include <vector>   // for vector

#include "TSandersCalibration.h"
#include "coretools/Files/TInputFile.h"
#include "coretools/Main/TLog.h"

namespace stattools {

class TSandersCalibrationTest {
	// Sander's Calibration test for binary outcomes

protected:
	// store values for Sander's Calibration test in map
	double _cutoffSignificance_SCTest;
	uint16_t _numBins;
	size_t _numReps;

	// relevant columns in trace- and simulated file
	std::vector<size_t> _relevantCols;

	// maps
	std::map<std::string, std::vector<double>> _simulatedValues;
	std::map<std::string, std::vector<std::vector<double>>> _posteriorProbabilities;
	bool _numStatesInitialized;
	size_t _numStates;

	// vector indicating model switch
	bool _modelsAreSwitched;
	std::vector<size_t> _switchedModels;

	// vector storing SC and p-value after each replicate
	std::map<std::string, std::vector<double>> _sc_afterEachRep;
	std::map<std::string, std::vector<double>> _pValue_afterEachRep;

	void
	_calculateMeanPosteriorProbability_PerModel(const size_t &ModelIndex, const std::vector<double> &SimulatedValues,
	                                            const std::vector<std::vector<double>> &PosteriorProbabilitiesPerModel,
	                                            std::vector<double> &MeanPerModel);
	void _assignSwitchedModels(const std::vector<double> &SimulatedValues,
	                           const std::vector<std::vector<double>> &PosteriorProbabilitiesPerModel);
	void _reAssignSimulatedValues(std::vector<double> &SimulatedValues);
	void _parseTraceFile(coretools::TInputFile &TraceFile, std::vector<std::vector<double>> &SumOverIterationsPerParam);
	void _saveSimulatedValues(std::vector<double> &SimulatedValues, coretools::TConstView<std::string> Header,
	                          const size_t &Replicate);
	void _prepareVectorsForSandersTest(const size_t &Model,
	                                   const std::vector<std::vector<double>> &PosteriorProbabilitiesAllModels,
	                                   const std::vector<double> &SimulatedValuesAllModels,
	                                   std::vector<double> &PosteriorProbabilityThisModel,
	                                   std::vector<double> &SimulatedThisModel, const size_t &CurRep);
	void _writePosteriorProbsToFile(const std::pair<std::string, std::vector<std::vector<double>>> &PosteriorProbsParam,
	                                const std::pair<std::string, std::vector<double>> &SimulatedValues) const;

	void _addToPValMap(const std::string &Name, const size_t &Rep, double PValue);
	void _addToSCMap(const std::string &Name, const size_t &Rep, double ObservedSC);

public:
	TSandersCalibrationTest();

	// setters
	void setNumStates(const size_t &NumStates);
	void setCutoffSignificance(double Cutoff);
	void setNumBins(uint16_t NumBins);
	void prepareMaps(const std::string &Name, const size_t &NumReps);
	void fillRelevantColumns(coretools::TConstView<std::string> Header);

	// getters
	size_t numParameters() const;
	uint16_t numBins() const;
	bool modelsSwitched() const;
	const std::vector<size_t> &getSwitchedModelOrder() const;
	const std::map<std::string, std::vector<double>> &getPValuesAllReps() const;
	const std::map<std::string, std::vector<double>> &getObservedSCsAllReps() const;

	// evaluate files
	void evaluateTraceFile(std::vector<double> &SimulatedValues, coretools::TInputFile &TraceFile,
	                       const size_t &Replicate);

	// run test!
	bool passSandersCalibrationTest(bool WriteFile, const size_t &Rep);
};

}; // end namespace stattools

#endif // EMPTY_TSANDERSCALIBRATIONTEST_H
