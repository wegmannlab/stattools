//
// Created by madleina on 16.11.20.
//

#include <string> // for string, allocator, basic_string

#include "coretools/IntegrationTests/TTest.h" // for TTest
#include "coretools/Main/TMain.h"             // for TMain
#include "integrationtests/TMCMCTests.h"
#include "integrationtests/TRunMCMC.h" // for TMCMCFrameworkTest_Bernoulli

using namespace stattools;

void addTasks(coretools::TMain &) {
	// no task
}

void addTests(coretools::TMain &main) {
	main.addTest("empty", new coretools::TTest());
	main.addTest("normal", new TMCMCTest<TTest_Normal>());
	main.addTest("exponential", new TMCMCTest<TTest_Exponential>());
	main.addTest("poisson", new TMCMCTest<TTest_Poisson>());
	main.addTest("gamma", new TMCMCTest<TTest_Gamma>());
	main.addTest("Beta", new TMCMCTest<TTest_Beta>());
	main.addTest("Beta_updateInLog", new TMCMCTest<TTest_Beta_UpdateLog>());
	main.addTest("symmetricBeta", new TMCMCTest<TTest_SymmetricBeta>());
	main.addTest("symmetricBeta_updateInLog", new TMCMCTest<TTest_SymmetricBeta_UpdateLog>());
	main.addTest("binomial", new TMCMCTest<TTest_Binomial>());
	main.addTest("negativeBinomial", new TMCMCTest<TTest_NegativeBinomial>());
	main.addTest("bernoulli", new TMCMCTest<TTest_Bernoulli>());
	main.addTest("gibbsBetaBernoulli", new TMCMCTest<TTest_GibbsBetaBernoulli>());
	main.addTest("categorical", new TMCMCTest<TTest_Categorical>());
	main.addTest("dirichletVar", new TMCMCTest<TTest_DirichletVar>());
	main.addTest("dirichletMeanVar", new TMCMCTest<TTest_DirichletMeanVar>());
	main.addTest("hmm_bool", new TMCMCTest<TTest_HMMBool>());
	main.addTest("hmm_bool_generating_matrix", new TMCMCTest<TTest_HMMBoolGeneratingMatrix>());
	main.addTest("hmm_categorical", new TMCMCTest<TTest_HMMCategorical>());
	main.addTest("hmm_ladder", new TMCMCTest<TTest_HMMLadder>());
	main.addTest("linearModelWithIntercept", new TMCMCTest<TTest_LinearModel_Intercept>());
	main.addTest("hmm_scaledLadderCombined", new TMCMCTest<TTest_HMMScaledLadderCombined>());
	main.addTest("multivariateNormal", new TMCMCTest<TTest_MultivariateNormal>());
	// main.addTest("normalMixedModel", new TMCMCTest<TTest_NormalMixedModel>());
	// main.addTest("normalMixedModel_HMM", new TMCMCTest<TTest_NormalMixedModel_HMM>());
	main.addTest("normalMixedModelStretch", new TMCMCTest<TTest_NormalMixedModel_Stretch>());
	main.addTest("normalMixedModelStretch_HMM", new TMCMCTest<TTest_NormalMixedModel_Stretch_HMM>());
	// main.addTest("multivariateNormalMixedModel", new TMCMCTest<TTest_MultivariateNormalMixedModel>());
	main.addTest("multivariateNormalMixedModelStretch", new TMCMCTest<TTest_MultivariateNormalMixedModel_Stretch>());
	main.addTest("multivariateNormalMixedModelStretch_HMM",
	             new TMCMCTest<TTest_MultivariateNormalMixedModel_Stretch_HMM>());

	main.addTestSuite("all", {"normal", "exponential", "poisson", "gamma", "Beta", "Beta_updateInLog", "symmetricBeta",
	                          "symmetricBeta_updateInLog", "binomial", "negativeBinomial", "bernoulli",
	                          "gibbsBetaBernoulli", "categorical", "dirichletVar", "dirichletMeanVar", "hmm_bool",
	                          "hmm_bool_generating_matrix", "hmm_categorical", "hmm_ladder", "multivariateNormal",
	                          //"normalMixedModel",
	                          //"normalMixedModel_HMM",
	                          "normalMixedModelStretch", "normalMixedModelStretch_HMM",
	                          //"multivariateNormalMixedModel",
	                          "multivariateNormalMixedModelStretch", "multivariateNormalMixedModelStretch_HMM",
	                          "linearModelWithIntercept"});
}

int main(int argc, char **argv) {
	// runs integration test
	coretools::TMain main("stattools", "1.0", "University of Fribourg", "https://bitbucket.org/wegmannlab/stattools",
	                      "madleina.caduff@unifr.ch");

	addTasks(main);
	addTests(main);

	// now run program
	return main.run(argc, argv);
}
