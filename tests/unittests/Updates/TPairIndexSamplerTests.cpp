//
// Created by caduffm on 10/20/22.
//

#include "stattools/Updates/TPairIndexSampler.h"
#include "gtest/gtest.h"

using namespace stattools;

//---------------------------------------
// TPairIndexSampler
//---------------------------------------

class TPairIndexSamplerBridge : public TPairIndexSampler {
public:
	TPairIndexSamplerBridge(size_t Size) : TPairIndexSampler(Size){};
	void sampleIndices(size_t p) { TPairIndexSampler::_sampleIndices(p); }
};

TEST(TPairIndexSamplerTest, sample_even) {
	TPairIndexSamplerBridge samples(10);

	// p = 0
	samples.sampleIndices(0);

	std::vector<size_t> expected_1 = {0, 9, 8, 7, 6};
	std::vector<size_t> expected_2 = {1, 2, 3, 4, 5};
	for (size_t i = 0; i < samples.length(); i++) {
		EXPECT_EQ(samples.getIndexPair(i).first, expected_1[i]);
		EXPECT_EQ(samples.getIndexPair(i).second, expected_2[i]);
	}

	// p = 1
	samples.sampleIndices(1);

	expected_1 = {1, 0, 9, 8, 7};
	expected_2 = {2, 3, 4, 5, 6};
	for (size_t i = 0; i < samples.length(); i++) {
		EXPECT_EQ(samples.getIndexPair(i).first, expected_1[i]);
		EXPECT_EQ(samples.getIndexPair(i).second, expected_2[i]);
	}

	// p = 5
	samples.sampleIndices(5);

	expected_1 = {5, 4, 3, 2, 1};
	expected_2 = {6, 7, 8, 9, 0};
	for (size_t i = 0; i < samples.length(); i++) {
		EXPECT_EQ(samples.getIndexPair(i).first, expected_1[i]);
		EXPECT_EQ(samples.getIndexPair(i).second, expected_2[i]);
	}

	// p = 7
	samples.sampleIndices(7);

	expected_1 = {7, 6, 5, 4, 3};
	expected_2 = {8, 9, 0, 1, 2};
	for (size_t i = 0; i < samples.length(); i++) {
		EXPECT_EQ(samples.getIndexPair(i).first, expected_1[i]);
		EXPECT_EQ(samples.getIndexPair(i).second, expected_2[i]);
	}

	// p = 9
	samples.sampleIndices(9);

	expected_1 = {9, 8, 7, 6, 5};
	expected_2 = {0, 1, 2, 3, 4};
	for (size_t i = 0; i < samples.length(); i++) {
		EXPECT_EQ(samples.getIndexPair(i).first, expected_1[i]);
		EXPECT_EQ(samples.getIndexPair(i).second, expected_2[i]);
	}
}

TEST(TPairIndexSamplerTest, sample_odd) {
	TPairIndexSamplerBridge samples(11);

	// p = 0
	samples.sampleIndices(0);

	std::vector<size_t> expected_1 = {0, 10, 9, 8, 7};
	std::vector<size_t> expected_2 = {1, 2, 3, 4, 5};
	for (size_t i = 0; i < samples.length(); i++) {
		EXPECT_EQ(samples.getIndexPair(i).first, expected_1[i]);
		EXPECT_EQ(samples.getIndexPair(i).second, expected_2[i]);
	}

	// p = 1
	samples.sampleIndices(1);

	expected_1 = {1, 0, 10, 9, 8};
	expected_2 = {2, 3, 4, 5, 6};
	for (size_t i = 0; i < samples.length(); i++) {
		EXPECT_EQ(samples.getIndexPair(i).first, expected_1[i]);
		EXPECT_EQ(samples.getIndexPair(i).second, expected_2[i]);
	}

	// p = 5
	samples.sampleIndices(5);

	expected_1 = {5, 4, 3, 2, 1};
	expected_2 = {6, 7, 8, 9, 10};
	for (size_t i = 0; i < samples.length(); i++) {
		EXPECT_EQ(samples.getIndexPair(i).first, expected_1[i]);
		EXPECT_EQ(samples.getIndexPair(i).second, expected_2[i]);
	}

	// p = 7
	samples.sampleIndices(7);

	expected_1 = {7, 6, 5, 4, 3};
	expected_2 = {8, 9, 10, 0, 1};
	for (size_t i = 0; i < samples.length(); i++) {
		EXPECT_EQ(samples.getIndexPair(i).first, expected_1[i]);
		EXPECT_EQ(samples.getIndexPair(i).second, expected_2[i]);
	}

	// p = 9
	samples.sampleIndices(9);

	expected_1 = {9, 8, 7, 6, 5};
	expected_2 = {10, 0, 1, 2, 3};
	for (size_t i = 0; i < samples.length(); i++) {
		EXPECT_EQ(samples.getIndexPair(i).first, expected_1[i]);
		EXPECT_EQ(samples.getIndexPair(i).second, expected_2[i]);
	}

	// p = 10
	samples.sampleIndices(10);

	expected_1 = {10, 9, 8, 7, 6};
	expected_2 = {0, 1, 2, 3, 4};
	for (size_t i = 0; i < samples.length(); i++) {
		EXPECT_EQ(samples.getIndexPair(i).first, expected_1[i]);
		EXPECT_EQ(samples.getIndexPair(i).second, expected_2[i]);
	}
}

TEST(TPairIndexSamplerTest, many_even) {
	TPairIndexSampler samples(100);

	for (size_t rep = 0; rep < 1000; rep++) {
		samples.sampleIndices();

		std::set<size_t> unique;
		for (size_t i = 0; i < samples.length(); i++) {
			const auto [ix1, ix2] = samples.getIndexPair(i);
			EXPECT_NE(ix1, ix2);
			unique.emplace(ix1);
			unique.emplace(ix2);
		}

		EXPECT_EQ(unique.size(), 100);
	}
}

TEST(TPairIndexSamplerTest, many_odd) {
	TPairIndexSampler samples(101);

	for (size_t rep = 0; rep < 1000; rep++) {
		samples.sampleIndices();

		std::set<size_t> unique;
		for (size_t i = 0; i < samples.length(); i++) {
			const auto [ix1, ix2] = samples.getIndexPair(i);
			EXPECT_NE(ix1, ix2);
			unique.emplace(ix1);
			unique.emplace(ix2);
		}

		EXPECT_EQ(unique.size(), 100);
	}
}

//---------------------------------------
// TPairIndexSamplerMultiDim
//---------------------------------------

TEST(TPairIndexSamplerMultiDim, dim_1) {
	const size_t numDim   = 1;
	const size_t alongDim = 0;
	const coretools::TDimension<numDim> dim({101});
	TPairIndexSamplerMultiDim<numDim, alongDim> sampler(dim);
	EXPECT_EQ(sampler.length(), 50);

	for (size_t rep = 0; rep < 1000; rep++) {
		sampler.sampleIndices();

		std::set<size_t> unique;
		for (size_t i = 0; i < sampler.length(); i++) {
			const auto [ix1, ix2] = sampler.getIndexPair(i);
			EXPECT_NE(ix1, ix2);
			unique.emplace(ix1);
			unique.emplace(ix2);
		}
		EXPECT_EQ(unique.size(), 100);
	}
}

TEST(TPairIndexSamplerMultiDim, dim_2_alongDim_0) {
	const size_t numDim   = 2;
	const size_t alongDim = 0;
	const coretools::TDimension<numDim> dim({101, 3});
	TPairIndexSamplerMultiDim<numDim, alongDim> sampler(dim);
	EXPECT_EQ(sampler.length(), 50 * 3);

	for (size_t rep = 0; rep < 1000; rep++) {
		sampler.sampleIndices();

		std::set<size_t> unique;
		for (size_t i = 0; i < sampler.length(); i++) {
			const auto [ix1, ix2] = sampler.getIndexPair(i);
			EXPECT_NE(ix1, ix2);
			unique.emplace(ix1);
			unique.emplace(ix2);
		}
		EXPECT_EQ(unique.size(), 300);
	}
}

TEST(TPairIndexSamplerMultiDim, dim_2_alongDim_1) {
	const size_t numDim   = 2;
	const size_t alongDim = 1;
	const coretools::TDimension<numDim> dim({101, 3});
	TPairIndexSamplerMultiDim<numDim, alongDim> sampler(dim);
	EXPECT_EQ(sampler.length(), 101);

	for (size_t rep = 0; rep < 1000; rep++) {
		sampler.sampleIndices();

		std::set<size_t> unique;
		for (size_t i = 0; i < sampler.length(); i++) {
			const auto [ix1, ix2] = sampler.getIndexPair(i);
			EXPECT_NE(ix1, ix2);
			unique.emplace(ix1);
			unique.emplace(ix2);
		}
		EXPECT_EQ(unique.size(), 202);
	}
}

TEST(TPairIndexSamplerMultiDim, dim_3_alongDim_1) {
	const size_t numDim   = 3;
	const size_t alongDim = 1;
	const coretools::TDimension<numDim> dim({101, 5, 3});
	TPairIndexSamplerMultiDim<numDim, alongDim> sampler(dim);
	EXPECT_EQ(sampler.length(), 101 * 2 * 3);

	for (size_t rep = 0; rep < 1000; rep++) {
		sampler.sampleIndices();

		std::set<size_t> unique;
		for (size_t i = 0; i < sampler.length(); i++) {
			const auto [ix1, ix2] = sampler.getIndexPair(i);
			EXPECT_NE(ix1, ix2);
			unique.emplace(ix1);
			unique.emplace(ix2);
		}
		EXPECT_EQ(unique.size(), 101 * 4 * 3);
	}
}

//---------------------------------------
// TPairIndexSamplerPerType
//---------------------------------------

template<size_t NumTypes, typename ParameterType>
class TPairIndexSamplerPerTypeBridge : public TPairIndexSamplerPerType<NumTypes, ParameterType> {
public:
	TPairIndexSamplerPerTypeBridge(size_t Size) : TPairIndexSamplerPerType<NumTypes, ParameterType>(Size){};
	void sampleIndices(size_t p, ParameterType Parameter) {
		TPairIndexSamplerPerType<NumTypes, ParameterType>::_sampleIndices(p, Parameter);
	}
};

TEST(TPairIndexSamplerPerTypeTest, sample_even) {
	std::vector<size_t> container = {1, 0, 2, 2, 1, 1, 2, 0, 1, 2, 1, 1};
	TPairIndexSamplerPerTypeBridge<3, std::vector<size_t>> samples(container.size());

	// p = 0
	samples.sampleIndices(0, container);

	std::vector<size_t> expected_1 = {7, 0, 11, 10, 9, 2};
	std::vector<size_t> expected_2 = {1, 8, 4, 5, 3, 6};
	for (size_t i = 0; i < samples.length(); i++) {
		const auto [first, second] = samples.getIndexPair(i);
		EXPECT_EQ(first, expected_1[i]);
		EXPECT_EQ(second, expected_2[i]);
	}

	// p = 1
	samples.sampleIndices(1, container);

	expected_1 = {1, 0, 11, 10, 9, 2};
	expected_2 = {7, 8, 4, 5, 3, 6};
	for (size_t i = 0; i < samples.length(); i++) {
		const auto [first, second] = samples.getIndexPair(i);
		EXPECT_EQ(first, expected_1[i]);
		EXPECT_EQ(second, expected_2[i]);
	}

	// p = 5
	samples.sampleIndices(5, container);

	expected_1 = {1, 5, 4, 0, 3, 2};
	expected_2 = {7, 8, 10, 11, 6, 9};
	for (size_t i = 0; i < samples.length(); i++) {
		const auto [first, second] = samples.getIndexPair(i);
		EXPECT_EQ(first, expected_1[i]);
		EXPECT_EQ(second, expected_2[i]);
	}

	// p = 7
	samples.sampleIndices(7, container);

	expected_1 = {7, 5, 4, 8, 6, 3};
	expected_2 = {1, 10, 11, 0, 2, 9};
	for (size_t i = 0; i < samples.length(); i++) {
		const auto [first, second] = samples.getIndexPair(i);
		EXPECT_EQ(first, expected_1[i]);
		EXPECT_EQ(second, expected_2[i]);
	}

	// p = 11
	samples.sampleIndices(11, container);

	expected_1 = {7, 11, 10, 8, 9, 6};
	expected_2 = {1, 0, 4, 5, 2, 3};
	for (size_t i = 0; i < samples.length(); i++) {
		const auto [first, second] = samples.getIndexPair(i);
		EXPECT_EQ(first, expected_1[i]);
		EXPECT_EQ(second, expected_2[i]);
	}
}

TEST(TPairIndexSamplerPerTypeTest, sample_odd) {
	std::vector<size_t> container = {2, 0, 1, 0, 0, 1, 2, 1, 1, 2, 0};
	TPairIndexSamplerPerTypeBridge<3, std::vector<size_t>> samples(container.size());

	// p = 0
	samples.sampleIndices(0, container);

	std::vector<size_t> expected_1 = {10, 1, 8, 7, 0};
	std::vector<size_t> expected_2 = {3, 4, 2, 5, 9};
	for (size_t i = 0; i < samples.length(); i++) {
		const auto [first, second] = samples.getIndexPair(i);
		EXPECT_EQ(first, expected_1[i]);
		EXPECT_EQ(second, expected_2[i]);
	}

	// p = 1
	samples.sampleIndices(1, container);

	expected_1 = {1, 10, 8, 0};
	expected_2 = {3, 4, 2, 9};
	for (size_t i = 0; i < samples.length(); i++) {
		const auto [first, second] = samples.getIndexPair(i);
		EXPECT_EQ(first, expected_1[i]);
		EXPECT_EQ(second, expected_2[i]);
	}

	// p = 5
	samples.sampleIndices(5, container);

	expected_1 = {4, 3, 5, 2, 6};
	expected_2 = {1, 10, 7, 8, 9};
	for (size_t i = 0; i < samples.length(); i++) {
		const auto [first, second] = samples.getIndexPair(i);
		EXPECT_EQ(first, expected_1[i]);
		EXPECT_EQ(second, expected_2[i]);
	}

	// p = 7
	samples.sampleIndices(7, container);

	expected_1 = {4, 3, 7, 6};
	expected_2 = {10, 1, 5, 9};
	for (size_t i = 0; i < samples.length(); i++) {
		const auto [first, second] = samples.getIndexPair(i);
		EXPECT_EQ(first, expected_1[i]);
		EXPECT_EQ(second, expected_2[i]);
	}

	// p = 9
	samples.sampleIndices(9, container);

	expected_1 = {10, 8, 7, 9};
	expected_2 = {1, 5, 2, 6};
	for (size_t i = 0; i < samples.length(); i++) {
		const auto [first, second] = samples.getIndexPair(i);
		EXPECT_EQ(first, expected_1[i]);
		EXPECT_EQ(second, expected_2[i]);
	}

	// p = 10
	samples.sampleIndices(10, container);

	expected_1 = {10, 1, 8, 9};
	expected_2 = {3, 4, 7, 6};
	for (size_t i = 0; i < samples.length(); i++) {
		const auto [first, second] = samples.getIndexPair(i);
		EXPECT_EQ(first, expected_1[i]);
		EXPECT_EQ(second, expected_2[i]);
	}
}

TEST(TPairIndexSamplerPerTypeTest, sample_type_odd_len_even) {
	std::vector<size_t> container = {2, 0, 1, 0, 1, 1, 0, 2, 2, 1};
	TPairIndexSamplerPerTypeBridge<3, std::vector<size_t>> samples(container.size());

	// p = 0
	samples.sampleIndices(0, container);

	std::vector<size_t> expected_1 = {6, 9, 2, 0};
	std::vector<size_t> expected_2 = {1, 4, 5, 8};
	for (size_t i = 0; i < samples.length(); i++) {
		const auto [first, second] = samples.getIndexPair(i);
		EXPECT_EQ(first, expected_1[i]);
		EXPECT_EQ(second, expected_2[i]);
	}

	// p = 1
	samples.sampleIndices(1, container);

	expected_1 = {1, 9, 2, 0};
	expected_2 = {3, 4, 5, 8};
	for (size_t i = 0; i < samples.length(); i++) {
		const auto [first, second] = samples.getIndexPair(i);
		EXPECT_EQ(first, expected_1[i]);
		EXPECT_EQ(second, expected_2[i]);
	}

	// p = 4
	samples.sampleIndices(4, container);

	expected_1 = {3, 4, 2, 0};
	expected_2 = {1, 5, 9, 7};
	for (size_t i = 0; i < samples.length(); i++) {
		const auto [first, second] = samples.getIndexPair(i);
		EXPECT_EQ(first, expected_1[i]);
		EXPECT_EQ(second, expected_2[i]);
	}

	// p = 9
	samples.sampleIndices(9, container);

	expected_1 = {6, 9, 5, 8};
	expected_2 = {1, 2, 4, 7};
	for (size_t i = 0; i < samples.length(); i++) {
		const auto [first, second] = samples.getIndexPair(i);
		EXPECT_EQ(first, expected_1[i]);
		EXPECT_EQ(second, expected_2[i]);
	}
}

TEST(TPairIndexSamplerPerTypeTest, sample_no_zeros) {
	std::vector<size_t> container = {1, 2, 2, 1, 2, 1};
	TPairIndexSamplerPerTypeBridge<3, std::vector<size_t>> samples(container.size());

	// p = 0
	samples.sampleIndices(0, container);

	std::vector<size_t> expected_1 = {0, 4};
	std::vector<size_t> expected_2 = {5, 1};
	for (size_t i = 0; i < samples.length(); i++) {
		const auto [first, second] = samples.getIndexPair(i);
		EXPECT_EQ(first, expected_1[i]);
		EXPECT_EQ(second, expected_2[i]);
	}

	// p = 1
	samples.sampleIndices(1, container);

	expected_1 = {0, 1};
	expected_2 = {5, 2};
	for (size_t i = 0; i < samples.length(); i++) {
		const auto [first, second] = samples.getIndexPair(i);
		EXPECT_EQ(first, expected_1[i]);
		EXPECT_EQ(second, expected_2[i]);
	}

	// p = 5
	samples.sampleIndices(5, container);

	expected_1 = {5, 4};
	expected_2 = {3, 1};
	for (size_t i = 0; i < samples.length(); i++) {
		const auto [first, second] = samples.getIndexPair(i);
		EXPECT_EQ(first, expected_1[i]);
		EXPECT_EQ(second, expected_2[i]);
	}
}

TEST(TPairIndexSamplerPerTypeTest, sample_no_ones) {
	std::vector<size_t> container = {0, 2, 2, 0, 0};
	TPairIndexSamplerPerTypeBridge<3, std::vector<size_t>> samples(container.size());

	// p = 0
	samples.sampleIndices(0, container);

	std::vector<size_t> expected_1 = {0, 1};
	std::vector<size_t> expected_2 = {4, 2};
	for (size_t i = 0; i < samples.length(); i++) {
		const auto [first, second] = samples.getIndexPair(i);
		EXPECT_EQ(first, expected_1[i]);
		EXPECT_EQ(second, expected_2[i]);
	}

	// p = 1
	samples.sampleIndices(1, container);

	expected_1 = {0, 1};
	expected_2 = {3, 2};
	for (size_t i = 0; i < samples.length(); i++) {
		const auto [first, second] = samples.getIndexPair(i);
		EXPECT_EQ(first, expected_1[i]);
		EXPECT_EQ(second, expected_2[i]);
	}

	// p = 3
	samples.sampleIndices(3, container);

	expected_1 = {3};
	expected_2 = {4};
	for (size_t i = 0; i < samples.length(); i++) {
		const auto [first, second] = samples.getIndexPair(i);
		EXPECT_EQ(first, expected_1[i]);
		EXPECT_EQ(second, expected_2[i]);
	}

	// p = 4
	samples.sampleIndices(4, container);

	expected_1 = {4};
	expected_2 = {3};
	for (size_t i = 0; i < samples.length(); i++) {
		const auto [first, second] = samples.getIndexPair(i);
		EXPECT_EQ(first, expected_1[i]);
		EXPECT_EQ(second, expected_2[i]);
	}
}

TEST(TPairIndexSamplerPerTypeTest, sample_no_twos) {
	std::vector<size_t> container = {1, 0, 1, 1, 0, 1};
	TPairIndexSamplerPerTypeBridge<3, std::vector<size_t>> samples(container.size());

	// p = 0
	samples.sampleIndices(0, container);

	std::vector<size_t> expected_1 = {4, 0, 5};
	std::vector<size_t> expected_2 = {1, 2, 3};
	for (size_t i = 0; i < samples.length(); i++) {
		const auto [first, second] = samples.getIndexPair(i);
		EXPECT_EQ(first, expected_1[i]);
		EXPECT_EQ(second, expected_2[i]);
	}

	// p = 1
	samples.sampleIndices(1, container);

	expected_1 = {1, 0, 5};
	expected_2 = {4, 2, 3};
	for (size_t i = 0; i < samples.length(); i++) {
		const auto [first, second] = samples.getIndexPair(i);
		EXPECT_EQ(first, expected_1[i]);
		EXPECT_EQ(second, expected_2[i]);
	}

	// p = 5
	samples.sampleIndices(5, container);

	expected_1 = {4, 5, 3};
	expected_2 = {1, 0, 2};
	for (size_t i = 0; i < samples.length(); i++) {
		const auto [first, second] = samples.getIndexPair(i);
		EXPECT_EQ(first, expected_1[i]);
		EXPECT_EQ(second, expected_2[i]);
	}
}

TEST(TPairIndexSamplerPerTypeTest, many_even) {
	std::vector<size_t> container = {3, 3, 2, 0, 4, 4, 2, 0, 2, 0, 4, 0, 4, 2, 0, 2, 3, 1, 2, 4, 2, 2, 4, 3, 4,
									 3, 0, 0, 2, 2, 0, 3, 0, 1, 1, 4, 1, 1, 3, 3, 1, 1, 4, 0, 0, 3, 4, 2, 3, 4,
									 2, 4, 2, 3, 0, 0, 2, 0, 4, 1, 2, 1, 1, 4, 2, 1, 1, 4, 4, 3, 3, 0, 0, 2, 0,
									 1, 2, 2, 3, 0, 3, 2, 2, 0, 4, 3, 4, 2, 4, 3, 3, 4, 2, 1, 2, 3, 3, 3, 0, 4};

	TPairIndexSamplerPerType<5, std::vector<size_t>> samples(container.size());

	for (size_t rep = 0; rep < 1000; rep++) {
		samples.sampleIndices(container);

		std::set<size_t> unique;
		for (size_t i = 0; i < samples.length(); i++) {
			const auto [first, second] = samples.getIndexPair(i);
			unique.emplace(first);
			unique.emplace(second);
			EXPECT_TRUE(container[first] == container[second]);
		}

		EXPECT_EQ(unique.size(), 98); // uneven number of 3's and 4's
	}
}

TEST(TPairIndexSamplerPerTypeTest, many_odd) {
	std::vector<size_t> container = {3, 3, 2, 0, 4, 4, 2, 0, 2, 0, 4, 0, 4, 2, 0, 2, 3, 1, 2, 4, 2, 2, 4, 3, 4, 3,
									 0, 0, 2, 2, 0, 3, 0, 1, 1, 4, 1, 1, 3, 3, 1, 1, 4, 0, 0, 3, 4, 2, 3, 4, 2, 4,
									 2, 3, 0, 0, 2, 0, 4, 1, 2, 1, 1, 4, 2, 1, 1, 4, 4, 3, 3, 0, 0, 2, 0, 1, 2, 2,
									 3, 0, 3, 2, 2, 0, 4, 3, 4, 2, 4, 3, 3, 4, 2, 1, 2, 3, 3, 3, 0, 4, 0};
	TPairIndexSamplerPerType<5, std::vector<size_t>> samples(container.size());

	for (size_t rep = 0; rep < 1000; rep++) {
		samples.sampleIndices(container);

		std::set<size_t> unique;
		for (size_t i = 0; i < samples.length(); i++) {
			const auto [first, second] = samples.getIndexPair(i);
			unique.emplace(first);
			unique.emplace(second);
			EXPECT_TRUE(container[first] == container[second]);
		}

		EXPECT_TRUE(unique.size() >= 96 && unique.size() <= 98); // uneven number of 0's, 3's and 4's
	}
}
