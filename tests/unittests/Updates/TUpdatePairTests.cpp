//
// Created by madleina on 06.12.23.
//

#include "stattools/Updates/TUpdatePair.h"
#include "gtest/gtest.h"
#include <vector> // for vector, allocator

#ifdef _OPENMP
#include "omp.h"

using namespace testing;
using namespace stattools;
using namespace coretools::instances;

//--------------------------------------
// TUpdatePairWeighted
//-------------------------------------

TEST(TUpdatePairWeighted, pick_1D_1_thread) {
	coretools::TDimension<1> dim({10});
	size_t numThreads         = 1;
	std::vector<double> stats = {0.0481417924769027, 0.0674731714588995, 0.10386925764235,  0.16467542126275,
								 0.0365687867033919, 0.162895211272098,  0.171287671750737, 0.119815262593038,
								 0.114070393738812,  0.0112030311010203};
	TUpdatePairWeighted<1, 0, stattools::UpdateWeights::irregular> weights(dim.dimensions(), numThreads);
	weights.setWeights({stats}, {}, 1.0);

	size_t numRep = 1e6;
	std::vector<size_t> counts(dim.size(), 0);
	std::vector<size_t> threadcount(numThreads, 0);
#pragma omp parallel for num_threads(numThreads) schedule(static)
	for (size_t i = 0; i < numRep; ++i) {
		auto ix = weights.pick(i, omp_get_thread_num());
		++counts[ix.begin];
		++counts[ix.begin + ix.increment];
		EXPECT_NE(ix.begin, ix.begin + ix.increment);
		++threadcount[omp_get_thread_num()];
	}

	// check if these are approximately the weights
	// Note: can not be exactly the same as we have the constraint the pair must be different
	for (size_t l = 0; l < dim.size(); l++) { EXPECT_NEAR((double)counts[l] / (2 * numRep), stats[l], 0.01); }
	for (size_t l = 0; l < numThreads; l++) { EXPECT_FLOAT_EQ(threadcount[l], numRep / numThreads); }
}

TEST(TUpdatePairWeighted, pick_1D_2_threads) {
	coretools::TDimension<1> dim({10});
	size_t numThreads         = 2;
	std::vector<double> stats = {0.0481417924769027, 0.0674731714588995, 0.10386925764235,  0.16467542126275,
								 0.0365687867033919, 0.162895211272098,  0.171287671750737, 0.119815262593038,
								 0.114070393738812,  0.0112030311010203};
	TUpdatePairWeighted<1, 0, stattools::UpdateWeights::irregular> weights(dim.dimensions(), numThreads);
	weights.setWeights({stats}, {}, 1.0);

	size_t numRep = 1e6;
	std::vector<size_t> counts(dim.size(), 0);
	std::vector<size_t> threadcount(numThreads, 0);
#pragma omp parallel for num_threads(numThreads) schedule(static)
	for (size_t i = 0; i < numRep; ++i) {
		auto ix = weights.pick(i, omp_get_thread_num());
		++counts[ix.begin];
		++counts[ix.begin + ix.increment];
		EXPECT_NE(ix.begin, ix.begin + ix.increment);
		++threadcount[omp_get_thread_num()];
	}

	// check if these are approximately the weights
	// Note: can not be exactly the same as we have the constraint the pair must be different
	// even less accurate than pick_1_thread, since if pair is already occupied, it has to draw again
	for (size_t l = 0; l < dim.size(); l++) { EXPECT_NEAR((double)counts[l] / (2 * numRep), stats[l], 0.05); }
	for (size_t l = 0; l < numThreads; l++) { EXPECT_FLOAT_EQ(threadcount[l], numRep / numThreads); }
}

TEST(TUpdatePairWeighted, pick_2D_2_threads) {
	coretools::TDimension<2> dim({10, 3});
	size_t numThreads         = 2;
	std::vector<double> stats = {0.0481417924769027, 0.0674731714588995, 0.10386925764235,  0.16467542126275,
								 0.0365687867033919, 0.162895211272098,  0.171287671750737, 0.119815262593038,
								 0.114070393738812,  0.0112030311010203};
	TUpdatePairWeighted<2, 0, stattools::UpdateWeights::irregular, stattools::UpdateWeights::regular> weights(
		dim.dimensions(), numThreads);
	weights.setWeights({stats, {}}, {}, 1.0);

	size_t numRep = 1e6;
	std::vector<size_t> counts(dim.size(), 0);
	std::vector<size_t> threadcount(numThreads, 0);
#pragma omp parallel for num_threads(numThreads) schedule(static)
	for (size_t i = 0; i < numRep; ++i) {
		auto ix             = weights.pick(i, omp_get_thread_num());
		const size_t first  = ix.begin;
		const size_t second = ix.begin + ix.increment;
		EXPECT_NE(dim.getSubscripts(first)[0], dim.getSubscripts(second)[0]);
		EXPECT_EQ(dim.getSubscripts(first)[1], dim.getSubscripts(second)[1]);
		++counts[first];
		++counts[second];
		++threadcount[omp_get_thread_num()];
	}

	// check if these are approximately the weights
	// Note: can not be exactly the same as we have the constraint the pair must be different
	size_t c = 0;
	for (size_t i = 0; i < dim.dimensions()[0]; ++i) {
		for (size_t j = 0; j < dim.dimensions()[1]; ++j, ++c) {
			EXPECT_NEAR(3.0 * (double)counts[c] / (2 * numRep), stats[i], 0.05);
		}
	}
	for (size_t l = 0; l < numThreads; l++) { EXPECT_FLOAT_EQ(threadcount[l], numRep / numThreads); }
}
#endif
