//
// Created by madleina on 24.01.24.
//

#include "stattools/Updates/TMarkovOrder.h"
#include "gtest/gtest.h"

using namespace stattools;

TEST(TMarkovOrder, markovOrder_1_1) {
	TMarkovOrder<2> mO;

	coretools::TDimension<2> fullDim({7, 5});
	mO.initialize({1, 1}, fullDim.dimensions());

	EXPECT_EQ(mO.getNumPickers(), 4);

	// starts
	std::array<size_t, 6> starts = {0, 1, 5, 6};
	for (size_t p = 0; p < mO.getNumPickers(); ++p) { EXPECT_EQ(mO.linearStartInFull(p), starts[p]); }

	// lengths
	std::array<size_t, 6> lengths = {12, 8, 9, 6};
	for (size_t p = 0; p < mO.getNumPickers(); ++p) { EXPECT_EQ(mO.length(p), lengths[p]); }

	// index
	for (size_t p = 0; p < mO.getNumPickers(); ++p) {
		int previousCoord0 = -2 + (int)fullDim.getSubscripts(mO.linearStartInFull(p))[0];
		int previousCoord1 = -2 + (int)fullDim.getSubscripts(mO.linearStartInFull(p))[1];
		for (size_t i = 0; i < mO.length(p); ++i) {
			size_t ix  = mO.ixInFull(p, i);
			auto coord = fullDim.getSubscripts(ix);

			if ((int)coord[0] == previousCoord0) {            // jumped to the right
				EXPECT_EQ((int)coord[1] - previousCoord1, 2); // markov order = 1 -> jump 2
				previousCoord1 = (int)coord[1];
			} else {                                          // jumped down
				EXPECT_EQ((int)coord[0] - previousCoord0, 2); // markov order = 1 -> jump 2
				previousCoord0 = (int)coord[0];
				previousCoord1 = (int)coord[1];
			}
		}
	}
}

TEST(TMarkovOrder, markovOrder_1_2) {
	TMarkovOrder<2> mO;

	coretools::TDimension<2> fullDim({7, 5});
	mO.initialize({1, 2}, fullDim.dimensions());

	EXPECT_EQ(mO.getNumPickers(), 6);

	// starts
	std::array<size_t, 6> starts = {0, 1, 2, 5, 6, 7};
	for (size_t p = 0; p < mO.getNumPickers(); ++p) { EXPECT_EQ(mO.linearStartInFull(p), starts[p]); }

	// lengths
	std::array<size_t, 6> lengths = {8, 8, 4, 6, 6, 3};
	for (size_t p = 0; p < mO.getNumPickers(); ++p) { EXPECT_EQ(mO.length(p), lengths[p]); }

	// index
	for (size_t p = 0; p < mO.getNumPickers(); ++p) {
		int previousCoord0 = -2 + (int)fullDim.getSubscripts(mO.linearStartInFull(p))[0];
		int previousCoord1 = -2 + (int)fullDim.getSubscripts(mO.linearStartInFull(p))[1];
		for (size_t i = 0; i < mO.length(p); ++i) {
			size_t ix  = mO.ixInFull(p, i);
			auto coord = fullDim.getSubscripts(ix);

			if ((int)coord[0] == previousCoord0) {            // jumped to the right
				EXPECT_EQ((int)coord[1] - previousCoord1, 3); // markov order = 2 -> jump 3
				previousCoord1 = (int)coord[1];
			} else {                                          // jumped down
				EXPECT_EQ((int)coord[0] - previousCoord0, 2); // markov order = 1 -> jump 2
				previousCoord0 = (int)coord[0];
				previousCoord1 = (int)coord[1];
			}
		}
	}
}

TEST(TMarkovOrder, markovOrder_max_0) {
	TMarkovOrder<2> mO;

	coretools::TDimension<2> fullDim({7, 5});
	mO.initialize({6, 0}, fullDim.dimensions());

	EXPECT_EQ(mO.getNumPickers(), 7);

	// starts
	std::array<size_t, 7> starts = {0, 5, 10, 15, 20, 25, 30};
	for (size_t p = 0; p < mO.getNumPickers(); ++p) { EXPECT_EQ(mO.linearStartInFull(p), starts[p]); }

	// lengths
	std::array<size_t, 7> lengths = {5, 5, 5, 5, 5, 5, 5};
	for (size_t p = 0; p < mO.getNumPickers(); ++p) { EXPECT_EQ(mO.length(p), lengths[p]); }

	// index
	for (size_t p = 0; p < mO.getNumPickers(); ++p) {
		int previousCoord0 = (int)p;
		int previousCoord1 = -1;
		for (size_t i = 0; i < mO.length(p); ++i) {
			size_t ix  = mO.ixInFull(p, i);
			auto coord = fullDim.getSubscripts(ix);

			if ((int)coord[0] == previousCoord0) {            // jumped to the right
				EXPECT_EQ((int)coord[1] - previousCoord1, 1); // markov order = 0 -> jump 1
				previousCoord1 = (int)coord[1];
			} else {                // jumped down
				EXPECT_FALSE(true); // markov order = max -> should never jump down!
			}
		}
	}
}

TEST(TMarkovOrder, markovOrder_0_0) {
	TMarkovOrder<2> mO;

	coretools::TDimension<2> fullDim({7, 5});
	mO.initialize({0, 0}, fullDim.dimensions());

	EXPECT_EQ(mO.getNumPickers(), 1);

	// starts
	for (size_t p = 0; p < mO.getNumPickers(); ++p) { EXPECT_EQ(mO.linearStartInFull(p), p); }

	// lengths
	for (size_t p = 0; p < mO.getNumPickers(); ++p) { EXPECT_EQ(mO.length(p), fullDim.size()); }

	// index
	for (size_t p = 0; p < mO.getNumPickers(); ++p) {
		for (size_t i = 0; i < mO.length(p); ++i) {
			size_t ix = mO.ixInFull(p, i);
			EXPECT_EQ(ix, i);
		}
	}
}

TEST(TMarkovOrder, markovOrder_max_max) {
	TMarkovOrder<2> mO;

	coretools::TDimension<2> fullDim({7, 5});
	mO.initialize({6, 4}, fullDim.dimensions());

	EXPECT_EQ(mO.getNumPickers(), fullDim.size());

	// starts
	for (size_t p = 0; p < mO.getNumPickers(); ++p) { EXPECT_EQ(mO.linearStartInFull(p), p); }

	// lengths
	for (size_t p = 0; p < mO.getNumPickers(); ++p) { EXPECT_EQ(mO.length(p), 1); }

	// index
	for (size_t p = 0; p < mO.getNumPickers(); ++p) {
		for (size_t i = 0; i < mO.length(p); ++i) {
			size_t ix = mO.ixInFull(p, i);
			EXPECT_EQ(ix, p);
		}
	}
}