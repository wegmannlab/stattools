//
// Created by caduffm on 6/28/21.
//

#include <cmath>
#include <limits>
#include <vector>

#include "gmock/gmock.h"
#include "gtest/gtest.h"

#include "coretools/Main/TRandomGenerator.h"
#include "coretools/Types/commonWeakTypes.h"
#include "coretools/Types/probability.h"
#include "stattools/Updates/TProposal.h"

using namespace stattools;
using namespace coretools;
using namespace testing;

//--------------------------------
// TPropKernelBase
//--------------------------------

template<class Type, class UnderlyingType> class TPropKernelBaseDummy : public TPropKernelBase<Type, UnderlyingType> {
public:
	TPropKernelBaseDummy() : TPropKernelBase<Type, UnderlyingType>(){};

	// override pure virtual method
	Type propose(Type Value, UnderlyingType) const override { return Value; };
	Type propose(Type Value, UnderlyingType, UnderlyingType, UnderlyingType) const override { return Value; };

	bool isAdjusted() const override { return true; };

	bool isReversible() const override { return true; }
};

TEST(TPropKernelTest, adjust_default_double) {
	// min and max are given by numeric limits -> range is just the maximum value that type can take
	TPropKernelBaseDummy<Unbounded, double> propKernel;

	// check what happens if prop kernel = 0 -> should be 0.1
	EXPECT_EQ(propKernel.adjustPropKernelIfTooBig(0, "name"), 0.1);
	// smaller than max -> don't adjust
	EXPECT_EQ(propKernel.adjustPropKernelIfTooBig(0.1, "name"), 0.1);
	EXPECT_EQ(propKernel.adjustPropKernelIfTooBig(std::numeric_limits<double>::max() / 2. -
													  std::nextafter(std::numeric_limits<double>::max() / 2., 0.),
												  "name"),
			  std::numeric_limits<double>::max() / 2. - std::nextafter(std::numeric_limits<double>::max() / 2., 0.));
	// exactly max -> don't adjust
	EXPECT_THAT(propKernel.adjustPropKernelIfTooBig(std::numeric_limits<double>::max() / 2, "name"),
				std::numeric_limits<double>::max() / 2.);
	// larger than max -> adjust
	EXPECT_THAT(propKernel.adjustPropKernelIfTooBig(
					std::numeric_limits<double>::max() / 2. +
						std::nextafter(std::numeric_limits<double>::max() / 2., std::numeric_limits<double>::max()),
					"name"),
				std::numeric_limits<double>::max() / 2.);
}

TEST(TPropKernelTest, adjust_nonDefault_double) {
	TPropKernelBaseDummy<Probability, double> propKernel;

	// smaller than max -> don't adjust
	EXPECT_THAT(propKernel.adjustPropKernelIfTooBig(0.1, "name"), 0.1);
	// exactly max -> don't adjust
	EXPECT_THAT(propKernel.adjustPropKernelIfTooBig(0.5, "name"), 0.5);
	// larger than max -> adjust
	EXPECT_THAT(propKernel.adjustPropKernelIfTooBig(0.6, "name"), 0.5);
}

TEST(TPropKernelTest, adjust_default_integer) {
	// min and max are given by numeric limits -> range is just the maximum value that type can take
	TPropKernelBaseDummy<Integer, int> propKernel;

	// check what happens if propkernel = 0 -> should be 1
	EXPECT_THAT(propKernel.adjustPropKernelIfTooBig(0, "name"), 1);
	// smaller than max -> don't adjust
	EXPECT_THAT(propKernel.adjustPropKernelIfTooBig(1, "name"), 1);
	EXPECT_THAT(propKernel.adjustPropKernelIfTooBig(std::numeric_limits<int>::max() / 2. - 1, "name"),
				std::numeric_limits<int>::max() / 2. - 1);
	// exactly max -> don't adjust
	EXPECT_THAT(propKernel.adjustPropKernelIfTooBig(std::numeric_limits<int>::max() / 2., "name"),
				std::numeric_limits<int>::max() / 2.);
	// larger than max -> adjust
	EXPECT_THAT(propKernel.adjustPropKernelIfTooBig(std::numeric_limits<int>::max() / 2. + 1, "name"),
				std::numeric_limits<int>::max() / 2.);
}

TEST(TPropKernelTest, adjust_default_uint8) {
	TPropKernelBaseDummy<UnsignedInt8, uint8_t> propKernel;

	// check what happens if prop kernel = 0 -> should be 1
	EXPECT_THAT(propKernel.adjustPropKernelIfTooBig(0, "name"), 1);
	// smaller than max -> don't adjust
	EXPECT_THAT(propKernel.adjustPropKernelIfTooBig(1, "name"), 1);
	EXPECT_THAT(propKernel.adjustPropKernelIfTooBig(std::numeric_limits<uint8_t>::max() / 2. - 1, "name"),
				std::numeric_limits<uint8_t>::max() / 2. - 1);
	// exactly max -> don't adjust
	EXPECT_THAT(propKernel.adjustPropKernelIfTooBig(std::numeric_limits<uint8_t>::max() / 2., "name"),
				std::numeric_limits<uint8_t>::max() / 2.);
	// larger than max -> adjust
	EXPECT_THAT(propKernel.adjustPropKernelIfTooBig(std::numeric_limits<uint8_t>::max() / 2. + 1, "name"),
				std::numeric_limits<uint8_t>::max() / 2.);
}

//-------------------------------------------
struct TPropKernelNormalTest : Test {
	double value;
};

template<typename Type, typename UnderlyingType>
class MockTPropKernelNormal : public TPropKernelNormal<Type, UnderlyingType> {
public:
	MOCK_METHOD(double, _getJump, (UnderlyingType ProposalWidth), (const, override));
};

TEST_F(TPropKernelNormalTest, propose_positive) {
	MockTPropKernelNormal<Unbounded, double> propKernel;
	EXPECT_CALL(propKernel, _getJump(1.0)).Times(1).WillOnce(Return(0.1));

	// default min max
	value = 1.;
	value = propKernel.propose(value, 1);

	EXPECT_EQ(value, 1.1);
}

TEST_F(TPropKernelNormalTest, propose_negative) {
	MockTPropKernelNormal<Unbounded, double> propKernel;
	EXPECT_CALL(propKernel, _getJump(1.0)).Times(1).WillOnce(Return(-0.1));

	// default min max
	value = -1.;
	value = propKernel.propose(value, 1);

	EXPECT_EQ(value, -1.1);
}

TEST_F(TPropKernelNormalTest, proposeWithoutMirroring_minMax) {
	MockTPropKernelNormal<Probability, double> propKernel;
	EXPECT_CALL(propKernel, _getJump(0.5)).Times(1).WillOnce(Return(0.1));

	value = 0.5;
	value = propKernel.propose(P(value), 0.5);

	EXPECT_EQ(value, 0.6);
}

TEST_F(TPropKernelNormalTest, proposeInsideRange_withMirroring_minMax) {
	MockTPropKernelNormal<Probability, double> propKernel;
	EXPECT_CALL(propKernel, _getJump(0.5)).Times(1).WillOnce(Return(0.1));

	value = 1.;
	value = propKernel.propose(P(value), 0.5);

	EXPECT_FLOAT_EQ(value, 0.9); // 1.1 -> then mirrors -> 0.9
}

TEST_F(TPropKernelNormalTest, proposeOutsideRangePositive_minMax) {
	MockTPropKernelNormal<Probability, double> propKernel;
	EXPECT_CALL(propKernel, _getJump(0.5))
		// always propose outside range/2
		.Times(AtLeast(1))
		.WillOnce(Return(2.))
		.WillOnce(Return(-1.2))
		.WillOnce(Return(1.8))
		.WillOnce(Return(1.12))
		.WillOnce(Return(-1.))
		.WillOnce(Return(1.))
		// now its ok, the proposed value is exactly range/2
		.WillOnce(Return(0.5));

	value = 1.;
	value = propKernel.propose(P(value), 0.5);
	EXPECT_EQ(value, 0.5); // adds 0.5 to 1 -> 1.5 -> mirrors -> 0.5
}

TEST_F(TPropKernelNormalTest, propose_defaultMinMax) {
	TPropKernelNormal<Unbounded, double> propKernel;
	value = 0.;

	double propWidth = 0.5;

	// draw 1000 random values and check if value behaves as expected
	double oldValue = value;
	for (size_t i = 0; i < 100000; i++) {
		value = propKernel.propose(value, propWidth);
		EXPECT_TRUE(value != oldValue);
		EXPECT_TRUE(std::abs(oldValue - value) <= std::numeric_limits<double>::max() / 2); // we didn't propose > max/2
		oldValue = value;
	}
}

TEST_F(TPropKernelNormalTest, propose_nonDefaultMinMax) {
	TPropKernelNormal<Probability, double> propKernel;
	value = 0.;

	double propWidth = 0.5; // maximum allowed

	// draw 1000 random values and check if value behaves as expected
	double oldValue = value;
	for (size_t i = 0; i < 100000; i++) {
		value = propKernel.propose(P(value), propWidth);
		// EXPECT_TRUE(value.value() != value.oldValue()); -> can happen when mirroring
		EXPECT_TRUE(std::abs(oldValue - value) <= 1.); // we didn't propose > max/2
		EXPECT_TRUE(value > 0.);                       // we mirrored
		EXPECT_TRUE(value < 1.);                       // we mirrored
	}
}

//-------------------------------------------
struct TPropKernelUniformTest : Test {
	double value;
};

template<typename Type, typename UnderlyingType>
class MockTPropKernelUniform : public TPropKernelUniform<Type, UnderlyingType> {
public:
	MOCK_METHOD(double, _getJump, (), (const, override));
};

TEST_F(TPropKernelUniformTest, propose_positive) {
	MockTPropKernelUniform<Unbounded, double> propKernel;
	EXPECT_CALL(propKernel, _getJump()).Times(1).WillOnce(Return(0.1));

	value = 1.;
	value = propKernel.propose(value, 1); // 0.1*1 - 1/2 = -0.4

	EXPECT_EQ(value, 0.6);                // directly adds it to TValue
}

TEST_F(TPropKernelUniformTest, propose_negative) {
	MockTPropKernelUniform<Unbounded, double> propKernel;
	EXPECT_CALL(propKernel, _getJump()).Times(1).WillOnce(Return(-0.1));

	value = -1.;
	value = propKernel.propose(value, 1); // -0.1*1 - 1/2 = -0.6

	EXPECT_EQ(value, -1.6);               // directly adds it to TValue
}

TEST_F(TPropKernelUniformTest, propose_minMax_withoutMirroring) {
	MockTPropKernelUniform<Probability, double> propKernel;
	EXPECT_CALL(propKernel, _getJump()).Times(1).WillOnce(Return(-0.1));

	value = 0.7;
	value = propKernel.propose(P(value), 0.5); // -0.1*0.5 - 0.5/2 = -0.3

	EXPECT_FLOAT_EQ(value, 0.4);            // directly adds it to TValue
}

TEST_F(TPropKernelUniformTest, propose_minMax_withMirroring) {
	MockTPropKernelUniform<Probability, double> propKernel;
	EXPECT_CALL(propKernel, _getJump()).Times(1).WillOnce(Return(-0.1));

	value = 0.1;
	value = propKernel.propose(P(value), 0.5); // -0.1*0.5 - 0.5/2 = -0.3

	EXPECT_FLOAT_EQ(value, 0.2);            // directly adds it to TValue and mirrors
}

TEST_F(TPropKernelUniformTest, propose_defaultMinMax) {
	TPropKernelUniform<Unbounded, double> propKernel;
	value = 0.;

	double propWidth = 0.5;
	// draw 100000 random values and check if value behaves as expected
	double oldValue  = value;
	for (size_t i = 0; i < 100000; i++) {
		value = propKernel.propose(value, propWidth);
		EXPECT_TRUE(value != oldValue);
		EXPECT_TRUE(std::abs(oldValue - value) <= std::numeric_limits<double>::max() / 2); // we didn't propose > max/2
		oldValue = value;
	}
}

TEST_F(TPropKernelUniformTest, propose_nonDefaultMinMax) {
	TPropKernelUniform<Probability, double> propKernel;
	value = 0.;

	double propWidth = 0.5;
	// draw 10000 random values and check if value behaves as expected
	double oldValue  = value;
	for (size_t i = 0; i < 1000000; i++) {
		value = propKernel.propose(P(value), propWidth);
		// EXPECT_TRUE(value.value() != value.oldValue()); -> can happen when mirroring
		EXPECT_TRUE(std::abs(oldValue - value) <= 1.); // we didn't propose > max/2
		EXPECT_TRUE(value > 0.);                       // we mirrored
		EXPECT_TRUE(value < 1.);                       // we mirrored
		oldValue = value;
	}
}

//-------------------------------------------
struct TPropKernelScalingLogNormalTest : Test {
	double value;
};

template<typename Type, typename UnderlyingType>
class MockTPropKernelScalingLogNormal : public TPropKernelScalingLogNormal<Type, UnderlyingType> {
public:
	MOCK_METHOD(double, _getJump, (UnderlyingType ProposalWidth), (const, override));
};

TEST_F(TPropKernelScalingLogNormalTest, propose_positive_valueIsPositive) {
	MockTPropKernelScalingLogNormal<Positive, double> propKernel;
	EXPECT_CALL(propKernel, _getJump(1.0)).Times(1).WillOnce(Return(0.1));

	value = 1.;
	value = propKernel.propose(value, 1);

	EXPECT_FLOAT_EQ(value, 1.105170918);
}

TEST_F(TPropKernelScalingLogNormalTest, propose_negative_valueIsPositive) {
	MockTPropKernelScalingLogNormal<Positive, double> propKernel;
	EXPECT_CALL(propKernel, _getJump(1.0)).Times(1).WillOnce(Return(-0.1));

	value = 1.;
	value = propKernel.propose(value, 1);

	EXPECT_FLOAT_EQ(value, 0.904837418);
}

TEST_F(TPropKernelScalingLogNormalTest, propose_positive_valueIsZero) {
	MockTPropKernelScalingLogNormal<Positive, double> propKernel;
	EXPECT_CALL(propKernel, _getJump(1.0)).Times(1).WillOnce(Return(0.1));

	value = 0.0;
	value = propKernel.propose(value, 1);

	EXPECT_FLOAT_EQ(value, 1.105170918e-05);
}

TEST_F(TPropKernelScalingLogNormalTest, propose_zero) {
	MockTPropKernelScalingLogNormal<Positive, double> propKernel;
	EXPECT_CALL(propKernel, _getJump(1.0))
		.Times(2)
		.WillOnce(Return(-1000000)) // exp(-10000000) = 0.0
		.WillOnce(Return(0.1));

	value = 1.0;
	value = propKernel.propose(value, 1);

	EXPECT_FLOAT_EQ(value, 1.105170918);
}

TEST_F(TPropKernelScalingLogNormalTest, propose) {
	TPropKernelScalingLogNormal<Positive, double> propKernel;
	value = 0.;

	double propWidth = 0.5;
	// draw 1000 random values and check if value behaves as expected
	double oldValue  = value;
	for (size_t i = 0; i < 100000; i++) {
		value = propKernel.propose(value, propWidth);
		EXPECT_TRUE(value != oldValue);
		EXPECT_TRUE(std::abs(oldValue - value) <= std::numeric_limits<double>::max() / 2); // we didn't propose > max/2
		oldValue = value;
	}
}

TEST_F(TPropKernelScalingLogNormalTest, adjustProposalWidth) {
	TPropKernelScalingLogNormal<Positive, double> propKernel;

	// acceptance rate = 0.44 -> don't change
	double adj = propKernel.adjustProposalWidth(0.5, 0.44_P, "dummy");
	EXPECT_FLOAT_EQ(adj, 0.5);

	// acceptance rate > 0.44
	adj = propKernel.adjustProposalWidth(0.5, P(1. / 3), "dummy");
	EXPECT_FLOAT_EQ(adj, 0.45090142);

	// acceptance rate < 0.44
	adj = propKernel.adjustProposalWidth(0.5, P(1. / 6), "dummy");
	EXPECT_FLOAT_EQ(adj, 0.34060714);
}

TEST_F(TPropKernelScalingLogNormalTest, hastingsRatioPropKernel) {
	TPropKernelScalingLogNormal<Positive, double> propKernel;

	double ratio = propKernel.hastingsRatioPropKernel(0.5, 0.8);
	EXPECT_FLOAT_EQ(ratio, 0.5 / 0.8);
}

TEST_F(TPropKernelScalingLogNormalTest, logHastingsRatioPropKernel) {
	TPropKernelScalingLogNormal<Positive, double> propKernel;

	double ratio = propKernel.logHastingsRatioPropKernel(0.5, 0.8);
	EXPECT_FLOAT_EQ(ratio, log(0.5) - log(0.8));
}

TEST_F(TPropKernelScalingLogNormalTest, isReversible) {
	TPropKernelScalingLogNormal<Positive, double> propKernel;
	EXPECT_FALSE(propKernel.isReversible());
}

//-------------------------------------------
template<class Type, class UnderlyingType>
class BridgeTPropKernelInteger : public TPropKernelInteger<Type, UnderlyingType> {
public:
	BridgeTPropKernelInteger() : TPropKernelInteger<Type, UnderlyingType>(){};
	void _fillCumulProbabilities(const UnderlyingType &propWidth) {
		TPropKernelInteger<Type, UnderlyingType>::_fillCumulProbabilities(propWidth);
	}
	std::vector<double> getCumulProbs() { return this->_cumulProbs; }
};

struct TPropKernelIntTest : Test {
	int value;
};

TEST_F(TPropKernelIntTest, _fillCumulProbabilities_propWidth1) {
	int propWidth = 1;
	BridgeTPropKernelInteger<Integer, int> propKernel;
	propKernel._fillCumulProbabilities(propWidth);
	std::vector<double> cumulProbs = propKernel.getCumulProbs();

	EXPECT_EQ(cumulProbs.size(), 3);
	EXPECT_FLOAT_EQ(cumulProbs[0], 1. / 3.);
	EXPECT_FLOAT_EQ(cumulProbs[1], 2. / 3.);
	EXPECT_FLOAT_EQ(cumulProbs[2], 1.);
}

TEST_F(TPropKernelIntTest, _fillCumulProbabilities_propWidth3) {
	int propWidth = 3;
	BridgeTPropKernelInteger<Integer, int> propKernel;
	propKernel._fillCumulProbabilities(propWidth);
	std::vector<double> cumulProbs = propKernel.getCumulProbs();

	EXPECT_EQ(cumulProbs.size(), 7);
	EXPECT_FLOAT_EQ(cumulProbs[0], 1. / 7.);
	EXPECT_FLOAT_EQ(cumulProbs[1], 2. / 7.);
	EXPECT_FLOAT_EQ(cumulProbs[2], 3. / 7.);
	EXPECT_FLOAT_EQ(cumulProbs[3], 4. / 7.);
	EXPECT_FLOAT_EQ(cumulProbs[4], 5. / 7.);
	EXPECT_FLOAT_EQ(cumulProbs[5], 6. / 7.);
	EXPECT_FLOAT_EQ(cumulProbs[6], 1.);
}

TEST_F(TPropKernelIntTest, pickOne_cumulProbs) {
	// check if function pickOne always returns values within the given propWidth
	int propWidth = 3;
	BridgeTPropKernelInteger<Integer, int> propKernel;
	propKernel._fillCumulProbabilities(propWidth);
	std::vector<double> cumulProbs = propKernel.getCumulProbs();

	coretools::TRandomGenerator randomGenerator;

	// draw 1000 random values and check if (randomValue - propWidth) is indeed inside range -propWidth:propWidth
	for (size_t i = 0; i < 100000; i++) {
		int val = randomGenerator.pickOne(cumulProbs) - propWidth;
		EXPECT_TRUE(val >= -propWidth && val <= propWidth);
	}
}

TEST_F(TPropKernelIntTest, propose_defaultMinMax) {
	BridgeTPropKernelInteger<Integer, int> propKernel;
	value         = 0.;
	int propWidth = 3;
	propKernel.adjustPropKernelIfTooBig(propWidth, "name");

	// draw 1000 random values and check if value behaves as expected
	// (it is really hard to mock templated functions, so I don't work with mocks here)
	int oldValue = value;
	for (size_t i = 0; i < 100000; i++) {
		value = propKernel.propose(value, propWidth);
		EXPECT_TRUE(value != oldValue);                       // we never proposed 0
		EXPECT_TRUE(std::abs(oldValue - value) <= propWidth); // we didn't propose > propWidth
		oldValue = value;
	}
}

TEST_F(TPropKernelIntTest, propose_defaultMinMax_uint8) {
	BridgeTPropKernelInteger<UnsignedInt8, uint8_t> propKernel;
	value             = 0.;
	uint8_t propWidth = 3;
	propKernel.adjustPropKernelIfTooBig(propWidth, "name");

	// draw 1000 random values and check if value behaves as expected
	// (it is really hard to mock templated functions, so I don't work with mocks here)
	uint8_t oldValue = value;
	for (size_t i = 0; i < 100000; i++) {
		value = propKernel.propose(value, propWidth);
		// EXPECT_TRUE(value != oldValue); // can happen when mirroring!
		EXPECT_TRUE(std::abs((int)oldValue - (int)value) <= propWidth); // we didn't propose > propWidth
		oldValue = value;
	}
}

//-------------------------------------------

struct TPropKernelRandomIntegerTest : Test {
	int value;
};

TEST_F(TPropKernelRandomIntegerTest, propose_defaultMinMax) {
	BridgeTPropKernelInteger<Integer, int> propKernel;
	value         = 0.;
	int propWidth = 3; // doesn't matter for this prop kernel
	propKernel.adjustPropKernelIfTooBig(propWidth, "name");

	// draw 1000 random values and check if value behaves as expected
	int oldValue = value;
	for (size_t i = 0; i < 100000; i++) {
		value = propKernel.propose(value, propWidth);
		EXPECT_TRUE(value != oldValue); // we never proposed 0
		oldValue = value;
	}
}

TEST_F(TPropKernelRandomIntegerTest, propose_defaultMinMax_uint8) {
	BridgeTPropKernelInteger<UnsignedInt8, uint8_t> propKernel;
	value             = 0.;
	uint8_t propWidth = 3;
	propKernel.adjustPropKernelIfTooBig(propWidth, "name");

	// draw 1000 random values and check if value behaves as expected
	uint8_t oldValue = value;
	for (size_t i = 0; i < 100000; i++) {
		value = propKernel.propose(value, propWidth);
		EXPECT_TRUE(value != oldValue);
		oldValue = value;
	}
}

//-----------------------------------
// TPropKernelBool
//-----------------------------------

struct TPropKernelBoolTest : Test {
	bool value;
};

TEST_F(TPropKernelBoolTest, propose) {
	TPropKernelBool<Boolean> propKernel;
	value = false;

	// false -> true
	value = propKernel.propose(value, 1);
	EXPECT_EQ(value, true);

	// true -> false
	value = propKernel.propose(value, 1);
	EXPECT_EQ(value, false);

	// false -> true
	value = propKernel.propose(value, 1);
	EXPECT_EQ(value, true);
}
