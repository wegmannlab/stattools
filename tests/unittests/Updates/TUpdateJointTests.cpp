//
// Created by madleina on 07.12.23.
//
#include "stattools/Updates/TUpdateJoint.h"
#include "gtest/gtest.h"
#include <vector>

#ifdef _OPENMP
#include "omp.h"
#endif

using namespace testing;
using namespace stattools;

//--------------------------------------
// TUpdateJoint
//-------------------------------------

#ifdef _OPENMP
TEST(TUpdateJoint, pick_1D) {
	coretools::TDimension<1> dim({10});
	size_t numThreads = 4;

	TUpdateJoint<1, 0, stattools::UpdateWeights::regular> weights(dim.dimensions(), numThreads);

	std::vector<size_t> counts(dim.size(), 0);
#pragma omp parallel for num_threads(numThreads) schedule(static)
	for (size_t i = 0; i < weights.numUpdates(); ++i) {
		auto ix = weights.pick(i, omp_get_thread_num());
		for (size_t j = ix.begin; j < ix.end; j += ix.increment) { ++counts[j]; }
	}

	for (size_t l = 0; l < dim.size(); l++) { EXPECT_EQ(counts[l], 1); }
}

TEST(TUpdateJoint, pick_2D) {
	coretools::TDimension<2> dim({10, 20});
	size_t numThreads = 4;

	TUpdateJoint<2, 0, stattools::UpdateWeights::regular> weights(dim.dimensions(), numThreads);

	std::vector<size_t> counts(dim.size(), 0);
#pragma omp parallel for num_threads(numThreads) schedule(static)
	for (size_t i = 0; i < weights.numUpdates(); ++i) {
		auto ix = weights.pick(i, omp_get_thread_num());
		for (size_t j = ix.begin; j < ix.end; j += ix.increment) { ++counts[j]; }
	}

	for (size_t l = 0; l < dim.size(); l++) { EXPECT_EQ(counts[l], 1); }
}
#endif
