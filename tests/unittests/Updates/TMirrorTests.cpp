//
// Created by caduffm on 7/1/21.
//

#include <cmath>
#include <limits>
#include <stdint.h>

#include "gtest/gtest.h"

#include "coretools/Types/commonWeakTypes.h"
#include "coretools/Types/probability.h"
#include "stattools/Updates/TMirror.h"

using namespace stattools;
using namespace coretools;
using namespace testing;

//--------------------------------
// Mirroring
//--------------------------------

TEST(MirrorSignedTest, dontMirror_double_defaultMinMax) {
	using Type = Unbounded;
	MirrorSigned<Type> mirror;

	// value = 0, jump > 0 (cannot jump directly on maximum because jump size is restricted to range/2)
	EXPECT_EQ(mirror.mirror(0., 1., Type::min(), Type::max()), 1.);
	EXPECT_EQ(mirror.mirror(0., 1000., Type::min(), Type::max()), 1000.);
	// value = 0, jump < 0 (cannot jump directly on minimum because jump size is restricted to range/2)
	EXPECT_EQ(mirror.mirror(0., -0.5, Type::min(), Type::max()), -0.5);
	EXPECT_EQ(mirror.mirror(0., -1000., Type::min(), Type::max()), -1000.);
	// value < 0, jump > 0 (cannot jump directly on maximum because jump size is restricted to range/2)
	EXPECT_EQ(mirror.mirror(-0.5, 1., Type::min(), Type::max()), 0.5);
	EXPECT_EQ(mirror.mirror(-0.5, 1000., Type::min(), Type::max()), 999.5);
	// value < 0, jump < 0
	EXPECT_EQ(mirror.mirror(-0.5, -0.2, Type::min(), Type::max()), -0.7);
	double value = std::nextafter(std::numeric_limits<double>::lowest(), 0.); // next-smaller value above numeric min
	EXPECT_EQ(mirror.mirror(value, std::numeric_limits<double>::lowest() - value, Type::min(), Type::max()),
			  std::numeric_limits<double>::lowest()); // jump exactly on min -> ok because min is included
	// value > 0, jump > 0
	EXPECT_EQ(mirror.mirror(1., 1., Type::min(), Type::max()), 2.);
	EXPECT_EQ(mirror.mirror(1000000., 1000., Type::min(), Type::max()), 1001000.);
	value = std::nextafter(std::numeric_limits<double>::max(), 0.); // next-smaller value below numeric max
	EXPECT_EQ(mirror.mirror(value, std::numeric_limits<double>::max() - value, Type::min(), Type::max()),
			  std::numeric_limits<double>::max());                  // jump exactly on max -> ok because max is included
	// value > 0, jump < 0 (cannot jump directly on minimum because jump size is restricted to range/2)
	EXPECT_EQ(mirror.mirror(1., -2., Type::min(), Type::max()), -1.); // ok because min is included
	EXPECT_EQ(mirror.mirror(1000000., -1000., Type::min(), Type::max()), 999000.);
}

TEST(MirrorSignedTest, doMirror_double_defaultMinMax) {
	using Type = Unbounded;
	MirrorSigned<Type> mirror;

	// value = 0, jump > 0 -> not possible to cross maximum in this case, as jump size is restricted to range/2
	// value = 0, jump < 0 -> not possible to cross maximum in this case, as jump size is restricted to range/2
	// value < 0, jump > 0 -> not possible to cross maximum in this case, as jump size is restricted to range/2
	// value < 0, jump < 0
	EXPECT_EQ(mirror.mirror(-0.5, -0.2, Type::min(), Type::max()), -0.7);
	double value = std::nextafter(std::numeric_limits<double>::lowest(), 0.); // next-smaller value above numeric min
	EXPECT_EQ(
		mirror.mirror(value, -3. * (value - std::numeric_limits<double>::lowest()), Type::min(), Type::max()),
		std::numeric_limits<double>::lowest() +
			2. * (value - std::numeric_limits<double>::lowest())); // jump exactly on min -> ok because min is included
	// value > 0, jump > 0
	value = std::nextafter(std::numeric_limits<double>::max(), 0.); // next-smaller value below numeric max
	EXPECT_EQ(mirror.mirror(value, 3. * (std::numeric_limits<double>::max() - value), Type::min(), Type::max()),
			  std::numeric_limits<double>::max() - 2. * (std::numeric_limits<double>::max() - value));
	// value > 0, jump < 0 -> not possible to cross maximum in this case, as jump size is restricted to range/2
}

TEST(MirrorSignedTest, dontMirror_doublePositive) {
	using Type = Positive;
	MirrorSigned<Type> mirror;

	// value = 0, jump > 0 (cannot jump directly on maximum because jump size is restricted to range/2)
	EXPECT_EQ(mirror.mirror(0., 1., Type::min(), Type::max()), 1.);
	EXPECT_EQ(mirror.mirror(0., 1000., Type::min(), Type::max()), 1000.);
	// value = 0, jump < 0 -> always mirror -> will be tested below in seperate test
	// value < 0, jump > 0 -> will never occur, as min = 0
	// value < 0, jump < 0 -> will never occur, as min = 0
	// value > 0, jump > 0
	EXPECT_EQ(mirror.mirror(1., 1., Type::min(), Type::max()), 2.);
	EXPECT_EQ(mirror.mirror(1000000., 1000., Type::min(), Type::max()), 1001000.);
	double value = std::nextafter(std::numeric_limits<double>::max(), 0.); // next-smaller value below numeric max
	EXPECT_EQ(mirror.mirror(value, std::numeric_limits<double>::max() - value, Type::min(), Type::max()),
			  std::numeric_limits<double>::max()); // jump exactly on max -> ok because max is included
	// value > 0, jump < 0
	EXPECT_EQ(mirror.mirror(1., -1., Type::min(), Type::max()), 0.); // ok because min is included
	EXPECT_EQ(mirror.mirror(1000000., -1000., Type::min(), Type::max()), 999000.);
}

TEST(MirrorSignedTest, doMirror_doublePositive) {
	using Type = Positive;
	MirrorSigned<Type> mirror;

	// value = 0, jump > 0 -> not possible to cross maximum in this case, as jump size is restricted to range/2
	EXPECT_EQ(mirror.mirror(0., 1000000000., Type::min(), Type::max()), 1000000000.);
	// value = 0, jump < 0
	EXPECT_EQ(mirror.mirror(0., -3., Type::min(), Type::max()), 3.);
	EXPECT_EQ(mirror.mirror(0., -100000., Type::min(), Type::max()), 100000.);
	// value < 0, jump > 0 -> will never occur, as min = 0
	// value < 0, jump < 0 -> will never occur, as min = 0
	// value > 0, jump > 0
	double value = std::nextafter(std::numeric_limits<double>::max(), 0.); // next-smaller value below numeric max
	EXPECT_EQ(mirror.mirror(value, 3. * (std::numeric_limits<double>::max() - value), Type::min(), Type::max()),
			  std::numeric_limits<double>::max() - 2. * (std::numeric_limits<double>::max() - value));
	// value > 0, jump < 0
	EXPECT_EQ(mirror.mirror(1., -10., Type::min(), Type::max()), 9.);
	EXPECT_EQ(mirror.mirror(1000000., -3000000., Type::min(), Type::max()), 2000000.);
}

TEST(MirrorSignedTest, dontMirror_doubleStrictlyPositive) {
	using Type = StrictlyPositive;
	MirrorSigned<Type> mirror;

	// value = 0, jump > 0 -> will never occur as min excluded
	// value = 0, jump < 0 -> will never occur as min excluded
	// value < 0, jump > 0 -> will never occur, as min = 0
	// value < 0, jump < 0 -> will never occur, as min = 0
	// value > 0, jump > 0
	EXPECT_EQ(mirror.mirror(1., 1., Type::min(), Type::max()), 2.);
	EXPECT_EQ(mirror.mirror(1000000., 1000., Type::min(), Type::max()), 1001000.);
	double value = std::nextafter(std::numeric_limits<double>::max(), 0.); // next-smaller value below numeric max
	EXPECT_EQ(mirror.mirror(value, std::numeric_limits<double>::max() - value, Type::min(), Type::max()),
			  std::numeric_limits<double>::max());
	// value > 0, jump < 0
	EXPECT_EQ(mirror.mirror(1., -1., Type::min(), Type::max()), std::numeric_limits<double>::min());
	EXPECT_EQ(mirror.mirror(1000000., -1000., Type::min(), Type::max()), 999000.);
}

TEST(MirrorSignedTest, doMirror_doubleStrictlyPositive) {
	using Type = StrictlyPositive;
	MirrorSigned<Type> mirror;

	// value = 0, jump > 0 -> will never occur as min excluded
	// value = 0, jump < 0 -> will never occur as min excluded
	// value < 0, jump > 0 -> will never occur, as min = 0
	// value < 0, jump < 0 -> will never occur, as min = 0
	// value > 0, jump > 0 -> same as doMirrorIncluded
	double value = std::nextafter(std::numeric_limits<double>::max(), 0.); // next-smaller value below numeric max
	EXPECT_EQ(mirror.mirror(value, 3. * (std::numeric_limits<double>::max() - value), Type::min(), Type::max()),
			  std::numeric_limits<double>::max() - 2. * (std::numeric_limits<double>::max() - value));
	// value > 0, jump < 0 -> same as doMirrorIncluded
	EXPECT_EQ(mirror.mirror(1., -10., Type::min(), Type::max()), 9.);
	EXPECT_EQ(mirror.mirror(1000000., -3000000., Type::min(), Type::max()), 2000000.);
}

TEST(MirrorSignedTest, dontMirror_doubleNegative) {
	using Type = LogProbability;
	MirrorSigned<Type> mirror;

	// value = 0, jump > 0 -> will never occur, as max = 0
	// value = 0, jump < 0 (cannot jump directly on minimum because jump size is restricted to range/2)
	EXPECT_EQ(mirror.mirror(logP(0.), -1., Type::min(), Type::max()), -1.);
	EXPECT_EQ(mirror.mirror(logP(0.), -1000., Type::min(), Type::max()), -1000.);
	// value < 0, jump > 0
	EXPECT_EQ(mirror.mirror(logP(-0.5), 0.25, Type::min(), Type::max()), -0.25);
	EXPECT_EQ(mirror.mirror(logP(-0.5), 0.5, Type::min(), Type::max()), 0.); // ok because max is included
	// value < 0, jump < 0
	EXPECT_EQ(mirror.mirror(logP(-0.5), -0.2, Type::min(), Type::max()), -0.7);
	double value = std::nextafter(std::numeric_limits<double>::lowest(), 0.); // next-smaller value above numeric min
	EXPECT_EQ(mirror.mirror(logP(value), std::numeric_limits<double>::lowest() - value, Type::min(), Type::max()),
			  std::numeric_limits<double>::lowest()); // jump exactly on min -> ok because min is included
													  // value > 0, jump > 0 -> will never occur, as max = 0
													  // value > 0, jump < 0 -> will never occur, as max = 0
}

TEST(MirrorSignedTest, doMirror_doubleNegative) {
	using Type = LogProbability;
	MirrorSigned<Type> mirror;

	// value = 0, jump > 0 -> will never occur, as max = 0
	// value = 0, jump < 0 -> not possible to cross minimum in this case, as jump size is restricted to range/2
	EXPECT_EQ(mirror.mirror(logP(0.), -10000000., Type::min(), Type::max()), -10000000.);
	// value < 0, jump > 0
	EXPECT_EQ(mirror.mirror(logP(-0.5), 2., Type::min(), Type::max()), -1.5);
	EXPECT_EQ(mirror.mirror(logP(-0.5), 1000., Type::min(), Type::max()), -999.5);
	// value < 0, jump < 0
	double value = std::nextafter(std::numeric_limits<double>::lowest(), 0.); // next-smaller value above numeric min
	EXPECT_EQ(
		mirror.mirror(logP(value), -3. * (value - std::numeric_limits<double>::lowest()), Type::min(), Type::max()),
		std::numeric_limits<double>::lowest() +
			2. * (value - std::numeric_limits<double>::lowest())); // jump exactly on min -> ok because min is included
	// value > 0, jump > 0 -> will never occur, as max = 0
	// value > 0, jump < 0 -> will never occur, as max = 0
}

TEST(MirrorSignedTest, dontMirror_probability) {
	using Type = Probability;
	MirrorSigned<Type> mirror;

	// value = 0, jump > 0
	EXPECT_EQ(mirror.mirror(0._P, 0.5, Type::min(), Type::max()), 0.5);   // ok because min is included
	EXPECT_EQ(mirror.mirror(0._P, 0.25, Type::min(), Type::max()), 0.25); // ok because min is included
	// value = 0, jump < 0 -> can only happen by mirroring -> tested below
	// value < 0, jump > 0 -> never occurs, as min = 0
	// value < 0, jump < 0 -> never occurs, as min = 0
	// value > 0, jump > 0
	EXPECT_EQ(mirror.mirror(0.5_P, 0.25, Type::min(), Type::max()), 0.75);
	EXPECT_EQ(mirror.mirror(0.5_P, 0.5, Type::min(), Type::max()), 1.); // ok because max is included
	// value > 0, jump < 0
	EXPECT_EQ(mirror.mirror(0.5_P, -0.3, Type::min(), Type::max()), 0.2);
	EXPECT_EQ(mirror.mirror(0.5_P, -0.5, Type::min(), Type::max()), 0.); // ok because min is included
}

TEST(MirrorSignedTest, doMirror_probability) {
	using Type = Probability;
	MirrorSigned<Type> mirror;

	// value = 0, jump > 0 -> never reach max in order to mirror, because jump size is restricted to range/2
	// value = 0, jump < 0
	EXPECT_EQ(mirror.mirror(0._P, -0.25, Type::min(), Type::max()), 0.25); // ok because min is included
	// value < 0, jump > 0 -> never occurs, as min = 0
	// value < 0, jump < 0 -> never occurs, as min = 0
	// value > 0, jump > 0
	EXPECT_EQ(mirror.mirror(0.8_P, 0.5, Type::min(), Type::max()), 0.7);
	EXPECT_FLOAT_EQ(mirror.mirror(0.9_P, 0.2, Type::min(), Type::max()), 0.9);
	// value > 0, jump < 0
	EXPECT_FLOAT_EQ(mirror.mirror(0.2_P, -0.3, Type::min(), Type::max()), 0.1);
	EXPECT_EQ(mirror.mirror(0.1_P, -0.5, Type::min(), Type::max()), 0.4);
}

TEST(MirrorSignedTest, dontMirror_ZeroOneOpenDouble) {
	using Type = ZeroOneOpen;
	MirrorSigned<Type> mirror;

	// value = 0, jump > 0 -> never occurs, as min = 0 and min is excluded
	// value = 0, jump < 0 -> never occurs, as min = 0 and min is excluded
	// value < 0, jump > 0 -> never occurs, as min = 0
	// value < 0, jump < 0 -> never occurs, as min = 0
	// value > 0, jump > 0
	EXPECT_EQ(mirror.mirror(0.5, 0.25, Type::min(), Type::max()), 0.75);
	EXPECT_EQ(mirror.mirror(0.5, 0.5, Type::min(), Type::max()),
			  nextafter(1.0, 0.0) - (0.5 - (nextafter(1.0, 0.0) - 0.5)));
	// value > 0, jump < 0
	EXPECT_EQ(mirror.mirror(0.5, -0.3, Type::min(), Type::max()), 0.2);
	EXPECT_EQ(mirror.mirror(0.5, -0.5, Type::min(), Type::max()), std::numeric_limits<double>::min());
}

TEST(MirrorSignedTest, doMirror_ZeroOneOpenDouble) {
	using Type = ZeroOneOpen;
	MirrorSigned<Type> mirror;

	// value = 0, jump > 0 -> never reach max in order to mirror, because jump size is restricted to range/2
	// value = 0, jump < 0 -> never occurs, as min = 0 and min is excluded
	// value < 0, jump > 0 -> never occurs, as min = 0
	// value < 0, jump < 0 -> never occurs, as min = 0
	// value > 0, jump > 0
	EXPECT_FLOAT_EQ(mirror.mirror(0.8, 0.5, Type::min(), Type::max()), 0.7);
	EXPECT_FLOAT_EQ(mirror.mirror(0.9, 0.2, Type::min(), Type::max()), 0.9);
	// value > 0, jump < 0
	EXPECT_FLOAT_EQ(mirror.mirror(0.2, -0.3, Type::min(), Type::max()), 0.1);
	EXPECT_FLOAT_EQ(mirror.mirror(0.1, -0.5, Type::min(), Type::max()), 0.4);
}

TEST(MirrorSignedTest, dontMirror_Int) {
	using Type = Integer;
	MirrorSigned<Type> mirror;

	// value = 0, jump > 0 (cannot jump directly on maximum because jump size is restricted to range/2)
	EXPECT_EQ(mirror.mirror(0, 1, Type::min(), Type::max()), 1);
	EXPECT_EQ(mirror.mirror(0, 1000, Type::min(), Type::max()), 1000);
	// value = 0, jump < 0 (cannot jump directly on minimum because jump size is restricted to range/2)
	EXPECT_EQ(mirror.mirror(0, -5, Type::min(), Type::max()), -5);
	EXPECT_EQ(mirror.mirror(0, -1000, Type::min(), Type::max()), -1000);
	// value < 0, jump > 0 (cannot jump directly on maximum because jump size is restricted to range/2)
	EXPECT_EQ(mirror.mirror(-5, 3, Type::min(), Type::max()), -2);
	EXPECT_EQ(mirror.mirror(-5, 1000, Type::min(), Type::max()), 995);
	// value < 0, jump < 0
	EXPECT_EQ(mirror.mirror(-5, -2, Type::min(), Type::max()), -7);
	int value = std::numeric_limits<int>::lowest() + 1; // next-smaller value above numeric min
	EXPECT_EQ(mirror.mirror(value, -1, Type::min(), Type::max()),
			  std::numeric_limits<int>::lowest());      // jump exactly on min -> ok because min is included
	// value > 0, jump > 0
	EXPECT_EQ(mirror.mirror(1, 1, Type::min(), Type::max()), 2);
	EXPECT_EQ(mirror.mirror(1000000, 1000, Type::min(), Type::max()), 1001000);
	value = std::numeric_limits<int>::max() - 1; // next-smaller value below numeric max
	EXPECT_EQ(mirror.mirror(value, 1, Type::min(), Type::max()),
			  std::numeric_limits<int>::max());  // jump exactly on max -> ok because max is included
	// value > 0, jump < 0 (cannot jump directly on minimum because jump size is restricted to range/2)
	EXPECT_EQ(mirror.mirror(1, -2, Type::min(), Type::max()), -1);
	EXPECT_EQ(mirror.mirror(1000000, -1000, Type::min(), Type::max()), 999000.);
}

TEST(MirrorSignedTest, doMirror_Int) {
	using Type = Integer;
	MirrorSigned<Type> mirror;

	// value = 0, jump > 0 -> not possible to cross maximum in this case, as jump size is restricted to range/2
	// value = 0, jump < 0 -> not possible to cross maximum in this case, as jump size is restricted to range/2
	// value < 0, jump > 0 -> not possible to cross maximum in this case, as jump size is restricted to range/2
	// value < 0, jump < 0
	int value = std::numeric_limits<int>::lowest() + 1; // next-smaller value above numeric min
	EXPECT_EQ(mirror.mirror(value, -3, Type::min(), Type::max()), std::numeric_limits<int>::lowest() + 1);
	// value > 0, jump > 0
	value = std::numeric_limits<int>::max() - 1; // next-smaller value below numeric max
	EXPECT_EQ(mirror.mirror(value, 3, Type::min(), Type::max()), std::numeric_limits<int>::max() - 1);
	// value > 0, jump < 0 -> not possible to cross maximum in this case, as jump size is restricted to range/2
}

TEST(MirrorUnsignedTest, dontMirror_Uint8) {
	using Type = UnsignedInt8;
	MirrorUnsigned<Type> mirror;

	// value = 0, jump > 0 (cannot jump directly on maximum because jump size is restricted to range/2)
	EXPECT_EQ(mirror.mirror(0, 2, 1, Type::min(), Type::max()), 1);
	EXPECT_EQ(mirror.mirror(0, 255, 128, Type::min(), Type::max()), 127);
	// value = 0, jump < 0 -> always mirrors, see below in separate test
	// value < 0, jump > 0 -> never the case for uints
	// value < 0, jump < 0 -> never the case for uints
	// value > 0, jump > 0
	EXPECT_EQ(mirror.mirror(1, 200, 100, Type::min(), Type::max()), 101);
	uint8_t value = std::numeric_limits<uint8_t>::max() - 1; // next-smaller value below numeric max
	EXPECT_EQ(mirror.mirror(value, 2, 1, Type::min(), Type::max()),
			  std::numeric_limits<uint8_t>::max());          // jump exactly on max -> ok because max is included
	// value > 0, jump < 0
	EXPECT_EQ(mirror.mirror(255, 2, 4, Type::min(), Type::max()), 253);
	EXPECT_EQ(mirror.mirror(10, 10, 20, Type::min(), Type::max()), 0); // ok because mis included
}

TEST(MirrorUnsignedTest, doMirror_Uint8) {
	using Type = UnsignedInt8;
	MirrorUnsigned<Type> mirror;

	// value = 0, jump > 0 -> not possible to cross maximum in this case, as jump size is restricted to range/2
	// value = 0, jump < 0
	EXPECT_EQ(mirror.mirror(0, 2, 4, Type::min(), Type::max()), 1);
	EXPECT_EQ(mirror.mirror(0, 128, 255, Type::min(), Type::max()), 126);
	// value < 0, jump > 0 -> never the case for uints
	// value < 0, jump < 0 -> never the case for uints
	// value > 0, jump > 0
	uint8_t value = std::numeric_limits<uint8_t>::max() - 1; // next-smaller value below numeric max
	EXPECT_EQ(mirror.mirror(value, 6, 3, Type::min(), Type::max()), std::numeric_limits<uint8_t>::max() - 1);
	// value > 0, jump < 0
	value = std::numeric_limits<uint8_t>::lowest() + 1; // next-smaller value above numeric min
	EXPECT_EQ(mirror.mirror(value, 3, 6, Type::min(), Type::max()), std::numeric_limits<uint8_t>::lowest() + 1);
}

TEST(MirrorUnsignedTest, smallRange_1) {
	using Type = coretools::UnsignedInt8WithMax<0>;
	Type::setMax(1);
	MirrorUnsigned<Type> mirror;

	std::vector<size_t> vals = {0, 0, 0, 1, 1, 1};

	const size_t shift = 1;

	size_t c = 0;
	for (size_t jump = 0; jump <= 2 * coretools::UnsignedInt8WithMax<0>::max().get(); jump++) {
		for (size_t val = 0; val <= coretools::UnsignedInt8WithMax<0>::max().get(); ++val, ++c) {
			EXPECT_EQ(mirror.mirror(val, jump, shift, Type::min(), Type::max()), vals[c]);
		}
	}
}

TEST(MirrorUnsignedTest, smallRange_2) {
	using Type = coretools::UnsignedInt8WithMax<0>;
	Type::setMax(2);
	MirrorUnsigned<Type> mirror;

	std::vector<size_t> vals = {1, 0, 0, 0, 0, 1, 0, 1, 2, 1, 2, 2, 2, 2, 1};

	const size_t shift = 2;

	size_t c = 0;
	for (size_t jump = 0; jump <= 2 * coretools::UnsignedInt8WithMax<0>::max().get(); jump++) {
		for (size_t val = 0; val <= coretools::UnsignedInt8WithMax<0>::max().get(); ++val, ++c) {
			EXPECT_EQ(mirror.mirror(val, jump, shift, Type::min(), Type::max()), vals[c]);
		}
	}
}

TEST(MirrorUnsignedTest, smallRange_3) {
	using Type = coretools::UnsignedInt8WithMax<0>;
	Type::setMax(3);
	MirrorUnsigned<Type> mirror;

	std::vector<size_t> vals = {2, 1, 0, 0, 1, 0, 0, 1, 0, 0, 1, 2, 0, 1, 2, 3, 1, 2, 3, 3, 2, 3, 3, 2, 3, 3, 2, 1};

	const size_t shift = 3;

	size_t c = 0;
	for (size_t jump = 0; jump <= 2 * coretools::UnsignedInt8WithMax<0>::max().get(); jump++) {
		for (size_t val = 0; val <= coretools::UnsignedInt8WithMax<0>::max().get(); ++val, ++c) {
			EXPECT_EQ(mirror.mirror(val, jump, shift, Type::min(), Type::max()), vals[c]);
		}
	}
}
