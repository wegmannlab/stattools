//
// Created by caduffm on 7/1/21.
//

#include <cstddef>
#include <string>

#include "gtest/gtest.h"

#include "coretools/Types/commonWeakTypes.h"
#include "coretools/Types/probability.h"
#include "stattools/Updates/TUpdate.h"

using namespace stattools;
using namespace coretools;
using namespace testing;

//----------------------------------------
// TUpdateTypedBase
//----------------------------------------

class TUpdateTypedBaseTest : public Test {
public:
	size_t size      = 5;
	std::string name = "param";
	bool isUpdated   = true;
};

TEST_F(TUpdateTypedBaseTest, isUpdated) {
	// not updated
	TUpdateShared<coretools::Unbounded> updater(
		false, std::make_unique<TPropKernelNormal<coretools::Unbounded, double>>(), name);
	EXPECT_FALSE(updater.isUpdated());
	EXPECT_EQ(updater.proposalKernelName(), ProposalKernel::missing);

	// isUpdated
	TUpdateShared<coretools::Unbounded> updater2(
		true, std::make_unique<TPropKernelNormal<coretools::Unbounded, double>>(), name);
	EXPECT_TRUE(updater2.isUpdated());
	EXPECT_EQ(updater2.proposalKernelName(), ProposalKernel::normal);
}

TEST_F(TUpdateTypedBaseTest, initialize_double) {
	// double, normal proposal kernel
	TUpdateShared<coretools::Unbounded> updater(
		true, std::make_unique<TPropKernelNormal<coretools::Unbounded, double>>(), name);
	EXPECT_EQ(updater.proposalKernelName(), ProposalKernel::normal);

	// double, uniform proposal kernel
	TUpdateShared<coretools::Unbounded> updater2(
		true, std::make_unique<TPropKernelUniform<coretools::Unbounded, double>>(), name);
	EXPECT_EQ(updater2.proposalKernelName(), ProposalKernel::uniform);
}

TEST_F(TUpdateTypedBaseTest, initialize_int) {
	// integer, integer proposal kernel
	TUpdateShared<coretools::Integer> updater(true, std::make_unique<TPropKernelInteger<coretools::Integer, int>>(),
											  name);
	EXPECT_EQ(updater.proposalKernelName(), ProposalKernel::integer);

	// ints always have shared proposal widths
	using T = TUpdateUnique<coretools::Integer, true>;
	EXPECT_ANY_THROW(T updater5(size, true, std::make_unique<TPropKernelInteger<coretools::Integer, int>>(), name));
	EXPECT_ANY_THROW(T updater6(size, true, std::make_unique<TPropKernelInteger<coretools::Integer, int>>(), name));
}

TEST_F(TUpdateTypedBaseTest, initialize_bool) {
	// boolean, boolean proposal kernel
	TUpdateShared<coretools::Boolean> updater(true, std::make_unique<TPropKernelBool<coretools::Boolean>>(), name);
	EXPECT_EQ(updater.proposalKernelName(), ProposalKernel::boolean);

	// ints always have shared proposal widths
	using T = TUpdateUnique<coretools::Boolean, true>;
	EXPECT_ANY_THROW(T updater5(size, true, std::make_unique<TPropKernelBool<coretools::Boolean>>(), name));
	EXPECT_ANY_THROW(T updater6(size, true, std::make_unique<TPropKernelBool<coretools::Boolean>>(), name));
}

TEST_F(TUpdateTypedBaseTest, setProposalWidth_double_default) {
	TUpdateShared<coretools::Unbounded> updater(
		isUpdated, std::make_unique<TPropKernelNormal<coretools::Unbounded, double>>(), name);
	EXPECT_FLOAT_EQ(updater.proposalWidth(0), 0.1);

	TUpdateUnique<coretools::Unbounded, true> updater2(
		size, isUpdated, std::make_unique<TPropKernelNormal<coretools::Unbounded, double>>(), name);
	for (size_t i = 0; i < size; i++) { EXPECT_FLOAT_EQ(updater2.proposalWidth(i), 0.1); }

	TUpdateUnique<coretools::Unbounded, false> updater3(
		size, isUpdated, std::make_unique<TPropKernelNormal<coretools::Unbounded, double>>(), name);
	for (size_t i = 0; i < size; i++) { EXPECT_FLOAT_EQ(updater3.proposalWidth(i), 0.1); }
}

//----------------------------------------
// TUpdateSharedTest
//----------------------------------------

class TUpdateSharedTest : public Test {
public:
	size_t size      = 5;
	std::string name = "param";
	bool isUpdated   = true;
};

TEST_F(TUpdateSharedTest, setProposalWidth_double) {
	TUpdateShared<coretools::Unbounded> updater(
		isUpdated, std::make_unique<TPropKernelNormal<coretools::Unbounded, double>>(), name);

	EXPECT_NO_THROW(updater.setProposalWidth("1."));
	EXPECT_EQ(coretools::str::fromString<double>(updater.getProposalWidthAsString(0)), 1.);

	EXPECT_NO_THROW(updater.setProposalWidth("               1.  "));
	EXPECT_EQ(coretools::str::fromString<double>(updater.getProposalWidthAsString(0)), 1.);

	// proposal width can not be negative
	EXPECT_ANY_THROW(updater.setProposalWidth("-1"));
	// proposal width can not be a filename or a vector
	EXPECT_ANY_THROW(updater.setProposalWidth("j"));
	EXPECT_ANY_THROW(updater.setProposalWidth("vals.txt"));
	EXPECT_ANY_THROW(updater.setProposalWidth("1,1,1,1,1"));
}

TEST_F(TUpdateSharedTest, setProposalWidth_WithInterval) {
	TUpdateShared<coretools::Probability> updater(
		isUpdated, std::make_unique<TPropKernelNormal<coretools::Probability, double>>(), name);

	EXPECT_NO_THROW(updater.setProposalWidth("1."));
	EXPECT_EQ(coretools::str::fromString<double>(updater.getProposalWidthAsString(0)), 0.5); // restricted to range/2
}

TEST_F(TUpdateSharedTest, proposeBool) {
	TUpdateShared<coretools::Boolean> updater(isUpdated, std::make_unique<TPropKernelBool<coretools::Boolean>>(), name);
	updater.setProposalWidth("1");

	EXPECT_EQ(updater.propose(1, 0), 0);
	EXPECT_EQ(updater.propose(0, 0), 1);
}

TEST_F(TUpdateSharedTest, acceptanceRate) {
	TUpdateShared<coretools::Unbounded> updater(
		isUpdated, std::make_unique<TPropKernelUniform<coretools::Unbounded, double>>(), name);
	updater.setProposalWidth("1.");

	// nothing happened yet
	EXPECT_EQ(updater.acceptanceRate(0), 1.);

	// propose once and reject
	updater.propose(0., 0);
	updater.reject(0);
	EXPECT_FLOAT_EQ(updater.acceptanceRate(0), 0.5); // not zero on purpose (could not adjust proposal kernel)

	// propose again and accept
	updater.propose(0., 0);
	EXPECT_FLOAT_EQ(updater.acceptanceRate(0), 2. / 3.);

	// propose again and reject
	updater.propose(0., 0);
	updater.reject(0);
	EXPECT_FLOAT_EQ(updater.acceptanceRate(0), 2. / 4.);

	// clear counters
	updater.clear();
	EXPECT_EQ(updater.acceptanceRate(0), 1.);
}

TEST_F(TUpdateSharedTest, adjustJumpSize) {
	TUpdateShared<coretools::Unbounded> updater(
		isUpdated, std::make_unique<TPropKernelUniform<coretools::Unbounded, double>>(), name);
	updater.setProposalWidth("0.1");

	// nothing happened yet
	EXPECT_EQ(coretools::str::fromString<double>(updater.getProposalWidthAsString(0)), 0.1);

	// propose once and reject
	updater.propose(0., 0);
	updater.reject(0);
	updater.adjustProposalWidth();
	EXPECT_FLOAT_EQ(coretools::str::fromString<double>(updater.getProposalWidthAsString(0)), 0.113636);

	// propose again and accept
	updater.propose(0., 0);
	updater.adjustProposalWidth();
	EXPECT_FLOAT_EQ(coretools::str::fromString<double>(updater.getProposalWidthAsString(0)), 0.172176);
}

//-----------------------------------
// TUpdateUniqueBase
//-----------------------------------

class TUpdateUniqueTest : public Test {
public:
	size_t size      = 5;
	std::string name = "param";
	bool isUpdated   = true;
};

TEST_F(TUpdateUniqueTest, setProposalWidth_double) {
	TUpdateUnique<coretools::Unbounded, true> updater(
		size, isUpdated, std::make_unique<TPropKernelNormal<coretools::Unbounded, double>>(), name);
	EXPECT_NO_THROW(updater.setProposalWidth("1."));
	EXPECT_EQ(coretools::str::fromString<double>(updater.getProposalWidthAsString(0)), 1.);

	TUpdateUnique<coretools::Unbounded, true> updater2(
		size, isUpdated, std::make_unique<TPropKernelNormal<coretools::Unbounded, double>>(), name);
	EXPECT_NO_THROW(updater2.setProposalWidth("               1.  "));
	EXPECT_EQ(coretools::str::fromString<double>(updater2.getProposalWidthAsString(0)), 1.);

	// proposal width can be a filename or a vector
	TUpdateUnique<coretools::Unbounded, true> updater3(
		size, isUpdated, std::make_unique<TPropKernelNormal<coretools::Unbounded, double>>(), name);
	EXPECT_NO_THROW(updater3.setProposalWidth("1,2,3,4,5"));
	EXPECT_EQ(coretools::str::fromString<double>(updater3.getProposalWidthAsString(0)), 1.);
	EXPECT_EQ(coretools::str::fromString<double>(updater3.getProposalWidthAsString(1)), 2.);
	EXPECT_EQ(coretools::str::fromString<double>(updater3.getProposalWidthAsString(2)), 3.);
	EXPECT_EQ(coretools::str::fromString<double>(updater3.getProposalWidthAsString(3)), 4.);
	EXPECT_EQ(coretools::str::fromString<double>(updater3.getProposalWidthAsString(4)), 5.);

	// ... will not test file, as this is part of TReadInitialValues

	// proposal width can not be negative or letter
	TUpdateUnique<coretools::Unbounded, true> updater4(
		size, isUpdated, std::make_unique<TPropKernelNormal<coretools::Unbounded, double>>(), name);
	EXPECT_ANY_THROW(updater4.setProposalWidth("-1"));

	TUpdateUnique<coretools::Unbounded, true> updater5(
		size, isUpdated, std::make_unique<TPropKernelNormal<coretools::Unbounded, double>>(), name);
	EXPECT_ANY_THROW(updater5.setProposalWidth("j"));
}

TEST_F(TUpdateUniqueTest, setProposalWidth_WithInterval) {
	TUpdateUnique<coretools::Probability, true> updater(
		size, isUpdated, std::make_unique<TPropKernelNormal<coretools::Probability, double>>(), name);

	EXPECT_NO_THROW(updater.setProposalWidth("0.1, 0.3, 0.5, 1., 10."));
	EXPECT_EQ(coretools::str::fromString<double>(updater.getProposalWidthAsString(0)), 0.1);
	EXPECT_EQ(coretools::str::fromString<double>(updater.getProposalWidthAsString(1)), 0.3);
	EXPECT_EQ(coretools::str::fromString<double>(updater.getProposalWidthAsString(2)), 0.5);
	EXPECT_EQ(coretools::str::fromString<double>(updater.getProposalWidthAsString(3)), 0.5);
	EXPECT_EQ(coretools::str::fromString<double>(updater.getProposalWidthAsString(4)), 0.5);
}

TEST_F(TUpdateUniqueTest, acceptanceRate_equallyOften) {
	TUpdateUnique<coretools::Unbounded, true> updater(
		size, isUpdated, std::make_unique<TPropKernelUniform<coretools::Unbounded, double>>(), name);
	updater.setProposalWidth("1.");

	// nothing happened yet
	EXPECT_EQ(updater.meanAcceptanceRate(), 0.);

	// propose once and reject all uneven elements
	updater.propose(0., 0);
	updater.propose(0., 1);
	updater.propose(0., 2);
	updater.propose(0., 3);
	updater.propose(0., 4);

	updater.reject(1);
	updater.reject(3);

	EXPECT_FLOAT_EQ(updater.acceptanceRate(0), 1.);
	EXPECT_FLOAT_EQ(updater.acceptanceRate(1), 0.5);
	EXPECT_FLOAT_EQ(updater.acceptanceRate(2), 1.);
	EXPECT_FLOAT_EQ(updater.acceptanceRate(3), 0.5);
	EXPECT_FLOAT_EQ(updater.acceptanceRate(4), 1.);
	EXPECT_FLOAT_EQ(updater.meanAcceptanceRate(), 4. / 5.);

	updater.adjustProposalWidth();
	EXPECT_EQ(coretools::str::fromString<double>(updater.getProposalWidthAsString(0)), 2.);
	EXPECT_EQ(coretools::str::fromString<double>(updater.getProposalWidthAsString(1)), 1.13636);
	EXPECT_EQ(coretools::str::fromString<double>(updater.getProposalWidthAsString(2)), 2.);
	EXPECT_EQ(coretools::str::fromString<double>(updater.getProposalWidthAsString(3)), 1.13636);
	EXPECT_EQ(coretools::str::fromString<double>(updater.getProposalWidthAsString(4)), 2.);

	// propose again and reject all uneven elements
	updater.propose(0., 0);
	updater.propose(0., 1);
	updater.propose(0., 2);
	updater.propose(0., 3);
	updater.propose(0., 4);

	updater.reject(1);
	updater.reject(3);

	EXPECT_FLOAT_EQ(updater.acceptanceRate(0), 1.);
	EXPECT_FLOAT_EQ(updater.acceptanceRate(1), 1. / 3.);
	EXPECT_FLOAT_EQ(updater.acceptanceRate(2), 1.);
	EXPECT_FLOAT_EQ(updater.acceptanceRate(3), 1. / 3.);
	EXPECT_FLOAT_EQ(updater.acceptanceRate(4), 1.);
	EXPECT_FLOAT_EQ(updater.meanAcceptanceRate(), 3.6666666 / 5.);

	updater.adjustProposalWidth();
	EXPECT_FLOAT_EQ(coretools::str::fromString<double>(updater.getProposalWidthAsString(0)), 4.);
	EXPECT_FLOAT_EQ(coretools::str::fromString<double>(updater.getProposalWidthAsString(1)), 0.86088198);
	EXPECT_FLOAT_EQ(coretools::str::fromString<double>(updater.getProposalWidthAsString(2)), 4.);
	EXPECT_FLOAT_EQ(coretools::str::fromString<double>(updater.getProposalWidthAsString(3)), 0.86088198);
	EXPECT_FLOAT_EQ(coretools::str::fromString<double>(updater.getProposalWidthAsString(4)), 4.);

	// propose again and reject all even elements
	updater.propose(0., 0);
	updater.propose(0., 1);
	updater.propose(0., 2);
	updater.propose(0., 3);
	updater.propose(0., 4);

	updater.reject(0);
	updater.reject(2);
	updater.reject(4);

	EXPECT_FLOAT_EQ(updater.acceptanceRate(0), 3. / 4.);
	EXPECT_FLOAT_EQ(updater.acceptanceRate(1), 2. / 4.);
	EXPECT_FLOAT_EQ(updater.acceptanceRate(2), 3. / 4.);
	EXPECT_FLOAT_EQ(updater.acceptanceRate(3), 2. / 4.);
	EXPECT_FLOAT_EQ(updater.acceptanceRate(4), 3. / 4.);
	EXPECT_FLOAT_EQ(updater.meanAcceptanceRate(), 3.25 / 5.);

	updater.adjustProposalWidth();
	EXPECT_FLOAT_EQ(coretools::str::fromString<double>(updater.getProposalWidthAsString(0)), 6.8181801);
	EXPECT_FLOAT_EQ(coretools::str::fromString<double>(updater.getProposalWidthAsString(1)), 0.97827399);
	EXPECT_FLOAT_EQ(coretools::str::fromString<double>(updater.getProposalWidthAsString(2)), 6.8181801);
	EXPECT_FLOAT_EQ(coretools::str::fromString<double>(updater.getProposalWidthAsString(3)), 0.97827399);
	EXPECT_FLOAT_EQ(coretools::str::fromString<double>(updater.getProposalWidthAsString(4)), 6.8181801);

	// clear counters
	updater.clear();
	EXPECT_EQ(updater.acceptanceRate(0), 1.);
	EXPECT_EQ(updater.acceptanceRate(1), 1.);
	EXPECT_EQ(updater.acceptanceRate(2), 1.);
	EXPECT_EQ(updater.acceptanceRate(3), 1.);
	EXPECT_EQ(updater.acceptanceRate(4), 1.);
	EXPECT_FLOAT_EQ(updater.meanAcceptanceRate(), 0.);
}

TEST_F(TUpdateUniqueTest, acceptanceRate_notEquallyOften) {
	TUpdateUnique<coretools::Unbounded, false> updater(
		size, isUpdated, std::make_unique<TPropKernelUniform<coretools::Unbounded, double>>(), name);
	updater.setProposalWidth("1.");

	// nothing happened yet
	EXPECT_EQ(updater.meanAcceptanceRate(), 0.);

	// just update third element as an example

	// propose once and reject
	updater.propose(0., 3);
	updater.reject(3);
	EXPECT_FLOAT_EQ(updater.acceptanceRate(3),
					0.5); // not zero on purpose (could not adjust proposal kernel)
	EXPECT_FLOAT_EQ(updater.meanAcceptanceRate(), 0.5);

	updater.adjustProposalWidth();
	EXPECT_FLOAT_EQ(coretools::str::fromString<double>(updater.getProposalWidthAsString(3)), 1.13636);

	// propose again and accept
	updater.propose(0., 3);
	EXPECT_FLOAT_EQ(updater.acceptanceRate(3), 2. / 3.);
	EXPECT_FLOAT_EQ(updater.meanAcceptanceRate(), 2. / 3.);
	updater.adjustProposalWidth();
	EXPECT_FLOAT_EQ(coretools::str::fromString<double>(updater.getProposalWidthAsString(3)), 1.72176);

	// propose again and reject
	updater.propose(0., 3);
	updater.reject(3);
	EXPECT_FLOAT_EQ(updater.acceptanceRate(3), 2. / 4.);
	EXPECT_FLOAT_EQ(updater.meanAcceptanceRate(), 2. / 4.);
	updater.adjustProposalWidth();
	EXPECT_FLOAT_EQ(coretools::str::fromString<double>(updater.getProposalWidthAsString(3)), 1.95655);

	// clear counters
	updater.clear();
	EXPECT_EQ(updater.acceptanceRate(3), 1.);
	EXPECT_FLOAT_EQ(updater.meanAcceptanceRate(), 0.);
}
