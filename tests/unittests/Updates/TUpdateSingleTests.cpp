//
// Created by madleina on 05.12.23.
//

#include "stattools/Updates/TUpdateSingle.h"
#include "gtest/gtest.h"
#include <vector>

#ifdef _OPENMP
#include "omp.h"
#endif

using namespace testing;
using namespace stattools;
using namespace coretools::instances;

//--------------------------------------
// TUpdateSingle
//-------------------------------------
#ifdef _OPENMP

TEST(TUpdateSingle, pick) {
	coretools::TDimension<1> dim({10});
	size_t numThreads = 4;

	TMarkovOrder<1> markov({0}, dim.dimensions());
	EXPECT_EQ(markov.getNumPickers(), 1);
	TUpdateSingle<1, stattools::UpdateWeights::regular> weights(markov, 0, numThreads);

	std::vector<size_t> counts(dim.size(), 0);
#pragma omp parallel for num_threads(numThreads) schedule(static)
	for (size_t i = 0; i < dim.size(); ++i) {
		auto ix = weights.pick(i, omp_get_thread_num());
		++counts[ix.begin];
	}

	for (size_t l = 0; l < dim.size(); l++) { EXPECT_EQ(counts[l], 1); }
}
#endif

TEST(TUpdateSingle, pick_HMM) {
	coretools::TDimension<1> dim({11});
	size_t numThreads = 1;

	TMarkovOrder<1> markov({1}, dim.dimensions());
	EXPECT_EQ(markov.getNumPickers(), 2);
	TUpdateSingle<1, stattools::UpdateWeights::regular> weights_even(markov, 0, numThreads);
	TUpdateSingle<1, stattools::UpdateWeights::regular> weights_odd(markov, 1, numThreads);

	// even
	size_t c = 0;
	for (size_t i = 0; i < weights_even.numUpdates(); ++i, c += 2) {
		const auto range = weights_even.pick(i, 0);
		for (size_t r = range.begin; r < range.end; r += range.increment) { EXPECT_EQ(r, c); } // should be 1 element
	}

	// odd
	c = 1;
	for (size_t i = 0; i < weights_odd.numUpdates(); ++i, c += 2) {
		const auto range = weights_odd.pick(i, 0);
		for (size_t r = range.begin; r < range.end; r += range.increment) { EXPECT_EQ(r, c); } // should be 1 element
	}
}

//--------------------------------------
// TUpdateSingleWeighted
//-------------------------------------

std::vector<double> getStats() {
	return {0.2655086631421,   0.37212389963679,  0.572853363351896, 0.908207789994776, 0.201681931037456,
			0.898389684967697, 0.944675268605351, 0.660797792486846, 0.62911404389888,  0.0617862704675645};
}

TEST(TUpdateSingleWeighted, threads_1_size_1) {
	coretools::TDimension<1> dim({1});

	TMarkovOrder<1> markov({0}, dim.dimensions());
	EXPECT_EQ(markov.getNumPickers(), 1);

	size_t numThreads         = 1;
	std::vector<double> stats = {1.0};
	TUpdateSingleWeighted<1, stattools::UpdateWeights::irregular> weights(markov, 0, numThreads);
	weights.setWeights({stats}, {}, 1.0);

	// first thread
	auto cumW = weights.cumWeights(0);
	EXPECT_EQ(cumW.size(), 1);
	EXPECT_FLOAT_EQ(cumW[0], 1.0);
	EXPECT_EQ(weights.numElementsLeftOf(0), 0);
}

TEST(TUpdateSingleWeighted, threads_1_size_10) {
	coretools::TDimension<1> dim({10});

	TMarkovOrder<1> markov({0}, dim.dimensions());
	EXPECT_EQ(markov.getNumPickers(), 1);

	size_t numThreads         = 1;
	std::vector<double> stats = getStats();
	TUpdateSingleWeighted<1, stattools::UpdateWeights::irregular> weights(markov, 0, numThreads);
	weights.setWeights({stats}, {}, 1.0);

	// first thread
	auto cumW = weights.cumWeights(0);
	EXPECT_EQ(cumW.size(), 10);
	std::vector<double> expected = {
		0.0481417924769027, 0.115614963935802, 0.219484221578152, 0.384159642840902, 0.420728429544294,
		0.583623640816393,  0.75491131256713,  0.874726575160168, 0.98879696889898,  1};
	for (size_t i = 0; i < cumW.size(); ++i) { EXPECT_FLOAT_EQ(cumW[i], expected[i]); }
	EXPECT_EQ(weights.numElementsLeftOf(0), 0);
}

TEST(TUpdateSingleWeighted, threads_3_size_10) {
	coretools::TDimension<1> dim({10});

	TMarkovOrder<1> markov({0}, dim.dimensions());
	EXPECT_EQ(markov.getNumPickers(), 1);

	size_t numThreads         = 3;
	std::vector<double> stats = getStats();
	TUpdateSingleWeighted<1, stattools::UpdateWeights::irregular> weights(markov, 0, numThreads);
	weights.setWeights({stats}, {}, 1.0);

	// first thread
	auto cumW = weights.cumWeights(0);
	EXPECT_EQ(cumW.size(), 4);
	std::vector<double> expected = {0.125317152319512, 0.300955517036659, 0.57133596844021, 1};
	for (size_t i = 0; i < cumW.size(); ++i) { EXPECT_FLOAT_EQ(cumW[i], expected[i]); }
	EXPECT_EQ(weights.numElementsLeftOf(0), 0);

	// second thread
	cumW = weights.cumWeights(1);
	EXPECT_EQ(cumW.size(), 2);
	expected = {0.18333527390685, 1};
	for (size_t i = 0; i < cumW.size(); ++i) { EXPECT_FLOAT_EQ(cumW[i], expected[i]); }
	EXPECT_EQ(weights.numElementsLeftOf(1), 4);

	// third thread
	cumW = weights.cumWeights(2);
	EXPECT_EQ(cumW.size(), 4);
	expected = {0.411377034197096, 0.69913415573003, 0.973093978911324, 1};
	for (size_t i = 0; i < cumW.size(); ++i) { EXPECT_FLOAT_EQ(cumW[i], expected[i]); }
	EXPECT_EQ(weights.numElementsLeftOf(2), 6);
}

TEST(TUpdateSingleWeighted, threads_10_size_10) {
	coretools::TDimension<1> dim({10});

	TMarkovOrder<1> markov({0}, dim.dimensions());
	EXPECT_EQ(markov.getNumPickers(), 1);

	size_t numThreads         = 10;
	std::vector<double> stats = getStats();
	TUpdateSingleWeighted<1, stattools::UpdateWeights::irregular> weights(markov, 0, numThreads);
	weights.setWeights({stats}, {}, 1.0);

	for (size_t t = 0; t < numThreads; ++t) {
		auto cumW = weights.cumWeights(t);
		EXPECT_EQ(cumW.size(), 1);
		EXPECT_EQ(cumW[0], 1.0);
		EXPECT_EQ(weights.numElementsLeftOf(t), t);
	}
}

TEST(TUpdateSingleWeighted, threads_9_size_10_skewed_right) {
	coretools::TDimension<1> dim({10});

	TMarkovOrder<1> markov({0}, dim.dimensions());
	EXPECT_EQ(markov.getNumPickers(), 1);

	size_t numThreads         = 9;
	std::vector<double> stats = {0.001, 0.001, 0.001, 0.001, 0.001, 0.001, 0.001, 0.001, 0.001, 0.991};
	TUpdateSingleWeighted<1, stattools::UpdateWeights::irregular> weights(markov, 0, numThreads);
	weights.setWeights({stats}, {}, 1.0);

	// first thread: gets two
	auto cumW = weights.cumWeights(0);
	EXPECT_EQ(cumW.size(), 2);
	std::vector<double> expected = {0.5, 1.0};
	for (size_t i = 0; i < cumW.size(); ++i) { EXPECT_FLOAT_EQ(cumW[i], expected[i]); }
	EXPECT_EQ(weights.numElementsLeftOf(0), 0);

	// all other threads: get one
	for (size_t t = 1; t < numThreads; ++t) {
		cumW = weights.cumWeights(t);
		EXPECT_EQ(cumW.size(), 1);
		EXPECT_EQ(cumW[0], 1.0);
		EXPECT_EQ(weights.numElementsLeftOf(t), t + 1);
	}
}

TEST(TUpdateSingleWeighted, threads_9_size_10_skewed_left) {
	coretools::TDimension<1> dim({10});

	TMarkovOrder<1> markov({0}, dim.dimensions());
	EXPECT_EQ(markov.getNumPickers(), 1);

	size_t numThreads         = 9;
	std::vector<double> stats = {0.991, 0.001, 0.001, 0.001, 0.001, 0.001, 0.001, 0.001, 0.001, 0.001};
	TUpdateSingleWeighted<1, stattools::UpdateWeights::irregular> weights(markov, 0, numThreads);
	weights.setWeights({stats}, {}, 1.0);

	// all but last thread: get one
	for (size_t t = 0; t < numThreads - 1; ++t) {
		auto cumW = weights.cumWeights(t);
		EXPECT_EQ(cumW.size(), 1);
		EXPECT_EQ(cumW[0], 1.0);
		EXPECT_EQ(weights.numElementsLeftOf(t), t);
	}

	// last thread: get two
	auto cumW = weights.cumWeights(numThreads - 1);
	EXPECT_EQ(cumW.size(), 2);
	std::vector<double> expected = {0.5, 1.0};
	for (size_t i = 0; i < cumW.size(); ++i) { EXPECT_FLOAT_EQ(cumW[i], expected[i]); }
	EXPECT_EQ(weights.numElementsLeftOf(numThreads - 1), 8);
}

#ifdef _OPENMP
TEST(TUpdateSingleWeighted, pick) {
	coretools::TDimension<1> dim({10});
	size_t numThreads = 2;

	TMarkovOrder<1> markov({0}, dim.dimensions());
	EXPECT_EQ(markov.getNumPickers(), 1);
	// numbers chosen such that cumulative at element 5 (middle) is exactly 0.5 -> two threads have exactly the
	// same cumulative weight -> should result in the same weights as input
	std::vector<double> stats = {0.0885866908243853, 0.116241357997899, 0.11537695711476,  0.102837260125396,
								 0.0769577339375602, 0.104776257992376, 0.108418680172836, 0.106051829034795,
								 0.0936754372652141, 0.0870777955347789};
	TUpdateSingleWeighted<1, stattools::UpdateWeights::irregular> weights(markov, 0, numThreads);
	weights.setWeights({stats}, {}, 1.0);

	size_t numRep = 1e7;
	std::vector<size_t> counts(dim.size(), 0);
	std::vector<size_t> threadcount(numThreads, 0);
#pragma omp parallel for num_threads(numThreads) schedule(static)
	for (size_t i = 0; i < numRep; ++i) {
		auto ix = weights.pick(i, omp_get_thread_num());
		++counts[ix.begin];
		++threadcount[omp_get_thread_num()];
	}

	// check if these are approximately the weights
	for (size_t l = 0; l < dim.size(); l++) { EXPECT_NEAR((double)counts[l] / numRep, stats[l], 0.001); }
	for (size_t l = 0; l < numThreads; l++) { EXPECT_EQ(threadcount[l], numRep / numThreads); }
}
#endif

#ifdef _OPENMP
TEST(TUpdateSingleWeighted, pick_HMM) {
	// chosen such that each thread (for even and odd) have same cumulative weights
	// should result in the same weights as input

	std::vector<double> stats = {0.109670280922132, 0.226115894850442, 0.153708478390266, 0.0502126174363267,
								 0.236621240687602, 0.223671487713231, 0.211375798798661, 0.0695293982954575,
								 0.147856798916211, 0.231787550025846, 0.140767402285129, 0.198683051678697};

	coretools::TDimension<1> dim({12});
	size_t numThreads = 2;

	TMarkovOrder<1> markov({1}, dim.dimensions());
	EXPECT_EQ(markov.getNumPickers(), 2);

	std::array<TUpdateSingleWeighted<1, stattools::UpdateWeights::irregular>, 2> weights;
	for (size_t p = 0; p < 2; ++p) {
		weights[p].initialize(markov, p, numThreads);
		weights[p].setWeights({stats}, {}, 1.0);
	}

	size_t numRep = 1e6;
	std::vector<size_t> counts(dim.size(), 0);
#pragma omp parallel for num_threads(numThreads) schedule(static)
	for (size_t rep = 0; rep < numRep; ++rep) {
		for (size_t p = 0; p < 2; ++p) {
			for (size_t i = 0; i < weights[p].numUpdates(); ++i) {
				const auto range = weights[p].pick(i, omp_get_thread_num());
				size_t c         = 0;
				for (size_t r = range.begin; r < range.end; r += range.increment) {
					++counts[r];
					++c;
				}
				EXPECT_EQ(c, 1); // should be 1 element per range
			}
		}
	}

	// check if these are approximately the weights
	for (size_t l = 0; l < dim.size(); l++) { EXPECT_NEAR((double)counts[l] / (numRep * 6), stats[l], 0.001); }
}
#endif
