//
// Created by caduffm on 12/22/22.
//

#include "stattools/Updates/TLogHCalculator.h"
#include "gmock/gmock.h"
#include "gtest/gtest.h"
#include "coretools/Distributions/TNormalDistr.h"

//--------------------------------------
// Common Functions
//--------------------------------------

class TLogHCalculator : public testing::Test {
public:
	static constexpr size_t N = 21;
	const double newMu        = 0.1;
	const double oldMu        = 0.0;
	const double priorLogH    = 0.5;

	std::array<double, N> y = {-0.626453810742332,  0.183643324222082,  -0.835628612410047, 1.59528080213779,
							   0.329507771815361,   -0.820468384118015, 0.487429052428485,  0.738324705129217,
							   0.575781351653492,   -0.305388387156356, 1.51178116845085,   0.389843236411431,
							   -0.621240580541804,  -2.2146998871775,   1.12493091814311,   -0.0449336090152309,
							   -0.0161902630989461, 0.943836210685299,  0.821221195098089,  0.593901321217509,
							   0.918977371608218};

	double calculateLLRatio(size_t i, const coretools::TRange & /*IxParameters*/) {
		return coretools::probdist::TNormalDistr::logDensity(y[i], newMu, 1.0) -
			   coretools::probdist::TNormalDistr::logDensity(y[i], oldMu, 1.0);
	}

	double calculateLikRatio(size_t i, const coretools::TRange & /*IxParameters*/) {
		return coretools::probdist::TNormalDistr::density(y[i], newMu, 1.0) /
			   coretools::probdist::TNormalDistr::density(y[i], oldMu, 1.0);
	}
};

TEST_F(TLogHCalculator, sumLogH_log) {
	// whole range (begin until end)
	auto ptr   = &TLogHCalculator::calculateLLRatio;
	double sum = impl::sumLogH<true>(0, N, *this, ptr, coretools::TRange(0), priorLogH);
	EXPECT_FLOAT_EQ(sum, 0.8679455);

	// begin until middle
	sum = impl::sumLogH<true>(0, 10, *this, ptr, coretools::TRange(0), priorLogH);
	EXPECT_FLOAT_EQ(sum, 0.5822028);

	// middle until end
	sum = impl::sumLogH<true>(10, N, *this, ptr, coretools::TRange(0), priorLogH);
	EXPECT_FLOAT_EQ(sum, 0.7857427);

	// something in between
	sum = impl::sumLogH<true>(10, 15, *this, ptr, coretools::TRange(0), priorLogH);
	EXPECT_FLOAT_EQ(sum, 0.4940615);
}

TEST_F(TLogHCalculator, sumLogH_noLog) {
	// whole range (begin until end)
	auto ptr   = &TLogHCalculator::calculateLikRatio;
	double sum = impl::sumLogH<false>(0, N, *this, ptr, coretools::TRange(0), priorLogH);
	EXPECT_FLOAT_EQ(sum, 0.8679455);

	// begin until middle
	sum = impl::sumLogH<false>(0, 10, *this, ptr, coretools::TRange(0), priorLogH);
	EXPECT_FLOAT_EQ(sum, 0.5822028);

	// middle until end
	sum = impl::sumLogH<false>(10, N, *this, ptr, coretools::TRange(0), priorLogH);
	EXPECT_FLOAT_EQ(sum, 0.7857427);

	// something in between
	sum = impl::sumLogH<false>(10, 15, *this, ptr, coretools::TRange(0), priorLogH);
	EXPECT_FLOAT_EQ(sum, 0.4940615);
}

TEST_F(TLogHCalculator, acceptOrReject) {
	const size_t numRep = 10000;

	size_t counter = 0;
	for (size_t i = 0; i < numRep; ++i) {
		if (evalLogH(log(0.2))) { ++counter; }
	}
	EXPECT_NEAR((double)counter / (double)numRep, 0.2, 0.01);
}

//--------------------------------------
// TLogHCalculatorRegular
//--------------------------------------

TEST_F(TLogHCalculator, TLogHCalculatorRegular) {
	TLogHCalculatorRegular logHCalculator{};
	logHCalculator.initialize(0, 0.0, 0.0, 0.0);
	logHCalculator.setSizeData(N);

	auto ptr      = &TLogHCalculator::calculateLLRatio;
	bool accepted = logHCalculator.acceptUpdate<true>(*this, ptr, coretools::TRange(0), priorLogH);
	EXPECT_TRUE(accepted); // always accept since logH = 0.8679455 --> h = 2.382012 > 1

	// for a fixed logH
	const size_t numRep = 10000;
	size_t counter      = 0;
	for (size_t i = 0; i < numRep; ++i) {
		if (logHCalculator.acceptUpdate(log(0.2))) { ++counter; }
	}
	EXPECT_NEAR((double)counter / (double)numRep, 0.2, 0.01);
}

//--------------------------------------
// LogH Correlations
//--------------------------------------

class TLogHCorrelationTest : public testing::Test {
public:
	static constexpr size_t numIter   = 10;
	static constexpr size_t numBlocks = 5;

	std::array<double, numIter> totalLogH = {
		0.398105880367068, -0.612026393250771, 0.341119691424425, -1.12936309608079, 1.43302370170104,
		1.98039989850586,  -0.367221476466509, -1.04413462631653, 0.569719627442413, -0.135054603880824};
	std::array<double, numIter * numBlocks> blockLogH = {
		-0.626453810742332, 1.51178116845085,    0.918977371608218,   1.35867955152904,    -0.164523596253587,
		0.183643324222082,  0.389843236411431,   0.782136300731067,   -0.102787727342996,  -0.253361680136508,
		-0.835628612410047, -0.621240580541804,  0.0745649833651906,  0.387671611559369,   0.696963375404737,
		1.59528080213779,   -2.2146998871775,    -1.98935169586337,   -0.0538050405829051, 0.556663198673657,
		0.329507771815361,  1.12493091814311,    0.61982574789471,    -1.37705955682861,   -0.68875569454952,
		-0.820468384118015, -0.0449336090152309, -0.0561287395290008, -0.41499456329968,   -0.70749515696212,
		0.487429052428485,  -0.0161902630989461, -0.155795506705329,  -0.394289953710349,  0.36458196213683,
		0.738324705129217,  0.943836210685299,   -1.47075238389927,   -0.0593133967111857, 0.768532924515416,
		0.575781351653492,  0.821221195098089,   -0.47815005510862,   1.10002537198388,    -0.112346212150228,
		-0.305388387156356, 0.593901321217509,   0.417941560199702,   0.763175748457544,   0.881107726454215};

	TLogHCorrelation cor;

	void fillCorrelations() {
		cor.reserve(100, numBlocks);

		size_t l = 0;
		for (size_t i = 0; i < numIter; ++i) {
			cor.addTotalLogH(totalLogH[i]);
			for (size_t b = 0; b < numBlocks; ++b, ++l) { cor.addBlockLogH(blockLogH[l]); }
		}
		cor.finalize();
	}
};

//--------------------------------------
// TTotalLogH
//--------------------------------------

TEST_F(TLogHCorrelationTest, totalLogH) {
	TTotalLogH tot;
	tot.reserve(100);
	for (size_t i = 0; i < numIter; ++i) { tot.add(totalLogH[i]); }

	EXPECT_FLOAT_EQ(tot.y(), 1.434569);
	EXPECT_FLOAT_EQ(tot.y2(), 9.468318);
	EXPECT_FLOAT_EQ(tot.n(), numIter);
	for (size_t i = 0; i < numIter; ++i) { EXPECT_FLOAT_EQ(tot[i], totalLogH[i]); }
}

//--------------------------------------
// TLogHCorrelation
//--------------------------------------

TEST_F(TLogHCorrelationTest, calculateRPerBlock) {
	fillCorrelations();

	std::array<double, numBlocks> r_from_R = {-0.613362711250911, 0.330437866601923, 0.503398055661233,
											  -0.195224654448993, -0.735587722400498};
	for (size_t b = 0; b < numBlocks; ++b) { EXPECT_FLOAT_EQ(r_from_R[b], cor.calculateR(b)); }
}

TEST_F(TLogHCorrelationTest, getBlockIxWithHighestR) {
	fillCorrelations();
	EXPECT_EQ(cor.getBlockIxWithHighestR(), 2);
}

TEST_F(TLogHCorrelationTest, calculateRMergedBlock_2Blocks) {
	fillCorrelations();

	// all possible combinations with 2 blocks
	std::array<double, numBlocks * numBlocks> r_from_R = {
		-0.613362711250911,  -0.118246107664849,  0.00336494961784029, -0.651607888242465, -0.845870901747561,
		-0.118246107664849,  0.330437866601923,   0.459931777614816,   0.134891180588387,  -0.0820938031029407,
		0.00336494961784029, 0.459931777614816,   0.503398055661233,   0.248235368075141,  0.0497784443682451,
		-0.651607888242465,  0.134891180588387,   0.248235368075141,   -0.195224654448993, -0.515302155655937,
		-0.845870901747561,  -0.0820938031029407, 0.0497784443682451,  -0.515302155655937, -0.735587722400498};

	size_t l = 0;
	for (size_t b1 = 0; b1 < numBlocks; ++b1) {
		for (size_t b2 = 0; b2 < numBlocks; ++b2, ++l) { EXPECT_FLOAT_EQ(r_from_R[l], cor.calculateR({b1, b2})); }
	}
}

TEST_F(TLogHCorrelationTest, calculateRMergedBlock_3Blocks) {
	fillCorrelations();

	// all possible unique combinations with 3 blocks
	static constexpr size_t numCombi      = 10;
	std::array<double, numCombi> r_from_R = {
		0.239573000446269,  -0.212672881968023, -0.502273870397073, -0.161503615762622, -0.621454305686195,
		-0.790266609138588, 0.32337674056774,   0.24170488756812,   -0.158137943886712, -0.0845526915510191};

	std::vector<std::vector<size_t>> blockCombis = {{0, 1, 2}, {0, 1, 3}, {0, 1, 4}, {0, 2, 3}, {0, 2, 4},
													{0, 3, 4}, {1, 2, 3}, {1, 2, 4}, {1, 3, 4}, {2, 3, 4}};

	for (size_t b = 0; b < numCombi; ++b) { EXPECT_FLOAT_EQ(r_from_R[b], cor.calculateR(blockCombis[b])); }
}

TEST_F(TLogHCorrelationTest, calculateRMergedBlock_4Blocks) {
	fillCorrelations();

	// all possible unique combinations with 4 blocks
	static constexpr size_t numCombi      = 5;
	std::array<double, numCombi> r_from_R = {0.114564816316182, -0.0613558847933618, -0.482272057631633,
											 -0.521814522598821, 0.117683116627507};

	std::vector<std::vector<size_t>> blockCombis = {
		{0, 1, 2, 3}, {0, 1, 2, 4}, {0, 1, 3, 4}, {0, 2, 3, 4}, {1, 2, 3, 4}};

	for (size_t b = 0; b < numCombi; ++b) { EXPECT_FLOAT_EQ(r_from_R[b], cor.calculateR(blockCombis[b])); }
}

TEST_F(TLogHCorrelationTest, calculateRMergedBlock_5Blocks) {
	fillCorrelations();
	// note: in practice this will be 1.0, but for the test case this is not true
	EXPECT_FLOAT_EQ(cor.calculateR({0, 1, 2, 3, 4}), -0.140747749244151);
}

TEST_F(TLogHCorrelationTest, getBlockOrder) {
	fillCorrelations();
	EXPECT_THAT(cor.getBlockOrder(), testing::ElementsAre(2, 1, 3, 4, 0));
}

TEST_F(TLogHCorrelationTest, fitLinearModel) {
	fillCorrelations();

	std::array<double, numBlocks * 3> coeff_from_R = {
		0.214893286292375,  0.534410814183516, 0.929738394548425, 0.113837824893405,   0.25717279435324,
		0.955454827361494,  0.106469308928038, 0.156792065338087, 1.01820394687169,    0.121646220136009,
		0.0589415242566501, 1.06854103810566,  0.185440558639084, -0.0835926674128385, 1.06530684013112};
	const auto blockOrder = cor.getBlockOrder();
	for (size_t b = 0; b < numBlocks; ++b) {
		const auto end                       = blockOrder.begin() + (int)b + 1;
		std::vector<size_t> blocks           = std::vector<size_t>(blockOrder.begin(), end);
		const auto [intercept, slope, sigma] = cor.fitLinearModel(blocks);
		EXPECT_FLOAT_EQ(coeff_from_R[b * 3], intercept);
		EXPECT_FLOAT_EQ(coeff_from_R[b * 3 + 1], slope);
		EXPECT_FLOAT_EQ(coeff_from_R[b * 3 + 2], sigma);
	}
}

//--------------------------------------
// TLogHCalculatorBlocks
//--------------------------------------

class BridgeTLogHCalculatorBlocks : public TLogHCalculatorBlocks {
	// bridge class for testing protected functions
public:
	double _calculateLogProbPredLargerR(double r, double Pred_totalLogH, double Sigma) const {
		return TLogHCalculatorBlocks::_calculateLogProbPredLargerR(r, Pred_totalLogH, Sigma);
	}
	void _fitLinearModelLogH() { return TLogHCalculatorBlocks::_fitLinearModelLogH(); }

	void _setBlockEnds(double RelativeBlockSize) { return TLogHCalculatorBlocks::_setBlockEnds(RelativeBlockSize); }

	template<bool FuncCalculatesLogH, class Object, class Function>
	bool _calculateLogHRegular_trackBlockLogH(Object &Obj, Function &FuncCalcHastingsRatio,
											  const coretools::TRange &IxParameters, double LogPriorRatio) {
		return TLogHCalculatorBlocks::_calculateLogHRegular_trackBlockLogH<FuncCalculatesLogH>(
			Obj, FuncCalcHastingsRatio, IxParameters, LogPriorRatio);
	}

	template<bool FuncCalculatesLogH, class Object, class Function>
	bool _calculateLogHInBlocks(Object &Obj, Function &FuncCalcHastingsRatio, const coretools::TRange &IxParameters,
								double LogPriorRatio, double r) const {
		return TLogHCalculatorBlocks::_calculateLogHInBlocks<FuncCalculatesLogH>(Obj, FuncCalcHastingsRatio,
																				 IxParameters, LogPriorRatio, r);
	}

	std::tuple<size_t, size_t, double> _startBlock(size_t BlockIndex, double LogPriorRatio) const {
		return TLogHCalculatorBlocks::_startBlock(BlockIndex, LogPriorRatio);
	}

	auto blockEnds() { return _blockEnds; }
	auto blockOrder() { return _blockOrder; }
	const auto &coeff() { return _coeff; }
};

class TLogHCalculatorBlocksTest : public testing::Test {
public:
	static constexpr size_t numIter      = 3;
	static constexpr size_t numBlocks    = 5;
	static constexpr size_t N            = 21;
	static constexpr double relBlockSize = 0.25;

	const double logProbReject = -20;
	const double logProbAccept = log(1 - exp(logProbReject));

	size_t curIter = 0;

	BridgeTLogHCalculatorBlocks logH;

	void SetUp() override {
		logH.initialize(relBlockSize, logProbReject, logProbAccept);
		logH.setSizeData(N);
	}

	double calculateLLRatio(size_t i, const coretools::TRange & /*IxParameters*/) {
		std::array<double, numIter * N> LLRatio = {
			-1.57797716330459,   -0.944370786930691,  -1.74158036888713,   0.159722128019635,   -0.830284907481904,
			-1.72972300401257,   -0.706768941256441,  -0.510534343583584,  -0.63766540077951,   -1.32686024060841,
			0.0944140334153302,  -0.783094350399843,  -1.57389970672069,   -2.82020207417823,   -0.208155590297253,
			-1.12314910392537,   -1.10066788967978,   -0.349796334870278,  -0.445697989575741,  -0.623493114815351,
			-0.369239335306495,  -0.182376449967702,  -0.121971570574892,  -0.197973565574473,  -0.0167128455164276,
			-0.111095190466526,  -0.196843143404065,  -0.0993197928045995, -0.0806117626345706, -0.0927318050826103,
			-0.158436211998876,  -0.022938994313294,  -0.106596277552589,  -0.181987725544521,  -0.300803992236919,
			-0.0517844767923106, -0.139015405799399,  -0.136872158689291,  -0.0652878006414261, -0.074430587239009,
			-0.0913806898534917, -0.0671413995636867, 2.96110871409168,    1.34954060483617,    3.37723096052124,
			-1.45870280579974,   1.0593649186503,     3.34707193465881,    0.74520395124967,    0.246084259064721,
			0.569440154952974,   2.32239666919783,    -1.29259266792065,   0.939336459835407,   2.95073776575134,
			6.12068873969633,    -0.523011466425779,  1.80426051460707,    1.74707989066372,    -0.162750403032706,
			0.0811739861640253,  0.533393162771805,   -0.11329742935752};

		return LLRatio[curIter * N + i];
	}

	void trackBlockLogH() {
		logH.startTrackingBlockLogH(100);

		const double logPriorRatio = 0.5;
		for (size_t iter = 0; iter < numIter; ++iter) {
			curIter  = iter;
			auto ptr = &TLogHCalculatorBlocksTest::calculateLLRatio;
			logH._calculateLogHRegular_trackBlockLogH<true>(*this, ptr, coretools::TRange(0), logPriorRatio);
		}

		logH.startEvaluatingBlockLogH();
	}
};

TEST_F(TLogHCalculatorBlocksTest, blockEnds) { EXPECT_THAT(logH.blockEnds(), testing::ElementsAre(5, 10, 15, 20, 21)); }

TEST_F(TLogHCalculatorBlocksTest, startBlock) {
	const double logPriorRatio = 0.5;

	for (size_t b = 0; b < numBlocks - 1; b++) {
		auto [start, end, blockLogH] = logH._startBlock(b, logPriorRatio);
		EXPECT_EQ(start, b * numBlocks);
		EXPECT_EQ(end, (b + 1) * numBlocks);
		EXPECT_EQ(blockLogH, logPriorRatio * 5.0 / (double)N);
	}

	// last block:
	auto [start, end, blockLogH] = logH._startBlock(numBlocks - 1, logPriorRatio);
	EXPECT_EQ(start, 20);
	EXPECT_EQ(end, 21);
	EXPECT_EQ(blockLogH, logPriorRatio * 1.0 / (double)N);
}

TEST_F(TLogHCalculatorBlocksTest, _calculateLogProbPredLargerR) {
	// pnorm(0.5, 0.8, 0.1, lower.tail = FALSE, log.p = TRUE)
	EXPECT_FLOAT_EQ(logH._calculateLogProbPredLargerR(0.5, 0.8, 0.1), -0.00135081);

	// pnorm(0.5, 0.01, 0.1, lower.tail = FALSE, log.p = TRUE)
	EXPECT_FLOAT_EQ(logH._calculateLogProbPredLargerR(0.5, 0.01, 0.1), -14.55118);

	// pnorm(0.5, 0.01, 0.1, lower.tail = FALSE, log.p = TRUE)
	EXPECT_FLOAT_EQ(logH._calculateLogProbPredLargerR(0.5, 0.5, 0.1), -0.6931472);
}

TEST_F(TLogHCalculatorBlocksTest, _calculateLogHRegular_trackBlockLogH) {
	trackBlockLogH();

	// check if linear model is the same as in R
	const auto &coeff = logH.coeff();

	EXPECT_THAT(logH.blockOrder(), testing::ElementsAre(1, 4, 0, 3, 2));

	std::array<double, numBlocks * 3> coeff_from_R = {-0.413955051545369,   3.76018988749973,
													  0.41143514170161,     0.220044029808763,
													  3.69412958358639,     0.218417675372812,
													  -0.107357347425675,   1.85733358062965,
													  0.106631119169947,    0.325168904256372,
													  1.42001419776975,     0.322706998072753,
													  5.12790049702284e-16, 1,
													  7.105427357601e-15};
	for (size_t b = 0; b < numBlocks; ++b) {
		EXPECT_NEAR(coeff_from_R[b * 3], coeff(b, 0), 1e-10);
		EXPECT_NEAR(coeff_from_R[b * 3 + 1], coeff(b, 1), 1e-10);
		EXPECT_NEAR(coeff_from_R[b * 3 + 2], coeff(b, 2), 1e-10);
	}
}

TEST_F(TLogHCalculatorBlocksTest, _calculateLogHInBlocks) {
	trackBlockLogH();

	std::array<double, numIter> random = {-18.64902, -100.0, 100.0};
	const double logPriorRatio         = 0.5;

	std::array<bool, numIter> accepted{};
	for (size_t iter = 0; iter < numIter; ++iter) {
		curIter  = iter;
		auto ptr = &TLogHCalculatorBlocksTest::calculateLLRatio;
		accepted[iter] =
			logH._calculateLogHInBlocks<true>(*this, ptr, coretools::TRange(0), logPriorRatio, random[iter]);
	}

	EXPECT_THAT(accepted, testing::ElementsAre(false, true, false));
}

//--------------------------------------
// TMultiLogHCalculatorBlocks
//--------------------------------------

TEST(TMultiLogHCalculatorBlocksTest, initialize_shared) {
	TMultiLogHCalculatorBlocks multiBlocks;
	multiBlocks.initialize(1, 0.25, -20, log(1 - exp(-20)));

	EXPECT_TRUE(multiBlocks.isShared());

	multiBlocks.initialize(100, 0.25, -20, log(1 - exp(-20)));

	EXPECT_FALSE(multiBlocks.isShared());
}
