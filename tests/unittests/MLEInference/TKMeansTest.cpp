//
// Created by madleina on 15.11.22.
//

#include <stdint.h>

#include "gtest/gtest.h"

#include "coretools/Main/TRandomGenerator.h"
#include "coretools/Math/TMatrix.h"
#include "coretools/algorithms.h"
#include "stattools/MLEInference/TKMeans.h"

using namespace testing;
using namespace stattools;
using namespace coretools::instances;

class TKMeansTest : public Test {
public:
	std::vector<size_t> trueAssignment;
	coretools::TMatrix<double> trueMeans;

	auto createInput(size_t N, size_t M, size_t K) {
		// fill true means
		trueMeans.resize(K, M, 0.0);
		for (size_t k = 0; k < K; k++) {
			for (size_t m = 0; m < M; m++) {
				trueMeans(k, m) = (double)coretools::getLinearIndex<size_t, 2>({k, m}, {K, M}) * 10.0;
			}
		}

		// fill data
		coretools::TMatrix<double> mat;
		mat.resize(N, M, 0.0);
		trueAssignment.resize(N);
		for (size_t n = 0; n < N; n++) {
			trueAssignment[n] = randomGenerator().getRand(0UL, K);
			for (size_t m = 0; m < M; m++) {
				mat(n, m) = randomGenerator().getNormalRandom(trueMeans(trueAssignment[n], m), 0.1);
			}
		}
		TDataForKMeans<coretools::TMatrix<double>> data({mat});
		return data;
	}

	void sortClusters(coretools::TMatrix<double> &Means, std::vector<size_t> &Assignments) {
		const size_t K = Means.n_rows;
		const size_t M = Means.n_cols;
		std::vector<double> tmp(K);
		for (size_t k = 0; k < K; k++) { tmp[k] = Means(k, 0); }
		std::vector<size_t> ranks = coretools::rankSort(tmp);

		auto newMeans = Means;
		for (size_t k = 0; k < K; k++) {
			for (size_t m = 0; m < M; m++) { newMeans(k, m) = Means(ranks[k], m); }
		}

		auto newAssignments = Assignments;
		for (size_t n = 0; n < Assignments.size(); n++) {
			newAssignments[n] = std::distance(ranks.begin(), std::find(ranks.begin(), ranks.end(), Assignments[n]));
		}

		Means       = newMeans;
		Assignments = newAssignments;
	}
};

TEST_F(TKMeansTest, test) {
	// simulate data
	size_t N = 1000;

	for (size_t M = 1; M < 5; M++) {
		for (size_t K = 1; K < 5; K++) {
			auto data = createInput(N, M, K);

			// estimate
			TKMeans kMeans(N, M, K, data);
			auto [means, ass] = kMeans.doKMeans();

			// account for possibly switched clusters
			sortClusters(means[0], ass);

			// compare
			double absError = 0.1;
			for (size_t k = 0; k < K; k++) {
				for (size_t m = 0; m < M; m++) { EXPECT_NEAR(trueMeans(k, m), means[0](k, m), absError); }
			}

			size_t c = 0;
			for (size_t n = 0; n < N; n++) {
				if (trueAssignment[n] != ass[n]) { ++c; }
			}
			EXPECT_EQ(c, 0);
		}
	}
};
