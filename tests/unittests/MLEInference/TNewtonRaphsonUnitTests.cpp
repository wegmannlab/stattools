//
// Created by madleina on 24.03.21.
//

#include <armadillo>
#include <cassert> // for assert
#include <cmath>   // for fabs
#include <cstddef> // for size_t
#include <ostream> // for operator<<
#include <string>  // for operator<<, allocator
#include <vector>  // for vector

#include "gtest/gtest.h"

#include "coretools/Main/TRandomGenerator.h"       // for TRandomGenerator
#include "stattools/MLEInference/TNewtonRaphson.h" // for TNewtonRaphson, TMultiDime...
#include "stattools/MLEInference/TReturnCodes.h"   // for TReturnCode, stattools

using namespace testing;
using namespace stattools;

//---------------------------------
// One-Dimensional Newton-Raphson
//---------------------------------

class TPolynomial {
	// use polynomial function y = x^3 - 2*x + 2
public:
	double polynomial(double x) { return x * x * x - 2 * x + 2; }

	double firstDerivative(double x) { return 3. * x * x - 2.; }
};

class TNewtonRaphsonTest : public Test {
public:
	TPolynomial polynomial;

	// solution
	double root = -1.769292; // from WolframAlpha
};

TEST_F(TNewtonRaphsonTest, newtonRaphson_bisection) {
	auto ptr1 = &TPolynomial::polynomial;
	auto ptr2 = &TPolynomial::firstDerivative;
	TNewtonRaphson newtonRaphson(polynomial, ptr1, ptr2);
	newtonRaphson.setEpsilon(10e-10);
	newtonRaphson.setMaxIterations(100000);

	size_t numRep = 1000;
	for (size_t i = 0; i < numRep; i++) {
		// get random starting value and initial step size
		double min     = coretools::instances::randomGenerator().getRand(-10., root);
		double max     = coretools::instances::randomGenerator().getRand(root, 10.);
		double start   = 0.5 * (min + max);
		bool converged = newtonRaphson.runNewtonRaphson_withBisection(start, min, max);
		EXPECT_TRUE(converged);
		EXPECT_FLOAT_EQ(newtonRaphson.root(), root);
	}
}

TEST_F(TNewtonRaphsonTest, newtonRaphson_bisection_dontAllowBisection) {
	auto ptr1 = &TPolynomial::polynomial;
	auto ptr2 = &TPolynomial::firstDerivative;
	TNewtonRaphson newtonRaphson(polynomial, ptr1, ptr2);
	newtonRaphson.setEpsilon(10e-10);
	newtonRaphson.setMaxIterations(100000);

	size_t numRep = 1000;
	for (size_t i = 0; i < numRep; i++) {
		// get random starting value and initial step size
		double min     = coretools::instances::randomGenerator().getRand(-10., root);
		double max     = coretools::instances::randomGenerator().getRand(root, 10.);
		double start   = 0.5 * (min + max);
		bool converged = newtonRaphson.runNewtonRaphson_withBisection<false>(start, min, max);
		EXPECT_TRUE(converged);
		EXPECT_FLOAT_EQ(newtonRaphson.root(), root);
	}
}

TEST_F(TNewtonRaphsonTest, newtonRaphson_backtracking) {
	auto ptr1 = &TPolynomial::polynomial;
	auto ptr2 = &TPolynomial::firstDerivative;
	TNewtonRaphson newtonRaphson(polynomial, ptr1, ptr2);
	newtonRaphson.setEpsilon(10e-10);
	newtonRaphson.setMaxIterations(100000);

	size_t numRep = 1000;
	for (size_t i = 0; i < numRep; i++) {
		// get random starting value and initial step size
		double start   = coretools::instances::randomGenerator().getRand(-10., 10.);
		bool converged = newtonRaphson.runNewtonRaphson_withBacktracking(start);
		EXPECT_TRUE(converged);
		EXPECT_FLOAT_EQ(newtonRaphson.root(), root);
	}
}

//---------------------------------
// One-Dimensional Newton-Raphson
// with function that converges to zero
//---------------------------------

class TNormalDistr {
	// use derivatives of normal distribution function
protected:
	double mu    = 1.0;
	double sigma = 1.0;

public:
	double firstDerivative(double x) const {
		const double tmp = x - mu;
		return -(tmp * exp(-(tmp * tmp) / 2. * sigma * sigma)) / (sqrt(2. * M_PI) * sigma * sigma * sigma);
	}

	double secondDerivative(double x) const {
		const double tmp2    = (x - mu) * (x - mu);
		const double s2      = sigma * sigma;
		const double expFrac = exp(-tmp2 / (2. * s2));
		return (((tmp2 * expFrac) / std::pow(sigma, 4)) - expFrac / s2) / (sqrt(2. * M_PI) * sigma);
	}

	double getMu() const { return mu; }
};

class TNewtonRaphsonNormalTest : public Test {
public:
	TNormalDistr normal;

	// solution
	double root = normal.getMu();
};

TEST_F(TNewtonRaphsonNormalTest, newtonRaphson_bisection_convergeZero) {
	auto ptr1 = &TNormalDistr::firstDerivative;
	auto ptr2 = &TNormalDistr::secondDerivative;
	TNewtonRaphson newtonRaphson(normal, ptr1, ptr2);
	newtonRaphson.setEpsilon(1e-10);
	newtonRaphson.setMaxIterations(100000);

	size_t numRep = 1000000;
	for (size_t i = 0; i < numRep; i++) {
		// get random max and initial step size
		double min     = 0.0;
		double max     = coretools::instances::randomGenerator().getRand(root, 20.);
		double start   = 1e-06;
		bool converged = newtonRaphson.runNewtonRaphson_withBisection<false>(start, min, max);
		EXPECT_TRUE(converged);
		EXPECT_NEAR(newtonRaphson.root(), root, 10e-8);
	}
}

//---------------------------------
// Multi-Dimensional Newton-Raphson
//---------------------------------
class THimmelblau {
	// use Himmelblau's function: https://en.wikipedia.org/wiki/Himmelblau%27s_function
	// this is a nice example of a function that should be optimized
	// solutions are known -> we can check if we get close to these!
public:
	double himmelblau(const std::vector<double> &Values) {
		assert(Values.size() == 2);
		double x = Values[0];
		double y = Values[1];

		double tmp1 = x * x + y - 11;
		double tmp2 = x + y * y - 7;
		return tmp1 * tmp1 + tmp2 * tmp2;
	}

	template<class VectorType> std::pair<VectorType, bool> gradient(coretools::TConstView<double> Values, size_t) {
		assert(Values.size() == 2);
		double x = Values[0];
		double y = Values[1];

		VectorType gradient;
		if constexpr (std::is_same_v<VectorType, std::vector<double>>) { gradient.resize(2); }
		gradient[0] = 4. * x * (x * x + y - 11.) + 2. * (x + y * y - 7.);
		gradient[1] = 2. * (x * x + y - 11.) + 4. * y * (x + y * y - 7.);

		return {gradient, true};
	}

	arma::mat hessian(coretools::TConstView<double> Values, coretools::TConstView<double>, size_t) {
		assert(Values.size() == 2);
		double x = Values[0];
		double y = Values[1];

		arma::mat hessian(2, 2);
		hessian(0, 0) = 4. * (x * x + y - 11.) + 8. * x * x + 2.;
		hessian(0, 1) = 4. * x + 4. * y;
		hessian(1, 0) = 4. * x + 4. * y;
		hessian(1, 1) = 4. * (x + y * y - 7.) + 8. * y * y + 2.;

		return hessian;
	}
};

class TMultiDimensionalNewtonRaphsonTest : public Test {
public:
	THimmelblau himmelblau;

	// there are 4 possible solutions for minima's for Himmelblau's function (from
	// https://en.wikipedia.org/wiki/Himmelblau%27s_function)
	std::vector<std::vector<double>> criticalPoints = {// minima
	                                                   {3., 2.},
	                                                   {-2.805118, 3.131312},
	                                                   {-3.779310, -3.283186},
	                                                   {3.584428, -1.848126},
	                                                   // saddle points
	                                                   {-3.07303, -0.081353},
	                                                   {-0.127961, -1.95371},
	                                                   {0.0866775, 2.88425},
	                                                   {3.38515, 0.0738519},
	                                                   // maximum
	                                                   {-0.270845, -0.923039}};
};

TEST_F(TMultiDimensionalNewtonRaphsonTest, newtonRaphson) {
	auto ptr1 = &THimmelblau::gradient<std::vector<double>>;
	auto ptr2 = &THimmelblau::hessian;
	TMultiDimensionalNewtonRaphson newtonRaphson(himmelblau, ptr1, ptr2);

	size_t numRep        = 1000;
	size_t counterFailed = 0;
	double absDifference = 10e-5;
	coretools::instances::randomGenerator().setSeed(1, true);
	for (size_t i = 0; i < numRep; i++) {
		bool converged;
		do {
			// restart
			// get random starting value and initial step size
			double start_x            = coretools::instances::randomGenerator().getRand(-10., 10.);
			double start_y            = coretools::instances::randomGenerator().getRand(-10., 10.);
			std::vector<double> start = {start_x, start_y};
			converged                 = newtonRaphson.runNewtonRaphson_withBacktracking(start);
		} while (!converged);

		// check if they are somewhat close to known critical points
		bool closeToCriticalPoint = false;
		for (auto &knownCriticalPoint : criticalPoints) {
			// check if both x and y are close to known minimum
			if (std::fabs(knownCriticalPoint[0] - newtonRaphson.root()[0]) < absDifference &&
			    std::fabs(knownCriticalPoint[1] - newtonRaphson.root()[1]) < absDifference) {
				closeToCriticalPoint = true;
				break;
			}
		}
		if (!closeToCriticalPoint) { counterFailed++; }
	}

	EXPECT_EQ(counterFailed, 0);
}

TEST_F(TMultiDimensionalNewtonRaphsonTest, newtonRaphson_array) {
	auto ptr1 = &THimmelblau::gradient<std::array<double, 2>>;
	auto ptr2 = &THimmelblau::hessian;
	TMultiDimensionalNewtonRaphson<2> newtonRaphson(himmelblau, ptr1, ptr2);

	size_t numRep        = 1000;
	size_t counterFailed = 0;
	double absDifference = 10e-5;
	coretools::instances::randomGenerator().setSeed(1, true);
	for (size_t i = 0; i < numRep; i++) {
		bool converged;
		do {
			// restart
			// get random starting value and initial step size
			double start_x              = coretools::instances::randomGenerator().getRand(-10., 10.);
			double start_y              = coretools::instances::randomGenerator().getRand(-10., 10.);
			std::array<double, 2> start = {start_x, start_y};
			converged                   = newtonRaphson.runNewtonRaphson_withBacktracking(start);
		} while (!converged);

		// check if they are somewhat close to known critical points
		bool closeToCriticalPoint = false;
		for (auto &knownCriticalPoint : criticalPoints) {
			// check if both x and y are close to known minimum
			if (std::fabs(knownCriticalPoint[0] - newtonRaphson.root()[0]) < absDifference &&
			    std::fabs(knownCriticalPoint[1] - newtonRaphson.root()[1]) < absDifference) {
				closeToCriticalPoint = true;
				break;
			}
		}
		if (!closeToCriticalPoint) { counterFailed++; }
	}

	EXPECT_EQ(counterFailed, 0);
}

TEST_F(TMultiDimensionalNewtonRaphsonTest, newtonRaphson_JacobianApproximation) {
	// Note: creates many warnings that Jacobian matrix is not symmetric when calling eig_sym on line 506
	// That's because we approximate the Jacobian, and fill element (0,1) independently from element (1,0)
	// Test still passes -> not sure if this is problematic in a real-case scenario!
	auto ptr1 = &THimmelblau::gradient<std::vector<double>>;
	TApproxJacobian approxJacobian(himmelblau, ptr1);
	auto ptr2 = &TApproxJacobian<0>::approximateJacobian;
	TMultiDimensionalNewtonRaphson newtonRaphson(himmelblau, ptr1, approxJacobian, ptr2);

	size_t numRep        = 1000;
	size_t counterFailed = 0;
	double absDifference = 10e-5;
	for (size_t i = 0; i < numRep; i++) {
		bool converged = false;
		do {
			// restart
			// get random starting value and initial step size
			double start_x            = coretools::instances::randomGenerator().getRand(-10., 10.);
			double start_y            = coretools::instances::randomGenerator().getRand(-10., 10.);
			std::vector<double> start = {start_x, start_y};
			converged                 = newtonRaphson.runNewtonRaphson_withBacktracking(start);
		} while (!converged);

		// check if they are somewhat close to known minima
		bool closeToCriticalPoint = false;
		for (auto &knownCriticalPoint : criticalPoints) {
			// check if both x and y are close to known minimum
			if (std::fabs(knownCriticalPoint[0] - newtonRaphson.root()[0]) < absDifference &&
			    std::fabs(knownCriticalPoint[1] - newtonRaphson.root()[1]) < absDifference) {
				closeToCriticalPoint = true;
				break;
			}
		}
		// check if the found minimum is not close to any known minimum
		if (!closeToCriticalPoint) { counterFailed++; }
	}

	EXPECT_EQ(counterFailed, 0);
}
