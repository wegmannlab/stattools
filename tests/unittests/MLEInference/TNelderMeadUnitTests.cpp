//
// Created by madleina on 17.02.21.
//

#include <algorithm>
#include <cmath>
#include <type_traits>
#include "gtest/gtest.h"

#include "coretools/Main/TRandomGenerator.h"
#include "stattools/MLEInference/TNelderMead.h"

using namespace testing;
using namespace stattools;

//------------------------------------------
// Test with class member function
//------------------------------------------

class THimmelblauSimple {
	// use Himmelblau's function: https://en.wikipedia.org/wiki/Himmelblau%27s_function
	// this is a nice example of a function that should be optimized
	// solutions are known -> we can check if we get close to these!
public:
	THimmelblauSimple() = default;

	double himmelblau(coretools::TConstView<double> Values) {
		assert(Values.size() == 2);
		double x = Values[0];
		double y = Values[1];

		double tmp1 = x * x + y - 11;
		double tmp2 = x + y * y - 7;
		return tmp1 * tmp1 + tmp2 * tmp2;
	}
};

class TNelderMeadMemberFunctionTest : public Test {
public:
	THimmelblauSimple himmelblau;

	// there are 4 possible solutions for minima's for Himmelblau's function (from
	// https://en.wikipedia.org/wiki/Himmelblau%27s_function)
	std::vector<std::vector<double>> minima = {
		{3., 2.}, {-2.805118, 3.131312}, {-3.779310, -3.283186}, {3.584428, -1.848126}};
};

TEST_F(TNelderMeadMemberFunctionTest, minimize_delIsOneNumber) {
	using coretools::instances::randomGenerator;
	// run algorithm 1000x, draw random starting values for simplex and check if we get close to one of the minima
	size_t numReplicates = 1000;
	size_t counterFailed = 0;
	double absDifference = 0.01;
	for (size_t rep = 0; rep < numReplicates; rep++) {
		// get initial starting values (between -4 and 4, this the "interesting" range for Himmelblau's function
		const auto init = std::array<double, 2>{randomGenerator().getRand(-4., 4.), randomGenerator().getRand(-4., 4.)};

		// define displacement for initialVertex
		double displacement = 2.;

		// run Nelder-Mead
		auto ptr        = &THimmelblauSimple::himmelblau;
		auto nelderMead = TNelderMead<2>(himmelblau, ptr);

		bool converged = nelderMead.minimize(init, displacement);
		EXPECT_TRUE(converged);

		// check if they are somewhat close to known minima
		bool closeToMinimum = false;
		for (auto &knownMinimum : minima) {
			// check if both x and y are close to known minimum
			if (std::fabs(knownMinimum[0] - nelderMead.coordinates()[0]) < absDifference &&
				std::fabs(knownMinimum[1] - nelderMead.coordinates()[1]) < absDifference) {
				closeToMinimum = true;
				break;
			}
		}
		// check if the found minimum is not close to any known minimum
		if (!closeToMinimum) { counterFailed++; }
	}

	EXPECT_EQ(counterFailed, 0);
}

TEST_F(TNelderMeadMemberFunctionTest, minimize_delIsVector) {
	// run algorithm 1000x, draw random starting values for simplex and check if we get close to one of the minima
	size_t numReplicates = 1000;
	size_t counterFailed = 0;
	double absDifference = 0.0001;
	for (size_t rep = 0; rep < numReplicates; rep++) {
		// get initial starting values (between -4 and 4, this the "interesting" range for Himmelblau's function)
		// create initial vertex
		std::array<double, 2> initialVertex{};
		initialVertex[0] = coretools::instances::randomGenerator().getRand(-4., 4.);
		initialVertex[1] = coretools::instances::randomGenerator().getRand(-4., 4.);

		// define displacement vector for initialVertex
		double del1                       = coretools::instances::randomGenerator().getRand(-3., 3.);
		double del2                       = coretools::instances::randomGenerator().getRand(-3., 3.);
		std::vector<double> displacements = {del1, del2};

		// run Nelder-Mead
		auto ptr        = &THimmelblauSimple::himmelblau;
		auto nelderMead = TNelderMead<2>(himmelblau, ptr);
		bool converged  = nelderMead.minimize(initialVertex, displacements);

		EXPECT_TRUE(converged);

		// check if they are somewhat close to known minima
		bool closeToMinimum = false;
		for (auto &knownMinimum : minima) {
			// check if both x and y are close to known minimum
			if (std::fabs(knownMinimum[0] - nelderMead.coordinates()[0]) < absDifference &&
				std::fabs(knownMinimum[1] - nelderMead.coordinates()[1]) < absDifference) {
				closeToMinimum = true;
				break;
			}
		}
		// check if the found minimum is not close to any known minimum
		if (!closeToMinimum) { counterFailed++; }
	}

	EXPECT_EQ(counterFailed, 0);
}

TEST_F(TNelderMeadMemberFunctionTest, minimize_provideInitialSimplex) {

	// run algorithm 1000x, draw random starting values for simplex and check if we get close to one of the minima
	size_t numReplicates = 1000;
	size_t counterFailed = 0;
	double absDifference = 0.0001;
	for (size_t rep = 0; rep < numReplicates; rep++) {
		// get initial starting values (between -4 and 4, this the "interesting" range for Himmelblau's function)
		// create initial simplex
		TSimplex<2> initialSimplex;
		for (size_t vertex = 0; vertex < 3; vertex++) {
			initialSimplex[vertex][0] = coretools::instances::randomGenerator().getRand(-4., 4.);
			initialSimplex[vertex][1] = coretools::instances::randomGenerator().getRand(-4., 4.);
		}

		// run Nelder-Mead
		auto ptr        = &THimmelblauSimple::himmelblau;
		auto nelderMead = TNelderMead<2>(himmelblau, ptr);
		bool converged  = nelderMead.minimize(initialSimplex);

		EXPECT_TRUE(converged);

		// check if they are somewhat close to known minima
		bool closeToMinimum = false;
		for (auto &knownMinimum : minima) {
			// check if both x and y are close to known minimum
			if (std::fabs(knownMinimum[0] - nelderMead.coordinates()[0]) < absDifference &&
				std::fabs(knownMinimum[1] - nelderMead.coordinates()[1]) < absDifference) {
				closeToMinimum = true;
				break;
			}
		}
		// check if the found minimum is not close to any known minimum
		if (!closeToMinimum) { counterFailed++; }
	}

	EXPECT_EQ(counterFailed, 0);
}

//------------------------------------------
// Test with free function
//------------------------------------------

double himmelblau(double x, double y) {
	double tmp1 = x * x + y - 11;
	double tmp2 = x + y * y - 7;
	return tmp1 * tmp1 + tmp2 * tmp2;
}

TEST(TNelderMeadFreeFunctionTest, Static) {
	using coretools::instances::randomGenerator;

	const auto init   = std::array<double, 2>{randomGenerator().getRand(-4., 4.), randomGenerator().getRand(-4., 4.)};
	const auto del    = 10 * std::max(init.front(), init.back());
	const auto minima = std::array<std::array<double, 2>, 4>{
		{{3., 2.}, {-2.805118, 3.131312}, {-3.779310, -3.283186}, {3.584428, -1.848126}}};

	auto nm2 = TNelderMead<2>([](auto Vals) { return himmelblau(Vals[0], Vals[1]); });
	EXPECT_TRUE(nm2.minimize(init, del));

	// check if they are somewhat close to known minima
	bool closeToMinimum     = false;
	const auto foundMinimum = nm2.coordinates();
	static_assert(std::is_same_v<std::remove_cv_t<decltype(foundMinimum)>, std::array<double, 2>>);

	for (auto &knownMinimum : minima) {
		constexpr auto maxDiff = 0.0001;
		// check if both x and y are close to known minimum
		if (std::fabs(knownMinimum[0] - foundMinimum[0]) < maxDiff &&
			std::fabs(knownMinimum[1] - foundMinimum[1]) < maxDiff) {
			closeToMinimum = true;
			break;
		}
	}
	EXPECT_TRUE(closeToMinimum);
}

TEST(TNelderMeadFreeFunctionTest, Dynamic) {
	using coretools::instances::randomGenerator;

	const auto init   = std::array<double, 2>{randomGenerator().getRand(-4., 4.), randomGenerator().getRand(-4., 4.)};
	const auto del    = 10 * std::max(init.front(), init.back());
	const auto minima = std::array<std::array<double, 2>, 4>{
		{{3., 2.}, {-2.805118, 3.131312}, {-3.779310, -3.283186}, {3.584428, -1.848126}}};

	auto nmdyn = TNelderMead<>([](auto Vals) { return himmelblau(Vals[0], Vals[1]); });
	EXPECT_TRUE(nmdyn.minimize(init, del));

	// check if they are somewhat close to known minima
	bool closeToMinimum     = false;
	const auto foundMinimum = nmdyn.coordinates();
	static_assert(std::is_same_v<std::remove_cv_t<decltype(foundMinimum)>, std::vector<double>>);

	for (auto &knownMinimum : minima) {
		constexpr auto maxDiff = 0.0001;
		// check if both x and y are close to known minimum
		if (std::fabs(knownMinimum[0] - foundMinimum[0]) < maxDiff &&
			std::fabs(knownMinimum[1] - foundMinimum[1]) < maxDiff) {
			closeToMinimum = true;
			break;
		}
	}
	EXPECT_TRUE(closeToMinimum);
}

TEST(TNelderMeadFreeFunctionTest, Positiv) {
	using coretools::instances::randomGenerator;

	// Logspace
	const auto init = std::array<double, 2>{log(2.), log(4.)};
	const auto del  = 5 * std::max(init.front(), init.back());

	auto nmdyn = TNelderMead<>([](auto Vals) { return himmelblau(exp(Vals[0]), exp(Vals[1])); });
	EXPECT_TRUE(nmdyn.minimize(init, del));
	const auto foundMinimum = nmdyn.coordinates();

	// only possible positive solutions
	EXPECT_FLOAT_EQ(exp(foundMinimum.front()), 3.);
	EXPECT_FLOAT_EQ(exp(foundMinimum.back()), 2.);
}
