//
// Created by madleina on 24.03.21.
//

#include <stdint.h>

#include "gtest/gtest.h"

#include "stattools/MLEInference/TLineSearch.h"
#include "coretools/Main/TRandomGenerator.h"
#include "stattools/MLEInference/TReturnCodes.h"

using namespace testing;
using namespace stattools;

class TPolynomial {
	// use polynomial functions
public:
	TPolynomial(){};

	double polynomialWithMinimum(double x) { return x * x - x - 2; }

	double polynomialWithMaximum(double x) { return -x * x + x - 2; }

	double polynomialWithZero(double x) { return x * x * x; }
};

class TLineSearchTest : public Test {
public:
	TPolynomial polynomial;
	TLineSearch lineSearch;
	coretools::TRandomGenerator randomGenerator;

	// solutions
	double min  = 0.5;
	double max  = 0.5;
	double root = 0.;
};

TEST_F(TLineSearchTest, findMinimum) {
	auto ptr = &TPolynomial::polynomialWithMinimum;
	// lineSearch.report(&logfile);

	uint16_t numRep = 1000;
	for (uint16_t i = 0; i < numRep; i++) {
		// get random starting value and initial step size
		double start       = randomGenerator.getRand(-10., 10.);
		double initialStep = randomGenerator.getRand(-10., 10.);
		auto result        = lineSearch.findMin(polynomial, ptr, start, initialStep, 10e-10, 100000);
		EXPECT_TRUE(result.converged());
		EXPECT_FLOAT_EQ(result.result(), min);
	}
}

TEST_F(TLineSearchTest, findMaximum) {
	auto ptr = &TPolynomial::polynomialWithMaximum;
	// lineSearch.report(&logfile);

	uint16_t numRep = 1000;
	for (uint16_t i = 0; i < numRep; i++) {
		// get random starting value and initial step size
		double start       = randomGenerator.getRand(-10., 10.);
		double initialStep = randomGenerator.getRand(-10., 10.);
		auto result        = lineSearch.findMax(polynomial, ptr, start, initialStep, 10e-10, 100000);
		EXPECT_TRUE(result.converged());
		EXPECT_FLOAT_EQ(result.result(), max);
	}
}

TEST_F(TLineSearchTest, findZero) {
	auto ptr = &TPolynomial::polynomialWithZero;
	// lineSearch.report(&logfile);

	uint16_t numRep = 1000;
	for (uint16_t i = 0; i < numRep; i++) {
		// get random starting value and initial step size
		double start       = randomGenerator.getRand(-10., 10.);
		double initialStep = randomGenerator.getRand(-10., 10.);
		auto result        = lineSearch.findZero(polynomial, ptr, start, initialStep, 10e-10, 100000);
		EXPECT_TRUE(result.converged());
		EXPECT_NEAR(result.result(), root, 10e-8);
	}
}
