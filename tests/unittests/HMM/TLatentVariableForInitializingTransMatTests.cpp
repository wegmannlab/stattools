//
// Created by madleina on 13.03.23.
//

#include <memory> // for shared_ptr, make_shared
#include <vector> // for vector

#include "gtest/gtest.h"

#include "coretools/Distributions/TNormalDistr.h"
#include "coretools/Types/probability.h"
#include "genometools/GenomePositions/TDistances.h" // for TDistancesBinned::TDis...
#include "stattools/EM/TDataVector.h"               // for TDataVector
#include "stattools/HMM/TLatentVariableBeta.h"
#include "stattools/HMMDistances/TOptimizeTransMatLineSearch.h"
#include "stattools/HMMDistances/TTransitionMatrixHMMDistances.h"
#include "stattools/commonTestCases/commonTestingFunctions.h"
#include "testFiles/THMMLadder_Tables.h"
#include "stattools/HMMDistances/TTransitionMatrixDist.h"
#include "stattools/HMMDistances/TTransitionMatrixHMMDistances.h"

using namespace testing;
using namespace stattools;

//-------------------------------------------
// Test:
// TLatentVariableMLStates
//-------------------------------------------

class TDummyLatentVariableForTest : public TLatentVariableWithRep<double, size_t, size_t> {
protected:
public:
	TDummyLatentVariableForTest() : TLatentVariableWithRep<double, size_t, size_t>() {}
	~TDummyLatentVariableForTest() override = default;

	std::vector<double> getParameters() const override { return {}; };
	bool setParameters(coretools::TConstView<double>) override { return true; };

	// function to calculate emission probabilities
	void calculateEmissionProbabilities(size_t Index, size_t /*Replicate*/,
										TDataVector<double, size_t> &Emission) const override {
		std::vector<double> mus  = {-2, 0, 2};
		std::vector<double> vars = {1.0, 1.0, 1.0}; // larger variance than simulated to make it harder

		for (size_t k = 0; k < Emission.size(); k++) {
			Emission[k] =
				coretools::probdist::TNormalDistr::density(getX_3States_Ladder()[Index], mus[k], sqrt(vars[k]));
		}
	};
};

TEST(TLatentVariableBetaTest, runEM) {
	instances::dagBuilder().clear();

	coretools::instances::parameters().add("halfWindowSize", 0);

	// fill distances
	auto distances = createAndFillDistances("HMM/testFiles/positions_THMMLadder_EMMock.txt", 4);

	size_t numStates = 3;

	// create dummy latent variable for maximum likelihood states
	TDummyLatentVariableForTest latentVariable_MLStates;

	// create another latent variable to smooth ML states
	TLatentVariableBeta<double, size_t, size_t> latentVariable(numStates, distances->size(), &latentVariable_MLStates);

	// transition matrix
	using TypeOptimizer = TOptimizeTransMatLineSearch<double, size_t, size_t, TTransitionMatrixLadder<double, size_t>>;
	std::shared_ptr<TTransitionMatrixDistancesOptimizerBase<double, size_t, size_t>> optimizer =
		std::make_shared<TypeOptimizer>(numStates);
	TTransitionMatrixDistances transMat(distances.get(), optimizer);

	// run HMM
	size_t maxIterations          = 100;
	double minDeltaLL             = 0.0001;
	bool estimateTransitionMatrix = true;
	stattools::THMM<double, size_t, size_t> hmm(transMat, latentVariable, maxIterations, minDeltaLL,
												estimateTransitionMatrix);
	hmm.runEM(distances->getChunkEnds<size_t>());

	// check if kappa of transition matrix is as expected
	double absError   = 0.02;
	const auto result = transMat.getFinalParameterEstimatesEM();
	EXPECT_EQ(result.size(), 1);
	EXPECT_NEAR(result[0], 0.2, absError);

	// check if phi and kappa are ok (no way to know the truth, but if anything changes in framework, this should still
	// be the same)
	EXPECT_FLOAT_EQ(latentVariable.phi(), 0.89455694);
	EXPECT_FLOAT_EQ(latentVariable.kappa(), 66.190224);

	// estimate state posteriors of smoothed latent variable
	hmm.estimateStatePosteriors(distances->getChunkEnds<size_t>(), 1, "statePosteriors_TLatentVariableBetaTest.txt");

	// read true z (used for simulation)
	std::vector<uint8_t> trueZ = readSingleLineIntoVec<uint8_t>("HMM/testFiles/trueZ_THMMLadder_EMMock.txt");

	// read smoothed z and compare
	coretools::TInputFile file("statePosteriors_TLatentVariableBetaTest.txt", coretools::FileType::Header);
	EXPECT_EQ(file.numCols(), 5);

	size_t counterDifferences = 0;
	size_t row                = 0;
	for (; !file.empty(); file.popFront()) {
		std::vector<double> statePosteriors(numStates);
		for (size_t i = 0; i < numStates; ++i) { statePosteriors[i] = file.get<coretools::Probability>(i + 2); }
		size_t maxIx =
			std::distance(statePosteriors.begin(), std::max_element(statePosteriors.begin(), statePosteriors.end()));
		if (trueZ[row] != maxIx) { counterDifferences++; }
		++row;
	}
	EXPECT_EQ(row, distances->size());

	// check if posterior estimates of latent variable are close to truth
	double fracDifferent = static_cast<double>(counterDifferences) / (double)distances->size();
	EXPECT_TRUE(fracDifferent < 0.01);

	remove("statePosteriors_TLatentVariableBetaTest.txt");
};
