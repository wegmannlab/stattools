//
// Created by madleina on 02.11.22.
//

#include <vector>

#include "gmock/gmock.h"
#include "gtest/gtest.h"

#include "stattools/commonTestCases/THMMCombinedScaledLaddersTest.h"
#include "stattools/commonTestCases/commonTestingFunctions.h"
#include "EM/TMockLatentVariable.h"
#include "coretools/Distributions/TNormalDistr.h"
#include "testFiles/THMMCombinedScaledLadder_Tables.h"

using namespace testing;
using namespace stattools;
using namespace stattools::prior;

template<typename T> double calcDiffZ(std::string_view Filename, size_t N, const T &Z) {
	auto trueZ                = testing::readSingleLineIntoVec<uint8_t>(Filename);
	size_t counterDifferences = 0;
	for (size_t i = 0; i < N; i++) {
		if (trueZ[i] != (size_t)Z[i]) { counterDifferences++; }
	}
	double fracDifferent = static_cast<double>(counterDifferences) / (double)N;
	return fracDifferent;
}

template<typename T>
double calcDiffZAccountForSwitching(std::string_view Filename, size_t N, const std::vector<size_t> &NewComponents,
									const T *Z) {
	// transition matrix parameters: Can not directly compare to simulated values,
	// since components switched -> totally different probabilities than for simulated!
	// I just check if z are correctly estimated -> if yes, then probably transition matrix parameters are also ok
	auto trueZ = testing::readSingleLineIntoVec<uint8_t>(Filename);

	size_t counterDifferences = 0;
	for (size_t i = 0; i < N; i++) {
		auto trueComponent         = trueZ[i];
		auto switchedTrueComponent = NewComponents[trueComponent];
		if (switchedTrueComponent != Z->value(i)) { counterDifferences++; }
	}
	return static_cast<double>(counterDifferences) / N;
}

class THMMCombinedScaledLadderInferred_3States_EMMockTest : public Test {
protected:
	constexpr static size_t numChains = 1;
	constexpr static size_t numStates = 3;
	constexpr static size_t N         = 10000;

	using Type    = coretools::UnsignedInt8WithMax<0>;
	using TypeHMM = THMMCombinedScaledLaddersTest<true, Type, TTransitionMatrixScaledLadder>;
	std::unique_ptr<genometools::TDistancesBinned<uint8_t>> distances;
	std::unique_ptr<TypeHMM> hmm;
	std::unique_ptr<TypeHMM::SpecObs> obs;

	void SetUp() override {
		instances::dagBuilder().clear();

		distances =
			createAndFillDistances("HMM/testFiles/positions_THMMCombinedScaledLadderInferred_3States_EMMock.txt", 4);
		hmm = std::make_unique<TypeHMM>("out", std::vector<size_t>{numStates}, distances.get());
		obs = createObservation<Type, TypeHMM::NumDimObs, TypeHMM::BoxOnObs>("obs", &hmm->boxOnObs, {N}, {"out"}, {});
	}

	static void fillEmissions_3States_ScaledLadder(size_t Index, datVec &Emission) {
		std::vector<double> mus  = {-2, 0, 2};
		std::vector<double> vars = {0.001, 0.001, 0.001};
		for (size_t k = 0; k < Emission.size(); k++) {
			Emission[k] = coretools::probdist::TNormalDistr::density(getX_3States()[Index], mus[k], sqrt(vars[k]));
		}
	}
};

TEST_F(THMMCombinedScaledLadderInferred_3States_EMMockTest, EM) {
	// create mock latent variable and define expectations
	MockLatentVariable mockLatentVariable;

	// emission probabilities: get corresponding emission probabilities for this index
	EXPECT_CALL(mockLatentVariable, calculateEmissionProbabilities)
		.WillRepeatedly(Invoke(fillEmissions_3States_ScaledLadder));

	// EM updates of latent variable parameters: will be called, but don't have to return anything
	EXPECT_CALL(mockLatentVariable, getMLEmissionState).Times(N);
	EXPECT_CALL(mockLatentVariable, prepareEMParameterEstimationInitial).Times(1);
	EXPECT_CALL(mockLatentVariable, prepareEMParameterEstimationOneIteration).Times(AtLeast(1));
	EXPECT_CALL(mockLatentVariable, handleEMParameterEstimationOneIteration).Times(AtLeast(1));
	EXPECT_CALL(mockLatentVariable, finalizeEMParameterEstimationOneIteration).Times(AtLeast(1));
	EXPECT_CALL(mockLatentVariable, finalizeEMParameterEstimationFinal).Times(1);

	// now run EM!
	hmm->boxOnObs.runEMEstimation(mockLatentVariable);

	// check if correct kappa and mu was estimated
	double absError = 0.15;
	EXPECT_NEAR(hmm->boxOnObs.kappasMusNus()[0], 0.5, absError); // kappa
	absError = 0.005;
	EXPECT_NEAR(hmm->boxOnObs.kappasMusNus()[1], 0.03, absError); // nu

	// check if posterior estimates of latent variable are close to truth
	auto fracDifferent =
		calcDiffZ("HMM/testFiles/trueZ_THMMCombinedScaledLadderInferred_3States_EMMock.txt", N, mockLatentVariable.z);
	EXPECT_TRUE(fracDifferent < 0.001);
}

//-------------------------------------------
// Test:
// THMMCombinedScaledLadderInferred
// Only 1 Chain, with 5 States
// together with mock latent variable
//-------------------------------------------

class THMMCombinedScaledLadderInferred_5States_EMMockTest : public Test {
protected:
	constexpr static size_t numChains = 1;
	constexpr static size_t numStates = 5;
	constexpr static size_t N         = 10000;

	using Type    = coretools::UnsignedInt8WithMax<0>;
	using TypeHMM = THMMCombinedScaledLaddersTest<true, Type, TTransitionMatrixScaledLadder>;
	std::unique_ptr<genometools::TDistancesBinned<uint8_t>> distances;
	std::unique_ptr<TypeHMM> hmm;
	std::unique_ptr<TypeHMM::SpecObs> obs;

	void SetUp() override {
		instances::dagBuilder().clear();

		distances =
			createAndFillDistances("HMM/testFiles/positions_THMMCombinedScaledLadderInferred_5States_EMMock.txt", 4);
		hmm = std::make_unique<TypeHMM>("out", std::vector<size_t>{numStates}, distances.get());

		obs = createObservation<Type, TypeHMM::NumDimObs, TypeHMM::BoxOnObs>("obs", &hmm->boxOnObs, {N}, {"out"}, {});
	}

	static void fillEmissions_5States_ScaledLadder(size_t Index, datVec &Emission) {
		// my desperate solution to calculate emission probabilities
		// didn't find another way to fill 2nd argument of calculateEmissionProbabilities,
		// so it is calculated in here every time (must be a free function)
		std::vector<double> mus  = {-3., -1.5, 0, 1.5, 3.};
		std::vector<double> vars = {0.001, 0.001, 0.001, 0.001, 0.001};

		for (size_t k = 0; k < Emission.size(); k++) {
			Emission[k] = coretools::probdist::TNormalDistr::density(getX_5States()[Index], mus[k], sqrt(vars[k]));
		}
	}
};

TEST_F(THMMCombinedScaledLadderInferred_5States_EMMockTest, EM) {
	// create mock latent variable and define expectations
	MockLatentVariable mockLatentVariable;

	// emission probabilities: get corresponding emission probabilities for this index
	EXPECT_CALL(mockLatentVariable, calculateEmissionProbabilities)
		.WillRepeatedly(Invoke(fillEmissions_5States_ScaledLadder));

	// EM updates of latent variable parameters: will be called, but don't have to return anything
	EXPECT_CALL(mockLatentVariable, getMLEmissionState).Times(N);
	EXPECT_CALL(mockLatentVariable, prepareEMParameterEstimationInitial).Times(1);
	EXPECT_CALL(mockLatentVariable, prepareEMParameterEstimationOneIteration).Times(AtLeast(1));
	EXPECT_CALL(mockLatentVariable, handleEMParameterEstimationOneIteration).Times(AtLeast(1));
	EXPECT_CALL(mockLatentVariable, finalizeEMParameterEstimationOneIteration).Times(AtLeast(1));
	EXPECT_CALL(mockLatentVariable, finalizeEMParameterEstimationFinal).Times(1);

	// now run EM!
	hmm->boxOnObs.runEMEstimation(mockLatentVariable);

	// check if correct kappa was estimated
	double absError = 0.05;
	EXPECT_NEAR(hmm->boxOnObs.kappasMusNus()[0], 0.01, absError); // kappa

	absError = 0.1;
	EXPECT_NEAR(hmm->boxOnObs.kappasMusNus()[1], 0.9, absError); // nu
	absError = 0.05;
	EXPECT_NEAR(hmm->boxOnObs.kappasMusNus()[2], 0.5, absError); // mu

	// check if posterior estimates of latent variable are close to truth
	auto fracDifferent =
		calcDiffZ("HMM/testFiles/trueZ_THMMCombinedScaledLadderInferred_5States_EMMock.txt", N, mockLatentVariable.z);
	EXPECT_TRUE(fracDifferent < 0.001);
}
