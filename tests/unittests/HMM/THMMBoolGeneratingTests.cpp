//
// Created by madleina on 02.11.22.
//

/*
#include <memory> // for shared_ptr, make_shared
#include <vector> // for vector

#include "gtest/gtest.h"

#include "stattools/commonTestCases/commonTestingFunctions.h"
#include "coretools/Math/TMatrix.h"                           // for TMatrix, stattools
#include "coretools/Storage/TNames.h"                         // for coretools::TNamesIndices, TNamesE...
#include "coretools/Types/commonWeakTypes.h"                  // for WeakType, StrictlyPosi...
#include "genometools/GenomePositions/TDistances.h"           // for TDistancesBinned::TDis...
#include "stattools/ParametersObservations/TDefinition.h"     // for TParameterDefinition
#include "stattools/ParametersObservations/TParameter.h"      // for TParameter::TPara...
#include "stattools/Priors/TPriorBase.h"                      // for TBase::getLogDensity
#include "stattools/Priors/TPriorHMMBoolGenerating.h"
#include "stattools/Priors/TPriorNormalMixedModel.h"

using namespace testing;
using namespace stattools;
using namespace stattools::prior;
*/

//-------------------------------------------
// Test:
// THMMBoolGeneratingMatrixInferred
// together with TNormalMixedModelInferred
//-------------------------------------------

/*
std::vector<double> true_mus() { return {-1.0, 1.0}; }
std::vector<double> true_vars() { return {0.2, 0.1}; }
std::vector<double> true_log_lambda() { return {-2.302585, -1.609438}; }

class THMMBoolGenerating_NormalMixedModel_Test : public Test {
protected:
    static constexpr size_t N = 1000;
    static constexpr size_t K = 2;

    using TypeObs                     = coretools::Unbounded;
    static constexpr size_t NumDimObs = 1;
    using SpecZ                       = stattools::ParamSpec<coretools::Boolean, coretools::toHash("z"), 1>;
    using SpecMus                     = stattools::ParamSpec<coretools::Unbounded, coretools::toHash("mus"), 1>;
    using SpecVars                    = stattools::ParamSpec<coretools::StrictlyPositive, coretools::toHash("vars"), 1>;
    using SpecLogLambda1 =
        stattools::ParamSpec<coretools::WeakTypeWithExp<double>, coretools::toHash("log_lambda_1"), 1>;
    using SpecLogLambda2 =
        stattools::ParamSpec<coretools::WeakTypeWithExp<double>, coretools::toHash("log_lambda_2"), 1>;
    using BoxTypeZ = THMMBoolGeneratingMatrixInferred<TParameterBase, SpecZ::value_type, SpecZ::numDim, SpecLogLambda1,
                                                      SpecLogLambda2>;
    using BoxType  = TNormalMixedModelInferred<TObservationBase, TypeObs, 1, SpecZ, SpecMus, SpecVars, K>;

    using TypeParamLogLambda1 = TParameter<SpecLogLambda1, BoxTypeZ>;
    using TypeParamLogLambda2 = TParameter<SpecLogLambda2, BoxTypeZ>;
    using TypeParamZ          = TParameter<SpecZ, BoxType>;
    using TypeParamMus        = TParameter<SpecMus, BoxType>;
    using TypeParamVars       = TParameter<SpecVars, BoxType>;

    std::unique_ptr<TypeParamLogLambda1> log_lambda_1;
    std::unique_ptr<TypeParamLogLambda2> log_lambda_2;
    std::array<std::unique_ptr<TypeParamMus>, 2> mus;
    std::array<std::unique_ptr<TypeParamVars>, 2> vars;
    std::unique_ptr<TypeParamZ> z;
    std::shared_ptr<BoxTypeZ> priorZ;
    std::shared_ptr<BoxType> prior;
    std::unique_ptr<TObservation<TypeObs, NumDimObs>> obs;

    std::unique_ptr<genometools::TDistancesBinnedBase> distances;

    void initialize(bool FixMixedModelParams, bool FixTransMat) {
        using namespace coretools::str;
        distances = createAndFillDistances("HMM/testFiles/positions_THMMBoolGenerating_NormalMixedModel.txt", 4);

        TParameterDefinition def("out");
        if (FixTransMat) { def.setInitVal(toString(true_log_lambda()[0])); }
        log_lambda_1 = createParameter<TUniformFixed, SpecLogLambda1, BoxTypeZ>("log_lambda_1", def);

        if (FixTransMat) { def.setInitVal(toString(true_log_lambda()[1])); }
        log_lambda_2 = createParameter<TUniformFixed, SpecLogLambda2, BoxTypeZ>("log_lambda_2", def);

        for (size_t i = 0; i < K; ++i) {
            TParameterDefinition defMu("out");
            if (FixMixedModelParams) { defMu.setInitVal(toString(true_mus()[i])); }
            mus[i] = createParameter<TUniformFixed, SpecMus, BoxType>("mus_" + toString(i), defMu);

            TParameterDefinition defVar("out");
            if (FixMixedModelParams) { defVar.setInitVal(toString(true_vars()[i])); }
            vars[i] = createParameter<TUniformFixed, SpecVars, BoxType>("vars_" + toString(i), defVar);
        }

        priorZ = createPrior<BoxTypeZ>(log_lambda_1.get(), log_lambda_2.get(), distances.get());
        z      = createParameter<BoxTypeZ, SpecZ, BoxType>("z", {"out"}, priorZ);

        std::array<TypeParamMus *, K> arrayMeans = {mus[0].get(), mus[1].get()};
        std::array<TypeParamVars *, K> arrayVars = {vars[0].get(), vars[1].get()};
        prior                                    = createPrior<BoxType>(z.get(), arrayMeans, arrayVars);

        auto data = readSingleLineIntoVec<double>("HMM/testFiles/data_THMMBoolGenerating_NormalMixedModel.txt");
        obs       = createObservation<TypeObs, NumDimObs>("obs", prior, {N}, {"out"}, data);

        // start initialization
        for (auto it : priorZ->getAllPriorParameters()) { it->startInitialization(); }
        for (auto it : prior->getAllPriorParameters()) { it->startInitialization(); }
    }
};

TEST_F(THMMBoolGenerating_NormalMixedModel_Test, fixMixedModelParams) {
    initialize(true, false);
    obs->guessInitialValues();

    // check if parameters of mixed model remained unchanged
    EXPECT_EQ(mus[0]->value(), true_mus()[0]);
    EXPECT_EQ(mus[1]->value(), true_mus()[1]);
    EXPECT_EQ(vars[0]->value(), true_vars()[0]);
    EXPECT_EQ(vars[1]->value(), true_vars()[1]);

    // check if correct transition matrices were estimated
    std::vector<std::vector<double>> simulatedQ1 = {{0.9136061, 0.08639393}, {0.1727879, 0.82721215}};
    std::vector<std::vector<double>> simulatedQ2 = {{0.8496039, 0.1503961}, {0.3007922, 0.6992078}};

    double absError = 0.08;

    auto transMat1 = priorZ->getTau(1);
    auto transMat2 = priorZ->getTau(2);

    // Q1
    EXPECT_NEAR(transMat1(0, 0), simulatedQ1[0][0], absError);
    EXPECT_NEAR(transMat1(0, 1), simulatedQ1[0][1], absError);
    EXPECT_NEAR(transMat1(1, 0), simulatedQ1[1][0], absError);
    EXPECT_NEAR(transMat1(1, 1), simulatedQ1[1][1], absError);

    // Q2
    EXPECT_NEAR(transMat2(0, 0), simulatedQ2[0][0], absError);
    EXPECT_NEAR(transMat2(0, 1), simulatedQ2[0][1], absError);
    EXPECT_NEAR(transMat2(1, 0), simulatedQ2[1][0], absError);
    EXPECT_NEAR(transMat2(1, 1), simulatedQ2[1][1], absError);

    // Lambda's
    EXPECT_NEAR(exp(log_lambda_1->value()), exp(true_log_lambda()[0]), absError);
    EXPECT_NEAR(exp(log_lambda_2->value()), exp(true_log_lambda()[1]), absError);

    // check if posterior estimates of latent variable are close to truth
    std::vector<uint8_t> trueZ =
        readSingleLineIntoVec<uint8_t>("HMM/testFiles/trueZ_THMMBoolGenerating_NormalMixedModel.txt");
    size_t counterDifferences = 0;
    for (size_t i = 0; i < N; i++) {
        if (trueZ[i] != z->value(i)) { counterDifferences++; }
    }
    double fracDifferent = static_cast<double>(counterDifferences) / N;
    EXPECT_TRUE(fracDifferent < 0.01);
}

TEST_F(THMMBoolGenerating_NormalMixedModel_Test, fixTransMatParams) {
    initialize(false, true);
    obs->guessInitialValues();

    // check if parameters of mixed model are close to truth
    double absError = 0.1;

    EXPECT_NEAR(mus[0]->value(), true_mus()[0], absError);
    EXPECT_NEAR(mus[1]->value(), true_mus()[1], absError);
    EXPECT_NEAR(vars[0]->value(), true_vars()[0], absError);
    EXPECT_NEAR(vars[1]->value(), true_vars()[1], absError);

    // check if transition matrices remained unchanged - are not exactly equal as in R, becuase taking the matrix
    // exponential from the generating matrix yields slightly different results
    std::vector<std::vector<double>> simulatedQ1 = {{0.9136061, 0.08639393}, {0.1727879, 0.82721215}};
    std::vector<std::vector<double>> simulatedQ2 = {{0.8496039, 0.1503961}, {0.3007922, 0.6992078}};

    auto transMat1 = priorZ->getTau(1);
    auto transMat2 = priorZ->getTau(2);

    absError = 0.01;
    // Q1
    EXPECT_NEAR(transMat1(0, 0), simulatedQ1[0][0], absError);
    EXPECT_NEAR(transMat1(0, 1), simulatedQ1[0][1], absError);
    EXPECT_NEAR(transMat1(1, 0), simulatedQ1[1][0], absError);
    EXPECT_NEAR(transMat1(1, 1), simulatedQ1[1][1], absError);

    // Q2
    EXPECT_NEAR(transMat2(0, 0), simulatedQ2[0][0], absError);
    EXPECT_NEAR(transMat2(0, 1), simulatedQ2[0][1], absError);
    EXPECT_NEAR(transMat2(1, 0), simulatedQ2[1][0], absError);
    EXPECT_NEAR(transMat2(1, 1), simulatedQ2[1][1], absError);

    // Lambda's
    EXPECT_NEAR(exp(log_lambda_1->value()), exp(true_log_lambda()[0]), absError);
    EXPECT_NEAR(exp(log_lambda_2->value()), exp(true_log_lambda()[1]), absError);

    // check if posterior estimates of latent variable are close to truth
    std::vector<uint8_t> trueZ =
        readSingleLineIntoVec<uint8_t>("HMM/testFiles/trueZ_THMMBoolGenerating_NormalMixedModel.txt");
    size_t counterDifferences = 0;
    for (size_t i = 0; i < N; i++) {
        if (trueZ[i] != z->value(i)) { counterDifferences++; }
    }
    double fracDifferent = static_cast<double>(counterDifferences) / N;
    EXPECT_TRUE(fracDifferent < 0.01);
}

TEST_F(THMMBoolGenerating_NormalMixedModel_Test, estimateBoth) {
    initialize(false, false);
    obs->guessInitialValues();

    // check if parameters of mixed model are close to truth
    double absError = 0.1;

    EXPECT_NEAR(mus[0]->value(), true_mus()[0], absError);
    EXPECT_NEAR(mus[1]->value(), true_mus()[1], absError);
    EXPECT_NEAR(vars[0]->value(), true_vars()[0], absError);
    EXPECT_NEAR(vars[1]->value(), true_vars()[1], absError);

    // check if correct transition matrices were estimated
    std::vector<std::vector<double>> simulatedQ1 = {{0.9136061, 0.08639393}, {0.1727879, 0.82721215}};
    std::vector<std::vector<double>> simulatedQ2 = {{0.8496039, 0.1503961}, {0.3007922, 0.6992078}};

    absError = 0.08;

    auto transMat1 = priorZ->getTau(1);
    auto transMat2 = priorZ->getTau(2);

    // Q1
    EXPECT_NEAR(transMat1(0, 0), simulatedQ1[0][0], absError);
    EXPECT_NEAR(transMat1(0, 1), simulatedQ1[0][1], absError);
    EXPECT_NEAR(transMat1(1, 0), simulatedQ1[1][0], absError);
    EXPECT_NEAR(transMat1(1, 1), simulatedQ1[1][1], absError);

    // Q2
    EXPECT_NEAR(transMat2(0, 0), simulatedQ2[0][0], absError);
    EXPECT_NEAR(transMat2(0, 1), simulatedQ2[0][1], absError);
    EXPECT_NEAR(transMat2(1, 0), simulatedQ2[1][0], absError);
    EXPECT_NEAR(transMat2(1, 1), simulatedQ2[1][1], absError);

    // Lambda's
    EXPECT_NEAR(exp(log_lambda_1->value()), exp(true_log_lambda()[0]), absError);
    EXPECT_NEAR(exp(log_lambda_2->value()), exp(true_log_lambda()[1]), absError);

    // check if posterior estimates of latent variable are close to truth
    std::vector<uint8_t> trueZ =
        readSingleLineIntoVec<uint8_t>("HMM/testFiles/trueZ_THMMBoolGenerating_NormalMixedModel.txt");
    size_t counterDifferences = 0;
    for (size_t i = 0; i < N; i++) {
        if (trueZ[i] != z->value(i)) { counterDifferences++; }
    }
    double fracDifferent = static_cast<double>(counterDifferences) / N;
    EXPECT_TRUE(fracDifferent < 0.01);
}
*/
