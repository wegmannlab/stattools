//
// Created by madleina on 02.11.22.
//

#include <memory> // for shared_ptr, make_shared
#include <vector> // for vector

#include "gmock/gmock.h"
#include "gtest/gtest.h"

#include "stattools/commonTestCases/THMMLadderTest.h"
#include "stattools/commonTestCases/commonTestingFunctions.h"
#include "EM/TMockLatentVariable.h"
#include "coretools/Distributions/TNormalDistr.h"
#include "genometools/GenomePositions/TDistances.h"      // for TDistancesBinned::TDis...
#include "testFiles/THMMLadder_Tables.h"

using namespace testing;
using namespace stattools;
using namespace stattools::prior;

//-------------------------------------------
// Test:
// TPriorHMMLadderWithHyperPrior
// together with mock latent variable
//-------------------------------------------

class THMMLadder_EMMock_Test : public Test {
protected:
	uint8_t numStates = 3;
	size_t N          = 5000;

	using TypeHMM = THMMLadderTest;

	std::unique_ptr<genometools::TDistancesBinnedBase> distances;
	std::unique_ptr<TypeHMM> hmm;
	std::unique_ptr<TypeHMM::SpecObs> obs;

	void SetUp() override {
		instances::dagBuilder().clear();

		distances = createAndFillDistances("HMM/testFiles/positions_THMMLadder_EMMock.txt", 4);
		hmm       = std::make_unique<TypeHMM>("out", numStates, distances.get());

		obs = createObservation<TypeHMM::Type, TypeHMM::NumDimObs, TypeHMM::BoxOnObs>("obs", &hmm->boxOnObs, {N},
																					  {"out"}, {});

		// start initialization
		for (auto it : instances::dagBuilder().getAllParameters()) { it->startInitialization(); }
	}
};

void fillEmissions_3States_Ladder(size_t Index, datVec &Emission) {
	std::vector<double> mus  = {-2, 0, 2};
	std::vector<double> vars = {0.07551818, 0.11816428, 0.01457067};

	for (size_t k = 0; k < Emission.size(); k++) {
		Emission[k] = coretools::probdist::TNormalDistr::density(getX_3States_Ladder()[Index], mus[k], sqrt(vars[k]));
	}
}

TEST_F(THMMLadder_EMMock_Test, EM) {
	// create mock latent variable and define expectations
	MockLatentVariable mockLatentVariable;

	// emission probabilities: get corresponding emission probabilities for this index
	EXPECT_CALL(mockLatentVariable, calculateEmissionProbabilities)
		.WillRepeatedly(Invoke(fillEmissions_3States_Ladder));

	// EM updates of latent variable parameters: will be called, but don't have to return anything
	EXPECT_CALL(mockLatentVariable, getMLEmissionState).Times(N);
	EXPECT_CALL(mockLatentVariable, prepareEMParameterEstimationInitial).Times(1);
	EXPECT_CALL(mockLatentVariable, prepareEMParameterEstimationOneIteration).Times(AtLeast(1));
	EXPECT_CALL(mockLatentVariable, handleEMParameterEstimationOneIteration).Times(AtLeast(1));
	EXPECT_CALL(mockLatentVariable, finalizeEMParameterEstimationOneIteration).Times(AtLeast(1));
	EXPECT_CALL(mockLatentVariable, finalizeEMParameterEstimationFinal).Times(1);

	// now run EM!
	hmm->boxOnObs.runEMEstimation(mockLatentVariable);

	// check if correct kappa was estimated
	double absError = 0.005;
	EXPECT_NEAR(hmm->boxOnObs.kappa(), 0.2, absError);

	// check if posterior estimates of latent variable are close to truth
	std::vector<uint8_t> trueZ = readSingleLineIntoVec<uint8_t>("HMM/testFiles/trueZ_THMMLadder_EMMock.txt");
	size_t counterDifferences  = 0;
	for (size_t i = 0; i < N; i++) {
		if (trueZ[i] != mockLatentVariable.z[i]) { counterDifferences++; }
	}
	double fracDifferent = static_cast<double>(counterDifferences) / N;
	EXPECT_TRUE(fracDifferent < 0.001);
}
