//
// Created by madleina on 05.08.20.
//

#include "gtest/gtest.h"

#include "stattools/commonTestCases/THMMBoolTest.h"
#include "stattools/commonTestCases/commonTestingFunctions.h"
#include "stattools/commonTestCases/TTwoMultivariateNormalMixedModelsTest.h"
#include "stattools/commonTestCases/TTwoNormalMixedModelsTest.h"

using namespace testing;
using namespace stattools;
using namespace stattools::prior;

template<typename T> std::vector<size_t> countDiffZ(std::string_view FilenameTrueZ, size_t N, T *z) {
	std::vector<uint8_t> trueZ = testing::readSingleLineIntoVec<uint8_t>(FilenameTrueZ);
	std::vector<size_t> indicesThatAreWronglyClassified;
	for (size_t i = 0; i < N; i++) {
		if (trueZ[i] != z->value(i)) { indicesThatAreWronglyClassified.push_back(i); }
	}
	return indicesThatAreWronglyClassified;
}

//--------------------------------------------
// THMMBool_TwoNormalMixedModelStretch_Test
//--------------------------------------------

class THMMBool_TwoNormalMixedModelStretch_Test : public Test {
protected:
	static constexpr size_t N = 5000;

	using TypeHMM = THMMBoolTest<false>;
	using TypeMM  = TTwoNormalMixedModelsTest<TypeHMM::BoxOnZ, true, 1>;

	std::unique_ptr<genometools::TDistancesBinnedBase> distances;
	std::unique_ptr<TypeHMM> hmm;
	std::unique_ptr<TypeMM> mixedModel;
	std::unique_ptr<TypeMM::SpecObs> obs;

	double true_mu                = 0.0;
	std::vector<double> true_vars = {0.1, 1.0};

	double true_pi    = 0.2;
	double true_gamma = 5.0;

	void SetUp() override { instances::dagBuilder().clear(); }

	void initialize(bool FixMixedModelParams, bool FixTransMat) {
		distances  = createAndFillDistances("HMM/testFiles/positions_THMMBool_TwoNormalMixedModelStretch.txt", 4);
		hmm        = std::make_unique<TypeHMM>("out", distances.get());
		mixedModel = std::make_unique<TypeMM>("out", &hmm->boxOnZ);
		hmm->boxOnZ.initialize();

		auto data = readSingleLineIntoVec<double>("HMM/testFiles/data_THMMBool_TwoNormalMixedModelStretch.txt");
		obs = createObservation<TypeMM::Type, TypeMM::numDimObs, TypeMM::BoxOnObs>("obs", &mixedModel->boxOnObs, {N},
																				   {"out"}, data);

		if (FixTransMat) {
			hmm->pi.set(true_pi);
			hmm->gamma.set(true_gamma);

			hmm->pi.fixInitialization();
			hmm->gamma.fixInitialization();
		}
		if (FixMixedModelParams) {
			mixedModel->mu.set(true_mu);
			mixedModel->var0.set(true_vars[0]);
			mixedModel->var1.set(true_vars[1]);

			mixedModel->mu.fixInitialization();
			mixedModel->var0.fixInitialization();
			mixedModel->var1.fixInitialization();
			mixedModel->boxOnObs.updateTempVals(&mixedModel->var1, 0, true);
		}

		// start initialization
		for (auto it : instances::dagBuilder().getAllParameters()) { it->startInitialization(); }
	}
};

TEST_F(THMMBool_TwoNormalMixedModelStretch_Test, fixMixedModelParams) {
	initialize(true, false);
	obs->guessInitialValues();

	// check if parameters of mixed model remained unchanged
	EXPECT_EQ(mixedModel->mu.value(), true_mu);
	EXPECT_EQ(mixedModel->var0.value(), true_vars[0]);
	EXPECT_EQ(mixedModel->var1.value(), true_vars[1]);

	// check if correct transition matrices were estimated
	std::vector<std::vector<double>> simulatedQ1 = {{0.9666667, 0.03333333}, {0.1333333, 0.86666667}};
	std::vector<std::vector<double>> simulatedQ2 = {{0.9388889, 0.06111111}, {0.2444444, 0.75555556}};

	double absError = 0.08;

	auto transMat1 = hmm->boxOnZ.getTau(1);
	auto transMat2 = hmm->boxOnZ.getTau(2);

	// Q1
	EXPECT_NEAR(transMat1(0, 0), simulatedQ1[0][0], absError);
	EXPECT_NEAR(transMat1(0, 1), simulatedQ1[0][1], absError);
	EXPECT_NEAR(transMat1(1, 0), simulatedQ1[1][0], absError);
	EXPECT_NEAR(transMat1(1, 1), simulatedQ1[1][1], absError);

	// Q2
	EXPECT_NEAR(transMat2(0, 0), simulatedQ2[0][0], absError);
	EXPECT_NEAR(transMat2(0, 1), simulatedQ2[0][1], absError);
	EXPECT_NEAR(transMat2(1, 0), simulatedQ2[1][0], absError);
	EXPECT_NEAR(transMat2(1, 1), simulatedQ2[1][1], absError);

	// transition matrix parameters
	EXPECT_NEAR(hmm->boxOnZ.pi(), true_pi, absError);
	EXPECT_NEAR(hmm->boxOnZ.gamma(), true_gamma, 0.4); // larger error, since limited number of peak widths

	// check if posterior estimates of latent variable are close to truth
	auto indicesThatAreWronglyClassified =
		countDiffZ("HMM/testFiles/trueZ_THMMBool_TwoNormalMixedModelStretch.txt", N, &mixedModel->z);
	double fracDifferent = static_cast<double>(indicesThatAreWronglyClassified.size()) / N;
	EXPECT_TRUE(fracDifferent < 0.15); // allow for more false positives, as model has a hard time classifying the x_i
									   // that are close zero to one or the other component

	for (auto &i : indicesThatAreWronglyClassified) {
		EXPECT_TRUE(obs->value(i) > -1.2 &&
					obs->value(i) < 1.2); // those that are wrongly classified have an x close to 0
	}
}

TEST_F(THMMBool_TwoNormalMixedModelStretch_Test, fixTransMatParams) {
	initialize(false, true);
	obs->guessInitialValues();

	// check if parameters of mixed model are close to truth
	double absError = 0.05;

	EXPECT_NEAR(mixedModel->mu.value(), 0., absError);
	EXPECT_NEAR(mixedModel->var0.value(), true_vars[0], absError);
	EXPECT_NEAR(mixedModel->var1.value(), 0.9, absError); // var0 + vars[1] = 1

	// check if transition matrices remained unchanged - are not exactly equal as in R, because taking the matrix
	// exponential from the generating matrix yields slightly different results
	std::vector<std::vector<double>> simulatedQ1 = {{0.9666667, 0.03333333}, {0.1333333, 0.86666667}};
	std::vector<std::vector<double>> simulatedQ2 = {{0.9388889, 0.06111111}, {0.2444444, 0.75555556}};

	auto transMat1 = hmm->boxOnZ.getTau(1);
	auto transMat2 = hmm->boxOnZ.getTau(2);

	absError = 0.01;
	// Q1
	EXPECT_NEAR(transMat1(0, 0), simulatedQ1[0][0], absError);
	EXPECT_NEAR(transMat1(0, 1), simulatedQ1[0][1], absError);
	EXPECT_NEAR(transMat1(1, 0), simulatedQ1[1][0], absError);
	EXPECT_NEAR(transMat1(1, 1), simulatedQ1[1][1], absError);

	// Q2
	EXPECT_NEAR(transMat2(0, 0), simulatedQ2[0][0], absError);
	EXPECT_NEAR(transMat2(0, 1), simulatedQ2[0][1], absError);
	EXPECT_NEAR(transMat2(1, 0), simulatedQ2[1][0], absError);
	EXPECT_NEAR(transMat2(1, 1), simulatedQ2[1][1], absError);

	// transition matrix parameters
	EXPECT_NEAR(hmm->boxOnZ.pi(), true_pi, absError);
	EXPECT_NEAR(hmm->boxOnZ.gamma(), true_gamma, absError);

	// check if posterior estimates of latent variable are close to truth
	auto indicesThatAreWronglyClassified =
		countDiffZ("HMM/testFiles/trueZ_THMMBool_TwoNormalMixedModelStretch.txt", N, &mixedModel->z);
	double fracDifferent = static_cast<double>(indicesThatAreWronglyClassified.size()) / N;
	EXPECT_TRUE(fracDifferent < 0.15); // allow for more false positives, as model has a hard time classifying the x_i
									   // that are close zero to one or the other component

	for (auto &i : indicesThatAreWronglyClassified) {
		EXPECT_TRUE(obs->value(i) > -1.2 &&
					obs->value(i) < 1.2); // those that are wrongly classified have an x close to 0
	}
}

TEST_F(THMMBool_TwoNormalMixedModelStretch_Test, estimateBoth) {
	initialize(false, false);
	obs->guessInitialValues();

	// check if parameters of mixed model are close to truth
	double absError = 0.1;

	EXPECT_NEAR(mixedModel->mu.value(), true_mu, absError);
	EXPECT_NEAR(mixedModel->var0.value(), true_vars[0], absError);
	EXPECT_NEAR(mixedModel->var1.value(), 0.9, absError); // var0 + vars[1] = 1

	// check if correct transition matrices were estimated
	std::vector<std::vector<double>> simulatedQ1 = {{0.9666667, 0.03333333}, {0.1333333, 0.86666667}};
	std::vector<std::vector<double>> simulatedQ2 = {{0.9388889, 0.06111111}, {0.2444444, 0.75555556}};

	absError = 0.08;

	auto transMat1 = hmm->boxOnZ.getTau(1);
	auto transMat2 = hmm->boxOnZ.getTau(2);

	// Q1
	EXPECT_NEAR(transMat1(0, 0), simulatedQ1[0][0], absError);
	EXPECT_NEAR(transMat1(0, 1), simulatedQ1[0][1], absError);
	EXPECT_NEAR(transMat1(1, 0), simulatedQ1[1][0], absError);
	EXPECT_NEAR(transMat1(1, 1), simulatedQ1[1][1], absError);

	// Q2
	EXPECT_NEAR(transMat2(0, 0), simulatedQ2[0][0], absError);
	EXPECT_NEAR(transMat2(0, 1), simulatedQ2[0][1], absError);
	EXPECT_NEAR(transMat2(1, 0), simulatedQ2[1][0], absError);
	EXPECT_NEAR(transMat2(1, 1), simulatedQ2[1][1], absError);

	// transition matrix parameters
	EXPECT_NEAR(hmm->boxOnZ.pi(), true_pi, absError);
	EXPECT_NEAR(hmm->boxOnZ.gamma(), true_gamma, 0.5); // larger error, since limited number of peak widths

	// check if posterior estimates of latent variable are close to truth
	auto indicesThatAreWronglyClassified =
		countDiffZ("HMM/testFiles/trueZ_THMMBool_TwoNormalMixedModelStretch.txt", N, &mixedModel->z);
	double fracDifferent = static_cast<double>(indicesThatAreWronglyClassified.size()) / N;
	EXPECT_TRUE(fracDifferent < 0.15); // allow for more false positives, as model has a hard time classifying the x_i
									   // that are close zero to one or the other component

	for (auto &i : indicesThatAreWronglyClassified) {
		EXPECT_TRUE(obs->value(i) > -1.2 &&
					obs->value(i) < 1.2); // those that are wrongly classified have an x close to 0
	}
}

//-------------------------------------------------------
// THMMBool_TwoMultivariateNormalStretchedMixedModel_Test
//-------------------------------------------------------

class THMMBool_TwoMultivariateNormalStretchedMixedModel_Test : public Test {
protected:
	size_t N = 1000;
	size_t D = 3;

	using TypeHMM = THMMBoolTest<false>;
	using TypeMM  = TTwoMulativariateNormalMixedModelsTest<TypeHMM::BoxOnZ>;

	std::unique_ptr<genometools::TDistancesBinnedBase> distances;
	std::unique_ptr<TypeHMM> hmm;
	std::unique_ptr<TypeMM> mixedModel;
	std::unique_ptr<TypeMM::SpecObs> obs;

	const std::vector<TypeMM::TypeMu> true_mus  = {-0.4988023, 1.9971286, -1.9975496};
	const std::vector<TypeMM::TypeMrr> true_mrr = {22.899099, 20.360399, 15.4947};
	const std::vector<TypeMM::TypeMrs> true_mrs = {11.4102, 12.9688, 14.424};
	const std::vector<TypeMM::TypeRho> true_rho = {11.1658, 24.5425, 19.8962};

	double true_pi    = 0.2;
	double true_gamma = 5.0;

	void SetUp() override { instances::dagBuilder().clear(); }

	void initialize(bool FixMixedModelParams, bool FixTransMat) {
		distances =
			createAndFillDistances("HMM/testFiles/positions_THMMBool_TwoMultivariateNormalStretchedMixedModel.txt", 4);
		hmm        = std::make_unique<TypeHMM>("out", distances.get());
		mixedModel = std::make_unique<TypeMM>("out", &hmm->boxOnZ);
		hmm->boxOnZ.initialize();

		auto data =
			readSingleLineIntoVec<double>("HMM/testFiles/data_THMMBool_TwoMultivariateNormalStretchedMixedModel.txt");
		obs = createObservation<TypeMM::Type, TypeMM::NumDimObs, TypeMM::BoxOnObs>("obs", &mixedModel->boxOnObs, {N, D},
																				   {"out"}, data);

		if (FixTransMat) {
			hmm->pi.set(true_pi);
			hmm->gamma.set(true_gamma);

			hmm->pi.fixInitialization();
			hmm->gamma.fixInitialization();
		}
		if (FixMixedModelParams) {
			mixedModel->mu.set(true_mus);
			mixedModel->m.set(1.0);
			mixedModel->Mrr.set(true_mrr);
			mixedModel->Mrs.set(true_mrs);
			mixedModel->rho.set(true_rho);

			mixedModel->mu.fixInitialization();
			mixedModel->m.fixInitialization();
			mixedModel->Mrr.fixInitialization();
			mixedModel->Mrs.fixInitialization();
			mixedModel->rho.fixInitialization();
		}

		// start initialization
		for (auto it : instances::dagBuilder().getAllParameters()) { it->startInitialization(); }
	}
};

TEST_F(THMMBool_TwoMultivariateNormalStretchedMixedModel_Test, fixMixedModelParams) {
	initialize(true, false);
	obs->guessInitialValues();

	// check if parameters of mixed model remained unchanged
	for (size_t d = 0; d < D; d++) {
		EXPECT_NEAR(mixedModel->mu.value(d), true_mus[d], 10e-05); // string conversion rounding
	}
	EXPECT_FLOAT_EQ(mixedModel->m.value(), 1.);
	for (size_t d = 0; d < D; d++) { EXPECT_FLOAT_EQ(mixedModel->Mrr.value(d), true_mrr[d]); }
	for (size_t d = 0; d < mixedModel->Mrs.size(); d++) { EXPECT_FLOAT_EQ(mixedModel->Mrs.value(d), true_mrs[d]); }
	for (size_t d = 0; d < D; d++) { EXPECT_FLOAT_EQ(mixedModel->rho.value(d), true_rho[d]); }

	// check if correct transition matrices were estimated
	std::vector<std::vector<double>> simulatedQ1 = {{0.9666667, 0.03333333}, {0.1333333, 0.86666667}};
	std::vector<std::vector<double>> simulatedQ2 = {{0.9388889, 0.06111111}, {0.2444444, 0.75555556}};

	double absError = 0.15;

	auto transMat1 = hmm->boxOnZ.getTau(1);
	auto transMat2 = hmm->boxOnZ.getTau(2);

	// Q1
	EXPECT_NEAR(transMat1(0, 0), simulatedQ1[0][0], absError);
	EXPECT_NEAR(transMat1(0, 1), simulatedQ1[0][1], absError);
	EXPECT_NEAR(transMat1(1, 0), simulatedQ1[1][0], absError);
	EXPECT_NEAR(transMat1(1, 1), simulatedQ1[1][1], absError);

	// Q2
	EXPECT_NEAR(transMat2(0, 0), simulatedQ2[0][0], absError);
	EXPECT_NEAR(transMat2(0, 1), simulatedQ2[0][1], absError);
	EXPECT_NEAR(transMat2(1, 0), simulatedQ2[1][0], absError);
	EXPECT_NEAR(transMat2(1, 1), simulatedQ2[1][1], absError);

	// Lambda's
	EXPECT_NEAR(hmm->boxOnZ.pi(), true_pi, absError);
	EXPECT_NEAR(hmm->boxOnZ.gamma(), true_gamma, 1.5); // larger error, since limited number of peak widths

	// check if posterior estimates of latent variable are close to truth
	auto indicesThatAreWronglyClassified =
		countDiffZ("HMM/testFiles/trueZ_THMMBool_TwoMultivariateNormalStretchedMixedModel.txt", N, &mixedModel->z);
	double fracDifferent = static_cast<double>(indicesThatAreWronglyClassified.size()) / (double)N;
	EXPECT_TRUE(fracDifferent < 0.15); // allow for more false positives, as model has a hard time classifying the x_i
									   // that are close zero to one or the other component
}

TEST_F(THMMBool_TwoMultivariateNormalStretchedMixedModel_Test, fixTransMatParams) {
	initialize(false, true);
	obs->guessInitialValues();

	// check if parameters of mixed model are close to truth ( leave quite an error, because model has a hard time
	// distinguishing the data points that are close to the center)
	double absError = 0.5;

	for (size_t d = 0; d < D; d++) { EXPECT_NEAR(mixedModel->mu.value(d), true_mus[d], absError); }
	EXPECT_NEAR(mixedModel->m.value(), 1., absError);
	for (size_t d = 0; d < D; d++) { EXPECT_NEAR(mixedModel->Mrr.value(d), true_mrr[d], absError); }
	for (size_t d = 0; d < mixedModel->Mrs.size(); d++) {
		EXPECT_NEAR(mixedModel->Mrs.value(d), true_mrs[d], absError);
	}
	for (size_t d = 0; d < D; d++) { EXPECT_NEAR(mixedModel->rho.value(d), true_rho[d], absError); }

	// check if transition matrices remained unchanged
	std::vector<std::vector<double>> simulatedQ1 = {{0.9666667, 0.03333333}, {0.1333333, 0.86666667}};
	std::vector<std::vector<double>> simulatedQ2 = {{0.9388889, 0.06111111}, {0.2444444, 0.75555556}};

	auto transMat1 = hmm->boxOnZ.getTau(1);
	auto transMat2 = hmm->boxOnZ.getTau(2);

	absError = 0.01;
	// Q1
	EXPECT_FLOAT_EQ(transMat1(0, 0), simulatedQ1[0][0]);
	EXPECT_FLOAT_EQ(transMat1(0, 1), simulatedQ1[0][1]);
	EXPECT_FLOAT_EQ(transMat1(1, 0), simulatedQ1[1][0]);
	EXPECT_FLOAT_EQ(transMat1(1, 1), simulatedQ1[1][1]);

	// Q2
	EXPECT_FLOAT_EQ(transMat2(0, 0), simulatedQ2[0][0]);
	EXPECT_FLOAT_EQ(transMat2(0, 1), simulatedQ2[0][1]);
	EXPECT_FLOAT_EQ(transMat2(1, 0), simulatedQ2[1][0]);
	EXPECT_FLOAT_EQ(transMat2(1, 1), simulatedQ2[1][1]);

	// Lambda's
	EXPECT_NEAR(hmm->boxOnZ.pi(), true_pi, absError);
	EXPECT_NEAR(hmm->boxOnZ.gamma(), true_gamma, absError);

	// check if posterior estimates of latent variable are close to truth
	auto indicesThatAreWronglyClassified =
		countDiffZ("HMM/testFiles/trueZ_THMMBool_TwoMultivariateNormalStretchedMixedModel.txt", N, &mixedModel->z);
	double fracDifferent = static_cast<double>(indicesThatAreWronglyClassified.size()) / (double)N;
	EXPECT_TRUE(fracDifferent < 0.15); // allow for more false positives, as model has a hard time classifying the x_i
									   // that are close zero to one or the other component
}

TEST_F(THMMBool_TwoMultivariateNormalStretchedMixedModel_Test, estimateBoth) {
	initialize(false, false);
	obs->guessInitialValues();

	// check if parameters of mixed model are close to truth
	double absError = 0.5;

	for (size_t d = 0; d < D; d++) { EXPECT_NEAR(mixedModel->mu.value(d), true_mus[d], absError); }
	EXPECT_NEAR(mixedModel->m.value(), 1., absError);
	for (size_t d = 0; d < D; d++) { EXPECT_NEAR(mixedModel->Mrr.value(d), true_mrr[d], absError); }
	for (size_t d = 0; d < mixedModel->Mrs.size(); d++) {
		EXPECT_NEAR(mixedModel->Mrs.value(d), true_mrs[d], absError);
	}
	for (size_t d = 0; d < D; d++) { EXPECT_NEAR(mixedModel->rho.value(d), true_rho[d], absError); }

	// check if correct transition matrices were estimated
	std::vector<std::vector<double>> simulatedQ1 = {{0.9666667, 0.03333333}, {0.1333333, 0.86666667}};
	std::vector<std::vector<double>> simulatedQ2 = {{0.9388889, 0.06111111}, {0.2444444, 0.75555556}};

	absError = 0.15;

	auto transMat1 = hmm->boxOnZ.getTau(1);
	auto transMat2 = hmm->boxOnZ.getTau(2);

	// Q1
	EXPECT_NEAR(transMat1(0, 0), simulatedQ1[0][0], absError);
	EXPECT_NEAR(transMat1(0, 1), simulatedQ1[0][1], absError);
	EXPECT_NEAR(transMat1(1, 0), simulatedQ1[1][0], absError);
	EXPECT_NEAR(transMat1(1, 1), simulatedQ1[1][1], absError);

	// Q2
	EXPECT_NEAR(transMat2(0, 0), simulatedQ2[0][0], absError);
	EXPECT_NEAR(transMat2(0, 1), simulatedQ2[0][1], absError);
	EXPECT_NEAR(transMat2(1, 0), simulatedQ2[1][0], absError);
	EXPECT_NEAR(transMat2(1, 1), simulatedQ2[1][1], absError);

	// Lambda's
	EXPECT_NEAR(hmm->boxOnZ.pi(), true_pi, absError);
	EXPECT_NEAR(hmm->boxOnZ.gamma(), true_gamma, 1.8); // larger error, since limited number of peak widths

	// check if posterior estimates of latent variable are close to truth
	auto indicesThatAreWronglyClassified =
		countDiffZ("HMM/testFiles/trueZ_THMMBool_TwoMultivariateNormalStretchedMixedModel.txt", N, &mixedModel->z);
	double fracDifferent = static_cast<double>(indicesThatAreWronglyClassified.size()) / (double)N;
	EXPECT_TRUE(fracDifferent < 0.15); // allow for more false positives, as model has a hard time classifying the x_i
									   // that are close zero to one or the other component
}

class THMMBool_ZAreKnown_Test : public Test {
protected:
	size_t N = 5000;

	using TypeHMM = THMMBoolTest<true>;

	std::unique_ptr<genometools::TDistancesBinnedBase> distances;
	std::unique_ptr<TypeHMM> hmm;
	std::unique_ptr<TypeHMM::SpecZ> obs;

	double true_pi    = 0.2;
	double true_gamma = 5.0;

	void SetUp() override {
		instances::dagBuilder().clear();

		distances = createAndFillDistances("HMM/testFiles/positions_THMMBool_ZAreKnown_Test.txt", 4);
		hmm       = std::make_unique<TypeHMM>("out", distances.get());

		auto data = readSingleLineIntoVec<bool>("HMM/testFiles/trueZ_THMMBool_ZAreKnown_Test.txt");
		obs = createObservation<TypeHMM::Type, TypeHMM::NumDimObs, TypeHMM::BoxOnZ>("obs", &hmm->boxOnZ, {N}, {"out"},
																					data);
	}

	void TearDown() override {}
};

TEST_F(THMMBool_ZAreKnown_Test, guessInitialValues_HiddenStateKnown) {
	// estimate transition matrix if z are known
	hmm->boxOnZ.guessInitialValues();

	// check if correct transition matrices were estimated
	std::vector<std::vector<double>> simulatedQ1 = {{0.9666667, 0.03333333}, {0.1333333, 0.86666667}};
	std::vector<std::vector<double>> simulatedQ2 = {{0.9388889, 0.06111111}, {0.2444444, 0.75555556}};

	double absError = 0.05;

	auto transMat1 = hmm->boxOnZ.getTau(1);
	auto transMat2 = hmm->boxOnZ.getTau(2);

	// Q1
	EXPECT_NEAR(transMat1(0, 0), simulatedQ1[0][0], absError);
	EXPECT_NEAR(transMat1(0, 1), simulatedQ1[0][1], absError);
	EXPECT_NEAR(transMat1(1, 0), simulatedQ1[1][0], absError);
	EXPECT_NEAR(transMat1(1, 1), simulatedQ1[1][1], absError);

	// Q2
	EXPECT_NEAR(transMat2(0, 0), simulatedQ2[0][0], absError);
	EXPECT_NEAR(transMat2(0, 1), simulatedQ2[0][1], absError);
	EXPECT_NEAR(transMat2(1, 0), simulatedQ2[1][0], absError);
	EXPECT_NEAR(transMat2(1, 1), simulatedQ2[1][1], absError);

	// Lambda's
	EXPECT_NEAR(hmm->boxOnZ.pi(), true_pi, absError);
	EXPECT_NEAR(hmm->boxOnZ.gamma(), true_gamma, 0.2); // larger error, since limited number of peak widths
}
