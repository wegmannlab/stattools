//
// Created by madleina on 02.11.22.
//

#include <memory> // for shared_ptr, make_shared
#include <vector> // for vector

#include "gmock/gmock.h"
#include "gtest/gtest.h"

#include "EM/TMockLatentVariable.h"
#include "coretools/Distributions/TNormalDistr.h"
#include "genometools/GenomePositions/TDistances.h" // for TDistancesBinned::TDis...
#include "stattools/HMMDistances/TOptimizeTransMatNelderMeadAttractorShift.h"
#include "stattools/commonTestCases/commonTestingFunctions.h"
#include "testFiles/THMMScaledLadderAttractor_Table.h"
#include "stattools/HMMDistances/TTransitionMatrixDist.h"
#include "stattools/HMMDistances/TTransitionMatrixHMMDistances.h"

using namespace testing;
using namespace stattools;

//-------------------------------------------
// Test:
// TTransitionMatrixScaledLadderAttractorShift
// together with mock latent variable
// first: 5 States
//-------------------------------------------

class THMMScaledLadderAttractorShift_EMMock_Test : public Test {
protected:
	uint8_t numStates = 0;
	size_t N          = 2000;

	using PrecisionType = double;
	using NumStatesType = size_t;
	using LengthType    = size_t;
	using TypeTransMat  = TTransitionMatrixScaledLadderAttractorShift<PrecisionType, NumStatesType>;
	using TypeOptimizer =
		TOptimizeTransMatNelderMeadAttractorShift<PrecisionType, NumStatesType, LengthType, TypeTransMat>;
	using TypeHMM = TTransitionMatrixDistances<PrecisionType, NumStatesType, LengthType>;

	std::unique_ptr<genometools::TDistancesBinnedBase> distances;
	std::shared_ptr<TypeOptimizer> optimizer;
	std::unique_ptr<TypeHMM> hmm;

	double simulatedKappa = 0.5;
	double simulatedNu    = 0.5;
	double simulatedMu    = 0.8;

	void SetUp() override { instances::dagBuilder().clear(); }

	void initialize(size_t NumStates) {
		numStates = NumStates;

		distances = createAndFillDistances("HMM/testFiles/positions_THMMScaledLadderAttractorShift_EMMock_" +
											   coretools::str::toString(numStates) + "States.txt",
										   10000);

		optimizer = std::make_shared<TypeOptimizer>(numStates);
		hmm       = std::make_unique<TypeHMM>(distances.get(), optimizer);
	}

	double calcDiffZ(std::string_view Filename, const MockLatentVariableNOMLEmissionState &mockLatentVariable) const {
		std::vector<uint8_t> trueZ = readSingleLineIntoVec<uint8_t>(Filename);
		size_t counterDifferences  = 0;
		for (size_t i = 0; i < N; i++) {
			if (trueZ[i] != mockLatentVariable.z[i]) { counterDifferences++; }
		}
		double fracDifferent = static_cast<double>(counterDifferences) / (double)N;
		return fracDifferent;
	}
};

template<size_t NumStates, size_t Ix> void fillEmissions(size_t Index, datVec &Emission) {
	// my desperate solution to calculate emission probabilities
	// didn't find another way to fill 2nd argument of calculateEmissionProbabilities,
	// so it is calculated in here every time (must be a free function)
	std::vector<double> mus;
	std::vector<double> vars;
	if constexpr (NumStates == 2) {
		mus  = {-6, 6};
		vars = {0.001, 0.001};
	} else if constexpr (NumStates == 3) {
		mus  = {-6, 0, 6};
		vars = {0.001, 0.001, 0.001};
	} else if constexpr (NumStates == 5) {
		mus  = {-6, -3, 0, 3, 6};
		vars = {0.001, 0.001, 0.001, 0.001, 0.001};
	}

	for (size_t k = 0; k < Emission.size(); k++) {
		if constexpr (NumStates == 5 && Ix == 0) {
			Emission[k] = coretools::probdist::TNormalDistr::density(getX_5States_ix0()[Index], mus[k], sqrt(vars[k]));
		} else if constexpr (NumStates == 5 && Ix == 2) {
			Emission[k] = coretools::probdist::TNormalDistr::density(getX_5States_ix2()[Index], mus[k], sqrt(vars[k]));
		} else if constexpr (NumStates == 2 && Ix == 0) {
			Emission[k] = coretools::probdist::TNormalDistr::density(getX_2States_ix0()[Index], mus[k], sqrt(vars[k]));
		} else if constexpr (NumStates == 3 && Ix == 0) {
			Emission[k] = coretools::probdist::TNormalDistr::density(getX_3States_ix0()[Index], mus[k], sqrt(vars[k]));
		} else if constexpr (NumStates == 3 && Ix == 1) {
			Emission[k] = coretools::probdist::TNormalDistr::density(getX_3States_ix1()[Index], mus[k], sqrt(vars[k]));
		}
	}
}

TEST_F(THMMScaledLadderAttractorShift_EMMock_Test, EM_2States_ix0) {
	initialize(2);

	// attractor at position 0
	size_t ix = 0;

	// create mock latent variable and define expectations
	MockLatentVariableNOMLEmissionState mockLatentVariable;

	// emission probabilities: get corresponding emission probabilities for this index
	EXPECT_CALL(mockLatentVariable, calculateEmissionProbabilities).WillRepeatedly(Invoke(fillEmissions<2, 0>));

	// EM updates of latent variable parameters: will be called, but don't have to return anything
	EXPECT_CALL(mockLatentVariable, prepareEMParameterEstimationInitial).Times(1);
	EXPECT_CALL(mockLatentVariable, prepareEMParameterEstimationOneIteration).Times(AtLeast(1));
	EXPECT_CALL(mockLatentVariable, handleEMParameterEstimationOneIteration).Times(AtLeast(1));
	EXPECT_CALL(mockLatentVariable, finalizeEMParameterEstimationOneIteration).Times(AtLeast(1));
	EXPECT_CALL(mockLatentVariable, finalizeEMParameterEstimationFinal).Times(1);

	// now run EM!
	hmm->runEMEstimation(mockLatentVariable);
	std::vector<double> values = hmm->getFinalParameterEstimatesEM();

	// check if correct kappa was estimated
	double absError = 0.08;
	EXPECT_NEAR(values[0], simulatedKappa, absError);
	EXPECT_NEAR(values[1], simulatedNu, absError);
	EXPECT_FLOAT_EQ(values[2], 1.0);
	EXPECT_EQ(values[3], ix);

	// check if posterior estimates of latent variable are close to truth
	double fracDifferent =
		calcDiffZ("HMM/testFiles/trueZ_THMMScaledLadderAttractorShift_EMMock_2States_ix0.txt", mockLatentVariable);
	EXPECT_TRUE(fracDifferent < 0.001);
}

TEST_F(THMMScaledLadderAttractorShift_EMMock_Test, EM_3States_ix0) {
	initialize(3);

	// attractor at position 0
	size_t ix = 0;

	// create mock latent variable and define expectations
	MockLatentVariableNOMLEmissionState mockLatentVariable;

	// emission probabilities: get corresponding emission probabilities for this index
	EXPECT_CALL(mockLatentVariable, calculateEmissionProbabilities).WillRepeatedly(Invoke(fillEmissions<3, 0>));

	// EM updates of latent variable parameters: will be called, but don't have to return anything
	EXPECT_CALL(mockLatentVariable, prepareEMParameterEstimationInitial).Times(1);
	EXPECT_CALL(mockLatentVariable, prepareEMParameterEstimationOneIteration).Times(AtLeast(1));
	EXPECT_CALL(mockLatentVariable, handleEMParameterEstimationOneIteration).Times(AtLeast(1));
	EXPECT_CALL(mockLatentVariable, finalizeEMParameterEstimationOneIteration).Times(AtLeast(1));
	EXPECT_CALL(mockLatentVariable, finalizeEMParameterEstimationFinal).Times(1);

	// now run EM!
	hmm->runEMEstimation(mockLatentVariable);
	std::vector<double> values = hmm->getFinalParameterEstimatesEM();

	// check if correct kappa was estimated
	double absError = 0.09;
	EXPECT_NEAR(values[0], simulatedKappa, absError);
	EXPECT_NEAR(values[1], simulatedNu, absError);
	EXPECT_NEAR(values[2], simulatedMu, absError);
	EXPECT_EQ(values[3], ix);

	// check if posterior estimates of latent variable are close to truth
	double fracDifferent =
		calcDiffZ("HMM/testFiles/trueZ_THMMScaledLadderAttractorShift_EMMock_3States_ix0.txt", mockLatentVariable);
	EXPECT_TRUE(fracDifferent < 0.001);
}

TEST_F(THMMScaledLadderAttractorShift_EMMock_Test, EM_3States_ix1) {
	initialize(3);

	// attractor at position 1
	size_t ix = 1;

	// create mock latent variable and define expectations
	MockLatentVariableNOMLEmissionState mockLatentVariable;

	// emission probabilities: get corresponding emission probabilities for this index
	EXPECT_CALL(mockLatentVariable, calculateEmissionProbabilities).WillRepeatedly(Invoke(fillEmissions<3, 1>));

	// EM updates of latent variable parameters: will be called, but don't have to return anything
	EXPECT_CALL(mockLatentVariable, prepareEMParameterEstimationInitial).Times(1);
	EXPECT_CALL(mockLatentVariable, prepareEMParameterEstimationOneIteration).Times(AtLeast(1));
	EXPECT_CALL(mockLatentVariable, handleEMParameterEstimationOneIteration).Times(AtLeast(1));
	EXPECT_CALL(mockLatentVariable, finalizeEMParameterEstimationOneIteration).Times(AtLeast(1));
	EXPECT_CALL(mockLatentVariable, finalizeEMParameterEstimationFinal).Times(1);

	// now run EM!
	hmm->runEMEstimation(mockLatentVariable);
	std::vector<double> values = hmm->getFinalParameterEstimatesEM();

	// check if correct kappa was estimated
	double absError = 0.08;
	EXPECT_NEAR(values[0], simulatedKappa, absError);
	EXPECT_NEAR(values[1], simulatedNu, absError);
	EXPECT_FLOAT_EQ(values[2], 1.0); // fix mu
	EXPECT_EQ(values[3], ix);

	// check if posterior estimates of latent variable are close to truth
	double fracDifferent =
		calcDiffZ("HMM/testFiles/trueZ_THMMScaledLadderAttractorShift_EMMock_3States_ix1.txt", mockLatentVariable);
	EXPECT_TRUE(fracDifferent < 0.001);
}

TEST_F(THMMScaledLadderAttractorShift_EMMock_Test, EM_5States_ix0) {
	initialize(5);

	// attractor at position 0
	size_t ix = 0;

	// create mock latent variable and define expectations
	MockLatentVariableNOMLEmissionState mockLatentVariable;

	// emission probabilities: get corresponding emission probabilities for this index
	EXPECT_CALL(mockLatentVariable, calculateEmissionProbabilities).WillRepeatedly(Invoke(fillEmissions<5, 0>));

	// EM updates of latent variable parameters: will be called, but don't have to return anything
	EXPECT_CALL(mockLatentVariable, prepareEMParameterEstimationInitial).Times(1);
	EXPECT_CALL(mockLatentVariable, prepareEMParameterEstimationOneIteration).Times(AtLeast(1));
	EXPECT_CALL(mockLatentVariable, handleEMParameterEstimationOneIteration).Times(AtLeast(1));
	EXPECT_CALL(mockLatentVariable, finalizeEMParameterEstimationOneIteration).Times(AtLeast(1));
	EXPECT_CALL(mockLatentVariable, finalizeEMParameterEstimationFinal).Times(1);

	// now run EM!
	hmm->runEMEstimation(mockLatentVariable);
	std::vector<double> values = hmm->getFinalParameterEstimatesEM();

	// check if correct kappa was estimated
	double absError = 0.08;
	EXPECT_NEAR(values[0], simulatedKappa, absError);
	EXPECT_NEAR(values[1], simulatedNu, absError);
	EXPECT_NEAR(values[2], simulatedMu, absError);
	EXPECT_EQ(values[3], ix);

	// check if posterior estimates of latent variable are close to truth
	double fracDifferent =
		calcDiffZ("HMM/testFiles/trueZ_THMMScaledLadderAttractorShift_EMMock_5States_ix0.txt", mockLatentVariable);
	EXPECT_TRUE(fracDifferent < 0.001);
}

TEST_F(THMMScaledLadderAttractorShift_EMMock_Test, EM_5States_ix2) {
	initialize(5);

	// attractor at position 2
	size_t ix = 2;

	// create mock latent variable and define expectations
	MockLatentVariableNOMLEmissionState mockLatentVariable;

	// emission probabilities: get corresponding emission probabilities for this index
	EXPECT_CALL(mockLatentVariable, calculateEmissionProbabilities).WillRepeatedly(Invoke(fillEmissions<5, 2>));

	// EM updates of latent variable parameters: will be called, but don't have to return anything
	EXPECT_CALL(mockLatentVariable, prepareEMParameterEstimationInitial).Times(1);
	EXPECT_CALL(mockLatentVariable, prepareEMParameterEstimationOneIteration).Times(AtLeast(1));
	EXPECT_CALL(mockLatentVariable, handleEMParameterEstimationOneIteration).Times(AtLeast(1));
	EXPECT_CALL(mockLatentVariable, finalizeEMParameterEstimationOneIteration).Times(AtLeast(1));
	EXPECT_CALL(mockLatentVariable, finalizeEMParameterEstimationFinal).Times(1);

	// now run EM!
	hmm->runEMEstimation(mockLatentVariable);
	std::vector<double> values = hmm->getFinalParameterEstimatesEM();

	// check if correct kappa was estimated
	double absError = 0.08;
	EXPECT_NEAR(values[0], simulatedKappa, absError);
	EXPECT_NEAR(values[1], simulatedNu, absError);
	EXPECT_NEAR(values[2], simulatedMu, absError);
	EXPECT_EQ(values[3], ix);

	// check if posterior estimates of latent variable are close to truth
	double fracDifferent =
		calcDiffZ("HMM/testFiles/trueZ_THMMScaledLadderAttractorShift_EMMock_5States_ix2.txt", mockLatentVariable);
	EXPECT_TRUE(fracDifferent < 0.001);
}
