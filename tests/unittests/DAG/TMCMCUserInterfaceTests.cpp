#include "gtest/gtest.h"

#include "stattools/DAG/TDAGBuilder.h"
#include "stattools/ParametersObservations/spec.h"
#include "stattools/ParametersObservations/TParameter.h"
#include "stattools/Priors/TPriorUniform.h"
#include "stattools/commonTestCases/commonTestingFunctions.h"
#include "coretools/Types/TStringHash.h"

using namespace stattools;

using Type = coretools::Unbounded;

// parameter
using BoxAbove  = prior::TUniformFixed<TParameterBase, Type, 1>;
using BoxAround = testing::DummyBox<Type, 1>;
using Spec      = ParamSpec<Type, Hash<coretools::toHash("a")>, BoxAbove>;

// observation
using BoxAboveObs = prior::TUniformFixed<TObservationBase, Type, 1>;
using Obs         = TObservation<Type, 1, BoxAboveObs>;

//--------------------------------------------
// TMCMCUserInterface
//--------------------------------------------

class BridgeTMCMCUserInterface : public TMCMCUserInterface {
public:
	void _matchConfig(TParameterDefinition &ParameterDefinition, std::string_view Name, std::string_view config,
					  std::string_view val) {
		TMCMCUserInterface::_matchConfig(ParameterDefinition, Name, config, val);
	}
	void _matchConfig(TObservationDefinition &ObservationDefinition, std::string_view Name, std::string_view config,
					  std::string_view val) {
		TMCMCUserInterface::_matchConfig(ObservationDefinition, Name, config, val);
	}
	void _checkHeaderConfigFile(std::string_view filename, const std::vector<std::string> &actualColNames) {
		TMCMCUserInterface::_checkHeaderConfigFile(filename, actualColNames);
	}
	void _parseInitVals(std::vector<TParameterBase *> &Parameters, std::vector<TObservationBase *> &Observations,
						coretools::TInputMaybeRcppFile &File) {
		TMCMCUserInterface::_parseInitVals(Parameters, Observations, File);
	}
	void _readParamConfigFile(std::vector<TParameterBase *> &Parameters,
							  std::vector<TObservationBase *> &Observations) {
		TMCMCUserInterface::_readParamConfigFile(Parameters, Observations);
	}
	void _readInitValFile(std::vector<TParameterBase *> &Parameters, std::vector<TObservationBase *> &Observations) {
		TMCMCUserInterface::_readInitValFile(Parameters, Observations);
	}
	void _parseCommandLineParamConfigs(std::vector<TParameterBase *> &Parameters,
									   std::vector<TObservationBase *> &Observations) {
		TMCMCUserInterface::_parseCommandLineParamConfigs(Parameters, Observations);
	}
	void _parseCommandLineParamInitVals(std::vector<TParameterBase *> &Parameters,
										std::vector<TObservationBase *> &Observations) {
		TMCMCUserInterface::_parseCommandLineParamInitVals(Parameters, Observations);
	}
};

class TMCMCUserInterfaceTest : public testing::Test {
public:
	BoxAbove boxAbove;
	BoxAboveObs boxAboveObs;

	BridgeTMCMCUserInterface interface;
	TParameterDefinition def;
	TObservationDefinition obsDef;

	std::unique_ptr<TParameter<Spec, BoxAround>> param1;
	std::unique_ptr<TParameter<Spec, BoxAround>> param2;
	std::unique_ptr<Obs> obs;

	std::vector<TParameterBase *> params;
	std::vector<TObservationBase *> observations;

	void SetUp() override {
		// clear all parameters from previous tests
		coretools::instances::parameters().clear();
		instances::dagBuilder().clear();

		param1 = std::make_unique<TParameter<Spec, BoxAround>>("def", &boxAbove, def);
		param2 = std::make_unique<TParameter<Spec, BoxAround>>("def2", &boxAbove, def);

		coretools::TMultiDimensionalStorage<Type, 1> storage;
		obs = std::make_unique<Obs>("obs", &boxAboveObs, storage, obsDef);

		params.push_back(param1.get());
		params.push_back(param2.get());
		observations.push_back(obs.get());
	}
};

TEST_F(TMCMCUserInterfaceTest, _matchConfig_parameters) {
	std::string config = "priorParameters";

	// nothing happens if val is empty:
	interface._matchConfig(def, "def", config, "");
	EXPECT_EQ(def.priorParameters(), "");

	def.setPriorParameters("");
	interface._matchConfig(def, "def", config, "2");
	EXPECT_EQ(def.priorParameters(), "2");

	def.setPriorParameters("2");
	interface._matchConfig(def, "def", config, "5");
	EXPECT_EQ(def.priorParameters(), "5");

	// -------- same applies to observation ---------
	// nothing happens if val is empty:
	interface._matchConfig(obsDef, "obs", config, "");
	EXPECT_EQ(obsDef.priorParameters(), "");

	obsDef.setPriorParameters("");
	interface._matchConfig(obsDef, "obs", config, "2");
	EXPECT_EQ(obsDef.priorParameters(), "2");

	obsDef.setPriorParameters("2");
	interface._matchConfig(obsDef, "obs", config, "5");
	EXPECT_EQ(obsDef.priorParameters(), "5");
}

TEST_F(TMCMCUserInterfaceTest, _matchConfig_trace) {
	std::string config = "traceFile";

	// nothing happens if val is empty:
	std::string val = "";
	interface._matchConfig(def, "def", config, val);
	EXPECT_EQ(def.getPrefix(stattools::MCMCFiles::trace), "");

	// trace can be changed
	val = "outer.txt";
	interface._matchConfig(def, "def", config, val);
	EXPECT_EQ(def.getPrefix(stattools::MCMCFiles::trace), val);

	// observation can not have trace file -> throw
	EXPECT_ANY_THROW(interface._matchConfig(obsDef, "def", config, val));
}

TEST_F(TMCMCUserInterfaceTest, _matchConfig_meanVar) {
	std::string config = "meanVarFile";

	// nothing happens if val is empty:
	std::string val = "";
	interface._matchConfig(def, "def", config, val);
	EXPECT_EQ(def.getPrefix(stattools::MCMCFiles::meanVar), "");

	// meanVar can be changed
	val = "outer.txt";
	interface._matchConfig(def, "def", config, val);
	EXPECT_EQ(def.getPrefix(stattools::MCMCFiles::meanVar), val);

	// observation can not have meanVar file -> throw
	EXPECT_ANY_THROW(interface._matchConfig(obsDef, "def", config, val));
}

TEST_F(TMCMCUserInterfaceTest, _matchConfig_simulation) {
	std::string config = "simulationFile";

	// nothing happens if val is empty:
	std::string val = "";
	interface._matchConfig(def, "def", config, val);
	EXPECT_EQ(def.getPrefix(stattools::MCMCFiles::simulation), "");

	// simulationFile can be changed
	val = "outer.txt";
	interface._matchConfig(def, "def", config, val);
	EXPECT_EQ(def.getPrefix(stattools::MCMCFiles::simulation), val);

	// same applies to observation:
	// nothing happens if val is empty:
	val = "";
	interface._matchConfig(obsDef, "def", config, val);
	EXPECT_EQ(obsDef.getPrefix(stattools::MCMCFiles::simulation), "");

	// simulationFile can be changed
	val = "outer.txt";
	interface._matchConfig(obsDef, "def", config, val);
	EXPECT_EQ(obsDef.getPrefix(stattools::MCMCFiles::simulation), val);
}

TEST_F(TMCMCUserInterfaceTest, _matchConfig_update) {
	std::string config = "update";

	// nothing happens if val is empty:
	std::string val = "";
	interface._matchConfig(def, "def", config, val);
	EXPECT_EQ(def.isUpdated(), true);

	// update can be changed
	val = "false";
	interface._matchConfig(def, "def", config, val);
	EXPECT_EQ(def.isUpdated(), false);

	// observation can not be updated -> throw
	EXPECT_ANY_THROW(interface._matchConfig(obsDef, "def", config, val));
}

TEST_F(TMCMCUserInterfaceTest, _matchConfig_propKernel) {
	std::string config = "propKernel";

	// nothing happens if val is empty:
	std::string val = "";
	interface._matchConfig(def, "def", config, val);
	EXPECT_EQ(def.propKernel(), ProposalKernel::normal);

	// propKernel can be changed
	val = "uniform";
	interface._matchConfig(def, "def", config, val);
	EXPECT_EQ(def.propKernel(), ProposalKernel::uniform);

	// observation can not have proposal kernel -> throw
	EXPECT_ANY_THROW(interface._matchConfig(obsDef, "def", config, val));
}

TEST_F(TMCMCUserInterfaceTest, _matchConfig_sharedJumpSize) {
	std::string config = "sharedJumpSize";

	// nothing happens if val is empty:
	std::string val = "";
	interface._matchConfig(def, "def", config, val);
	EXPECT_EQ(def.oneJumpSizeForAll(), true);

	// sharedJumpSize can be changed
	val = "true";
	interface._matchConfig(def, "def", config, val);
	EXPECT_EQ(def.oneJumpSizeForAll(), true);

	// observation can not have proposal kernel -> throw
	EXPECT_ANY_THROW(interface._matchConfig(obsDef, "def", config, val));
}

TEST_F(TMCMCUserInterfaceTest, _parseHeaderConfigFile) {
	// all names are given -> ok!
	std::vector<std::string> header = {"name",           "priorParameters", "traceFile",  "meanVarFile",
									   "simulationFile", "update",          "propKernel", "sharedJumpSize"};
	EXPECT_NO_THROW(interface._checkHeaderConfigFile("file.txt", header));

	// only some names are given, and not in right order -> still ok!
	header = {"meanVarFile", "traceFile", "update", "name"};
	EXPECT_NO_THROW(interface._checkHeaderConfigFile("file.txt", header));

	// name is not given -> throw
	header = {"meanVarFile", "traceFile", "update"};
	EXPECT_ANY_THROW(interface._checkHeaderConfigFile("file.txt", header));

	// duplicate colnames -> throw
	header = {"name", "update", "traceFile", "update"};
	EXPECT_ANY_THROW(interface._checkHeaderConfigFile("file.txt", header));

	// invalid colnames -> throw
	header = {"name", "min", "TraceFile", "max", "update"};
	EXPECT_ANY_THROW(interface._checkHeaderConfigFile("file.txt", header));
}

void writeInitVals(const std::vector<std::string> & vec) {
	coretools::TOutputFile out("tmp.txt");
	out.writeln(vec);
	out.close();
}

TEST_F(TMCMCUserInterfaceTest, _parseInitVals_1) {
	// open
	writeInitVals({"def", "0.1", "jumpSizes.txt"});
	coretools::TInputFile in("tmp.txt", coretools::FileType::NoHeader);
	interface._parseInitVals(params, observations, in);
	in.close();
	EXPECT_EQ(params.at(0)->getDefinition().initVal(), "0.1");
	EXPECT_EQ(params.at(0)->getDefinition().initJumpSizeProposal(), "jumpSizes.txt");
	remove("tmp.txt");
}

TEST_F(TMCMCUserInterfaceTest, _parseInitVals_2) {
	// array
	writeInitVals({"def2", "1", "jumpSizes1.txt"});
	coretools::TInputFile in("tmp.txt", coretools::FileType::NoHeader);
	interface._parseInitVals(params, observations, in);
	in.close();
	EXPECT_EQ(params.at(1)->getDefinition().initVal(), "1");
	EXPECT_EQ(params.at(1)->getDefinition().initJumpSizeProposal(), "jumpSizes1.txt");

	remove("tmp.txt");
}


TEST_F(TMCMCUserInterfaceTest, _parseInitVals_3) {
	// invalid parameter name
	writeInitVals({"whatever", "2", "jumpSizes2.txt"});
	coretools::TInputFile in("tmp.txt", coretools::FileType::NoHeader);
	EXPECT_ANY_THROW(interface._parseInitVals(params, observations, in));
	in.close();

	remove("tmp.txt");
}


TEST_F(TMCMCUserInterfaceTest, _parseInitVals_4) {
	// observation can not have initial values -> throw
	writeInitVals({"obs", "2", "jumpSizes.txt"});
	coretools::TInputFile in("tmp.txt", coretools::FileType::NoHeader);
	EXPECT_ANY_THROW(interface._parseInitVals(params, observations, in));
	in.close();

	remove("tmp.txt");
}

struct WriteConfigFile {
public:
	coretools::TOutputFile file;
	explicit WriteConfigFile(std::string_view name) {
		file.open(name, {"name", "update", "traceFile"});

		file.writeln("def", "false", "out.txt");
		file.writeln("def2", "", "out1.txt");
		file.close();
	}
};

TEST_F(TMCMCUserInterfaceTest, _readParamConfigFile) {
	// write config file
	WriteConfigFile write("config.txt");
	coretools::instances::parameters().add("config", "config.txt");

	// now call _readParamConfigFile
	interface._readParamConfigFile(params, observations);

	// check if all configurations have been accepted
	EXPECT_EQ(params.at(0)->getDefinition().isUpdated(), false);
	EXPECT_EQ(params.at(0)->getDefinition().getPrefix(stattools::MCMCFiles::trace), "out.txt");
	EXPECT_EQ(params.at(1)->getDefinition().isUpdated(), true);
	EXPECT_EQ(params.at(1)->getDefinition().getPrefix(stattools::MCMCFiles::trace), "out1.txt");

	remove("config.txt");
}

TEST_F(TMCMCUserInterfaceTest, _readParamConfigFile_paramNotGiven) {
	// write config file
	WriteConfigFile write("config.txt");

	// now call _readParamConfigFile -> parameter config is not given -> will do nothing
	interface._readParamConfigFile(params, observations);

	// check if nothing has changed
	EXPECT_EQ(params.at(0)->getDefinition().isUpdated(), true);
	EXPECT_EQ(params.at(0)->getDefinition().getPrefix(stattools::MCMCFiles::trace), "");

	EXPECT_EQ(params.at(1)->getDefinition().isUpdated(), true);
	EXPECT_EQ(params.at(1)->getDefinition().getPrefix(stattools::MCMCFiles::trace), "");

	remove("config.txt");
}

struct WriteConfigFile_invalid {
public:
	coretools::TOutputFile file;
	explicit WriteConfigFile_invalid(std::string_view name) {
		file.open(name, {"name", "update", "traceFile"});

		file.writeln("whatever", "false", "out.txt");
		file.close();
	}
};

TEST_F(TMCMCUserInterfaceTest, _readParamConfigFile_invalidParamName) {
	// write config file
	WriteConfigFile_invalid write("config.txt");
	coretools::instances::parameters().add("config", "config.txt");

	// now call _readParamConfigFile -> throws because of invalid param name
	EXPECT_ANY_THROW(interface._readParamConfigFile(params, observations));
	remove("config.txt");
}

struct WriteInitFile {
public:
	coretools::TOutputFile file;
	explicit WriteInitFile(std::string_view name) {
		std::vector<std::string> header = {"name", "value", "jumpSize"};
		file.open(name, header);
		file.writeln("def", 0.01, 1);
		file.writeln("def2", "", 2);
		file.close();
	}
};

TEST_F(TMCMCUserInterfaceTest, _readInitValFile) {
	// write config file
	WriteInitFile write("init.txt");
	coretools::instances::parameters().add("initVals", "init.txt");

	// now call _readInitValFile
	interface._readInitValFile(params, observations);

	// check if all configurations have been accepted
	EXPECT_EQ(params.at(0)->getDefinition().initVal(), "0.01");
	EXPECT_EQ(params.at(0)->getDefinition().initJumpSizeProposal(), "1");

	EXPECT_EQ(params.at(1)->getDefinition().initVal(), "0");
	EXPECT_EQ(params.at(1)->getDefinition().initJumpSizeProposal(), "2");

	remove("init.txt");
}

TEST_F(TMCMCUserInterfaceTest, _readInitValFile_paramNotGiven) {
	// write config file
	WriteInitFile write("init.txt");

	// now call _readInitValFile -> parameter config is not given -> will do nothing
	interface._readInitValFile(params, observations);

	// check if nothing has changed
	EXPECT_EQ(params.at(0)->getDefinition().initVal(), "0");
	EXPECT_EQ(params.at(0)->getDefinition().initJumpSizeProposal(), "1");

	EXPECT_EQ(params.at(1)->getDefinition().initVal(), "0");
	EXPECT_EQ(params.at(1)->getDefinition().initJumpSizeProposal(), "1");

	remove("init.txt");
}

struct WriteInitFile_invalid {
public:
	coretools::TOutputFile file;
	explicit WriteInitFile_invalid(std::string_view name) {
		std::vector<std::string> header = {"name", "value", "jumpSize"};
		file.open(name, header);

		file << "whatever" << 0.01 << 1 << coretools::endl;
		file.close();
	}
};

TEST_F(TMCMCUserInterfaceTest, _readInitValFile_invalidParamName) {
	// write config file
	WriteInitFile_invalid write("init.txt");
	coretools::instances::parameters().add("initVals", "init.txt");

	// now call _readInitValFile -> throws because of invalid param name
	EXPECT_ANY_THROW(interface._readInitValFile(params, observations));
}

TEST_F(TMCMCUserInterfaceTest, _parseCommandLineParamConfigs) {
	// add parameters
	coretools::instances::parameters().add("def.update", "false");
	coretools::instances::parameters().add("def.traceFile", "out.txt");
	coretools::instances::parameters().add("def.updatingScheme", "geometricUniform,regular");

	coretools::instances::parameters().add("def2.traceFile", "out1.txt");

	coretools::instances::parameters().add("obs.simulationFile", "sim.txt");

	// invalid parameter names will not throw, but simply be ignored (and listed in the end as unused parameters)
	coretools::instances::parameters().add("whatever.update", "false");

	interface._parseCommandLineParamConfigs(params, observations);
	// check if all configurations have been accepted
	EXPECT_EQ(params.at(0)->getDefinition().isUpdated(), false);
	EXPECT_EQ(params.at(0)->getDefinition().getPrefix(stattools::MCMCFiles::trace), "out.txt");

	EXPECT_EQ(params.at(1)->getDefinition().isUpdated(), true);
	EXPECT_EQ(params.at(1)->getDefinition().getPrefix(stattools::MCMCFiles::trace), "out1.txt");

	EXPECT_EQ(observations.at(0)->getDefinition().getPrefix(stattools::MCMCFiles::simulation), "sim.txt");

	remove("init.txt");
}

TEST_F(TMCMCUserInterfaceTest, _parseCommandLineParamInitVals) {
	// add parameters
	coretools::instances::parameters().add("def", "0.01");
	coretools::instances::parameters().add("def.jumpSize", "1");

	coretools::instances::parameters().add("def2.jumpSize", "2");

	// invalid parameter names will not throw, but simply be ignored (and listed in the end as unused parameters)
	coretools::instances::parameters().add("whatever", "-10");
	coretools::instances::parameters().add("whatever.jumpSize", "0.472");

	interface._parseCommandLineParamInitVals(params, observations);
	// check if all configurations have been accepted
	EXPECT_EQ(params.at(0)->getDefinition().initVal(), "0.01");
	EXPECT_EQ(params.at(0)->getDefinition().initJumpSizeProposal(), "1");

	EXPECT_EQ(params.at(1)->getDefinition().initVal(), "0");
	EXPECT_EQ(params.at(1)->getDefinition().initJumpSizeProposal(), "2");
}
