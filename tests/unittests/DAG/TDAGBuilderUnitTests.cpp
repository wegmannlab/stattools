//
// Created by caduffm on 7/7/20.
//

#include "gtest/gtest.h"

#include "stattools/DAG/TDAGBuilder.h"
#include "stattools/Priors/TPriorUniform.h"
#include "stattools/commonTestCases/commonTestingFunctions.h"
#include "stattools/ParametersObservations/spec.h"
#include "stattools/ParametersObservations/TParameter.h"
#include "coretools/Types/TStringHash.h"

using namespace stattools;
using Type = coretools::Unbounded;

// parameter
using BoxAbove  = prior::TUniformFixed<TParameterBase, Type, 1>;
using BoxAround = testing::DummyBox<Type, 1>;
using Spec      = ParamSpec<Type, Hash<coretools::toHash("a")>, BoxAbove>;

// observation
using BoxAboveObs = prior::TUniformFixed<TObservationBase, Type, 1>;
using Obs         = TObservation<Type, 1, BoxAboveObs>;

class TDAGBuilderTest : public testing::Test {
public:
	BoxAbove boxAbove;
	BoxAboveObs boxAboveObs;
	void SetUp() override { instances::dagBuilder().clear(); }
};

TEST_F(TDAGBuilderTest, addToDAG) {
	TParameter<Spec, BoxAround> param("def", &boxAbove, {});
	// other name
	TParameter<Spec, BoxAround> param2("def2", &boxAbove, {});

	// same name
	EXPECT_ANY_THROW((TParameter<Spec, BoxAround>("def", &boxAbove, {})));
	EXPECT_ANY_THROW(Obs("def2", &boxAboveObs, {}, {}););
}

TEST_F(TDAGBuilderTest, buildDAG_validParams) {
	TParameter<Spec, BoxAround> param("def", &boxAbove, {});
	param.setIsPartOfBox();

	TParameter<Spec, BoxAround> param2("def2", &boxAbove, {});
	param2.setIsPartOfBox();

	Obs obs("obs", &boxAboveObs, {}, {});
	Obs obs2("obs2", &boxAboveObs, {}, {});

	// now initialize
	EXPECT_NO_THROW(instances::dagBuilder().buildDAG());

	// now get vector
	auto vec = instances::dagBuilder().getAllParametersAndObservations();

	EXPECT_EQ(vec.size(), 4);
	EXPECT_EQ(vec[0]->name(), "def");
	EXPECT_EQ(vec[1]->name(), "def2");
	EXPECT_EQ(vec[2]->name(), "obs");
	EXPECT_EQ(vec[3]->name(), "obs2");
}

TEST_F(TDAGBuilderTest, _buildDAG_noObservation) {
	TParameter<Spec, BoxAround> param("def", &boxAbove, {});
	param.setIsPartOfBox();
	// don't add observation

	// now initialize
	EXPECT_ANY_THROW(instances::dagBuilder().buildDAG());
}

TEST_F(TDAGBuilderTest, _buildDAG_parameterWithoutObservation) {
	// build one valid DAG with observation
	TParameter<Spec, BoxAround> param("def", &boxAbove, {});
	param.setIsPartOfBox();

	TParameter<Spec, BoxAround> param2("def2", &boxAbove, {});
	param.setIsPartOfBox();

	Obs obs("obs", &boxAboveObs, {}, {});

	// add some other parameter, unrelated to rest
	TParameter<Spec, BoxAround> param3("def3", &boxAbove, {});

	// now initialize
	EXPECT_ANY_THROW(instances::dagBuilder().buildDAG());
}

TEST_F(TDAGBuilderTest, bundleParameterFilesNoFile) {
	TParameter<Spec, BoxAround> param("def", &boxAbove, {});
	param.setIsPartOfBox();

	Obs obs("obs", &boxAboveObs, {}, {});

	instances::dagBuilder().buildDAG();
	instances::dagBuilder().prepareAllMCMCFiles(false, false, "");

	EXPECT_TRUE(instances::dagBuilder().getTraceFiles().empty());
	EXPECT_TRUE(instances::dagBuilder().getMeanVarFiles().empty());
}

TEST_F(TDAGBuilderTest, bundleParameterFilesTraceAndMeanVar) {
	TParameterDefinition def;
	def.editFile(stattools::MCMCFiles::meanVar, "dummy");
	TParameter<Spec, BoxAround> param("def", &boxAbove, def);
	param.setIsPartOfBox();

	TParameterDefinition def2;
	def2.editFile(stattools::MCMCFiles::trace, "dummy");
	TParameter<Spec, BoxAround> param2("def2", &boxAbove, def2);
	param2.setIsPartOfBox();

	Obs obs("obs", &boxAboveObs, {}, {});

	instances::dagBuilder().buildDAG();
	instances::dagBuilder().prepareAllMCMCFiles(false, false, "");

	EXPECT_EQ(instances::dagBuilder().getTraceFiles().size(), 1);
	EXPECT_EQ(instances::dagBuilder().getMeanVarFiles().size(), 1);
	remove("dummy_trace.txt");
	remove("dummy_meanVar.txt");
}

TEST_F(TDAGBuilderTest, bundleParameterFilesTwoDifferentTraces) {
	TParameterDefinition def;
	def.editFile(stattools::MCMCFiles::trace, "dummy1");
	TParameter<Spec, BoxAround> param("def", &boxAbove, def);
	param.setIsPartOfBox();

	TParameterDefinition def2;
	def2.editFile(stattools::MCMCFiles::trace, "dummy2");
	TParameter<Spec, BoxAround> param2("def2", &boxAbove, def2);
	param2.setIsPartOfBox();

	Obs obs("obs", &boxAboveObs, {}, {});

	instances::dagBuilder().buildDAG();
	instances::dagBuilder().prepareAllMCMCFiles(false, false, "");

	EXPECT_EQ(instances::dagBuilder().getTraceFiles().size(), 2);
	EXPECT_TRUE(instances::dagBuilder().getMeanVarFiles().empty());
	remove("dummy1_trace.txt");
	remove("dummy2_trace.txt");
}
