//
// Created by caduffm on 7/2/21.
//

#include <cstdio>
#include <string>
#include <vector>

#include "gmock/gmock.h"
#include "gtest/gtest.h"

#include "coretools/Types/commonWeakTypes.h"
#include "stattools/ParametersObservations/TReadInitialValues.h"

using namespace stattools;
using namespace testing;
using namespace coretools;

class TReadInitialValuesTest : public Test {
public:
	TReadInitialValues<Unbounded, 1> reader;

	std::string initValue;
	std::vector<Unbounded> storage;
	std::string name = "name";

	void read(size_t Size) {
		storage.resize(Size);
		reader.readVals(initValue, storage, name);
	}
};

TEST_F(TReadInitialValuesTest, readValsOnlyOneNumber) {
	initValue = "1.";
	read(5);
	EXPECT_THAT(storage, ElementsAre(1., 1., 1., 1., 1.));
}

TEST_F(TReadInitialValuesTest, readValsOnlyOneBool_true) {
	TReadInitialValues<Boolean, 1> readerBool;
	std::vector<Boolean> storageBool(5);

	initValue = "true";
	readerBool.readVals(initValue, storageBool, name);
	EXPECT_EQ(storageBool.size(), 5);
	for (auto &it : storageBool) { EXPECT_EQ(it, true); }
}

TEST_F(TReadInitialValuesTest, readValsOnlyOneBool_false) {
	TReadInitialValues<Boolean, 1> readerBool;
	std::vector<Boolean> storageBool(5);

	initValue = "false";
	readerBool.readVals(initValue, storageBool, name);
	EXPECT_EQ(storageBool.size(), 5);
	for (auto &it : storageBool) { EXPECT_EQ(it, false); }
}

TEST_F(TReadInitialValuesTest, readValsVec) {
	initValue = "1.,1.";
	EXPECT_ANY_THROW(read(5)); // too few
	initValue = "1.,1.,1.,1.,1.,1.,1.,1.";
	EXPECT_ANY_THROW(read(5)); // too many

	// ok!
	initValue = "1.,1.,1.,1.,1.";
	read(5);
	EXPECT_THAT(storage, ElementsAre(1., 1., 1., 1., 1.));
	storage.clear();

	initValue = "1. ,2. ,5. ,6. ,2.";
	read(5);
	EXPECT_THAT(storage, ElementsAre(1., 2., 5., 6., 2.));
}

TEST_F(TReadInitialValuesTest, readValsFromFile_oneRow_tooFewCols) {
	// first write file
	coretools::TOutputFile file("vals.txt", 4);
	file.writeln(1.1, 2.4, -3.5, 4.9);
	file.close();

	// now read
	initValue = "vals.txt";
	EXPECT_ANY_THROW(read(5)); // too few

	// remove file
	remove("vals.txt");
}

TEST_F(TReadInitialValuesTest, readValsFromFile_oneRow_tooManyCols) {
	// first write file
	coretools::TOutputFile file("vals.txt", 6);
	file << 1.1 << 2.4 << -3.5 << 4.98 << 5.189 << 6.4 << coretools::endl; // one col too much
	file.close();

	// now read
	initValue = "vals.txt";
	EXPECT_ANY_THROW(read(5)); // too few

	// remove file
	remove("vals.txt");
}

TEST_F(TReadInitialValuesTest, readValsFromFile_oneRow_tooManyLines) {
	// first write file
	coretools::TOutputFile file("vals.txt", 5);
	file << 1.1 << 2.4 << -3.5 << 4.98 << 5.189 << coretools::endl;
	file << 1.1 << 2.4 << -3.5 << 4.98 << 5.189 << coretools::endl; // one line too much
	file.close();

	// now read
	initValue = "vals.txt";
	EXPECT_ANY_THROW(read(5)); // too many

	// remove file
	remove("vals.txt");
}

TEST_F(TReadInitialValuesTest, readValsFromFile_oneRow_ok) {
	// first write file
	coretools::TOutputFile file("vals.txt", 5);
	file << 1.1 << 2.4 << -3.5 << 4.98 << 5.189 << coretools::endl;
	file.close();

	// now read
	initValue = "vals.txt";
	read(5);
	EXPECT_THAT(storage, ElementsAre(1.1, 2.4, -3.5, 4.98, 5.189));

	// remove file
	remove("vals.txt");
}

TEST_F(TReadInitialValuesTest, readValsFromFile_oneCol_tooFewLines) {
	// first write file
	coretools::TOutputFile file("vals.txt", 1);
	file << 1.1 << coretools::endl;
	file << 2.4 << coretools::endl;
	file << -3.5 << coretools::endl;
	file << 4.98 << coretools::endl;
	file.close();

	// now read
	initValue = "vals.txt";
	EXPECT_ANY_THROW(read(5)); // too many

	// remove file
	remove("vals.txt");
}

TEST_F(TReadInitialValuesTest, readValsFromFile_oneCol_tooManyLines) {
	// first write file
	coretools::TOutputFile file("vals.txt", 1);
	file << 1.1 << coretools::endl;
	file << 2.4 << coretools::endl;
	file << -3.5 << coretools::endl;
	file << 4.98 << coretools::endl;
	file << 5.189 << coretools::endl;
	file << 7.3 << coretools::endl; // one line too much
	file.close();

	// now read
	initValue = "vals.txt";
	EXPECT_ANY_THROW(read(5)); // too many

	// remove file
	remove("vals.txt");
}

TEST_F(TReadInitialValuesTest, readValsFromFile_oneCol_ok) {
	// first write file
	coretools::TOutputFile file("vals.txt", 1);
	file << 1.1 << coretools::endl;
	file << 2.4 << coretools::endl;
	file << -3.5 << coretools::endl;
	file << 4.98 << coretools::endl;
	file << 5.189 << coretools::endl;
	file.close();

	// now read
	initValue = "vals.txt";
	read(5);
	EXPECT_THAT(storage, ElementsAre(1.1, 2.4, -3.5, 4.98, 5.189));

	// remove file
	remove("vals.txt");
}

void writeTraceFile() {
	// for 3 parameters:
	// a has size 1, and is integer
	// b has size 3
	// c has size 2x2
	coretools::TOutputFile file("trace.txt", {"a", "b_1", "b_2", "b_3", "c_1_1", "c_1_2", "c_2_1", "c_2_2"});
	file << 1 << 11.1 << 11.2 << 11.3 << 21.1 << 21.2 << 21.3 << 21.4 << coretools::endl; // iter 1
	file << 2 << 12.1 << 12.2 << 12.3 << 22.1 << 22.2 << 22.3 << 22.4 << coretools::endl; // iter 2
	file << 3 << 13.1 << 13.2 << 13.3 << 23.1 << 23.2 << 23.3 << 23.4 << coretools::endl; // ...
	file << 4 << 14.1 << 14.2 << 14.3 << 24.1 << 24.2 << 24.3 << 24.4 << coretools::endl;
	file << 5 << 15.1 << 15.2 << 15.3 << 25.1 << 25.2 << 25.3 << 25.4 << coretools::endl;
	file.close();
}

void writeMeanVarFile() {
	// for 3 parameters:
	// a has size 1
	// b has size 3
	// c has size 2x2
	coretools::TOutputFile file("meanVar.txt", {"name", "posterior_mean", "posterior_variance"});
	file << "a" << 1 << 2 << coretools::endl;
	file << "b_1" << 11.1 << 12.1 << coretools::endl;
	file << "b_2" << 11.2 << 12.2 << coretools::endl;
	file << "b_3" << 11.3 << 12.3 << coretools::endl;
	file << "c_1_1" << 21.1 << 22.1 << coretools::endl;
	file << "c_1_2" << 21.2 << 22.2 << coretools::endl;
	file << "c_2_1" << 21.3 << 22.3 << coretools::endl;
	file << "c_2_2" << 21.4 << 22.4 << coretools::endl;
	file.close();
}

void writeSimulationFile() {
	// for 3 parameters:
	// a has size 1
	// b has size 3
	// c has size 2x2
	coretools::TOutputFile file("simulated.txt", {"name", "value"});
	file << "a" << 1 << coretools::endl;
	file << "b_1" << 11.1 << coretools::endl;
	file << "b_2" << 11.2 << coretools::endl;
	file << "b_3" << 11.3 << coretools::endl;
	file << "c_1_1" << 21.1 << coretools::endl;
	file << "c_1_2" << 21.2 << coretools::endl;
	file << "c_2_1" << 21.3 << coretools::endl;
	file << "c_2_2" << 21.4 << coretools::endl;
	file.close();
}

TEST_F(TReadInitialValuesTest, readValsFromFile_trace) {
	// write trace
	writeTraceFile();

	// a
	using TypeA = UnsignedInt8;
	TReadInitialValues<TypeA, 1> readerA;
	coretools::TMultiDimensionalStorage<TValueUpdated<TypeA>, 1> storageA;
	storageA.resize(std::vector<size_t>(1, 1));
	readerA.readVals("trace.txt", storageA, "a");
	EXPECT_EQ(storageA.size(), 1);
	EXPECT_EQ((TypeA)storageA[0], 5);

	// b
	using TypeB = Unbounded;
	TReadInitialValues<TypeB, 1> readerB;
	coretools::TMultiDimensionalStorage<TValueUpdated<TypeB>, 1> storageB;
	storageB.resize(std::vector<size_t>(1, 3));
	readerB.readVals("trace.txt", storageB, "b");
	EXPECT_EQ(storageB.size(), 3);
	std::vector<double> vals = {15.1, 15.2, 15.3};
	for (size_t i = 0; i < 3; i++) { EXPECT_EQ((TypeB)storageB[i], vals[i]); }

	// c
	TReadInitialValues<TypeB, 2> readerC;
	coretools::TMultiDimensionalStorage<TValueUpdated<TypeB>, 2> storageC;
	storageC.resize(std::vector<size_t>(2, 2));
	readerC.readVals("trace.txt", storageC, "c");
	EXPECT_EQ(storageC.size(), 4);
	vals = {25.1, 25.2, 25.3, 25.4};
	for (size_t i = 0; i < 4; i++) { EXPECT_EQ((TypeB)storageC[i], vals[i]); }

	// remove file
	remove("trace.txt");
}

TEST_F(TReadInitialValuesTest, readValsFromFile_simulated) {
	// write simulated
	writeSimulationFile();

	// a
	using TypeA = UnsignedInt8;
	TReadInitialValues<TypeA, 1> readerA;
	coretools::TMultiDimensionalStorage<TValueUpdated<TypeA>, 1> storageA;
	storageA.resize(std::vector<size_t>(1, 1));
	readerA.readVals("simulated.txt", storageA, "a");
	EXPECT_EQ(storageA.size(), 1);
	EXPECT_EQ((TypeA)storageA[0], 1);

	// b
	using TypeB = Unbounded;
	TReadInitialValues<TypeB, 1> readerB;
	coretools::TMultiDimensionalStorage<TValueUpdated<TypeB>, 1> storageB;
	storageB.resize(std::vector<size_t>(1, 3));
	readerB.readVals("simulated.txt", storageB, "b");
	EXPECT_EQ(storageB.size(), 3);
	std::vector<double> vals = {11.1, 11.2, 11.3};
	for (size_t i = 0; i < 3; i++) { EXPECT_EQ((TypeB)storageB[i], vals[i]); }

	// c
	TReadInitialValues<TypeB, 2> readerC;
	coretools::TMultiDimensionalStorage<TValueUpdated<TypeB>, 2> storageC;
	storageC.resize(std::vector<size_t>(2, 2));
	readerC.readVals("simulated.txt", storageC, "c");
	EXPECT_EQ(storageC.size(), 4);
	vals = {21.1, 21.2, 21.3, 21.4};
	for (size_t i = 0; i < 4; i++) { EXPECT_EQ((TypeB)storageC[i], vals[i]); }

	// remove file
	remove("simulated.txt");
}

TEST_F(TReadInitialValuesTest, readValsFromFile_meanVar) {
	// write simulated
	writeMeanVarFile();

	// a
	using TypeA = UnsignedInt8;
	TReadInitialValues<TypeA, 1> readerA;
	coretools::TMultiDimensionalStorage<TValueUpdated<TypeA>, 1> storageA;
	storageA.resize(std::vector<size_t>(1, 1));
	readerA.readVals("meanVar.txt", storageA, "a");
	EXPECT_EQ(storageA.size(), 1);
	EXPECT_EQ((TypeA)storageA[0], 1);

	// b
	using TypeB = Unbounded;
	TReadInitialValues<TypeB, 1> readerB;
	coretools::TMultiDimensionalStorage<TValueUpdated<TypeB>, 1> storageB({3});
	storageB.resize(std::vector<size_t>(1, 3));
	readerB.readVals("meanVar.txt", storageB, "b");
	EXPECT_EQ(storageB.size(), 3);
	std::vector<double> vals = {11.1, 11.2, 11.3};
	for (size_t i = 0; i < 3; i++) { EXPECT_EQ((TypeB)storageB[i], vals[i]); }

	// c
	TReadInitialValues<TypeB, 2> readerC;
	coretools::TMultiDimensionalStorage<TValueUpdated<TypeB>, 2> storageC;
	storageC.resize(std::vector<size_t>(2, 2));
	readerC.readVals("meanVar.txt", storageC, "c");
	EXPECT_EQ(storageC.size(), 4);
	vals = {21.1, 21.2, 21.3, 21.4};
	for (size_t i = 0; i < 4; i++) { EXPECT_EQ((TypeB)storageC[i], vals[i]); }

	// remove file
	remove("meanVar.txt");
}
