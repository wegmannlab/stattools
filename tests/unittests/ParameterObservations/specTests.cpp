//
// Created by madleina on 15.12.23.
//

#include "coretools/Types/TStringHash.h"
#include "coretools/Types/probability.h"
#include "stattools/ParametersObservations/spec.h"
#include "gtest/gtest.h"

using namespace stattools;

TEST(Spec, spec_allDefault) {
	struct Dummy {};
	using Type         = coretools::Probability;
	using TypeBoxAbove = Dummy;

	using Spec = ParamSpec<Type, Hash<coretools::toHash("a")>, TypeBoxAbove>;

	static_assert(std::is_same_v<Spec::value_type, Type>);
	static_assert(std::is_same_v<Spec::typeBoxAbove, TypeBoxAbove>);
	static_assert(Spec::parallelize == false);
	static_assert(Spec::markovOrder == stattools::MarkovOrder::allDependent);
	static_assert(Spec::constraint::updateType == UpdateTypes::one);
	static_assert(Spec::constraint::constraint == Constraints::unconstrained);
	static_assert(Spec::constraint::numUpdatesPerIteration == 1);
	static_assert(Spec::weights.size() == 1);
	static_assert(Spec::weights[0] == UpdateWeights::regular);
}

TEST(Spec, spec_someDefault) {
	struct Dummy {};
	using Type         = coretools::Probability;
	using TypeBoxAbove = Dummy;

	using Spec = ParamSpec<Type, Hash<coretools::toHash("a")>, TypeBoxAbove,
						   Parallelize<stattools::MarkovOrder::different>, NumDim<2>>;

	static_assert(std::is_same_v<Spec::value_type, Type>);
	static_assert(std::is_same_v<Spec::typeBoxAbove, TypeBoxAbove>);
	static_assert(Spec::parallelize == true);
	static_assert(Spec::markovOrder == stattools::MarkovOrder::different);
	static_assert(Spec::constraint::updateType == UpdateTypes::one);
	static_assert(Spec::constraint::constraint == Constraints::unconstrained);
	static_assert(Spec::constraint::numUpdatesPerIteration == 1);
	static_assert(Spec::weights.size() == 2);
	static_assert(Spec::weights[0] == UpdateWeights::regular);
	static_assert(Spec::weights[1] == UpdateWeights::regular);
}

TEST(Spec, spec_noDefault) {
	struct Dummy {};
	using Type         = coretools::Probability;
	using TypeBoxAbove = Dummy;

	using Spec = ParamSpec<Type, Hash<coretools::toHash("a")>, TypeBoxAbove,
						   Weights<2, UpdateWeights::log10StatePosterior, UpdateWeights::geometricUniform>, NumDim<2>,
						   Parallelize<stattools::MarkovOrder::allDependent>, SumOne<0>>;

	static_assert(std::is_same_v<Spec::value_type, Type>);
	static_assert(std::is_same_v<Spec::typeBoxAbove, TypeBoxAbove>);
	static_assert(Spec::parallelize == false);
	static_assert(Spec::markovOrder == stattools::MarkovOrder::allDependent);
	static_assert(Spec::constraint::updateType == UpdateTypes::pair);
	static_assert(Spec::constraint::constraint == Constraints::sumOne);
	static_assert(Spec::constraint::numUpdatesPerIteration == 1);
	static_assert(Spec::weights.size() == 2);
	static_assert(Spec::weights[0] == UpdateWeights::log10StatePosterior);
	static_assert(Spec::weights[1] == UpdateWeights::geometricUniform);
}
