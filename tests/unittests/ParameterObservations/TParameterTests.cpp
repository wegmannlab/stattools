//
// Created by caduffm on 7/5/21.
//

#include <limits>
#include <memory>
#include <vector>

#include "gtest/gtest.h"

#include "coretools/Distributions/TNormalDistr.h"
#include "coretools/Storage/TNames.h"
#include "coretools/Types/commonWeakTypes.h"
#include "coretools/Types/probability.h"
#include "stattools/ParametersObservations/TDefinition.h"
#include "stattools/ParametersObservations/TParameter.h"
#include "stattools/Priors/TPriorNormal.h"
#include "stattools/Priors/TPriorUniform.h"
#include "stattools/commonTestCases/commonTestingFunctions.h"

using namespace testing;
using namespace stattools;

class TParameterTest : public Test {
protected:
	using Type     = coretools::Unbounded;
	using TypeBool = coretools::Boolean;

	using BoxOnParam_1D = prior::TNormalFixed<TParameterBase, Type, 1>;
	using BoxOnParam_2D = prior::TNormalFixed<TParameterBase, Type, 2>;
	using BoxOnBool     = prior::TUniformFixed<TParameterBase, TypeBool, 1>;

	// 1D parameters
	using TypeParam1D          = ParamSpec<Type, Hash<0>, BoxOnParam_1D, NumDim<1>>;
	using TypeParam1DLengthOne = ParamSpec<Type, Hash<0>, BoxOnParam_1D, NumDim<1>, LengthOne<0>>;
	using TypeParam1DSumOne    = ParamSpec<Type, Hash<0>, BoxOnParam_1D, NumDim<1>, SumOne<0>>;
	using TypeParamBool        = ParamSpec<TypeBool, Hash<0>, BoxOnBool, NumDim<1>>;

	// 2D parameters
	using TypeParam2D          = ParamSpec<Type, Hash<0>, BoxOnParam_2D, NumDim<2>>;
	using TypeParam2DLengthOne = ParamSpec<Type, Hash<0>, BoxOnParam_2D, NumDim<2>, LengthOne<1>>;
	using TypeParam2DSumOne    = ParamSpec<Type, Hash<0>, BoxOnParam_2D, NumDim<2>, SumOne<1>>;

	// boxes around
	using DB_1    = DummyBox<Type, 1>;
	using DB_2    = DummyBox<Type, 2>;
	using DB_bool = DummyBox<TypeBool, 1>;

	TParameterDefinition def;
	std::shared_ptr<coretools::TNamesEmpty> dimNames;

	// priors
	std::shared_ptr<BoxOnParam_1D> boxOnParam_1D;
	std::shared_ptr<BoxOnParam_2D> boxOnParam_2D;
	std::shared_ptr<BoxOnBool> boxOnBool;

	DB_1 box_1{};
	DB_2 box_2{};
	DB_bool box_bool{};

	void SetUp() override {
		instances::dagBuilder().clear();

		def.setPriorParameters("0,1");
		def.setDefaultFiles("test");
		dimNames = std::make_shared<coretools::TNamesIndices>();

		boxOnParam_1D = std::make_shared<BoxOnParam_1D>();
		boxOnParam_2D = std::make_shared<BoxOnParam_2D>();
		boxOnBool     = std::make_shared<BoxOnBool>();
	}
};

TEST_F(TParameterTest, constructor) {
	auto param = std::make_shared<TParameter<TypeParam1D, DB_1>>("param", boxOnParam_1D.get(), def);
	param->initStorage(&box_1);
	EXPECT_EQ(param->name(), "param");
	EXPECT_EQ(param->isUpdated(), true);
}

TEST_F(TParameterTest, constructorIsNotUpdated) {
	def.update(false);
	auto param = std::make_shared<TParameter<TypeParam1D, DB_1>>("param", boxOnParam_1D.get(), def);
	param->initStorage(&box_1);

	EXPECT_EQ(param->name(), "param");
	EXPECT_EQ(param->isUpdated(), false);
}

TEST_F(TParameterTest, initialize_Single_Default) {
	auto param = std::make_shared<TParameter<TypeParam1D, DB_1>>("param", boxOnParam_1D.get(), def);
	param->initStorage(&box_1);

	EXPECT_EQ(param->dimensions().at(0), 1);
	EXPECT_EQ(param->size(), 1);
	EXPECT_EQ(param->value(), std::numeric_limits<double>::lowest());
	EXPECT_FALSE(param->hasFixedInitialValue());
}

TEST_F(TParameterTest, initialize_Single_NonDefaultInitVal) {
	def.setInitVal("5.24");
	auto param = std::make_shared<TParameter<TypeParam1D, DB_1>>("param", boxOnParam_1D.get(), def);
	param->initStorage(&box_1);

	EXPECT_EQ(param->value(), 5.24);
	EXPECT_TRUE(param->hasFixedInitialValue());
}

TEST_F(TParameterTest, initialize_Array_Default) {
	auto param = std::make_shared<TParameter<TypeParam2D, DB_2>>("param", boxOnParam_2D.get(), def);
	param->initStorage(&box_2, {2, 3}, {dimNames, dimNames});

	EXPECT_EQ(param->dimensions().at(0), 2);
	EXPECT_EQ(param->dimensions().at(1), 3);
	EXPECT_EQ(param->size(), 3 * 2);

	for (size_t i = 0; i < 5; i++) { EXPECT_EQ(param->value(i), std::numeric_limits<double>::lowest()); }
}

TEST_F(TParameterTest, initialize_Array_NonDefaultInitVal) {
	def.setInitVal("1,2,3,4,5,6");
	def.setJumpSizeForAll(false);
	def.setInitJumpSizeProposal("0.1, 0.2, 0.3, 0.4, 0.5, 0.6");
	auto param = std::make_shared<TParameter<TypeParam2D, DB_2>>("param", boxOnParam_2D.get(), def);
	param->initStorage(&box_2, {2, 3}, {dimNames, dimNames});

	double val = 1;
	for (size_t i = 0; i < 5; i++) {
		EXPECT_EQ(param->value(i), val);
		val++;
	}
}

TEST_F(TParameterTest, getLogRatio_SingleElement) {
	def.setInitVal("10.");
	auto param = std::make_shared<TParameter<TypeParam1D, DB_1>>("param", boxOnParam_1D.get(), def);
	param->initStorage(&box_1);

	double logDensityOld = param->getLogDensity();
	param->set(1.);
	EXPECT_FLOAT_EQ(param->getLogDensityRatio(), 49.5);
	EXPECT_FLOAT_EQ(param->getLogDensityRatio(), param->getLogDensity() - logDensityOld);
}

TEST_F(TParameterTest, getLogRatio_Array) {
	def.setInitVal("0.");
	auto param = std::make_shared<TParameter<TypeParam1D, DB_1>>("param", boxOnParam_1D.get(), def);
	param->initStorage(&box_1, {5}, {dimNames});

	std::array<double, 5> logDensitiesOld{};
	for (size_t i = 0; i < 5; i++) { logDensitiesOld[i] = param->getLogDensity(i); }

	// set some values
	param->set(0, 0.);
	param->set(1, 1.);
	param->set(2, 2.);
	param->set(3, 3.);
	param->set(4, 4.);

	std::vector<double> expected = {0., -0.5, -2, -4.5, -8};
	for (size_t i = 0; i < 5; i++) {
		EXPECT_FLOAT_EQ(param->getLogDensityRatio(i), expected[i]);
		EXPECT_FLOAT_EQ(param->getLogDensityRatio(i), param->getLogDensity(i) - logDensitiesOld[i]);
	}
}

TEST_F(TParameterTest, update) {
	auto param = std::make_shared<TParameter<TypeParam1D, DB_1>>("param", boxOnParam_1D.get(), def);
	param->initStorage(&box_1, {5}, {dimNames});
	for (size_t i = 0; i < param->size(); ++i) { param->set(i, 0.0); }

	param->update(0);

	EXPECT_EQ(box_1.updateCounts.size(), 5);
	EXPECT_EQ(box_1.updateTmpValsCounts.size(), 5);
	for (size_t i = 0; i < param->size(); ++i) {
		EXPECT_EQ(box_1.updateCounts[i], 1);
		EXPECT_EQ(box_1.updateTmpValsCounts[i], 1);
	}
}

TEST_F(TParameterTest, update_isUpdatedFalse) {
	def.update(false);
	auto param = std::make_shared<TParameter<TypeParam1D, DB_1>>("param", boxOnParam_1D.get(), def);
	param->initStorage(&box_1, {5}, {dimNames});
	for (size_t i = 0; i < param->size(); ++i) { param->set(i, 0.0); }

	param->update(0);

	EXPECT_EQ(box_1.updateCounts.size(), 0);
	EXPECT_EQ(box_1.updateTmpValsCounts.size(), 0);
}

TEST_F(TParameterTest, normalize) {
	auto param = std::make_shared<TParameter<TypeParam1DSumOne, DB_1>>("param", boxOnParam_1D.get(), def);
	param->initStorage(&box_1, {5}, {dimNames});

	// set some values
	param->set(0, 0.01);
	param->set(1, 0.05);
	param->set(2, 0.1);
	param->set(3, 0.24);
	param->set(4, 0.6);
	EXPECT_FLOAT_EQ(param->sum(), 1.0);

	// change all a little bit
	for (size_t i = 0; i < 5; ++i) { param->set(i, param->value(i) + 0.01); }

	// normalize
	param->normalize(param->getFull());

	// check
	EXPECT_FLOAT_EQ(param->value(0), 0.01904762);
	EXPECT_FLOAT_EQ(param->value(1), 0.05714286);
	EXPECT_FLOAT_EQ(param->value(2), 0.10476190);
	EXPECT_FLOAT_EQ(param->value(3), 0.23809524);
	EXPECT_FLOAT_EQ(param->value(4), 0.58095238);
	EXPECT_FLOAT_EQ(param->sum(), 1.0);

	// oldValue: same as before
	EXPECT_FLOAT_EQ(param->oldValue(0), 0.01);
	EXPECT_FLOAT_EQ(param->oldValue(1), 0.05);
	EXPECT_FLOAT_EQ(param->oldValue(2), 0.1);
	EXPECT_FLOAT_EQ(param->oldValue(3), 0.24);
	EXPECT_FLOAT_EQ(param->oldValue(4), 0.6);
}

TEST_F(TParameterTest, update_DoReject_SingleElement) {
	def.update(true);
	def.setInitVal("10.");
	auto param = std::make_shared<TParameter<TypeParam1D, DB_1>>("param", boxOnParam_1D.get(), def);
	param->initStorage(&box_1);

	param->propose(coretools::TRange(0));
	EXPECT_EQ(param->oldValue(), 10.);
	EXPECT_TRUE(param->oldValue() != param->value());

	double logH = -1000; // super bad -> should always reject
	EXPECT_FALSE(param->acceptOrReject(logH, coretools::TRange(0)));
	EXPECT_FLOAT_EQ(param->oldValue(), 10.);
	EXPECT_FLOAT_EQ(param->value(), 10.);
	EXPECT_EQ(param->getPtrToUpdater()->acceptanceRate(0), 0.5); // = (0 + 1) / (1 + 1)

	// now without counting rejection
	param->propose(coretools::TRange(0));
	EXPECT_FALSE(param->acceptOrReject(logH, coretools::TRange(0), {}));
	EXPECT_EQ(param->getPtrToUpdater()->acceptanceRate(0), 2. / 3.); // = (1 + 1) / (2 + 1)
}

TEST_F(TParameterTest, update_DoAccept_SingleElement) {
	def.setInitVal("10.");
	auto param = std::make_shared<TParameter<TypeParam1D, DB_1>>("param", boxOnParam_1D.get(), def);
	param->initStorage(&box_1);

	param->propose(coretools::TRange(0));
	EXPECT_EQ(param->oldValue(), 10.);
	EXPECT_TRUE(param->oldValue() != param->value());

	double logH = 1000; // super good -> always accept
	EXPECT_TRUE(param->acceptOrReject(logH, coretools::TRange(0)));
	EXPECT_EQ(param->oldValue(), 10.);
	EXPECT_TRUE(param->oldValue() != param->value());
	EXPECT_EQ(param->getPtrToUpdater()->acceptanceRate(0), 1.0);
}

TEST_F(TParameterTest, update_DoReject_Array) {
	def.setInitVal("0.");
	auto param = std::make_shared<TParameter<TypeParam1D, DB_1>>("param", boxOnParam_1D.get(), def);
	param->initStorage(&box_1, {5}, {dimNames});
	for (size_t i = 0; i < param->size(); ++i) { param->set(i, 0.0); }

	// update
	for (size_t i = 0; i < param->size(); ++i) {
		param->propose(coretools::TRange(i));
		EXPECT_EQ(param->oldValue(i), 0.);
		EXPECT_TRUE(param->oldValue(i) != param->value(i));
	}

	// ...and reject
	double logH = -1000; // super bad
	for (size_t i = 0; i < 5; i++) {
		EXPECT_FALSE(param->acceptOrReject(logH, coretools::TRange(i)));
		EXPECT_EQ(param->oldValue(i), 0.);
		EXPECT_EQ(param->value(i), 0.);
	}
	EXPECT_EQ(param->getPtrToUpdater()->acceptanceRate(0), 1. / 6.); // = (0 + 1) / (5 + 1)

	// update again, but then don't count rejection
	for (size_t i = 0; i < 5; i++) {
		param->propose(coretools::TRange(i));
		EXPECT_FALSE(param->acceptOrReject(logH, coretools::TRange(i), {}));
	}
	EXPECT_EQ(param->getPtrToUpdater()->acceptanceRate(0), 6. / 11.); // = (5 + 1) / (10 + 1)
}

TEST_F(TParameterTest, update_DoAccept_Array) {
	def.setInitVal("0.");
	auto param = std::make_shared<TParameter<TypeParam1D, DB_1>>("param", boxOnParam_1D.get(), def);
	param->initStorage(&box_1, {5}, {dimNames});
	for (size_t i = 0; i < param->size(); ++i) { param->set(i, 0.0); }

	// update
	for (size_t i = 0; i < param->size(); ++i) {
		param->propose(coretools::TRange(i));
		EXPECT_EQ(param->oldValue(i), 0.);
		EXPECT_TRUE(param->oldValue(i) != param->value(i));
	}

	// ...and accept
	double logH = 1000; // super good
	for (size_t i = 0; i < 5; i++) {
		EXPECT_TRUE(param->acceptOrReject(logH, coretools::TRange(i)));
		EXPECT_EQ(param->oldValue(i), 0.);
		EXPECT_TRUE(param->oldValue(i) != param->value(i));
	}
}

TEST_F(TParameterTest, update_DoReject_Array_IxProposed) {
	def.setInitVal("0.");
	auto param = std::make_shared<TParameter<TypeParam1D, DB_1>>("param", boxOnParam_1D.get(), def);
	param->initStorage(&box_1, {5}, {dimNames});
	for (size_t i = 0; i < param->size(); ++i) { param->set(i, 0.0); }

	// update only first index, set all others deterministically
	param->propose(coretools::TRange(0));
	for (size_t i = 1; i < 5; i++) { param->set(i, (double)i); }

	// ...and reject
	double logH = -1000;                                                        // super bad
	EXPECT_FALSE(param->acceptOrReject(logH, {0, 5, 1}, coretools::TRange(0))); // only first element was proposed

	// should reset the entire range
	for (size_t i = 0; i < 5; i++) {
		EXPECT_EQ(param->oldValue(i), 0.);
		EXPECT_EQ(param->value(i), 0.);
	}
	// but acceptance rate counters are still correct!
	EXPECT_EQ(param->getPtrToUpdater()->acceptanceRate(0), 0.5); // = (0 + 1) / (1 + 1)
}

TEST_F(TParameterTest, update_length_one_pairwise) {
	coretools::instances::parameters().add("numThreads", 2);
	auto param = std::make_shared<TParameter<TypeParam1DLengthOne, DB_1>>("param", boxOnParam_1D.get(), def);
	param->initStorage(&box_1, {5}, {dimNames});

	std::vector<double> initVals = {0.134839972492648, 0.269679944985297, 0.404519917477945, 0.539359889970594,
									0.674199862463242};
	for (size_t i = 0; i < 5; ++i) { param->set(i, initVals[i]); }

	// update
	size_t numRep = 1000;
	for (size_t r = 0; r < numRep; ++r) {
		param->update(r);

		EXPECT_FLOAT_EQ(param->vectorNorm(param->getFull()), 1.0);
		EXPECT_EQ(coretools::containerSum(box_1.updateCounts), 4); // 4 out of 5 elements can be updated with pairs
		EXPECT_EQ(coretools::containerSum(box_1.updateTmpValsCounts), 4);

		for (size_t i = 0; i < box_1.accepted.size(); ++i) {
			if (box_1.accepted[i]) { EXPECT_NE(param->value(i), param->oldValue(i)); }
		}
		for (size_t i = 0; i < box_1.rejected.size(); ++i) {
			if (box_1.rejected[i]) { EXPECT_EQ(param->value(i), param->oldValue(i)); }
		}

		box_1.clear();
	}
}

TEST_F(TParameterTest, update_2D_length_one_pairwise) {
	coretools::instances::parameters().add("numThreads", 2);
	auto param = std::make_shared<TParameter<TypeParam2DLengthOne, DB_2>>("param", boxOnParam_2D.get(), def);
	param->initStorage(&box_2, {2, 5}, {dimNames, dimNames});

	std::vector<double> initVals = {0.134839972492648, 0.269679944985297,  0.404519917477945,  0.539359889970594,
									0.674199862463242, 0.0857237227378754, 0.0419280944739101, 0.676964614923248,
									0.450583438447973, 0.574096641160673};
	for (size_t i = 0; i < initVals.size(); ++i) { param->set(i, initVals[i]); }

	// update
	size_t numRep = 1000;
	for (size_t r = 0; r < numRep; ++r) {
		param->update(r);

		EXPECT_FLOAT_EQ(param->vectorNorm(param->get1DSlice(1, {0, 0})), 1.0);
		EXPECT_FLOAT_EQ(param->vectorNorm(param->get1DSlice(1, {1, 0})), 1.0);
		// 4 out of 5 elements can be updated with pairs (per row)
		EXPECT_EQ(coretools::containerSum(box_2.updateCounts), 8);
		EXPECT_EQ(coretools::containerSum(box_2.updateTmpValsCounts), 8);

		for (size_t i = 0; i < box_2.accepted.size(); ++i) {
			if (box_2.accepted[i]) { EXPECT_NE(param->value(i), param->oldValue(i)); }
		}
		for (size_t i = 0; i < box_2.rejected.size(); ++i) {
			if (box_2.rejected[i]) { EXPECT_EQ(param->value(i), param->oldValue(i)); }
		}

		box_2.clear();
	}
}

TEST_F(TParameterTest, update_2D_length_one_joint) {
	coretools::instances::parameters().add("numThreads", 2);
	using T = ParamSpec<Type, Hash<0>, BoxOnParam_2D, NumDim<2>, LengthOne<1, UpdateTypes::joint>>;

	auto param = std::make_shared<TParameter<T, DB_2>>("param", boxOnParam_2D.get(), def);
	param->initStorage(&box_2, {2, 5}, {dimNames, dimNames});

	std::vector<double> initVals = {0.134839972492648, 0.269679944985297,  0.404519917477945,  0.539359889970594,
									0.674199862463242, 0.0857237227378754, 0.0419280944739101, 0.676964614923248,
									0.450583438447973, 0.574096641160673};
	for (size_t i = 0; i < initVals.size(); ++i) { param->set(i, initVals[i]); }

	// update
	size_t numRep = 1000;
	for (size_t r = 0; r < numRep; ++r) {
		param->update(r);

		EXPECT_FLOAT_EQ(param->vectorNorm(param->get1DSlice(1, {0, 0})), 1.0);
		EXPECT_FLOAT_EQ(param->vectorNorm(param->get1DSlice(1, {1, 0})), 1.0);
		for (size_t i = 0; i < initVals.size(); ++i) {
			EXPECT_EQ(box_2.updateCounts[i], 1);
			EXPECT_EQ(box_2.updateTmpValsCounts[i], 1);
		}

		for (size_t i = 0; i < box_2.accepted.size(); ++i) {
			if (box_2.accepted[i]) { EXPECT_NE(param->value(i), param->oldValue(i)); }
		}
		for (size_t i = 0; i < box_2.rejected.size(); ++i) {
			if (box_2.rejected[i]) { EXPECT_EQ(param->value(i), param->oldValue(i)); }
		}

		box_2.clear();
	}
}

TEST_F(TParameterTest, update_sum_one_pairwise) {
	coretools::instances::parameters().add("numThreads", 2);
	auto param = std::make_shared<TParameter<TypeParam1DSumOne, DB_1>>("param", boxOnParam_1D.get(), def);
	param->initStorage(&box_1, {5}, {dimNames});

	std::vector<double> initVals = {0.0666666666666667, 0.133333333333333, 0.2, 0.266666666666667, 0.333333333333333};
	for (size_t i = 0; i < 5; ++i) { param->set(i, initVals[i]); }

	// update
	size_t numRep = 1000;
	for (size_t r = 0; r < numRep; ++r) {
		param->update(r);

		EXPECT_FLOAT_EQ(param->sum(param->getFull()), 1.0);
		EXPECT_EQ(coretools::containerSum(box_1.updateCounts), 4); // 4 out of 5 elements can be updated with pairs
		EXPECT_EQ(coretools::containerSum(box_1.updateTmpValsCounts), 4);

		for (size_t i = 0; i < box_1.accepted.size(); ++i) {
			if (box_1.accepted[i]) { EXPECT_NE(param->value(i), param->oldValue(i)); }
		}
		for (size_t i = 0; i < box_1.rejected.size(); ++i) {
			if (box_1.rejected[i]) { EXPECT_EQ(param->value(i), param->oldValue(i)); }
		}

		box_1.clear();
	}
}

TEST_F(TParameterTest, update_2D_sum_one_pairwise) {
	coretools::instances::parameters().add("numThreads", 2);
	auto param = std::make_shared<TParameter<TypeParam2DSumOne, DB_2>>("param", boxOnParam_2D.get(), def);
	param->initStorage(&box_2, {2, 5}, {dimNames, dimNames});

	std::vector<double> initVals = {0.0666666666666667, 0.133333333333333,  0.2,
									0.266666666666667,  0.333333333333333,  0.317790752687308,
									0.340641991709671,  0.0213855838659709, 0.3141758858745,
									0.00600578586254965};
	for (size_t i = 0; i < initVals.size(); ++i) { param->set(i, initVals[i]); }

	// update
	size_t numRep = 1000;
	for (size_t r = 0; r < numRep; ++r) {
		param->update(r);

		EXPECT_FLOAT_EQ(param->sum(param->get1DSlice(1, {0, 0})), 1.0);
		EXPECT_FLOAT_EQ(param->sum(param->get1DSlice(1, {1, 0})), 1.0);
		// 4 out of 5 elements can be updated with pairs (per row)
		EXPECT_EQ(coretools::containerSum(box_2.updateCounts), 8);
		EXPECT_EQ(coretools::containerSum(box_2.updateTmpValsCounts), 8);

		for (size_t i = 0; i < box_2.accepted.size(); ++i) {
			if (box_2.accepted[i]) { EXPECT_NE(param->value(i), param->oldValue(i)); }
		}
		for (size_t i = 0; i < box_2.rejected.size(); ++i) {
			if (box_2.rejected[i]) { EXPECT_EQ(param->value(i), param->oldValue(i)); }
		}

		box_2.clear();
	}
}

TEST_F(TParameterTest, update_2D_sum_one_joint) {
	coretools::instances::parameters().add("numThreads", 2);
	using T    = ParamSpec<Type, Hash<0>, BoxOnParam_2D, NumDim<2>, SumOne<1, UpdateTypes::joint>>;
	auto param = std::make_shared<TParameter<T, DB_2>>("param", boxOnParam_2D.get(), def);
	param->initStorage(&box_2, {2, 5}, {dimNames, dimNames});

	std::vector<double> initVals = {0.0666666666666667, 0.133333333333333,  0.2,
									0.266666666666667,  0.333333333333333,  0.317790752687308,
									0.340641991709671,  0.0213855838659709, 0.3141758858745,
									0.00600578586254965};
	for (size_t i = 0; i < initVals.size(); ++i) { param->set(i, initVals[i]); }

	// update
	size_t numRep = 1000;
	for (size_t r = 0; r < numRep; ++r) {
		param->update(r);

		EXPECT_FLOAT_EQ(param->sum(param->get1DSlice(1, {0, 0})), 1.0);
		EXPECT_FLOAT_EQ(param->sum(param->get1DSlice(1, {1, 0})), 1.0);
		for (size_t i = 0; i < initVals.size(); ++i) {
			EXPECT_EQ(box_2.updateCounts[i], 1);
			EXPECT_EQ(box_2.updateTmpValsCounts[i], 1);
		}

		for (size_t i = 0; i < box_2.accepted.size(); ++i) {
			if (box_2.accepted[i]) { EXPECT_NE(param->value(i), param->oldValue(i)); }
		}
		for (size_t i = 0; i < box_2.rejected.size(); ++i) {
			if (box_2.rejected[i]) { EXPECT_EQ(param->value(i), param->oldValue(i)); }
		}

		box_2.clear();
	}
}

TEST_F(TParameterTest, updateEveryNthIteration) {
	def.setUpdateEveryNthIter(3);
	auto param = std::make_shared<TParameter<TypeParam1D, DB_1>>("param", boxOnParam_1D.get(), def);
	param->initStorage(&box_1, {5}, {dimNames});
	for (size_t i = 0; i < param->size(); ++i) { param->set(i, 0.0); }

	// update
	size_t numRep = 1000;
	for (size_t r = 0; r < numRep; ++r) { param->update(r); }

	for (size_t i = 0; i < 5; ++i) { EXPECT_EQ(param->numUpdates(i), 1000 / 3 + 1); }
}

TEST_F(TParameterTest, set_SingleElement) {
	def.setInitVal("20.");
	auto param = std::make_shared<TParameter<TypeParam1D, DB_1>>("param", boxOnParam_1D.get(), def);
	param->initStorage(&box_1);

	param->set(10.);
	EXPECT_EQ(param->oldValue(), 20.);
	EXPECT_FLOAT_EQ(param->value(), 10.);
}

TEST_F(TParameterTest, set_Array) {
	def.setInitVal("0,1,2,3,4");
	auto param = std::make_shared<TParameter<TypeParam1D, DB_1>>("param", boxOnParam_1D.get(), def);
	param->initStorage(&box_1, {5}, {dimNames});

	for (size_t i = 0; i < 5; i++) param->set(i, i + 1);

	for (size_t i = 0; i < 5; i++) {
		EXPECT_EQ(param->oldValue(i), i);
		EXPECT_EQ(param->value(i), i + 1.);
	}
#ifdef _DEBUG
	EXPECT_DEATH(param->set(5, 0.), "");
#endif
}

TEST_F(TParameterTest, reset_SingleElement) {
	def.setInitVal("20.");
	auto param = std::make_shared<TParameter<TypeParam1D, DB_1>>("param", boxOnParam_1D.get(), def);
	param->initStorage(&box_1);

	param->set(10.);
	EXPECT_EQ(param->oldValue(), 20.);
	EXPECT_FLOAT_EQ(param->value(), 10.);
	param->reset();
	EXPECT_EQ(param->oldValue(), 20.);
	EXPECT_FLOAT_EQ(param->value(), 20.);
}

TEST_F(TParameterTest, reset_Array) {
	def.setInitVal("0,1,2,3,4");
	auto param = std::make_shared<TParameter<TypeParam1D, DB_1>>("param", boxOnParam_1D.get(), def);
	param->initStorage(&box_1, {5}, {dimNames});

	// set
	for (size_t i = 0; i < 5; i++) param->set(i, i + 1.);

	for (size_t i = 0; i < 5; i++) {
		EXPECT_EQ(param->oldValue(i), i);
		EXPECT_EQ(param->value(i), i + 1.);
	}

	// reset
	for (size_t i = 0; i < 5; i++) param->reset(i);

	for (size_t i = 0; i < 5; i++) {
		EXPECT_EQ(param->oldValue(i), i);
		EXPECT_EQ(param->value(i), i);
	}
#ifdef _DEBUG
	EXPECT_DEATH(param->reset(5), "");
#endif
}

TEST_F(TParameterTest, addToMeanVar) {
	auto param = std::make_shared<TParameter<TypeParam1D, DB_1>>("param", boxOnParam_1D.get(), def);
	param->initStorage(&box_1);

	param->set(2.);
	param->addToMeanVar(0);
	param->set(3.);
	param->addToMeanVar(0);
	param->set(4.);
	param->addToMeanVar(0);
	param->set(5.);
	param->addToMeanVar(0);
	param->set(6.);
	param->addToMeanVar(0);

	EXPECT_FLOAT_EQ(param->mean(0), 4.);
	EXPECT_FLOAT_EQ(param->var(0), 2.);
	EXPECT_FLOAT_EQ(param->sd(0), 1.414214);

	param->clearMeanVar();
	EXPECT_FLOAT_EQ(param->mean(0), 6.);
	EXPECT_FLOAT_EQ(param->var(0), 0.);
	EXPECT_FLOAT_EQ(param->sd(0), 0.);
}

TEST_F(TParameterTest, addToCounts) {
	TParameterDefinition defBool;
	defBool.setDefaultFiles("test");
	auto param = std::make_shared<TParameter<TypeParamBool, DB_bool>>("param", boxOnBool.get(), defBool);
	param->initStorage(&box_bool);

	param->set(false);
	param->addToMeanVar(0);
	param->set(true);
	param->addToMeanVar(0);
	param->set(true);
	param->addToMeanVar(0);
	param->set(false);
	param->addToMeanVar(0);
	param->set(true);
	param->addToMeanVar(0);

	EXPECT_FLOAT_EQ(param->fracCounts(0, 0), 2. / 5.);
	EXPECT_FLOAT_EQ(param->fracCounts(0, 1), 3. / 5.);

	param->clearMeanVar();
	EXPECT_FLOAT_EQ(param->fracCounts(0, 0), 0.);
	EXPECT_FLOAT_EQ(param->fracCounts(0, 1), 1.);
}

TEST_F(TParameterTest, sampleBinary) {
	TParameterDefinition defBool;
	defBool.setDefaultFiles("test");
	auto param = std::make_shared<TParameter<TypeParamBool, DB_bool>>("param", boxOnBool.get(), defBool);
	param->initStorage(&box_bool);

	std::array<double, 2> means = {-1.0, 1.0};
	coretools::probdist::TNormalDistr normal_0(means[0], 0.1);
	coretools::probdist::TNormalDistr normal_1(means[1], 0.1);

	coretools::Probability prob_0{0.1};
	size_t numRep = 10000;
	size_t sum    = 0;
	for (size_t i = 0; i < numRep; ++i) {
		size_t which = coretools::instances::randomGenerator().pickOneOfTwo(prob_0);
		sum += which;
		double mean               = means[which];
		std::array<double, 2> arr = {normal_0.logDensity(mean), normal_1.logDensity(mean)};
		param->sampleDiscrete<true, true>(0, arr);
	}
	EXPECT_FLOAT_EQ(param->fracCounts(0, 1), (double)sum / (double)numRep);
}
