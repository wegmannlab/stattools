//
// Created by madleina on 04.02.21.
//

#include <cmath>
#include <stdint.h>

#include "gtest/gtest.h"

#include "coretools/Types/old/weakTypesWithLogExp.h"
#include "stattools/ParametersObservations/TValue.h"
#include "coretools/Types/commonWeakTypes.h"

using namespace testing;
using namespace stattools;
using namespace coretools;

//-------------------------------------------
// TValueUpdated
//-------------------------------------------
TEST(TValueUpdatedTest, init) {
	TValueUpdated<coretools::Unbounded> val;
	EXPECT_EQ((coretools::Unbounded)val, std::numeric_limits<double>::lowest());
	EXPECT_EQ(val.oldValue(), std::numeric_limits<double>::lowest());
}

TEST(TValueUpdatedTest, initBoth) {
	TValueUpdated<coretools::Unbounded> val;
	// positive
	val.initBoth(0.837);
	EXPECT_EQ((coretools::Unbounded)val, 0.837);
	EXPECT_EQ(val.oldValue(), 0.837);

	// negative
	val.initBoth(-2.74);
	EXPECT_EQ((coretools::Unbounded)val, -2.74);
	EXPECT_EQ(val.oldValue(), -2.74);

	// zero
	val.initBoth(0.);
	EXPECT_EQ((coretools::Unbounded)val, 0.);
	EXPECT_EQ(val.oldValue(), 0.);
}

TEST(TValueUpdatedTest, setVal) {
	TValueUpdated<coretools::Unbounded> val;
	// positive
	val.setVal(0.837);
	EXPECT_EQ((coretools::Unbounded)val, 0.837);
	EXPECT_EQ(val.oldValue(), std::numeric_limits<double>::lowest());

	// negative
	val.setVal(-2.74);
	EXPECT_EQ((coretools::Unbounded)val, -2.74);
	EXPECT_EQ(val.oldValue(), std::numeric_limits<double>::lowest());

	// zero
	val.setVal(0.);
	EXPECT_EQ((coretools::Unbounded)val, 0.);
	EXPECT_EQ(val.oldValue(), std::numeric_limits<double>::lowest());
}

TEST(TValueUpdatedTest, set) {
	TValueUpdated<coretools::Unbounded> val;
	// positive
	val = 0.837;
	EXPECT_EQ((coretools::Unbounded)val, 0.837);
	EXPECT_EQ(val.oldValue(), std::numeric_limits<double>::lowest());

	// negative
	val = -2.74;
	EXPECT_EQ((coretools::Unbounded)val, -2.74);
	EXPECT_EQ(val.oldValue(), 0.837);

	// zero
	val = 0.;
	EXPECT_EQ((coretools::Unbounded)val, 0.);
	EXPECT_EQ(val.oldValue(), -2.74);
}

TEST(TValueUpdatedTest, reset) {
	TValueUpdated<coretools::Unbounded> val;
	// first set to something which is not 0
	val = 10.3;

	// reset
	val.reset();
	EXPECT_EQ((coretools::Unbounded)val, std::numeric_limits<double>::lowest());
	EXPECT_EQ(val.oldValue(), std::numeric_limits<double>::lowest());
}

TEST(TValueUpdatedTest, expValue) {
	TValueUpdated<coretools::Unbounded> val;
	// first set to something which is not 0
	val = 10.3;

	// expValue

	EXPECT_EQ(exp((coretools::Unbounded)val), exp(10.3));
	EXPECT_EQ(exp(val.oldValue()), exp(std::numeric_limits<double>::lowest()));
}

TEST(TValueUpdatedTest, operatorEqual) {
	// same
	TValueUpdated<coretools::Unbounded> val1;
	val1 = 10.3;
	val1 = 0.376;

	TValueUpdated<coretools::Unbounded> val2;
	val2 = 10.3;
	val2 = 0.376;

	EXPECT_TRUE(val1 == val2);

	// different
	val2.setVal(0.4271);
	EXPECT_FALSE(val1 == val2);
}

// int
TEST(TValueUpdatedTest, integer) {
	TValueUpdated<coretools::Integer> val;

	// init
	EXPECT_EQ((coretools::Integer)val, std::numeric_limits<int>::lowest());
	EXPECT_EQ(val.oldValue(), std::numeric_limits<int>::lowest());

	// initBoth
	val.initBoth(1);
	EXPECT_EQ((coretools::Integer)val, 1);
	EXPECT_EQ(val.oldValue(), 1);

	// set
	val = 3;
	EXPECT_EQ((coretools::Integer)val, 3);
	EXPECT_EQ(val.oldValue(), 1);

	// setVal
	val.setVal(4);
	EXPECT_EQ((coretools::Integer)val, 4);
	EXPECT_EQ(val.oldValue(), 1);

	// set
	val = -4;
	EXPECT_EQ((coretools::Integer)val, -4);
	EXPECT_EQ(val.oldValue(), 4);

	// reset
	val.reset();
	EXPECT_EQ((coretools::Integer)val, 4);
	EXPECT_EQ(val.oldValue(), 4);

	// expValue
	EXPECT_EQ(std::exp((double)(coretools::Integer)val), exp(4));
	EXPECT_EQ(std::exp((double)val.oldValue()), exp(4));
}

// int
TEST(TValueUpdatedTest, boolean) {
	TValueUpdated<coretools::Boolean> val;

	// init
	EXPECT_EQ((coretools::Boolean)val, false);
	EXPECT_EQ(val.oldValue(), false);

	// initBoth
	val.initBoth(true);
	EXPECT_EQ((coretools::Boolean)val, true);
	EXPECT_EQ(val.oldValue(), true);

	// set
	val = false;
	EXPECT_EQ((coretools::Boolean)val, false);
	EXPECT_EQ(val.oldValue(), true);

	// reset
	val.reset();
	EXPECT_EQ((coretools::Boolean)val, true);
	EXPECT_EQ(val.oldValue(), true);

	// setVal
	val.setVal(false);
	EXPECT_EQ((coretools::Boolean)val, false);
	EXPECT_EQ(val.oldValue(), true);

	// expValue
	EXPECT_EQ(exp((coretools::Boolean)val), exp(0));
	EXPECT_EQ(exp(val.oldValue()), exp(1));
}

//-------------------------------------------

TEST(TValueUpdatedWithExponentialTest, init) {
	TValueUpdated<WeakTypeWithExp<double>> val;
	EXPECT_EQ((WeakTypeWithExp<double>)val, 0.0);
	EXPECT_EQ(val.oldValue(), 0.0);
	EXPECT_EQ(exp((WeakTypeWithExp<double>)val), exp(0.0));
	EXPECT_EQ(exp(val.oldValue()), exp(0.0));
}

TEST(TValueUpdatedWithExponentialTest, initBoth) {
	TValueUpdated<WeakTypeWithExp<double>> val;
	// positive
	val.initBoth(0.837);
	EXPECT_EQ((WeakTypeWithExp<double>)val, 0.837);
	EXPECT_EQ(val.oldValue(), 0.837);
	EXPECT_EQ(exp((WeakTypeWithExp<double>)val), exp(0.837));
	EXPECT_EQ(exp(val.oldValue()), exp(0.837));

	// negative
	val.initBoth(-2.74);
	EXPECT_EQ((WeakTypeWithExp<double>)val, -2.74);
	EXPECT_EQ(val.oldValue(), -2.74);
	EXPECT_EQ(exp((WeakTypeWithExp<double>)val), exp(-2.74));
	EXPECT_EQ(exp(val.oldValue()), exp(-2.74));

	// zero
	val.initBoth(0.);
	EXPECT_EQ((WeakTypeWithExp<double>)val, 0.);
	EXPECT_EQ(val.oldValue(), 0.);
	EXPECT_EQ(exp((WeakTypeWithExp<double>)val), exp(0.));
	EXPECT_EQ(exp(val.oldValue()), exp(0.));
}

TEST(TValueUpdatedWithExponentialTest, setVal) {
	TValueUpdated<WeakTypeWithExp<double>> val;
	// positive
	val.setVal(0.837);
	EXPECT_EQ((WeakTypeWithExp<double>)val, 0.837);
	EXPECT_EQ(val.oldValue(), 0.0);
	EXPECT_EQ(exp((WeakTypeWithExp<double>)val), exp(0.837));
	EXPECT_EQ(exp(val.oldValue()), exp(0.0));

	// negative
	val.setVal(-2.74);
	EXPECT_EQ((WeakTypeWithExp<double>)val, -2.74);
	EXPECT_EQ(val.oldValue(), 0.0);
	EXPECT_EQ(exp((WeakTypeWithExp<double>)val), exp(-2.74));
	EXPECT_EQ(exp(val.oldValue()), exp(0.0));

	// zero
	val.setVal(0.);
	EXPECT_EQ((WeakTypeWithExp<double>)val, 0.);
	EXPECT_EQ(val.oldValue(), 0.0);
	EXPECT_EQ(exp((WeakTypeWithExp<double>)val), exp(0.));
	EXPECT_EQ(exp(val.oldValue()), exp(0.0));
}

TEST(TValueUpdatedWithExponentialTest, set) {
	TValueUpdated<WeakTypeWithExp<double>> val;
	// positive
	val = 0.837;
	EXPECT_EQ((WeakTypeWithExp<double>)val, 0.837);
	EXPECT_EQ(val.oldValue(), 0.0);
	EXPECT_EQ(exp((WeakTypeWithExp<double>)val), exp(0.837));
	EXPECT_EQ(exp(val.oldValue()), exp(0.0));

	// negative
	val = -2.74;
	EXPECT_EQ((WeakTypeWithExp<double>)val, -2.74);
	EXPECT_EQ(val.oldValue(), 0.837);
	EXPECT_EQ(exp((WeakTypeWithExp<double>)val), exp(-2.74));
	EXPECT_EQ(exp(val.oldValue()), exp(0.837));

	// zero
	val = 0.;
	EXPECT_EQ((WeakTypeWithExp<double>)val, 0.);
	EXPECT_EQ(val.oldValue(), -2.74);
	EXPECT_EQ(exp((WeakTypeWithExp<double>)val), exp(0.));
	EXPECT_EQ(exp(val.oldValue()), exp(-2.74));
}

TEST(TValueUpdatedWithExponentialTest, reset) {
	TValueUpdated<WeakTypeWithExp<double>> val;
	// first set to something which is not 0
	val = 10.3;

	// reset
	val.reset();
	EXPECT_EQ((WeakTypeWithExp<double>)val, 0.0);
	EXPECT_EQ(val.oldValue(), 0.0);
	EXPECT_EQ(exp((WeakTypeWithExp<double>)val), exp(0.0));
	EXPECT_EQ(exp(val.oldValue()), exp(0.0));
}

// int
TEST(TValueUpdatedWithExponentialTest, integer) {
	TValueUpdated<WeakTypeWithExp<int>> val;

	// init
	EXPECT_EQ((WeakTypeWithExp<int>)val, 0);
	EXPECT_EQ(val.oldValue(), 0);
	EXPECT_EQ(exp((WeakTypeWithExp<int>)val), exp(0));
	EXPECT_EQ(exp(val.oldValue()), exp(0));

	// initBoth
	val.initBoth(1);
	EXPECT_EQ((WeakTypeWithExp<int>)val, 1);
	EXPECT_EQ(val.oldValue(), 1);
	EXPECT_EQ(exp((WeakTypeWithExp<int>)val), exp(1));
	EXPECT_EQ(exp(val.oldValue()), exp(1));

	// set
	val = 3;
	EXPECT_EQ((WeakTypeWithExp<int>)val, 3);
	EXPECT_EQ(val.oldValue(), 1);
	EXPECT_EQ(exp((WeakTypeWithExp<int>)val), exp(3));
	EXPECT_EQ(exp(val.oldValue()), exp(1));

	// setVal
	val.setVal(4);
	EXPECT_EQ((WeakTypeWithExp<int>)val, 4);
	EXPECT_EQ(val.oldValue(), 1);
	EXPECT_EQ(exp((WeakTypeWithExp<int>)val), exp(4));
	EXPECT_EQ(exp(val.oldValue()), exp(1));

	// set
	val = -4;
	EXPECT_EQ((WeakTypeWithExp<int>)val, -4);
	EXPECT_EQ(val.oldValue(), 4);
	EXPECT_EQ(exp((WeakTypeWithExp<int>)val), exp(-4));
	EXPECT_EQ(exp(val.oldValue()), exp(4));

	// reset
	val.reset();
	EXPECT_EQ((WeakTypeWithExp<int>)val, 4);
	EXPECT_EQ(val.oldValue(), 4);
	EXPECT_EQ(exp((WeakTypeWithExp<int>)val), exp(4));
	EXPECT_EQ(exp(val.oldValue()), exp(4));
}

// bool
TEST(TValueUpdatedWithExponentialTest, boolean) {
	TValueUpdated<WeakTypeWithExp<bool>> val;
	EXPECT_EQ((WeakTypeWithExp<bool>)val, false);
	EXPECT_EQ(val.oldValue(), false);
	EXPECT_EQ(exp((WeakTypeWithExp<bool>)val), exp(0));
	EXPECT_EQ(exp(val.oldValue()), exp(0));

	// set
	val = true;
	EXPECT_EQ((WeakTypeWithExp<bool>)val, true);
	EXPECT_EQ(val.oldValue(), false);
	EXPECT_EQ(exp((WeakTypeWithExp<bool>)val), exp(1));
	EXPECT_EQ(exp(val.oldValue()), exp(0));
}

//-------------------------------------------

TEST(TValueUpdatedWithLogTest, init) {
	TValueUpdated<WeakTypeWithLog<double>> val;
	EXPECT_EQ((WeakTypeWithLog<double>)val, 0.);
	EXPECT_EQ(val.oldValue(), 0.);
	EXPECT_EQ(log((WeakTypeWithLog<double>)val), log(0.));
	EXPECT_EQ(log(val.oldValue()), log(0.));
}

TEST(TValueUpdatedWithLogTest, initBoth) {
	TValueUpdated<WeakTypeWithLog<double>> val;
	// positive
	val.initBoth(0.837);
	EXPECT_EQ((WeakTypeWithLog<double>)val, 0.837);
	EXPECT_EQ(val.oldValue(), 0.837);
	EXPECT_EQ(log((WeakTypeWithLog<double>)val), log(0.837));
	EXPECT_EQ(log(val.oldValue()), log(0.837));

	// negative
	EXPECT_ANY_THROW(val.initBoth(-2.74)); // negative is invalid for log
}

TEST(TValueUpdatedWithLogTest, setVal) {
	TValueUpdated<WeakTypeWithLog<double>> val;
	// positive
	val.setVal(0.837);
	EXPECT_EQ((WeakTypeWithLog<double>)val, 0.837);
	EXPECT_EQ(val.oldValue(), 0.);
	EXPECT_EQ(log((WeakTypeWithLog<double>)val), log(0.837));
	EXPECT_EQ(log(val.oldValue()), log(0.));

	// negative
	EXPECT_ANY_THROW(val.setVal(-2.74)); // negative is invalid for log
}

TEST(TValueUpdatedWithLogTest, set) {
	TValueUpdated<WeakTypeWithLog<double>> val;
	// positive
	val = 0.837;
	EXPECT_EQ((WeakTypeWithLog<double>)val, 0.837);
	EXPECT_EQ(val.oldValue(), 0.);
	EXPECT_EQ(log((WeakTypeWithLog<double>)val), log(0.837));
	EXPECT_EQ(log(val.oldValue()), log(0.));

	// negative
	EXPECT_ANY_THROW(val = -2.74); // negative is invalid for log
}

TEST(TValueUpdatedWithLogTest, reset) {
	TValueUpdated<WeakTypeWithLog<double>> val;
	// first set to something which is not 0
	val = 10.3;

	// reset
	val.reset();
	EXPECT_EQ((WeakTypeWithLog<double>)val, 0.);
	EXPECT_EQ(val.oldValue(), 0.);
	EXPECT_EQ(log((WeakTypeWithLog<double>)val), log(0.));
	EXPECT_EQ(log(val.oldValue()), log(0.));
}

// int
TEST(TValueUpdatedWithLogTest, integer) {
	TValueUpdated<WeakTypeWithLog<uint8_t>> val;

	// init
	EXPECT_EQ((WeakTypeWithLog<uint8_t>)val, 0);
	EXPECT_EQ(val.oldValue(), 0);
	EXPECT_EQ(log((WeakTypeWithLog<uint8_t>)val), log(0));
	EXPECT_EQ(log(val.oldValue()), log(0));

	// initBoth
	val.initBoth(1);
	EXPECT_EQ((WeakTypeWithLog<uint8_t>)val, 1);
	EXPECT_EQ(val.oldValue(), 1);
	EXPECT_EQ(log((WeakTypeWithLog<uint8_t>)val), log(1));
	EXPECT_EQ(log(val.oldValue()), log(1));

	// set
	val = 3;
	EXPECT_EQ((WeakTypeWithLog<uint8_t>)val, 3);
	EXPECT_EQ(val.oldValue(), 1);
	EXPECT_EQ(log((WeakTypeWithLog<uint8_t>)val), log(3));
	EXPECT_EQ(log(val.oldValue()), log(1));

	// setVal
	val.setVal(4);
	EXPECT_EQ((WeakTypeWithLog<uint8_t>)val, 4);
	EXPECT_EQ(val.oldValue(), 1);
	EXPECT_EQ(log((WeakTypeWithLog<uint8_t>)val), log(4));
	EXPECT_EQ(log(val.oldValue()), log(1));

	// set
	val = 8;
	EXPECT_EQ((WeakTypeWithLog<uint8_t>)val, 8);
	EXPECT_EQ(val.oldValue(), 4);
	EXPECT_EQ(log((WeakTypeWithLog<uint8_t>)val), log(8));
	EXPECT_EQ(log(val.oldValue()), log(4));

	// reset
	val.reset();
	EXPECT_EQ((WeakTypeWithLog<uint8_t>)val, 4);
	EXPECT_EQ(val.oldValue(), 4);
	EXPECT_EQ(log((WeakTypeWithLog<uint8_t>)val), log(4));
	EXPECT_EQ(log(val.oldValue()), log(4));
}

// bool
TEST(TValueUpdatedWithLogTest, boolean) {
	TValueUpdated<WeakTypeWithLog<bool>> val;
	EXPECT_EQ((WeakTypeWithLog<bool>)val, false);
	EXPECT_EQ(val.oldValue(), false);
	EXPECT_EQ(log((WeakTypeWithLog<bool>)val), log(0));
	EXPECT_EQ(log(val.oldValue()), log(0));
}

//-------------------------------------------

TEST(TValueUpdatedWithLogAndExponentialTest, init) {
	TValueUpdated<WeakTypeWithLogAndExp<double>> val;
	EXPECT_EQ((WeakTypeWithLogAndExp<double>)val, 0.);
	EXPECT_EQ(val.oldValue(), 0.);
	EXPECT_EQ(log((WeakTypeWithLogAndExp<double>)val), log(0.));
	EXPECT_EQ(log(val.oldValue()), log(0.));
	EXPECT_EQ(exp((WeakTypeWithLogAndExp<double>)val), exp(0.));
	EXPECT_EQ(exp(val.oldValue()), exp(0.));
}

TEST(TValueUpdatedWithLogAndExponentialTest, initBoth) {
	TValueUpdated<WeakTypeWithLogAndExp<double>> val;
	// positive
	val.initBoth(0.837);
	EXPECT_EQ((WeakTypeWithLogAndExp<double>)val, 0.837);
	EXPECT_EQ(val.oldValue(), 0.837);
	EXPECT_EQ(log((WeakTypeWithLogAndExp<double>)val), log(0.837));
	EXPECT_EQ(log(val.oldValue()), log(0.837));
	EXPECT_EQ(exp((WeakTypeWithLogAndExp<double>)val), exp(0.837));
	EXPECT_EQ(exp(val.oldValue()), exp(0.837));
	// negative
	EXPECT_ANY_THROW(val.initBoth(-2.74)); // negative is invalid for log
}

TEST(TValueUpdatedWithLogAndExponentialTest, setVal) {
	TValueUpdated<WeakTypeWithLogAndExp<double>> val;
	// positive
	val.setVal(0.837);
	EXPECT_EQ((WeakTypeWithLogAndExp<double>)val, 0.837);
	EXPECT_EQ(val.oldValue(), 0.);
	EXPECT_EQ(log((WeakTypeWithLogAndExp<double>)val), log(0.837));
	EXPECT_EQ(log(val.oldValue()), log(0.));
	EXPECT_EQ(exp((WeakTypeWithLogAndExp<double>)val), exp(0.837));
	EXPECT_EQ(exp(val.oldValue()), exp(0.));
	// negative
	EXPECT_ANY_THROW(val.setVal(-2.74)); // negative is invalid for log
}

TEST(TValueUpdatedWithLogAndExponentialTest, set) {
	TValueUpdated<WeakTypeWithLogAndExp<double>> val;
	// positive
	val = 0.837;
	EXPECT_EQ((WeakTypeWithLogAndExp<double>)val, 0.837);
	EXPECT_EQ(val.oldValue(), 0.);
	EXPECT_EQ(log((WeakTypeWithLogAndExp<double>)val), log(0.837));
	EXPECT_EQ(log(val.oldValue()), log(0.));
	EXPECT_EQ(exp((WeakTypeWithLogAndExp<double>)val), exp(0.837));
	EXPECT_EQ(exp(val.oldValue()), exp(0.));

	// negative
	EXPECT_ANY_THROW(val = -2.74); // negative is invalid for log
}

TEST(TValueUpdatedWithLogAndExponentialTest, reset) {
	TValueUpdated<WeakTypeWithLogAndExp<double>> val;
	// first set to something which is not 0
	val = 10.3;

	// reset
	val.reset();
	EXPECT_EQ((WeakTypeWithLogAndExp<double>)val, 0.);
	EXPECT_EQ(val.oldValue(), 0.);
	EXPECT_EQ(log((WeakTypeWithLogAndExp<double>)val), log(0.));
	EXPECT_EQ(log(val.oldValue()), log(0.));
	EXPECT_EQ(exp((WeakTypeWithLogAndExp<double>)val), exp(0.));
	EXPECT_EQ(exp(val.oldValue()), exp(0.));
}

// int
TEST(TValueUpdatedWithLogAndExponentialTest, integer) {
	TValueUpdated<WeakTypeWithLogAndExp<uint8_t>> val;

	// init
	EXPECT_EQ((WeakTypeWithLogAndExp<uint8_t>)val, 0);
	EXPECT_EQ(val.oldValue(), 0);
	EXPECT_EQ(log((WeakTypeWithLogAndExp<uint8_t>)val), log(0));
	EXPECT_EQ(log(val.oldValue()), log(0));
	EXPECT_EQ(exp((WeakTypeWithLogAndExp<uint8_t>)val), exp(0));
	EXPECT_EQ(exp(val.oldValue()), exp(0));

	// initBoth
	val.initBoth(1);
	EXPECT_EQ((WeakTypeWithLogAndExp<uint8_t>)val, 1);
	EXPECT_EQ(val.oldValue(), 1);
	EXPECT_EQ(log((WeakTypeWithLogAndExp<uint8_t>)val), log(1));
	EXPECT_EQ(log(val.oldValue()), log(1));
	EXPECT_EQ(exp((WeakTypeWithLogAndExp<uint8_t>)val), exp(1));
	EXPECT_EQ(exp(val.oldValue()), exp(1));

	// set
	val = 3;
	EXPECT_EQ((WeakTypeWithLogAndExp<uint8_t>)val, 3);
	EXPECT_EQ(val.oldValue(), 1);
	EXPECT_EQ(log((WeakTypeWithLogAndExp<uint8_t>)val), log(3));
	EXPECT_EQ(log(val.oldValue()), log(1));
	EXPECT_EQ(exp((WeakTypeWithLogAndExp<uint8_t>)val), exp(3));
	EXPECT_EQ(exp(val.oldValue()), exp(1));

	// setVal
	val.setVal(4);
	EXPECT_EQ((WeakTypeWithLogAndExp<uint8_t>)val, 4);
	EXPECT_EQ(val.oldValue(), 1);
	EXPECT_EQ(log((WeakTypeWithLogAndExp<uint8_t>)val), log(4));
	EXPECT_EQ(log(val.oldValue()), log(1));
	EXPECT_EQ(exp((WeakTypeWithLogAndExp<uint8_t>)val), exp(4));
	EXPECT_EQ(exp(val.oldValue()), exp(1));

	// set
	val = 8;
	EXPECT_EQ((WeakTypeWithLogAndExp<uint8_t>)val, 8);
	EXPECT_EQ(val.oldValue(), 4);
	EXPECT_EQ(log((WeakTypeWithLogAndExp<uint8_t>)val), log(8));
	EXPECT_EQ(log(val.oldValue()), log(4));
	EXPECT_EQ(exp((WeakTypeWithLogAndExp<uint8_t>)val), exp(8));
	EXPECT_EQ(exp(val.oldValue()), exp(4));

	// reset
	val.reset();
	EXPECT_EQ((WeakTypeWithLogAndExp<uint8_t>)val, 4);
	EXPECT_EQ(val.oldValue(), 4);
	EXPECT_EQ(log((WeakTypeWithLogAndExp<uint8_t>)val), log(4));
	EXPECT_EQ(log(val.oldValue()), log(4));
	EXPECT_EQ(exp((WeakTypeWithLogAndExp<uint8_t>)val), exp(4));
	EXPECT_EQ(exp(val.oldValue()), exp(4));
}

// bool
TEST(TValueUpdatedWithLogAndExponentialTest, boolean) {
	TValueUpdated<WeakTypeWithLogAndExp<bool>> val;

	EXPECT_EQ((WeakTypeWithLogAndExp<bool>)val, false);
	EXPECT_EQ(val.oldValue(), false);
	EXPECT_EQ(log((WeakTypeWithLogAndExp<bool>)val), log(0));
	EXPECT_EQ(log(val.oldValue()), log(0));
	EXPECT_EQ(exp((WeakTypeWithLogAndExp<bool>)val), exp(0));
	EXPECT_EQ(exp(val.oldValue()), exp(0));
}
