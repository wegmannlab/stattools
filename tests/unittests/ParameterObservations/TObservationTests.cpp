//
// Created by madleina on 22.01.21.
//

#include <array>
#include <memory>
#include <stddef.h>

#include "gtest/gtest.h"

#include "coretools/Distributions/TNormalDistr.h"
#include "coretools/Storage/TNames.h"
#include "coretools/Types/commonWeakTypes.h"
#include "stattools/ParametersObservations/TDefinition.h"
#include "stattools/ParametersObservations/TObservation.h"
#include "stattools/Priors/TPriorNormal.h"

using namespace testing;
using namespace stattools;

class TObservationTest_1D : public Test {
protected:
	using Type                     = coretools::Unbounded;
	constexpr static size_t NumDim = 1;
	using BoxOnObs                 = prior::TNormalFixed<TObservationBase, Type, NumDim>;
	using SpecObs                  = TObservation<Type, NumDim, BoxOnObs>;

	TObservationDefinition def;
	std::shared_ptr<BoxOnObs> prior;
	std::shared_ptr<SpecObs> observation;

	std::shared_ptr<coretools::TNamesEmpty> dimNames;

	void SetUp() override {
		instances::dagBuilder().clear();

		def.setPriorParameters("0,1");
		prior = std::make_shared<BoxOnObs>();
		observation =
			std::make_shared<SpecObs>("obs", prior.get(), coretools::TMultiDimensionalStorage<Type, NumDim>({5}), def);
		dimNames = std::make_shared<coretools::TNamesIndices>();
	}
};

TEST_F(TObservationTest_1D, constructor) { EXPECT_EQ(observation->name(), "obs"); }

// fill 1D data

TEST_F(TObservationTest_1D, prepareFillData_1D) {
	// before filling data: dimensions should be as in definition (5)
	EXPECT_EQ(observation->dimensions()[0], 5);

	observation->storage().prepareFillData(100, {});

	// dimensions should now have changed to 100
	EXPECT_EQ(observation->dimensions()[0], 100);
}

TEST_F(TObservationTest_1D, fill_1D_dimensionsExactlySame) {
	observation->storage().prepareFillData(100, {});

	for (size_t i = 0; i < 100; i++) { observation->storage().emplace_back(i + 0.5); }
	observation->storage().finalizeFillData();

	// dimensions should be the same as we set initially (100)
	EXPECT_EQ(observation->dimensions()[0], 100);
	for (size_t i = 0; i < 100; i++) { EXPECT_EQ(observation->value(i), i + 0.5); }
}

TEST_F(TObservationTest_1D, fill_1D_dimensionsLessThanExpected) {
	observation->storage().prepareFillData(100, {});

	for (size_t i = 0; i < 18; i++) { // only fill 18 values
		observation->storage().emplace_back(i + 0.5);
	}
	observation->storage().finalizeFillData();

	// dimensions should be the smaller than we set initially

	EXPECT_EQ(observation->dimensions()[0], 18);
	for (size_t i = 0; i < 18; i++) { EXPECT_EQ(observation->value(i), i + 0.5); }
}

TEST_F(TObservationTest_1D, fill_1D_dimensionsMoreThanExpected) {
	observation->storage().prepareFillData(10, {});

	for (size_t i = 0; i < 100; i++) { // fill much more values
		observation->storage().emplace_back(i + 0.5);
	}
	observation->storage().finalizeFillData();

	// dimensions should be the higher than we set initially

	EXPECT_EQ(observation->dimensions()[0], 100);
	for (size_t i = 0; i < 100; i++) { EXPECT_EQ(observation->value(i), i + 0.5); }
}

// fill 2D data

class TObservationTest_2D : public Test {
protected:
	using Type                     = coretools::Unbounded;
	constexpr static size_t NumDim = 2;
	using BoxOnObs                 = prior::TNormalFixed<TObservationBase, Type, NumDim>;
	using SpecObs                  = TObservation<Type, NumDim, BoxOnObs>;

	TObservationDefinition def;
	std::shared_ptr<BoxOnObs> prior;
	std::shared_ptr<SpecObs> observation;

	std::shared_ptr<coretools::TNamesEmpty> dimNames;

	void SetUp() override {
		instances::dagBuilder().clear();

		def.setPriorParameters("0,1");
		prior       = std::make_shared<BoxOnObs>();
		observation = std::make_shared<SpecObs>("obs", prior.get(),
												coretools::TMultiDimensionalStorage<Type, NumDim>({5, 3}), def);
		dimNames    = std::make_shared<coretools::TNamesIndices>();
	}
};

TEST_F(TObservationTest_2D, prepareFillData_invalid) {
	// unknown dimension = 0
	EXPECT_ANY_THROW(observation->storage().prepareFillData(0, {100}));
	// total size = 0
	EXPECT_ANY_THROW(observation->storage().prepareFillData(100, {0}));
}

TEST_F(TObservationTest_2D, prepareFillData_2D) {
	// before filling data: dimensions should be as in definition (5, 3)
	EXPECT_EQ(observation->dimensions()[0], 5);
	EXPECT_EQ(observation->dimensions()[1], 3);

	observation->storage().prepareFillData(100, {50});

	// dimensions should now have changed to 100x50
	EXPECT_EQ(observation->dimensions()[0], 100);
	EXPECT_EQ(observation->dimensions()[1], 50);
}

TEST_F(TObservationTest_2D, fill_2D_dimensionsExactlySame) {
	observation->storage().prepareFillData(100, {50});

	for (size_t i = 0; i < 100 * 50; i++) { observation->storage().emplace_back(i + 0.5); }
	observation->storage().finalizeFillData();

	// dimensions should be the same as we set initially (100x50)
	EXPECT_EQ(observation->dimensions()[0], 100);
	EXPECT_EQ(observation->dimensions()[1], 50);
	for (size_t i = 0; i < 100 * 50; i++) { EXPECT_EQ(observation->value(i), i + 0.5); }
}

TEST_F(TObservationTest_2D, fill_2D_dimensionsLessThanExpected) {
	observation->storage().prepareFillData(100, {50});

	for (size_t i = 0; i < 18 * 50; i++) { // only fill 18*50 values
		observation->storage().emplace_back(i + 0.5);
	}
	observation->storage().finalizeFillData();

	// dimensions should be the smaller than we set initially
	EXPECT_EQ(observation->dimensions()[0], 18);
	EXPECT_EQ(observation->dimensions()[1], 50);
	for (size_t i = 0; i < 18 * 50; i++) { EXPECT_EQ(observation->value(i), i + 0.5); }
}

TEST_F(TObservationTest_2D, fill_2D_dimensionsMoreThanExpected) {
	observation->storage().prepareFillData(10, {50});

	for (size_t i = 0; i < 100 * 50; i++) { // fill much more values
		observation->storage().emplace_back(i + 0.5);
	}
	observation->storage().finalizeFillData();

	// dimensions should be the higher than we set initially
	EXPECT_EQ(observation->dimensions()[0], 100);
	EXPECT_EQ(observation->dimensions()[1], 50);
	for (size_t i = 0; i < 100 * 50; i++) { EXPECT_FLOAT_EQ(observation->value(i), i + 0.5); }
}

// fill 3D data

class TObservationTest_3D : public Test {
protected:
	using Type                     = coretools::Unbounded;
	constexpr static size_t NumDim = 3;
	using BoxOnObs                 = prior::TNormalFixed<TObservationBase, Type, NumDim>;
	using SpecObs                  = TObservation<Type, NumDim, BoxOnObs>;

	TObservationDefinition def;
	std::shared_ptr<BoxOnObs> prior;
	std::shared_ptr<SpecObs> observation;

	std::shared_ptr<coretools::TNamesEmpty> dimNames;

	void SetUp() override {
		instances::dagBuilder().clear();

		def.setPriorParameters("0,1");
		prior       = std::make_shared<BoxOnObs>();
		observation = std::make_shared<SpecObs>("obs", prior.get(),
												coretools::TMultiDimensionalStorage<Type, NumDim>({5, 7, 2}), def);
		dimNames    = std::make_shared<coretools::TNamesIndices>();
	}
};

TEST_F(TObservationTest_3D, prepareFillData_invalid) {
	EXPECT_ANY_THROW(observation->storage().prepareFillData(0, {10, 3}));
	EXPECT_ANY_THROW(observation->storage().prepareFillData(100, {10, 0}));
}

TEST_F(TObservationTest_3D, prepareFillData_3D) {
	// before filling data: dimensions should be as in definition (5, 7, 2)
	EXPECT_EQ(observation->dimensions()[0], 5);
	EXPECT_EQ(observation->dimensions()[1], 7);
	EXPECT_EQ(observation->dimensions()[2], 2);

	observation->storage().prepareFillData(100, {50, 15});

	// dimensions should now have changed to 100x50x15
	EXPECT_EQ(observation->dimensions()[0], 100);
	EXPECT_EQ(observation->dimensions()[1], 50);
	EXPECT_EQ(observation->dimensions()[2], 15);
}

TEST_F(TObservationTest_3D, fill_3D_dimensionsExactlySame) {
	observation->storage().prepareFillData(100, {50, 15});

	for (size_t i = 0; i < 100 * 50 * 15; i++) { observation->storage().emplace_back(i + 0.5); }
	observation->storage().finalizeFillData();

	// dimensions should be the same as we set initially (100x50x15)
	EXPECT_EQ(observation->dimensions()[0], 100);
	EXPECT_EQ(observation->dimensions()[1], 50);
	EXPECT_EQ(observation->dimensions()[2], 15);
	for (size_t i = 0; i < 100 * 50 * 15; i++) { EXPECT_EQ(observation->value(i), i + 0.5); }
}

TEST_F(TObservationTest_3D, fill_3D_dimensionsLessThanExpected) {
	observation->storage().prepareFillData(100, {50, 15});

	for (size_t i = 0; i < 18 * 50 * 15; i++) { // only fill 18*50*15 values
		observation->storage().emplace_back(i + 0.5);
	}
	observation->storage().finalizeFillData();

	// dimensions should be the smaller than we set initially
	EXPECT_EQ(observation->dimensions()[0], 18);
	EXPECT_EQ(observation->dimensions()[1], 50);
	EXPECT_EQ(observation->dimensions()[2], 15);
	for (size_t i = 0; i < 18 * 50 * 15; i++) { EXPECT_EQ(observation->value(i), i + 0.5); }
}

TEST_F(TObservationTest_3D, fill_3D_dimensionsMoreThanExpected) {
	observation->storage().prepareFillData(10, {50, 15});

	for (size_t i = 0; i < 100 * 50 * 15; i++) { // fill much more values
		observation->storage().emplace_back(i + 0.5);
	}
	observation->storage().finalizeFillData();

	// dimensions should be the higher than we set initially
	EXPECT_EQ(observation->dimensions()[0], 100);
	EXPECT_EQ(observation->dimensions()[1], 50);
	EXPECT_EQ(observation->dimensions()[2], 15);
	for (size_t i = 0; i < 100 * 50 * 15; i++) { EXPECT_EQ(observation->value(i), i + 0.5); }
}

TEST_F(TObservationTest_3D, getLogDensity) {
	// fill observation
	observation->storage().prepareFillData(10, {5, 3});
	for (size_t i = 0; i < 8 * 5 * 3; i++) { // fill less
		observation->storage().emplace_back(i + 0.5);
	}
	observation->storage().finalizeFillData();

	// get log prior density
	for (size_t i = 0; i < 8 * 5 * 3; i++) {
		EXPECT_EQ(observation->getLogDensity(i), coretools::probdist::TNormalDistr::logDensity(i + 0.5, 0, 1));
	}
}

TEST_F(TObservationTest_3D, set) {
	// fill observation
	observation->storage().prepareFillData(10, {5, 3});
	for (size_t i = 0; i < 8 * 5 * 3; i++) { // fill less
		observation->storage().emplace_back(i + 0.5);
	}
	observation->storage().finalizeFillData();

	// set
	for (size_t i = 0; i < 8 * 5 * 3; i++) { observation->set(i, i - 1); }

	// check if ok
	for (size_t i = 0; i < 8 * 5 * 3; i++) { EXPECT_EQ(observation->value(i), i - 1); }
}
