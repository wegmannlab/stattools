//
// Created by madleina on 11.12.20.
//

#include "gmock/gmock.h"
#include "gtest/gtest.h"

#include "stattools/ParametersObservations/TDefinition.h" // for TParameterDefinition, TObservati...

using namespace testing;
using namespace stattools;

//-------------------------------------------
// TParameterDefinition
//-------------------------------------------

// Test: isObserved

TEST(TDefTest, reSetObserved) {
	// not observed
	TParameterDefinition def;
	// ok: not observed -> not observed
	EXPECT_NO_THROW(def.reSetObserved("false"));
	EXPECT_FALSE(def.isObserved());
	// throw: not observed -> observed
	EXPECT_ANY_THROW(def.reSetObserved("true"));
	EXPECT_FALSE(def.isObserved());

	// observed
	TObservationDefinition obs;
	// ok: observed -> observed
	EXPECT_NO_THROW(obs.reSetObserved("true"));
	EXPECT_TRUE(obs.isObserved());
	// throw: observed -> not observed
	EXPECT_ANY_THROW(obs.reSetObserved("false"));
	EXPECT_TRUE(obs.isObserved());
}

//--------------------------------------------
// TMCMCParameterDef
//--------------------------------------------

TEST(TDefTest, setPropKernel_throw) {
	TParameterDefinition def;
	// don't give valid proposal kernel -> throw
	EXPECT_ANY_THROW(def.setPropKernel("whatever"));
	EXPECT_ANY_THROW(def.setPropKernel(""));
	EXPECT_ANY_THROW(def.setPropKernel("chisq"));
	EXPECT_ANY_THROW(def.setPropKernel("normal."));
	EXPECT_ANY_THROW(def.setPropKernel("normal,"));
}

TEST(TDefTest, setPropKernel) {
	TParameterDefinition def;
	EXPECT_NO_THROW(def.setPropKernel("normal"));
	EXPECT_NO_THROW(def.setPropKernel("           normal             "));
	EXPECT_NO_THROW(def.setPropKernel("normal"
									  ""));
	EXPECT_NO_THROW(def.setPropKernel("normal "));
	EXPECT_NO_THROW(def.setPropKernel("uniform"));
}

// Test: initVal()
TEST(TDefTest, testInitValDefault) {
	TParameterDefinition def;
	EXPECT_THAT(def.hasDefaultInitVal(), true);
}

TEST(TDefTest, testInitVal) {
	TParameterDefinition def;
	def.setInitVal("1.");
	EXPECT_THAT(def.hasDefaultInitVal(), false);
}

// Test: storeMeanVar()
TEST(TDefTest, testMeanVarDefault) {
	TParameterDefinition def;
	EXPECT_THAT(def.writesFile(stattools::MCMCFiles::meanVar), false);
}

TEST(TDefTest, testMeanVarWithFilename) {
	TParameterDefinition def;
	def.editFile(stattools::MCMCFiles::meanVar, "cheggie");
	EXPECT_THAT(def.writesFile(stattools::MCMCFiles::meanVar), true);
}
