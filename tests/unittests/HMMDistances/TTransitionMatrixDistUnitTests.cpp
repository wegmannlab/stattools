//
// Created by caduffm on 11/16/21.
//

#include <stddef.h>
#include <string>
#include <vector>

#include "gtest/gtest.h"

#include "stattools/HMMDistances/TGeneratingMatrix.h"
#include "stattools/HMMDistances/TTransitionMatrixDist.h"

using namespace testing;
using namespace stattools;

//-------------------------------------------
// TTransitionMatrixDistBase
//-------------------------------------------

class TGeneratingMatrixDummy : public TGeneratingMatrixBase<double, size_t> {
	// some other generating matrix, to specifically test the function _fillStationaryDistribution_SolveNormalEquations
	// (all other existing generating matrices do not use this function as they can calculate the stationary
	// distribution directly from the parameters of the generating matrix)
public:
	TGeneratingMatrixDummy() { resize(3); }

	void fillGeneratingMatrix(coretools::TConstView<double>) override {
		// fill generating matrix
		double lambda12 = 0.73;
		double lambda13 = 0.62;
		double lambda21 = 0.123;
		double lambda23 = 0.99;
		double lambda31 = 0.28;
		double lambda32 = 0.92;
		_generatingMatrix.zeros(3, 3);
		_generatingMatrix(0, 0) = -lambda12 - lambda13;
		_generatingMatrix(0, 1) = lambda12;
		_generatingMatrix(0, 2) = lambda13;
		_generatingMatrix(1, 0) = lambda21;
		_generatingMatrix(1, 1) = -lambda21 - lambda23;
		_generatingMatrix(1, 2) = lambda23;
		_generatingMatrix(2, 0) = lambda31;
		_generatingMatrix(2, 1) = lambda32;
		_generatingMatrix(2, 2) = -lambda31 - lambda32;
	}
	size_t numParameters() const override {
		return 0; // just something
	}
};

class TTransitionMatrixDummy : public TTransitionMatrixDistBase<double, size_t> {
	// some other transition matrix, to specifically test the function _fillStationaryDistribution_SolveNormalEquations
	// (all other existing generating matrices do not use this function as they can calculate the stationary
	// distribution directly from the parameters of the generating matrix)
protected:
	TGeneratingMatrixDummy _generatingMatrix;

public:
	TTransitionMatrixDummy(const size_t &NumStates) { resize(NumStates); };

	void fillTransitionMatrixFromStartValues() override {
		// first fill generating matrix
		_generatingMatrix.fillGeneratingMatrix({});
		// now take matrix exponential
		_transitionMatrix = arma::expmat(_generatingMatrix.getGeneratingMatrix());
	}

	bool fillTransitionMatrix(coretools::TConstView<double> Values) override {
		// first fill generating matrix
		_generatingMatrix.fillGeneratingMatrix(Values);
		// now take matrix exponential
		_transitionMatrix = arma::expmat(_generatingMatrix.getGeneratingMatrix());
		return true;
	}

	bool optimizeWithNelderMead() const override {
		return true; // just something
	}
	bool optimizeWithLineSearch() const override {
		return false; // just something
	}
	size_t numParameters() const override {
		return 0; // just something
	}
	std::vector<double> getStartValues() const override { return {}; }
	void fillHeaderEMReportFile(std::vector<std::string> &) const override {}
};

TEST(TTransitionMatrixDistBase, fillStationaryDistribution_SolveNormalEquations) {
	uint16_t numStates = 3;
	TTransitionMatrixDummy transitionMatrix(numStates);
	std::vector<double> values = {};
	transitionMatrix.fillTransitionMatrix(values);
	transitionMatrix.fillStationary(values);

	EXPECT_NEAR(transitionMatrix.stationary(0), 0.1293560, 0.001);
	EXPECT_NEAR(transitionMatrix.stationary(1), 0.4404439, 0.001);
	EXPECT_NEAR(transitionMatrix.stationary(2), 0.4302001, 0.001);
}

//-------------------------------------------
// TTransitionMatrixBool
//-------------------------------------------

TEST(TTransitionMatrixBoolTest, constructor) {
	TTransitionMatrixBool<double, size_t> mat(2);

	const auto &matrix = mat.getTransitionMatrix();
	EXPECT_EQ(matrix.n_rows, 2);
	EXPECT_EQ(mat.numStates(), 2);

	// invalid number of states
	EXPECT_ANY_THROW((TTransitionMatrixBool<double, size_t>(1)));
	EXPECT_ANY_THROW((TTransitionMatrixBool<double, size_t>(3)));
}

TEST(TTransitionMatrixBoolTest, resize) {
	TTransitionMatrixBool<double, size_t> mat;

	mat.resize(2);
	const auto &matrix = mat.getTransitionMatrix();
	EXPECT_EQ(matrix.n_rows, 2);
	EXPECT_EQ(mat.numStates(), 2);

	// invalid number of states
	EXPECT_ANY_THROW(mat.resize(1));
	EXPECT_ANY_THROW(mat.resize(3));
}

TEST(TTransitionMatrixBoolTest, fillTransitionMatrix) {
	TTransitionMatrixBool<double, size_t> mat;

	std::vector<double> vals = {0.01, 0.4};
	mat.fillTransitionMatrix(vals);
	EXPECT_FLOAT_EQ(mat(0, 0), 0.9928571);
	EXPECT_FLOAT_EQ(mat(0, 1), 0.007142857);
	EXPECT_FLOAT_EQ(mat(1, 0), 0.7071429);
	EXPECT_FLOAT_EQ(mat(1, 1), 0.292857143);
}

TEST(TTransitionMatrixBoolTest, fillTransitionMatrix_pi_0) {
	TTransitionMatrixBool<double, size_t> mat;

	std::vector<double> vals = {0.0, 0.4};
	mat.fillTransitionMatrix(vals);
	EXPECT_FLOAT_EQ(mat(0, 0), 1.0);
	EXPECT_FLOAT_EQ(mat(0, 1), 0.0);
	EXPECT_FLOAT_EQ(mat(1, 0), 0.7142857);
	EXPECT_FLOAT_EQ(mat(1, 1), 0.2857143);
}

TEST(TTransitionMatrixBoolTest, fillTransitionMatrix_pi_1) {
	TTransitionMatrixBool<double, size_t> mat;

	std::vector<double> vals = {1.0, 0.4};
	mat.fillTransitionMatrix(vals);
	EXPECT_FLOAT_EQ(mat(0, 0), 0.2857143);
	EXPECT_FLOAT_EQ(mat(0, 1), 0.7142857);
	EXPECT_FLOAT_EQ(mat(1, 0), 0.0);
	EXPECT_FLOAT_EQ(mat(1, 1), 1.0);
}

TEST(TTransitionMatrixBoolTest, fillTransitionMatrix_gamma_0) {
	TTransitionMatrixBool<double, size_t> mat;

	std::vector<double> vals = {0.01, 0.0};
	mat.fillTransitionMatrix(vals);
	EXPECT_FLOAT_EQ(mat(0, 0), 0.99);
	EXPECT_FLOAT_EQ(mat(0, 1), 0.01);
	EXPECT_FLOAT_EQ(mat(1, 0), 0.99);
	EXPECT_FLOAT_EQ(mat(1, 1), 0.01);
}

TEST(TTransitionMatrixBoolTest, fillTransitionMatrix_pi_0_gamma_0) {
	TTransitionMatrixBool<double, size_t> mat;

	std::vector<double> vals = {0.0, 0.0};
	mat.fillTransitionMatrix(vals);
	EXPECT_FLOAT_EQ(mat(0, 0), 1.0);
	EXPECT_FLOAT_EQ(mat(0, 1), 0.0);
	EXPECT_FLOAT_EQ(mat(1, 0), 1.0);
	EXPECT_FLOAT_EQ(mat(1, 1), 0.0);
}

TEST(TTransitionMatrixBoolTest, fillStationary) {
	TTransitionMatrixBool<double, size_t> mat;

	std::vector<double> vals = {0.01, 0.4};
	mat.fillStationary(vals);
	EXPECT_FLOAT_EQ(mat.stationary(0), 0.99);
	EXPECT_FLOAT_EQ(mat.stationary(1), 0.01);
}

TEST(TTransitionMatrixBoolTest, fillStationary_pi_0) {
	TTransitionMatrixBool<double, size_t> mat;

	std::vector<double> vals = {0.0, 0.4};
	mat.fillStationary(vals);
	EXPECT_FLOAT_EQ(mat.stationary(0), 1.0);
	EXPECT_FLOAT_EQ(mat.stationary(1), 0.0);
}

TEST(TTransitionMatrixBoolTest, fillStationary_pi_1) {
	TTransitionMatrixBool<double, size_t> mat;

	std::vector<double> vals = {1.0, 0.4};
	mat.fillStationary(vals);
	EXPECT_FLOAT_EQ(mat.stationary(0), 0.0);
	EXPECT_FLOAT_EQ(mat.stationary(1), 1.0);
}

TEST(TTransitionMatrixBoolTest, optimize) {
	TTransitionMatrixBool<double, size_t> mat;

	EXPECT_TRUE(mat.optimizeWithNelderMead());
	EXPECT_FALSE(mat.optimizeWithLineSearch());
}

TEST(TTransitionMatrixBoolTest, transformNelderMeadSpace) {
	TTransitionMatrixBool<double, size_t> mat;

	std::vector<double> values = {0.01, 0.9};
	auto result                = mat.transformToNelderMeadSpace(values);
	EXPECT_FLOAT_EQ(result[0], -4.59512);
	EXPECT_FLOAT_EQ(result[1], -0.1053605);

	result = mat.transformFromNelderMeadSpace(result);
	EXPECT_FLOAT_EQ(result[0], 0.01);
	EXPECT_FLOAT_EQ(result[1], 0.9);
}

//-------------------------------------------
// TTransitionMatrixCategorical
//-------------------------------------------

TEST(TTransitionMatrixCategorical, constructor) {
	TTransitionMatrixCategorical<double, size_t> mat(2);

	const auto &matrix = mat.getTransitionMatrix();
	EXPECT_EQ(matrix.n_rows, 2);
	EXPECT_EQ(mat.numStates(), 2);
	EXPECT_EQ(mat.D(), 1);

	// invalid number of states
	EXPECT_ANY_THROW((TTransitionMatrixCategorical<double, size_t>(0)));
	EXPECT_ANY_THROW((TTransitionMatrixCategorical<double, size_t>(1)));
}

TEST(TTransitionMatrixCategoricalTest, resize) {
	TTransitionMatrixCategorical<double, size_t> mat;

	mat.resize(2);
	const auto &matrix = mat.getTransitionMatrix();
	EXPECT_EQ(matrix.n_rows, 2);
	EXPECT_EQ(mat.numStates(), 2);
	EXPECT_EQ(mat.D(), 1);

	// invalid number of states
	EXPECT_ANY_THROW(mat.resize(0));
	EXPECT_ANY_THROW(mat.resize(1));
}

TEST(TTransitionMatrixCategoricalTest, fillTransitionMatrix_D1) {
	TTransitionMatrixCategorical<double, size_t> mat(2);

	std::vector<double> values = {0.01, 0.5, 1.0};
	mat.fillTransitionMatrix(values);

	std::vector<double> fromR = {0.993333333333333, 0.00666666666666667, 0.66, 0.34};
	size_t c                  = 0;
	for (size_t i = 0; i < 2; i++) {
		for (size_t j = 0; j < 2; j++, c++) { EXPECT_FLOAT_EQ(mat(i, j), fromR[c]); }
	}
}

TEST(TTransitionMatrixCategoricalTest, fillTransitionMatrix_D2) {
	TTransitionMatrixCategorical<double, size_t> mat(3);

	std::vector<double> values = {0.5, 0.9, 0.2, 0.8};
	mat.fillTransitionMatrix(values);

	std::vector<double> fromR = {0.736842105263158, 0.0526315789473684,
								 0.210526315789474, 0.263157894736842,
								 0.736842105263158, 0,
								 0.263157894736842, 0,
								 0.736842105263158};
	size_t c                  = 0;
	for (size_t i = 0; i < 3; i++) {
		for (size_t j = 0; j < 3; j++, c++) { EXPECT_FLOAT_EQ(mat(i, j), fromR[c]); }
	}
}

TEST(TTransitionMatrixCategoricalTest, fillTransitionMatrix_pi_0) {
	TTransitionMatrixCategorical<double, size_t> mat(3);

	std::vector<double> values = {0.0, 0.9, 0.2, 0.8};
	mat.fillTransitionMatrix(values);

	std::vector<double> fromR = {
		1, 0, 0, 0.526315789473684, 0.473684210526316, 0, 0.526315789473684, 0, 0.473684210526316};
	size_t c = 0;
	for (size_t i = 0; i < 3; i++) {
		for (size_t j = 0; j < 3; j++, c++) { EXPECT_FLOAT_EQ(mat(i, j), fromR[c]); }
	}
}

TEST(TTransitionMatrixCategoricalTest, fillTransitionMatrix_pi_1) {
	TTransitionMatrixCategorical<double, size_t> mat(3);

	std::vector<double> values = {1.0, 0.9, 0.2, 0.8};
	mat.fillTransitionMatrix(values);

	std::vector<double> fromR = {0.473684210526316, 0.105263157894737, 0.421052631578947, 0, 1, 0, 0, 0, 1};
	size_t c                  = 0;
	for (size_t i = 0; i < 3; i++) {
		for (size_t j = 0; j < 3; j++, c++) { EXPECT_FLOAT_EQ(mat(i, j), fromR[c]); }
	}
}

TEST(TTransitionMatrixCategoricalTest, fillTransitionMatrix_gamma_0) {
	TTransitionMatrixCategorical<double, size_t> mat(3);

	std::vector<double> values = {0.5, 0.0, 0.2, 0.8};
	mat.fillTransitionMatrix(values);

	std::vector<double> fromR = {0.5, 0.1, 0.4, 0.5, 0.5, 0, 0.5, 0, 0.5};
	size_t c                  = 0;
	for (size_t i = 0; i < 3; i++) {
		for (size_t j = 0; j < 3; j++, c++) { EXPECT_FLOAT_EQ(mat(i, j), fromR[c]); }
	}
}

TEST(TTransitionMatrixCategoricalTest, fillTransitionMatrix_gamma_1) {
	TTransitionMatrixCategorical<double, size_t> mat(3);

	std::vector<double> values = {0.5, 1.0, 0.2, 0.8};
	mat.fillTransitionMatrix(values);

	std::vector<double> fromR = {0.75, 0.05, 0.2, 0.25, 0.75, 0, 0.25, 0, 0.75};
	size_t c                  = 0;
	for (size_t i = 0; i < 3; i++) {
		for (size_t j = 0; j < 3; j++, c++) { EXPECT_FLOAT_EQ(mat(i, j), fromR[c]); }
	}
}

TEST(TTransitionMatrixCategoricalTest, fillTransitionMatrix_pi_0_gamma_0) {
	TTransitionMatrixCategorical<double, size_t> mat(3);

	std::vector<double> values = {0.0, 0.0, 0.2, 0.8};
	mat.fillTransitionMatrix(values);

	std::vector<double> fromR = {1, 0, 0, 1, 0, 0, 1, 0, 0};
	size_t c                  = 0;
	for (size_t i = 0; i < 3; i++) {
		for (size_t j = 0; j < 3; j++, c++) { EXPECT_FLOAT_EQ(mat(i, j), fromR[c]); }
	}
}

TEST(TTransitionMatrixCategoricalTest, fillStationary) {
	TTransitionMatrixCategorical<double, size_t> mat(3);

	std::vector<double> values = {0.01, 0.5, 0.2, 0.8};
	mat.fillStationary(values);

	EXPECT_FLOAT_EQ(mat.stationary(0), 0.990);
	EXPECT_FLOAT_EQ(mat.stationary(1), 0.002);
	EXPECT_FLOAT_EQ(mat.stationary(2), 0.008);
}

TEST(TTransitionMatrixCategoricalTest, fillStationary_pi_0) {
	TTransitionMatrixCategorical<double, size_t> mat(3);

	std::vector<double> values = {0.0, 0.5, 0.2, 0.8};
	mat.fillStationary(values);

	EXPECT_FLOAT_EQ(mat.stationary(0), 1.0);
	EXPECT_FLOAT_EQ(mat.stationary(1), 0.0);
	EXPECT_FLOAT_EQ(mat.stationary(2), 0.0);
}

TEST(TTransitionMatrixCategoricalTest, fillStationary_pi_1) {
	TTransitionMatrixCategorical<double, size_t> mat(3);

	std::vector<double> values = {1.0, 0.5, 0.2, 0.8};
	mat.fillStationary(values);

	EXPECT_FLOAT_EQ(mat.stationary(0), 0.0);
	EXPECT_FLOAT_EQ(mat.stationary(1), 0.2);
	EXPECT_FLOAT_EQ(mat.stationary(2), 0.8);
}

TEST(TTransitionMatrixCategoricalTest, optimize) {
	TTransitionMatrixCategorical<double, size_t> mat(3);

	EXPECT_TRUE(mat.optimizeWithNelderMead());
	EXPECT_FALSE(mat.optimizeWithLineSearch());
}

TEST(TTransitionMatrixCategoricalTest, transformNelderMeadSpace_D1) {
	TTransitionMatrixCategorical<double, size_t> mat(2);

	std::vector<double> values = {0.01, 0.9, 1.0};
	auto result                = mat.transformToNelderMeadSpace(values);
	EXPECT_EQ(result.size(), 3);
	EXPECT_FLOAT_EQ(result[0], -4.59512);
	EXPECT_FLOAT_EQ(result[1], -0.1053605);
	EXPECT_FLOAT_EQ(result[2], 0.0);

	result = mat.transformFromNelderMeadSpace(result);
	EXPECT_EQ(result.size(), 3);
	EXPECT_FLOAT_EQ(result[0], 0.01);
	EXPECT_FLOAT_EQ(result[1], 0.9);
	EXPECT_FLOAT_EQ(result[2], 1.0);
}

TEST(TTransitionMatrixCategoricalTest, transformNelderMeadSpace_D2) {
	TTransitionMatrixCategorical<double, size_t> mat(3);

	std::vector<double> values = {0.01, 0.9, 0.2, 0.8};
	auto result                = mat.transformToNelderMeadSpace(values);
	EXPECT_EQ(result.size(), 4);
	EXPECT_FLOAT_EQ(result[0], -4.59512);
	EXPECT_FLOAT_EQ(result[1], -0.1053605);
	EXPECT_FLOAT_EQ(result[2], -1.609438);
	EXPECT_FLOAT_EQ(result[3], -0.2231436);

	result = mat.transformFromNelderMeadSpace(result);
	EXPECT_EQ(result.size(), 4);
	EXPECT_FLOAT_EQ(result[0], 0.01);
	EXPECT_FLOAT_EQ(result[1], 0.9);
	EXPECT_FLOAT_EQ(result[2], 0.2);
	EXPECT_FLOAT_EQ(result[3], 0.8);
}

//-------------------------------------------
// TTransitionMatrixBoolGeneratingMatrix
//-------------------------------------------

TEST(TTransitionMatrixBoolGeneratingMatrixTest, constructor) {
	TTransitionMatrixBoolGeneratingMatrix<double, size_t> mat(2);

	const auto &matrix = mat.getTransitionMatrix();
	EXPECT_EQ(matrix.n_rows, 2);
	EXPECT_EQ(mat.numStates(), 2);

	// invalid number of states
	EXPECT_ANY_THROW((TTransitionMatrixBoolGeneratingMatrix<double, size_t>(1)));
	EXPECT_ANY_THROW((TTransitionMatrixBoolGeneratingMatrix<double, size_t>(3)));
}

TEST(TTransitionMatrixBoolGeneratingMatrixTest, resize) {
	TTransitionMatrixBoolGeneratingMatrix<double, size_t> mat;

	mat.resize(2);
	const auto &matrix = mat.getTransitionMatrix();
	EXPECT_EQ(matrix.n_rows, 2);
	EXPECT_EQ(mat.numStates(), 2);

	// invalid number of states
	EXPECT_ANY_THROW(mat.resize(1));
	EXPECT_ANY_THROW(mat.resize(3));
}

TEST(TTransitionMatrixBoolGeneratingMatrixTest, fillTransitionMatrix) {
	TTransitionMatrixBoolGeneratingMatrix<double, size_t> mat;
	std::vector<double> values = {0.1, 0.2};
	mat.fillTransitionMatrix(values);

	double absError = 0.002; // not exactly the same as in R because of matrix exponential
	EXPECT_NEAR(mat(0, 0), 0.9136061, absError);
	EXPECT_NEAR(mat(0, 1), 0.08639393, absError);
	EXPECT_NEAR(mat(1, 0), 0.1727879, absError);
	EXPECT_NEAR(mat(1, 1), 0.82721215, absError);
}

TEST(TTransitionMatrixBoolGeneratingMatrixTest, fillStationary) {
	TTransitionMatrixBoolGeneratingMatrix<double, size_t> mat;
	std::vector<double> values = {0.1, 0.2};
	mat.fillTransitionMatrix(values);
	mat.fillStationary(values);

	EXPECT_FLOAT_EQ(mat.stationary(0), 0.6666667);
	EXPECT_FLOAT_EQ(mat.stationary(1), 0.3333333);
}

TEST(TTransitionMatrixBoolGeneratingMatrixTest, optimize) {
	TTransitionMatrixBoolGeneratingMatrix<double, size_t> mat;

	EXPECT_TRUE(mat.optimizeWithNelderMead());
	EXPECT_FALSE(mat.optimizeWithLineSearch());
}

TEST(TTransitionMatrixBoolGeneratingMatrixTest, transformNelderMeadSpace) {
	TTransitionMatrixBoolGeneratingMatrix<double, size_t> mat;

	std::vector<double> values = {0.01, 0.9};
	auto result                = mat.transformToNelderMeadSpace(values);
	EXPECT_FLOAT_EQ(result[0], -4.60517);
	EXPECT_FLOAT_EQ(result[1], -0.1053605);

	result = mat.transformFromNelderMeadSpace(result);
	EXPECT_FLOAT_EQ(result[0], 0.01);
	EXPECT_FLOAT_EQ(result[1], 0.9);
}

//-------------------------------------------
// TTransitionMatrixLadder
//-------------------------------------------

TEST(TTransitionMatrixLadderTest, constructor) {
	TTransitionMatrixLadder<double, size_t> mat(2);

	const auto &matrix = mat.getTransitionMatrix();
	EXPECT_EQ(matrix.n_rows, 2);
	EXPECT_EQ(mat.numStates(), 2);

	// invalid number of states
	EXPECT_ANY_THROW((TTransitionMatrixLadder<double, size_t>(1)));
}

TEST(TTransitionMatrixLadderTest, resize) {
	TTransitionMatrixLadder<double, size_t> mat;

	mat.resize(2);
	const auto &matrix = mat.getTransitionMatrix();
	EXPECT_EQ(matrix.n_rows, 2);
	EXPECT_EQ(mat.numStates(), 2);

	// invalid number of states
	EXPECT_ANY_THROW(mat.resize(1));
}

TEST(TTransitionMatrixLadderTest, fillTransitionMatrix_3_states) {
	TTransitionMatrixLadder<double, size_t> mat(3);
	EXPECT_EQ(mat.numStates(), 3);
	std::vector<double> values = {0.2};
	mat.fillTransitionMatrix(values);

	double absError = 0.001; // not exactly the same as in R because of matrix exponential
	EXPECT_NEAR(mat(0, 0), 0.83416732, absError);
	EXPECT_NEAR(mat(0, 1), 0.1503961, absError);
	EXPECT_NEAR(mat(0, 2), 0.01543656, absError);
	EXPECT_NEAR(mat(1, 0), 0.15039612, absError);
	EXPECT_NEAR(mat(1, 1), 0.6992078, absError);
	EXPECT_NEAR(mat(1, 2), 0.15039612, absError);
	EXPECT_NEAR(mat(2, 0), 0.01543656, absError);
	EXPECT_NEAR(mat(2, 1), 0.1503961, absError);
	EXPECT_NEAR(mat(2, 2), 0.83416732, absError);
}

TEST(TTransitionMatrixLadderTest, fillTransitionMatrix_5_states) {
	TTransitionMatrixLadder<double, size_t> mat(5);
	EXPECT_EQ(mat.numStates(), 5);
	std::vector<double> values = {0.2};
	mat.fillTransitionMatrix(values);

	double absError = 0.001; // not exactly the same as in R because of matrix exponential
	EXPECT_NEAR(mat(0, 0), 8.341654e-01, absError);
	EXPECT_NEAR(mat(0, 1), 0.1503492734, absError);
	EXPECT_NEAR(mat(0, 2), 0.01448878, absError);
	EXPECT_NEAR(mat(0, 3), 0.0009478418, absError);
	EXPECT_NEAR(mat(0, 4), 4.870557e-05, absError);
	EXPECT_NEAR(mat(1, 0), 1.503493e-01, absError);
	EXPECT_NEAR(mat(1, 1), 0.6983049058, absError);
	EXPECT_NEAR(mat(1, 2), 0.13680833, absError);
	EXPECT_NEAR(mat(1, 3), 0.0135896481, absError);
	EXPECT_NEAR(mat(1, 4), 9.478418e-04, absError);
	EXPECT_NEAR(mat(2, 0), 1.448878e-02, absError);
	EXPECT_NEAR(mat(2, 1), 0.1368083309, absError);
	EXPECT_NEAR(mat(2, 2), 0.69740577, absError);
	EXPECT_NEAR(mat(2, 3), 0.1368083309, absError);
	EXPECT_NEAR(mat(2, 4), 1.448878e-02, absError);
	EXPECT_NEAR(mat(3, 0), 9.478418e-04, absError);
	EXPECT_NEAR(mat(3, 1), 0.0135896481, absError);
	EXPECT_NEAR(mat(3, 2), 0.13680833, absError);
	EXPECT_NEAR(mat(3, 3), 0.6983049058, absError);
	EXPECT_NEAR(mat(3, 4), 1.503493e-01, absError);
	EXPECT_NEAR(mat(4, 0), 4.870557e-05, absError);
	EXPECT_NEAR(mat(4, 1), 0.0009478418, absError);
	EXPECT_NEAR(mat(4, 2), 0.01448878, absError);
	EXPECT_NEAR(mat(4, 3), 0.1503492734, absError);
	EXPECT_NEAR(mat(4, 4), 8.341654e-01, absError);
}

TEST(TTransitionMatrixLadderTest, fillStationary) {
	TTransitionMatrixLadder<double, size_t> mat(3);

	std::vector<double> values = {0.2};
	mat.fillTransitionMatrix(values);
	mat.fillStationary(values);

	for (size_t i = 0; i < 3; i++) { EXPECT_FLOAT_EQ(mat.stationary(i), 0.3333333); }
}

TEST(TTransitionMatrixLadderTest, optimize) {
	TTransitionMatrixLadder<double, size_t> mat(3);

	EXPECT_FALSE(mat.optimizeWithNelderMead());
	EXPECT_TRUE(mat.optimizeWithLineSearch());
}

//-------------------------------------------
// TTransitionMatrixScaledLadder
//-------------------------------------------

TEST(TTransitionMatrixScaledLadderTest, constructor) {
	TTransitionMatrixScaledLadder<double, size_t> mat(3);

	const auto &matrix = mat.getTransitionMatrix();
	EXPECT_EQ(matrix.n_rows, 3);
	EXPECT_EQ(mat.numStates(), 3);

	// invalid number of states
	EXPECT_ANY_THROW((TTransitionMatrixScaledLadder<double, size_t>(1)));
	EXPECT_ANY_THROW((TTransitionMatrixScaledLadder<double, size_t>(2)));
	EXPECT_ANY_THROW((TTransitionMatrixScaledLadder<double, size_t>(4)));
	EXPECT_ANY_THROW((TTransitionMatrixScaledLadder<double, size_t>(6)));
}

TEST(TTransitionMatrixScaledLadderTest, resize) {
	TTransitionMatrixScaledLadder<double, size_t> mat;

	mat.resize(3);
	const auto &matrix = mat.getTransitionMatrix();
	EXPECT_EQ(matrix.n_rows, 3);
	EXPECT_EQ(mat.numStates(), 3);

	// invalid number of states
	EXPECT_ANY_THROW(mat.resize(1));
	EXPECT_ANY_THROW(mat.resize(2));
	EXPECT_ANY_THROW(mat.resize(4));
	EXPECT_ANY_THROW(mat.resize(6));
}

TEST(TTransitionMatrixScaledLadderTest, fillTransitionMatrix_3_states) {
	TTransitionMatrixScaledLadder<double, size_t> mat(3);
	EXPECT_EQ(mat.numStates(), 3);
	std::vector<double> values = {2, 0.3};
	mat.fillTransitionMatrix(values);

	std::vector<double> fromR = {0.267905830361546, 0.599523622513521, 0.132570547124933,
								 0.179857086754056, 0.640285826491888, 0.179857086754056,
								 0.132570547124933, 0.599523622513521, 0.267905830361546};

	double absError = 0.001; // not exactly the same as in R because of matrix exponential
	size_t counter  = 0;
	for (size_t r = 0; r < 3; r++) {
		for (size_t c = 0; c < 3; c++, counter++) { EXPECT_NEAR(mat(r, c), fromR[counter], absError); }
	}
}

TEST(TTransitionMatrixScaledLadderTest, fillTransitionMatrix_5_states) {
	TTransitionMatrixScaledLadder<double, size_t> mat(5);
	EXPECT_EQ(mat.numStates(), 5);
	std::vector<double> values = {2, 0.01, 0.3};
	mat.fillTransitionMatrix(values);

	std::vector<double> fromR = {
		0.209268659606433,    0.247319599545216,    0.542478003531398, 0.000822975402480652, 0.000110761914471979,
		0.0741958798635647,   0.136700213753463,    0.787365710457906, 0.00149130330432198,  0.000246892620744196,
		0.000488230203178258, 0.00236209713137372,  0.994299345330896, 0.00236209713137372,  0.000488230203178258,
		0.000246892620744196, 0.00149130330432198,  0.787365710457906, 0.136700213753463,    0.0741958798635647,
		0.00011076191447198,  0.000822975402480653, 0.542478003531399, 0.247319599545216,    0.209268659606434};

	double absError = 0.001; // not exactly the same as in R because of matrix exponential
	size_t counter  = 0;
	for (size_t r = 0; r < 5; r++) {
		for (size_t c = 0; c < 5; c++, counter++) { EXPECT_NEAR(mat(r, c), fromR[counter], absError); }
	}
}

TEST(TTransitionMatrixScaledLadderTest, fillTransitionMatrix_7_states) {
	TTransitionMatrixScaledLadder<double, size_t> mat(7);
	EXPECT_EQ(mat.numStates(), 7);
	std::vector<double> values = {2, 0.01, 0.3};
	mat.fillTransitionMatrix(values);

	std::vector<double> fromR = {
		0.215578600481377,    0.288089915749578,    0.222465068187123,    0.273485330060978,    0.000341510796910093,
		3.60828999967124e-05, 3.49182403725808e-06, 0.0864269747248735,   0.19589114621264,     0.222170851283625,
		0.494650997064645,    0.000754085651106619, 9.51201931112723e-05, 1.08248699990137e-05, 0.0200218561368411,
		0.0666512553850874,   0.130635578747697,    0.780957728118089,    0.00147661994523218,  0.000226225695331986,
		3.07359717219084e-05, 7.3841039116464e-05,  0.000445185897358181, 0.00234287318435427,  0.994276199758342,
		0.00234287318435426,  0.000445185897358181, 7.3841039116464e-05,  3.07359717219084e-05, 0.000226225695331985,
		0.00147661994523218,  0.780957728118088,    0.130635578747697,    0.0666512553850874,   0.020021856136841,
		1.08248699990137e-05, 9.51201931112721e-05, 0.000754085651106618, 0.494650997064644,    0.222170851283624,
		0.19589114621264,     0.0864269747248735,   3.49182403725807e-06, 3.60828999967123e-05, 0.000341510796910092,
		0.273485330060978,    0.222465068187123,    0.288089915749578,    0.215578600481377};

	double absError = 0.001; // not exactly the same as in R because of matrix exponential
	size_t counter  = 0;
	for (size_t r = 0; r < 7; r++) {
		for (size_t c = 0; c < 7; c++, counter++) { EXPECT_NEAR(mat(r, c), fromR[counter], absError); }
	}
}

TEST(TTransitionMatrixScaledLadderTest, fillStationary_3States) {
	TTransitionMatrixScaledLadder<double, size_t> mat(3);

	std::vector<double> values = {2, 0.3};
	mat.fillStationary(values);

	std::vector<double> fromR = {0.1875, 0.625, 0.1875};
	for (size_t i = 0; i < 3; i++) { EXPECT_FLOAT_EQ(mat.stationary(i), fromR[i]); }
}

TEST(TTransitionMatrixScaledLadderTest, fillStationary_5States) {
	TTransitionMatrixScaledLadder<double, size_t> mat(5);

	std::vector<double> values = {2, 0.01, 0.3};
	mat.fillStationary(values);

	std::vector<double> fromR = {0.000893034332208742, 0.00297678110736251, 0.992260369120857, 0.00297678110736257,
								 0.000893034332208759};
	for (size_t i = 0; i < 5; i++) { EXPECT_FLOAT_EQ(mat.stationary(i), fromR[i]); }
}

TEST(TTransitionMatrixScaledLadderTest, fillStationary_7States) {
	TTransitionMatrixScaledLadder<double, size_t> mat(7);

	std::vector<double> values = {2, 0.01, 0.3};
	mat.fillStationary(values);

	std::vector<double> fromR = {0.000267766824682061, 0.000892556082273756, 0.0029751869409128, 0.991728980304263,
								 0.00297518694091281,  0.000892556082273763, 0.00026776682468213};
	for (size_t i = 0; i < 7; i++) { EXPECT_FLOAT_EQ(mat.stationary(i), fromR[i]); }
}

TEST(TTransitionMatrixScaledLadderTest, transformNelderMeadSpace_3States) {
	TTransitionMatrixScaledLadder<double, size_t> mat(3);

	std::vector<double> values = {2, 0.3, 1.0};
	auto result                = mat.transformToNelderMeadSpace(values);
	EXPECT_FLOAT_EQ(result[0], 0.6931472);
	EXPECT_FLOAT_EQ(result[1], coretools::logit(0.3));
	EXPECT_FLOAT_EQ(result[2], 0.0);

	result = mat.transformFromNelderMeadSpace(result);
	EXPECT_FLOAT_EQ(result[0], 2);
	EXPECT_FLOAT_EQ(result[1], 0.3);
	EXPECT_FLOAT_EQ(result[2], 1.0);
}

TEST(TTransitionMatrixScaledLadderTest, transformNelderMeadSpace_7States) {
	TTransitionMatrixScaledLadder<double, size_t> mat(7);

	std::vector<double> values = {2, 0.01, 0.3};
	auto result                = mat.transformToNelderMeadSpace(values);
	EXPECT_FLOAT_EQ(result[0], 0.6931472);
	EXPECT_FLOAT_EQ(result[1], coretools::logit(0.01));
	EXPECT_FLOAT_EQ(result[2], coretools::logit(0.3));

	result = mat.transformFromNelderMeadSpace(result);
	EXPECT_FLOAT_EQ(result[0], 2);
	EXPECT_FLOAT_EQ(result[1], 0.01);
	EXPECT_FLOAT_EQ(result[2], 0.3);
}

TEST(TTransitionMatrixScaledLadderTest, optimize) {
	TTransitionMatrixScaledLadder<double, size_t> mat(7);

	EXPECT_TRUE(mat.optimizeWithNelderMead());
	EXPECT_FALSE(mat.optimizeWithLineSearch());
}

//-------------------------------------------
// TTransitionMatrixScaledLadderShifted
//-------------------------------------------

TEST(TTransitionMatrixScaledLadderAttractorShiftTest, constructor) {
	TTransitionMatrixScaledLadderAttractorShift<double, size_t> mat(2);
	const auto &matrix = mat.getTransitionMatrix();
	EXPECT_EQ(matrix.n_rows, 2);
	EXPECT_EQ(mat.numStates(), 2);

	TTransitionMatrixScaledLadderAttractorShift<double, size_t> mat3(3);
	const auto &matrix3 = mat3.getTransitionMatrix();
	EXPECT_EQ(matrix3.n_rows, 3);
	EXPECT_EQ(mat3.numStates(), 3);

	TTransitionMatrixScaledLadderAttractorShift<double, size_t> mat4(4);
	const auto &matrix4 = mat4.getTransitionMatrix();
	EXPECT_EQ(matrix4.n_rows, 4);
	EXPECT_EQ(mat4.numStates(), 4);

	// invalid number of states
	EXPECT_ANY_THROW((TTransitionMatrixScaledLadderAttractorShift<double, size_t>(1)));
}

TEST(TTransitionMatrixScaledLadderAttractorShiftTest, resize) {
	TTransitionMatrixScaledLadderAttractorShift<double, size_t> mat;

	mat.resize(2);
	const auto &matrix2 = mat.getTransitionMatrix();
	EXPECT_EQ(matrix2.n_rows, 2);
	EXPECT_EQ(mat.numStates(), 2);

	mat.resize(3);
	const auto &matrix3 = mat.getTransitionMatrix();
	EXPECT_EQ(matrix3.n_rows, 3);
	EXPECT_EQ(mat.numStates(), 3);

	mat.resize(4);
	const auto &matrix4 = mat.getTransitionMatrix();
	EXPECT_EQ(matrix4.n_rows, 4);
	EXPECT_EQ(mat.numStates(), 4);

	// invalid number of states
	EXPECT_ANY_THROW(mat.resize(1));
}

TEST(TTransitionMatrixScaledLadderAttractorShiftTest, fillTransitionMatrix_2_states_ix0) {
	TTransitionMatrixScaledLadderAttractorShift<double, size_t> mat(2);
	EXPECT_EQ(mat.numStates(), 2);
	mat.setIxAttractor(0);
	std::vector<double> values = {2, 0.01, 0.3};
	mat.fillTransitionMatrix(values);

	std::vector<double> fromR = {0.991412430347328, 0.00858756965267206, 0.858756965267206, 0.141243034732794};

	double absError = 0.001; // not exactly the same as in R because of matrix exponential
	size_t counter  = 0;
	for (size_t r = 0; r < 2; r++) {
		for (size_t c = 0; c < 2; c++, counter++) { EXPECT_NEAR(mat(r, c), fromR[counter], absError); }
	}
}

TEST(TTransitionMatrixScaledLadderAttractorShiftTest, fillTransitionMatrix_2_states_ix1) {
	TTransitionMatrixScaledLadderAttractorShift<double, size_t> mat(2);
	EXPECT_EQ(mat.numStates(), 2);
	mat.setIxAttractor(1);
	std::vector<double> values = {2, 0.01, 0.3};
	mat.fillTransitionMatrix(values);

	std::vector<double> fromR = {0.141243034732794, 0.858756965267206, 0.00858756965267206, 0.991412430347328};

	double absError = 0.001; // not exactly the same as in R because of matrix exponential
	size_t counter  = 0;
	for (size_t r = 0; r < 2; r++) {
		for (size_t c = 0; c < 2; c++, counter++) { EXPECT_NEAR(mat(r, c), fromR[counter], absError); }
	}
}

TEST(TTransitionMatrixScaledLadderAttractorShiftTest, fillTransitionMatrix_3_states_ix0) {
	TTransitionMatrixScaledLadderAttractorShift<double, size_t> mat(3);
	EXPECT_EQ(mat.numStates(), 3);
	mat.setIxAttractor(0);
	std::vector<double> values = {2, 0.01, 0.3};
	mat.fillTransitionMatrix(values);

	std::vector<double> fromR = {0.997143627631343, 0.00236730294549401, 0.00048906942316356,
								 0.789100981831338, 0.136702795250617,   0.0741962229180449,
								 0.543410470181734, 0.24732074306015,    0.209268786758117};

	double absError = 0.001; // not exactly the same as in R because of matrix exponential
	size_t counter  = 0;
	for (size_t r = 0; r < 3; r++) {
		for (size_t c = 0; c < 3; c++, counter++) { EXPECT_NEAR(mat(r, c), fromR[counter], absError); }
	}
}

TEST(TTransitionMatrixScaledLadderAttractorShiftTest, fillTransitionMatrix_3_states_ix1) {
	TTransitionMatrixScaledLadderAttractorShift<double, size_t> mat(3);
	EXPECT_EQ(mat.numStates(), 3);
	mat.setIxAttractor(1);
	std::vector<double> values = {2, 0.01, 0.3};
	mat.fillTransitionMatrix(values);

	std::vector<double> fromR = {0.141211127343025,   0.852913028550563, 0.00587584410641224,
								 0.00852913028550563, 0.982941739428989, 0.00852913028550563,
								 0.00587584410641224, 0.852913028550563, 0.141211127343025};

	double absError = 0.001; // not exactly the same as in R because of matrix exponential
	size_t counter  = 0;
	for (size_t r = 0; r < 3; r++) {
		for (size_t c = 0; c < 3; c++, counter++) { EXPECT_NEAR(mat(r, c), fromR[counter], absError); }
	}
}

TEST(TTransitionMatrixScaledLadderAttractorShiftTest, fillTransitionMatrix_3_states_ix2) {
	TTransitionMatrixScaledLadderAttractorShift<double, size_t> mat(3);
	EXPECT_EQ(mat.numStates(), 3);
	mat.setIxAttractor(2);
	std::vector<double> values = {2, 0.01, 0.3};
	mat.fillTransitionMatrix(values);

	std::vector<double> fromR = {0.209268786758117,   0.24732074306015,    0.543410470181734,
								 0.0741962229180448,  0.136702795250617,   0.789100981831338,
								 0.00048906942316356, 0.00236730294549401, 0.997143627631343};

	double absError = 0.001; // not exactly the same as in R because of matrix exponential
	size_t counter  = 0;
	for (size_t r = 0; r < 3; r++) {
		for (size_t c = 0; c < 3; c++, counter++) { EXPECT_NEAR(mat(r, c), fromR[counter], absError); }
	}
}

TEST(TTransitionMatrixScaledLadderAttractorShiftTest, fillTransitionMatrix_5_states_ix0) {
	TTransitionMatrixScaledLadderAttractorShift<double, size_t> mat(5);
	EXPECT_EQ(mat.numStates(), 5);
	mat.setIxAttractor(0);
	std::vector<double> values = {2, 0.01, 0.3};
	mat.fillTransitionMatrix(values);

	std::vector<double> fromR = {
		0.997131755425914, 0.00234735703711022, 0.000443697676762491, 6.79586868214404e-05, 9.23117339192616e-06,
		0.782452345703407, 0.130190288636091,   0.0655633037311855,   0.0180660198710884,   0.00372804205822725,
		0.49299741862499,  0.218544345770618,   0.188931362617177,    0.0777635816358714,   0.0217632913513426,
		0.251698840079409, 0.200733554123205,   0.259211938786238,    0.201255600884691,    0.0871000661264566,
		0.113965103604027, 0.138075631786194,   0.241814348348251,    0.290333553754855,    0.215811362506673};

	double absError = 0.001; // not exactly the same as in R because of matrix exponential
	size_t counter  = 0;
	for (size_t r = 0; r < 5; r++) {
		for (size_t c = 0; c < 5; c++, counter++) { EXPECT_NEAR(mat(r, c), fromR[counter], absError); }
	}
}

TEST(TTransitionMatrixScaledLadderAttractorShiftTest, fillTransitionMatrix_5_states_ix1) {
	TTransitionMatrixScaledLadderAttractorShift<double, size_t> mat(5);
	EXPECT_EQ(mat.numStates(), 5);
	mat.setIxAttractor(1);
	std::vector<double> values = {2, 0.01, 0.3};
	mat.fillTransitionMatrix(values);

	std::vector<double> fromR = {
		0.137111325659816,    0.860989687397757, 0.00161967560621899, 0.000246148132415641, 3.31632037918931e-05,
		0.00258296906219327,  0.994554750539406, 0.00234321451274321, 0.000445221445932898, 7.38444397246922e-05,
		0.00161967560621899,  0.781071504247738, 0.130635697080912,   0.0666512660377469,   0.0200218570273832,
		0.000820493774718801, 0.494690495480997, 0.22217088679249,    0.19589114901908,     0.0864269749327144,
		0.000368480042132145, 0.273497924906267, 0.222465078082035,   0.288089916442381,    0.215578600527184};

	double absError = 0.001; // not exactly the same as in R because of matrix exponential
	size_t counter  = 0;
	for (size_t r = 0; r < 5; r++) {
		for (size_t c = 0; c < 5; c++, counter++) { EXPECT_NEAR(mat(r, c), fromR[counter], absError); }
	}
}

TEST(TTransitionMatrixScaledLadderAttractorShiftTest, fillTransitionMatrix_5_states_ix2) {
	TTransitionMatrixScaledLadderAttractorShift<double, size_t> mat(5);
	EXPECT_EQ(mat.numStates(), 5);
	mat.setIxAttractor(2);
	std::vector<double> values = {2, 0.01, 0.3};
	mat.fillTransitionMatrix(values);

	std::vector<double> fromR = {
		0.209268659606433,    0.247319599545216,    0.542478003531398, 0.000822975402480652, 0.000110761914471979,
		0.0741958798635647,   0.136700213753463,    0.787365710457906, 0.00149130330432198,  0.000246892620744196,
		0.000488230203178258, 0.00236209713137372,  0.994299345330896, 0.00236209713137372,  0.000488230203178258,
		0.000246892620744196, 0.00149130330432198,  0.787365710457906, 0.136700213753463,    0.0741958798635647,
		0.00011076191447198,  0.000822975402480653, 0.542478003531399, 0.247319599545216,    0.209268659606434};

	double absError = 0.001; // not exactly the same as in R because of matrix exponential
	size_t counter  = 0;
	for (size_t r = 0; r < 5; r++) {
		for (size_t c = 0; c < 5; c++, counter++) { EXPECT_NEAR(mat(r, c), fromR[counter], absError); }
	}
}

TEST(TTransitionMatrixScaledLadderAttractorShiftTest, fillTransitionMatrix_5_states_ix3) {
	TTransitionMatrixScaledLadderAttractorShift<double, size_t> mat(5);
	EXPECT_EQ(mat.numStates(), 5);
	mat.setIxAttractor(3);
	std::vector<double> values = {2, 0.01, 0.3};
	mat.fillTransitionMatrix(values);

	std::vector<double> fromR = {
		0.215578600527184,    0.288089916442382,    0.222465078082035,   0.273497924906267, 0.000368480042132145,
		0.0864269749327145,   0.19589114901908,     0.22217088679249,    0.494690495480997, 0.000820493774718801,
		0.0200218570273832,   0.0666512660377469,   0.130635697080912,   0.781071504247738, 0.00161967560621899,
		7.38444397246922e-05, 0.000445221445932897, 0.00234321451274322, 0.994554750539406, 0.00258296906219327,
		3.31632037918931e-05, 0.000246148132415641, 0.00161967560621899, 0.860989687397757, 0.137111325659816};

	double absError = 0.001; // not exactly the same as in R because of matrix exponential
	size_t counter  = 0;
	for (size_t r = 0; r < 5; r++) {
		for (size_t c = 0; c < 5; c++, counter++) { EXPECT_NEAR(mat(r, c), fromR[counter], absError); }
	}
}

TEST(TTransitionMatrixScaledLadderAttractorShiftTest, fillTransitionMatrix_5_states_ix4) {
	TTransitionMatrixScaledLadderAttractorShift<double, size_t> mat(5);
	EXPECT_EQ(mat.numStates(), 5);
	mat.setIxAttractor(4);
	std::vector<double> values = {2, 0.01, 0.3};
	mat.fillTransitionMatrix(values);

	std::vector<double> fromR = {
		0.215811362506673,    0.290333553754855,    0.241814348348251,    0.138075631786194,   0.113965103604026,
		0.0871000661264566,   0.201255600884691,    0.259211938786238,    0.200733554123205,   0.251698840079409,
		0.0217632913513426,   0.0777635816358714,   0.188931362617177,    0.218544345770618,   0.49299741862499,
		0.00372804205822725,  0.0180660198710884,   0.0655633037311854,   0.130190288636091,   0.782452345703407,
		9.23117339192614e-06, 6.79586868214404e-05, 0.000443697676762491, 0.00234735703711022, 0.997131755425914};

	double absError = 0.001; // not exactly the same as in R because of matrix exponential
	size_t counter  = 0;
	for (size_t r = 0; r < 5; r++) {
		for (size_t c = 0; c < 5; c++, counter++) { EXPECT_NEAR(mat(r, c), fromR[counter], absError); }
	}
}

TEST(TTransitionMatrixScaledLadderAttractorShiftTest, fillStationary_2States) {
	TTransitionMatrixScaledLadderAttractorShift<double, size_t> mat(3);

	// ix = 0
	mat.setIxAttractor(0);
	std::vector<double> values = {2, 0.01, 0.3};
	mat.fillTransitionMatrix(values);
	mat.fillStationary(values);
	std::vector<double> fromR = {0.99009900990099, 0.00990099009900992};
	double absError           = 0.01; // not exactly the same as in R because of matrix exponential and solve
	for (size_t i = 0; i < 2; i++) { EXPECT_NEAR(mat.stationary(i), fromR[i], absError); }

	// ix = 1
	mat.setIxAttractor(1);
	mat.fillTransitionMatrix(values);
	mat.fillStationary(values);
	fromR = {0.00990099009900997, 0.99009900990099};
	for (size_t i = 0; i < 2; i++) { EXPECT_NEAR(mat.stationary(i), fromR[i], absError); }
}

TEST(TTransitionMatrixScaledLadderAttractorShiftTest, fillStationary_3States) {
	TTransitionMatrixScaledLadderAttractorShift<double, size_t> mat(3);

	// ix = 0
	mat.setIxAttractor(0);
	std::vector<double> values = {2, 0.01, 0.3};
	mat.fillTransitionMatrix(values);
	mat.fillStationary(values);
	std::vector<double> fromR = {0.996115150911445, 0.00298834545273429, 0.000896503635820322};
	double absError           = 0.02; // not exactly the same as in R because of matrix exponential and solve
	for (size_t i = 0; i < 3; i++) { EXPECT_NEAR(mat.stationary(i), fromR[i], absError); }

	// ix = 1
	mat.setIxAttractor(1);
	mat.fillTransitionMatrix(values);
	mat.fillStationary(values);
	fromR = {0.00980392156862741, 0.980392156862745, 0.00980392156862749};
	for (size_t i = 0; i < 3; i++) { EXPECT_NEAR(mat.stationary(i), fromR[i], absError); }

	// ix = 2
	mat.setIxAttractor(2);
	mat.fillTransitionMatrix(values);
	mat.fillStationary(values);
	fromR = {0.00980392156862728, 0.00980392156862729, 0.980392156862746};
	for (size_t i = 0; i < 3; i++) { EXPECT_NEAR(mat.stationary(i), fromR[i], absError); }
}

TEST(TTransitionMatrixScaledLadderAttractorShiftTest, fillStationary_5States) {
	TTransitionMatrixScaledLadderAttractorShift<double, size_t> mat(5);

	// ix = 0
	mat.setIxAttractor(0);
	std::vector<double> values = {2, 0.01, 0.3};
	mat.fillTransitionMatrix(values);
	mat.fillStationary(values);
	std::vector<double> fromR = {0.995766994506353, 0.00298730098351915, 0.000896190295055952, 0.000268857088516886,
								 8.06571265552904e-05};
	for (size_t i = 0; i < 5; i++) { EXPECT_FLOAT_EQ(mat.stationary(i), fromR[i]); }

	// ix = 1
	mat.setIxAttractor(1);
	mat.fillTransitionMatrix(values);
	mat.fillStationary(values);
	fromR = {0.00297864312876674, 0.992881042922248, 0.00297864312876662, 0.000893592938629988, 0.000268077881588865};
	for (size_t i = 0; i < 5; i++) { EXPECT_FLOAT_EQ(mat.stationary(i), fromR[i]); }

	// ix = 2
	mat.setIxAttractor(2);
	mat.fillTransitionMatrix(values);
	mat.fillStationary(values);
	fromR = {0.000893034332208742, 0.00297678110736251, 0.992260369120857, 0.00297678110736257, 0.000893034332208759};
	for (size_t i = 0; i < 5; i++) { EXPECT_FLOAT_EQ(mat.stationary(i), fromR[i]); }

	// ix = 3
	mat.setIxAttractor(3);
	mat.fillTransitionMatrix(values);
	mat.fillStationary(values);
	fromR = {0.000268077881588923, 0.000893592938630084, 0.00297864312876666, 0.992881042922248, 0.00297864312876666};
	for (size_t i = 0; i < 5; i++) { EXPECT_FLOAT_EQ(mat.stationary(i), fromR[i]); }

	// ix = 4
	mat.setIxAttractor(4);
	mat.fillTransitionMatrix(values);
	mat.fillStationary(values);
	fromR = {8.06571265552235e-05, 0.000268857088516862, 0.000896190295055813, 0.00298730098351926, 0.995766994506353};
	for (size_t i = 0; i < 5; i++) { EXPECT_FLOAT_EQ(mat.stationary(i), fromR[i]); }
}

TEST(TTransitionMatrixScaledLadderAttractorShiftTest, transformNelderMeadSpace_5States) {
	TTransitionMatrixScaledLadderAttractorShift<double, size_t> mat(5);

	std::vector<double> values = {2, 0.01, 0.3};
	auto result                = mat.transformToNelderMeadSpace(values);
	EXPECT_FLOAT_EQ(result[0], log(2.0));
	EXPECT_FLOAT_EQ(result[1], coretools::logit(0.01));
	EXPECT_FLOAT_EQ(result[2], coretools::logit(0.3));

	result = mat.transformFromNelderMeadSpace(result);
	EXPECT_FLOAT_EQ(result[0], 2);
	EXPECT_FLOAT_EQ(result[1], 0.01);
	EXPECT_FLOAT_EQ(result[2], 0.3);
}

TEST(TTransitionMatrixScaledLadderAttractorShiftTest, transformNelderMeadSpace_2States) {
	TTransitionMatrixScaledLadderAttractorShift<double, size_t> mat(2);

	std::vector<double> values = {2, 0.01, 0.3};
	auto result                = mat.transformToNelderMeadSpace(values);
	EXPECT_FLOAT_EQ(result[0], log(2.0));
	EXPECT_FLOAT_EQ(result[1], coretools::logit(0.01));
	EXPECT_FLOAT_EQ(result[2], 0.0);

	result = mat.transformFromNelderMeadSpace(result);
	EXPECT_FLOAT_EQ(result[0], 2);
	EXPECT_FLOAT_EQ(result[1], 0.01);
	EXPECT_FLOAT_EQ(result[2], 1.0);
}

TEST(TTransitionMatrixScaledLadderAttractorShiftTest, optimize) {
	TTransitionMatrixScaledLadderAttractorShift<double, size_t> mat(7);

	EXPECT_TRUE(mat.optimizeWithNelderMead());
	EXPECT_FALSE(mat.optimizeWithLineSearch());
}
