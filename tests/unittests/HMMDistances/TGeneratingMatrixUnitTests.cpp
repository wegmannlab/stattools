//
// Created by caduffm on 11/16/21.
//

#include <stddef.h>
#include <vector>

#include "gtest/gtest.h"

#include "stattools/HMMDistances/TGeneratingMatrix.h"

using namespace testing;
using namespace stattools;

//-------------------------------------------
// TGeneratingMatrixBool
//-------------------------------------------

TEST(TGeneratingMatrixBoolTest, constructor) {
	TGeneratingMatrixBool<double, size_t> mat;

	const auto &matrix = mat.getGeneratingMatrix();
	EXPECT_EQ(matrix.n_rows, 2);
}

TEST(TGeneratingMatrixBoolTest, resize) {
	TGeneratingMatrixBool<double, size_t> mat;

	mat.resize(2);
	const auto &matrix = mat.getGeneratingMatrix();
	EXPECT_EQ(matrix.n_rows, 2);

	// throw if invalid numStates
	EXPECT_ANY_THROW(mat.resize(3));
}

TEST(TGeneratingMatrixBoolTest, fillGeneratingMatrix) {
	TGeneratingMatrixBool<double, size_t> mat;

	const std::vector<double> v{0.1, 0.7};
	mat.fillGeneratingMatrix(v);
	EXPECT_FLOAT_EQ(mat(0, 0), -0.1);
	EXPECT_FLOAT_EQ(mat(0, 1), 0.1);
	EXPECT_FLOAT_EQ(mat(1, 0), 0.7);
	EXPECT_FLOAT_EQ(mat(1, 1), -0.7);
}

TEST(TGeneratingMatrixBoolTest, stationary) {
	TGeneratingMatrixBool<double, size_t> mat;

	std::vector<double> values = {0.1, 0.7};
	auto stationary            = mat.fillStationary(values);
	EXPECT_FLOAT_EQ(stationary[0], 0.875);
	EXPECT_FLOAT_EQ(stationary[1], 0.125);
}

//-------------------------------------------
// TGeneratingMatrixLadder
//-------------------------------------------

TEST(TGeneratingMatrixLadderTest, constructor) {
	TGeneratingMatrixLadder<double, size_t> mat(5);

	const auto &matrix = mat.getGeneratingMatrix();
	EXPECT_EQ(matrix.n_rows, 5);
}

TEST(TGeneratingMatrixLadderTest, resize) {
	TGeneratingMatrixLadder<double, size_t> mat(10);

	mat.resize(5);
	const auto &matrix = mat.getGeneratingMatrix();
	EXPECT_EQ(matrix.n_rows, 5);
}

TEST(TGeneratingMatrixLadderTest, fillGeneratingMatrix_2States) {
	TGeneratingMatrixLadder<double, size_t> mat(2);

	std::vector<double> values = {0.2};
	mat.fillGeneratingMatrix(values);
	EXPECT_FLOAT_EQ(mat(0, 0), -0.2);
	EXPECT_FLOAT_EQ(mat(0, 1), 0.2);
	EXPECT_FLOAT_EQ(mat(1, 0), 0.2);
	EXPECT_FLOAT_EQ(mat(1, 1), -0.2);
}

TEST(TGeneratingMatrixLadderTest, fillGeneratingMatrix_3States) {
	TGeneratingMatrixLadder<double, size_t> mat(3);

	std::vector<double> values = {0.2};
	mat.fillGeneratingMatrix(values);
	EXPECT_FLOAT_EQ(mat(0, 0), -0.2);
	EXPECT_FLOAT_EQ(mat(0, 1), 0.2);
	EXPECT_FLOAT_EQ(mat(0, 2), 0.);
	EXPECT_FLOAT_EQ(mat(1, 0), 0.2);
	EXPECT_FLOAT_EQ(mat(1, 1), -0.4);
	EXPECT_FLOAT_EQ(mat(1, 2), 0.2);
	EXPECT_FLOAT_EQ(mat(2, 0), 0.);
	EXPECT_FLOAT_EQ(mat(2, 1), 0.2);
	EXPECT_FLOAT_EQ(mat(2, 2), -0.2);
}

TEST(TGeneratingMatrixLadderTest, fillGeneratingMatrix_5States) {
	TGeneratingMatrixLadder<double, size_t> mat(5);

	std::vector<double> values = {0.2};
	mat.fillGeneratingMatrix(values);
	EXPECT_FLOAT_EQ(mat(0, 0), -0.2);
	EXPECT_FLOAT_EQ(mat(0, 1), 0.2);
	EXPECT_FLOAT_EQ(mat(0, 2), 0.);
	EXPECT_FLOAT_EQ(mat(0, 3), 0.);
	EXPECT_FLOAT_EQ(mat(0, 4), 0.);
	EXPECT_FLOAT_EQ(mat(1, 0), 0.2);
	EXPECT_FLOAT_EQ(mat(1, 1), -0.4);
	EXPECT_FLOAT_EQ(mat(1, 2), 0.2);
	EXPECT_FLOAT_EQ(mat(1, 3), 0.);
	EXPECT_FLOAT_EQ(mat(1, 4), 0.);
	EXPECT_FLOAT_EQ(mat(2, 0), 0.);
	EXPECT_FLOAT_EQ(mat(2, 1), 0.2);
	EXPECT_FLOAT_EQ(mat(2, 2), -0.4);
	EXPECT_FLOAT_EQ(mat(2, 3), 0.2);
	EXPECT_FLOAT_EQ(mat(2, 4), 0.);
	EXPECT_FLOAT_EQ(mat(3, 0), 0.);
	EXPECT_FLOAT_EQ(mat(3, 1), 0.);
	EXPECT_FLOAT_EQ(mat(3, 2), 0.2);
	EXPECT_FLOAT_EQ(mat(3, 3), -0.4);
	EXPECT_FLOAT_EQ(mat(3, 4), 0.2);
	EXPECT_FLOAT_EQ(mat(4, 0), 0.);
	EXPECT_FLOAT_EQ(mat(4, 1), 0.);
	EXPECT_FLOAT_EQ(mat(4, 2), 0.);
	EXPECT_FLOAT_EQ(mat(4, 3), 0.2);
	EXPECT_FLOAT_EQ(mat(4, 4), -0.2);
}

TEST(TGeneratingMatrixLadderTest, fillRawLambda_3States) {
	TGeneratingMatrixLadder<double, size_t> lambda(3);

	auto mat = lambda.getUnparametrizedGeneratingMatrix();
	EXPECT_FLOAT_EQ(mat(0, 0), -1);
	EXPECT_FLOAT_EQ(mat(0, 1), 1);
	EXPECT_FLOAT_EQ(mat(0, 2), 0.);
	EXPECT_FLOAT_EQ(mat(1, 0), 1);
	EXPECT_FLOAT_EQ(mat(1, 1), -2);
	EXPECT_FLOAT_EQ(mat(1, 2), 1);
	EXPECT_FLOAT_EQ(mat(2, 0), 0.);
	EXPECT_FLOAT_EQ(mat(2, 1), 1);
	EXPECT_FLOAT_EQ(mat(2, 2), -1);
}

TEST(TGeneratingMatrixLadderTest, fillRawLambda_5States) {
	TGeneratingMatrixLadder<double, size_t> lambda(5);

	auto mat = lambda.getUnparametrizedGeneratingMatrix();
	EXPECT_FLOAT_EQ(mat(0, 0), -1);
	EXPECT_FLOAT_EQ(mat(0, 1), 1);
	EXPECT_FLOAT_EQ(mat(0, 2), 0.);
	EXPECT_FLOAT_EQ(mat(0, 3), 0.);
	EXPECT_FLOAT_EQ(mat(0, 4), 0.);
	EXPECT_FLOAT_EQ(mat(1, 0), 1);
	EXPECT_FLOAT_EQ(mat(1, 1), -2);
	EXPECT_FLOAT_EQ(mat(1, 2), 1);
	EXPECT_FLOAT_EQ(mat(1, 3), 0.);
	EXPECT_FLOAT_EQ(mat(1, 4), 0.);
	EXPECT_FLOAT_EQ(mat(2, 0), 0.);
	EXPECT_FLOAT_EQ(mat(2, 1), 1);
	EXPECT_FLOAT_EQ(mat(2, 2), -2);
	EXPECT_FLOAT_EQ(mat(2, 3), 1);
	EXPECT_FLOAT_EQ(mat(2, 4), 0.);
	EXPECT_FLOAT_EQ(mat(3, 0), 0.);
	EXPECT_FLOAT_EQ(mat(3, 1), 0.);
	EXPECT_FLOAT_EQ(mat(3, 2), 1);
	EXPECT_FLOAT_EQ(mat(3, 3), -2);
	EXPECT_FLOAT_EQ(mat(3, 4), 1);
	EXPECT_FLOAT_EQ(mat(4, 0), 0.);
	EXPECT_FLOAT_EQ(mat(4, 1), 0.);
	EXPECT_FLOAT_EQ(mat(4, 2), 0.);
	EXPECT_FLOAT_EQ(mat(4, 3), 1);
	EXPECT_FLOAT_EQ(mat(4, 4), -1);
}

TEST(TGeneratingMatrixLadderTest, fillStationary) {
	TGeneratingMatrixLadder<double, size_t> mat(3);

	std::vector<double> values = {0.2};
	mat.fillGeneratingMatrix(values);
	auto stationary            = mat.fillStationary(values);

	for (size_t i = 0; i < 3; i++) { EXPECT_FLOAT_EQ(stationary[i], 0.3333333); }
}

//-------------------------------------------
// TGeneratingMatrixScaledLadder
//-------------------------------------------

TEST(TGeneratingMatrixScaledLadderTest, constructor) {
	// 3 states
	TGeneratingMatrixScaledLadder<double, size_t> mat(3);

	const auto &matrix = mat.getGeneratingMatrix();
	EXPECT_EQ(matrix.n_rows, 3);

	// 5 states
	TGeneratingMatrixScaledLadder<double, size_t> mat2(5);

	const auto &matrix2 = mat2.getGeneratingMatrix();
	EXPECT_EQ(matrix2.n_rows, 5);

	// 7 states
	TGeneratingMatrixScaledLadder<double, size_t> mat3(7);

	const auto &matrix3 = mat3.getGeneratingMatrix();
	EXPECT_EQ(matrix3.n_rows, 7);
}

TEST(TGeneratingMatrixScaledLadderTest, resize) {
	TGeneratingMatrixScaledLadder<double, size_t> mat(3);

	mat.resize(5);
	const auto &matrix = mat.getGeneratingMatrix();
	EXPECT_EQ(matrix.n_rows, 5);

	// throw if invalid too few states
	EXPECT_ANY_THROW(mat.resize(1));
	EXPECT_ANY_THROW(mat.resize(2));
	// throw if even number of states
	EXPECT_ANY_THROW(mat.resize(4));
}

TEST(TGeneratingMatrixScaledLadderTest, fillGeneratingMatrix_3States) {
	TGeneratingMatrixScaledLadder<double, size_t> mat(3);

	std::vector<double> values = {2, 0.3, 1.0};
	mat.fillGeneratingMatrix(values);
	std::vector<double> fromR = {-2, 2, 0, 0.6, -1.2, 0.6, 0, 2, -2};

	size_t counter = 0;
	for (size_t r = 0; r < 3; r++) {
		for (size_t c = 0; c < 3; c++, counter++) { EXPECT_FLOAT_EQ(mat(r, c), fromR[counter]); }
	}
}

TEST(TGeneratingMatrixScaledLadderTest, fillGeneratingMatrix_5States) {
	TGeneratingMatrixScaledLadder<double, size_t> mat(5);

	std::vector<double> values = {2, 0.01, 0.3};
	mat.fillGeneratingMatrix(values);
	std::vector<double> fromR = {-2,    2, 0, 0, 0, 0.6,  -2.6, 2, 0, 0, 0, 0.006, -0.012,
								 0.006, 0, 0, 0, 2, -2.6, 0.6,  0, 0, 0, 2, -2};

	size_t counter = 0;
	for (size_t r = 0; r < 5; r++) {
		for (size_t c = 0; c < 5; c++, counter++) { EXPECT_FLOAT_EQ(mat(r, c), fromR[counter]); }
	}
}

TEST(TGeneratingMatrixScaledLadderTest, fillGeneratingMatrix_7States) {
	TGeneratingMatrixScaledLadder<double, size_t> mat(7);

	std::vector<double> values = {2, 0.01, 0.3};
	mat.fillGeneratingMatrix(values);
	std::vector<double> fromR = {-2, 2, 0, 0, 0, 0, 0,     0.6,    -2.6,  2, 0, 0, 0, 0, 0, 0.6,  -2.6,
								 2,  0, 0, 0, 0, 0, 0.006, -0.012, 0.006, 0, 0, 0, 0, 0, 2, -2.6, 0.6,
								 0,  0, 0, 0, 0, 2, -2.6,  0.6,    0,     0, 0, 0, 0, 2, -2};

	size_t counter = 0;
	for (size_t r = 0; r < 7; r++) {
		for (size_t c = 0; c < 7; c++, counter++) { EXPECT_FLOAT_EQ(mat(r, c), fromR[counter]); }
	}
}

TEST(TGeneratingMatrixScaledLadderTest, fillStationary_3States) {
	TGeneratingMatrixScaledLadder<double, size_t> mat(3);

	std::vector<double> values = {2, 0.3, 1.0};
	auto stationary = mat.fillStationary(values);

	std::vector<double> fromR = {0.1875, 0.625, 0.1875};
	for (size_t i = 0; i < 3; i++) { EXPECT_FLOAT_EQ(stationary[i], fromR[i]); }
}

TEST(TGeneratingMatrixScaledLadderTest, fillStationary_3States_mu1) {
	TGeneratingMatrixScaledLadder<double, size_t> mat(3);

	std::vector<double> values = {2, 1.0, 1.0};
	auto stationary = mat.fillStationary(values);

	std::vector<double> fromR = {1. / 3., 1. / 3., 1. / 3.};
	for (size_t i = 0; i < 3; i++) { EXPECT_FLOAT_EQ(stationary[i], fromR[i]); }
}

TEST(TGeneratingMatrixScaledLadderTest, fillStationary_5States) {
	TGeneratingMatrixScaledLadder<double, size_t> mat(5);

	std::vector<double> values = {2, 0.01, 0.3};
	auto stationary = mat.fillStationary(values);

	std::vector<double> fromR = {0.000893034332208742, 0.00297678110736251, 0.992260369120857, 0.00297678110736257,
								 0.000893034332208759};
	for (size_t i = 0; i < 5; i++) { EXPECT_FLOAT_EQ(stationary[i], fromR[i]); }
}

TEST(TGeneratingMatrixScaledLadderTest, fillStationary_5States_mu_1) {
	TGeneratingMatrixScaledLadder<double, size_t> mat(5);

	std::vector<double> values = {2, 0.1, 1.0};
	auto stationary = mat.fillStationary(values);

	std::vector<double> fromR = {0.0714285714285715, 0.0714285714285715, 0.714285714285714, 0.0714285714285716,
								 0.0714285714285716};
	for (size_t i = 0; i < 5; i++) { EXPECT_FLOAT_EQ(stationary[i], fromR[i]); }
}

TEST(TGeneratingMatrixScaledLadderTest, fillStationary_7States) {
	TGeneratingMatrixScaledLadder<double, size_t> mat(7);

	std::vector<double> values = {2, 0.01, 0.3};
	auto stationary = mat.fillStationary(values);

	std::vector<double> fromR = {0.000267766824682061, 0.000892556082273756, 0.0029751869409128, 0.991728980304263,
								 0.00297518694091281,  0.000892556082273763, 0.00026776682468213};
	for (size_t i = 0; i < 7; i++) { EXPECT_FLOAT_EQ(stationary[i], fromR[i]); }
}

TEST(TGeneratingMatrixScaledLadderTest, fillStationary_7States_mu1) {
	TGeneratingMatrixScaledLadder<double, size_t> mat(7);

	std::vector<double> values = {2, 0.01, 1.0};
	auto stationary = mat.fillStationary(values);

	std::vector<double> fromR = {0.00943396226415095, 0.00943396226415104, 0.00943396226415099, 0.943396226415095,
								 0.00943396226415092, 0.00943396226415084, 0.00943396226415083};
	for (size_t i = 0; i < 7; i++) { EXPECT_FLOAT_EQ(stationary[i], fromR[i]); }
}

//-------------------------------------------
// TGeneratingMatrixScaledLadderShifted
//-------------------------------------------

TEST(TGeneratingMatrixScaledLadderAttractorShiftTest, constructor) {
	// 2 states
	TGeneratingMatrixScaledLadderAttractorShift<double, size_t> mat2(2);
	const auto &matrix2 = mat2.getGeneratingMatrix();
	EXPECT_EQ(matrix2.n_rows, 2);

	// 3 states
	TGeneratingMatrixScaledLadderAttractorShift<double, size_t> mat3(3);
	const auto &matrix3 = mat3.getGeneratingMatrix();
	EXPECT_EQ(matrix3.n_rows, 3);

	// 4 states
	TGeneratingMatrixScaledLadderAttractorShift<double, size_t> mat4(4);
	const auto &matrix4 = mat4.getGeneratingMatrix();
	EXPECT_EQ(matrix4.n_rows, 4);

	// 5 states
	TGeneratingMatrixScaledLadderAttractorShift<double, size_t> mat5(5);
	const auto &matrix5 = mat5.getGeneratingMatrix();
	EXPECT_EQ(matrix5.n_rows, 5);
}

TEST(TGeneratingMatrixScaledLadderAttractorShiftTest, resize) {
	TGeneratingMatrixScaledLadderAttractorShift<double, size_t> mat(3);

	mat.resize(2);
	const auto &matrix = mat.getGeneratingMatrix();
	EXPECT_EQ(matrix.n_rows, 2);

	mat.resize(4);
	const auto &matrix4 = mat.getGeneratingMatrix();
	EXPECT_EQ(matrix4.n_rows, 4);

	mat.resize(5);
	const auto &matrix5 = mat.getGeneratingMatrix();
	EXPECT_EQ(matrix5.n_rows, 5);

	// throw if invalid too few states
	EXPECT_ANY_THROW(mat.resize(1));
}

TEST(TGeneratingMatrixScaledLadderAttractorShiftTest, fillGeneratingMatrix_2States_ix0) {
	TGeneratingMatrixScaledLadderAttractorShift<double, size_t> mat(2);

	// ix = 0
	mat.setIxAttractor(0);
	std::vector<double> values = {2, 0.01, 0.3};
	mat.fillGeneratingMatrix(values);
	std::vector<double> fromR = {-0.02, 0.02, 2, -2};

	size_t counter = 0;
	for (size_t r = 0; r < 2; r++) {
		for (size_t c = 0; c < 2; c++, counter++) { EXPECT_FLOAT_EQ(mat(r, c), fromR[counter]); }
	}
}

TEST(TGeneratingMatrixScaledLadderAttractorShiftTest, fillGeneratingMatrix_2States_ix1) {
	TGeneratingMatrixScaledLadderAttractorShift<double, size_t> mat(2);

	// ix = 1
	mat.setIxAttractor(1);
	std::vector<double> values = {2, 0.01, 0.3};
	mat.fillGeneratingMatrix(values);
	std::vector<double> fromR = {-2, 2, 0.02, -0.02};

	size_t counter = 0;
	for (size_t r = 0; r < 2; r++) {
		for (size_t c = 0; c < 2; c++, counter++) { EXPECT_FLOAT_EQ(mat(r, c), fromR[counter]); }
	}
}

TEST(TGeneratingMatrixScaledLadderAttractorShiftTest, fillGeneratingMatrix_3States_ix0) {
	TGeneratingMatrixScaledLadderAttractorShift<double, size_t> mat(3);

	mat.setIxAttractor(0);
	std::vector<double> values = {2, 0.01, 0.3};
	mat.fillGeneratingMatrix(values);
	std::vector<double> fromR = {-0.006, 0.006, 0, 2, -2.6, 0.6, 0, 2, -2};

	size_t counter = 0;
	for (size_t r = 0; r < 3; r++) {
		for (size_t c = 0; c < 3; c++, counter++) { EXPECT_FLOAT_EQ(mat(r, c), fromR[counter]); }
	}
}

TEST(TGeneratingMatrixScaledLadderAttractorShiftTest, fillGeneratingMatrix_3States_ix1) {
	TGeneratingMatrixScaledLadderAttractorShift<double, size_t> mat(3);

	mat.setIxAttractor(1);
	std::vector<double> values = {2, 0.01, 0.3};
	mat.fillGeneratingMatrix(values);
	std::vector<double> fromR = {-2, 2, 0, 0.02, -0.04, 0.02, 0, 2, -2};

	size_t counter = 0;
	for (size_t r = 0; r < 3; r++) {
		for (size_t c = 0; c < 3; c++, counter++) { EXPECT_FLOAT_EQ(mat(r, c), fromR[counter]); }
	}
}

TEST(TGeneratingMatrixScaledLadderAttractorShiftTest, fillGeneratingMatrix_3States_ix2) {
	TGeneratingMatrixScaledLadderAttractorShift<double, size_t> mat(3);

	mat.setIxAttractor(2);
	std::vector<double> values = {2, 0.01, 0.3};
	mat.fillGeneratingMatrix(values);
	std::vector<double> fromR = {-2, 2, 0, 0.6, -2.6, 2, 0, 0.006, -0.006};

	size_t counter = 0;
	for (size_t r = 0; r < 3; r++) {
		for (size_t c = 0; c < 3; c++, counter++) { EXPECT_FLOAT_EQ(mat(r, c), fromR[counter]); }
	}
}

TEST(TGeneratingMatrixScaledLadderAttractorShiftTest, fillGeneratingMatrix_5States_ix0) {
	TGeneratingMatrixScaledLadderAttractorShift<double, size_t> mat(5);

	mat.setIxAttractor(0);
	std::vector<double> values = {2, 0.01, 0.3};
	mat.fillGeneratingMatrix(values);
	std::vector<double> fromR = {-0.006, 0.006, 0, 0, 0, 2,    -2.6, 0.6, 0, 0, 0, 2, -2.6,
								 0.6,    0,     0, 0, 2, -2.6, 0.6,  0,   0, 0, 2, -2};

	size_t counter = 0;
	for (size_t r = 0; r < 5; r++) {
		for (size_t c = 0; c < 5; c++, counter++) { EXPECT_FLOAT_EQ(mat(r, c), fromR[counter]); }
	}
}

TEST(TGeneratingMatrixScaledLadderAttractorShiftTest, fillGeneratingMatrix_5States_ix1) {
	TGeneratingMatrixScaledLadderAttractorShift<double, size_t> mat(5);

	mat.setIxAttractor(1);
	std::vector<double> values = {2, 0.01, 0.3, 1};
	mat.fillGeneratingMatrix(values);
	std::vector<double> fromR = {-2,  2, 0, 0, 0, 0.006, -0.012, 0.006, 0, 0, 0, 2, -2.6,
								 0.6, 0, 0, 0, 2, -2.6,  0.6,    0,     0, 0, 2, -2};

	size_t counter = 0;
	for (size_t r = 0; r < 5; r++) {
		for (size_t c = 0; c < 5; c++, counter++) { EXPECT_FLOAT_EQ(mat(r, c), fromR[counter]); }
	}
}

TEST(TGeneratingMatrixScaledLadderAttractorShiftTest, fillGeneratingMatrix_5States_ix2) {
	TGeneratingMatrixScaledLadderAttractorShift<double, size_t> mat(5);

	mat.setIxAttractor(2);
	std::vector<double> values = {2, 0.01, 0.3, 2};
	mat.fillGeneratingMatrix(values);
	std::vector<double> fromR = {-2,    2, 0, 0, 0, 0.6,  -2.6, 2, 0, 0, 0, 0.006, -0.012,
								 0.006, 0, 0, 0, 2, -2.6, 0.6,  0, 0, 0, 2, -2};

	size_t counter = 0;
	for (size_t r = 0; r < 5; r++) {
		for (size_t c = 0; c < 5; c++, counter++) { EXPECT_FLOAT_EQ(mat(r, c), fromR[counter]); }
	}
}

TEST(TGeneratingMatrixScaledLadderAttractorShiftTest, fillGeneratingMatrix_5States_ix3) {
	TGeneratingMatrixScaledLadderAttractorShift<double, size_t> mat(5);

	mat.setIxAttractor(3);
	std::vector<double> values = {2, 0.01, 0.3, 3};
	mat.fillGeneratingMatrix(values);
	std::vector<double> fromR = {-2, 2, 0, 0, 0,     0.6,    -2.6,  2, 0, 0, 0, 0.6, -2.6,
								 2,  0, 0, 0, 0.006, -0.012, 0.006, 0, 0, 0, 2, -2};

	size_t counter = 0;
	for (size_t r = 0; r < 5; r++) {
		for (size_t c = 0; c < 5; c++, counter++) { EXPECT_FLOAT_EQ(mat(r, c), fromR[counter]); }
	}
}

TEST(TGeneratingMatrixScaledLadderAttractorShiftTest, fillGeneratingMatrix_5States_ix4) {
	TGeneratingMatrixScaledLadderAttractorShift<double, size_t> mat(5);

	mat.setIxAttractor(4);
	std::vector<double> values = {2, 0.01, 0.3};
	mat.fillGeneratingMatrix(values);
	std::vector<double> fromR = {-2, 2, 0, 0, 0,   0.6,  -2.6, 2, 0, 0, 0,     0.6,   -2.6,
								 2,  0, 0, 0, 0.6, -2.6, 2,    0, 0, 0, 0.006, -0.006};

	size_t counter = 0;
	for (size_t r = 0; r < 5; r++) {
		for (size_t c = 0; c < 5; c++, counter++) { EXPECT_FLOAT_EQ(mat(r, c), fromR[counter]); }
	}
}

//-------------------------------------------
// TGeneratingMatrixScaledLadderShifted2
//-------------------------------------------

TEST(TGeneratingMatrixScaledLadderAttractorShift2Test, constructor) {
	// 2 states
	TGeneratingMatrixScaledLadderAttractorShift2<double, size_t> mat2(2);
	const auto &matrix2 = mat2.getGeneratingMatrix();
	EXPECT_EQ(matrix2.n_rows, 2);

	// 3 states
	TGeneratingMatrixScaledLadderAttractorShift2<double, size_t> mat3(3);
	const auto &matrix3 = mat3.getGeneratingMatrix();
	EXPECT_EQ(matrix3.n_rows, 3);

	// 4 states
	TGeneratingMatrixScaledLadderAttractorShift2<double, size_t> mat4(4);
	const auto &matrix4 = mat4.getGeneratingMatrix();
	EXPECT_EQ(matrix4.n_rows, 4);

	// 5 states
	TGeneratingMatrixScaledLadderAttractorShift2<double, size_t> mat5(5);
	const auto &matrix5 = mat5.getGeneratingMatrix();
	EXPECT_EQ(matrix5.n_rows, 5);
}

TEST(TGeneratingMatrixScaledLadderAttractorShift2Test, resize) {
	TGeneratingMatrixScaledLadderAttractorShift2<double, size_t> mat(3);

	mat.resize(2);
	const auto &matrix = mat.getGeneratingMatrix();
	EXPECT_EQ(matrix.n_rows, 2);

	mat.resize(4);
	const auto &matrix4 = mat.getGeneratingMatrix();
	EXPECT_EQ(matrix4.n_rows, 4);

	mat.resize(5);
	const auto &matrix5 = mat.getGeneratingMatrix();
	EXPECT_EQ(matrix5.n_rows, 5);

	// throw if invalid too few states
	EXPECT_ANY_THROW(mat.resize(1));
}

TEST(TGeneratingMatrixScaledLadderAttractorShift2Test, fillGeneratingMatrix_2States_ix0) {
	TGeneratingMatrixScaledLadderAttractorShift2<double, size_t> mat(2);

	// ix = 0
	mat.setIxAttractor(0);
	std::vector<double> values = {2, 0.01, 0.3};
	mat.fillGeneratingMatrix(values);
	std::vector<double> fromR = {-0.02, 0.02, 2, -2};

	size_t counter = 0;
	for (size_t r = 0; r < 2; r++) {
		for (size_t c = 0; c < 2; c++, counter++) { EXPECT_FLOAT_EQ(mat(r, c), fromR[counter]); }
	}
}

TEST(TGeneratingMatrixScaledLadderAttractorShift2Test, fillGeneratingMatrix_2States_ix1) {
	TGeneratingMatrixScaledLadderAttractorShift2<double, size_t> mat(2);

	// ix = 1
	mat.setIxAttractor(1);
	std::vector<double> values = {2, 0.01, 0.3};
	mat.fillGeneratingMatrix(values);
	std::vector<double> fromR = {-2, 2, 0.02, -0.02};

	size_t counter = 0;
	for (size_t r = 0; r < 2; r++) {
		for (size_t c = 0; c < 2; c++, counter++) { EXPECT_FLOAT_EQ(mat(r, c), fromR[counter]); }
	}
}

TEST(TGeneratingMatrixScaledLadderAttractorShift2Test, fillGeneratingMatrix_3States_ix0) {
	TGeneratingMatrixScaledLadderAttractorShift2<double, size_t> mat(3);

	mat.setIxAttractor(0);
	std::vector<double> values = {2, 0.01, 0.3};
	mat.fillGeneratingMatrix(values);
	std::vector<double> fromR = {-0.02, 0.02, 0, 2.6, -4, 1.4, 0, 2, -2};

	size_t counter = 0;
	for (size_t r = 0; r < 3; r++) {
		for (size_t c = 0; c < 3; c++, counter++) { EXPECT_FLOAT_EQ(mat(r, c), fromR[counter]); }
	}
}

TEST(TGeneratingMatrixScaledLadderAttractorShift2Test, fillGeneratingMatrix_3States_ix1) {
	TGeneratingMatrixScaledLadderAttractorShift2<double, size_t> mat(3);

	mat.setIxAttractor(1);
	std::vector<double> values = {2, 0.01, 0.3};
	mat.fillGeneratingMatrix(values);
	std::vector<double> fromR = {-2, 2, 0, 0.02, -0.04, 0.02, 0, 2, -2};

	size_t counter = 0;
	for (size_t r = 0; r < 3; r++) {
		for (size_t c = 0; c < 3; c++, counter++) { EXPECT_FLOAT_EQ(mat(r, c), fromR[counter]); }
	}
}

TEST(TGeneratingMatrixScaledLadderAttractorShift2Test, fillGeneratingMatrix_3States_ix2) {
	TGeneratingMatrixScaledLadderAttractorShift2<double, size_t> mat(3);

	mat.setIxAttractor(2);
	std::vector<double> values = {2, 0.01, 0.3};
	mat.fillGeneratingMatrix(values);
	std::vector<double> fromR = {-2, 2, 0, 1.4, -4, 2.6, 0, 0.02, -0.02};

	size_t counter = 0;
	for (size_t r = 0; r < 3; r++) {
		for (size_t c = 0; c < 3; c++, counter++) { EXPECT_FLOAT_EQ(mat(r, c), fromR[counter]); }
	}
}

TEST(TGeneratingMatrixScaledLadderAttractorShift2Test, fillGeneratingMatrix_5States_ix0) {
	TGeneratingMatrixScaledLadderAttractorShift2<double, size_t> mat(5);

	mat.setIxAttractor(0);
	std::vector<double> values = {2, 0.01, 0.3};
	mat.fillGeneratingMatrix(values);
	std::vector<double> fromR = {-0.02, 0.02, 0, 0, 0,   2.6, -4,  1.4, 0, 0, 0, 2.6, -4,
								 1.4,   0,    0, 0, 2.6, -4,  1.4, 0,   0, 0, 2, -2};

	size_t counter = 0;
	for (size_t r = 0; r < 5; r++) {
		for (size_t c = 0; c < 5; c++, counter++) { EXPECT_FLOAT_EQ(mat(r, c), fromR[counter]); }
	}
}

TEST(TGeneratingMatrixScaledLadderAttractorShift2Test, fillGeneratingMatrix_5States_ix1) {
	TGeneratingMatrixScaledLadderAttractorShift2<double, size_t> mat(5);

	mat.setIxAttractor(1);
	std::vector<double> values = {2, 0.01, 0.3, 1};
	mat.fillGeneratingMatrix(values);
	std::vector<double> fromR = {-2,  2, 0, 0, 0,   0.02, -0.04, 0.02, 0, 0, 0, 2.6, -4,
								 1.4, 0, 0, 0, 2.6, -4,   1.4,   0,    0, 0, 2, -2};

	size_t counter = 0;
	for (size_t r = 0; r < 5; r++) {
		for (size_t c = 0; c < 5; c++, counter++) { EXPECT_FLOAT_EQ(mat(r, c), fromR[counter]); }
	}
}

TEST(TGeneratingMatrixScaledLadderAttractorShift2Test, fillGeneratingMatrix_5States_ix2) {
	TGeneratingMatrixScaledLadderAttractorShift2<double, size_t> mat(5);

	mat.setIxAttractor(2);
	std::vector<double> values = {2, 0.01, 0.3, 2};
	mat.fillGeneratingMatrix(values);
	std::vector<double> fromR = {-2,   2, 0, 0, 0,   1.4, -4,  2.6, 0, 0, 0, 0.02, -0.04,
								 0.02, 0, 0, 0, 2.6, -4,  1.4, 0,   0, 0, 2, -2};

	size_t counter = 0;
	for (size_t r = 0; r < 5; r++) {
		for (size_t c = 0; c < 5; c++, counter++) { EXPECT_FLOAT_EQ(mat(r, c), fromR[counter]); }
	}
}

TEST(TGeneratingMatrixScaledLadderAttractorShift2Test, fillGeneratingMatrix_5States_ix3) {
	TGeneratingMatrixScaledLadderAttractorShift2<double, size_t> mat(5);

	mat.setIxAttractor(3);
	std::vector<double> values = {2, 0.01, 0.3, 3};
	mat.fillGeneratingMatrix(values);
	std::vector<double> fromR = {-2,  2, 0, 0, 0,    1.4,   -4,   2.6, 0, 0, 0, 1.4, -4,
								 2.6, 0, 0, 0, 0.02, -0.04, 0.02, 0,   0, 0, 2, -2};

	size_t counter = 0;
	for (size_t r = 0; r < 5; r++) {
		for (size_t c = 0; c < 5; c++, counter++) { EXPECT_FLOAT_EQ(mat(r, c), fromR[counter]); }
	}
}

TEST(TGeneratingMatrixScaledLadderAttractorShift2Test, fillGeneratingMatrix_5States_ix4) {
	TGeneratingMatrixScaledLadderAttractorShift2<double, size_t> mat(5);

	mat.setIxAttractor(4);
	std::vector<double> values = {2, 0.01, 0.3};
	mat.fillGeneratingMatrix(values);
	std::vector<double> fromR = {-2,  2, 0, 0, 0,   1.4, -4,  2.6, 0, 0, 0,    1.4,  -4,
								 2.6, 0, 0, 0, 1.4, -4,  2.6, 0,   0, 0, 0.02, -0.02};

	size_t counter = 0;
	for (size_t r = 0; r < 5; r++) {
		for (size_t c = 0; c < 5; c++, counter++) { EXPECT_FLOAT_EQ(mat(r, c), fromR[counter]); }
	}
}
