//
// Created by madleina on 07.04.21.
//

#include <armadillo>
#include <cstdint>
#include <memory>
#include <stddef.h>
#include <vector>

#include "gtest/gtest.h"

#include "genometools/GenomePositions/TDistances.h"
#include "stattools/HMMDistances/TOptimizeTransMatLineSearch.h"
#include "stattools/HMMDistances/TOptimizeTransMatNelderMead.h"
#include "stattools/HMMDistances/TTransitionMatrixDist.h"

using namespace testing;
using namespace stattools;

//-------------------------------------------
// TTransitionMatrixHMMDistancesNelderMead
//-------------------------------------------

using TauBool = TOptimizeTransMatNelderMead<double, size_t, size_t, TTransitionMatrixBool<double, size_t>>;
using TauBoolGen =
	TOptimizeTransMatNelderMead<double, size_t, size_t, TTransitionMatrixBoolGeneratingMatrix<double, size_t>>;
using TauBoolLadder = TOptimizeTransMatLineSearch<double, size_t, size_t, TTransitionMatrixLadder<double, size_t>>;
using TransMat      = TTransitionMatrixDistances<double, size_t, size_t>;

TEST(TTransitionMatrixHMMDistancesNelderMeadTest, initializeWithDistances) {
	uint16_t numStates = 2;
	genometools::TDistancesBinned<uint8_t> distances(100);
	distances.add(0, "chunk_1"); // pro forma, to avoid error that distances are not filled

	TransMat transMat(&distances, std::make_shared<TauBool>(numStates));

	EXPECT_EQ(transMat.numStates(), numStates);
	EXPECT_EQ(transMat.numDistanceGroups(), 8);

	for (size_t i = 0; i < transMat.numDistanceGroups(); i++) {
		auto mat = transMat[0];
		EXPECT_EQ(mat.n_rows, numStates);

		EXPECT_EQ(mat(0, 0), 0.);
		EXPECT_EQ(mat(0, 1), 0.);
		EXPECT_EQ(mat(1, 0), 0.);
		EXPECT_EQ(mat(1, 1), 0.);
	}
}

TEST(TTransitionMatrixHMMDistancesNelderMeadTest, operator_square_brackets) {
	uint16_t numStates = 2;
	genometools::TDistancesBinned<uint8_t> distances(100);
	distances.add(0, "chunk_1"); // pro forma, to avoid error that distances are not filled

	// non-const
	TransMat base(&distances, std::make_shared<TauBool>(numStates));

	// this is ok
	EXPECT_EQ(base[0].n_rows, numStates);
	// this is outside range
#ifdef _DEBUG
	EXPECT_DEATH(base[8], "");
#endif
	// const
	TransMat base2(&distances, std::make_shared<TauBool>(numStates));
	// this is ok
	EXPECT_EQ(base2[0].n_rows, numStates);
// this is outside range
#ifdef _DEBUG
	EXPECT_DEATH(base2[8], "");
#endif
}

TEST(TTransitionMatrixHMMDistancesNelderMeadTest, _f_TTransitionMatrixBool) {
	genometools::TDistancesBinned<uint8_t> distances(16);
	distances.add(0, "chunk_1"); // pro forma, to avoid error that distances are not filled

	TransMat transMat(&distances, std::make_shared<TauBool>(2));

	// fill transition probabilities
	double pi    = 0.01;
	double gamma = 0.8;
	std::vector<double> vals = {pi, gamma};
	transMat.fillProbabilities(vals);

	double absError = 0.00001;
	// [1]
	EXPECT_NEAR(transMat[1](0, 0), 0.9944444, absError);
	EXPECT_NEAR(transMat[1](0, 1), 0.005555556, absError);
	EXPECT_NEAR(transMat[1](1, 0), 0.5500000, absError);
	EXPECT_NEAR(transMat[1](1, 1), 0.450000000, absError);

	// [2]
	EXPECT_NEAR(transMat[2](0, 0), 0.9919753, absError);
	EXPECT_NEAR(transMat[2](0, 1), 0.008024691, absError);
	EXPECT_NEAR(transMat[2](1, 0), 0.7944444, absError);
	EXPECT_NEAR(transMat[2](1, 1), 0.205555556, absError);

	// [3]
	EXPECT_NEAR(transMat[3](0, 0), 0.9903902, absError);
	EXPECT_NEAR(transMat[3](0, 1), 0.009609816, absError);
	EXPECT_NEAR(transMat[3](1, 0), 0.9513717, absError);
	EXPECT_NEAR(transMat[3](1, 1), 0.048628258, absError);

	// [4]
	EXPECT_NEAR(transMat[4](0, 0), 0.9900152, absError);
	EXPECT_NEAR(transMat[4](0, 1), 0.009984776, absError);
	EXPECT_NEAR(transMat[4](1, 0), 0.9884928, absError);
	EXPECT_NEAR(transMat[4](1, 1), 0.011507214, absError);
}

TEST(TTransitionMatrixHMMDistancesNelderMeadTest, _fillProbabilities_TTransitionMatrixBoolGeneratingMatrix) {
	genometools::TDistancesBinned<uint8_t> distances(16);
	distances.add(0, "chunk_1"); // pro forma, to avoid error that distances are not filled

	TransMat transMat(&distances, std::make_shared<TauBoolGen>(2));

	// fill transition probabilities
	double lambda1 = 0.1;
	double lambda2 = 0.2;
	std::vector<double> vals = {lambda1, lambda2};
	transMat.fillProbabilities(vals);

	// [1]
	EXPECT_NEAR(transMat[1](0, 0), 0.9136061, 0.01);
	EXPECT_NEAR(transMat[1](0, 1), 0.08639393, 0.01);
	EXPECT_NEAR(transMat[1](1, 0), 0.1727879, 0.01);
	EXPECT_NEAR(transMat[1](1, 1), 0.82721215, 0.01);

	// [2]
	EXPECT_NEAR(transMat[2](0, 0), 0.8496039, 0.01);
	EXPECT_NEAR(transMat[2](0, 1), 0.1503961, 0.01);
	EXPECT_NEAR(transMat[2](1, 0), 0.3007922, 0.01);
	EXPECT_NEAR(transMat[2](1, 1), 0.6992078, 0.01);

	// [3]
	EXPECT_NEAR(transMat[3](0, 0), 0.7670647, 0.01);
	EXPECT_NEAR(transMat[3](0, 1), 0.2329353, 0.01);
	EXPECT_NEAR(transMat[3](1, 0), 0.4658705, 0.01);
	EXPECT_NEAR(transMat[3](1, 1), 0.5341295, 0.01);

	// [4]
	EXPECT_NEAR(transMat[4](0, 0), 0.696906, 0.01);
	EXPECT_NEAR(transMat[4](0, 1), 0.303094, 0.01);
	EXPECT_NEAR(transMat[4](1, 0), 0.606188, 0.01);
	EXPECT_NEAR(transMat[4](1, 1), 0.393812, 0.01);
}

TEST(TTransitionMatrixHMMDistancesNelderMeadTest, _fillProbabilities_LadderGeneratingMatrix) {
	uint16_t numStates = 3;
	genometools::TDistancesBinned<uint8_t> distances(16);
	distances.add(0, "chunk_1"); // pro forma, to avoid error that distances are not filled

	TransMat transMat(&distances, std::make_shared<TauBoolLadder>(numStates));

	// fill transition probabilities
	double kappa = 0.2;
	std::vector<double> vals = {kappa};
	transMat.fillProbabilities(vals);

	// [1]
	EXPECT_NEAR(transMat[1](0, 0), 0.83416732, 0.01);
	EXPECT_NEAR(transMat[1](0, 1), 0.1503961, 0.01);
	EXPECT_NEAR(transMat[1](0, 2), 0.01543656, 0.01);
	EXPECT_NEAR(transMat[1](1, 0), 0.15039612, 0.01);
	EXPECT_NEAR(transMat[1](1, 1), 0.6992078, 0.01);
	EXPECT_NEAR(transMat[1](1, 2), 0.15039612, 0.01);
	EXPECT_NEAR(transMat[1](2, 0), 0.01543656, 0.01);
	EXPECT_NEAR(transMat[1](2, 1), 0.1503961, 0.01);
	EXPECT_NEAR(transMat[1](2, 2), 0.83416732, 0.01);

	// [2]
	EXPECT_NEAR(transMat[2](0, 0), 0.71869239, 0.01);
	EXPECT_NEAR(transMat[2](0, 1), 0.2329353, 0.01);
	EXPECT_NEAR(transMat[2](0, 2), 0.04837235, 0.01);
	EXPECT_NEAR(transMat[2](1, 0), 0.23293526, 0.01);
	EXPECT_NEAR(transMat[2](1, 1), 0.5341295, 0.01);
	EXPECT_NEAR(transMat[2](1, 2), 0.23293526, 0.01);
	EXPECT_NEAR(transMat[2](2, 0), 0.04837235, 0.01);
	EXPECT_NEAR(transMat[2](2, 1), 0.2329353, 0.01);
	EXPECT_NEAR(transMat[2](2, 2), 0.71869239, 0.01);

	// [3]
	EXPECT_NEAR(transMat[3](0, 0), 0.5731175, 0.01);
	EXPECT_NEAR(transMat[3](0, 1), 0.303094, 0.01);
	EXPECT_NEAR(transMat[3](0, 2), 0.1237885, 0.01);
	EXPECT_NEAR(transMat[3](1, 0), 0.3030940, 0.01);
	EXPECT_NEAR(transMat[3](1, 1), 0.393812, 0.01);
	EXPECT_NEAR(transMat[3](1, 2), 0.3030940, 0.01);
	EXPECT_NEAR(transMat[3](2, 0), 0.1237885, 0.01);
	EXPECT_NEAR(transMat[3](2, 1), 0.303094, 0.01);
	EXPECT_NEAR(transMat[3](2, 2), 0.5731175, 0.01);

	// [4]
	EXPECT_NEAR(transMat[4](0, 0), 0.4356532, 0.01);
	EXPECT_NEAR(transMat[4](0, 1), 0.3305901, 0.01);
	EXPECT_NEAR(transMat[4](0, 2), 0.2337567, 0.01);
	EXPECT_NEAR(transMat[4](1, 0), 0.3305901, 0.01);
	EXPECT_NEAR(transMat[4](1, 1), 0.3388198, 0.01);
	EXPECT_NEAR(transMat[4](1, 2), 0.3305901, 0.01);
	EXPECT_NEAR(transMat[4](2, 0), 0.2337567, 0.01);
	EXPECT_NEAR(transMat[4](2, 1), 0.3305901, 0.01);
	EXPECT_NEAR(transMat[4](2, 2), 0.4356532, 0.01);
}

TEST(TTransitionMatrixHMMDistancesNelderMeadTest, _fillStationaryDistribution_TTransitionMatrixBool) {
	genometools::TDistancesBinned<uint8_t> distances(16);
	distances.add(0, "chunk_1"); // pro forma, to avoid error that distances are not filled

	TransMat transMat(&distances, std::make_shared<TauBool>(2));

	// fill transition probabilities
	double pi    = 0.01;
	double gamma = 0.8;
	std::vector<double> vals = {pi, gamma};
	transMat.fillProbabilities(vals);

	// [0]
	EXPECT_FLOAT_EQ(transMat[0](0, 0), 0.99);
	EXPECT_FLOAT_EQ(transMat[0](0, 1), 0.01);
	EXPECT_FLOAT_EQ(transMat[0](1, 0), 0.99);
	EXPECT_FLOAT_EQ(transMat[0](1, 1), 0.01);

	EXPECT_FLOAT_EQ(transMat.stationary(0), 0.99);
	EXPECT_FLOAT_EQ(transMat.stationary(1), 0.01);
}

TEST(TTransitionMatrixHMMDistancesNelderMeadTest, _fillStationaryDistribution_TTransitionMatrixBoolGeneratingMatrix) {
	genometools::TDistancesBinned<uint8_t> distances(16);
	distances.add(0, "chunk_1"); // pro forma, to avoid error that distances are not filled

	TransMat transMat(&distances, std::make_shared<TauBoolGen>(2));

	// fill generating matrix
	double lambda1 = 0.1;
	double lambda2 = 0.2;
	std::vector<double> vals = {lambda1, lambda2};
	transMat.fillProbabilities(vals);

	// [0]
	EXPECT_NEAR(transMat[0](0, 0), 0.66666, 0.001);
	EXPECT_NEAR(transMat[0](0, 1), 0.33333, 0.001);
	EXPECT_NEAR(transMat[0](1, 0), 0.66666, 0.001);
	EXPECT_NEAR(transMat[0](1, 1), 0.33333, 0.001);

	EXPECT_NEAR(transMat.stationary(0), 0.66666, 0.001);
	EXPECT_NEAR(transMat.stationary(1), 0.33333, 0.001);
}

TEST(TTransitionMatrixHMMDistancesNelderMeadTest, _fillStationaryDistribution_LadderGeneratingMatrix) {
	uint16_t numStates = 3;
	genometools::TDistancesBinned<uint8_t> distances(16);
	distances.add(0, "chunk_1"); // pro forma, to avoid error that distances are not filled

	TransMat transMat(&distances, std::make_shared<TauBoolLadder>(numStates));

	// fill transition probabilities
	double kappa = 0.2;
	std::vector<double> vals = {kappa};
	transMat.fillProbabilities(vals);

	// [0]
	EXPECT_NEAR(transMat[0](0, 0), 0.33333, 0.001);
	EXPECT_NEAR(transMat[0](0, 1), 0.33333, 0.001);
	EXPECT_NEAR(transMat[0](0, 2), 0.33333, 0.001);
	EXPECT_NEAR(transMat[0](1, 0), 0.33333, 0.001);
	EXPECT_NEAR(transMat[0](1, 1), 0.33333, 0.001);
	EXPECT_NEAR(transMat[0](1, 2), 0.33333, 0.001);
	EXPECT_NEAR(transMat[0](2, 0), 0.33333, 0.001);
	EXPECT_NEAR(transMat[0](2, 1), 0.33333, 0.001);
	EXPECT_NEAR(transMat[0](2, 2), 0.33333, 0.001);

	EXPECT_NEAR(transMat.stationary(0), 0.33333, 0.001);
	EXPECT_NEAR(transMat.stationary(1), 0.33333, 0.001);
	EXPECT_NEAR(transMat.stationary(2), 0.33333, 0.001);
}
