//
// Created by madleina on 22.12.21.
//

/*
#include "stattools/commonTestCases/commonTestingFunctions.h"
#include "stattools/HMMDistances/TCombinedStatesTransitionMatrices.h"
#include "stattools/Priors/TPriorHMMCombinedScaledLadders.h"
#include "stattools/Priors/TPriorNormalMixedModel.h"
#include "stattools/Priors/TPriorUniform.h"
#include "gtest/gtest.h"

using namespace testing;
using namespace stattools;
using namespace prior;
*/

//-------------------------------------------
// Test:
// TCombinedStatesTransitionMatrices
// together with univariate normal mixed model
//-------------------------------------------

/*
class TCombinedStatesTransitionMatricesTest : public Test {
protected:
    constexpr static size_t N                       = 10000;
    constexpr static size_t numMixture              = 15;
    constexpr static size_t numChains               = 2;
    std::array<size_t, numChains> numStatesPerChain = {3, 5};

    using TypeZ = coretools::UnsignedInt8WithMax<0>;

    // mixed model
    using TypeMixedModel = TNormalMixedModelCreator<TypeZ, N, numMixture>;
    TypeMixedModel mixedModel;

    // HMM
    THMMCombinedScaledLadderCreator<numChains, N, true, TypeMixedModel::BoxType> hmm;

    // 15 normal mixture components
    std::vector<double> simulatedValuesMeans = {-7, -6, -5, -4, -3, -2, -1, 0, 1, 2, 3, 4, 5, 6, 7};
    std::vector<double> simulatedValuesVars  = {
        0.001, 0.001, 0.001, 0.001, 0.001, 0.001, 0.001, 0.001,
        0.001, 0.001, 0.001, 0.001, 0.001, 0.001, 0.001}; // all very small, to make sure that components are
                                                           // distinguishable
    std::vector<double> simulatedKappas = {2, 0.3};
    std::vector<double> simulatedMus    = {0.01, 0.99};
    std::vector<double> simulatedNu     = {0.0, 0.8};

    void initialize(bool FixMixedModelParams, bool FixTransMat) {
        hmm.initialize("HMMDistances/testFiles/positions_TCombinedStates.txt", numStatesPerChain, FixTransMat,
                       simulatedKappas, simulatedMus, simulatedNu);
        mixedModel.initialize(hmm.z.get(), FixMixedModelParams, "HMMDistances/testFiles/data_TCombinedStates.txt",
                              simulatedValuesMeans, simulatedValuesVars);
    }
};

TEST_F(TCombinedStatesTransitionMatricesTest, fixMixedModelParams) {
    initialize(true, false);
    mixedModel.obs->guessInitialValues();

    // check if parameters of mixed model remained unchanged
    for (size_t i = 0; i < numMixture; i++) {
        EXPECT_EQ(mixedModel.mus[i]->value(), simulatedValuesMeans[i]);
        EXPECT_EQ(mixedModel.vars[i]->value(), simulatedValuesVars[i]);
    }

    // transition matrix parameters
    // Note: There is noise in the simulation of z -> simulated parameters do not necessarily correspond to MLE
    // parameters for simulated data
    double absError =
        0.5; // a large change in kappa has quite a small change in the transition probabilities -> error can be large
    EXPECT_NEAR(hmm.prior->kappasMusNus()[0], simulatedKappas[0], absError);
    EXPECT_NEAR(hmm.prior->kappasMusNus()[2], simulatedKappas[1], absError);

    absError = 0.015;
    EXPECT_NEAR(hmm.prior->kappasMusNus()[1], simulatedMus[0], absError);
    EXPECT_NEAR(hmm.prior->kappasMusNus()[3], simulatedMus[1], absError);

    absError = 0.25;
    EXPECT_NEAR(hmm.prior->kappasMusNus()[4], simulatedNu[1], absError);

    // check if posterior estimates of latent variable are close to truth
    double fracDifferent = hmm.calcDiffZ("HMMDistances/testFiles/trueZ_TCombinedStates.txt", hmm.z->storage());
    EXPECT_TRUE(fracDifferent < 0.01);
}

TEST_F(TCombinedStatesTransitionMatricesTest, fixTransMatParams) {
    initialize(false, true);
    mixedModel.obs->guessInitialValues();

    // check if parameters of mixed model are close to truth
    // variances: same for all components
    double absError = 0.1;
    for (size_t i = 0; i < numMixture; i++) {
        EXPECT_NEAR(mixedModel.vars[i]->value(), simulatedValuesVars[i], absError);
    }
    // means: different per component
    // Note: components might switch! -> means[i] does not necessarily correspond to simulatedValuesMeans[i], but
    //       it should correspond to some simulatedValuesMeans (with any index)
    // -> check if all means are detected exactly once
    std::vector<size_t> newComponents(numMixture, 0);
    for (size_t i = 0; i < numMixture; i++) {
        double inferredMean = mixedModel.mus[i]->value();
        size_t counts_close = 0;
        for (size_t j = 0; j < numMixture; j++) {
            if (std::fabs(inferredMean - simulatedValuesMeans[j]) < absError) {
                counts_close++;
                newComponents[j] = i;
            }
        }
        EXPECT_EQ(counts_close, 1);
    }

    // transition matrix parameters
    EXPECT_FLOAT_EQ(hmm.prior->kappasMusNus()[0], simulatedKappas[0]);
    EXPECT_FLOAT_EQ(hmm.prior->kappasMusNus()[1], simulatedMus[0]);
    EXPECT_FLOAT_EQ(hmm.prior->kappasMusNus()[2], simulatedKappas[1]);
    EXPECT_FLOAT_EQ(hmm.prior->kappasMusNus()[3], simulatedMus[1]);
    EXPECT_FLOAT_EQ(hmm.prior->kappasMusNus()[4], simulatedNu[1]);

    double fracDifferent =
        hmm.calcDiffZAccountForSwitching("HMMDistances/testFiles/trueZ_TCombinedStates.txt", newComponents);
    EXPECT_TRUE(fracDifferent < 0.01);
}

TEST_F(TCombinedStatesTransitionMatricesTest, estimateBoth) {
    initialize(false, false);
    mixedModel.obs->guessInitialValues();

    // check if parameters of mixed model are close to truth
    // variances: same for all components
    double absError = 0.0015;
    for (size_t i = 0; i < numMixture; i++) {
        EXPECT_NEAR(mixedModel.vars[i]->value(), simulatedValuesVars[i], absError);
    }
    // means: different per component
    // Note: components might switch! -> means[i] does not necessarily correspond to simulatedValuesMeans[i], but
    //       it should correspond to some simulatedValuesMeans (with any index)
    // -> check if all means are detected exactly once
    std::vector<size_t> newComponents(numMixture, 0);

    absError = 0.1;
    for (size_t i = 0; i < numMixture; i++) {
        double inferredMean = mixedModel.mus[i]->value();
        size_t counts_close = 0;
        for (size_t j = 0; j < numMixture; j++) {
            if (std::fabs(inferredMean - simulatedValuesMeans[j]) < absError) {
                counts_close++;
                newComponents[j] = i;
            }
        }
        EXPECT_EQ(counts_close, 1);
    }

    double fracDifferent =
        hmm.calcDiffZAccountForSwitching("HMMDistances/testFiles/trueZ_TCombinedStates.txt", newComponents);
    EXPECT_TRUE(fracDifferent < 0.01);
}
*/
