//
// Created by caduffm on 6/16/22.
//

#include <stdint.h> // for uint16_t

#include "gtest/gtest.h"

#include "coretools/Files/TOutputFile.h"
#include "stattools/MCMC/TMCMCFiles.h"

using namespace testing;
using namespace stattools;

class TStatePosteriorsReaderBridge : public TStatePosteriorsReader {
public:
	TStatePosteriorsReaderBridge(std::string_view Filename) : TStatePosteriorsReader(Filename){};
	void _read(std::string_view ParamName) override { TStatePosteriorsReader::_read(ParamName); };
	auto values() const { return _values; }
	auto names() const { return _names; }
};

TEST(TMCMCPosteriorCountsFile_Reader_Test, oneLine) {
	// write file with just one line
	coretools::TOutputFile out("counts.txt", {"name", "state"});
	std::vector<std::string_view> names = {"p1", "p2", "p3", "p4", "p5", "p6", "p7", "p8", "p9", "p10"};
	std::vector<double> values     = {0.67, 0.49, 0.74, 0.81, 0.64, 0.83, 0.62, 0.37, 0.89, 0.62};
	for (size_t i = 0; i < values.size(); ++i) { out << names[i] << values[i] << coretools::endl; }
	out.close();

	TStatePosteriorsReaderBridge reader("counts.txt");
	reader._read("param");

	EXPECT_EQ(reader.values().size(), values.size());
	EXPECT_EQ(reader.names().size(), names.size());
	for (size_t i = 0; i < values.size(); i++) {
		EXPECT_EQ(reader.values()[i], "0");
		EXPECT_EQ(reader.names()[i], names[i]);
	}
}

TEST(TMCMCPosteriorCountsFile_Reader_Test, twoLines) {
	// write file with just one line
	coretools::TOutputFile out("counts.txt", {"name", "state0", "state1"});
	std::vector<std::string_view> names = {"p1", "p2", "p3", "p4", "p5", "p6", "p7", "p8", "p9", "p10"};
	std::vector<double> values_1   = {0.67, 0.49, 0.74, 0.81, 0.64, 0.83, 0.62, 0.37, 0.89, 0.62};
	std::vector<double> values_2   = {0.95, 0.34, 0.64, 0.14, 0.44, 0.22, 0.9, 0.36, 0.08, 0.11};
	std::vector<size_t> maxRow     = {1, 0, 0, 0, 0, 0, 1, 0, 0, 0};
	for (size_t i = 0; i < values_1.size(); ++i) { out << names[i] << values_1[i] << values_2[i] << coretools::endl; }
	out.close();

	TStatePosteriorsReaderBridge reader("counts.txt");
	reader._read("param");

	EXPECT_EQ(reader.values().size(), values_1.size());
	EXPECT_EQ(reader.names().size(), names.size());
	for (size_t i = 0; i < values_1.size(); i++) {
		EXPECT_EQ(reader.values()[i], coretools::str::toString(maxRow[i]));
		EXPECT_EQ(reader.names()[i], names[i]);
	}
}

TEST(TMCMCPosteriorCountsFile_Reader_Test, threeLines) {
	// write file with just one line
	coretools::TOutputFile out("counts.txt", {"name", "state0", "state1", "state2"});
	std::vector<std::string_view> names = {"p1", "p2", "p3", "p4", "p5", "p6", "p7", "p8", "p9", "p10"};
	std::vector<double> values_1   = {0.67, 0.49, 0.74, 0.81, 0.64, 0.83, 0.62, 0.37, 0.89, 0.62};
	std::vector<double> values_2   = {0.95, 0.34, 0.64, 0.14, 0.44, 0.22, 0.9, 0.36, 0.08, 0.11};
	std::vector<double> values_3   = {0.27, 0.51, 0.21, 0.4, 0.24, 0.14, 0.01, 0.76, 0.4, 0.48};
	std::vector<size_t> maxRow     = {1, 2, 0, 0, 0, 0, 1, 2, 0, 0};
	for (size_t i = 0; i < values_1.size(); ++i) {
		out << names[i] << values_1[i] << values_2[i] << values_3[i] << coretools::endl;
	}
	out.close();

	TStatePosteriorsReaderBridge reader("counts.txt");
	reader._read("param");

	EXPECT_EQ(reader.values().size(), values_1.size());
	EXPECT_EQ(reader.names().size(), names.size());
	for (size_t i = 0; i < values_1.size(); i++) {
		EXPECT_EQ(reader.values()[i], coretools::str::toString(maxRow[i]));
		EXPECT_EQ(reader.names()[i], names[i]);
	}
}
