//
// Created by madleina on 19.12.23.
//

#ifndef STATTOOLS_TMOCKLATENTVARIABLE_H
#define STATTOOLS_TMOCKLATENTVARIABLE_H

#include "stattools/EM/TLatentVariable.h"
#include "coretools/Containers/TView.h"
#include "gmock/gmock.h"

//------------------------------------------------
// EM / HMM: Mock latent variable
//------------------------------------------------

namespace testing {
// EM: create mock latent variable
// dummy class that defined templates, because google mock hates templates
class DummyLatentVariable : public stattools::TLatentVariable<double, size_t, size_t> {};

// typedef for templated data vector, because google mock hates templates
typedef stattools::TDataVector<double, size_t> datVec;
typedef coretools::TConstView<double> cview;

class MockLatentVariableNOMLEmissionState : public DummyLatentVariable {
public:
	// function to calculate emission probabilities
	MOCK_METHOD(void, calculateEmissionProbabilities, (size_t Index, datVec &Emission), (const, override));

	// functions to get/set parameters
	MOCK_METHOD(std::vector<double>, getParameters, (), (const override));
	MOCK_METHOD(bool, setParameters, (cview), (override));

	// EM estimation
	MOCK_METHOD(size_t, getHiddenState, (size_t index), (const override));
	MOCK_METHOD(void, prepareEMParameterEstimationInitial, (), (override));
	MOCK_METHOD(void, prepareEMParameterEstimationOneIteration, (), (override));
	MOCK_METHOD(void, handleEMParameterEstimationOneIteration, (size_t index, const datVec &Weights), (override));
	MOCK_METHOD(void, finalizeEMParameterEstimationOneIteration, (), (override));
	MOCK_METHOD(void, finalizeEMParameterEstimationFinal, (), (override));

	// store posterior estimate of latent variable
	std::vector<uint8_t> z;
	void handleStatePosteriorEstimation(size_t, const datVec &StatePosteriorProbabilities) override {
		z.push_back(StatePosteriorProbabilities.maxIndex());
	}
};

class MockLatentVariable : public MockLatentVariableNOMLEmissionState {
public:
	// EM estimation
	MOCK_METHOD(size_t, getMLEmissionState, (size_t index, size_t), (const override));
};

//----------------------------------------------------------------
// EM / HMM: Class to create TNormalMixedModelInferred
//----------------------------------------------------------------

/*
template<class TypeZ, size_t N, size_t K> class TNormalMixedModelCreator {
public:
	using TypeObs                     = coretools::Unbounded;
	static constexpr size_t NumDimObs = 1;
	using SpecZ                       = stattools::ParamSpec<TypeZ, coretools::toHash("z"), 1>;
	using SpecMus                     = stattools::ParamSpec<coretools::Unbounded, coretools::toHash("mus"), 1>;
	using SpecVars                    = stattools::ParamSpec<coretools::StrictlyPositive, coretools::toHash("vars"), 1>;

	using BoxType = stattools::prior::TNormalMixedModelInferred<stattools::TObservationBase, TypeObs, 1, SpecZ, SpecMus,
																SpecVars, K>;

	using TypeParamMus  = stattools::TParameter<SpecMus, BoxType>;
	using TypeParamVars = stattools::TParameter<SpecVars, BoxType>;

	std::array<std::unique_ptr<TypeParamMus>, K> mus;
	std::array<std::unique_ptr<TypeParamVars>, K> vars;
	std::shared_ptr<BoxType> prior;
	std::unique_ptr<stattools::TObservation<TypeObs, NumDimObs>> obs;

	template<class TypeParamZ>
	void initialize(TypeParamZ Z, bool Fix, std::string_view FilenameData, const std::vector<double> &TrueMus,
					const std::vector<double> &TrueVars) {
		using namespace coretools::str;
		using namespace stattools::prior;

		std::array<TypeParamMus *, K> arrayMeans;
		std::array<TypeParamVars *, K> arrayVars;
		for (size_t i = 0; i < K; ++i) {
			stattools::TParameterDefinition defMu("out");
			if (Fix) { defMu.setInitVal(toString(TrueMus[i])); }
			mus[i]        = createParameter<TUniformFixed, SpecMus, BoxType>("mus_" + toString(i), defMu);
			arrayMeans[i] = mus[i].get();

			stattools::TParameterDefinition defVar("out");
			if (Fix) { defVar.setInitVal(toString(TrueVars[i])); }
			vars[i]      = createParameter<TUniformFixed, SpecVars, BoxType>("vars_" + toString(i), defVar);
			arrayVars[i] = vars[i].get();
		}
		prior = createPrior<BoxType>(Z, arrayMeans, arrayVars);

		auto data = readSingleLineIntoVec<double>(FilenameData);
		obs       = createObservation<TypeObs, NumDimObs>("obs", prior, {N}, {"out"}, data);

		for (auto it : prior->getAllPriorParameters()) { it->startInitialization(); }
	}
};
*/

//----------------------------------------------------------------
// EM / HMM: Class to create TMultivariateNormalMixedModelInferred
//----------------------------------------------------------------

/*
template<class TypeZ, size_t N, size_t K, size_t D> class TMultivariateNormalMixedModelCreator {
public:
	using TypeObs                     = coretools::Unbounded;
	static constexpr size_t NumDimObs = 2;
	using SpecZ                       = stattools::ParamSpec<TypeZ, coretools::toHash("z"), 1>;
	using SpecMus                     = stattools::ParamSpec<coretools::Unbounded, coretools::toHash("mus"), 1>;
	using SpecM                       = stattools::ParamSpec<coretools::StrictlyPositive, coretools::toHash("m"), 1>;
	using SpecMrr                     = stattools::ParamSpec<coretools::StrictlyPositive, coretools::toHash("mrr"), 1>;
	using SpecMrs                     = stattools::ParamSpec<coretools::Unbounded, coretools::toHash("mrs"), 1>;

	using BoxType =
		stattools::prior::TMultivariateNormalMixedModelInferred<stattools::TObservationBase, TypeObs, NumDimObs, SpecZ,
																SpecMus, SpecM, SpecMrr, SpecMrs, K>;

	using TypeParamMus = stattools::TParameter<SpecMus, BoxType>;
	using TypeParamM   = stattools::TParameter<SpecM, BoxType>;
	using TypeParamMrr = stattools::TParameter<SpecMrr, BoxType>;
	using TypeParamMrs = stattools::TParameter<SpecMrs, BoxType>;

	std::array<std::unique_ptr<TypeParamMus>, K> mus;
	std::array<std::unique_ptr<TypeParamM>, K> m;
	std::array<std::unique_ptr<TypeParamMrr>, K> mrr;
	std::array<std::unique_ptr<TypeParamMrs>, K> mrs;

	std::shared_ptr<BoxType> prior;
	std::unique_ptr<stattools::TObservation<TypeObs, NumDimObs>> obs;

	template<class TypeParamZ>
	void initialize(TypeParamZ Z, bool Fix, std::string_view FilenameData,
					const std::vector<std::vector<double>> &TrueMus, const std::vector<std::vector<double>> &TrueMrr,
					const std::vector<std::vector<double>> &TrueMrs) {
		using namespace testing;
		using namespace stattools;
		using namespace stattools::prior;
		using namespace coretools::str;

		// initialize pointers
		std::array<TypeParamMus *, K> arrayMeans;
		std::array<TypeParamM *, K> arrayM;
		std::array<TypeParamMrr *, K> arrayMrr;
		std::array<TypeParamMrs *, K> arrayMrs;

		for (size_t i = 0; i < K; ++i) {
			TParameterDefinition defMu("out");
			if (Fix) { defMu.setInitVal(coretools::str::concatenateString(TrueMus[i], ",")); }
			mus[i]        = createParameter<TUniformFixed, SpecMus, BoxType>("mus_" + toString(i), defMu);
			arrayMeans[i] = mus[i].get();

			TParameterDefinition defM("out");
			if (Fix) { defM.setInitVal("1.0"); }
			m[i]      = createParameter<TUniformFixed, SpecM, BoxType>("m_" + toString(i), defM);
			arrayM[i] = m[i].get();

			TParameterDefinition defMrr("out");
			if (Fix) { defMrr.setInitVal(coretools::str::concatenateString(TrueMrr[i], ",")); }
			mrr[i]      = createParameter<TUniformFixed, SpecMrr, BoxType>("mrr_" + toString(i), defMrr);
			arrayMrr[i] = mrr[i].get();

			TParameterDefinition defMrs("out");
			if (Fix) { defMrs.setInitVal(coretools::str::concatenateString(TrueMrs[i], ",")); }
			mrs[i]      = createParameter<TUniformFixed, SpecMrs, BoxType>("mrs_" + toString(i), defMrs);
			arrayMrs[i] = mrs[i].get();
		}

		prior     = createPrior<BoxType>(Z, arrayMeans, arrayM, arrayMrr, arrayMrs);
		auto data = readSingleLineIntoVec<double>(FilenameData);
		obs       = createObservation<TypeObs, NumDimObs>("obs", prior, {N, D}, {"out"}, data);

		// start initialization
		for (auto it : prior->getAllPriorParameters()) { it->startInitialization(); }
	}
};
*/

} // namespace testing
#endif // STATTOOLS_TMOCKLATENTVARIABLE_H
