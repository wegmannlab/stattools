//
// Created by madleina on 20.01.21.
//

#include "gtest/gtest.h"

#include "TMockLatentVariable.h"

#include "stattools/commonTestCases/TBernoulliTest.h"
#include "stattools/commonTestCases/commonTestingFunctions.h"
#include "stattools/commonTestCases/TCategoricalTest.h"
#include "coretools/Distributions/TNormalDistr.h"


#include "testFiles/TEM_Tables.h"

using namespace stattools;
using namespace testing;
using namespace stattools::prior;

uint8_t fillInitialHiddenStates_3States(size_t Index) { return getZ_3States_EM()[Index]; }
uint8_t fillInitialHiddenStates_2States(size_t Index) { return getZ_2States_EM()[Index]; }

void fillEmissions_3States(size_t Index, datVec &Emission) {
	// my desperate solution to calculate emission probabilities
	// didn't find another way to fill 2nd argument of calculateEmissionProbabilities,
	// so it is calculated in here every time (must be a free function)
	std::vector<double> mus  = {-2, 0, 2};
	std::vector<double> vars = {0.07551818, 0.11816428, 0.01457067};

	for (size_t k = 0; k < Emission.size(); k++) {
		Emission[k] = coretools::probdist::TNormalDistr::density(getX_3States_EM()[Index], mus[k], sqrt(vars[k]));
	}
}

void fillEmissions_2States(size_t Index, datVec &Emission) {
	// my desperate solution to calculate emission probabilities
	// didn't find another way to fill 2nd argument of calculateEmissionProbabilities,
	// so it is calculated in here every time (must be a free function)
	std::vector<double> x    = {};
	std::vector<double> mus  = {-2, 2};
	std::vector<double> vars = {0.07551818, 0.11816428};

	for (size_t k = 0; k < Emission.size(); k++) {
		Emission[k] = coretools::probdist::TNormalDistr::density(getX_2States_EM()[Index], mus[k], sqrt(vars[k]));
	}
}

class TPriorBernoulliEMMockTest : public Test {
protected:
	using TypeBern = TBernoulliTest<true, 1>;
	std::unique_ptr<TypeBern> bernoulli;
	std::unique_ptr<TypeBern::SpecZ> obs;

	uint8_t numStates = 2;
	size_t N          = 1000;

	void SetUp() override {
		instances::dagBuilder().clear();
		bernoulli = std::make_unique<TypeBern>("out");
		obs = createObservation<TypeBern::Type, TypeBern::numDimObs>("obs", &bernoulli->boxOnZ, {N}, {"out"}, {});
	}
};

TEST_F(TPriorBernoulliEMMockTest, EM) {
	// create mock latent variable and define expectations
	MockLatentVariable mockLatentVariable;

	// hidden states at the beginning: return something unmeaningful, but we need to initialize transition matrices
	// somehow
	EXPECT_CALL(mockLatentVariable, getHiddenState).WillRepeatedly(Invoke(fillInitialHiddenStates_2States));

	// emission probabilities: get corresponding emission probabilities for this index
	EXPECT_CALL(mockLatentVariable, calculateEmissionProbabilities).WillRepeatedly(Invoke(fillEmissions_2States));

	// EM updates of latent variable parameters: will be called, but don't have to return anything
	EXPECT_CALL(mockLatentVariable, getMLEmissionState).Times(N);
	EXPECT_CALL(mockLatentVariable, prepareEMParameterEstimationInitial).Times(1);
	EXPECT_CALL(mockLatentVariable, prepareEMParameterEstimationOneIteration).Times(AtLeast(1));
	EXPECT_CALL(mockLatentVariable, handleEMParameterEstimationOneIteration).Times(AtLeast(1));
	EXPECT_CALL(mockLatentVariable, finalizeEMParameterEstimationOneIteration).Times(AtLeast(1));
	EXPECT_CALL(mockLatentVariable, finalizeEMParameterEstimationFinal).Times(1);

	// now run EM!
	bernoulli->boxOnZ.runEMEstimation(mockLatentVariable);

	// check if correct pi was estimated
	double absError = 0.005;
	EXPECT_NEAR(bernoulli->boxOnZ.pi(), 0.2, absError);

	// check if posterior estimates of latent variable are close to truth
	auto trueZ = readSingleLineIntoVec<uint8_t>("EM/testFiles/trueZ_Bernoulli.txt");

	size_t counterDifferences = 0;
	for (size_t i = 0; i < N; i++) {
		if (trueZ[i] != mockLatentVariable.z[i]) { counterDifferences++; }
	}
	double fracDifferent = static_cast<double>(counterDifferences) / N;
	EXPECT_TRUE(fracDifferent < 0.001);
}

class TPriorCategoricalEMMockTest : public Test {
protected:
	std::unique_ptr<TCategoricalTest> cat;
	std::unique_ptr<TCategoricalTest::SpecObs> obs;

	uint8_t numStates = 3;
	size_t N          = 5000;

	void SetUp() override {
		instances::dagBuilder().clear();
		cat = std::make_unique<TCategoricalTest>("out", numStates, "1,2");
		obs = createObservation<TCategoricalTest::Type, TCategoricalTest::NumDimObs>("obs", &cat->boxOnObs, {N},
																					 {"out"}, {});
	}
};

TEST_F(TPriorCategoricalEMMockTest, EM) {
	// create mock latent variable and define expectations
	MockLatentVariable mockLatentVariable;

	// hidden states at the beginning: return something unmeaningful, but we need to initialize transition matrices
	// somehow
	EXPECT_CALL(mockLatentVariable, getHiddenState).WillRepeatedly(Invoke(fillInitialHiddenStates_3States));

	// emission probabilities: get corresponding emission probabilities for this index
	EXPECT_CALL(mockLatentVariable, calculateEmissionProbabilities).WillRepeatedly(Invoke(fillEmissions_3States));

	// EM updates o f latent variable parameters: will be called, but don't have to return anything
	EXPECT_CALL(mockLatentVariable, getMLEmissionState).Times(N);
	EXPECT_CALL(mockLatentVariable, prepareEMParameterEstimationInitial).Times(1);
	EXPECT_CALL(mockLatentVariable, prepareEMParameterEstimationOneIteration).Times(AtLeast(1));
	EXPECT_CALL(mockLatentVariable, handleEMParameterEstimationOneIteration).Times(AtLeast(1));
	EXPECT_CALL(mockLatentVariable, finalizeEMParameterEstimationOneIteration).Times(AtLeast(1));
	EXPECT_CALL(mockLatentVariable, finalizeEMParameterEstimationFinal).Times(1);

	// now run EM!
	cat->boxOnObs.runEMEstimation(mockLatentVariable);

	// check if correct pi was estimated
	double absError = 0.005;
	EXPECT_NEAR(cat->boxOnObs.pi(0), 0.0442, absError);
	EXPECT_NEAR(cat->boxOnObs.pi(1), 0.774, absError);
	EXPECT_NEAR(cat->boxOnObs.pi(2), 0.1818, absError);

	// check if posterior estimates of latent variable are close to truth
	auto trueZ                = readSingleLineIntoVec<uint8_t>("EM/testFiles/trueZ_Categorical.txt");
	size_t counterDifferences = 0;
	for (size_t i = 0; i < N; i++) {
		if (trueZ[i] != mockLatentVariable.z[i]) { counterDifferences++; }
	}
	double fracDifferent = static_cast<double>(counterDifferences) / N;
	EXPECT_TRUE(fracDifferent < 0.001);
}

//---------------------------------------------------------------
// TEM_with3IndependentComponentMultivariateNormalMixedModel_Test
//---------------------------------------------------------------

/*
class TEM_with3IndependentComponentMultivariateNormalMixedModel_Test : public Test {
protected:
	constexpr static size_t N = 1000;
	constexpr static size_t D = 3;
	constexpr static size_t K = 3;

	using SpecZ   = ParamSpec<coretools::UnsignedInt8WithMax<0>, coretools::toHash("z"), 1>;
	using SpecPis = ParamSpec<coretools::ZeroOpenOneClosed, coretools::toHash("pis"), 1>;

	using TypeMVN = TMultivariateNormalMixedModelCreator<SpecZ::value_type, N, K, D>;
	TypeMVN MVN;

	using BoxTypeZ = TCategoricalInferred<TParameterBase, SpecZ::value_type, SpecPis::numDim, SpecPis>;

	using TypeParamZ   = TParameter<SpecZ, TypeMVN::BoxType>;
	using TypeParamPis = TParameter<SpecPis, BoxTypeZ>;

	std::unique_ptr<TypeParamZ> z;
	std::unique_ptr<TypeParamPis> pis;

	std::shared_ptr<BoxTypeZ> priorOnZ;

	const std::vector<std::vector<double>> true_mus = {
		{-0.998557, 1.00729, -2.993031}, {-2.9914801, -0.999178, 1.03175}, {-0.503576, 1.99252, -2.00054}};

	const std::vector<std::vector<double>> true_mrr = {
		{2.012891, 8.993407, 1.78256}, {11.438899, 6.9853902, 4.1658702}, {7.462788, 1.36045, 4.2674799}};

	const std::vector<std::vector<double>> true_mrs = {
		{-5.13276, 2.145361, -2.6061299}, {1.82253, -0.5311809, 2.1059599}, {3.67276, 0.8444969, 2.459260}};

	std::vector<double> true_pis = {0.36, 0.151, 0.489};

	void initialize(bool FixMixedModelParams, bool FixPrior) {
		using namespace coretools::str;

		// create pis and z
		SpecZ::value_type::setMax(2);
		TParameterDefinition defPi("out", "0.1,0.2");
		if (FixPrior) { defPi.setInitVal(coretools::str::concatenateString(true_pis, ",")); }
		pis = createParameter<TDirichletFixed, SpecPis, BoxTypeZ>("pi", defPi);

		priorOnZ = createPrior<BoxTypeZ>(pis.get(), 3);
		z        = createParameter<BoxTypeZ, SpecZ, TypeMVN::BoxType>("z", {"out"}, priorOnZ);

		MVN.initialize(z.get(), FixMixedModelParams, "EM/testFiles/data_EM_3_MVN.txt", true_mus, true_mrr, true_mrs);

		// start initialization
		for (auto it : priorOnZ->getAllPriorParameters()) { it->startInitialization(); }
	}
};

TEST_F(TEM_with3IndependentComponentMultivariateNormalMixedModel_Test, fixMixedModelParams) {
	initialize(true, false);
	MVN.obs->guessInitialValues();

	// check if parameters of mixed model remained unchanged
	for (size_t d = 0; d < D; d++) {
		EXPECT_FLOAT_EQ(MVN.mus[0]->value(d), true_mus[0][d]);
		EXPECT_FLOAT_EQ(MVN.mus[1]->value(d), true_mus[1][d]);
		EXPECT_FLOAT_EQ(MVN.mus[2]->value(d), true_mus[2][d]);
	}
	EXPECT_FLOAT_EQ(MVN.m[0]->value(), 1.);
	EXPECT_FLOAT_EQ(MVN.m[1]->value(), 1.);
	EXPECT_FLOAT_EQ(MVN.m[2]->value(), 1.);
	for (size_t d = 0; d < D; d++) {
		EXPECT_FLOAT_EQ(MVN.mrr[0]->value(d), true_mrr[0][d]);
		EXPECT_FLOAT_EQ(MVN.mrr[1]->value(d), true_mrr[1][d]);
		EXPECT_FLOAT_EQ(MVN.mrr[2]->value(d), true_mrr[2][d]);
	}
	for (size_t d = 0; d < MVN.mrs[0]->size(); d++) {
		EXPECT_FLOAT_EQ(MVN.mrs[0]->value(d), true_mrs[0][d]);
		EXPECT_FLOAT_EQ(MVN.mrs[1]->value(d), true_mrs[1][d]);
		EXPECT_FLOAT_EQ(MVN.mrs[2]->value(d), true_mrs[2][d]);
	}

	// check if correct pis were estimated
	double absError = 0.01;
	EXPECT_NEAR(pis->value(0), true_pis[0], absError);
	EXPECT_NEAR(pis->value(1), true_pis[1], absError);
	EXPECT_NEAR(pis->value(2), true_pis[2], absError);

	// check if posterior estimates of latent variable are close to truth
	auto trueZ                = readSingleLineIntoVec<uint8_t>("EM/testFiles/trueZ_EM_3_MVN.txt");
	size_t counterDifferences = 0;
	for (size_t i = 0; i < N; i++) {
		if (trueZ[i] != z->value(i)) { counterDifferences++; }
	}
	double fracDifferent = static_cast<double>(counterDifferences) / N;
	EXPECT_TRUE(fracDifferent < 0.01);
}

TEST_F(TEM_with3IndependentComponentMultivariateNormalMixedModel_Test, fixTransMatParams) {
	initialize(false, true);
	MVN.obs->guessInitialValues();

	// check if parameters of mixed model are close to truth
	double absError = 0.15;

	for (size_t d = 0; d < D; d++) {
		EXPECT_NEAR(MVN.mus[0]->value(d), true_mus[0][d], absError);
		EXPECT_NEAR(MVN.mus[1]->value(d), true_mus[1][d], absError);
		EXPECT_NEAR(MVN.mus[2]->value(d), true_mus[2][d], absError);
	}
	EXPECT_NEAR(MVN.m[0]->value(), 1., absError);
	EXPECT_NEAR(MVN.m[1]->value(), 1., absError);
	EXPECT_NEAR(MVN.m[2]->value(), 1., absError);
	for (size_t d = 0; d < D; d++) {
		EXPECT_NEAR(MVN.mrr[0]->value(d), true_mrr[0][d], absError);
		EXPECT_NEAR(MVN.mrr[1]->value(d), true_mrr[1][d], absError);
		EXPECT_NEAR(MVN.mrr[2]->value(d), true_mrr[2][d], absError);
	}
	for (size_t d = 0; d < MVN.mrs[0]->size(); d++) {
		EXPECT_NEAR(MVN.mrs[0]->value(d), true_mrs[0][d], absError);
		EXPECT_NEAR(MVN.mrs[1]->value(d), true_mrs[1][d], absError);
		EXPECT_NEAR(MVN.mrs[2]->value(d), true_mrs[2][d], absError);
	}

	// check if pis remained unchanged
	EXPECT_FLOAT_EQ(pis->value(0), true_pis[0]);
	EXPECT_FLOAT_EQ(pis->value(1), true_pis[1]);
	EXPECT_FLOAT_EQ(pis->value(2), true_pis[2]);

	// check if posterior estimates of latent variable are close to truth
	auto trueZ                = readSingleLineIntoVec<uint8_t>("EM/testFiles/trueZ_EM_3_MVN.txt");
	size_t counterDifferences = 0;
	for (size_t i = 0; i < N; i++) {
		if (trueZ[i] != z->value(i)) { counterDifferences++; }
	}
	double fracDifferent = static_cast<double>(counterDifferences) / N;
	EXPECT_TRUE(fracDifferent < 0.01);
}

TEST_F(TEM_with3IndependentComponentMultivariateNormalMixedModel_Test, estimateBoth) {
	initialize(false, false);
	MVN.obs->guessInitialValues();

	// check if parameters of mixed model are close to truth
	double absError = 0.15;

	for (size_t d = 0; d < D; d++) {
		EXPECT_NEAR(MVN.mus[0]->value(d), true_mus[0][d], absError);
		EXPECT_NEAR(MVN.mus[1]->value(d), true_mus[1][d], absError);
		EXPECT_NEAR(MVN.mus[2]->value(d), true_mus[2][d], absError);
	}
	EXPECT_NEAR(MVN.m[0]->value(), 1., absError);
	EXPECT_NEAR(MVN.m[1]->value(), 1., absError);
	EXPECT_NEAR(MVN.m[2]->value(), 1., absError);
	for (size_t d = 0; d < D; d++) {
		EXPECT_NEAR(MVN.mrr[0]->value(d), true_mrr[0][d], absError);
		EXPECT_NEAR(MVN.mrr[1]->value(d), true_mrr[1][d], absError);
		EXPECT_NEAR(MVN.mrr[2]->value(d), true_mrr[2][d], absError);
	}
	for (size_t d = 0; d < MVN.mrs[0]->size(); d++) {
		EXPECT_NEAR(MVN.mrs[0]->value(d), true_mrs[0][d], absError);
		EXPECT_NEAR(MVN.mrs[1]->value(d), true_mrs[1][d], absError);
		EXPECT_NEAR(MVN.mrs[2]->value(d), true_mrs[2][d], absError);
	}

	// check if correct pis were estimated
	absError = 0.01;
	EXPECT_NEAR(pis->value(0), true_pis[0], absError);
	EXPECT_NEAR(pis->value(1), true_pis[1], absError);
	EXPECT_NEAR(pis->value(2), true_pis[2], absError);

	// check if posterior estimates of latent variable are close to truth
	auto trueZ                = readSingleLineIntoVec<uint8_t>("EM/testFiles/trueZ_EM_3_MVN.txt");
	size_t counterDifferences = 0;
	for (size_t i = 0; i < N; i++) {
		if (trueZ[i] != z->value(i)) { counterDifferences++; }
	}
	double fracDifferent = static_cast<double>(counterDifferences) / N;
	EXPECT_TRUE(fracDifferent < 0.01);
}

 */
