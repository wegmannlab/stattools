#include "gtest/gtest.h"

#include "stattools/EM/TDataVector.h"
#include <vector>

using namespace stattools;


TEST(TDataVectorTests, copyFromCurrent) {
	int testSize = 10;
	TDataVector<double, int> datavec(testSize);
	std::vector<double> vec(testSize);

	for(int i = 0; i < testSize; ++i){
		datavec[i] = 1.0 / (double) i;
	}

	datavec.copyFromCurrent(vec.data());

	for(int i = 0; i < testSize; ++i){
		EXPECT_EQ(vec[i], datavec[i]);
	}
}

TEST(TDataVectorTests, copyToCurrent) {
	int testSize = 10;
	TDataVector<double, int> datavec(testSize);
	std::vector<double> vec(testSize);

	for(int i = 0; i < testSize; ++i){
		vec[i] = 1.0 / (double) i;
	}

	datavec.copyToCurrent(vec.data());

	for(int i = 0; i < testSize; ++i){
		EXPECT_EQ(vec[i], datavec[i]);
	}
}
