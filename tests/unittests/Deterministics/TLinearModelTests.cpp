//
// Created by madleina on 20.12.23.
//

#include "stattools/commonTestCases/TLinearModelTest.h"
#include "stattools/commonTestCases/commonTestingFunctions.h"
#include "stattools/Priors/TPriorNormal.h"
#include "gtest/gtest.h"

using namespace testing;
using namespace stattools;
using namespace stattools::prior;
using namespace coretools;

//-----------------------------
// TLinearModelTest
// Without Intercept
//-----------------------------

class TLinearModelNoInterceptTest : public Test {
public:
	std::vector<double> y() {
		return {-0.626453810742332, -0.820468384118015, 1.51178116845085,    -0.0449336090152309, 0.183643324222082,
				0.487429052428485,  0.389843236411431,  -0.0161902630989461, -0.835628612410047,  0.738324705129217,
				-0.621240580541804, 0.943836210685299,  1.59528080213779,    0.575781351653492,   -2.2146998871775,
				0.821221195098089,  0.329507771815361,  -0.305388387156356,  1.12493091814311,    0.593901321217509};
	}

	std::vector<double> x() {
		return {0.820946294115856, 0.789356231689453, 0.477619622135535, 0.647060193819925, 0.023331202333793,
				0.8612094768323,   0.78293276228942,  0.477230065036565, 0.438097107224166, 0.553036311641335,
				0.7323137386702,   0.244797277031466, 0.529719580197707, 0.692731556482613, 0.0706790471449494};
	}

	std::vector<double> beta() {
		return {-6.85640068517645, 1.73992630727224,   2.58885306117318,  2.61336167316667,
				4.72227100212956,  -1.3899975805281,   -1.73745291170378, -0.859473648049261,
				4.90131441745786,  -0.748985184224284, -1.64708150344172, -2.03055749281255};
	}

	std::vector<double> y_pred() {
		return {0.439781249822011, -0.0265472189075988, -0.0328384018400137, 0.497164598268426,  -0.105269270782164,
				0.44837360016024,  0.216124698226422,   -0.0777855988854192, -0.967239362457308, 0.370768431614176,
				0.476151470275126, 0.74633844502438,    0.866173809787012,   -0.239021431072509, -0.24383195629205,
				0.318804595125099, -0.0142833183972961, -0.0941597135497534, 0.0513635456850671, 0.645446461680641};
	}

	constexpr static bool Intercept = false;
	using BoxOnBeta                 = prior::TNormalFixed<TParameterBase, coretools::Unbounded, 2>;
	using TypeTest                  = TLinearModelTest<Intercept, true, BoxOnBeta>;
	using BoxAroundBelow            = DummyBox<TypeTest::TypeBelow, TypeTest::NumDimBelow>;

	std::unique_ptr<TypeTest> test;
	BoxOnBeta boxOnBeta;

	size_t I = 5;
	size_t J = 4;
	size_t K = 3;

	void SetUp() override {
		stattools::instances::dagBuilder().clear();

		test = std::make_unique<TypeTest>("out", std::array<size_t, 2>({I, K}), &boxOnBeta);
		test->y.initStorage(
			&test->boxOnPi, {I, J},
			{std::make_shared<coretools::TNamesIndices>(I), std::make_shared<coretools::TNamesIndices>(J)});
		test->boxOnY.initialize();

		// fill values in y and x
		for (size_t i = 0; i < test->y.size(); ++i) { test->y.set(i, y()[i]); }
		for (size_t i = 0; i < test->x.size(); ++i) { test->x[i] = x()[i]; }
	}
};

TEST_F(TLinearModelNoInterceptTest, guessInitialValues) {
	test->boxOnY.guessInitialValues();

	// in R: beta <- solve(t(X) %*% X) %*% t(X) %*% Y[,j]
	for (size_t i = 0; i < beta().size(); ++i) { EXPECT_FLOAT_EQ(test->beta.value(i), beta()[i]); }
}

TEST_F(TLinearModelNoInterceptTest, valuesForBelow) {
	for (size_t i = 0; i < beta().size(); ++i) { test->beta.set(i, beta()[i]); }

	// in R: y_pred = X %*% beta
	for (size_t i = 0; i < y_pred().size(); ++i) { EXPECT_FLOAT_EQ(test->boxOnY.valueForBelow(i), y_pred()[i]); }
}

TEST_F(TLinearModelNoInterceptTest, getYRange) {
	for (size_t k = 0; k < K; ++k) {
		for (size_t j = 0; j < J; ++j) {
			const coretools::TRange betaRange(test->beta.getIndex({k, j}));
			const coretools::TRange yRange = test->boxOnY.getYRange(betaRange);
			size_t c                       = 0;
			for (size_t i = yRange.begin; i < yRange.end; i += yRange.increment, ++c) {
				const auto coord = test->y.getSubscripts(i);
				EXPECT_EQ(coord[1], j);
			}
			EXPECT_EQ(c, I);
		}
	}
}

//-----------------------------
// TLinearModelTest
// With Intercept
//-----------------------------

class TLinearModelInterceptTest : public Test {
public:
	std::vector<double> y() {
		return {-0.626453810742332, -0.820468384118015, 1.51178116845085,  0.183643324222082,  0.487429052428485,
				0.389843236411431,  -0.835628612410047, 0.738324705129217, -0.621240580541804, 1.59528080213779,
				0.575781351653492,  -2.2146998871775,   0.329507771815361, -0.305388387156356, 1.12493091814311};
	}

	std::vector<double> x() {
		return {0.668466738192365, 0.820946294115856, 0.789356231689453, 0.79423986072652,  0.647060193819925,
				0.023331202333793, 0.107943625887856, 0.78293276228942,  0.477230065036565, 0.723710946040228,
				0.553036311641335, 0.7323137386702,   0.411274429643527, 0.529719580197707, 0.692731556482613};
	}

	std::vector<double> beta() {
		return {2.7437651456879,    2.37521804621869,  -2.82300204356829, 1.25795611161052,
				-0.908496379856677, 0.501589977557511, -5.39730473818567, -1.59869806084784,
				3.89664615533414,   0.558726438652774, -1.25702635524088, -0.0153685293481547};
	}

	std::vector<double> y_pred() {
		return {-0.405196162461919,  -0.536768400854184, 0.699100148929049, 0.263538742167345, 0.589872194665998,
				0.0963867605991818,  -1.07953216334664,  0.42558779476035,  0.274619011118141, 1.07841929527183,
				-0.0869464771343784, -0.316263654053279, 0.789119763392225, 0.283933226499038, -0.563227411307007};
	}

	constexpr static bool Intercept = true;
	using BoxOnBeta                 = prior::TNormalFixed<TParameterBase, coretools::Unbounded, 2>;
	using TypeTest                  = TLinearModelTest<Intercept, true, BoxOnBeta>;
	using BoxAroundBelow            = DummyBox<TypeTest::TypeBelow, TypeTest::NumDimBelow>;
	using TypeParamBelow            = TParameter<TypeTest::SpecY, BoxAroundBelow>;

	std::unique_ptr<TypeTest> test;
	BoxOnBeta boxOnBeta;

	size_t I = 5;
	size_t J = 3;
	size_t K = 3; // don't add intercept here

	void SetUp() override {
		stattools::instances::dagBuilder().clear();

		test = std::make_unique<TypeTest>("out", std::array<size_t, 2>({I, K}), &boxOnBeta);
		test->y.initStorage(
			&test->boxOnPi, {I, J},
			{std::make_shared<coretools::TNamesIndices>(I), std::make_shared<coretools::TNamesIndices>(J)});
		test->boxOnY.initialize();


		// fill values in y and x
		for (size_t i = 0; i < test->y.size(); ++i) { test->y.set(i, y()[i]); }
		for (size_t i = 0; i < test->x.size(); ++i) { test->x[i] = x()[i]; }
	}
};

TEST_F(TLinearModelInterceptTest, guessInitialValues) {
	test->boxOnY.guessInitialValues();

	// in R: beta <- solve(t(X) %*% X) %*% t(X) %*% Y[,j]
	for (size_t i = 0; i < beta().size(); ++i) { EXPECT_FLOAT_EQ(test->beta.value(i), beta()[i]); }
}

TEST_F(TLinearModelInterceptTest, valuesForBelow) {
	for (size_t i = 0; i < beta().size(); ++i) { test->beta.set(i, beta()[i]); }

	// in R: y_pred = X %*% beta
	for (size_t i = 0; i < y_pred().size(); ++i) { EXPECT_FLOAT_EQ(test->boxOnY.valueForBelow(i), y_pred()[i]); }
}

TEST_F(TLinearModelInterceptTest, getYRange) {
	for (size_t k = 0; k < K; ++k) {
		for (size_t j = 0; j < J; ++j) {
			const coretools::TRange betaRange(test->beta.getIndex({k, j}));
			const coretools::TRange yRange = test->boxOnY.getYRange(betaRange);
			size_t c                       = 0;
			for (size_t i = yRange.begin; i < yRange.end; i += yRange.increment, ++c) {
				const auto coord = test->y.getSubscripts(i);
				EXPECT_EQ(coord[1], j);
			}
			EXPECT_EQ(c, I);
		}
	}
}
