//
// Created by madleina on 20.12.23.
//

#include "coretools/Main/TRandomGenerator.h"
#include "stattools/Deterministics/TLinkFunctions.h"
#include "stattools/ParametersObservations/TParameter.h"
#include "stattools/commonTestCases/TLinkFunctionsTest.h"
#include "stattools/commonTestCases/commonTestingFunctions.h"
#include "gtest/gtest.h"

using namespace testing;
using namespace stattools;
using namespace stattools::prior;
using namespace coretools;
using coretools::P;
using coretools::instances::randomGenerator;

//-----------------------------
// TLogTest
//-----------------------------

class TLogTest : public Test {
public:
	using TypeBelow      = Unbounded;
	using TypeParam      = StrictlyPositive;
	using TypeTest       = TLinkFunctionsTest<TypeBelow, TypeParam, det::TLog>;
	using BoxAroundBelow = DummyBox<TypeBelow, 1>;
	using TypeParamBelow = TParameter<TypeTest::SpecBelow, BoxAroundBelow>;

	std::unique_ptr<TypeTest> test;
	std::unique_ptr<TypeParamBelow> below; // can not be observation
	BoxAroundBelow boxAroundBelow;

	void SetUp() override {
		stattools::instances::dagBuilder().clear();

		test  = std::make_unique<TypeTest>("out", "param");
		below = std::make_unique<TypeParamBelow>("below", &test->boxOnBelow, TParameterDefinition("out"));

		below->initStorage(&boxAroundBelow, {21}, {std::make_shared<TNamesIndices>(21)});
		test->boxOnBelow.initialize();
	}
};

TEST_F(TLogTest, guessInitialValues) {
	// set values in parameter below
	for (size_t i = 0; i < below->size(); ++i) { below->set(i, randomGenerator().getNormalRandom(0, 10)); }

	test->boxOnBelow.guessInitialValues();
	for (size_t i = 0; i < below->size(); ++i) { EXPECT_EQ(test->param.value(i), std::exp(below->value(i))); }
}

TEST_F(TLogTest, valuesForBelow) {
	// set values in parameter
	for (size_t i = 0; i < below->size(); ++i) { test->param.set(i, randomGenerator().getRand()); }

	for (size_t i = 0; i < below->size(); ++i) {
		EXPECT_EQ(test->boxOnBelow.valueForBelow(i), std::log(test->param.value(i)));
	}
}

//-----------------------------
// TExpTest
//-----------------------------

class TExpTest : public Test {
public:
	using TypeBelow      = StrictlyPositive;
	using TypeParam      = Unbounded;
	using TypeTest       = TLinkFunctionsTest<TypeBelow, TypeParam, det::TExp>;
	using BoxAroundBelow = DummyBox<TypeBelow, 1>;
	using TypeParamBelow = TParameter<TypeTest::SpecBelow, BoxAroundBelow>;

	std::unique_ptr<TypeTest> test;
	std::unique_ptr<TypeParamBelow> below; // can not be observation
	BoxAroundBelow boxAroundBelow;

	void SetUp() override {
		stattools::instances::dagBuilder().clear();

		test  = std::make_unique<TypeTest>("out", "param");
		below = std::make_unique<TypeParamBelow>("below", &test->boxOnBelow, TParameterDefinition("out"));

		below->initStorage(&boxAroundBelow, {21}, {std::make_shared<TNamesIndices>(21)});
		test->boxOnBelow.initialize();
	}
};

TEST_F(TExpTest, guessInitialValues) {
	// set values in parameter below
	for (size_t i = 0; i < below->size(); ++i) { below->set(i, randomGenerator().getRand()); }

	test->boxOnBelow.guessInitialValues();
	for (size_t i = 0; i < below->size(); ++i) { EXPECT_EQ(test->param.value(i), std::log(below->value(i))); }
}

TEST_F(TExpTest, valuesForBelow) {
	// set values in parameter
	for (size_t i = 0; i < below->size(); ++i) { test->param.set(i, randomGenerator().getNormalRandom(0, 10)); }

	for (size_t i = 0; i < below->size(); ++i) {
		EXPECT_EQ(test->boxOnBelow.valueForBelow(i), std::exp(test->param.value(i)));
	}
}

//-----------------------------
// TLogitTest
//-----------------------------

class TLogitTest : public Test {
public:
	using TypeBelow      = Unbounded;
	using TypeParam      = Probability;
	using TypeTest       = TLinkFunctionsTest<TypeBelow, TypeParam, det::TLogit>;
	using BoxAroundBelow = DummyBox<TypeBelow, 1>;
	using TypeParamBelow = TParameter<TypeTest::SpecBelow, BoxAroundBelow>;

	std::unique_ptr<TypeTest> test;
	std::unique_ptr<TypeParamBelow> below; // can not be observation
	BoxAroundBelow boxAroundBelow;

	void SetUp() override {
		stattools::instances::dagBuilder().clear();

		test  = std::make_unique<TypeTest>("out", "param");
		below = std::make_unique<TypeParamBelow>("below", &test->boxOnBelow, TParameterDefinition("out"));

		below->initStorage(&boxAroundBelow, {21}, {std::make_shared<TNamesIndices>(21)});
		test->boxOnBelow.initialize();
	}
};

TEST_F(TLogitTest, guessInitialValues) {
	// set values in parameter below
	for (size_t i = 0; i < below->size(); ++i) { below->set(i, randomGenerator().getNormalRandom(0, 10)); }

	test->boxOnBelow.guessInitialValues();
	for (size_t i = 0; i < below->size(); ++i) { EXPECT_EQ(test->param.value(i), logistic(below->value(i))); }
}

TEST_F(TLogitTest, valuesForBelow) {
	// set values in parameter
	for (size_t i = 0; i < below->size(); ++i) { test->param.set(i, P(randomGenerator().getRand())); }

	for (size_t i = 0; i < below->size(); ++i) {
		EXPECT_EQ(test->boxOnBelow.valueForBelow(i), logit(test->param.value(i)));
	}
}

//-----------------------------
// TLogisticTest
//-----------------------------

class TLogisticTest : public Test {
public:
	using TypeBelow      = Probability;
	using TypeParam      = Unbounded;
	using TypeTest       = TLinkFunctionsTest<TypeBelow, TypeParam, det::TLogistic>;
	using BoxAroundBelow = DummyBox<TypeBelow, 1>;
	using TypeParamBelow = TParameter<TypeTest::SpecBelow, BoxAroundBelow>;

	std::unique_ptr<TypeTest> test;
	std::unique_ptr<TypeParamBelow> below; // can not be observation
	BoxAroundBelow boxAroundBelow;

	void SetUp() override {
		stattools::instances::dagBuilder().clear();

		test  = std::make_unique<TypeTest>("out", "param");
		below = std::make_unique<TypeParamBelow>("below", &test->boxOnBelow, TParameterDefinition("out"));

		below->initStorage(&boxAroundBelow, {21}, {std::make_shared<TNamesIndices>(21)});
		test->boxOnBelow.initialize();
	}
};

TEST_F(TLogisticTest, guessInitialValues) {
	// set values in parameter below
	for (size_t i = 0; i < below->size(); ++i) { below->set(i, P(randomGenerator().getRand())); }

	test->boxOnBelow.guessInitialValues();
	for (size_t i = 0; i < below->size(); ++i) { EXPECT_EQ(test->param.value(i), logit(below->value(i))); }
}

TEST_F(TLogisticTest, valuesForBelow) {
	// set values in parameter
	for (size_t i = 0; i < below->size(); ++i) { test->param.set(i, randomGenerator().getNormalRandom(0, 10)); }

	for (size_t i = 0; i < below->size(); ++i) {
		EXPECT_EQ(test->boxOnBelow.valueForBelow(i), logistic(test->param.value(i)));
	}
}

//-----------------------------
// TExpTest
//-----------------------------

class TLinkTest : public Test {
public:
	using TypeBelow      = StrictlyPositive;
	using TypeParam      = Unbounded;
	using TypeTest       = TLinkFunctionsTest<TypeBelow, TypeParam, det::TLinkFunction>;
	using BoxAroundBelow = DummyBox<TypeBelow, 1>;
	using TypeParamBelow = TParameter<TypeTest::SpecBelow, BoxAroundBelow>;

	std::unique_ptr<TypeTest> test;
	std::unique_ptr<TypeParamBelow> below; // can not be observation
	BoxAroundBelow boxAroundBelow;

	void SetUp() override {
		stattools::instances::dagBuilder().clear();

		auto funcDown = [](double x) { return std::exp(x); };
		auto funcUp   = [](double x) { return std::log(x); };

		test  = std::make_unique<TypeTest>("out", "param", funcDown, funcUp, "customExp");
		below = std::make_unique<TypeParamBelow>("below", &test->boxOnBelow, TParameterDefinition("out"));

		below->initStorage(&boxAroundBelow, {21}, {std::make_shared<TNamesIndices>(21)});
		test->boxOnBelow.initialize();
	}
};

TEST_F(TLinkTest, guessInitialValues) {
	// set values in parameter below
	for (size_t i = 0; i < below->size(); ++i) { below->set(i, randomGenerator().getRand()); }

	test->boxOnBelow.guessInitialValues();
	for (size_t i = 0; i < below->size(); ++i) { EXPECT_EQ(test->param.value(i), std::log(below->value(i))); }
}

TEST_F(TLinkTest, valuesForBelow) {
	// set values in parameter
	for (size_t i = 0; i < below->size(); ++i) { test->param.set(i, randomGenerator().getNormalRandom(0, 10)); }

	for (size_t i = 0; i < below->size(); ++i) {
		EXPECT_EQ(test->boxOnBelow.valueForBelow(i), std::exp(test->param.value(i)));
	}
}
