#include "gtest/gtest.h"

#include "stattools/commonTestCases/TCategoricalTest.h"
#include "stattools/ParametersObservations/TObservation.h"
#include "stattools/commonTestCases/commonTestingFunctions.h"

using namespace testing;
using namespace stattools;
using namespace stattools::prior;

//--------------------------------------------
// TCategoricalInferred
//--------------------------------------------

std::vector<double> values_pis() {
	return {0.181818181818182,  0.163636363636364,  0.145454545454545,  0.127272727272727,  0.109090909090909,
			0.0909090909090909, 0.0727272727272727, 0.0545454545454545, 0.0363636363636364, 0.0181818181818182};
}

class TCategoricalInferredTest : public Test {
public:
	size_t K = 10;
	std::unique_ptr<TCategoricalTest> test;

	std::unique_ptr<TCategoricalTest::SpecObs> obs;

	void SetUp() override {
		instances::dagBuilder().clear();

		test = std::make_unique<TCategoricalTest>("out", K, "1,2,3,4,5,6,7,8,9,10");
		obs  = createObservation<TCategoricalTest::Type, TCategoricalTest::NumDimObs>(
			"obs", &test->boxOnObs, {21}, {"out"}, {7, 8, 6, 5, 7, 6, 1, 7, 2, 5, 6, 3, 2, 3, 4, 4, 7, 6, 5, 7, 9});

		for (size_t i = 0; i < values_pis().size(); ++i) { test->pi.set(i, values_pis()[i]); }
	}
};

TEST_F(TCategoricalInferredTest, constructor) {
	for (size_t k = 0; k < K; k++) { EXPECT_FLOAT_EQ(test->boxOnObs.pi(k), values_pis()[k]); }
	EXPECT_TRUE(test->pi.isPartOfBox());

	// check for max
	EXPECT_EQ(obs->max(), K - 1);
}

TEST_F(TCategoricalInferredTest, constructor_ThrowIfMaxDoesntMatch) {
	TCategoricalTest::Type::setMax(5);
	EXPECT_ANY_THROW(test->boxOnObs.initialize());
}

TEST_F(TCategoricalInferredTest, getDensity) {
	for (size_t i = 0; i < obs->size(); ++i) {
		EXPECT_FLOAT_EQ(test->boxOnObs.getDensity(obs->storage(), i), test->pi.value(obs->storage()[i]));
		EXPECT_FLOAT_EQ(test->boxOnObs.getLogDensity(obs->storage(), i), log(test->pi.value(obs->storage()[i])));
	}

	double sum = test->boxOnObs.getSumLogPriorDensity(obs->storage());
	EXPECT_FLOAT_EQ(sum, -53.76285);
}

TEST_F(TCategoricalInferredTest, getLogDensityRatio) {
	auto storage = createUpdatedStorage<TCategoricalTest::Type, TCategoricalTest::NumDimObs>(obs->storage());

	storage[0] = 1;
	storage[0] = 0; // update 1 -> 0
	EXPECT_FLOAT_EQ(test->boxOnObs.getLogDensityRatio(storage, 0), 0.1053605157);

	storage[5] = 8;
	storage[5] = 9; // update 9 -> 8
	EXPECT_FLOAT_EQ(test->boxOnObs.getLogDensityRatio(storage, 5), -0.6931471806);
}

TEST_F(TCategoricalInferredTest, updatePi) {
	test->pi.set(0, test->pi.value(0) + 0.01);
	test->pi.set(1, test->pi.value(1) - 0.01); // such that it sums to one

	// calculate LL
	// in R: sum(x == 0) * log(pi_new[1] / pi[1]) + sum(x == 1) * log(prob_new[2] / prob[2])
	auto f1 = test->boxOnObs.calculateLLRatio(&test->pi, 0);
	auto f2 = test->boxOnObs.calculateLLRatio(&test->pi, 1);
	EXPECT_FLOAT_EQ(f1(&obs->storage()) + f2(&obs->storage()), -0.06305814);
}

TEST_F(TCategoricalInferredTest, _setPiToMLE_1param) {
	test->boxOnObs.guessInitialValues();

	// equivalent to: mean(vals) in R
	std::vector<double> expected = {9.99990000099999e-06, 0.0476185714333333, 0.0952371428666666, 0.0952371428666666,
									0.0952371428666666,   0.1428557143,       0.190474285733333,  0.238092857166666,
									0.0476185714333333,   0.0476185714333333};
	for (size_t k = 0; k < K; k++) { EXPECT_FLOAT_EQ(test->pi.value(k), expected[k]); }
}
