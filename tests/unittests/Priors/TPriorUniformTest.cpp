//
// Created by caduffm on 3/28/22.
//

#include "stattools/Priors/TPriorUniform.h"
#include "gtest/gtest.h"

using namespace testing;
using namespace stattools;
using namespace stattools::prior;

struct TUniformFixedTest : Test {
	std::string params;

	using Type = coretools::Unbounded;
	TUniformFixed<TParameterBase, Type, 1> prior;

	coretools::TMultiDimensionalStorage<TValueUpdated<Type>, 1> storage;
};

TEST_F(TUniformFixedTest, initValid) { EXPECT_NO_THROW(prior.setFixedPriorParameters(params)); }

TEST_F(TUniformFixedTest, initUniformThrowIfParametersGiven) {
	params = "1.";
	EXPECT_ANY_THROW(prior.setFixedPriorParameters(params));
	params = "-10,10";
	EXPECT_ANY_THROW(prior.setFixedPriorParameters(params));
}

TEST_F(TUniformFixedTest, getLogDensity_Default) {
	prior.setFixedPriorParameters(params);
	storage.emplace_back(TValueUpdated<Type>(5));

	EXPECT_EQ(prior.getDensity(storage, 0), 1.);
	EXPECT_EQ(prior.getLogDensity(storage, 0), 0.);
}

TEST_F(TUniformFixedTest, getLogDensity_Probability) {
	TUniformFixed<TParameterBase, coretools::Probability, 1> priorProb;
	priorProb.setFixedPriorParameters(params);

	storage.emplace_back(TValueUpdated<Type>(0.1));
	storage.emplace_back(TValueUpdated<Type>(0.5));
	storage.emplace_back(TValueUpdated<Type>(1.0));

	EXPECT_FLOAT_EQ(prior.getLogDensity(storage, 0), 0.);
	EXPECT_FLOAT_EQ(prior.getLogDensity(storage, 1), 0.);
	EXPECT_FLOAT_EQ(prior.getLogDensity(storage, 2), 0.);

	EXPECT_FLOAT_EQ(prior.getDensity(storage, 0), 1.);
	EXPECT_FLOAT_EQ(prior.getDensity(storage, 1), 1.);
	EXPECT_FLOAT_EQ(prior.getDensity(storage, 2), 1.);
}

TEST_F(TUniformFixedTest, hastings_Default) {
	storage.emplace_back(TValueUpdated<Type>(-5.));
	storage[0] = TValueUpdated<Type>(5.); // update

	EXPECT_EQ(prior.getLogDensity(storage, 0) - prior.getLogDensity(storage, 0),
			  prior.getLogDensityRatio(storage, 0));
}

//------------------------------------------------
// Type = int
//------------------------------------------------

struct TUniformFixedTest_Int : Test {
	std::string params;

	using Type = coretools::Integer;

	TUniformFixed<TParameterBase, Type, 1> prior;
	coretools::TMultiDimensionalStorage<TValueUpdated<Type>, 1> storage;
};

TEST_F(TUniformFixedTest_Int, getLogDensity_Default) {
	prior.setFixedPriorParameters(params);
	storage.emplace_back(TValueUpdated<Type>(5));

	EXPECT_FLOAT_EQ(prior.getLogDensity(storage, 0), -22.18071);
	EXPECT_FLOAT_EQ(prior.getDensity(storage, 0), exp(-22.18071));
}

TEST_F(TUniformFixedTest_Int, hastings) {
	prior.setFixedPriorParameters(params);
	storage.emplace_back(TValueUpdated<Type>(-5.));
	storage[0] = TValueUpdated<Type>(5.); // update

	EXPECT_EQ(prior.getLogDensity(storage, 0) - prior.getLogDensity(storage, 0),
			  prior.getLogDensityRatio(storage, 0));
}

//------------------------------------------------
// Type = unit8_t
//------------------------------------------------

struct TUniformFixedTest_UInt8 : Test {
	std::string params;

	using Type = coretools::UnsignedInt8;

	TUniformFixed<TParameterBase, Type, 1> prior;
	coretools::TMultiDimensionalStorage<TValueUpdated<Type>, 1> storage;
};

TEST_F(TUniformFixedTest_UInt8, getLogDensity) {
	prior.setFixedPriorParameters(params);
	storage.emplace_back(TValueUpdated<Type>(0));

	EXPECT_FLOAT_EQ(prior.getLogDensity(storage, 0), -5.541264);
	EXPECT_FLOAT_EQ(prior.getDensity(storage, 0), exp(-5.541264));
}

//------------------------------------------------
// Type = bool
//------------------------------------------------

struct TUniformFixedTest_Bool : Test {
	std::string params;

	using Type = coretools::Boolean;

	TUniformFixed<TParameterBase, Type, 1> prior;
	coretools::TMultiDimensionalStorage<TValueUpdated<Type>, 1> storage;
};

TEST_F(TUniformFixedTest_Bool, checkMinMaxDefault) {
	prior.setFixedPriorParameters(params);

	storage.emplace_back(TValueUpdated<Type>(true));
	EXPECT_EQ(prior.getLogDensity(storage, 0), 0.);
}

TEST_F(TUniformFixedTest_Bool, hastings) {
	prior.setFixedPriorParameters(params);

	storage.emplace_back(TValueUpdated<Type>(true));
	storage[0] = TValueUpdated<Type>(false); // update
	EXPECT_EQ(prior.getLogDensity(storage, 0) - prior.getLogDensity(storage, 0),
			  prior.getLogDensityRatio(storage, 0));
}
