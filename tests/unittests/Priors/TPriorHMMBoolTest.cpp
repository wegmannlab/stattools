#include "stattools/commonTestCases/THMMBoolTest.h"
#include "stattools/commonTestCases/commonTestingFunctions.h"
#include "gtest/gtest.h"

using namespace testing;
using namespace stattools;
using namespace stattools::prior;

//--------------------------------------------
// THMMBoolInferred
//--------------------------------------------

class THMMBoolInferredTest : public Test {
public:
	using HMM = THMMBoolTest<true>;
	std::unique_ptr<HMM> test;

	std::unique_ptr<genometools::TDistancesBinnedBase> distances;
	std::unique_ptr<HMM::SpecZ> obs;
	std::unique_ptr<HMM::SpecZ> obs2;

	void SetUp() override {
		instances::dagBuilder().clear();

		distances = createAndFillDistances(21, 16);
		test      = std::make_unique<HMM>("out", distances.get());

		obs = createObservation<HMM::Type, HMM::NumDimObs>(
			"obs", &test->boxOnZ, {21}, {"out"},
			{false, true,  true, false, false, true,  true, false, false, true, true,
			 false, false, true, true,  false, false, true, true,  false, false});

		// set initial values
		test->pi.set(0.4);
		test->gamma.set(0.47);
		test->boxOnZ.updateTau(test->boxOnZ.getParams());
	}

	void initializeExtraObs() {
		obs2 = createObservation<HMM::Type, HMM::NumDimObs>(
			"obs2", &test->boxOnZ, {21}, {"out"},
			{true, false, false, true,  true, false, false, true,  true, false, false,
			 true, true,  false, false, true, true,  false, false, true, true});
	}

	auto getStorageForRatios() {
		auto storage = createUpdatedStorage<HMM::Type, HMM::NumDimObs>(obs->storage());

		bool val = true;
		for (size_t i = 0; i < 21; i++) {
			storage[i] = val;
			storage[i] = !value(storage[i]); // value is always opposite of oldValue
			obs->set(i, value(storage[i]));
			if (i % 2 == 0) // jump every 2nd element
				val = !val;
		}
		return storage;
	}
};

TEST_F(THMMBoolInferredTest, constructor) {
	EXPECT_FLOAT_EQ(test->boxOnZ.pi(), 0.4);
	EXPECT_FLOAT_EQ(test->boxOnZ.gamma(), 0.47);

	EXPECT_TRUE(test->pi.isPartOfBox());
	EXPECT_TRUE(test->gamma.isPartOfBox());
}

TEST_F(THMMBoolInferredTest, _fillStationaryDistribution) {
	// check stationary distribution
	EXPECT_FLOAT_EQ(test->boxOnZ.getTau(0)(0, 0), 0.6);
	EXPECT_FLOAT_EQ(test->boxOnZ.getTau(0)(0, 1), 0.4);
	EXPECT_FLOAT_EQ(test->boxOnZ.getTau(0)(1, 0), 0.6);
	EXPECT_FLOAT_EQ(test->boxOnZ.getTau(0)(1, 1), 0.4);
}

TEST_F(THMMBoolInferredTest, calcPOfValueGivenPrevious) {
	auto storage = getStorageForRatios();

	// check calcPOfValueGivenPrevious
	double absError = 10e-05;
	EXPECT_NEAR(test->boxOnZ.calcPOfValueGivenPrevious(obs->storage(), 0), (0.6),
				absError); // value = 0; stationary distr
	EXPECT_NEAR(test->boxOnZ.calcPOfValueGivenPrevious(obs->storage(), 1), (0.2721088),
				absError); // value previous = 0; value this = 1; distGroup = 1
	EXPECT_NEAR(test->boxOnZ.calcPOfValueGivenPrevious(obs->storage(), 2), (0.4613356),
				absError); // value previous = 1; value this = 1; distGroup = 2
	EXPECT_NEAR(test->boxOnZ.calcPOfValueGivenPrevious(obs->storage(), 3), (0.5937299),
				absError); // value previous = 1; value this = 0; distGroup = 3
	EXPECT_NEAR(test->boxOnZ.calcPOfValueGivenPrevious(obs->storage(), 4), (0.6041801),
				absError); // value previous = 0; value this = 0; distGroup = 3
	EXPECT_NEAR(test->boxOnZ.calcPOfValueGivenPrevious(obs->storage(), 5), (0.3999563),
				absError); // value previous = 0; value this = 1; distGroup = 4
	// ...
	EXPECT_NEAR(test->boxOnZ.calcPOfValueGivenPrevious(obs->storage(), 20), (0.6000437),
				absError); // value previous = 0; value this = 0; distGroup = 4

	// check calcPOfOldValueGivenPrevious
	EXPECT_NEAR(test->boxOnZ.calcPOfOldValueGivenPrevious(storage, 0), (0.4),
				absError); // oldValue = 1; stationary distr
	EXPECT_NEAR(test->boxOnZ.calcPOfOldValueGivenPrevious(storage, 1), (0.7278912),
				absError); // value previous = 0; oldValue this = 0; distGroup = 1
	EXPECT_NEAR(test->boxOnZ.calcPOfOldValueGivenPrevious(storage, 2), (0.5386644),
				absError); // value previous = 1; oldValue this = 0; distGroup = 2
	EXPECT_NEAR(test->boxOnZ.calcPOfOldValueGivenPrevious(storage, 3), (0.4062701),
				absError); // value previous = 1; oldValue this = 1; distGroup = 3
	EXPECT_NEAR(test->boxOnZ.calcPOfOldValueGivenPrevious(storage, 4), (0.3958199),
				absError); // value previous = 0; oldValue this = 1; distGroup = 3
	EXPECT_NEAR(test->boxOnZ.calcPOfOldValueGivenPrevious(storage, 5), (0.6000437),
				absError); // value previous = 0; oldValue this = 0; distGroup = 4
	// ...
	EXPECT_NEAR(test->boxOnZ.calcPOfOldValueGivenPrevious(storage, 20), (0.3999563),
				absError); // value previous = 0; oldValue this = 1; distGroup = 4
}

TEST_F(THMMBoolInferredTest, calcLogPOfValueGivenPrevious) {
	auto storage = getStorageForRatios();

	// check calcLogPOfValueGivenPrevious
	double absError = 10e-05;
	EXPECT_NEAR(test->boxOnZ.calcLogPOfValueGivenPrevious(obs->storage(), 0), log(0.6),
				absError); // value = 0; stationary distr
	EXPECT_NEAR(test->boxOnZ.calcLogPOfValueGivenPrevious(obs->storage(), 1), log(0.2721088),
				absError); // value previous = 0; value this = 1; distGroup = 1
	EXPECT_NEAR(test->boxOnZ.calcLogPOfValueGivenPrevious(obs->storage(), 2), log(0.4613356),
				absError); // value previous = 1; value this = 1; distGroup = 2
	EXPECT_NEAR(test->boxOnZ.calcLogPOfValueGivenPrevious(obs->storage(), 3), log(0.5937299),
				absError); // value previous = 1; value this = 0; distGroup = 3
	EXPECT_NEAR(test->boxOnZ.calcLogPOfValueGivenPrevious(obs->storage(), 4), log(0.6041801),
				absError); // value previous = 0; value this = 0; distGroup = 3
	EXPECT_NEAR(test->boxOnZ.calcLogPOfValueGivenPrevious(obs->storage(), 5), log(0.3999563),
				absError); // value previous = 0; value this = 1; distGroup = 4
	// ...
	EXPECT_NEAR(test->boxOnZ.calcLogPOfValueGivenPrevious(obs->storage(), 20), log(0.6000437),
				absError); // value previous = 0; value this = 0; distGroup = 4

	// check calcLogPOfOldValueGivenPrevious
	EXPECT_NEAR(test->boxOnZ.calcLogPOfOldValueGivenPrevious(storage, 0), log(0.4),
				absError); // oldValue = 1; stationary distr
	EXPECT_NEAR(test->boxOnZ.calcLogPOfOldValueGivenPrevious(storage, 1), log(0.7278912),
				absError); // value previous = 0; oldValue this = 0; distGroup = 1
	EXPECT_NEAR(test->boxOnZ.calcLogPOfOldValueGivenPrevious(storage, 2), log(0.5386644),
				absError); // value previous = 1; oldValue this = 0; distGroup = 2
	EXPECT_NEAR(test->boxOnZ.calcLogPOfOldValueGivenPrevious(storage, 3), log(0.4062701),
				absError); // value previous = 1; oldValue this = 1; distGroup = 3
	EXPECT_NEAR(test->boxOnZ.calcLogPOfOldValueGivenPrevious(storage, 4), log(0.3958199),
				absError); // value previous = 0; oldValue this = 1; distGroup = 3
	EXPECT_NEAR(test->boxOnZ.calcLogPOfOldValueGivenPrevious(storage, 5), log(0.6000437),
				absError); // value previous = 0; oldValue this = 0; distGroup = 4
	// ...
	EXPECT_NEAR(test->boxOnZ.calcLogPOfOldValueGivenPrevious(storage, 20), log(0.3999563),
				absError); // value previous = 0; oldValue this = 1; distGroup = 4
}

TEST_F(THMMBoolInferredTest, calcPOfNextGivenValue) {
	auto storage = getStorageForRatios();

	// check calcPOfNextGivenValue
	double absError = 10e-05;
	EXPECT_NEAR(test->boxOnZ.calcPOfNextGivenValue(obs->storage(), 0), (0.2721088),
				absError); // value this = 0; value next = 1, distGroup next = 1
	EXPECT_NEAR(test->boxOnZ.calcPOfNextGivenValue(obs->storage(), 1), (0.4613356),
				absError); // value this = 1; value next = 1, distGroup next = 2
	EXPECT_NEAR(test->boxOnZ.calcPOfNextGivenValue(obs->storage(), 2), (0.5937299),
				absError); // value this = 1; value next = 0,  distGroup next = 3
	EXPECT_NEAR(test->boxOnZ.calcPOfNextGivenValue(obs->storage(), 3), (0.6041801),
				absError); // value this = 0; value next = 0,  distGroup next = 3
	EXPECT_NEAR(test->boxOnZ.calcPOfNextGivenValue(obs->storage(), 4), (0.3999563),
				absError); // value this = 0; value next = 1,  distGroup next = 4
	EXPECT_NEAR(test->boxOnZ.calcPOfNextGivenValue(obs->storage(), 5), (0.4000655),
				absError); // value this = 1; value next = 1,  distGroup next = 4
	// ...
	EXPECT_NEAR(test->boxOnZ.calcPOfNextGivenValue(obs->storage(), 20), 1.,
				absError); // value this = 1; there is no next

	// check calcPOfNextGivenOldValue
	EXPECT_NEAR(test->boxOnZ.calcPOfNextGivenOldValue(storage, 0), (0.5918367),
				absError); // oldValue this = 1; value next = 1; distGroup next = 1
	EXPECT_NEAR(test->boxOnZ.calcPOfNextGivenOldValue(storage, 1), (0.3591096),
				absError); // oldValue this = 0; value next = 1; distGroup next = 2
	EXPECT_NEAR(test->boxOnZ.calcPOfNextGivenOldValue(storage, 2), (0.6041801),
				absError); // oldValue this = 0; value next = 0; distGroup next = 3
	EXPECT_NEAR(test->boxOnZ.calcPOfNextGivenOldValue(storage, 3), (0.5937299),
				absError); // oldValue this = 1; value next = 0; distGroup next = 3
	EXPECT_NEAR(test->boxOnZ.calcPOfNextGivenOldValue(storage, 4), (0.4000655),
				absError); // oldValue this = 1; value next = 1; distGroup next = 4
	EXPECT_NEAR(test->boxOnZ.calcPOfNextGivenOldValue(storage, 5), (0.3999563),
				absError); // oldValue this = 0; value next = 1; distGroup next = 4
	// ...
	EXPECT_NEAR(test->boxOnZ.calcPOfNextGivenOldValue(storage, 20), 1.,
				absError); // oldValue this = 0; there is no next
}

TEST_F(THMMBoolInferredTest, calcLogPOfNextGivenValue) {
	auto storage = getStorageForRatios();

	// check calcLogPOfNextGivenValue
	double absError = 10e-05;
	EXPECT_NEAR(test->boxOnZ.calcLogPOfNextGivenValue(obs->storage(), 0), log(0.2721088),
				absError); // value this = 0; value next = 1, distGroup next = 1
	EXPECT_NEAR(test->boxOnZ.calcLogPOfNextGivenValue(obs->storage(), 1), log(0.4613356),
				absError); // value this = 1; value next = 1, distGroup next = 2
	EXPECT_NEAR(test->boxOnZ.calcLogPOfNextGivenValue(obs->storage(), 2), log(0.5937299),
				absError); // value this = 1; value next = 0,  distGroup next = 3
	EXPECT_NEAR(test->boxOnZ.calcLogPOfNextGivenValue(obs->storage(), 3), log(0.6041801),
				absError); // value this = 0; value next = 0,  distGroup next = 3
	EXPECT_NEAR(test->boxOnZ.calcLogPOfNextGivenValue(obs->storage(), 4), log(0.3999563),
				absError); // value this = 0; value next = 1,  distGroup next = 4
	EXPECT_NEAR(test->boxOnZ.calcLogPOfNextGivenValue(obs->storage(), 5), log(0.4000655),
				absError); // value this = 1; value next = 1,  distGroup next = 4
	// ...
	EXPECT_NEAR(test->boxOnZ.calcLogPOfNextGivenValue(obs->storage(), 20), log(1.),
				absError); // value this = 1; there is no next

	// check calcLogPOfNextGivenOldValue
	EXPECT_NEAR(test->boxOnZ.calcLogPOfNextGivenOldValue(storage, 0), log(0.5918367),
				absError); // oldValue this = 1; value next = 1; distGroup next = 1
	EXPECT_NEAR(test->boxOnZ.calcLogPOfNextGivenOldValue(storage, 1), log(0.3591096),
				absError); // oldValue this = 0; value next = 1; distGroup next = 2
	EXPECT_NEAR(test->boxOnZ.calcLogPOfNextGivenOldValue(storage, 2), log(0.6041801),
				absError); // oldValue this = 0; value next = 0; distGroup next = 3
	EXPECT_NEAR(test->boxOnZ.calcLogPOfNextGivenOldValue(storage, 3), log(0.5937299),
				absError); // oldValue this = 1; value next = 0; distGroup next = 3
	EXPECT_NEAR(test->boxOnZ.calcLogPOfNextGivenOldValue(storage, 4), log(0.4000655),
				absError); // oldValue this = 1; value next = 1; distGroup next = 4
	EXPECT_NEAR(test->boxOnZ.calcLogPOfNextGivenOldValue(storage, 5), log(0.3999563),
				absError); // oldValue this = 0; value next = 1; distGroup next = 4
	// ...
	EXPECT_NEAR(test->boxOnZ.calcLogPOfNextGivenOldValue(storage, 20), log(1.),
				absError); // oldValue this = 0; there is no next
}

TEST_F(THMMBoolInferredTest, getLogDensityRatio) {
	auto storage = getStorageForRatios();

	// correct logPriorDensity?
	double absError = 10e-05;
	EXPECT_NEAR(test->boxOnZ.getLogDensity(obs->storage(), 0), log(0.6) + log(0.2721088), absError);
	EXPECT_NEAR(test->boxOnZ.getLogDensity(obs->storage(), 1), log(0.2721088) + log(0.4613356), absError);
	EXPECT_NEAR(test->boxOnZ.getLogDensity(obs->storage(), 2), log(0.4613356) + log(0.5937299), absError);
	EXPECT_NEAR(test->boxOnZ.getLogDensity(obs->storage(), 3), log(0.5937299) + log(0.6041801), absError);
	EXPECT_NEAR(test->boxOnZ.getLogDensity(obs->storage(), 4), log(0.6041801) + log(0.3999563), absError);
	EXPECT_NEAR(test->boxOnZ.getLogDensity(obs->storage(), 5), log(0.3999563) + log(0.4000655), absError);
	// ...
	EXPECT_NEAR(test->boxOnZ.getLogDensity(obs->storage(), 20), log(0.6000437) + 0., absError);

	// correct priorDensity?
	EXPECT_NEAR(test->boxOnZ.getDensity(obs->storage(), 0), (0.6) * (0.2721088), absError);
	EXPECT_NEAR(test->boxOnZ.getDensity(obs->storage(), 1), (0.2721088) * (0.4613356), absError);
	EXPECT_NEAR(test->boxOnZ.getDensity(obs->storage(), 2), (0.4613356) * (0.5937299), absError);
	EXPECT_NEAR(test->boxOnZ.getDensity(obs->storage(), 3), (0.5937299) * (0.6041801), absError);
	EXPECT_NEAR(test->boxOnZ.getDensity(obs->storage(), 4), (0.6041801) * (0.3999563), absError);
	EXPECT_NEAR(test->boxOnZ.getDensity(obs->storage(), 5), (0.3999563) * (0.4000655), absError);
	// ...
	EXPECT_NEAR(test->boxOnZ.getDensity(obs->storage(), 20), (0.6000437) * 1., absError);

	// correct logPriorRatio?
	EXPECT_NEAR(test->boxOnZ.getLogDensityRatio(storage, 0), log(0.6) - log(0.4) + log(0.2721088) - log(0.5918367),
				absError);
	EXPECT_NEAR(test->boxOnZ.getLogDensityRatio(storage, 1),
				log(0.2721088) - log(0.7278912) + log(0.4613356) - log(0.3591096), absError);
	EXPECT_NEAR(test->boxOnZ.getLogDensityRatio(storage, 2),
				log(0.4613356) - log(0.5386644) + log(0.5937299) - log(0.6041801), absError);
	EXPECT_NEAR(test->boxOnZ.getLogDensityRatio(storage, 3),
				log(0.5937299) - log(0.4062701) + log(0.6041801) - log(0.5937299), absError);
	EXPECT_NEAR(test->boxOnZ.getLogDensityRatio(storage, 4),
				log(0.6041801) - log(0.3958199) + log(0.3999563) - log(0.4000655), absError);
	EXPECT_NEAR(test->boxOnZ.getLogDensityRatio(storage, 5),
				log(0.3999563) - log(0.6000437) + log(0.4000655) - log(0.3999563), absError);
	// ...
	EXPECT_NEAR(test->boxOnZ.getLogDensityRatio(storage, 20), log(0.6000846) - log(0.3999563) + 0. - 0., absError);
}

TEST_F(THMMBoolInferredTest, _updatePi) {
	test->pi.set(0.3);

	// calculate LL
	double absError = 10e-05;
	auto f          = test->boxOnZ.calculateLLRatio(&test->pi, coretools::TRange(0));
	EXPECT_NEAR(f(&obs->storage()), -16.1402 + 15.02808, absError);
}

TEST_F(THMMBoolInferredTest, _updateGamma) {
	test->gamma.set(0.55);

	// calculate LL
	double absError = 10e-05;
	auto f          = test->boxOnZ.calculateLLRatio(&test->gamma, coretools::TRange(0));
	EXPECT_NEAR(f(&obs->storage()), -15.05254 + 15.02808, absError);
}

TEST_F(THMMBoolInferredTest, densities) {
	getStorageForRatios();

	double absError = 10e-05;
	double sum      = test->boxOnZ.getSumLogPriorDensity(obs->storage());
	EXPECT_NEAR(sum, -15.02808, absError);
}

TEST_F(THMMBoolInferredTest, _updateGamma_twoParameterArrays) {
	initializeExtraObs();

	test->gamma.set(0.55);

	// calculate LL
	double absError = 10e-05;
	auto f          = test->boxOnZ.calculateLLRatio(&test->gamma, coretools::TRange(0));
	EXPECT_NEAR(f(&obs->storage()) + f(&obs2->storage()), -29.67367 + 29.61346, absError);
}
