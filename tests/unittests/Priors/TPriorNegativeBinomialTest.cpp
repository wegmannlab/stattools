//
// Created by madleina on 02.05.22.
//

#include "gtest/gtest.h"

#include "stattools/commonTestCases/TNegativeBinomialTest.h"
#include "stattools/commonTestCases/commonTestingFunctions.h"
#include "stattools/Priors/TPriorNegativeBinomial.h"

using namespace testing;
using namespace stattools;
using namespace stattools::prior;

//-------------------------------------------------
// TNegativeBinomialFixed
//-------------------------------------------------

class TNegativeBinomialFixedTest : public Test {
protected:
	using TypeObs                     = coretools::UnsignedInt8;
	static constexpr size_t NumDimObs = 2;
	using BoxType                     = TNegativeBinomialFixed<TObservationBase, TypeObs, NumDimObs>;

	std::shared_ptr<BoxType> boxOnObs;
	std::unique_ptr<TObservation<TypeObs, NumDimObs, BoxType>> obs;

	void SetUp() override {
		instances::dagBuilder().clear();

		boxOnObs = std::make_shared<BoxType>();
		obs      = createObservation<TypeObs, NumDimObs, BoxType>(
			"obs", boxOnObs.get(), {10, 2}, {"out", "0.3"},
			{2, 0, 3, 0, 5, 2, 9, 2, 2, 1, 8, 2, 9, 3, 6, 5, 6, 1, 1, 0});
	}
};

TEST_F(TNegativeBinomialFixedTest, initValid) { EXPECT_EQ(boxOnObs->p(), 0.3); }

TEST_F(TNegativeBinomialFixedTest, invalidP) {
	// p can't be negative
	std::string params = "-1";
	EXPECT_ANY_THROW(boxOnObs->setFixedPriorParameters(params));
	// p can't be 0
	params = "0.";
	EXPECT_ANY_THROW(boxOnObs->setFixedPriorParameters(params));
	// p can't be 1
	params = "1.";
	EXPECT_ANY_THROW(boxOnObs->setFixedPriorParameters(params));
	// p can't be >1
	params = "1.2";
	EXPECT_ANY_THROW(boxOnObs->setFixedPriorParameters(params));
}

TEST_F(TNegativeBinomialFixedTest, noParams) { EXPECT_ANY_THROW(boxOnObs->setFixedPriorParameters("")); }

TEST_F(TNegativeBinomialFixedTest, getDensity) {
	EXPECT_FLOAT_EQ(boxOnObs->getLogDensity(obs->storage(), 0), -2.407946);
	EXPECT_FLOAT_EQ(boxOnObs->getLogDensity(obs->storage(), 4 * 2), -2.071473);
	EXPECT_FLOAT_EQ(boxOnObs->getLogDensity(obs->storage(), 9 * 2), -1.203973);

	EXPECT_FLOAT_EQ(boxOnObs->getDensity(obs->storage(), 0), 0.09);
	EXPECT_FLOAT_EQ(boxOnObs->getDensity(obs->storage(), 4 * 2), exp(-2.071473));
	EXPECT_FLOAT_EQ(boxOnObs->getDensity(obs->storage(), 9 * 2), exp(-1.203973));
}

TEST_F(TNegativeBinomialFixedTest, hastings) {
	auto storage          = createUpdatedStorage<TypeObs, NumDimObs>(obs->storage());
	std::vector<int> newX = {1, 2, 1, 3, 0, 2, 2, 0, 1, 0};
	for (size_t k = 0; k < 10; k++) { storage[k * 2 + 1] = newX[k]; }

	EXPECT_FLOAT_EQ(boxOnObs->getLogDensityRatio(storage, 0), 0.3364722);
	EXPECT_FLOAT_EQ(boxOnObs->getLogDensityRatio(storage, 4 * 2), -0.3364722);
	EXPECT_NEAR(boxOnObs->getLogDensityRatio(storage, 9 * 2), 0., 10e-15); // not exactly the same because of gammaLog
}

//-------------------------------------------------
// TNegativeBinomialInferred
//-------------------------------------------------

class TNegativeBinomialInferredTest : public Test {
public:
	std::unique_ptr<TNegativeBinomialTest> test;

	std::unique_ptr<TNegativeBinomialTest::SpecObs> obs;
	std::unique_ptr<TNegativeBinomialTest::SpecObs> obs2;
	std::unique_ptr<TNegativeBinomialTest::SpecObs> obs3;

	void SetUp() override {
		instances::dagBuilder().clear();

		test = std::make_unique<TNegativeBinomialTest>("out");
		obs  = createObservation<TNegativeBinomialTest::Type, TNegativeBinomialTest::NumDimObs>(
			"obs", &test->boxOnObs, {21, 2}, {"out"}, {2, 0, 3, 1, 5, 0, 9, 2, 2, 0, 8, 0, 9, 2, 6, 3, 6, 1, 0, 0, 2,
														0, 1, 0, 6, 2, 3, 0, 7, 3, 4, 2, 7, 3, 9, 1, 3, 1, 7, 2, 9, 4});

		// set initial values
		test->p.set(0.3);
		test->p.updateTempVals(test->p.getFull(), true);
	}

	void initializeExtraObs() {
		obs2 = createObservation<TNegativeBinomialTest::Type, TNegativeBinomialTest::NumDimObs>(
			"obs2", &test->boxOnObs, {10, 2}, {"out"}, {2, 1, 4, 1, 3, 0, 6, 2, 2, 1, 4, 2, 7, 2, 0, 0, 8, 3, 3, 2});
		obs3 = createObservation<TNegativeBinomialTest::Type, TNegativeBinomialTest::NumDimObs>(
			"obs3", &test->boxOnObs, {50, 2}, {"out"},
			{4, 1, 7, 0, 3, 0, 3, 1, 7, 4, 2, 1, 7, 2, 1, 0, 2, 2, 1, 0, 2, 1, 0, 0, 6, 2, 8, 1, 7, 1, 7, 3, 4, 1,
			 4, 0, 8, 3, 6, 0, 6, 3, 3, 1, 2, 1, 9, 2, 6, 2, 2, 1, 1, 0, 4, 1, 9, 1, 5, 1, 9, 2, 7, 1, 3, 2, 4, 1,
			 1, 1, 0, 0, 7, 4, 1, 0, 4, 0, 6, 1, 9, 3, 4, 1, 4, 1, 1, 1, 7, 3, 4, 1, 5, 1, 2, 1, 2, 1, 5, 2});
	}
};

TEST_F(TNegativeBinomialInferredTest, constructor) {
	EXPECT_EQ(test->boxOnObs.p(), 0.3);
	EXPECT_TRUE(test->p.isPartOfBox());
}

TEST_F(TNegativeBinomialInferredTest, getLogDensity) {
	// correct logPriorRatio and logPriorDensity?
	EXPECT_FLOAT_EQ(test->boxOnObs.getLogDensity(obs->storage(), 0), -2.407946);
	EXPECT_FLOAT_EQ(test->boxOnObs.getLogDensity(obs->storage(), 9 * 2), 0.);
	EXPECT_FLOAT_EQ(test->boxOnObs.getLogDensity(obs->storage(), 19 * 2), -5.808955);
	EXPECT_FLOAT_EQ(test->boxOnObs.getLogDensity(obs->storage(), 20 * 2), -6.057897);

	EXPECT_FLOAT_EQ(test->boxOnObs.getDensity(obs->storage(), 0), 0.09);
	EXPECT_FLOAT_EQ(test->boxOnObs.getDensity(obs->storage(), 9 * 2), 1.);
	EXPECT_FLOAT_EQ(test->boxOnObs.getDensity(obs->storage(), 19 * 2), exp(-5.808955));
	EXPECT_FLOAT_EQ(test->boxOnObs.getDensity(obs->storage(), 20 * 2), exp(-6.057897));

	double sum = test->boxOnObs.getSumLogPriorDensity(obs->storage());
	EXPECT_FLOAT_EQ(sum, -98.0889);
}

TEST_F(TNegativeBinomialInferredTest, getLogDensityRatio) {
	auto storage = createUpdatedStorage<TNegativeBinomialTest::Type, TNegativeBinomialTest::NumDimObs>(obs->storage());

	// update k
	std::vector<uint32_t> newX = {1, 1, 2, 3, 1, 0, 3, 2, 2, 0, 0, 1, 2, 0, 0, 0, 1, 3, 1, 2, 5};
	size_t c                   = 0;
	for (size_t i = 0; i < obs->size(); i += 2, ++c) { storage[i + 1] = newX[c]; }

	EXPECT_FLOAT_EQ(test->boxOnObs.getLogDensityRatio(storage, 0), 0.3364722);
	EXPECT_FLOAT_EQ(test->boxOnObs.getLogDensityRatio(storage, 9 * 2), 0);
	EXPECT_NEAR(test->boxOnObs.getLogDensityRatio(storage, 19 * 2), 0, 10e-15);
	EXPECT_FLOAT_EQ(test->boxOnObs.getLogDensityRatio(storage, 20 * 2), 0.5988365);
}

TEST_F(TNegativeBinomialInferredTest, updateP) {
	test->p.set(0.4);
	test->p.set(0.86);

	// calculate LL
	// equivalent to: sum(dnbinom(x, n, 0.86, log = T)) - sum(dnbinom(x, n, 0.4, log = T)) in R
	EXPECT_FLOAT_EQ(test->boxOnObs.calculateLLRatio(&test->p, 0)(&obs->storage()), 43.37777);
}

TEST_F(TNegativeBinomialInferredTest, _setPToMLE_1param) {
	test->boxOnObs.guessInitialValues();

	// equivalent to: sum(n) / (sum(n) + sum(x))
	EXPECT_FLOAT_EQ(test->p.value(), 0.8);
}

TEST_F(TNegativeBinomialInferredTest, _setPiToMLE_3params) {
	initializeExtraObs();
	test->boxOnObs.guessInitialValues();

	// equivalent to: (sum(n) + sum(n2) + sum(n3)) / (sum(n) + sum(x) + sum(n2) + sum(x2) + sum(n3) + sum(x3)) in R
	EXPECT_FLOAT_EQ(test->p.value(), 0.779661);
}
