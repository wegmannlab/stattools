//
// Created by caduffm on 3/28/22.
//

#include "gtest/gtest.h"

#include "stattools/commonTestCases/commonTestingFunctions.h"
#include "stattools/commonTestCases/TBernoulliTest.h"
#include "stattools/Priors/TPriorBernoulli.h"

using namespace testing;
using namespace stattools;
using namespace stattools::prior;

//--------------------------------------------
// TBernoulliFixed
//--------------------------------------------

struct TBernoulliFixedTest : Test {
	std::string params = "0.44";

	using Type = coretools::Boolean;
	TBernoulliFixed<TParameterBase, Type, 1> prior;

	coretools::TMultiDimensionalStorage<TValueUpdated<Type>, 1> storage;
};

TEST_F(TBernoulliFixedTest, initValid) {
	prior.setFixedPriorParameters(params);
	EXPECT_EQ(prior.pi(), 0.44);
}

TEST_F(TBernoulliFixedTest, initInvalidPi) {
	params = "-0.1";
	EXPECT_ANY_THROW(prior.setFixedPriorParameters(params));
	params = "0.";
	EXPECT_ANY_THROW(prior.setFixedPriorParameters(params));
	params = "1.";
	EXPECT_ANY_THROW(prior.setFixedPriorParameters(params));
	params = "1.1";
	EXPECT_ANY_THROW(prior.setFixedPriorParameters(params));
	params = "";
	EXPECT_ANY_THROW(prior.setFixedPriorParameters(params));
}

TEST_F(TBernoulliFixedTest, hastings) {
	std::string params = "0.4";
	prior.setFixedPriorParameters(params);
	storage.emplace_back(TValueUpdated<Type>(true));
	double logDensityOld = prior.getLogDensity(storage, 0);
	storage[0]           = false; // update

	EXPECT_FLOAT_EQ(prior.getLogDensityRatio(storage, 0), 0.4054651);
	EXPECT_FLOAT_EQ(prior.getLogDensityRatio(storage, 0), prior.getLogDensity(storage, 0) - logDensityOld);

	storage.emplace_back(TValueUpdated<Type>(false));
	logDensityOld = prior.getLogDensity(storage, 1);
	storage[1]    = true; // update
	EXPECT_FLOAT_EQ(prior.getLogDensityRatio(storage, 1), -0.4054651);
	EXPECT_FLOAT_EQ(prior.getLogDensityRatio(storage, 1), prior.getLogDensity(storage, 1) - logDensityOld);

	EXPECT_FLOAT_EQ(prior.getDensity(storage, 0), 0.6);
	EXPECT_FLOAT_EQ(prior.getDensity(storage, 1), 0.4);
}

//--------------------------------------------
// TBernoulliInferred
//--------------------------------------------

class TBernoulliInferredTest : public Test {
public:
	using TypeBern = TBernoulliTest<true, 1>;
	std::unique_ptr<TypeBern> test;

	std::unique_ptr<TypeBern::SpecZ> obs;
	std::unique_ptr<TypeBern::SpecZ> obs2;
	std::unique_ptr<TypeBern::SpecZ> obs3;

	void SetUp() override {
		instances::dagBuilder().clear();

		test = std::make_unique<TypeBern>("out");
		obs  = createObservation<TypeBern::Type, TypeBern::numDimObs>("obs", &test->boxOnZ, {21}, {"out"},
																	  {false, false, false, false, true,  false, false,
																	   false, true,  false, false, false, true,  false,
																	   false, false, false, false, false, false, false});

		// set initial values
		test->pi.set(0.4);
		test->pi.updateTempVals(coretools::TRange(0), true);
	}

	void initializeExtraObs() {
		obs2 = createObservation<TypeBern::Type, TypeBern::numDimObs>(
			"obs2", &test->boxOnZ, {10}, {"out"}, {false, false, true, true, false, true, true, true, true, false});
		obs3 = createObservation<TypeBern::Type, TypeBern::numDimObs>(
			"obs3", &test->boxOnZ, {50}, {"out"},
			{false, false, false, false, false, false, false, true,  false, false, true,  false, false,
			 false, false, false, false, false, false, false, false, false, false, false, false, false,
			 false, false, false, false, false, false, false, false, false, false, false, false, false,
			 false, false, false, false, false, false, false, false, false, false, false});
	}
};

TEST_F(TBernoulliInferredTest, constructor) {
	EXPECT_EQ(test->boxOnZ.pi(), 0.4);
	EXPECT_TRUE(test->pi.isPartOfBox());
}

TEST_F(TBernoulliInferredTest, getDensity) {
	for (size_t i = 0; i < obs->size(); ++i) {
		if (obs->storage()[i] == 0) {
			EXPECT_FLOAT_EQ(test->boxOnZ.getDensity(obs->storage(), i), 0.6);
			EXPECT_FLOAT_EQ(test->boxOnZ.getLogDensity(obs->storage(), i), log(0.6));
		} else {
			EXPECT_FLOAT_EQ(test->boxOnZ.getDensity(obs->storage(), i), 0.4);
			EXPECT_FLOAT_EQ(test->boxOnZ.getLogDensity(obs->storage(), i), log(0.4));
		}
	}

	double sum = test->boxOnZ.getSumLogPriorDensity(obs->storage());
	EXPECT_FLOAT_EQ(sum, -11.94373);
}

TEST_F(TBernoulliInferredTest, getLogDensityRatio) {
	auto storage = createUpdatedStorage<TypeBern ::Type, TypeBern::numDimObs>(obs->storage());
	storage[0]   = true; // update false -> true
	EXPECT_FLOAT_EQ(test->boxOnZ.getLogDensityRatio(storage, 0), -0.4054651);

	storage[4] = false; // update true -> false
	EXPECT_FLOAT_EQ(test->boxOnZ.getLogDensityRatio(storage, 4), 0.4054651);
}

TEST_F(TBernoulliInferredTest, updatePi) {
	test->pi.set(0.86);

	// calculate LL
	// equivalent to: sum(dbinom(vals, 1, 0.86, log = T)) - sum(dbinom(vals, 1, 0.4, log = T)) in R
	auto f = test->boxOnZ.calculateLLRatio(&test->pi, 0);
	EXPECT_FLOAT_EQ(f(&obs->storage()), -23.89877);
}

TEST_F(TBernoulliInferredTest, updatePi_3params) {
	initializeExtraObs();

	test->pi.set(0.86);

	// calculate LL
	// equivalent to: sum(dbinom(c(vals, vals2, vals3), 1, 0.86, log = T)) - sum(dbinom(c(vals, vals2, vals3), 1, 0.4,
	// log = T)) in R
	auto f = test->boxOnZ.calculateLLRatio(&test->pi, 0);
	EXPECT_FLOAT_EQ(f(&obs->storage()) + f(&obs2->storage()) + f(&obs3->storage()), -93.44996);
}

TEST_F(TBernoulliInferredTest, _setPiToMLE_1param) {
	test->boxOnZ.guessInitialValues();

	// equivalent to: mean(vals) in R
	EXPECT_FLOAT_EQ(test->pi.value(), 0.1428571);
}

TEST_F(TBernoulliInferredTest, _setPiToMLE_3params) {
	initializeExtraObs();

	test->boxOnZ.guessInitialValues();

	// equivalent to: mean(c(vals, vals2, vals3)) in R
	EXPECT_FLOAT_EQ(test->pi.value(), 0.1358025);
}
