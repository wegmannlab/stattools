//
// Created by caduffm on 3/28/22.
//

#include "gtest/gtest.h"

#include "stattools/commonTestCases/TNormalTest.h"
#include "stattools/commonTestCases/commonTestingFunctions.h"
#include "stattools/Priors/TPriorNormal.h"

using namespace testing;
using namespace stattools;
using namespace stattools::prior;

//--------------------------------------------
// TNormalFixed
//--------------------------------------------

struct TNormalFixedTest : Test {
	std::string params = "0,1";

	using Type = coretools::Unbounded;
	TNormalFixed<TParameterBase, Type, 1> prior;

	coretools::TMultiDimensionalStorage<TValueUpdated<Type>, 1> storage;
};

TEST_F(TNormalFixedTest, initValid) {
	prior.setFixedPriorParameters(params);
	EXPECT_EQ(prior.mean(), 0);
	EXPECT_EQ(prior.var(), 1);
}

TEST_F(TNormalFixedTest, checkSdVar) {
	params = "0,5";
	prior.setFixedPriorParameters(params);
	EXPECT_EQ(prior.mean(), 0);
	EXPECT_EQ(prior.var(), 5.);
}

TEST_F(TNormalFixedTest, initNegativeVar) {
	params = "0,-1";
	EXPECT_ANY_THROW(prior.setFixedPriorParameters(params));
}

TEST_F(TNormalFixedTest, hastings) {
	params = "0,1";
	prior.setFixedPriorParameters(params);
	storage.emplace_back(TValueUpdated<Type>(-10.));
	double logDensityOld = prior.getLogDensity(storage, 0);
	storage[0]           = 5.; // update
	storage.emplace_back(TValueUpdated<Type>(1.0));

	EXPECT_EQ(prior.getLogDensityRatio(storage, 0), 37.5);
	EXPECT_EQ(prior.getLogDensityRatio(storage, 0), prior.getLogDensity(storage, 0) - logDensityOld);
	EXPECT_FLOAT_EQ(prior.getDensity(storage, 1), 0.2419707);
}

//--------------------------------------------
// TNormalInferred
//--------------------------------------------

class TNormalInferredTest : public Test {
public:
	using NormalTest = TNormalTest<>;
	std::unique_ptr<NormalTest> test;

	std::unique_ptr<NormalTest::SpecObs> obs;
	std::unique_ptr<NormalTest::SpecObs> obs2;
	std::unique_ptr<NormalTest::SpecObs> obs3;

	void SetUp() override {
		instances::dagBuilder().clear();

		test = std::make_unique<NormalTest>("out");
		obs  = createObservation<NormalTest::Type, NormalTest::NumDimObs>(
			"obs", &test->boxOnObs, {21}, {"out"},
			{-10, -9, -8, -7, -6, -5, -4, -3, -2, -1, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10});

		// set initial values
		test->mu.set(0.0);
		test->mu.updateTempVals(test->mu.getFull(), true);
		test->sigma2.set(1.0);
		test->sigma2.updateTempVals(test->sigma2.getFull(), true);
	}

	void initializeExtraObs() {
		obs2 = createObservation<NormalTest::Type, NormalTest::NumDimObs>(
			"obs2", &test->boxOnObs, {10}, {"out"},
			{-0.626453810742332, 0.183643324222082, -0.835628612410047, 1.59528080213779, 0.329507771815361,
			 -0.820468384118015, 0.487429052428485, 0.738324705129217, 0.575781351653492, -0.305388387156356});
		obs3 = createObservation<NormalTest::Type, NormalTest::NumDimObs>(
			"obs3", &test->boxOnObs, {50}, {"out"},
			{151.178116845085,  38.9843236411431,  -62.1240580541804, -221.46998871775,  112.493091814311,
			 -4.49336090152308, -1.61902630989461, 94.3836210685299,  82.1221195098089,  59.3901321217509,
			 91.8977371608218,  78.2136300731067,  7.45649833651906,  -198.935169586337, 61.982574789471,
			 -5.61287395290008, -15.5795506705329, -147.075238389927, -47.815005510862,  41.7941560199702,
			 135.867955152904,  -10.2787727342996, 38.7671611559369,  -5.38050405829051, -137.705955682861,
			 -41.499456329968,  -39.4289953710349, -5.93133967111857, 110.002537198388,  76.3175748457544,
			 -16.4523596253587, -25.3361680136508, 69.6963375404737,  55.6663198673657,  -68.875569454952,
			 -70.749515696212,  36.458196213683,   76.8532924515416,  -11.2346212150228, 88.1107726454215,
			 39.8105880367068,  -61.2026393250771, 34.1119691424425,  -112.936309608079, 143.302370170104,
			 198.039989850586,  -36.7221476466509, -104.413462631653, 56.9719627442413,  -13.5054603880824});
	}
};

TEST_F(TNormalInferredTest, constructor) {
	EXPECT_EQ(test->mu.value(), 0.);
	EXPECT_EQ(test->sigma2.value(), 1.);

	EXPECT_TRUE(test->mu.isPartOfBox());
	EXPECT_TRUE(test->sigma2.isPartOfBox());
}

TEST_F(TNormalInferredTest, getDensity) {
	for (size_t i = 0; i < obs->size(); ++i) {
		EXPECT_FLOAT_EQ(test->boxOnObs.getDensity(obs->storage(), i),
						coretools::probdist::TNormalDistr::density((double)obs->storage()[i], test->mu.value(),
																   sqrt(test->sigma2.value())));
		EXPECT_FLOAT_EQ(test->boxOnObs.getLogDensity(obs->storage(), i),
						coretools::probdist::TNormalDistr::logDensity((double)obs->storage()[i], test->mu.value(),
																	  sqrt(test->sigma2.value())));
	}

	// = sum(dnorm(x, 0, 1, log = T))
	EXPECT_FLOAT_EQ(obs->getSumLogPriorDensity(), -404.2977);
}

TEST_F(TNormalInferredTest, getLogDensityRatio) {
	auto storage = createUpdatedStorage<NormalTest::Type, NormalTest::NumDimObs>(obs->storage());
	storage[0]   = 5.0; // update -10 -> 5

	EXPECT_EQ(test->boxOnObs.getLogDensityRatio(storage, 0), 37.5);
}

TEST_F(TNormalInferredTest, _updateMean) {
	// set two values for hastings ratio
	test->mu.set(2.);
	test->mu.set(2.19);

	// calculate LL
	// equivalent to: sum(dnorm(vals, mean = 2.19, sd = 1, log = T)) - sum(dnorm(vals, mean = 2, sd = 1, log = T))
	// in R
	EXPECT_FLOAT_EQ(test->boxOnObs.calculateLLRatio(&test->mu, 0)(&obs->storage()), -8.35905);
}

TEST_F(TNormalInferredTest, _updateMean_3params) {
	initializeExtraObs();

	// change value of mean, such that oldValue and newValue are not the same
	test->mu.set(2.);
	test->mu.set(2.19);

	// calculate LL
	// equivalent to: sum(dnorm(c(vals, vals2, vals3), mean = 2.19, sd = 1, log = T)) - sum(dnorm(c(vals, vals2,
	// vals3), mean = 2, sd = 1, log = T)) in R
	auto f = test->boxOnObs.calculateLLRatio(&test->mu, 0);
	EXPECT_FLOAT_EQ(f(&obs->storage()) + f(&obs2->storage()) + f(&obs3->storage()), 65.57328);
}

TEST_F(TNormalInferredTest, _updateVar) {
	// change value of var, such that oldValue and newValue are not the same
	test->sigma2.set(1.);
	test->sigma2.set(0.7396);

	// calculate LL
	// equivalent to: sum(dnorm(vals, mean = 0, sd = 0.86^2, log = T)) - sum(dnorm(vals, mean = 0, sd = 1, log = T))
	// in
	// R
	EXPECT_FLOAT_EQ(test->boxOnObs.calculateLLRatio(&test->sigma2, 0)(&obs->storage()), -132.3844);
}

TEST_F(TNormalInferredTest, _updateVar_3params) {
	initializeExtraObs();

	test->sigma2.set(1.);
	test->sigma2.set(0.7396);

	// calculate LL
	// equivalent to: sum(dnorm(c(vals, vals2, vals3), mean = 0, sd = 0.86^2, log = T)) - sum(dnorm(c(vals, vals2,
	// vals3), mean = 0, sd = 1, log = T)) in R
	auto f = test->boxOnObs.calculateLLRatio(&test->sigma2, 0);
	EXPECT_FLOAT_EQ(f(&obs->storage()) + f(&obs2->storage()) + f(&obs3->storage()), -67343.48);
}

TEST_F(TNormalInferredTest, _setMeanToMLE_1param) {
	test->boxOnObs.guessInitialValues();

	// calculate MLE
	// equivalent to: mean(vals) in R
	EXPECT_FLOAT_EQ(test->boxOnObs.mean(), 0.);
}

TEST_F(TNormalInferredTest, _setMeanToMLE_3params) {
	// create extra parameters
	initializeExtraObs();
	test->boxOnObs.guessInitialValues();

	// calculate MLE
	// equivalent to: mean(vals) in R
	EXPECT_FLOAT_EQ(test->boxOnObs.mean(), 6.355772);
}

TEST_F(TNormalInferredTest, _setVarToMLE_1param) {
	test->boxOnObs.guessInitialValues();

	// calculate MLE
	// equivalent to: 1/length(vals) * sum((vals - mean(vals))^2) in R (not exactly the same as var(vals), because
	// var divides by N-1)
	EXPECT_FLOAT_EQ(test->boxOnObs.var(), 36.66667);
}

TEST_F(TNormalInferredTest, _setVarToMLE_3params) {
	initializeExtraObs();

	test->boxOnObs.guessInitialValues();

	// calculate MLE
	// equivalent to: 1/(length(vals) + length(vals2) + length(vals3)) * sum((c(vals, vals2, vals3) - mean(c(vals,
	// vals2, vals3)))^2) in R (not exactly the same as var(c(vals, vals2, vals3)), because var divides by N-1)
	EXPECT_FLOAT_EQ(test->boxOnObs.var(), 4683.227);
}
