//
// Created by madleina on 27.05.22.
//

#include "gtest/gtest.h"

#include "stattools/commonTestCases/THMMCategoricalTest.h"
#include "stattools/commonTestCases/commonTestingFunctions.h"

using namespace testing;
using namespace stattools;
using namespace stattools::prior;

//--------------------------------------------
// THMMCategoricalInferred
//--------------------------------------------

class THMMCategoricalInferredTest : public Test {
public:
	size_t D = 2;

	std::unique_ptr<THMMCategoricalTest> test;

	std::unique_ptr<genometools::TDistancesBinnedBase> distances;
	std::unique_ptr<THMMCategoricalTest::SpecObs> obs;
	std::unique_ptr<THMMCategoricalTest::SpecObs> obs2;

	void SetUp() override {
		instances::dagBuilder().clear();

		distances = createAndFillDistances(21, 16);
		test      = std::make_unique<THMMCategoricalTest>("out", D, distances.get());

		obs = createObservation<THMMCategoricalTest::Type, THMMCategoricalTest::NumDimObs>(
			"obs", &test->boxOnObs, {21}, {"out"}, {2, 0, 1, 0, 0, 1, 0, 0, 1, 2, 0, 0, 1, 0, 1, 1, 0, 2, 1, 1, 2});

		// set initial values
		test->pi.set(0.4);
		test->gamma.set(0.47);
		test->rhos.set(0, 0.3);
		test->rhos.set(1, 0.7);
		test->boxOnObs.updateTau(test->boxOnObs.getParams());
	}

	void initializeExtraObs() {
		obs2 = createObservation<THMMCategoricalTest::Type, THMMCategoricalTest::NumDimObs>(
			"obs2", &test->boxOnObs, {21}, {"out"}, {1, 1, 1, 1, 2, 1, 1, 1, 1, 1, 2, 0, 2, 0, 2, 1, 1, 0, 0, 0, 1});
	}

	auto getStorageForRatios() {
		auto storage = createUpdatedStorage<THMMCategoricalTest::Type, THMMCategoricalTest::NumDimObs>(obs->storage());

		std::vector<size_t> oldVals = {0, 2, 1, 2, 2, 1, 2, 2, 1, 0, 2, 2, 1, 2, 1, 1, 2, 0, 1, 1, 0};
		std::vector<size_t> newVals = {2, 0, 1, 0, 0, 1, 0, 0, 1, 2, 0, 0, 1, 0, 1, 1, 0, 2, 1, 1, 2};
		for (size_t i = 0; i < 21; i++) {
			storage[i] = oldVals[i];
			storage[i] = newVals[i];
			obs->set(i, newVals[i]);
		}

		return storage;
	}
};

TEST_F(THMMCategoricalInferredTest, constructor) {
	EXPECT_FLOAT_EQ(test->boxOnObs.pi(), 0.4);
	EXPECT_FLOAT_EQ(test->boxOnObs.gamma(), 0.47);
	EXPECT_FLOAT_EQ(test->boxOnObs.rho(0), 0.3);
	EXPECT_FLOAT_EQ(test->boxOnObs.rho(1), 0.7);

	EXPECT_TRUE(test->pi.isPartOfBox());
	EXPECT_TRUE(test->gamma.isPartOfBox());
	EXPECT_TRUE(test->rhos.isPartOfBox());
}

TEST_F(THMMCategoricalInferredTest, _fillStationaryDistribution) {
	// check stationary distribution
	for (size_t i = 0; i < 3; i++) {
		EXPECT_FLOAT_EQ(test->boxOnObs.getTau(0)(i, 0), 0.6);
		EXPECT_FLOAT_EQ(test->boxOnObs.getTau(0)(i, 1), 0.12);
		EXPECT_FLOAT_EQ(test->boxOnObs.getTau(0)(i, 2), 0.28);
	}
}

TEST_F(THMMCategoricalInferredTest, calcPOfValueGivenPrevious) {
	auto storage = getStorageForRatios();

	// check calcPOfValueGivenPrevious
	double absError = 10e-05;
	EXPECT_NEAR(test->boxOnObs.calcPOfValueGivenPrevious(obs->storage(), 0), (0.28), absError);
	EXPECT_NEAR(test->boxOnObs.calcPOfValueGivenPrevious(obs->storage(), 1), (0.4081633), absError);
	EXPECT_NEAR(test->boxOnObs.calcPOfValueGivenPrevious(obs->storage(), 2), (0.1077329), absError);
	EXPECT_NEAR(test->boxOnObs.calcPOfValueGivenPrevious(obs->storage(), 3), (0.5937299), absError);
	EXPECT_NEAR(test->boxOnObs.calcPOfValueGivenPrevious(obs->storage(), 4), (0.6041801), absError);
	EXPECT_NEAR(test->boxOnObs.calcPOfValueGivenPrevious(obs->storage(), 5), (0.1199869), absError);
	// ...
	EXPECT_NEAR(test->boxOnObs.calcPOfValueGivenPrevious(obs->storage(), 20), (0.269509), absError);

	// check calcPOfOldValueGivenPrevious
	EXPECT_NEAR(test->boxOnObs.calcPOfOldValueGivenPrevious(storage, 0), (0.6), absError);
	EXPECT_NEAR(test->boxOnObs.calcPOfOldValueGivenPrevious(storage, 1), (0.5918367), absError);
	EXPECT_NEAR(test->boxOnObs.calcPOfOldValueGivenPrevious(storage, 2), (0.1077329), absError);
	EXPECT_NEAR(test->boxOnObs.calcPOfOldValueGivenPrevious(storage, 3), (0.1985064), absError);
	EXPECT_NEAR(test->boxOnObs.calcPOfOldValueGivenPrevious(storage, 4), (0.277074), absError);
	EXPECT_NEAR(test->boxOnObs.calcPOfOldValueGivenPrevious(storage, 5), (0.1199869), absError);
	// ...
	EXPECT_NEAR(test->boxOnObs.calcPOfOldValueGivenPrevious(storage, 20), (0.5999345), absError);
}

TEST_F(THMMCategoricalInferredTest, calcLogPOfValueGivenPrevious) {
	auto storage = getStorageForRatios();

	// check calcLogPOfValueGivenPrevious
	double absError = 10e-05;
	EXPECT_NEAR(test->boxOnObs.calcLogPOfValueGivenPrevious(obs->storage(), 0), log(0.28), absError);
	EXPECT_NEAR(test->boxOnObs.calcLogPOfValueGivenPrevious(obs->storage(), 1), log(0.4081633), absError);
	EXPECT_NEAR(test->boxOnObs.calcLogPOfValueGivenPrevious(obs->storage(), 2), log(0.1077329), absError);
	EXPECT_NEAR(test->boxOnObs.calcLogPOfValueGivenPrevious(obs->storage(), 3), log(0.5937299), absError);
	EXPECT_NEAR(test->boxOnObs.calcLogPOfValueGivenPrevious(obs->storage(), 4), log(0.6041801), absError);
	EXPECT_NEAR(test->boxOnObs.calcLogPOfValueGivenPrevious(obs->storage(), 5), log(0.1199869), absError);
	// ...
	EXPECT_NEAR(test->boxOnObs.calcLogPOfValueGivenPrevious(obs->storage(), 20), log(0.269509), absError);

	// check calcLogPOfOldValueGivenPrevious
	EXPECT_NEAR(test->boxOnObs.calcLogPOfOldValueGivenPrevious(storage, 0), log(0.6), absError);
	EXPECT_NEAR(test->boxOnObs.calcLogPOfOldValueGivenPrevious(storage, 1), log(0.5918367), absError);
	EXPECT_NEAR(test->boxOnObs.calcLogPOfOldValueGivenPrevious(storage, 2), log(0.1077329), absError);
	EXPECT_NEAR(test->boxOnObs.calcLogPOfOldValueGivenPrevious(storage, 3), log(0.1985064), absError);
	EXPECT_NEAR(test->boxOnObs.calcLogPOfOldValueGivenPrevious(storage, 4), log(0.277074), absError);
	EXPECT_NEAR(test->boxOnObs.calcLogPOfOldValueGivenPrevious(storage, 5), log(0.1199869), absError);
	// ...
	EXPECT_NEAR(test->boxOnObs.calcLogPOfOldValueGivenPrevious(storage, 20), log(0.5999345), absError);
}

TEST_F(THMMCategoricalInferredTest, calcPOfNextGivenValue) {
	auto storage = getStorageForRatios();

	// check calcPOfNextGivenValue
	double absError = 10e-05;
	EXPECT_NEAR(test->boxOnObs.calcPOfNextGivenValue(obs->storage(), 0), (0.4081633), absError);
	EXPECT_NEAR(test->boxOnObs.calcPOfNextGivenValue(obs->storage(), 1), (0.1077329), absError);
	EXPECT_NEAR(test->boxOnObs.calcPOfNextGivenValue(obs->storage(), 2), (0.5937299), absError);
	EXPECT_NEAR(test->boxOnObs.calcPOfNextGivenValue(obs->storage(), 3), (0.6041801), absError);
	EXPECT_NEAR(test->boxOnObs.calcPOfNextGivenValue(obs->storage(), 4), (0.1199869), absError);
	EXPECT_NEAR(test->boxOnObs.calcPOfNextGivenValue(obs->storage(), 5), (0.5999345), absError);
	// ...
	EXPECT_NEAR(test->boxOnObs.calcPOfNextGivenValue(obs->storage(), 20), 1., absError);

	// check calcPOfNextGivenOldValue
	EXPECT_NEAR(test->boxOnObs.calcPOfNextGivenOldValue(storage, 0), (0.7278912), absError);
	EXPECT_NEAR(test->boxOnObs.calcPOfNextGivenOldValue(storage, 1), (0.03331945), absError);
	EXPECT_NEAR(test->boxOnObs.calcPOfNextGivenOldValue(storage, 2), (0.5937299), absError);
	EXPECT_NEAR(test->boxOnObs.calcPOfNextGivenOldValue(storage, 3), (0.5937299), absError);
	EXPECT_NEAR(test->boxOnObs.calcPOfNextGivenOldValue(storage, 4), (0.1155038), absError);
	EXPECT_NEAR(test->boxOnObs.calcPOfNextGivenOldValue(storage, 5), (0.5999345), absError);
	// ...
	EXPECT_NEAR(test->boxOnObs.calcPOfNextGivenOldValue(storage, 20), 1., absError);
}

TEST_F(THMMCategoricalInferredTest, calcLogPOfNextGivenValue) {
	auto storage = getStorageForRatios();

	// check calcLogPOfNextGivenValue
	double absError = 10e-05;
	EXPECT_NEAR(test->boxOnObs.calcLogPOfNextGivenValue(obs->storage(), 0), log(0.4081633), absError);
	EXPECT_NEAR(test->boxOnObs.calcLogPOfNextGivenValue(obs->storage(), 1), log(0.1077329), absError);
	EXPECT_NEAR(test->boxOnObs.calcLogPOfNextGivenValue(obs->storage(), 2), log(0.5937299), absError);
	EXPECT_NEAR(test->boxOnObs.calcLogPOfNextGivenValue(obs->storage(), 3), log(0.6041801), absError);
	EXPECT_NEAR(test->boxOnObs.calcLogPOfNextGivenValue(obs->storage(), 4), log(0.1199869), absError);
	EXPECT_NEAR(test->boxOnObs.calcLogPOfNextGivenValue(obs->storage(), 5), log(0.5999345), absError);
	// ...
	EXPECT_NEAR(test->boxOnObs.calcLogPOfNextGivenValue(obs->storage(), 20), 0., absError);

	// check calcLogPOfNextGivenOldValue
	EXPECT_NEAR(test->boxOnObs.calcLogPOfNextGivenOldValue(storage, 0), log(0.7278912), absError);
	EXPECT_NEAR(test->boxOnObs.calcLogPOfNextGivenOldValue(storage, 1), log(0.03331945), absError);
	EXPECT_NEAR(test->boxOnObs.calcLogPOfNextGivenOldValue(storage, 2), log(0.5937299), absError);
	EXPECT_NEAR(test->boxOnObs.calcLogPOfNextGivenOldValue(storage, 3), log(0.5937299), absError);
	EXPECT_NEAR(test->boxOnObs.calcLogPOfNextGivenOldValue(storage, 4), log(0.1155038), absError);
	EXPECT_NEAR(test->boxOnObs.calcLogPOfNextGivenOldValue(storage, 5), log(0.5999345), absError);
	// ...
	EXPECT_NEAR(test->boxOnObs.calcLogPOfNextGivenOldValue(storage, 20), 0., absError);
}

TEST_F(THMMCategoricalInferredTest, getLogDensityRatio) {
	auto storage = getStorageForRatios();

	// correct logPriorDensity?
	double absError = 10e-05;
	EXPECT_NEAR(test->boxOnObs.getLogDensity(obs->storage(), 0), log(0.28) + log(0.4081633), absError);
	EXPECT_NEAR(test->boxOnObs.getLogDensity(obs->storage(), 1), log(0.4081633) + log(0.1077329), absError);
	EXPECT_NEAR(test->boxOnObs.getLogDensity(obs->storage(), 2), log(0.1077329) + log(0.5937299), absError);
	EXPECT_NEAR(test->boxOnObs.getLogDensity(obs->storage(), 3), log(0.5937299) + log(0.6041801), absError);
	EXPECT_NEAR(test->boxOnObs.getLogDensity(obs->storage(), 4), log(0.6041801) + log(0.1199869), absError);
	EXPECT_NEAR(test->boxOnObs.getLogDensity(obs->storage(), 5), log(0.1199869) + log(0.5999345), absError);
	// ...
	EXPECT_NEAR(test->boxOnObs.getLogDensity(obs->storage(), 20), log(0.269509) + 0., absError);

	// correct priorDensity?
	EXPECT_NEAR(test->boxOnObs.getDensity(obs->storage(), 0), (0.28) * (0.4081633), absError);
	EXPECT_NEAR(test->boxOnObs.getDensity(obs->storage(), 1), (0.4081633) * (0.1077329), absError);
	EXPECT_NEAR(test->boxOnObs.getDensity(obs->storage(), 2), (0.1077329) * (0.5937299), absError);
	EXPECT_NEAR(test->boxOnObs.getDensity(obs->storage(), 3), (0.5937299) * (0.6041801), absError);
	EXPECT_NEAR(test->boxOnObs.getDensity(obs->storage(), 4), (0.6041801) * (0.1199869), absError);
	EXPECT_NEAR(test->boxOnObs.getDensity(obs->storage(), 5), (0.1199869) * (0.5999345), absError);
	// ...
	EXPECT_NEAR(test->boxOnObs.getDensity(obs->storage(), 20), (0.269509) * 1., absError);

	// correct logPriorRatio?
	EXPECT_NEAR(test->boxOnObs.getLogDensityRatio(storage, 0), log(0.28) + log(0.4081633) - log(0.6) - log(0.7278912),
				absError);
	EXPECT_NEAR(test->boxOnObs.getLogDensityRatio(storage, 1),
				log(0.4081633) + log(0.1077329) - log(0.5918367) - log(0.03331945), absError);
	EXPECT_NEAR(test->boxOnObs.getLogDensityRatio(storage, 2),
				log(0.1077329) + log(0.5937299) - log(0.1077329) - log(0.5937299), absError);
	EXPECT_NEAR(test->boxOnObs.getLogDensityRatio(storage, 3),
				log(0.5937299) + log(0.6041801) - log(0.1985064) - log(0.5937299), absError);
	EXPECT_NEAR(test->boxOnObs.getLogDensityRatio(storage, 4),
				log(0.6041801) + log(0.1199869) - log(0.277074) - log(0.1155038), absError);
	EXPECT_NEAR(test->boxOnObs.getLogDensityRatio(storage, 5),
				log(0.1199869) + log(0.5999345) - log(0.1199869) - log(0.5999345), absError);
	// ...
	EXPECT_NEAR(test->boxOnObs.getLogDensityRatio(storage, 20), log(0.269509) + 0. - log(0.5999345) - 0., absError);
}

TEST_F(THMMCategoricalInferredTest, _updatePi) {
	// change value of pi, such that oldValue and newValue are not the same
	test->pi.set(0.3);

	// calculate LL
	double absError = 10e-05;
	auto f          = test->boxOnObs.calculateLLRatio(&test->pi, coretools::TRange(0));
	EXPECT_NEAR(f(&obs->storage()), -29.18593 + 27.09483, absError);
}

TEST_F(THMMCategoricalInferredTest, _updateGamma) {
	// change value of gamma, such that oldValue and newValue are not the same
	test->gamma.set(0.55);

	// calculate LL
	double absError = 10e-05;
	auto f          = test->boxOnObs.calculateLLRatio(&test->gamma, coretools::TRange(0));
	EXPECT_NEAR(f(&obs->storage()), -27.16316 + 27.09483, absError);
}

TEST_F(THMMCategoricalInferredTest, _updateRhos_0) {
	// change value of gamma, such that oldValue and newValue are not the same
	test->rhos.set(0, 0.4);
	test->rhos.normalize(test->rhos.getFull());

	// calculate LL
	double absError = 10e-05;
	auto f          = test->boxOnObs.calculateLLRatio(&test->rhos, coretools::TRange(0));
	EXPECT_NEAR(f(&obs->storage()), -25.97787 + 27.09483, absError);
}

TEST_F(THMMCategoricalInferredTest, _updateRhos_1) {
	// change value of gamma, such that oldValue and newValue are not the same
	test->rhos.set(1, 0.6);
	test->rhos.normalize(test->rhos.getFull());

	// calculate LL
	double absError = 10e-05;
	auto f          = test->boxOnObs.calculateLLRatio(&test->rhos, coretools::TRange(1));
	EXPECT_NEAR(f(&obs->storage()), -26.47031 + 27.09483, absError);
}

TEST_F(THMMCategoricalInferredTest, densities) {
	auto storage = getStorageForRatios();

	double absError = 10e-05;
	double sum      = test->boxOnObs.getSumLogPriorDensity(obs->storage());
	EXPECT_NEAR(sum, -27.09483, absError);
}

TEST_F(THMMCategoricalInferredTest, _updateGamma_twoParameterArrays) {
	initializeExtraObs();

	test->gamma.set(0.55);

	// calculate LL
	double absError = 10e-05;
	auto f          = test->boxOnObs.calculateLLRatio(&test->gamma, coretools::TRange(0));
	EXPECT_NEAR(f(&obs->storage()) + f(&obs2->storage()), -56.79116 + 56.91472, absError);
}
