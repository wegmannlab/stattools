#include "gtest/gtest.h"

#include "stattools/commonTestCases/TMultivariateNormalTest.h"
#include "stattools/commonTestCases/commonTestingFunctions.h"
#include "stattools/ParametersObservations/TObservation.h"
#include "stattools/Priors/TPriorMultivariateNormal.h"

using namespace testing;
using namespace stattools;
using namespace stattools::prior;

//--------------------------------------------
// TMultivariateNormalInferred
//--------------------------------------------
static std::string values_mu_1D() { return {"-0.6264538107423327"}; }
static std::string values_mu_5D() {
	return "-0.626453810742332,0.183643324222082,-0.835628612410047,1.59528080213779,0.329507771815361";
}

static std::string values_m() { return "0.2"; }

static std::string values_mrr_1D() { return "0.820468384118015"; }

static std::string values_mrr_5D() {
	return "0.820468384118015,0.487429052428485,0.738324705129217,0.575781351653492,0.305388387156356";
}

static std::string values_mrs_5D() {
	return "1.51178116845085,0.389843236411431,-0.621240580541804,-2.2146998871775,1.12493091814311,-0."
		   "0449336090152309,-0.0161902630989461,0.943836210685299,0.821221195098089,0.593901321217509";
}

static std::vector<double> values_obs_1D() {
	return {0.918977371608218, 1.35867955152904,   -0.164523596253587, 0.398105880367068,  2.40161776050478,
			0.782136300731067, -0.102787727342996, -0.253361680136508, -0.612026393250771, -0.0392400027331692};
}

static std::vector<double> values_obs_5D() {
	return {0.918977371608218,   1.35867955152904,    -0.164523596253587, 0.398105880367068,  2.40161776050478,
			0.782136300731067,   -0.102787727342996,  -0.253361680136508, -0.612026393250771, -0.0392400027331692,
			0.0745649833651906,  0.387671611559369,   0.696963375404737,  0.341119691424425,  0.689739362450777,
			-1.98935169586337,   -0.0538050405829051, 0.556663198673657,  -1.12936309608079,  0.0280021587806661,
			0.61982574789471,    -1.37705955682861,   -0.68875569454952,  1.43302370170104,   -0.743273208882405,
			-0.0561287395290008, -0.41499456329968,   -0.70749515696212,  1.98039989850586,   0.188792299514343,
			-0.155795506705329,  -0.394289953710349,  0.36458196213683,   -0.367221476466509, -1.80495862889104,
			-1.47075238389927,   -0.0593133967111857, 0.768532924515416,  -1.04413462631653,  1.46555486156289,
			-0.47815005510862,   1.10002537198388,    -0.112346212150228, 0.569719627442413,  0.153253338211898,
			0.417941560199702,   0.763175748457544,   0.881107726454215,  -0.135054603880824, 2.17261167036215};
}

static std::vector<double> values_obs2_5D() {
	return {0.475509528899663,   -0.54252003099165,  -0.635736453948977,  0.0601604404345152,  0.450187101272656,
			-0.709946430921815,  1.20786780598317,   -0.461644730360566,  -0.588894486259664,  -0.018559832714638,
			0.610726353489055,   1.16040261569495,   1.43228223854166,    0.531496192632572,   -0.318068374543844,
			-0.934097631644252,  0.700213649514998,  -0.650696353310367,  -1.51839408178679,   -0.929362147453702,
			-1.2536334002391,    1.58683345454085,   -0.207380743601965,  0.306557860789766,   -1.48746031014148,
			0.291446235517463,   0.558486425565304,  -0.392807929441984,  -1.53644982353759,   -1.07519229661568,
			-0.443291873218433,  -1.27659220845804,  -0.319992868548507,  -0.300976126836611,  1.00002880371391,
			0.00110535163162413, -0.573265414236886, -0.279113302976559,  -0.528279904445006,  -0.621266694796823,
			0.0743413241516641,  -1.22461261489836,  0.494188331267827,   -0.652094780680999,  -1.38442684738449,
			-0.589520946188072,  -0.473400636439312, -0.177330482269606,  -0.0568967778473925, 1.86929062242358,
			-0.568668732818502,  -0.620366677224124, -0.505957462114257,  -1.91435942568001,   0.425100377372448,
			-0.135178615123832,  0.0421158731442352, 1.34303882517041,    1.17658331201856,    -0.238647100913033,
			1.1780869965732,     -0.910921648552446, -0.214579408546869,  -1.664972436212,     1.05848304870902,
			-1.52356680042976,   0.158028772404075,  -0.179556530043387,  -0.463530401472386,  0.886422651374936,
			0.593946187628422,   -0.654584643918818, -0.100190741213562,  -1.11592010504285,   -0.619243048231147,
			0.332950371213518,   1.76728726937265,   0.712666307051405,   -0.750819001193448,  2.20610246454047,
			1.06309983727636,    0.716707476017206,  -0.0735644041263263, 2.08716654562835,    -0.255027030141015,
			-0.304183923634301,  0.910174229495227,  -0.0376341714670479, 0.0173956196932517,  -1.42449465021281,
			0.370018809916288,   0.384185357826345,  -0.681660478755657,  -1.28630053043433,   -0.144399601954219,
			0.267098790772231,   1.68217608051942,   -0.324270272246319,  -1.64060553441858,   0.207538339232345};
}

static std::vector<double> values_obs3_5D() {
	return {2.30797839905936,    -1.46725002909224,  1.44115770684428,    0.510108422952926,   -0.630300333928146,
			0.105802367893711,   0.521022742648139,  -1.01584746530465,   -0.164375831769667,  -0.340968579860405,
			0.456998805423414,   -0.158754604716016, 0.411974712317515,   0.420694643254513,   -1.15657236263585,
			-0.077152935356531,  1.4645873119698,    -0.38107605110892,   -0.400246743977644,  1.80314190791747,
			-0.334000842366544,  -0.766081999604665, 0.409401839650934,   -1.37020787754746,   -0.331132036391221,
			-0.0347260283112762, -0.430211753928547, 1.68887328620405,    0.987838267454879,   -1.60551341225308,
			0.787639605630162,   -0.926109497377437, 1.58658843344197,    1.51974502549955,    0.197193438739481,
			2.07524500865228,    -0.17710396143654,  -0.330907800682766,  -0.308740569225614,  0.263175646405474,
			1.02739243876377,    0.402011779486338,  -2.28523553529247,   -1.25328975560769,   -0.985826700409291,
			1.2079083983867,     -0.731748173119606, 2.49766158983416,    0.642241305677824,   -2.88892067167955,
			-1.23132342155804,   0.830373167981674,  0.667066166765493,   -0.0447091368939791, -0.640481702565115,
			0.983895570053379,   -1.20808278630446,  0.5413273359637,     -1.73321840682484,   0.570507635920485,
			0.219924803660651,   -1.04798441280774,  -0.0133995231459087, 0.00213185968026965, -0.05972327604261};
}

class TMultivariateNormalInferredTest : public Test {
public:
	std::unique_ptr<TMultivariateNormalTest> test;

	std::unique_ptr<TMultivariateNormalTest::SpecObs> obs;
	std::unique_ptr<TMultivariateNormalTest::SpecObs> obs2;
	std::unique_ptr<TMultivariateNormalTest::SpecObs> obs3;

	void SetUp() override { instances::dagBuilder().clear(); }

	void initialize(size_t NumDim, size_t Size, const std::vector<double> &ValuesObs) {
		test = std::make_unique<TMultivariateNormalTest>("out");

		// set initial values
		test->m.getDefinition().setInitVal(values_m());
		if (NumDim == 1) {
			test->mu.getDefinition().setInitVal(values_mu_1D());
			test->Mrr.getDefinition().setInitVal(values_mrr_1D());
		} else {
			test->mu.getDefinition().setInitVal(values_mu_5D());
			test->Mrr.getDefinition().setInitVal(values_mrr_5D());
			test->Mrs.getDefinition().setInitVal(values_mrs_5D());
		}

		obs  = createObservation<TMultivariateNormalTest::Type, TMultivariateNormalTest::NumDimObs>(
			"obs", &test->boxOnObs, {Size, NumDim}, {"out"}, ValuesObs);
	}

	void initializeExtraObs() {
		obs2 = createObservation<TMultivariateNormalTest::Type, TMultivariateNormalTest::NumDimObs>(
			"obs2", &test->boxOnObs, {20, 5}, {"out"}, values_obs2_5D());
		obs3 = createObservation<TMultivariateNormalTest::Type, TMultivariateNormalTest::NumDimObs>(
			"obs3", &test->boxOnObs, {13, 5}, {"out"}, values_obs3_5D());
	}
};

TEST_F(TMultivariateNormalInferredTest, numberOfElementsInTriangularMatrix_Diagonal0) {
	size_t dimensions = 1;
	EXPECT_EQ(stattools::prior::impl::mvn::numberOfElementsInTriangularMatrix_Diagonal0(dimensions), 0);
	dimensions = 2;
	EXPECT_EQ(stattools::prior::impl::mvn::numberOfElementsInTriangularMatrix_Diagonal0(dimensions), 1);
	dimensions = 3;
	EXPECT_EQ(stattools::prior::impl::mvn::numberOfElementsInTriangularMatrix_Diagonal0(dimensions), 3);
	dimensions = 4;
	EXPECT_EQ(stattools::prior::impl::mvn::numberOfElementsInTriangularMatrix_Diagonal0(dimensions), 6);
	dimensions = 5;
	EXPECT_EQ(stattools::prior::impl::mvn::numberOfElementsInTriangularMatrix_Diagonal0(dimensions), 10);
}

TEST_F(TMultivariateNormalInferredTest, indexMrs) {
	// example for 5 dimensions
	EXPECT_EQ(stattools::prior::impl::mvn::indexMrs(1, 0), 0);
	EXPECT_EQ(stattools::prior::impl::mvn::indexMrs(2, 0), 1);
	EXPECT_EQ(stattools::prior::impl::mvn::indexMrs(2, 1), 2);
	EXPECT_EQ(stattools::prior::impl::mvn::indexMrs(3, 0), 3);
	EXPECT_EQ(stattools::prior::impl::mvn::indexMrs(3, 1), 4);
	EXPECT_EQ(stattools::prior::impl::mvn::indexMrs(3, 2), 5);
	EXPECT_EQ(stattools::prior::impl::mvn::indexMrs(4, 0), 6);
	EXPECT_EQ(stattools::prior::impl::mvn::indexMrs(4, 1), 7);
	EXPECT_EQ(stattools::prior::impl::mvn::indexMrs(4, 2), 8);
	EXPECT_EQ(stattools::prior::impl::mvn::indexMrs(4, 3), 9);
}

TEST_F(TMultivariateNormalInferredTest, getCoordinatesFromLinearIndex_Mrs) {
	// example for 5 dimensions
	EXPECT_EQ(stattools::prior::impl::mvn::getCoordinatesFromLinearIndex_Mrs(0).first, 1);
	EXPECT_EQ(stattools::prior::impl::mvn::getCoordinatesFromLinearIndex_Mrs(0).second, 0);

	EXPECT_EQ(stattools::prior::impl::mvn::getCoordinatesFromLinearIndex_Mrs(1).first, 2);
	EXPECT_EQ(stattools::prior::impl::mvn::getCoordinatesFromLinearIndex_Mrs(1).second, 0);

	EXPECT_EQ(stattools::prior::impl::mvn::getCoordinatesFromLinearIndex_Mrs(2).first, 2);
	EXPECT_EQ(stattools::prior::impl::mvn::getCoordinatesFromLinearIndex_Mrs(2).second, 1);

	EXPECT_EQ(stattools::prior::impl::mvn::getCoordinatesFromLinearIndex_Mrs(3).first, 3);
	EXPECT_EQ(stattools::prior::impl::mvn::getCoordinatesFromLinearIndex_Mrs(3).second, 0);

	EXPECT_EQ(stattools::prior::impl::mvn::getCoordinatesFromLinearIndex_Mrs(4).first, 3);
	EXPECT_EQ(stattools::prior::impl::mvn::getCoordinatesFromLinearIndex_Mrs(4).second, 1);

	EXPECT_EQ(stattools::prior::impl::mvn::getCoordinatesFromLinearIndex_Mrs(5).first, 3);
	EXPECT_EQ(stattools::prior::impl::mvn::getCoordinatesFromLinearIndex_Mrs(5).second, 2);

	EXPECT_EQ(stattools::prior::impl::mvn::getCoordinatesFromLinearIndex_Mrs(6).first, 4);
	EXPECT_EQ(stattools::prior::impl::mvn::getCoordinatesFromLinearIndex_Mrs(6).second, 0);

	EXPECT_EQ(stattools::prior::impl::mvn::getCoordinatesFromLinearIndex_Mrs(7).first, 4);
	EXPECT_EQ(stattools::prior::impl::mvn::getCoordinatesFromLinearIndex_Mrs(7).second, 1);

	EXPECT_EQ(stattools::prior::impl::mvn::getCoordinatesFromLinearIndex_Mrs(8).first, 4);
	EXPECT_EQ(stattools::prior::impl::mvn::getCoordinatesFromLinearIndex_Mrs(8).second, 2);

	EXPECT_EQ(stattools::prior::impl::mvn::getCoordinatesFromLinearIndex_Mrs(9).first, 4);
	EXPECT_EQ(stattools::prior::impl::mvn::getCoordinatesFromLinearIndex_Mrs(9).second, 3);
}

TEST_F(TMultivariateNormalInferredTest, initParameter_wrongDimensions) {
	EXPECT_ANY_THROW(initialize(9, 10, values_obs_5D())); // this should throw, wrong number of columns
	EXPECT_ANY_THROW(initialize(10, 5, values_obs_5D())); // this should also throw, rows - cols switched
}

TEST_F(TMultivariateNormalInferredTest, getLogDensity_5D) {
	initialize(5, 10, values_obs_5D());

	// we want prior density of first element of first row
	size_t linearIndex = obs->getIndex({0, 0});
	EXPECT_FLOAT_EQ(test->boxOnObs.getLogDensity(obs->storage(), linearIndex), -517.4682);
	EXPECT_FLOAT_EQ(test->boxOnObs.getDensity(obs->storage(), linearIndex), exp(-517.4682));

	// we want prior density of third element of 6th row
	linearIndex = obs->getIndex({5, 2});
	EXPECT_FLOAT_EQ(test->boxOnObs.getLogDensity(obs->storage(), linearIndex), -18.99946);
	EXPECT_NEAR(test->boxOnObs.getDensity(obs->storage(), linearIndex), exp(-18.99946), 0.0001);

	// we want prior density of last element of last row
	linearIndex = obs->getIndex({9, 4});
	EXPECT_FLOAT_EQ(test->boxOnObs.getLogDensity(obs->storage(), linearIndex), -599.2394);
	EXPECT_FLOAT_EQ(test->boxOnObs.getDensity(obs->storage(), linearIndex), exp(-599.2394));
}

TEST_F(TMultivariateNormalInferredTest, getLogDensity_1D) {
	initialize(1, 10, values_obs_1D());

	// we want prior density of first element of first row
	size_t linearIndex = obs->getIndex({0, 0});
	EXPECT_FLOAT_EQ(test->boxOnObs.getLogDensity(obs->storage(), linearIndex), -29.16397);
	EXPECT_FLOAT_EQ(test->boxOnObs.getDensity(obs->storage(), linearIndex), exp(-29.16397));

	// we want prior density of third element of 6th row
	linearIndex = obs->getIndex({2, 0});
	EXPECT_FLOAT_EQ(test->boxOnObs.getLogDensity(obs->storage(), linearIndex), -1.976745);
	EXPECT_FLOAT_EQ(test->boxOnObs.getDensity(obs->storage(), linearIndex), exp(-1.976745));

	// we want prior density of last element of last row
	linearIndex = obs->getIndex({9, 0});
	EXPECT_FLOAT_EQ(test->boxOnObs.getLogDensity(obs->storage(), linearIndex), -3.619751);
	EXPECT_NEAR(test->boxOnObs.getDensity(obs->storage(), linearIndex), exp(-3.619751), 0.0000001);
}

TEST_F(TMultivariateNormalInferredTest, getLogDensityRatio) {
	initialize(5, 10, values_obs_5D());
	auto storage = createUpdatedStorage(obs->storage());

	// we want prior ratio of first element of first row
	size_t linearIndex   = obs->getIndex({0, 0});
	storage[linearIndex] = 0.8;
	EXPECT_FLOAT_EQ(test->boxOnObs.getLogDensityRatio(storage, linearIndex), 14.33759);
	storage[linearIndex].reset();

	// we want prior ratio of third element of 6th row
	linearIndex          = obs->getIndex({5, 2});
	storage[linearIndex] = 0.5;
	EXPECT_FLOAT_EQ(test->boxOnObs.getLogDensityRatio(storage, linearIndex), -5.650851);
	storage[linearIndex].reset();

	// we want prior ratio of last element of last row
	linearIndex          = obs->getIndex({9, 4});
	storage[linearIndex] = -0.5274;
	EXPECT_FLOAT_EQ(test->boxOnObs.getLogDensityRatio(storage, linearIndex), -79.19169);
}

TEST_F(TMultivariateNormalInferredTest, _getLogRatio_vec_1D) {
	initialize(1, 10, values_obs_1D());
	auto storage = createUpdatedStorage(obs->storage());

	// we want prior ratio of first element
	size_t linearIndex   = obs->getIndex({0, 0});
	storage[linearIndex] = 0.8;
	EXPECT_FLOAT_EQ(test->boxOnObs.getLogDensityRatio(storage, linearIndex), 4.419838);
	storage[linearIndex].reset();

	// we want prior ratio of 7th element
	linearIndex          = obs->getIndex({2, 0});
	storage[linearIndex] = 0.5;
	EXPECT_FLOAT_EQ(test->boxOnObs.getLogDensityRatio(storage, linearIndex), -13.19398);
	storage[linearIndex].reset();

	// we want prior ratio of 9th element
	linearIndex          = obs->getIndex({9, 0});
	storage[linearIndex] = -0.5274;
	EXPECT_FLOAT_EQ(test->boxOnObs.getLogDensityRatio(storage, linearIndex), 4.187605);
}

TEST_F(TMultivariateNormalInferredTest, _calcLLUpdateMrr) {
	initialize(5, 10, values_obs_5D());

	// change r=0 of Mrr
	size_t r = 0;
	test->Mrr.set(r, 0.62);
	EXPECT_FLOAT_EQ(test->boxOnObs.calculateLLRatio(&test->Mrr, r)(&obs->storage()), 72.85032);
	test->Mrr.reset(r);

	// change r=2 of Mrr
	r = 2;
	test->Mrr.set(r, 0.9467);
	EXPECT_FLOAT_EQ(test->boxOnObs.calculateLLRatio(&test->Mrr, r)(&obs->storage()), -71.71539);
	test->Mrr.reset(r);

	// change r=4 of Mrr
	r = 4;
	test->Mrr.set(r, 0.01);
	EXPECT_FLOAT_EQ(test->boxOnObs.calculateLLRatio(&test->Mrr, r)(&obs->storage()), -16.61078);
	test->Mrr.reset(r);
}

TEST_F(TMultivariateNormalInferredTest, _calcLLUpdateMrs) {
	initialize(5, 10, values_obs_5D());

	// change r=1, s=0 of Mrs
	size_t r           = 1;
	size_t s           = 0;
	size_t linearIndex = stattools::prior::impl::mvn::indexMrs(r, s);
	test->Mrs.set(linearIndex, 0.62);
	EXPECT_FLOAT_EQ(test->boxOnObs.calculateLLRatio(&test->Mrs, linearIndex)(&obs->storage()), 177.0145);
	test->Mrs.reset(linearIndex);

	// change r=2, s=1 of Mrs
	r           = 2;
	s           = 1;
	linearIndex = stattools::prior::impl::mvn::indexMrs(r, s);
	test->Mrs.set(linearIndex, 0.9467);
	EXPECT_FLOAT_EQ(test->boxOnObs.calculateLLRatio(&test->Mrs, linearIndex)(&obs->storage()), 573.3473);
	test->Mrs.reset(linearIndex);

	// change r=4, s=3 of Mrs
	r           = 4;
	s           = 3;
	linearIndex = stattools::prior::impl::mvn::indexMrs(r, s);
	test->Mrs.set(linearIndex, 0.01);
	EXPECT_FLOAT_EQ(test->boxOnObs.calculateLLRatio(&test->Mrs, linearIndex)(&obs->storage()), 41.36284);
	test->Mrs.reset(linearIndex);
}

TEST_F(TMultivariateNormalInferredTest, _calcLLUpdateMu) {
	initialize(5, 10, values_obs_5D());

	// change r=0 of mu
	size_t r = 0;
	test->mu.set(r, -0.62);
	EXPECT_FLOAT_EQ(test->boxOnObs.calculateLLRatio(&test->mu, r)(&obs->storage()), 5.160112);
	test->mu.reset(r);

	// change r=2 of mu
	r = 2;
	test->mu.set(r, 0.9467);
	EXPECT_FLOAT_EQ(test->boxOnObs.calculateLLRatio(&test->mu, r)(&obs->storage()), 1132.956);
	test->mu.reset(r);

	// change r=4 of mu
	r = 4;
	test->mu.set(r, 0.1);
	EXPECT_FLOAT_EQ(test->boxOnObs.calculateLLRatio(&test->mu, r)(&obs->storage()), 90.74116);
	test->mu.reset(r);
}

TEST_F(TMultivariateNormalInferredTest, _calcLLUpdateMu_1D) {
	initialize(1, 10, values_obs_1D());

	// change r=0 of mu
	size_t r = 0;
	test->mu.set(r, -0.62);

	// calculate LL ratio and logH
	EXPECT_FLOAT_EQ(test->boxOnObs.calculateLLRatio(&test->mu, r)(&obs->storage()), 1.761866);
}

TEST_F(TMultivariateNormalInferredTest, _calcLLUpdateM) {
	initialize(5, 10, values_obs_5D());

	// change m
	test->m.set(0.62);

	// calculate LL ratio
	EXPECT_FLOAT_EQ(test->boxOnObs.calculateLLRatio(&test->m, 0)(&obs->storage()), 3683.704);
}

TEST_F(TMultivariateNormalInferredTest, _calcLLUpdateM_1D) {
	initialize(1, 10, values_obs_1D());

	// change m
	test->m.set(0.62);

	// calculate LL ratio
	EXPECT_FLOAT_EQ(test->boxOnObs.calculateLLRatio(&test->m, 0)(&obs->storage()), 207.1167);
}

TEST_F(TMultivariateNormalInferredTest, guessInitialValues_1param_1D) {
	initialize(1, 10, values_obs_1D());

	test->boxOnObs.guessInitialValues();

	// check mus
	EXPECT_FLOAT_EQ(test->mu.value(0), 0.4687577);

	// check mrr
	EXPECT_FLOAT_EQ(test->Mrr.value(), 1.);

	// check m
	EXPECT_FLOAT_EQ(test->m.value(), 0.8665521445);
}

TEST_F(TMultivariateNormalInferredTest, guessInitialValues_1param_5D) {
	initialize(5, 10, values_obs_5D());

	test->boxOnObs.guessInitialValues();

	// check mus
	std::vector<double> valuesMu = {-0.1336732, 0.1207302, 0.1341367, 0.1434569, 0.4512100};
	for (size_t d = 0; d < 5; d++) { EXPECT_FLOAT_EQ(test->mu.value(d), valuesMu[d]); }

	// check mrr
	std::vector<double> valuesMrr = {1.3583827, 1.8108263, 2.8103980, 1.0447555, 0.8179298};
	for (size_t d = 0; d < 5; d++) { EXPECT_FLOAT_EQ(test->Mrr.value(d), valuesMrr[d]); }

	// check mrs
	std::vector<double> valuesMrs = {-0.250928795376855, 0.773588645448706, -0.199745612798616, -0.36948072030297,
									 0.0979577172098371, 1.17071283517945,  -0.15450849326371,  -0.735381241210143,
									 -0.326766798424084, 0.0858513827738757};
	for (size_t d = 0; d < 10; d++) { EXPECT_FLOAT_EQ(test->Mrs.value(d), valuesMrs[d]); }

	// check m
	EXPECT_FLOAT_EQ(test->m.value(), 1.);
}

TEST_F(TMultivariateNormalInferredTest, guessInitialValues_3params_5D) {
	initialize(5, 10, values_obs_5D());

	// re-set definitions, such that initial values are default
	initializeExtraObs();

	test->boxOnObs.guessInitialValues();

	// check mus
	std::vector<double> valuesMu = {0.11523468, 0.04907407, 0.12323281, -0.22317660, -0.03968175};
	for (size_t d = 0; d < 5; d++) { EXPECT_FLOAT_EQ(test->mu.value(d), valuesMu[d]); }

	// check mrr
	std::vector<double> valuesMrr = {1.1911603, 1.1422352, 1.2554085, 1.0098860, 0.8781874};
	for (size_t d = 0; d < 5; d++) { EXPECT_FLOAT_EQ(test->Mrr.value(d), valuesMrr[d]); }

	// check mrs
	std::vector<double> valuesMrs = {0.298477439029773,   0.0161442097269054, 0.189428554581099,   -0.191076324114716,
									 -0.0366835662693865, -0.335797691798708, -0.0166892047239951, -0.178660559742825,
									 0.0978876330684502,  0.119086121705966};
	for (size_t d = 0; d < 10; d++) { EXPECT_FLOAT_EQ(test->Mrs.value(d), valuesMrs[d]); }

	// check m
	EXPECT_FLOAT_EQ(test->m.value(), 1.);
}
