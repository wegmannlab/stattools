/*
#include "gmock/gmock.h"
#include "gtest/gtest.h"

#include "stattools/commonTestCases/commonTestingFunctions.h"
#include "stattools/Priors/TPriorExponential.h"
#include "stattools/Priors/TPriorNormal.h"
#include "stattools/Priors/TPriorNormalMixedModel.h"
#include "stattools/Priors/TPriorUniform.h"

using namespace testing;
using namespace stattools;
using namespace stattools::prior;
*/

//--------------------------------------------
// TNormalMixedModelInferred
//--------------------------------------------

/*
static std::vector<std::string> values_mu() {
    return {"-0.626453810742332", "0.183643324222082", "-0.835628612410047"};
}

static std::vector<std::string> values_var() { return {"0.218034312889587", "1.44748426873204", "0.614781026707447"}; }

static std::vector<std::string> prior_on_var() { return {"10.0", "2.0", "5.0"}; }

static std::vector<size_t> values_z() { return {1, 1, 1, 2, 0, 1, 2, 0, 1, 0}; }

static std::vector<double> values_obs() {
    return {0.129583051999254, 0.164164585485749,  1.3191862641652,   -0.115077080828884, -0.916536785000671,
            1.17166637278492,  -0.222371391058945, -1.66058897590131, 0.898174558239648,  -0.101176942503107};
}

static std::vector<double> values_obs2() {
    return {0.727593017742038, 0.540824712254107, 0.233559724874794, 0.685175339691341,  0.740851946175098,
            1.68942432384938,  0.307546445168555, -1.55074018798769, -0.820806634612381, -1.3951880345121};
}

static std::vector<double> values_obs_initialization() {
    return {-8.02583477180451, 18.887226106599,   0.898389684967697, 0.0617862704675645, -7.60173669829965,
            -6.69050672557205, 18.8492070999928,  -9.25512959435582, 16.9119397853501,   16.9305704627186,
            15.882783762645,   18.2583688304294,  16.7017449834384,  0.660797792486846,  16.3361033436377,
            15.6277754798066,  0.944675268605351, -8.07167953811586, 15.0669516657945,   16.0607126064133,
            16.9205185910687,  0.37212389963679,  18.4351142332889,  19.3484542286023,   18.5880925413221,
            16.0298728744965,  0.2655086631421,   0.572853363351896, 17.4884962104261,   19.9595304741524,
            0.908207789994776, 19.6735261555295,  16.9001758971717,  0.62911404389888,   0.201681931037456};
}

class TNormalMixedModelInferredTest : public Test {
public:
    constexpr static size_t K = 3;

    using TypeObs                     = coretools::Unbounded;
    static constexpr size_t NumDimObs = 1;
    using SpecZ                       = ParamSpec<coretools::UnsignedInt8WithMax<0>, coretools::toHash("z"), 1>;
    using SpecMeans                   = ParamSpec<coretools::Unbounded, coretools::toHash("means"), 1>;
    using SpecVars                    = ParamSpec<coretools::StrictlyPositive, coretools::toHash("vars"), 1>;

    using BoxType    = TNormalMixedModelInferred<TObservationBase, TypeObs, NumDimObs, SpecZ, SpecMeans, SpecVars, K>;
    using TypeParamZ = TParameter<SpecZ, BoxType>;
    using TypeParamMeans = TParameter<SpecMeans, BoxType>;
    using TypeParamVars  = TParameter<SpecVars, BoxType>;

    std::unique_ptr<TypeParamZ> z;
    std::array<std::unique_ptr<TypeParamMeans>, K> means;
    std::array<std::unique_ptr<TypeParamVars>, K> vars;

    std::shared_ptr<BoxType> prior;
    std::unique_ptr<TObservation<TypeObs, NumDimObs>> obs;
    std::unique_ptr<TObservation<TypeObs, NumDimObs>> obs2;

    void initialize(size_t Size, const std::vector<double> &ValuesObs,
                    const std::vector<size_t> &ValuesZ = values_z()) {
        using namespace coretools::str;

        SpecZ::value_type::setMax(2);

        // initialize pointers
        for (size_t i = 0; i < K; ++i) {
            means[i] = createParameter<TNormalFixed, SpecMeans, BoxType>("mean_" + toString(i),
                                                                         {"out", "0.0,1.0", values_mu()[i]});

            vars[i] = createParameter<TExponentialFixed, SpecVars, BoxType>(
                "var_" + toString(i), {"out", prior_on_var()[i], values_var()[i]});
        }
        std::array<TypeParamMeans *, K> arrayMeans = {means[0].get(), means[1].get(), means[2].get()};
        std::array<TypeParamVars *, K> arrayVars   = {vars[0].get(), vars[1].get(), vars[2].get()};

        // create z
        TParameterDefinition defZ("out", "");
        if (!ValuesZ.empty()) { defZ.setInitVal(coretools::str::concatenateString(ValuesZ, ",")); }
        z = createParameter<TUniformFixed, SpecZ, BoxType>("z", defZ);

        prior = createPrior<BoxType>(z.get(), arrayMeans, arrayVars);

        obs = createObservation<TypeObs, NumDimObs>("obs", prior, {Size}, {"out"}, ValuesObs);
    }

    void initializeExtraObs() {
        obs2 = createObservation<TypeObs, NumDimObs>("obs2", prior, {10}, {"out"}, values_obs2());
    }
};

TEST_F(TNormalMixedModelInferredTest, initParameter_wrongDimensions) {
    EXPECT_ANY_THROW(initialize(11, values_obs())); // this throws - wrong size of parameter compared to z
}

TEST_F(TNormalMixedModelInferredTest, getLogDensity) {
    initialize(10, values_obs());

    for (size_t i = 0; i < obs->size(); ++i) {
        EXPECT_FLOAT_EQ(test->boxOnObs.getDensity(obs->storage(), i),
                        coretools::probdist::TNormalDistr::density(
                            (double)obs->storage()[i], means[z->value(i)]->value(), sqrt(vars[z->value(i)]->value())));
        EXPECT_FLOAT_EQ(test->boxOnObs.getLogDensity(obs->storage(), i),
                        coretools::probdist::TNormalDistr::logDensity(
                            (double)obs->storage()[i], means[z->value(i)]->value(), sqrt(vars[z->value(i)]->value())));
    }

    // calculate sum of all log prior densities
    EXPECT_FLOAT_EQ(test->boxOnObs.getSumLogPriorDensity(obs->storage()), -12.30921);
}

TEST_F(TNormalMixedModelInferredTest, getLogDensityRatio) {
    initialize(10, values_obs());

    auto storage = createUpdatedStorage<TypeObs, NumDimObs>(obs->storage());

    storage[0] = -1.1;
    EXPECT_FLOAT_EQ(test->boxOnObs.getLogDensityRatio(storage, 0), -0.5681642648);
    storage[0].reset();

    storage[3] = 0.125;
    EXPECT_FLOAT_EQ(test->boxOnObs.getLogDensityRatio(storage, 3), -0.3282573825);
    storage[3].reset();

    storage[9] = 0.48;
    EXPECT_FLOAT_EQ(test->boxOnObs.getLogDensityRatio(storage, 9), -2.174713316);
    storage[9].reset();
}

TEST_F(TNormalMixedModelInferredTest, calcLLUpdateMean0) {
    initialize(10, values_obs());

    means[0]->set(-0.8);

    // calculate LL ratio
    EXPECT_FLOAT_EQ(test->boxOnObs.calcLLRatioMean(0)(&obs->storage()), 0.4287206);
    // calculate logH
    EXPECT_FLOAT_EQ(test->boxOnObs.calculateLogH(test->boxOnObs.calcLLRatioMean(0), means[0].get()), 0.4287206 - 0.1237778);
}

TEST_F(TNormalMixedModelInferredTest, _calcLLUpdateMean1) {
    initialize(10, values_obs());

    means[1]->set(0.5);

    // calculate LL ratio
    EXPECT_FLOAT_EQ(test->boxOnObs.calcLLRatioMean(1)(&obs->storage()), 0.4313570748);
    // calculate logH
    EXPECT_FLOAT_EQ(test->boxOnObs.calculateLogH(test->boxOnObs.calcLLRatioMean(1), means[1].get()), 0.4313570748 - 0.1081375647);
}

TEST_F(TNormalMixedModelInferredTest, _calcLLUpdateMean2) {
    initialize(10, values_obs());

    means[2]->set(0.26);

    // calculate LL ratio
    EXPECT_FLOAT_EQ(test->boxOnObs.calcLLRatioMean(2)(&obs->storage()), 0.4244714224);
    // calculate logH
    EXPECT_FLOAT_EQ(test->boxOnObs.calculateLogH(test->boxOnObs.calcLLRatioMean(2), means[2].get()), 0.4244714224 + 0.3153376);
}

TEST_F(TNormalMixedModelInferredTest, _calcLLUpdateVar0) {
    initialize(10, values_obs());

    vars[0]->set(0.5);

    // calculate LL ratio
    EXPECT_FLOAT_EQ(test->boxOnObs.calcLLRatioVar(0)(&obs->storage()), 0.6037196);
    // calculate logH
    EXPECT_FLOAT_EQ(test->boxOnObs.calculateLogH(test->boxOnObs.calcLLRatioVar(0), vars[0].get()), 0.6037196 - 2.819657);
}

TEST_F(TNormalMixedModelInferredTest, _calcLLUpdateVar1) {
    initialize(10, values_obs());

    vars[1]->set(0.77);

    // calculate LL ratio
    EXPECT_FLOAT_EQ(test->boxOnObs.calcLLRatioVar(1)(&obs->storage()), 0.7332222424);
    // calculate logH
    EXPECT_FLOAT_EQ(test->boxOnObs.calculateLogH(test->boxOnObs.calcLLRatioVar(1), vars[1].get()), 0.7332222424 + 1.354968537);
}

TEST_F(TNormalMixedModelInferredTest, _calcLLUpdateVar2) {
    initialize(10, values_obs());

    vars[2]->set(0.063);

    // calculate LL ratio
    EXPECT_FLOAT_EQ(test->boxOnObs.calcLLRatioVar(2)(&obs->storage()), -4.099128602);
    // calculate logH
    EXPECT_FLOAT_EQ(test->boxOnObs.calculateLogH(test->boxOnObs.calcLLRatioVar(2), vars[2].get()), -4.099128602 + 2.758905134);
}

TEST_F(TNormalMixedModelInferredTest, _sampleZ) {
    initialize(10, values_obs());

    // n=0 of z: check if sampling from posterior results approximately in posterior probabilities
    size_t n                     = 0;
    size_t numRep                = 10000;
    std::array<size_t, 3> counts = {0, 0, 0};
    for (size_t i = 0; i < numRep; i++) {
        test->boxOnObs.sampleZ(n);
        counts[z->value(n)]++;
    }

    EXPECT_NEAR((double)counts[0] / (double)numRep, 0.2878, 0.01);
    EXPECT_NEAR((double)counts[1] / (double)numRep, 0.4140, 0.01);
    EXPECT_NEAR((double)counts[2] / (double)numRep, 0.2980, 0.01);
}

TEST_F(TNormalMixedModelInferredTest, guessInitialValues) {
    // simulations and true values based on R script 'simulateHMM.R'
    initialize(35, values_obs_initialization(), {0, 1, 2, 2, 0, 0, 1, 0, 1, 1, 1, 1, 1, 2, 1, 1, 2, 0,
                                                 1, 1, 1, 2, 1, 1, 1, 1, 2, 2, 1, 1, 2, 1, 1, 2, 2});

    test->boxOnObs.guessInitialValues();

    std::vector<double> expectedMeans = {-7.928977, 17.44286, 0.5515139};
    std::vector<double> expectedVars  = {0.6858642, 2.006703, 0.08967447};

    EXPECT_FLOAT_EQ(means[0]->value(), expectedMeans[0]);
    EXPECT_FLOAT_EQ(means[1]->value(), expectedMeans[1]);
    EXPECT_FLOAT_EQ(means[2]->value(), expectedMeans[2]);
    EXPECT_FLOAT_EQ(vars[0]->value(), expectedVars[0]);
    EXPECT_FLOAT_EQ(vars[1]->value(), expectedVars[1]);
    EXPECT_FLOAT_EQ(vars[2]->value(), expectedVars[2]);

    std::vector<size_t> expectedZ = {0, 1, 2, 2, 0, 0, 1, 0, 1, 1, 1, 1, 1, 2, 1, 1, 2, 0,
                                     1, 1, 1, 2, 1, 1, 1, 1, 2, 2, 1, 1, 2, 1, 1, 2, 2};
    for (size_t i = 0; i < z->size(); i++) { EXPECT_EQ(z->value(i), expectedZ[i]); }
}

 */
