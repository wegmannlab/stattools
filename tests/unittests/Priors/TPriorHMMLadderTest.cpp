#include "gtest/gtest.h"

#include "stattools/commonTestCases/THMMLadderTest.h"
#include "stattools/commonTestCases/commonTestingFunctions.h"

using namespace testing;
using namespace stattools;
using namespace stattools::prior;

//--------------------------------------------
// THMMLadderInferred
//--------------------------------------------

class THMMLadderInferredTest : public Test {
public:
	size_t numStates = 3;

	std::unique_ptr<THMMLadderTest> test;

	std::unique_ptr<genometools::TDistancesBinnedBase> distances;
	std::unique_ptr<THMMLadderTest::SpecObs> obs;
	std::unique_ptr<THMMLadderTest::SpecObs> obs2;

	void SetUp() override {
		instances::dagBuilder().clear();

		distances = createAndFillDistances(21, 16);
		test      = std::make_unique<THMMLadderTest>("out", numStates, distances.get());

		obs = createObservation<THMMLadderTest::Type, THMMLadderTest::NumDimObs>(
			"obs", &test->boxOnObs, {21}, {"out"}, {0, 0, 0, 0, 2, 0, 2, 2, 2, 2, 0, 0, 0, 1, 1, 2, 1, 2, 2, 1, 2});

		// set initial values
		test->kappa.set(0.2);
		test->boxOnObs.updateTau(test->boxOnObs.getParams());
	}

	auto getStorageForRatios() {
		auto storage = createUpdatedStorage<THMMLadderTest::Type, THMMLadderTest::NumDimObs>(obs->storage());

		std::vector<size_t> oldVals = {1, 1, 1, 1, 0, 1, 0, 0, 0, 0, 1, 1, 1, 2, 2, 0, 2, 0, 0, 2, 0};
		std::vector<size_t> newVals = {0, 0, 0, 0, 2, 0, 2, 2, 2, 2, 0, 0, 0, 1, 1, 2, 1, 2, 2, 1, 2};
		for (size_t i = 0; i < 21; i++) {
			storage[i] = oldVals[i];
			storage[i] = newVals[i];
			obs->set(i, newVals[i]);
		}

		return storage;
	}
};

TEST_F(THMMLadderInferredTest, constructor) {
	EXPECT_EQ(test->boxOnObs.kappa(), 0.2);

	EXPECT_TRUE(test->kappa.isPartOfBox());

	// check for max
	EXPECT_EQ(obs->max(), numStates - 1);
}

TEST_F(THMMLadderInferredTest, constructor_ThrowIfMaxDoesntMatch) {
	THMMLadderTest::Type::setMax(1);
	EXPECT_ANY_THROW(obs->tellBoxAboveToInitStorage());
}

TEST_F(THMMLadderInferredTest, _fillStationaryDistribution) {
	// check stationary distribution
	EXPECT_FLOAT_EQ(test->boxOnObs.getTau(0)(0, 0), 0.3333333);
	EXPECT_FLOAT_EQ(test->boxOnObs.getTau(0)(0, 1), 0.3333333);
	EXPECT_FLOAT_EQ(test->boxOnObs.getTau(0)(0, 2), 0.3333333);
	EXPECT_FLOAT_EQ(test->boxOnObs.getTau(0)(1, 0), 0.3333333);
	EXPECT_FLOAT_EQ(test->boxOnObs.getTau(0)(1, 1), 0.3333333);
	EXPECT_FLOAT_EQ(test->boxOnObs.getTau(0)(1, 2), 0.3333333);
	EXPECT_FLOAT_EQ(test->boxOnObs.getTau(0)(2, 0), 0.3333333);
	EXPECT_FLOAT_EQ(test->boxOnObs.getTau(0)(2, 1), 0.3333333);
	EXPECT_FLOAT_EQ(test->boxOnObs.getTau(0)(2, 2), 0.3333333);
}

TEST_F(THMMLadderInferredTest, calcPOfValueGivenPrevious) {
	auto storage = getStorageForRatios();

	// check calcLogPOfValueGivenPrevious
	EXPECT_NEAR(test->boxOnObs.calcPOfValueGivenPrevious(obs->storage(), 0), (0.3333333),
				0.01); // value = 0; stationary distr
	EXPECT_NEAR(test->boxOnObs.calcPOfValueGivenPrevious(obs->storage(), 1), (0.8341673),
				0.01); // value previous = 0; value this = 0; distGroup = 1
	EXPECT_NEAR(test->boxOnObs.calcPOfValueGivenPrevious(obs->storage(), 2), (0.7186924),
				0.01); // value previous = 0; value this = 0; distGroup = 2
	EXPECT_NEAR(test->boxOnObs.calcPOfValueGivenPrevious(obs->storage(), 3), (0.5731175),
				0.01); // value previous = 0; value this = 0; distGroup = 3
	EXPECT_NEAR(test->boxOnObs.calcPOfValueGivenPrevious(obs->storage(), 4), (0.1237885),
				0.01); // value previous = 0; value this = 2; distGroup = 3
	EXPECT_NEAR(test->boxOnObs.calcPOfValueGivenPrevious(obs->storage(), 5), (0.2337567),
				0.01); // value previous = 2; value this = 0; distGroup = 4
	// ...
	EXPECT_NEAR(test->boxOnObs.calcPOfValueGivenPrevious(obs->storage(), 20), (0.3305901),
				0.01); // value previous = 1; value this = 2; distGroup = 4

	// check calcLogPOfOldValueGivenPrevious
	EXPECT_NEAR(test->boxOnObs.calcPOfOldValueGivenPrevious(storage, 0), (0.3333333),
				0.01); // oldValue = 0; stationary distr
	EXPECT_NEAR(test->boxOnObs.calcPOfOldValueGivenPrevious(storage, 1), (0.1503961),
				0.01); // value previous = 0; oldValue this = 1; distGroup = 1
	EXPECT_NEAR(test->boxOnObs.calcPOfOldValueGivenPrevious(storage, 2), (0.2329353),
				0.01); // value previous = 0; oldValue this = 1; distGroup = 2
	EXPECT_NEAR(test->boxOnObs.calcPOfOldValueGivenPrevious(storage, 3), (0.303094),
				0.01); // value previous = 0; oldValue this = 1; distGroup = 3
	EXPECT_NEAR(test->boxOnObs.calcPOfOldValueGivenPrevious(storage, 4), (0.5731175),
				0.01); // value previous = 0; oldValue this = 0; distGroup = 3
	EXPECT_NEAR(test->boxOnObs.calcPOfOldValueGivenPrevious(storage, 5), (0.3305901),
				0.01); // value previous = 2; oldValue this = 1; distGroup = 4
	// ...
	EXPECT_NEAR(test->boxOnObs.calcPOfOldValueGivenPrevious(storage, 20), (0.3305901),
				0.01); // value previous = 1; oldValue this = 0; distGroup = 4
}

TEST_F(THMMLadderInferredTest, calcLogPOfValueGivenPrevious) {
	auto storage = getStorageForRatios();

	// check calcLogPOfValueGivenPrevious
	EXPECT_NEAR(test->boxOnObs.calcLogPOfValueGivenPrevious(obs->storage(), 0), log(0.3333333),
				0.01); // value = 0; stationary distr
	EXPECT_NEAR(test->boxOnObs.calcLogPOfValueGivenPrevious(obs->storage(), 1), log(0.8341673),
				0.01); // value previous = 0; value this = 0; distGroup = 1
	EXPECT_NEAR(test->boxOnObs.calcLogPOfValueGivenPrevious(obs->storage(), 2), log(0.7186924),
				0.01); // value previous = 0; value this = 0; distGroup = 2
	EXPECT_NEAR(test->boxOnObs.calcLogPOfValueGivenPrevious(obs->storage(), 3), log(0.5731175),
				0.01); // value previous = 0; value this = 0; distGroup = 3
	EXPECT_NEAR(test->boxOnObs.calcLogPOfValueGivenPrevious(obs->storage(), 4), log(0.1237885),
				0.01); // value previous = 0; value this = 2; distGroup = 3
	EXPECT_NEAR(test->boxOnObs.calcLogPOfValueGivenPrevious(obs->storage(), 5), log(0.2337567),
				0.01); // value previous = 2; value this = 0; distGroup = 4
	// ...
	EXPECT_NEAR(test->boxOnObs.calcLogPOfValueGivenPrevious(obs->storage(), 20), log(0.3305901),
				0.01); // value previous = 1; value this = 2; distGroup = 4

	// check calcLogPOfOldValueGivenPrevious
	EXPECT_NEAR(test->boxOnObs.calcLogPOfOldValueGivenPrevious(storage, 0), log(0.3333333),
				0.01); // oldValue = 0; stationary distr
	EXPECT_NEAR(test->boxOnObs.calcLogPOfOldValueGivenPrevious(storage, 1), log(0.1503961),
				0.01); // value previous = 0; oldValue this = 1; distGroup = 1
	EXPECT_NEAR(test->boxOnObs.calcLogPOfOldValueGivenPrevious(storage, 2), log(0.2329353),
				0.01); // value previous = 0; oldValue this = 1; distGroup = 2
	EXPECT_NEAR(test->boxOnObs.calcLogPOfOldValueGivenPrevious(storage, 3), log(0.303094),
				0.01); // value previous = 0; oldValue this = 1; distGroup = 3
	EXPECT_NEAR(test->boxOnObs.calcLogPOfOldValueGivenPrevious(storage, 4), log(0.5731175),
				0.01); // value previous = 0; oldValue this = 0; distGroup = 3
	EXPECT_NEAR(test->boxOnObs.calcLogPOfOldValueGivenPrevious(storage, 5), log(0.3305901),
				0.01); // value previous = 2; oldValue this = 1; distGroup = 4
	// ...
	EXPECT_NEAR(test->boxOnObs.calcLogPOfOldValueGivenPrevious(storage, 20), log(0.3305901),
				0.01); // value previous = 1; oldValue this = 0; distGroup = 4
}

TEST_F(THMMLadderInferredTest, calcPOfNextGivenValue) {
	auto storage = getStorageForRatios();

	// check calcLogPOfNextGivenValue
	EXPECT_NEAR(test->boxOnObs.calcPOfNextGivenValue(obs->storage(), 0), (0.8341673),
				0.01); // value this = 0; value next = 0, distGroup next = 1
	EXPECT_NEAR(test->boxOnObs.calcPOfNextGivenValue(obs->storage(), 1), (0.7186924),
				0.01); // value this = 0; value next = 0, distGroup next = 2
	EXPECT_NEAR(test->boxOnObs.calcPOfNextGivenValue(obs->storage(), 2), (0.5731175),
				0.01); // value this = 0; value next = 0,  distGroup next = 3
	EXPECT_NEAR(test->boxOnObs.calcPOfNextGivenValue(obs->storage(), 3), (0.1237885),
				0.01); // value this = 0; value next = 2,  distGroup next = 3
	EXPECT_NEAR(test->boxOnObs.calcPOfNextGivenValue(obs->storage(), 4), (0.2337567),
				0.01); // value this = 2; value next = 0,  distGroup next = 4
	EXPECT_NEAR(test->boxOnObs.calcPOfNextGivenValue(obs->storage(), 5), (0.2337567),
				0.01); // value this = 0; value next = 2,  distGroup next = 4
	// ...
	EXPECT_NEAR(test->boxOnObs.calcPOfNextGivenValue(obs->storage(), 20), 1., 0.01); // value this = 1; there is no next

	// check calcLogPOfNextGivenOldValue
	EXPECT_NEAR(test->boxOnObs.calcPOfNextGivenOldValue(storage, 0), (0.1503961),
				0.01); // oldValue this = 0; value next = 0; distGroup next = 1
	EXPECT_NEAR(test->boxOnObs.calcPOfNextGivenOldValue(storage, 1), (0.2329353),
				0.01); // oldValue this = 0; value next = 0; distGroup next = 2
	EXPECT_NEAR(test->boxOnObs.calcPOfNextGivenOldValue(storage, 2), (0.303094),
				0.01); // oldValue this = 0; value next = 2; distGroup next = 3
	EXPECT_NEAR(test->boxOnObs.calcPOfNextGivenOldValue(storage, 3), (0.303094),
				0.01); // oldValue this = 0; value next = 2; distGroup next = 3
	EXPECT_NEAR(test->boxOnObs.calcPOfNextGivenOldValue(storage, 4), (0.4356532),
				0.01); // oldValue this = 2; value next = 0; distGroup next = 4
	EXPECT_NEAR(test->boxOnObs.calcPOfNextGivenOldValue(storage, 5), (0.3305901),
				0.01); // oldValue this = 0; value next = 2; distGroup next = 4
	// ...
	EXPECT_NEAR(test->boxOnObs.calcPOfNextGivenOldValue(storage, 20), 1.,
				0.01); // oldValue this = 0; there is no next
}

TEST_F(THMMLadderInferredTest, calcLogPOfNextGivenValue) {
	auto storage = getStorageForRatios();

	// check calcLogPOfNextGivenValue
	EXPECT_NEAR(test->boxOnObs.calcLogPOfNextGivenValue(obs->storage(), 0), log(0.8341673),
				0.01); // value this = 0; value next = 0, distGroup next = 1
	EXPECT_NEAR(test->boxOnObs.calcLogPOfNextGivenValue(obs->storage(), 1), log(0.7186924),
				0.01); // value this = 0; value next = 0, distGroup next = 2
	EXPECT_NEAR(test->boxOnObs.calcLogPOfNextGivenValue(obs->storage(), 2), log(0.5731175),
				0.01); // value this = 0; value next = 0,  distGroup next = 3
	EXPECT_NEAR(test->boxOnObs.calcLogPOfNextGivenValue(obs->storage(), 3), log(0.1237885),
				0.01); // value this = 0; value next = 2,  distGroup next = 3
	EXPECT_NEAR(test->boxOnObs.calcLogPOfNextGivenValue(obs->storage(), 4), log(0.2337567),
				0.01); // value this = 2; value next = 0,  distGroup next = 4
	EXPECT_NEAR(test->boxOnObs.calcLogPOfNextGivenValue(obs->storage(), 5), log(0.2337567),
				0.01); // value this = 0; value next = 2,  distGroup next = 4
	// ...
	EXPECT_NEAR(test->boxOnObs.calcLogPOfNextGivenValue(obs->storage(), 20), 0.,
				0.01); // value this = 1; there is no next

	// check calcLogPOfNextGivenOldValue
	EXPECT_NEAR(test->boxOnObs.calcLogPOfNextGivenOldValue(storage, 0), log(0.1503961),
				0.01); // oldValue this = 0; value next = 0; distGroup next = 1
	EXPECT_NEAR(test->boxOnObs.calcLogPOfNextGivenOldValue(storage, 1), log(0.2329353),
				0.01); // oldValue this = 0; value next = 0; distGroup next = 2
	EXPECT_NEAR(test->boxOnObs.calcLogPOfNextGivenOldValue(storage, 2), log(0.303094),
				0.01); // oldValue this = 0; value next = 2; distGroup next = 3
	EXPECT_NEAR(test->boxOnObs.calcLogPOfNextGivenOldValue(storage, 3), log(0.303094),
				0.01); // oldValue this = 0; value next = 2; distGroup next = 3
	EXPECT_NEAR(test->boxOnObs.calcLogPOfNextGivenOldValue(storage, 4), log(0.4356532),
				0.01); // oldValue this = 2; value next = 0; distGroup next = 4
	EXPECT_NEAR(test->boxOnObs.calcLogPOfNextGivenOldValue(storage, 5), log(0.3305901),
				0.01); // oldValue this = 0; value next = 2; distGroup next = 4
	// ...
	EXPECT_NEAR(test->boxOnObs.calcLogPOfNextGivenOldValue(storage, 20), 0.,
				0.01); // oldValue this = 0; there is no next
}

TEST_F(THMMLadderInferredTest, getLogDensityRatio) {
	auto storage = getStorageForRatios();

	// correct logPriorDensity?
	EXPECT_NEAR(test->boxOnObs.getLogDensity(obs->storage(), 0), log(0.333333) + log(0.8341673), 0.01);
	EXPECT_NEAR(test->boxOnObs.getLogDensity(obs->storage(), 1), log(0.8341673) + log(0.7186924), 0.01);
	EXPECT_NEAR(test->boxOnObs.getLogDensity(obs->storage(), 2), log(0.7186924) + log(0.5731175), 0.01);
	EXPECT_NEAR(test->boxOnObs.getLogDensity(obs->storage(), 3), log(0.5731175) + log(0.1237885), 0.01);
	EXPECT_NEAR(test->boxOnObs.getLogDensity(obs->storage(), 4), log(0.1237885) + log(0.2337567), 0.01);
	EXPECT_NEAR(test->boxOnObs.getLogDensity(obs->storage(), 5), log(0.2337567) + log(0.2337567), 0.01);
	// ...
	EXPECT_NEAR(test->boxOnObs.getLogDensity(obs->storage(), 20), log(0.3305901) + 0., 0.01);

	// correct priorDensity?
	EXPECT_NEAR(test->boxOnObs.getDensity(obs->storage(), 0), (0.333333) * (0.8341673), 0.01);
	EXPECT_NEAR(test->boxOnObs.getDensity(obs->storage(), 1), (0.8341673) * (0.7186924), 0.01);
	EXPECT_NEAR(test->boxOnObs.getDensity(obs->storage(), 2), (0.7186924) * (0.5731175), 0.01);
	EXPECT_NEAR(test->boxOnObs.getDensity(obs->storage(), 3), (0.5731175) * (0.1237885), 0.01);
	EXPECT_NEAR(test->boxOnObs.getDensity(obs->storage(), 4), (0.1237885) * (0.2337567), 0.01);
	EXPECT_NEAR(test->boxOnObs.getDensity(obs->storage(), 5), (0.2337567) * (0.2337567), 0.01);
	// ...
	EXPECT_NEAR(test->boxOnObs.getDensity(obs->storage(), 20), (0.3305901) * 1., 0.01);

	// correct logPriorRatio?
	EXPECT_NEAR(test->boxOnObs.getLogDensityRatio(storage, 0),
				log(0.3333333) - log(0.3333333) + log(0.8341673) - log(0.1503961), 0.01);
	EXPECT_NEAR(test->boxOnObs.getLogDensityRatio(storage, 1),
				log(0.8341673) - log(0.1503961) + log(0.7186924) - log(0.2329353), 0.01);
	EXPECT_NEAR(test->boxOnObs.getLogDensityRatio(storage, 2),
				log(0.7186924) - log(0.2329353) + log(0.5731175) - log(0.303094), 0.01);
	EXPECT_NEAR(test->boxOnObs.getLogDensityRatio(storage, 3), log(0.5731175) - log(0.303094) + log(0.1237885) - log(0.303094),
				0.01);
	EXPECT_NEAR(test->boxOnObs.getLogDensityRatio(storage, 4),
				log(0.1237885) - log(0.5731175) + log(0.2337567) - log(0.4356532), 0.01);
	EXPECT_NEAR(test->boxOnObs.getLogDensityRatio(storage, 5),
				log(0.2337567) - log(0.3305901) + log(0.2337567) - log(0.3305901), 0.01);
	// ...
	EXPECT_NEAR(test->boxOnObs.getLogDensityRatio(storage, 20), log(0.3305901) - log(0.3305901) + 0. - 0., 0.01);
}

TEST_F(THMMLadderInferredTest, _updateKappa) {
	test->kappa.set(0.3);

	// calculate LL
	auto f = test->boxOnObs.calculateLLRatio(&test->kappa, coretools::TRange(0));
	EXPECT_NEAR(f(&obs->storage()), -20.02215 + 19.89357, 0.01);
}

TEST_F(THMMLadderInferredTest, densities) {
	double sum = test->boxOnObs.getSumLogPriorDensity(obs->storage());
	EXPECT_NEAR(sum, -21.32552, 0.01);

	sum = obs->getSumLogPriorDensity() + test->kappa.getSumLogPriorDensity();
	EXPECT_NEAR(sum, -21.32552 - 0.2, 0.01);
}
