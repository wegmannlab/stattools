//
// Created by caduffm on 3/28/22.
//

#include "gtest/gtest.h"

#include "coretools/Types/probability.h"
#include "stattools/commonTestCases/TBetaTest.h"
#include "stattools/commonTestCases/commonTestingFunctions.h"
#include "stattools/Priors/TPriorBeta.h"
#include "stattools/Priors/TPriorExponential.h"

using namespace testing;
using namespace stattools;
using namespace stattools::prior;
using coretools::P;
using coretools::operator""_P;
using coretools::probdist::TBetaDistr;

std::vector<double> vals1() {
	return {0.001,  0.0509, 0.1008, 0.1507, 0.2006, 0.2505, 0.3004, 0.3503, 0.4002, 0.4501, 0.5,
			0.5499, 0.5998, 0.6497, 0.6996, 0.7495, 0.7994, 0.8493, 0.8992, 0.9491, 0.999};
}
std::vector<double> vals2() {
	return {0.234143806961374, 0.731057851890315, 0.80083513852956,  0.149552868462035, 0.860712598089965,
			0.925751318016275, 0.424374922630389, 0.992940929440277, 0.825610709278389, 0.23677231646186};
}

std::vector<double> vals3() {
	return {0.997855041432728, 0.0622833100395043, 0.525588429923676,  0.50922617010304,    0.0963243372014664,
			0.126802075030701, 0.201710963868274,  0.101982490809532,  0.137925490323595,   0.457595457794797,
			0.995202227676167, 0.191901016480627,  0.53194975426909,   0.587963984897121,   0.975408025165289,
			0.750516695313652, 0.276808882983362,  0.0336961317387989, 0.558341750310423,   0.291148849856178,
			0.530630074350011, 0.721812271885351,  0.712019709554695,  0.533757727869112,   0.0662704480006508,
			0.592792577078564, 0.640899275034239,  0.164658749400032,  0.216376583535156,   0.832580748720302,
			0.83883549484883,  0.30234483129109,   0.141963647965732,  0.563716573228606,   0.111101850090936,
			0.286161771168555, 0.31394054573018,   0.938404923676034,  0.0273833015625331,  0.698946036507677,
			0.924011270816947, 0.211157677156089,  0.576430141034066,  0.00105058911869594, 0.522350224222634,
			0.167086684461037, 0.484045928540885,  0.850306264805687,  0.39386781271289,    0.991124635568981};
}

//-------------------------------------
// TBetaSymmetricFixed
//-------------------------------------

struct TBetaSymmetricFixedTest : Test {
	std::string params = "1";

	using Type = coretools::ZeroOneOpen;
	TBetaSymmetricFixed<TParameterBase, Type, 1> prior;

	coretools::TMultiDimensionalStorage<TValueUpdated<Type>, 1> storage;
};

TEST_F(TBetaSymmetricFixedTest, initValid) {
	prior.setFixedPriorParameters(params);
	EXPECT_EQ(prior.alpha(), 1);
}

TEST_F(TBetaSymmetricFixedTest, initInvalid) {
	EXPECT_ANY_THROW(prior.setFixedPriorParameters("-1"));
	EXPECT_ANY_THROW(prior.setFixedPriorParameters("0"));
	EXPECT_ANY_THROW(prior.setFixedPriorParameters(""));
}

TEST_F(TBetaSymmetricFixedTest, hastings) {
	prior.setFixedPriorParameters("2");
	storage.emplace_back(TValueUpdated<Type>(0.8));
	double logDensityOld = prior.getLogDensity(storage, 0);
	storage[0]           = 0.1; // update
	storage.emplace_back(TValueUpdated<Type>(0.62));

	EXPECT_FLOAT_EQ(prior.getLogDensityRatio(storage, 0), -0.5753641);
	EXPECT_FLOAT_EQ(prior.getLogDensityRatio(storage, 0), prior.getLogDensity(storage, 0) - logDensityOld);
	EXPECT_FLOAT_EQ(prior.getDensity(storage, 1), 1.4136);
}

//-------------------------------------
// TBetaFixed
//-------------------------------------

struct TBetaFixedTest : Test {
	std::string params = "1,1";

	using Type = coretools::ZeroOneOpen;
	TBetaFixed<TParameterBase, Type, 1> prior;

	coretools::TMultiDimensionalStorage<TValueUpdated<Type>, 1> storage;
};

TEST_F(TBetaFixedTest, initValid) {
	prior.setFixedPriorParameters(params);
	EXPECT_EQ(prior.alpha(), 1);
	EXPECT_EQ(prior.beta(), 1);
}

TEST_F(TBetaFixedTest, initNegative) {
	params = "-1,1";
	EXPECT_ANY_THROW(prior.setFixedPriorParameters(params));
	params = "1,-1";
	EXPECT_ANY_THROW(prior.setFixedPriorParameters(params));
	params = "-1,-1";
	EXPECT_ANY_THROW(prior.setFixedPriorParameters(params));
}

TEST_F(TBetaFixedTest, initZero) {
	params = "0,1";
	EXPECT_ANY_THROW(prior.setFixedPriorParameters(params));
	params = "1,0";
	EXPECT_ANY_THROW(prior.setFixedPriorParameters(params));
	params = "0,0";
	EXPECT_ANY_THROW(prior.setFixedPriorParameters(params));
}

TEST_F(TBetaFixedTest, noParams) {
	params = "";
	EXPECT_ANY_THROW(prior.setFixedPriorParameters(params));
}

TEST_F(TBetaFixedTest, hastings) {
	std::string params = "1,2";
	prior.setFixedPriorParameters(params);
	storage.emplace_back(TValueUpdated<Type>(0.8));
	double logDensityOld = prior.getLogDensity(storage, 0);
	storage[0]           = 0.1; // update
	storage.emplace_back(TValueUpdated<Type>(0.62));

	EXPECT_FLOAT_EQ(prior.getLogDensityRatio(storage, 0), 1.504077);
	EXPECT_FLOAT_EQ(prior.getLogDensityRatio(storage, 0), prior.getLogDensity(storage, 0) - logDensityOld);
	EXPECT_FLOAT_EQ(prior.getDensity(storage, 1), 0.76);
}

//-------------------------------------
// TBetaSymmetricInferred
//-------------------------------------

class TBetaSymmetricInferredTest : public Test {
public:
	using TypeAlpha                     = coretools::StrictlyPositive;
	constexpr static size_t NumDimAlpha = 1;
	using BoxOnAlpha                    = prior::TExponentialFixed<TParameterBase, TypeAlpha, NumDimAlpha>;
	using Beta                          = TSymmetricBetaTest<TypeAlpha, BoxOnAlpha>;
	BoxOnAlpha _boxOnAlpha;

	std::unique_ptr<Beta> test;

	std::unique_ptr<Beta ::SpecObs> obs;
	std::unique_ptr<Beta ::SpecObs> obs2;
	std::unique_ptr<Beta ::SpecObs> obs3;

	void SetUp() override {
		instances::dagBuilder().clear();

		test = std::make_unique<Beta>("out", "1", &_boxOnAlpha);
		obs  = createObservation<Beta ::Type, Beta::NumDimObs>("obs", &test->boxOnObs, {21}, {"out"}, vals1());

		// set initial values
		test->alpha.set(0.7);
	}

	void initializeExtraObs() {
		obs2 = createObservation<Beta ::Type, Beta ::NumDimObs>("obs2", &test->boxOnObs, {10}, {"out"}, vals2());
		obs3 = createObservation<Beta ::Type, Beta ::NumDimObs>("obs3", &test->boxOnObs, {50}, {"out"}, vals3());
	}
};

TEST_F(TBetaSymmetricInferredTest, constructor) {
	EXPECT_EQ(test->boxOnObs.alpha(), 0.7);
	EXPECT_TRUE(test->alpha.isPartOfBox());
}

TEST_F(TBetaSymmetricInferredTest, getDensity) {
	for (size_t i = 0; i < obs->size(); ++i) {
		EXPECT_FLOAT_EQ(test->boxOnObs.getDensity(obs->storage(), i),
						TBetaDistr::density(P(obs->storage()[i]), test->alpha.value(),
																 test->alpha.value()));
		EXPECT_FLOAT_EQ(test->boxOnObs.getLogDensity(obs->storage(), i),
						TBetaDistr::logDensity(P(obs->storage()[i]), test->alpha.value(),
																	test->alpha.value()));
	}

	// = sum(dbeta(vals, 0.7, 0.7, log = T))
	double sum = obs->getSumLogPriorDensity();
	EXPECT_FLOAT_EQ(sum, 1.20473);
}

TEST_F(TBetaSymmetricInferredTest, getLogDensityRatio) {
	auto storage = createUpdatedStorage<Beta ::Type, Beta ::NumDimObs>(obs->storage());

	storage[0] = 0.8;
	storage[0] = 0.83; // update 0.8 -> 0.83

	EXPECT_FLOAT_EQ(test->boxOnObs.getLogDensityRatio(storage, 0), 0.03771149);
}

TEST_F(TBetaSymmetricInferredTest, _updateAlpha) {
	test->alpha.set(2.19);

	// calculate LL
	// equivalent to: sum(dbeta(vals, 2.19, 2.19, log = T)) - sum(dbeta(vals, 0.7, 0.7, log = T)) in R
	auto f = test->boxOnObs.calculateLLRatio(&test->alpha, 0);
	EXPECT_FLOAT_EQ(f(&obs->storage()), -15.18563);
}

TEST_F(TBetaSymmetricInferredTest, _updateAlpha_3params) {
	initializeExtraObs();

	test->alpha.set(2.19);

	// calculate LL
	// equivalent to: sum(dbeta(c(vals, vals2, vals3), 2.19, 2.19, log = T)) - sum(dbeta(c(vals, vals2, vals3),
	// 0.7, 0.7, log = T)) in R
	auto f = test->boxOnObs.calculateLLRatio(&test->alpha, 0);
	EXPECT_FLOAT_EQ(f(&obs->storage()) + f(&obs2->storage()) + f(&obs3->storage()), -48.88721);
}

TEST_F(TBetaSymmetricInferredTest, _setAlphaBetaToMOM_1param) {
	test->boxOnObs.guessInitialValues();

	// equivalent to: v <- 1/length(vals) * sum((mean(vals) - vals)^2); 1/(8*v) - 1/2
	EXPECT_FLOAT_EQ(test->boxOnObs.alpha(), 0.8691073);
}

TEST_F(TBetaSymmetricInferredTest, _setAlphaBetaToMOM_3params) {
	initializeExtraObs();

	test->boxOnObs.guessInitialValues();

	// equivalent to: v <- 1/length(c(vals, vals2, vals3)) * sum((mean(c(vals, vals2, vals3)) - c(vals, vals2,
	// vals3))^2); 1/(8*v) - 1/2
	EXPECT_FLOAT_EQ(test->boxOnObs.alpha(), 0.8243858);
}

//-------------------------------------
// TBetaInferred
//-------------------------------------

class TBetaInferredTest : public Test {
public:
	using TypeAlpha                     = coretools::StrictlyPositive;
	using TypeBeta                      = coretools::StrictlyPositive;
	constexpr static size_t NumDimAlpha = 1;
	constexpr static size_t NumDimBeta  = 1;
	using BoxOnAlpha                    = prior::TExponentialFixed<TParameterBase, TypeAlpha, NumDimAlpha>;
	using BoxOnBeta                     = prior::TExponentialFixed<TParameterBase, TypeBeta, NumDimBeta>;
	using Beta                          = TBetaTest<TypeAlpha, TypeBeta, BoxOnAlpha, BoxOnBeta>;
	BoxOnAlpha _boxOnAlpha;
	BoxOnBeta _boxOnBeta;

	std::unique_ptr<Beta> test;

	std::unique_ptr<Beta ::SpecObs> obs;
	std::unique_ptr<Beta ::SpecObs> obs2;
	std::unique_ptr<Beta ::SpecObs> obs3;

	void SetUp() override {
		instances::dagBuilder().clear();

		test = std::make_unique<Beta>("out", "1", &_boxOnAlpha, &_boxOnBeta);
		obs  = createObservation<Beta ::Type, Beta ::NumDimObs>("obs", &test->boxOnObs, {21}, {"out"}, vals1());

		// set initial values
		test->alpha.set(1.0);
		test->beta.set(0.7);
	}

	void initializeExtraObs() {
		obs2 = createObservation<Beta ::Type, Beta ::NumDimObs>("obs2", &test->boxOnObs, {10}, {"out"}, vals2());
		obs3 = createObservation<Beta ::Type, Beta ::NumDimObs>("obs3", &test->boxOnObs, {50}, {"out"}, vals3());
	}
};

TEST_F(TBetaInferredTest, constructor) {
	EXPECT_EQ(test->boxOnObs.alpha(), 1.);
	EXPECT_EQ(test->boxOnObs.beta(), 0.7);

	EXPECT_TRUE(test->alpha.isPartOfBox());
	EXPECT_TRUE(test->beta.isPartOfBox());
}

TEST_F(TBetaInferredTest, getDensity) {
	for (size_t i = 0; i < obs->size(); ++i) {
		EXPECT_FLOAT_EQ(test->boxOnObs.getDensity(obs->storage(), i),
						TBetaDistr::density(P(obs->storage()[i]), test->alpha.value(),
																 test->beta.value()));
		EXPECT_FLOAT_EQ(test->boxOnObs.getLogDensity(obs->storage(), i),
						TBetaDistr::logDensity(P(obs->storage()[i]), test->alpha.value(),
																	test->beta.value()));
	}

	// = sum(dbeta(vals, 1.0, 0.7, log = T))
	double sum = obs->getSumLogPriorDensity();
	EXPECT_FLOAT_EQ(sum, -0.153661);
}

TEST_F(TBetaInferredTest, getLogDensityRatio) {
	auto storage = createUpdatedStorage<Beta ::Type, Beta ::NumDimObs>(obs->storage());

	storage[0] = 0.8;
	storage[0] = 0.83; // update 0.8 -> 0.83

	EXPECT_FLOAT_EQ(test->boxOnObs.getLogDensityRatio(storage, 0), 0.04875568);
}

TEST_F(TBetaInferredTest, _updateAlpha) {
	test->alpha.set(2.19);

	// calculate LL
	// equivalent to: sum(dbeta(vals, 2.19, 0.7, log = T)) - sum(dbeta(vals, 1, 0.7, log = T)) in R
	auto f = test->boxOnObs.calculateLLRatio(&test->alpha, 0);
	EXPECT_FLOAT_EQ(f(&obs->storage()), -16.53637);
}

TEST_F(TBetaInferredTest, _updateAlpha_3params) {
	initializeExtraObs();

	test->alpha.set(2.19);

	// calculate LL
	// equivalent to: sum(dbeta(c(vals, vals2, vals3), 2.19, 0.7, log = T)) - sum(dbeta(c(vals, vals2, vals3),
	// 1, 0.7, log = T)) in R
	auto f = test->boxOnObs.calculateLLRatio(&test->alpha, 0);
	EXPECT_FLOAT_EQ(f(&obs->storage()) + f(&obs2->storage()) + f(&obs3->storage()), -59.58255);
}

TEST_F(TBetaInferredTest, _updateBeta) {
	test->beta.set(1.);

	// calculate LL
	// equivalent to: sum(dbeta(vals, 1, 1, log = T)) - sum(dbeta(vals, 1, 0.7, log = T)) in R
	auto f = test->boxOnObs.calculateLLRatio(&test->beta, 0);
	EXPECT_FLOAT_EQ(f(&obs->storage()), 0.153661);
}

TEST_F(TBetaInferredTest, _updateBeta_3params) {
	initializeExtraObs();

	test->beta.set(1.);

	// calculate LL
	// equivalent to: sum(dbeta(c(vals, vals2, vals3), 1, 1, log = T)) - sum(dbeta(c(vals, vals2, vals3), 1,
	// 0.7, log = T)) in R
	auto f = test->boxOnObs.calculateLLRatio(&test->beta, 0);
	EXPECT_FLOAT_EQ(f(&obs->storage()) + f(&obs2->storage()) + f(&obs3->storage()), 1.483910673);
}

TEST_F(TBetaInferredTest, _setAlphaBetaToMOM_1param) {
	test->boxOnObs.guessInitialValues();

	// equivalent to: ebeta(vals, "mme") in R
	EXPECT_FLOAT_EQ(test->boxOnObs.alpha(), 0.8691073);
	EXPECT_FLOAT_EQ(test->boxOnObs.beta(), 0.8691073);
}

TEST_F(TBetaInferredTest, _setAlphaBetaToMOM_3params) {
	initializeExtraObs();
	test->boxOnObs.guessInitialValues();

	// equivalent to: ebeta(c(vals, vals2, vals3), "mme") in R
	EXPECT_FLOAT_EQ(test->boxOnObs.alpha(), 0.8021020);
	EXPECT_FLOAT_EQ(test->boxOnObs.beta(), 0.8448825);
}

//-------------------------------------
// TBetaSymmetricZeroMixtureInferred
//-------------------------------------

class TBetaSymmetricZeroMixtureInferredTest : public Test {
public:
	using BoxTypeZ = DummyBox<coretools::Probability, 1>;
	std::shared_ptr<BoxTypeZ> priorThatManagesZ;

	using T = TSymmetricBetaZeroMixtureTest<BoxTypeZ>;
	std::unique_ptr<T> test;

	std::unique_ptr<T ::SpecObs> obs;
	std::unique_ptr<T ::SpecObs> obs2;
	std::unique_ptr<T ::SpecObs> obs3;

	void SetUp() override {
		instances::dagBuilder().clear();

		test = std::make_unique<T>("out", "1");
		test->z.initStorage(priorThatManagesZ.get(), {21}, {std::make_shared<coretools::TNamesIndices>()});
		obs = createObservation<T ::Type, T ::NumDimObs>("obs", &test->boxOnObs, {21}, {"out"}, vals1());

		// set initial values
		test->alpha.set(0.7);
		std::vector<bool> valsZ = {false, false, true, false, true,  false, true, false, true,  true, false,
								   true,  false, true, true,  false, false, true, true,  false, true};
		for (size_t i = 0; i < obs->size(); ++i) { test->z.set(i, (coretools::Boolean)valsZ[i]); }
	}

	void initializeExtraObs() {
		obs2 = createObservation<T ::Type, T ::NumDimObs>("obs2", &test->boxOnObs, {10}, {"out"}, vals2());
		obs3 = createObservation<T ::Type, T ::NumDimObs>("obs3", &test->boxOnObs, {50}, {"out"}, vals3());
	}
};

TEST_F(TBetaSymmetricZeroMixtureInferredTest, constructor) {
	EXPECT_EQ(test->boxOnObs.alpha(), 0.7);
	EXPECT_TRUE(test->alpha.isPartOfBox());
}

TEST_F(TBetaSymmetricZeroMixtureInferredTest, getDensity) {
	for (size_t i = 0; i < obs->size(); ++i) {
		if (test->z.value(i) == 0) {
			EXPECT_FLOAT_EQ(test->boxOnObs.getDensity(obs->storage(), i), 1.0);
			EXPECT_FLOAT_EQ(test->boxOnObs.getLogDensity(obs->storage(), i), 0.0);
		} else {
			EXPECT_FLOAT_EQ(test->boxOnObs.getDensity(obs->storage(), i),
							TBetaDistr::density(P(obs->storage()[i]), test->alpha.value(),
																	 test->alpha.value()));
			EXPECT_FLOAT_EQ(test->boxOnObs.getLogDensity(obs->storage(), i),
							TBetaDistr::logDensity(P(obs->storage()[i]), test->alpha.value(),
																		test->alpha.value()));
		}
	}

	// = sum(dbeta(vals[z==1], 0.7, 0.7, log = T))
	double sum = obs->getSumLogPriorDensity();
	EXPECT_FLOAT_EQ(sum, 0.2700309);
}

TEST_F(TBetaSymmetricZeroMixtureInferredTest, getLogDensityRatio) {
	auto storage = createUpdatedStorage<T ::Type, T ::NumDimObs>(obs->storage());

	// index 0: zero
	storage[0] = 0.8_P;
	storage[0] = 0.83_P; // update 0.8 -> 0.83. Doesn't matter since z=0

	EXPECT_FLOAT_EQ(test->boxOnObs.getLogDensityRatio(storage, 0), 0.0);

	// index 2: non-zero
	storage[2] = 0.8_P;
	storage[2] = 0.83_P; // update 0.8 -> 0.83

	EXPECT_FLOAT_EQ(test->boxOnObs.getLogDensityRatio(storage, 2), 0.03771149);
}

TEST_F(TBetaSymmetricZeroMixtureInferredTest, _updateAlpha) {
	test->alpha.set(2.19);

	// calculate LL
	// equivalent to: sum(dbeta(vals[z==1], 2.19, 2.19, log = T)) - sum(dbeta(vals[z==1], 0.7, 0.7, log = T)) in
	// R
	auto f = test->boxOnObs.calculateLLRatio(&test->alpha, 0);
	EXPECT_FLOAT_EQ(f(&obs->storage()), -6.16132);
}

TEST_F(TBetaSymmetricZeroMixtureInferredTest, _setAlphaBetaToMOM_1param) {
	test->boxOnObs.guessInitialValues();

	// equivalent to: v <- 1/length(vals[z==1]) * sum((mean(vals[z==1]) - vals[z==1])^2); 1/(8*v) - 1/2
	EXPECT_FLOAT_EQ(test->boxOnObs.alpha(), 1.091791);
}
