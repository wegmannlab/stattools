/*
#include "gmock/gmock.h"
#include "gtest/gtest.h"

#include "stattools/commonTestCases/commonTestingFunctions.h"
#include "stattools/Priors/TPriorChisq.h"
#include "stattools/Priors/TPriorExponential.h"
#include "stattools/Priors/TPriorMultivariateNormalMixedModel.h"
#include "stattools/Priors/TPriorNormal.h"
#include "stattools/Priors/TPriorUniform.h"

using namespace testing;
using namespace stattools;
using namespace stattools::prior;
*/

//--------------------------------------------
// TMultivariateNormalMixedModelInferred
//--------------------------------------------

/*
static std::vector<std::string> values_mu_1D() {
    return {"-0.820468384118015", "0.503607972233726", "0.556663198673657"};
}
static std::vector<std::string> values_mu_5D() {
    return {"-0.820468384118015, 0.487429052428485, 0.738324705129217, 0.575781351653492, -0.305388387156356",
            "0.503607972233726, 1.08576936214569, -0.69095383969683, -1.28459935387219, 0.046726172188352",
            "0.556663198673657, -0.68875569454952, -0.70749515696212, 0.36458196213683, 0.768532924515416"};
}

static std::vector<double> values_m_1D() { return {0.2, 0.4, 0.3}; }
static std::vector<double> values_m_5D() { return {0.2, 0.3, 0.4}; }

static std::vector<std::string> values_mrr_1D() { return {"0.820468384118015", "0.747393", "0.0928572161"}; }

static std::vector<std::string> values_mrr_5D() {
    return {"0.147045990503953, 1.39073512881043, 0.762029855470073, 1.23760355073483, 4.42393421768284",
            "1.32046792931379, 0.203510350031566, 1.02272587731005, 0.30174093414098, 0.725214303429139",
            "0.977395805823006, 0.209866580553353, 0.309447856154293, 1.10593626830251, 0.774187764110875"};
}

static std::vector<std::string> values_mrs_5D() {
    return {"-0.411510832795067, 0.252223448156132, -0.891921127284569, 0.435683299355719, "
            "-1.23753842192996,-0.224267885278309, 0.377395645981701, 0.133336360814841, 0.804189509744908, "
            "-0.0571067743838088",
            "-0.0538050405829051, -1.37705955682861, -0.41499456329968, -0.394289953710349, -0.0593133967111857, "
            "1.10002537198388, 0.763175748457544, -0.164523596253587, -0.253361680136508, 0.696963375404737",
            "2.44136462889459, -0.795339117255372, -0.0548774737115786, 0.250141322854153, 0.618243293566247, "
            "-0.172623502645857, -2.22390027400994, -1.26361438497058, 0.358728895971352, -0.0110454784656636"};
}

static std::vector<double> values_obs_1D() {
    return {-2.16457295409263, -0.291655279902113, -0.108401781156768, -0.726678930549294, 0.272389714054252,
            1.07882766410243,  3.88909248376303,   1.87870819962303,   -0.779213970307051, 1.49430854907625};
}
static std::vector<double> values_obs_5D() {
    return {-2.16457295409263,  0.361439854607573,   0.52964984639412,    0.60952230645136,   -0.35930690956568,
            -0.291655279902113, 0.568985644832297,   0.780844638931894,   0.593429066894042,  -0.296432894990763,
            -0.108401781156768, 0.00624008810329268, -1.28252217296438,   -0.494139504530673, 0.0387529644667214,
            -0.726678930549294, -2.28624631101354,   -2.21455941123792,   -0.310616200682877, 0.0904273283086975,
            0.272389714054252,  0.378358870785744,   0.605285791934401,   0.493920177370878,  -0.26142196592605,
            1.07882766410243,   4.11225197986768,    -0.0232194297031681, -1.5030881319683,   0.0147537423812551,
            3.88909248376303,   -2.2436412859183,    0.0459508831181846,  0.408099896811535,  0.387999342401306,
            1.87870819962303,   3.06413042961675,    0.612956137659212,   -2.03701992654969,  -0.0435783059986362,
            -0.779213970307051, -1.0959492637063,    -2.56202651072127,   0.259489520694582,  -0.56822803571598,
            1.49430854907625,   0.568464581792549,   1.1116208525791,     0.463288587121934,  -0.364931471355877};
}

static std::vector<double> values_obs_initialization() {
    return {1.91714536276596,    3.0828460206628,     5.61078100970474,   5.74152079791429,    7.3039648225257,
            -5.54214237953273,   -6.52653214649499,   -4.02167199368079,  -2.27771739405969,   -3.17368163188458,
            0.295667307425141,   0.0221284733892106,  -0.938626500416387, 0.392262137726352,   0.60036849165861,
            0.0917985539091022,  -0.813753260213032,  -0.682023897202607, 0.552080686486983,   0.290703532160413,
            4.05351567882921,    3.18971650280693,    2.87693394190199,   5.29192141099235,    5.68721342047964,
            -2.85616451407826,   2.18392291802792,    0.278246735587241,  1.12506961668223,    -0.151432502085334,
            1.04255931610422,    -0.734491194840375,  -0.891424766586292, -0.857470305521913,  0.415972011945491,
            2.82112063000613,    4.99559328314573,    4.82229569387376,   6.27473393571959,    7.24794637649306,
            -4.02230321365538,   -1.36909553958601,   -0.413891554934754, 2.48921826495574,    0.921010232489222,
            3.90552809216589,    3.94602330604539,    4.75424042419282,   4.84521439119284,    5.01619475728603,
            -5.37838066148848,   -5.27874487632671,   -2.73591467082369,  -3.19198688295824,   -3.41850168952339,
            3.58378324250254,    4.07028206432596,    4.95347477534063,   4.84140845940536,    7.60258407676699,
            3.73841444053325,    6.28798171613724,    5.8673005155776,    6.2025809379042,     7.62353223954902,
            3.5135285044922,     4.14696055516133,    4.93769947560116,   7.11961510955958,    7.24621140625639,
            -1.35470831434632,   1.89503334179611,    0.551009936822662,  1.74878589516192,    1.04947015244577,
            3.57349959788138,    2.7969047856018,     3.95850264397015,   6.26873496201223,    5.25467962256364,
            -1.45554881527043,   0.157889402027386,   -1.48546741238514,  1.96918454895137,    0.778238833895827,
            -0.111299942235672,  -1.56500611114445,   -0.777271197344697, 0.90786263410391,    1.75027167348149,
            -3.54040478510828,   -4.89684253416621,   -4.48308382877741,  -3.81621913231605,   -1.78999605217456,
            -1.04991861387281,   -1.36241030562427,   -0.497998098950626, -0.895918217076378,  0.575622790891925,
            3.95429277356773,    6.68799111569512,    6.42209579552564,   5.68324702147649,    5.34706716161904,
            -5.60677917848851,   -4.41309117949462,   -4.23429702939719,  -2.23747310432147,   -4.56379809166681,
            -0.190726923056152,  -1.21162227867747,   -2.68958517903653,  -0.0670470254026386, 2.29919883255821,
            -4.33598680827274,   -5.0478381979128,    -4.91744200544194,  -3.06289424696695,   -3.38496856155157,
            3.16259535297757,    3.78424738461006,    5.37482095804266,   7.88332796196972,    5.53608703843818,
            0.668095116963238,   0.585743355850373,   0.557528328618703,  -1.13683281866016,   0.728461970367885,
            -1.01722712359236,   -1.04263905055956,   1.9786926597401,    -0.866112645231459,  1.42489636147032,
            -0.0938508251234479, -0.0988571690435028, 0.0756974415287978, 1.55948916812251,    1.91693803731112,
            4.31808070003799,    3.85125099154918,    3.13109565647019,   4.29300342135454,    7.89725805765655,
            -6.49347152278348,   -5.59713482195183,   -4.02910674788401,  -1.32631508043571,   -1.63021295126421,
            0.00154009440800351, -0.69352784002616,   -0.9331768198576,   0.810329445057101,   2.58332171358656,
            4.19640746067212,    3.28347368776555,    4.71495713558065,   5.19965393518509,    5.3613794993824,
            0.286701362727813,   -0.663649099670905,  1.08522983690742,   1.22983194527456,    0.700824406037245,
            -2.48382894682663,   -0.786871051313951,  -0.901023769016841, -1.074017112396,     0.331685068476428,
            5.74284789606923,    2.63059940434558,    5.03118449189908,   4.29088587044379,    4.65894905206225,
            2.40984129075226,    3.89000878547744,    4.68671886626466,   4.57787023191352,    5.74351939204366,
            -0.0707565132032982, -0.788020655264366,  1.40937245611348,   0.553768001220129,   2.60676777835353,
            6.00858995573927,    2.64558048717593,    4.53728561751052,   6.54310383332729,    5.6961624786282,
            3.37489502208362,    3.62485324545076,    5.14335694280806,   6.92518963325776,    4.2652505739696,
            1.32195540765037,    5.10918047095619,    3.96635982281761,   5.99739149641744,    5.02610766346699,
            -4.15762469622184,   -3.54698369494362,   -4.41063181575339,  -3.3991917232368,    -2.07924430681596,
            3.66547142215042,    4.11546010451152,    4.5788317880187,    4.54164476708552,    6.24911630844292,
            2.94176429031876,    4.91722359556646,    4.68454846578415,   6.32554913408199,    7.29127203757818,
            -5.19029154455482,   -5.13804833369078,   -6.03538388162579,  -4.10437235544708,   -3.80816188907634,
            3.93632795124526,    5.36882763411433,    5.37837548052549,   6.91568816870243,    5.79097324342121,
            -2.71500317257465,   -0.307745313132091,  -1.16173323791098,  -0.14182866447016,   2.31243918737546,
            -5.75298064796168,   -5.14926724362705,   -4.25424445327965,  -4.79636955777938,   -3.42465709071545,
            -1.97630902147991,   0.692971683882685,   0.440959630244447,  0.0709024405101132,  0.622335733898549,
            -5.03764292957633,   -3.31270143362489,   -4.1056576938449,   -3.2975583477008,    -2.21959152572253,
            -1.36488733045942,   -0.718080698235093,  2.49935178789941,   0.645173889834543,   -0.477803443245903};
}

static std::vector<double> values_obs_initialization_2() {
    return {-0.549812898727345, -0.518559832714638,  -0.318068374543844,  -0.429362147453702, -0.487460310141485,
            3.4313312671815,    4.36482138487617,    6.1780869965732,     3.97643319957024,   6.59394618762842,
            -2.23132342155804,  0.483895570053379,   0.219924803660651,   -0.967250029092243, 1.52102274264814,
            -1.50595746211426,  0.843038825170412,   -0.214579408546869,  0.320443469956613,  0.899809258786438,
            3.83547640374641,   4.24663831986349,    5.69696337540474,    6.05666319867366,   5.31124430545048,
            -1.62036667722412,  -0.457884126855765,  -0.910921648552446,  0.658028772404075,  0.345415356081182,
            -6.73321840682484,  -4.49786814031973,   -4.63030033392815,   -3.84096857986041,  -4.15657236263585,
            4.47550952889966,   3.79005356907819,    5.61072635348905,    4.56590236835575,   4.7463665997609,
            -5.09817874400523,  -3.93917927137988,   -5.18645863857947,   -2.40322295572576,  -3.00534402827817,
            -1.39280792944198,  -0.819992868548507,  -0.279113302976559,  0.994188331267827,  0.822669517730394,
            4.18879229951434,   2.69504137110896,    6.46555486156289,    5.6532533382119,    8.17261167036215,
            5.35867955152904,   4.397212272657,      5.38767161155937,    5.4461949594171,    4.62294044317139,
            -0.939839559565485, -1.08889448625966,   0.531496192632572,   -1.01839408178679,  1.30655786078977,
            -0.287333692948595, -0.573564404126326,  -0.0376341714670479, -0.181660478755657, 0.675729727753681,
            -1.75081900119345,  1.58716654562835,    0.0173956196932517,  -0.786300530434326, -0.640605534418578,
            -5.16437583176967,  -4.07930535674549,   -4.40024674397764,   -4.87020787754746,  -2.01216173254512,
            3.943871260471,     4.34420449329467,    3.52924761610073,    5.02184994489138,   6.4179415601997,
            -3.48025497450045,  -4.80874056922561,   -5.25328975560769,   -2.85775869432218,  -3.04470913689398,
            3.37354618925767,   4.68364332422208,    4.16437138758995,    7.09528080213779,   6.32950777181536,
            -4.29268933260192,  -3.46589226526254,   -3.7765195850847,    -4.37870761286602,  -1.83703544403267,
            6.40161776050478,   4.46075999726683,    5.68973936245078,    5.52800215878067,   5.25672679111759,
            -0.574899622627552, -0.738647100913033,  1.05848304870902,    1.38642265137494,   0.380756951768853,
            -1.03472602831128,  0.287639605630162,   2.07524500865228,    1.52739243876377,   2.2079083983867,
            -2.50233841016584,  -3.83293383323451,   -3.4586726640363,    -3.51339952314591,  -2.48989157704707,
            3.58500543670032,   4.10571004628965,    4.94068660328881,    6.60002537198388,   6.76317574845754,
            3.17953161588198,   4.98742905242849,    5.73832470512922,    6.07578135165349,   5.69461161284364,
            -3.19685809208253,  -4.83113203639122,   -5.60551341225308,   -3.30280656126052,  -2.73682435359453,
            -1.15875460471602,  0.964587311969797,   -0.766081999604665,  0.0697882460714535, 0.0738905026225628,
            4.5584864255653,    3.22340779154196,    4.42673458576311,    4.27538738510164,   5.52659936356069,
            -2.04798441280774,  0.941157706844281,   -1.01584746530465,   0.911974712317515,  0.61892394889108,
            0.767287269372646,  0.216707476017206,   0.910174229495227,   0.884185357826345,  2.68217608051942,
            4.91897737160822,   5.28213630073107,    5.07456498336519,    3.51064830413663,   6.61982574789471,
            3.29250484303788,   4.86458196213683,    5.76853292451542,    5.38765378784977,   6.88110772645422,
            5.98039989850586,   4.13277852353349,    3.95586537368347,    6.06971962744241,   5.86494539611918,
            3.95506639098477,   4.48380973690105,    5.9438362106853,     6.32122119509809,   6.59390132121751,
            -1.17710396143654,  -0.0979882205136621, -0.731748173119606,  1.33037316798167,   -0.208082786304465,
            -1.63573645394898,  -0.961644730360566,  1.43228223854166,    -0.150696353310367, 0.792619256398035,
            -2.53644982353759,  -0.800976126836611,  -0.528279904445006,  -0.152094780680999, 0.943103222152607,
            -5.98582670040929,  -7.38892067167954,   -4.64048170256511,   -2.92949236407952,  -3.05972327604261,
            -2.07519229661568,  0.500028803713914,   -0.621266694796823,  -0.884426847384491, 2.86929062242358,
            3.45747996900835,   5.70786780598317,    6.16040261569495,    6.200213649515,     7.58683345454085,
            -4.59059816034907,  -2.81112671379595,   -2.41341156655803,   -3.83090780068277,  -5.28523553529247,
            4.33295037121352,   5.56309983727636,    4.6958160763657,     5.87001880991629,   6.26709879077223,
            -2.91435942568001,  0.676583312018562,   -1.664972436212,     0.0364695985276144, -0.115920105042845,
            4.39810588036707,   3.88797360674923,    5.34111969142442,    4.37063690391921,   7.43302370170104,
            4.29144623551746,   4.05670812678157,    5.00110535163162,    5.57434132415166,   5.41047905381193,
            -7.00016494478548,  -5.04479074000172,   -4.25567070915699,   -3.66612103676501,  -1.97953609121589,
            1.30797839905936,   -0.394197632106289,  0.456998805423414,   0.422847064643469,  0.665999157633456,
            1.20610246454047,   -0.755027030141015,  -1.42449465021281,   0.355600398045781,  1.20753833923234,
            5.51178116845085,   4.88984323641143,    4.3787594194582,     3.2853001128225,    7.12493091814311};
}

static std::vector<size_t> values_z() { return {0, 0, 1, 1, 0, 1, 2, 1, 1, 0}; }

static std::vector<size_t> values_z_testInitialization() {
    return {1, 0, 1, 1, 0, 1, 2, 0, 2, 1, 0, 0, 1, 1, 1, 2, 0, 2, 0, 2, 0, 1, 1, 2, 0,
            0, 2, 1, 0, 1, 1, 0, 0, 0, 0, 1, 1, 1, 2, 1, 0, 2, 0, 1, 0, 0, 2, 1, 1, 0};
}

class TMultivariateNormalMixedModelInferredTest : public Test {
public:
    constexpr static size_t K = 3;

    using TypeObs                     = coretools::Unbounded;
    static constexpr size_t NumDimObs = 2;
    using SpecZ                       = ParamSpec<coretools::UnsignedInt8WithMax<0>, coretools::toHash("z"), 1>;
    using SpecMus                     = ParamSpec<coretools::Unbounded, coretools::toHash("mus"), 1>;
    using SpecM                       = ParamSpec<coretools::StrictlyPositive, coretools::toHash("m"), 1>;
    using SpecMrr                     = ParamSpec<coretools::StrictlyPositive, coretools::toHash("mrr"), 1>;
    using SpecMrs                     = ParamSpec<coretools::Unbounded, coretools::toHash("mrs"), 1>;
    using BoxType = TMultivariateNormalMixedModelInferred<TObservationBase, TypeObs, NumDimObs, SpecZ, SpecMus, SpecM,
                                                          SpecMrr, SpecMrs, K>;
    using TypeParamZ   = TParameter<SpecZ, BoxType>;
    using TypeParamMus = TParameter<SpecMus, BoxType>;
    using TypeParamM   = TParameter<SpecM, BoxType>;
    using TypeParamMrr = TParameter<SpecMrr, BoxType>;
    using TypeParamMrs = TParameter<SpecMrs, BoxType>;

    std::unique_ptr<TypeParamZ> z;
    std::array<std::unique_ptr<TypeParamMus>, K> mus;
    std::array<std::unique_ptr<TypeParamM>, K> m;
    std::array<std::unique_ptr<TypeParamMrr>, K> mrr;
    std::array<std::unique_ptr<TypeParamMrs>, K> mrs;

    std::shared_ptr<BoxType> prior;
    std::unique_ptr<TObservation<TypeObs, NumDimObs>> obs;

    void initialize(size_t NumDim, size_t Size, const std::vector<double> &ValuesObs,
                    const std::vector<size_t> &ValuesZ = values_z()) {
        using namespace coretools::str;

        SpecZ::value_type::setMax(2);

        // initialize pointers
        for (size_t i = 0; i < K; ++i) {
            const auto values_mu = (NumDim == 1) ? values_mu_1D()[i] : values_mu_5D()[i];
            mus[i] =
                createParameter<TNormalFixed, SpecMus, BoxType>("mus_" + toString(i), {"out", "0.0,1.0", values_mu});

            const auto values_m = (NumDim == 1) ? values_m_1D : values_m_5D;
            m[i]                = createParameter<TExponentialFixed, SpecM, BoxType>("m_" + toString(i),
                                                                      {"out", "5.0", toString(values_m()[i])});

            const auto values_mrr = (NumDim == 1) ? values_mrr_1D()[i] : values_mrr_5D()[i];
            mrr[i]                = createParameter<TMultivariateChisqFixed, SpecMrr, BoxType>("mrr_" + toString(i),
                                                                                {"out", "5,4,3,2,1", values_mrr});

            const auto values_mrs = (NumDim == 1) ? "" : values_mrs_5D()[i];
            mrs[i] =
                createParameter<TNormalFixed, SpecMrs, BoxType>("mrs_" + toString(i), {"out", "0.,1.", values_mrs});
        }
        std::array<TypeParamMus *, K> arrayMeans = {mus[0].get(), mus[1].get(), mus[2].get()};
        std::array<TypeParamM *, K> arrayM       = {m[0].get(), m[1].get(), m[2].get()};
        std::array<TypeParamMrr *, K> arrayMrr   = {mrr[0].get(), mrr[1].get(), mrr[2].get()};
        std::array<TypeParamMrs *, K> arrayMrs   = {mrs[0].get(), mrs[1].get(), mrs[2].get()};

        // create z
        TParameterDefinition defZ("out", "");
        if (!ValuesZ.empty()) { defZ.setInitVal(coretools::str::concatenateString(ValuesZ, ",")); }
        z = createParameter<TUniformFixed, SpecZ, BoxType>("z", defZ);

        prior = createPrior<BoxType>(z.get(), arrayMeans, arrayM, arrayMrr, arrayMrs);

        obs = createObservation<TypeObs, NumDimObs>("obs", prior, {Size, NumDim}, {"out"}, ValuesObs);
    }
};

TEST_F(TMultivariateNormalMixedModelInferredTest, initParameter_wrongDimensions) {
    // this should throw, wrong number of columns
    EXPECT_ANY_THROW(initialize(10, 9, values_obs_1D()));

    // this should also throw, rows - cols switched
    EXPECT_ANY_THROW(initialize(10, 5, values_obs_5D()));
}

TEST_F(TMultivariateNormalMixedModelInferredTest, getLogDensity_5D) {
    initialize(5, 10, values_obs_5D());

    // we want prior density of first element of first row -> z = 0
    size_t linearIndex = obs->getIndex({0, 0});
    EXPECT_FLOAT_EQ(test->boxOnObs.getLogDensity(obs->storage(), linearIndex), 1.46775254410025);
    EXPECT_FLOAT_EQ(test->boxOnObs.getDensity(obs->storage(), linearIndex), exp(1.46775254410025));

    // we want prior density of third element of 7th row -> z = 2
    linearIndex = obs->getIndex({6, 2});
    EXPECT_FLOAT_EQ(test->boxOnObs.getLogDensity(obs->storage(), linearIndex), -3.53834226494083);
    EXPECT_FLOAT_EQ(test->boxOnObs.getDensity(obs->storage(), linearIndex), exp(-3.53834226494083));

    // we want prior density of last element of 9th row -> z = 1
    linearIndex = obs->getIndex({8, 4});
    EXPECT_FLOAT_EQ(test->boxOnObs.getLogDensity(obs->storage(), linearIndex), -3.20203842196268);
    EXPECT_FLOAT_EQ(test->boxOnObs.getDensity(obs->storage(), linearIndex), exp(-3.20203842196268));
}

TEST_F(TMultivariateNormalMixedModelInferredTest, getLogDensity_1D) {
    initialize(1, 10, values_obs_1D());

    // we want prior density of first element (z = 0)
    size_t linearIndex = obs->getIndex({0, 0});
    EXPECT_FLOAT_EQ(test->boxOnObs.getLogDensity(obs->storage(), linearIndex), -21.8922143086036);
    EXPECT_FLOAT_EQ(test->boxOnObs.getDensity(obs->storage(), linearIndex), exp(-21.8922143086036));

    // we want prior density of 7th element (z = 2)
    linearIndex = obs->getIndex({6, 0});
    EXPECT_FLOAT_EQ(test->boxOnObs.getLogDensity(obs->storage(), linearIndex), -61.4098820628858);
    EXPECT_FLOAT_EQ(test->boxOnObs.getDensity(obs->storage(), linearIndex), exp(-61.4098820628858));

    // we want prior density of 9th element (z = 1)
    linearIndex = obs->getIndex({8, 0});
    EXPECT_FLOAT_EQ(test->boxOnObs.getLogDensity(obs->storage(), linearIndex), -5.14524822715581);
    EXPECT_FLOAT_EQ(test->boxOnObs.getDensity(obs->storage(), linearIndex), exp(-5.14524822715581));
}

TEST_F(TMultivariateNormalMixedModelInferredTest, getLogRatio_5D) {
    initialize(5, 10, values_obs_5D());
    auto storage = createUpdatedStorage(obs->storage());

    // we want prior ratio of first element of first row
    size_t linearIndex   = obs->getIndex({0, 0});
    storage[linearIndex] = 0.8;
    EXPECT_FLOAT_EQ(test->boxOnObs.getLogDensityRatio(storage, linearIndex), -0.151311651277237);
    storage[linearIndex].reset();

    // we want prior ratio of third element of 7th row
    linearIndex          = obs->getIndex({6, 2});
    storage[linearIndex] = 0.5;
    EXPECT_FLOAT_EQ(test->boxOnObs.getLogDensityRatio(storage, linearIndex), -1.16191082678025);
    storage[linearIndex].reset();

    // we want prior ratio of last element of 9th row
    linearIndex          = obs->getIndex({8, 4});
    storage[linearIndex] = -0.5274;
    EXPECT_FLOAT_EQ(test->boxOnObs.getLogDensityRatio(storage, linearIndex), 0.165073210306163);
}

TEST_F(TMultivariateNormalMixedModelInferredTest, getLogRatio_1D) {
    initialize(1, 10, values_obs_1D());
    auto storage = createUpdatedStorage(obs->storage());

    // we want prior ratio of first element
    size_t linearIndex   = obs->getIndex({0, 0});
    storage[linearIndex] = 0.8;
    EXPECT_FLOAT_EQ(test->boxOnObs.getLogDensityRatio(storage, linearIndex), -10.2412586112426);
    storage[linearIndex].reset();

    // we want prior ratio of 7th element
    linearIndex          = obs->getIndex({6, 0});
    storage[linearIndex] = 0.5;
    EXPECT_FLOAT_EQ(test->boxOnObs.getLogDensityRatio(storage, linearIndex), 61.6770790113185);
    storage[linearIndex].reset();

    // we want prior ratio of 9th element
    linearIndex          = obs->getIndex({8, 0});
    storage[linearIndex] = -0.5274;
    EXPECT_FLOAT_EQ(test->boxOnObs.getLogDensityRatio(storage, linearIndex), 1.8207959295456);
}

TEST_F(TMultivariateNormalMixedModelInferredTest, calcLLUpdateMrr) {
    initialize(5, 10, values_obs_5D());

    // change r=0 of Mrr
    size_t r                   = 0;
    std::array<double, 3> vals = {0.62, 0.65, 0.79};
    for (size_t i = 0; i < K; ++i) { mrr[i]->set(r, vals[i]); }

    // calculate LL ratio
    EXPECT_FLOAT_EQ(test->boxOnObs.calcLLRatioMrr(0, r)(&obs->storage()), -33.0550168865815);
    EXPECT_FLOAT_EQ(test->boxOnObs.calcLLRatioMrr(1, r)(&obs->storage()), -18.4747599040689);
    EXPECT_FLOAT_EQ(test->boxOnObs.calcLLRatioMrr(2, r)(&obs->storage()), -2.52841403480806);
    for (size_t i = 0; i < K; ++i) { mrr[i]->reset(); }

    // change r=4 of Mrr
    r    = 4;
    vals = {0.01, 0.1, 0.2};
    for (size_t i = 0; i < K; ++i) { mrr[i]->set(r, vals[i]); }

    EXPECT_FLOAT_EQ(test->boxOnObs.calcLLRatioMrr(0, r)(&obs->storage()), -22.2977281660075);
    EXPECT_FLOAT_EQ(test->boxOnObs.calcLLRatioMrr(1, r)(&obs->storage()), -8.79057566389597);
    EXPECT_FLOAT_EQ(test->boxOnObs.calcLLRatioMrr(2, r)(&obs->storage()), -1.10037348686971);
}

TEST_F(TMultivariateNormalMixedModelInferredTest, calcLLUpdateMrs) {
    initialize(5, 10, values_obs_5D());

    // change r=1, s=0 of Mrs
    size_t r                   = 1;
    size_t s                   = 0;
    size_t linearIndex         = stattools::prior::impl::mvn::indexMrs(r, s);
    std::array<double, 3> vals = {0.62, 0.77, 0.88};
    for (size_t i = 0; i < K; ++i) { mrs[i]->set(linearIndex, vals[i]); }

    // calculate LL ratio
    EXPECT_FLOAT_EQ(test->boxOnObs.calcLLRatioMrs(0, r, s, linearIndex)(&obs->storage()), -1.60576348134709);
    EXPECT_FLOAT_EQ(test->boxOnObs.calcLLRatioMrs(1, r, s, linearIndex)(&obs->storage()), -104.501921796656);
    EXPECT_FLOAT_EQ(test->boxOnObs.calcLLRatioMrs(2, r, s, linearIndex)(&obs->storage()), -14.1543608666133);

    // reset previous change
    for (size_t i = 0; i < K; ++i) { mrs[i]->reset(); }

    // change r=4, s=3 of Mrs
    r           = 4;
    s           = 3;
    linearIndex = stattools::prior::impl::mvn::indexMrs(r, s);
    vals        = {0.01, 0.87, 0.2};
    for (size_t i = 0; i < K; ++i) { mrs[i]->set(linearIndex, vals[i]); }

    EXPECT_FLOAT_EQ(test->boxOnObs.calcLLRatioMrs(0, r, s, linearIndex)(&obs->storage()), -0.00265077348486553);
    EXPECT_FLOAT_EQ(test->boxOnObs.calcLLRatioMrs(1, r, s, linearIndex)(&obs->storage()), -0.100092120820062);
    EXPECT_FLOAT_EQ(test->boxOnObs.calcLLRatioMrs(2, r, s, linearIndex)(&obs->storage()), 0.00611172924459069);
}

TEST_F(TMultivariateNormalMixedModelInferredTest, calcLLRatioMu) {
    initialize(5, 10, values_obs_5D());

    // change r=0 of mu
    size_t r                   = 0;
    std::array<double, 3> vals = {-0.62, 0.77, 0.001};
    for (size_t i = 0; i < K; ++i) { mus[i]->set(r, vals[i]); }

    // calculate LL ratio and logH
    EXPECT_FLOAT_EQ(test->boxOnObs.calcLLRatioMu(0, r)(&obs->storage()), 0.210483743353389);
    EXPECT_FLOAT_EQ(test->boxOnObs.calcLLRatioMu(1, r)(&obs->storage()), -4.00976070689726);
    EXPECT_FLOAT_EQ(test->boxOnObs.calcLLRatioMu(2, r)(&obs->storage()), 0.0321774047667285);

    // reset previous change
    for (size_t i = 0; i < K; ++i) { mus[i]->reset(); }

    // change r=4 of mu
    r    = 4;
    vals = {0.1, 0.5, 0.3};
    for (size_t i = 0; i < K; ++i) { mus[i]->set(r, vals[i]); }
    EXPECT_FLOAT_EQ(test->boxOnObs.calcLLRatioMu(0, r)(&obs->storage()), -177.735773265188);
    EXPECT_FLOAT_EQ(test->boxOnObs.calcLLRatioMu(1, r)(&obs->storage()), -12.5581449248626);
    EXPECT_FLOAT_EQ(test->boxOnObs.calcLLRatioMu(2, r)(&obs->storage()), -5.72357149340797);
}

TEST_F(TMultivariateNormalMixedModelInferredTest, calcLLRatioMu_1D) {
    initialize(1, 10, values_obs_1D());

    // change r=0 of mu
    size_t r                   = 0;
    std::array<double, 3> vals = {-0.62, 0.88, 0.01};
    for (size_t i = 0; i < K; ++i) { mus[i]->set(r, vals[i]); }

    // calculate LL ratio and logH
    EXPECT_FLOAT_EQ(test->boxOnObs.calcLLRatioMu(0, r)(&obs->storage()), 10.9826944903589);
    EXPECT_FLOAT_EQ(test->boxOnObs.calcLLRatioMu(1, r)(&obs->storage()), -4.97726408495443);
    EXPECT_FLOAT_EQ(test->boxOnObs.calcLLRatioMu(2, r)(&obs->storage()), -21.9015197636976);
}

TEST_F(TMultivariateNormalMixedModelInferredTest, _calcLLUpdateM) {
    initialize(5, 10, values_obs_5D());

    // change m
    std::array<double, 3> vals = {0.62, 0.44, 0.33};
    for (size_t i = 0; i < K; ++i) { m[i]->set(vals[i]); }

    // calculate LL ratio
    EXPECT_FLOAT_EQ(test->boxOnObs.calcLLRatioM(0)(&obs->storage()), -16.9218632242873);
    EXPECT_FLOAT_EQ(test->boxOnObs.calcLLRatioM(1)(&obs->storage()), -4.28648917144376);
    EXPECT_FLOAT_EQ(test->boxOnObs.calcLLRatioM(2)(&obs->storage()), 0.674337666686083);
}

TEST_F(TMultivariateNormalMixedModelInferredTest, _calcLLUpdateM_1D) {
    initialize(1, 10, values_obs_1D());

    // change m
    std::array<double, 3> vals = {0.62, 0.44, 0.33};
    for (size_t i = 0; i < K; ++i) { m[i]->set(vals[i]); }

    // calculate LL ratio
    EXPECT_FLOAT_EQ(test->boxOnObs.calcLLRatioM(0)(&obs->storage()), 92.2225634249593);
    EXPECT_FLOAT_EQ(test->boxOnObs.calcLLRatioM(1)(&obs->storage()), 2.64501463832118);
    EXPECT_FLOAT_EQ(test->boxOnObs.calcLLRatioM(2)(&obs->storage()), 10.6120719938663);
}

TEST_F(TMultivariateNormalMixedModelInferredTest, calcLikelihoodSampleZ) {
    initialize(5, 10, values_obs_5D());

    // n=0 of z: calculate likelihood
    size_t n = 0;
    EXPECT_FLOAT_EQ(test->boxOnObs.calcLikelihoodSampleZ(n, 0), 4.33947140278094);
    EXPECT_FLOAT_EQ(test->boxOnObs.calcLikelihoodSampleZ(n, 1), 8.75252982245735e-125);
    EXPECT_FLOAT_EQ(test->boxOnObs.calcLikelihoodSampleZ(n, 2), 3.24095925886371e-10);

    // n=3 of z: calculate likelihood
    n = 3;
    EXPECT_FLOAT_EQ(test->boxOnObs.calcLikelihoodSampleZ(n, 0), 3.02768372915135e-39);
    EXPECT_FLOAT_EQ(test->boxOnObs.calcLikelihoodSampleZ(n, 1), 0.0193296242349379);
    EXPECT_FLOAT_EQ(test->boxOnObs.calcLikelihoodSampleZ(n, 2), 6.26420955859796e-13);

    // n=9 of z: calculate likelihood
    n = 9;
    EXPECT_FLOAT_EQ(test->boxOnObs.calcLikelihoodSampleZ(n, 0), 0.88804779984572);
    EXPECT_FLOAT_EQ(test->boxOnObs.calcLikelihoodSampleZ(n, 1), 6.93538629893235e-51);
    EXPECT_FLOAT_EQ(test->boxOnObs.calcLikelihoodSampleZ(n, 2), 6.70287493963411e-42);
}

TEST_F(TMultivariateNormalMixedModelInferredTest, calcLikelihoodSampleZ_1D) {
    initialize(1, 10, values_obs_1D());

    // n=0 of z: calculate likelihood
    size_t n = 0;
    EXPECT_FLOAT_EQ(test->boxOnObs.calcLikelihoodSampleZ(n, 0), 3.10693472549252e-10);
    EXPECT_FLOAT_EQ(test->boxOnObs.calcLikelihoodSampleZ(n, 1), 2.17219044442493e-10);
    EXPECT_FLOAT_EQ(test->boxOnObs.calcLikelihoodSampleZ(n, 2), 1.80755653015659e-18);

    // n=3 of z: calculate likelihood
    n = 3;
    EXPECT_FLOAT_EQ(test->boxOnObs.calcLikelihoodSampleZ(n, 0), 1.7870096016539);
    EXPECT_FLOAT_EQ(test->boxOnObs.calcLikelihoodSampleZ(n, 1), 0.0088029699184364);
    EXPECT_FLOAT_EQ(test->boxOnObs.calcLikelihoodSampleZ(n, 2), 0.000141277783462032);

    // n=9 of z: calculate likelihood
    n = 9;
    EXPECT_FLOAT_EQ(test->boxOnObs.calcLikelihoodSampleZ(n, 0), 1.62915902395468e-29);
    EXPECT_FLOAT_EQ(test->boxOnObs.calcLikelihoodSampleZ(n, 1), 0.0464305953586183);
    EXPECT_FLOAT_EQ(test->boxOnObs.calcLikelihoodSampleZ(n, 2), 0.0100589508104711);
}

TEST_F(TMultivariateNormalMixedModelInferredTest, _findMostDistantPoints) {
    // simulations and true values based on R script 'simulateHMM.R'
    initialize(5, 50, values_obs_initialization(), {});
    test->boxOnObs.setInitialZ();
    std::vector<size_t> expectedZ = {0, 1, 2, 2, 0, 2, 2, 0, 2, 0, 1, 0, 0, 0, 2, 0, 2, 2, 1, 2, 0, 1, 2, 1, 0,
                                     2, 2, 2, 0, 1, 2, 0, 2, 2, 0, 0, 2, 0, 0, 0, 1, 0, 0, 1, 0, 2, 1, 2, 1, 2};
    for (size_t i = 0; i < z->size(); i++) { EXPECT_EQ(z->value(i), expectedZ[i]); }
}

TEST_F(TMultivariateNormalMixedModelInferredTest, _setInitialMeanAndM) {
    // simulations and true values based on R script 'simulateHMM.R'
    initialize(5, 50, values_obs_initialization_2(), values_z_testInitialization());
    test->boxOnObs.setInitialMeanAndM();

    std::vector<std::vector<double>> expectedMeans = {{4.323528, 4.408386, 5.207406, 5.344226, 6.260891},
                                                      {-1.10760496, -0.03532857, -0.09055041, 0.21637645, 0.82806711},
                                                      {-4.804450, -4.469989, -4.362057, -3.559359, -2.960703}};
    std::vector<std::vector<double>> expectedMrr   = {{1.3139124, 1.4277875, 1.2930445, 0.9863651, 1.0790508},
                                                      {0.9920687, 1.3452873, 1.1737095, 1.2817348, 1.0743238},
                                                      {0.8255203, 1.0288827, 1.4561026, 1.4652312, 0.9877661}};
    std::vector<std::vector<double>> expectedMrs   = {{0.1573176, 0.2993756, 0.09015391, 0.3761421, -0.08936336,
                                                       -0.07192644, 0.3827986, -0.08068906, -0.20038523, 0.06971016},
                                                      {0.4383422, -0.1329276, 0.2177355024, -0.3212492, 0.0858837914,
                                                       -0.2262790, -0.1141799, 0.00090212241, -0.4357094, -0.000684821},
                                                      {-0.54646253, -0.07741808, -0.4668938, -0.70797852, 0.4506182,
                                                       1.0506617, -0.35595644, 0.1541158, 0.5971107, 0.1993824}};
    // m's
    for (size_t k = 0; k < K; ++k) {
        EXPECT_FLOAT_EQ(m[k]->value(), 1.);
        for (size_t d = 0; d < 5; d++) {
            // means
            EXPECT_FLOAT_EQ(mus[k]->value(d), expectedMeans[k][d]);
            EXPECT_FLOAT_EQ(mrr[k]->value(d), expectedMrr[k][d]);
        }
        for (size_t d = 0; d < 10; d++) { EXPECT_FLOAT_EQ(mrs[k]->value(d), expectedMrs[k][d]); }
    }
}

 */
