//
// Created by caduffm on 3/28/22.
//

#include "stattools/Priors/TPriorExponential.h"
#include "stattools/commonTestCases/TExponentialTest.h"
#include "stattools/commonTestCases/commonTestingFunctions.h"
#include "gtest/gtest.h"

using namespace testing;
using namespace stattools;
using namespace stattools::prior;

//----------------------------------------
// TExponentialFixed
//----------------------------------------

struct TExponentialFixedTest : Test {
	std::string params = "1";

	using Type = coretools::Positive;
	TExponentialFixed<TParameterBase, Type, 1> prior;

	coretools::TMultiDimensionalStorage<TValueUpdated<Type>, 1> storage;
};

TEST_F(TExponentialFixedTest, initValid) {
	prior.setFixedPriorParameters(params);
	EXPECT_EQ(prior.lambda(), 1);
	params = "1.5";
	TExponentialFixed<TParameterBase, coretools::Positive, 1> prior2;
	prior2.setFixedPriorParameters(params);
	EXPECT_EQ(prior2.lambda(), 1.5);
}

TEST_F(TExponentialFixedTest, initZeroLambda) {
	params = "0";
	EXPECT_ANY_THROW(prior.setFixedPriorParameters(params));
}

TEST_F(TExponentialFixedTest, initNegativeLambda) {
	params = "-1";
	EXPECT_ANY_THROW(prior.setFixedPriorParameters(params));
}

TEST_F(TExponentialFixedTest, noLambda) {
	params = "";
	EXPECT_ANY_THROW(prior.setFixedPriorParameters(params));
}

TEST_F(TExponentialFixedTest, hastings) {
	prior.setFixedPriorParameters(params);
	storage.emplace_back(TValueUpdated<Type>(10.));
	double logDensityOld = prior.getLogDensity(storage, 0);
	storage[0]           = 5.; // update
	storage.emplace_back(TValueUpdated<Type>(1.0));

	EXPECT_EQ(prior.getLogDensityRatio(storage, 0), 5.);
	EXPECT_EQ(prior.getLogDensityRatio(storage, 0), prior.getLogDensity(storage, 0) - logDensityOld);
	EXPECT_FLOAT_EQ(prior.getDensity(storage, 1), 0.3678794);
}

//----------------------------------------
// TExponentialInferred
//----------------------------------------

class TExponentialInferredTest : public Test {
public:
	std::unique_ptr<TExponentialTest> test;

	std::unique_ptr<TExponentialTest::SpecObs> obs;
	std::unique_ptr<TExponentialTest::SpecObs> obs2;
	std::unique_ptr<TExponentialTest::SpecObs> obs3;

	void SetUp() override {
		instances::dagBuilder().clear();

		test = std::make_unique<TExponentialTest>("out");
		obs  = createObservation<TExponentialTest::Type, TExponentialTest::NumDimObs>(
			"obs", &test->boxOnObs, {21}, {"out"},
			{0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20});

		test->lambda.set(1.0);
	}

	void initializeExtraObs() {
		obs2 = createObservation<TExponentialTest::Type, TExponentialTest::NumDimObs>(
			"obs2", &test->boxOnObs, {10}, {"out"},
			{0.755181833128345, 1.18164277910711, 0.145706726703793, 0.139795261868498, 0.436068625779175,
			 2.89496853746407, 1.22956205341489, 0.539682839997113, 0.956567493698454, 0.147045990503953});
		obs3 = createObservation<TExponentialTest::Type, TExponentialTest::NumDimObs>(
			"obs3", &test->boxOnObs, {50}, {"out"},
			{0.695367564405215,  0.381014927735037,  0.618801775367415, 2.21196710884142,  0.527271583655334,
			 0.517621973015371,  0.938017586204124,  0.327373318606988, 0.168466738192365, 0.29423986072652,
			 1.18225762651182,   0.320946294115856,  0.147060193819925, 0.28293276228942,  0.0530363116413355,
			 0.0297195801977068, 0.289356231689453,  1.97946642608124,  0.586656052910385, 0.498406477621922,
			 0.717642671686538,  0.0186342631932348, 0.162005076417699, 0.660233964656896, 0.101755175015783,
			 0.511362938655025,  0.15087046707049,   0.362607151714569, 0.375771345862542, 0.117513725435484,
			 0.539940568597472,  0.514123451875007,  0.64613082383708,  0.626552677226669, 0.277320698834956,
			 0.150641498187325,  0.646562328075475,  0.497277894036329, 0.257087148027495, 1.0039162011397,
			 0.211121222469956,  1.08938628411976,   1.60889450887402,  0.278914677444845, 0.29730882588774,
			 0.488697902911503,  0.104933290276676,  0.154723928077146, 0.552968134151256, 0.387093882055438});
	}
};

TEST_F(TExponentialInferredTest, constructor) {
	EXPECT_EQ(test->boxOnObs.lambda(), 1.);
	EXPECT_TRUE(test->lambda.isPartOfBox());
}

TEST_F(TExponentialInferredTest, getDensity) {
	for (size_t i = 0; i < obs->size(); ++i) {
		EXPECT_FLOAT_EQ(
			test->boxOnObs.getDensity(obs->storage(), i),
			coretools::probdist::TExponentialDistr::density((double)obs->storage()[i], test->lambda.value()));
		EXPECT_FLOAT_EQ(
			test->boxOnObs.getLogDensity(obs->storage(), i),
			coretools::probdist::TExponentialDistr::logDensity((double)obs->storage()[i], test->lambda.value()));
	}

	double sum = obs->getSumLogPriorDensity();
	EXPECT_FLOAT_EQ(sum, -210.0);
}

TEST_F(TExponentialInferredTest, getLogDensityRatio) {
	auto storage = createUpdatedStorage<TExponentialTest::Type, TExponentialTest::NumDimObs>(obs->storage());
	storage[10]  = 5.0;

	EXPECT_EQ(test->boxOnObs.getLogDensityRatio(storage, 10), 5.);
}

TEST_F(TExponentialInferredTest, _updateLambda) {
	test->lambda.set(0.86);

	// calculate LL
	// equivalent to: sum(dexp(vals, 0.86, log = T)) - sum(dexp(vals, 1, log = T)) in R
	auto f = test->boxOnObs.calculateLLRatio(&test->lambda, 0);
	EXPECT_FLOAT_EQ(f(&obs->storage()), 26.23272);
}

TEST_F(TExponentialInferredTest, _updateLambda_3params) {
	initializeExtraObs();
	test->lambda.set(0.86);

	// calculate LL
	// equivalent to: sum(dexp(c(vals, vals2, vals3), 0.86, log = T)) - sum(dexp(c(vals, vals2, vals3), 1, log = T)) in
	// R
	auto f = test->boxOnObs.calculateLLRatio(&test->lambda, 0);
	EXPECT_FLOAT_EQ(f(&obs->storage()) + f(&obs2->storage()) + f(&obs3->storage()), 21.94169);
}

TEST_F(TExponentialInferredTest, _setLambdaToMLE_1param) {
	test->boxOnObs.guessInitialValues();

	// calculate LL
	// equivalent to: 1/mean(vals) in R
	EXPECT_FLOAT_EQ(test->boxOnObs.lambda(), 0.1);
}

TEST_F(TExponentialInferredTest, _setLambdaToMLE_3params) {
	initializeExtraObs();
	test->boxOnObs.guessInitialValues();

	// calculate LL
	// equivalent to: 1/mean(c(vals, vals2, vals3)) in R
	EXPECT_FLOAT_EQ(test->boxOnObs.lambda(), 0.3319833);
}
