//
// Created by madleina on 29.04.22.
//

#include "stattools/Priors/TPriorPoisson.h"
#include "stattools/commonTestCases/TPoissonTest.h"
#include "stattools/commonTestCases/commonTestingFunctions.h"
#include "stattools/Priors/TPriorExponential.h"
#include "gtest/gtest.h"

using namespace testing;
using namespace stattools;
using namespace stattools::prior;

//----------------------------------------
// TPoissonFixed
//----------------------------------------

struct TPoissonFixedTest : Test {
	std::string params = "1";

	using Type = coretools::UnsignedInt8;
	TPoissonFixed<TParameterBase, Type, 1> prior;

	coretools::TMultiDimensionalStorage<TValueUpdated<Type>, 1> storage;
};

TEST_F(TPoissonFixedTest, initValid) {
	prior.setFixedPriorParameters(params);
	EXPECT_EQ(prior.lambda(), 1);
	params = "1.5";
	TExponentialFixed<TParameterBase, coretools::Positive, 1> prior2;
	prior2.setFixedPriorParameters(params);
	EXPECT_EQ(prior2.lambda(), 1.5);
}

TEST_F(TPoissonFixedTest, initZeroLambda) {
	params = "0";
	EXPECT_ANY_THROW(prior.setFixedPriorParameters(params));
}

TEST_F(TPoissonFixedTest, initNegativeLambda) {
	params = "-1";
	EXPECT_ANY_THROW(prior.setFixedPriorParameters(params));
}

TEST_F(TPoissonFixedTest, noLambda) {
	params = "";
	EXPECT_ANY_THROW(prior.setFixedPriorParameters(params));
}

TEST_F(TPoissonFixedTest, hastings) {
	prior.setFixedPriorParameters(params);
	storage.emplace_back(TValueUpdated<Type>(10.));
	double logDensityOld = prior.getLogDensity(storage, 0);
	storage[0]           = 5.; // update
	storage.emplace_back(TValueUpdated<Type>(1.0));

	EXPECT_FLOAT_EQ(prior.getLogDensityRatio(storage, 0), 10.31692);
	EXPECT_FLOAT_EQ(prior.getLogDensityRatio(storage, 0), prior.getLogDensity(storage, 0) - logDensityOld);
	EXPECT_FLOAT_EQ(prior.getDensity(storage, 1), 0.3678794);
}

//----------------------------------------
// TPoissonInferred
//----------------------------------------

class TPoissonInferredTest : public Test {
public:
	std::unique_ptr<TPoissonTest> test;

	std::unique_ptr<TPoissonTest::SpecObs> obs;
	std::unique_ptr<TPoissonTest::SpecObs> obs2;
	std::unique_ptr<TPoissonTest::SpecObs> obs3;

	void SetUp() override {
		instances::dagBuilder().clear();

		test = std::make_unique<TPoissonTest>("out");
		obs  = createObservation<TPoissonTest::Type, TPoissonTest::NumDimObs>(
			"obs", &test->boxOnObs, {21}, {"out"},
			{0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20});

		// set initial values
		test->lambda.set(1.0);
		test->lambda.updateTempVals(test->lambda.getFull(), true);
	}

	void initializeExtraObs() {
		obs2 = createObservation<TPoissonTest::Type, TPoissonTest::NumDimObs>("obs2", &test->boxOnObs, {10}, {"out"},
																			  {2, 1, 0, 0, 2, 1, 0, 1, 3, 1});
		obs3 = createObservation<TPoissonTest::Type, TPoissonTest::NumDimObs>(
			"obs3", &test->boxOnObs, {50}, {"out"},
			{18, 11, 14, 6, 16, 5,  8,  10, 7,  12, 11, 12, 11, 9, 7,  13, 9, 11, 8,  8,  13, 11, 17, 10, 5,
			 9,  8,  13, 9, 11, 14, 12, 4,  12, 15, 14, 15, 13, 9, 12, 11, 8, 6,  12, 10, 7,  8,  9,  10, 8});
	}
};

TEST_F(TPoissonInferredTest, constructor) {
	EXPECT_EQ(test->boxOnObs.lambda(), 1.);
	EXPECT_TRUE(test->lambda.isPartOfBox());
}

TEST_F(TPoissonInferredTest, getDensity) {
	for (size_t i = 0; i < obs->size(); ++i) {
		EXPECT_FLOAT_EQ(test->boxOnObs.getDensity(obs->storage(), i),
						coretools::probdist::TPoissonDistr::density(obs->storage()[i], test->lambda.value()));
		EXPECT_FLOAT_EQ(test->boxOnObs.getLogDensity(obs->storage(), i),
						coretools::probdist::TPoissonDistr::logDensity(obs->storage()[i], test->lambda.value()));
	}

	double sum = obs->getSumLogPriorDensity();
	EXPECT_FLOAT_EQ(sum, -380.4458);
}

TEST_F(TPoissonInferredTest, getLogDensityRatio) {
	auto storage = createUpdatedStorage<TPoissonTest::Type, TPoissonTest::NumDimObs>(obs->storage());

	for (size_t i = 0; i < obs->size(); ++i) {
		storage[i] = 5;

		EXPECT_FLOAT_EQ(
			test->boxOnObs.getLogDensityRatio(storage, i),
			coretools::probdist::TPoissonDistr::logDensity(value(storage[i]), test->lambda.value()) -
				coretools::probdist::TPoissonDistr::logDensity(storage[i].oldValue(), test->lambda.value()));
	}
}

TEST_F(TPoissonInferredTest, _updateLambda) {
	test->lambda.set(0.86);

	// calculate LL
	// equivalent to: sum(dpois(vals, 0.86, log = T)) - sum(dpois(vals, 1, log = T)) in R
	auto f = test->boxOnObs.calculateLLRatio(&test->lambda, 0);
	EXPECT_FLOAT_EQ(f(&obs->storage()), -28.73281);
}

TEST_F(TPoissonInferredTest, _updateLambda_3params) {
	initializeExtraObs();

	test->lambda.set(0.86);

	// calculate LL
	// equivalent to: sum(dpois(c(vals, vals2, vals3), 0.86, log = T)) - sum(dpois(c(vals, vals2, vals3), 1, log = T))
	auto f = test->boxOnObs.calculateLLRatio(&test->lambda, 0);
	EXPECT_FLOAT_EQ(f(&obs->storage()) + f(&obs2->storage()) + f(&obs3->storage()), -100.5706);
}

TEST_F(TPoissonInferredTest, _setLambdaToMLE_1param) {
	test->boxOnObs.guessInitialValues();

	// calculate LL
	// equivalent to: mean(vals) in R
	EXPECT_FLOAT_EQ(test->boxOnObs.lambda(), 10.);
}

TEST_F(TPoissonInferredTest, _setLambdaToMLE_3params) {
	initializeExtraObs();

	test->boxOnObs.guessInitialValues();

	// calculate LL
	// equivalent to: mean(c(vals, vals2, vals3)) in R
	EXPECT_FLOAT_EQ(test->boxOnObs.lambda(), 9.160494);
}
