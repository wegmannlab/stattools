#include "gtest/gtest.h"

#include "stattools/commonTestCases/THMMCombinedScaledLaddersTest.h"
#include "stattools/commonTestCases/commonTestingFunctions.h"

using namespace testing;
using namespace stattools;
using namespace stattools::prior;

//--------------------------------------------
// THMMCombinedScaledLadderInferred
//--------------------------------------------

class THMMCombinedScaledLadderInferredTest : public Test {
public:
	using Type = coretools::UnsignedInt8WithMax<0>;
	using HMM  = THMMCombinedScaledLaddersTest<true, Type, TTransitionMatrixScaledLadder>;
	std::unique_ptr<HMM> test;

	std::unique_ptr<genometools::TDistancesBinnedBase> distances;
	std::unique_ptr<HMM::SpecObs> obs;
	std::unique_ptr<HMM::SpecObs> obs2;

	size_t numChains              = 2;
	std::vector<size_t> numStates = {3, 5};
	size_t numCombinedStates      = 15;

	std::vector<double> simulatedKappas = {2, 10.};
	std::vector<double> simulatedNus    = {0.01, 0.4};
	double simulatedMu                  = {0.99};

	void SetUp() override {
		instances::dagBuilder().clear();

		distances = createAndFillDistances(21, 16);
		test      = std::make_unique<HMM>("out", numStates, distances.get());

		obs = createObservation<Type, HMM::NumDimObs>(
			"obs", &test->boxOnObs, {21}, {"out"},
			{8, 3, 6, 0, 1, 12, 6, 10, 13, 1, 10, 2, 0, 4, 4, 9, 5, 13, 9, 6, 8});

		// set initial values
		test->kappa.set(0, simulatedKappas[0]);
		test->kappa.set(1, simulatedKappas[1]);
		test->nu.set(0, simulatedNus[0]);
		test->nu.set(1, simulatedNus[1]);
		test->mu.set(simulatedMu);
		test->boxOnObs.updateTau(test->boxOnObs.getParams());
	}

	void initializeExtraObs() {
		obs2 = createObservation<Type, HMM::NumDimObs>("obs2", &test->boxOnObs, {21}, {"out"},
													   {1, 1, 1, 1, 2, 1, 1, 1, 1, 1, 2, 0, 2, 0, 2, 1, 1, 0, 0, 0, 1});
	}

	auto getStorageForRatios() {
		auto storage = createUpdatedStorage<Type, HMM::NumDimObs>(obs->storage());

		std::vector<uint8_t> oldVals = {14, 4, 4, 8, 8, 13, 4, 4, 1, 9, 13, 8, 11, 14, 0, 3, 2, 5, 9, 9, 5};
		std::vector<uint8_t> newVals = {8, 3, 6, 0, 1, 12, 6, 10, 13, 1, 10, 2, 0, 4, 4, 9, 5, 13, 9, 6, 8};
		for (size_t i = 0; i < 21; i++) {
			storage[i] = oldVals[i];
			storage[i] = newVals[i];
			obs->set(i, newVals[i]);
		}

		return storage;
	}
};

TEST_F(THMMCombinedScaledLadderInferredTest, constructor) {
	EXPECT_EQ(test->boxOnObs.kappasMusNus()[0], simulatedKappas[0]);
	EXPECT_EQ(test->boxOnObs.kappasMusNus()[1], simulatedNus[0]);
	EXPECT_EQ(test->boxOnObs.kappasMusNus()[2], 1.0); // fixed mu
	EXPECT_EQ(test->boxOnObs.kappasMusNus()[3], simulatedKappas[1]);
	EXPECT_EQ(test->boxOnObs.kappasMusNus()[4], simulatedNus[1]);
	EXPECT_EQ(test->boxOnObs.kappasMusNus()[5], simulatedMu);

	EXPECT_TRUE(test->kappa.isPartOfBox());
	EXPECT_TRUE(test->nu.isPartOfBox());
	EXPECT_TRUE(test->mu.isPartOfBox());

	// check for max
	EXPECT_EQ(obs->max(), numCombinedStates - 1);
}

TEST_F(THMMCombinedScaledLadderInferredTest, constructor_ThrowIfMaxDoesntMatch) {
	Type::setMax(20);
	EXPECT_ANY_THROW(obs->tellBoxAboveToInitStorage());
}

TEST_F(THMMCombinedScaledLadderInferredTest, _fillStationaryDistribution) {
	// check stationary distribution
	std::vector<double> stationaryFromR = {
		0.00149200700745501, 0.00150707778530809, 0.00380575198310123, 0.00150707778530809, 0.00149200700745501,
		0.149200700745501,   0.150707778530809,   0.380575198310124,   0.150707778530809,   0.149200700745501,
		0.00149200700745502, 0.0015070777853081,  0.00380575198310126, 0.0015070777853081,  0.00149200700745502};
	for (size_t i = 0; i < numCombinedStates; i++) {
		EXPECT_FLOAT_EQ(test->boxOnObs.stationary(i), stationaryFromR[i]);
	}
}

TEST_F(THMMCombinedScaledLadderInferredTest, calcPOfValueGivenPrevious) {
	auto storage = getStorageForRatios();

	// check calcLogPOfValueGivenPrevious
	EXPECT_NEAR(test->boxOnObs.calcPOfValueGivenPrevious(obs->storage(), 0), (0.1507078), 0.01);   // stationary distr
	EXPECT_NEAR(test->boxOnObs.calcPOfValueGivenPrevious(obs->storage(), 1), (0.001336615), 0.01); // distGroup = 1
	EXPECT_NEAR(test->boxOnObs.calcPOfValueGivenPrevious(obs->storage(), 2), (0.1480976), 0.01);   // distGroup = 2
	EXPECT_NEAR(test->boxOnObs.calcPOfValueGivenPrevious(obs->storage(), 3), (0.001491581), 0.01); // distGroup = 3
	EXPECT_NEAR(test->boxOnObs.calcPOfValueGivenPrevious(obs->storage(), 4), (0.001554403), 0.01); // distGroup = 3
	EXPECT_NEAR(test->boxOnObs.calcPOfValueGivenPrevious(obs->storage(), 5), (0.003805746), 0.01); // distGroup = 4
	// ...
	EXPECT_NEAR(test->boxOnObs.calcPOfValueGivenPrevious(obs->storage(), 20), (0.1507078), 0.01); // distGroup = 4

	// check calcLogPOfOldValueGivenPrevious
	EXPECT_NEAR(test->boxOnObs.calcPOfOldValueGivenPrevious(storage, 0), (0.001492007), 0.01); // stationary distr
	EXPECT_NEAR(test->boxOnObs.calcPOfOldValueGivenPrevious(storage, 1), (0.001338992), 0.01); // distGroup = 1
	EXPECT_NEAR(test->boxOnObs.calcPOfOldValueGivenPrevious(storage, 2), (0.004149811), 0.01); // distGroup = 2
	EXPECT_NEAR(test->boxOnObs.calcPOfOldValueGivenPrevious(storage, 3), (0.1507086), 0.01);   // distGroup = 3
	EXPECT_NEAR(test->boxOnObs.calcPOfOldValueGivenPrevious(storage, 4), (0.1506646), 0.01);   // distGroup = 3
	EXPECT_NEAR(test->boxOnObs.calcPOfOldValueGivenPrevious(storage, 5), (0.001507075), 0.01); // distGroup = 4
	// ...
	EXPECT_NEAR(test->boxOnObs.calcPOfOldValueGivenPrevious(storage, 20), (0.1492007), 0.01); // distGroup = 4
}

TEST_F(THMMCombinedScaledLadderInferredTest, calcLogPOfValueGivenPrevious) {
	auto storage = getStorageForRatios();

	// check calcLogPOfValueGivenPrevious
	EXPECT_NEAR(test->boxOnObs.calcLogPOfValueGivenPrevious(obs->storage(), 0), log(0.1507078),
				0.01); // stationary distr
	EXPECT_NEAR(test->boxOnObs.calcLogPOfValueGivenPrevious(obs->storage(), 1), log(0.001336615),
				0.01); // distGroup = 1
	EXPECT_NEAR(test->boxOnObs.calcLogPOfValueGivenPrevious(obs->storage(), 2), log(0.1480976),
				0.01); // distGroup = 2
	EXPECT_NEAR(test->boxOnObs.calcLogPOfValueGivenPrevious(obs->storage(), 3), log(0.001491581),
				0.01); // distGroup = 3
	EXPECT_NEAR(test->boxOnObs.calcLogPOfValueGivenPrevious(obs->storage(), 4), log(0.001554403),
				0.01); // distGroup = 3
	EXPECT_NEAR(test->boxOnObs.calcLogPOfValueGivenPrevious(obs->storage(), 5), log(0.003805746),
				0.01); // distGroup = 4
	// ...
	EXPECT_NEAR(test->boxOnObs.calcLogPOfValueGivenPrevious(obs->storage(), 20), log(0.1507078),
				0.01); // distGroup = 4

	// check calcLogPOfOldValueGivenPrevious
	EXPECT_NEAR(test->boxOnObs.calcLogPOfOldValueGivenPrevious(storage, 0), log(0.001492007),
				0.01); // stationary distr
	EXPECT_NEAR(test->boxOnObs.calcLogPOfOldValueGivenPrevious(storage, 1), log(0.001338992),
				0.01); // distGroup = 1
	EXPECT_NEAR(test->boxOnObs.calcLogPOfOldValueGivenPrevious(storage, 2), log(0.004149811),
				0.01);                                                                             // distGroup = 2
	EXPECT_NEAR(test->boxOnObs.calcLogPOfOldValueGivenPrevious(storage, 3), log(0.1507086), 0.01); // distGroup = 3
	EXPECT_NEAR(test->boxOnObs.calcLogPOfOldValueGivenPrevious(storage, 4), log(0.1506646), 0.01); // distGroup = 3
	EXPECT_NEAR(test->boxOnObs.calcLogPOfOldValueGivenPrevious(storage, 5), log(0.001507075),
				0.01); // distGroup = 4
	// ...
	EXPECT_NEAR(test->boxOnObs.calcLogPOfOldValueGivenPrevious(storage, 20), log(0.1492007), 0.01); // distGroup = 4
}

TEST_F(THMMCombinedScaledLadderInferredTest, calcPOfNextGivenValue) {
	auto storage = getStorageForRatios();

	// check calcLogPOfNextGivenValue
	EXPECT_NEAR(test->boxOnObs.calcPOfNextGivenValue(obs->storage(), 0), (0.001336615), 0.01); // distGroup next = 1
	EXPECT_NEAR(test->boxOnObs.calcPOfNextGivenValue(obs->storage(), 1), (0.1480976), 0.01);   // distGroup next = 2
	EXPECT_NEAR(test->boxOnObs.calcPOfNextGivenValue(obs->storage(), 2), (0.001491581), 0.01); // distGroup next = 3
	EXPECT_NEAR(test->boxOnObs.calcPOfNextGivenValue(obs->storage(), 3), (0.001554403), 0.01); // distGroup next = 3
	EXPECT_NEAR(test->boxOnObs.calcPOfNextGivenValue(obs->storage(), 4), (0.003805746), 0.01); // distGroup next = 4
	EXPECT_NEAR(test->boxOnObs.calcPOfNextGivenValue(obs->storage(), 5), (0.1507078), 0.01);   // distGroup next = 4
	// ...
	EXPECT_NEAR(test->boxOnObs.calcPOfNextGivenValue(obs->storage(), 20), 1., 0.01); // there is no next

	// check calcLogPOfNextGivenOldValue
	EXPECT_NEAR(test->boxOnObs.calcPOfNextGivenOldValue(storage, 0), (0.000931769), 0.01); // distGroup next = 1
	EXPECT_NEAR(test->boxOnObs.calcPOfNextGivenOldValue(storage, 1), (0.1480589), 0.01);   // distGroup next = 2
	EXPECT_NEAR(test->boxOnObs.calcPOfNextGivenOldValue(storage, 2), (0.001538858), 0.01); // distGroup next = 3
	EXPECT_NEAR(test->boxOnObs.calcPOfNextGivenOldValue(storage, 3), (0.001506647), 0.01); // distGroup next = 3
	EXPECT_NEAR(test->boxOnObs.calcPOfNextGivenOldValue(storage, 4), (0.003805752), 0.01); // distGroup next = 4
	EXPECT_NEAR(test->boxOnObs.calcPOfNextGivenOldValue(storage, 5), (0.1507078), 0.01);   // distGroup next = 4
	// ...
	EXPECT_NEAR(test->boxOnObs.calcPOfNextGivenOldValue(storage, 20), 1., 0.01); // there is no next
}

TEST_F(THMMCombinedScaledLadderInferredTest, calcLogPOfNextGivenValue) {
	auto storage = getStorageForRatios();

	// check calcLogPOfNextGivenValue
	EXPECT_NEAR(test->boxOnObs.calcLogPOfNextGivenValue(obs->storage(), 0), log(0.001336615),
				0.01); // distGroup next = 1
	EXPECT_NEAR(test->boxOnObs.calcLogPOfNextGivenValue(obs->storage(), 1), log(0.1480976),
				0.01); // distGroup next = 2
	EXPECT_NEAR(test->boxOnObs.calcLogPOfNextGivenValue(obs->storage(), 2), log(0.001491581),
				0.01); // distGroup next = 3
	EXPECT_NEAR(test->boxOnObs.calcLogPOfNextGivenValue(obs->storage(), 3), log(0.001554403),
				0.01); // distGroup next = 3
	EXPECT_NEAR(test->boxOnObs.calcLogPOfNextGivenValue(obs->storage(), 4), log(0.003805746),
				0.01); // distGroup next = 4
	EXPECT_NEAR(test->boxOnObs.calcLogPOfNextGivenValue(obs->storage(), 5), log(0.1507078),
				0.01); // distGroup next = 4
	// ...
	EXPECT_NEAR(test->boxOnObs.calcLogPOfNextGivenValue(obs->storage(), 20), log(1.), 0.01); // there is no next

	// check calcLogPOfNextGivenOldValue
	EXPECT_NEAR(test->boxOnObs.calcLogPOfNextGivenOldValue(storage, 0), log(0.000931769),
				0.01);                                                                         // distGroup next = 1
	EXPECT_NEAR(test->boxOnObs.calcLogPOfNextGivenOldValue(storage, 1), log(0.1480589), 0.01); // distGroup next = 2
	EXPECT_NEAR(test->boxOnObs.calcLogPOfNextGivenOldValue(storage, 2), log(0.001538858),
				0.01); // distGroup next = 3
	EXPECT_NEAR(test->boxOnObs.calcLogPOfNextGivenOldValue(storage, 3), log(0.001506647),
				0.01); // distGroup next = 3
	EXPECT_NEAR(test->boxOnObs.calcLogPOfNextGivenOldValue(storage, 4), log(0.003805752),
				0.01);                                                                         // distGroup next = 4
	EXPECT_NEAR(test->boxOnObs.calcLogPOfNextGivenOldValue(storage, 5), log(0.1507078), 0.01); // distGroup next = 4
	// ...
	EXPECT_NEAR(test->boxOnObs.calcLogPOfNextGivenOldValue(storage, 20), log(1.), 0.01); // there is no next
}

TEST_F(THMMCombinedScaledLadderInferredTest, getLogDensityRatio) {
	auto storage = getStorageForRatios();

	// correct logPriorDensity?
	EXPECT_NEAR(test->boxOnObs.getLogDensity(obs->storage(), 0), log(0.1507078) + log(0.001336615), 0.01);
	EXPECT_NEAR(test->boxOnObs.getLogDensity(obs->storage(), 1), log(0.001336615) + log(0.1480976), 0.01);
	EXPECT_NEAR(test->boxOnObs.getLogDensity(obs->storage(), 2), log(0.1480976) + log(0.001491581), 0.01);
	EXPECT_NEAR(test->boxOnObs.getLogDensity(obs->storage(), 3), log(0.001491581) + log(0.001554403), 0.01);
	EXPECT_NEAR(test->boxOnObs.getLogDensity(obs->storage(), 4), log(0.001554403) + log(0.003805746), 0.01);
	EXPECT_NEAR(test->boxOnObs.getLogDensity(obs->storage(), 5), log(0.003805746) + log(0.1507078), 0.01);
	// ...
	EXPECT_NEAR(test->boxOnObs.getLogDensity(obs->storage(), 20), log(0.1507078) + 0., 0.01);

	// correct priorDensity?
	EXPECT_NEAR(test->boxOnObs.getDensity(obs->storage(), 0), (0.1507078) * (0.001336615), 0.01);
	EXPECT_NEAR(test->boxOnObs.getDensity(obs->storage(), 1), (0.001336615) * (0.1480976), 0.01);
	EXPECT_NEAR(test->boxOnObs.getDensity(obs->storage(), 2), (0.1480976) * (0.001491581), 0.01);
	EXPECT_NEAR(test->boxOnObs.getDensity(obs->storage(), 3), (0.001491581) * (0.001554403), 0.01);
	EXPECT_NEAR(test->boxOnObs.getDensity(obs->storage(), 4), (0.001554403) * (0.003805746), 0.01);
	EXPECT_NEAR(test->boxOnObs.getDensity(obs->storage(), 5), (0.003805746) * (0.1507078), 0.01);
	// ...
	EXPECT_NEAR(test->boxOnObs.getDensity(obs->storage(), 20), (0.1507078) * 1., 0.01);

	// correct logPriorRatio?
	EXPECT_NEAR(test->boxOnObs.getLogDensityRatio(storage, 0),
				log(0.1507078) - log(0.001492007) + log(0.001336615) - log(0.000931769), 0.01);
	EXPECT_NEAR(test->boxOnObs.getLogDensityRatio(storage, 1),
				log(0.001336615) - log(0.001338992) + log(0.1480976) - log(0.1480589), 0.01);
	EXPECT_NEAR(test->boxOnObs.getLogDensityRatio(storage, 2),
				log(0.1480976) - log(0.004149811) + log(0.001491581) - log(0.001538858), 0.01);
	EXPECT_NEAR(test->boxOnObs.getLogDensityRatio(storage, 3),
				log(0.001491581) - log(0.1507086) + log(0.001554403) - log(0.001506647), 0.01);
	EXPECT_NEAR(test->boxOnObs.getLogDensityRatio(storage, 4),
				log(0.001554403) - log(0.1506646) + log(0.003805746) - log(0.003805752), 0.01);
	EXPECT_NEAR(test->boxOnObs.getLogDensityRatio(storage, 5),
				log(0.003805746) - log(0.001507075) + log(0.1507078) - log(0.1507078), 0.01);
	// ...
	EXPECT_NEAR(test->boxOnObs.getLogDensityRatio(storage, 20), log(0.1507078) - log(0.1492007) + 0. - 0., 0.01);
}

TEST_F(THMMCombinedScaledLadderInferredTest, _updateKappa) {
	test->kappa.set(0, 0.3);

	// calculate LL
	auto f = test->boxOnObs.calculateLLRatio(&test->kappa, coretools::TRange(0));
	EXPECT_NEAR(f(&obs->storage()), -89.55604 - (-97.95248), 0.01);
}

TEST_F(THMMCombinedScaledLadderInferredTest, _updateMu) {
	test->mu.set(0, 0.3);

	// calculate LL
	auto f = test->boxOnObs.calculateLLRatio(&test->mu, coretools::TRange(1));
	EXPECT_NEAR(f(&obs->storage()), -117.2255 - (-97.95248), 0.01);
}

TEST_F(THMMCombinedScaledLadderInferredTest, _updateNu) {
	test->nu.set(1, 0.3);

	// calculate LL
	auto f = test->boxOnObs.calculateLLRatio(&test->nu, coretools::TRange(0));
	EXPECT_NEAR(f(&obs->storage()), -99.93001 - (-97.95248), 0.01);
}

TEST_F(THMMCombinedScaledLadderInferredTest, densities) {
	double sum = test->boxOnObs.getSumLogPriorDensity(obs->storage());
	EXPECT_NEAR(sum, -97.95248, 0.01);
}
