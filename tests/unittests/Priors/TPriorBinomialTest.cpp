//
// Created by caduffm on 3/28/22.
//

#include "gtest/gtest.h"

#include "coretools/Distributions/TBinomialDistr.h"
#include "stattools/commonTestCases/TBinomialTest.h"
#include "stattools/commonTestCases/commonTestingFunctions.h"
#include "stattools/Priors/TPriorBinomial.h"

using namespace testing;
using namespace stattools;
using namespace stattools::prior;
using coretools::P;
using coretools::probdist::TBinomialDistr;

//-------------------------------------------------
// TBinomialFixed
//-------------------------------------------------

class TBinomialFixedTest : public Test {
public:
	using TypeObs                     = coretools::UnsignedInt8;
	static constexpr size_t NumDimObs = 2;
	using BoxType                     = TBinomialFixed<TObservationBase, TypeObs, NumDimObs>;

	std::shared_ptr<BoxType> boxOnObs;
	std::unique_ptr<TObservation<TypeObs, NumDimObs, BoxType>> obs;

	void SetUp() override {
		instances::dagBuilder().clear();

		boxOnObs = std::make_shared<BoxType>();
		obs      = createObservation<TypeObs, NumDimObs>("obs", boxOnObs.get(), {10, 2}, {"out", "0.3"},
														 {2, 0, 3, 0, 5, 2, 9, 2, 2, 1, 8, 2, 9, 3, 6, 5, 6, 1, 0, 0});
	}
};

TEST_F(TBinomialFixedTest, initValid) { EXPECT_EQ(boxOnObs->p(), 0.3); }

TEST_F(TBinomialFixedTest, invalidP) {
	// p can't be negative
	std::string params = "-1";
	EXPECT_ANY_THROW(boxOnObs->setFixedPriorParameters(params));
	// p can't be 0
	params = "0.";
	EXPECT_ANY_THROW(boxOnObs->setFixedPriorParameters(params));
	// p can't be 1
	params = "1.";
	EXPECT_ANY_THROW(boxOnObs->setFixedPriorParameters(params));
	// p can't be >1
	params = "1.2";
	EXPECT_ANY_THROW(boxOnObs->setFixedPriorParameters(params));

	EXPECT_ANY_THROW(boxOnObs->setFixedPriorParameters(""));
}

TEST_F(TBinomialFixedTest, getDensity) {
	EXPECT_FLOAT_EQ(boxOnObs->getLogDensity(obs->storage(), 0), -0.7133499);
	EXPECT_FLOAT_EQ(boxOnObs->getLogDensity(obs->storage(), 4 * 2), -0.8675006);
	EXPECT_NEAR(boxOnObs->getLogDensity(obs->storage(), 9 * 2), 0.,
				10e-15); // gammaLog does not return exactly 0, but 10e-16 -> float_eq doesn't work

	EXPECT_FLOAT_EQ(boxOnObs->getDensity(obs->storage(), 0), exp(-0.7133499));
	EXPECT_FLOAT_EQ(boxOnObs->getDensity(obs->storage(), 4 * 2), exp(-0.8675006));
	EXPECT_NEAR(boxOnObs->getDensity(obs->storage(), 9 * 2), exp(0.),
				10e-15); // gammaLog does not return exactly 0, but 10e-16 -> float_eq doesn't work
}

TEST_F(TBinomialFixedTest, hastings) {
	auto storage          = createUpdatedStorage<TypeObs, NumDimObs>(obs->storage());
	std::vector<int> newK = {1, 2, 1, 3, 0, 2, 2, 0, 1, 0};
	for (size_t k = 0; k < 10; k++) { storage[k * 2 + 1] = newK[k]; }

	EXPECT_FLOAT_EQ(boxOnObs->getLogDensityRatio(storage, 0), -0.1541507);
	EXPECT_FLOAT_EQ(boxOnObs->getLogDensityRatio(storage, 4 * 2), 0.1541507);
	EXPECT_FLOAT_EQ(boxOnObs->getLogDensityRatio(storage, 9 * 2), 0.);
}

//-------------------------------------------------
// TBinomialInferred
//-------------------------------------------------

class TBinomialInferredTest : public Test {
public:
	std::unique_ptr<TBinomialTest> test;

	std::unique_ptr<TBinomialTest::SpecObs> obs;
	std::unique_ptr<TBinomialTest::SpecObs> obs2;
	std::unique_ptr<TBinomialTest::SpecObs> obs3;

	void SetUp() override {
		instances::dagBuilder().clear();

		TBinomialTest::Type::setMax(10);

		test = std::make_unique<TBinomialTest>("out");
		obs  = createObservation<TBinomialTest::Type, TBinomialTest::NumDimObs>(
			"obs", &test->boxOnObs, {21, 2}, {"out"}, {2, 0, 3, 1, 5, 0, 9, 2, 2, 0, 8, 0, 9, 2, 6, 3, 6, 1, 0, 0, 2,
														0, 1, 0, 6, 2, 3, 0, 7, 3, 4, 2, 7, 3, 9, 1, 3, 1, 7, 2, 9, 4});

		// set initial values
		test->p.set(0.3);
		test->p.updateTempVals(coretools::TRange(0), true);
	}

	void initializeExtraObs() {
		obs2 = createObservation<TBinomialTest::Type, TBinomialTest::NumDimObs>(
			"obs2", &test->boxOnObs, {10, 2}, {"out"}, {2, 1, 4, 1, 3, 0, 6, 2, 2, 1, 4, 2, 7, 2, 0, 0, 8, 3, 3, 2});
		obs3 = createObservation<TBinomialTest::Type, TBinomialTest::NumDimObs>(
			"obs3", &test->boxOnObs, {50, 2}, {"out"},
			{4, 1, 7, 0, 3, 0, 3, 1, 7, 4, 2, 1, 7, 2, 1, 0, 2, 2, 1, 0, 2, 1, 0, 0, 6, 2, 8, 1, 7, 1, 7, 3, 4, 1,
			 4, 0, 8, 3, 6, 0, 6, 3, 3, 1, 2, 1, 9, 2, 6, 2, 2, 1, 1, 0, 4, 1, 9, 1, 5, 1, 9, 2, 7, 1, 3, 2, 4, 1,
			 1, 1, 0, 0, 7, 4, 1, 0, 4, 0, 6, 1, 9, 3, 4, 1, 4, 1, 1, 1, 7, 3, 4, 1, 5, 1, 2, 1, 2, 1, 5, 2});
	}
};

TEST_F(TBinomialInferredTest, constructor) {
	EXPECT_EQ(test->boxOnObs.p(), 0.3);

	EXPECT_TRUE(test->p.isPartOfBox());
}

TEST_F(TBinomialInferredTest, getDensity) {
	for (size_t i = 0; i < obs->size(); i += 2) {
		EXPECT_FLOAT_EQ(test->boxOnObs.getDensity(obs->storage(), i),
						TBinomialDistr::density(obs->storage()[i], obs->storage()[i + 1],
												P(test->p.value())));
		EXPECT_FLOAT_EQ(test->boxOnObs.getLogDensity(obs->storage(), i),
						TBinomialDistr::logDensity(obs->storage()[i], obs->storage()[i + 1],
												   P(test->p.value())));
	}

	double sum = test->boxOnObs.getSumLogPriorDensity(obs->storage());
	EXPECT_FLOAT_EQ(sum, -25.55765);
}

TEST_F(TBinomialInferredTest, getLogDensityRatio) {
	auto storage = createUpdatedStorage<TBinomialTest::Type, TBinomialTest::NumDimObs>(obs->storage());

	// update k
	std::vector<uint32_t> newK = {1, 1, 2, 3, 1, 0, 3, 2, 2, 0, 0, 1, 2, 0, 0, 0, 1, 3, 1, 2, 5};
	size_t c                   = 0;
	for (size_t i = 0; i < obs->size(); i += 2, ++c) { storage[i + 1] = newK[c]; }

	EXPECT_FLOAT_EQ(test->boxOnObs.getLogDensityRatio(storage, 0), -0.1541507);
	EXPECT_FLOAT_EQ(test->boxOnObs.getLogDensityRatio(storage, 9 * 2), 0.0);
	EXPECT_FLOAT_EQ(test->boxOnObs.getLogDensityRatio(storage, 19 * 2), 0.0);
	EXPECT_FLOAT_EQ(test->boxOnObs.getLogDensityRatio(storage, 20 * 2), -0.8472979);
}

TEST_F(TBinomialInferredTest, updateP) {
	test->p.set(0.4);
	test->p.set(0.86);

	// calculate LL
	// equivalent to: sum(dbinom(k, n, 0.86, log = T)) - sum(dbinom(k, n, 0.4, log = T)) in R
	EXPECT_FLOAT_EQ(test->boxOnObs.calculateLLRatio(&test->p, 0)(&obs->storage()), -97.21063);
}

TEST_F(TBinomialInferredTest, _setPToMLE_1param) {
	test->boxOnObs.guessInitialValues();

	// equivalent to: sum(k) / (sum(n))
	EXPECT_FLOAT_EQ(test->p.value(), 0.25);
}

TEST_F(TBinomialInferredTest, _setPiToMLE_3params) {
	initializeExtraObs();
	test->boxOnObs.guessInitialValues();

	// equivalent to: (sum(k) + sum(k2) + sum(k3)) / (sum(n) + sum(n2) + sum(n3)) in R
	EXPECT_FLOAT_EQ(test->p.value(), 0.2826087);
}
