//
// Created by madleina on 29.04.22.
//

#include "stattools/Priors/TPriorGamma.h"
#include "stattools/commonTestCases/TGammaTest.h"
#include "stattools/commonTestCases/commonTestingFunctions.h"

#include "gtest/gtest.h"

using namespace testing;
using namespace stattools;
using namespace stattools::prior;

//----------------------------------------
// TGammaFixed
//----------------------------------------

struct TGammaFixedTest : Test {
	std::string params = "0.3,0.7";

	using Type = coretools::StrictlyPositive;
	TGammaFixed<TParameterBase, Type, 1> prior;

	coretools::TMultiDimensionalStorage<TValueUpdated<Type>, 1> storage;
};

TEST_F(TGammaFixedTest, initValid) {
	prior.setFixedPriorParameters(params);
	EXPECT_EQ(prior.alpha(), 0.3);
	EXPECT_EQ(prior.beta(), 0.7);
	params = "1.5, 9.8";
	TGammaFixed<TParameterBase, coretools::StrictlyPositive, 1> prior2;
	prior2.setFixedPriorParameters(params);
	EXPECT_EQ(prior2.alpha(), 1.5);
	EXPECT_EQ(prior2.beta(), 9.8);
}

TEST_F(TGammaFixedTest, initZero) {
	params = "0,0.2";
	EXPECT_ANY_THROW(prior.setFixedPriorParameters(params));
	params = "0.2,0";
	EXPECT_ANY_THROW(prior.setFixedPriorParameters(params));
}

TEST_F(TGammaFixedTest, initNegative) {
	params = "-1,0.2";
	EXPECT_ANY_THROW(prior.setFixedPriorParameters(params));
	params = "0.2,-1";
	EXPECT_ANY_THROW(prior.setFixedPriorParameters(params));
}

TEST_F(TGammaFixedTest, noParams) {
	params = "";
	EXPECT_ANY_THROW(prior.setFixedPriorParameters(params));
}

TEST_F(TGammaFixedTest, hastings) {
	prior.setFixedPriorParameters(params);
	storage.emplace_back(TValueUpdated<Type>(10.));
	double logDensityOld = prior.getLogDensity(storage, 00);
	storage[0]           = 5.; // update
	storage.emplace_back(TValueUpdated<Type>(1.0));

	EXPECT_FLOAT_EQ(prior.getLogDensityRatio(storage, 0), 3.985203);
	EXPECT_FLOAT_EQ(prior.getLogDensityRatio(storage, 0), prior.getLogDensity(storage, 0) - logDensityOld);
	EXPECT_FLOAT_EQ(prior.getDensity(storage, 1), 0.1491503);
}

//----------------------------------------
// TGammaInferred
//----------------------------------------

class TGammaInferredTest : public Test {
public:
	std::unique_ptr<TGammaTest> test;

	std::unique_ptr<TGammaTest::SpecObs> obs;
	std::unique_ptr<TGammaTest::SpecObs> obs2;
	std::unique_ptr<TGammaTest::SpecObs> obs3;

	void SetUp() override {
		instances::dagBuilder().clear();

		test = std::make_unique<TGammaTest>("out");
		obs  = createObservation<TGammaTest::Type, TGammaTest::NumDimObs>(
			"obs", &test->boxOnObs, {21}, {"out"},
			{0.161250930153163,  0.599646306260543, 0.620970975991668, 0.326760053197426, 0.164105438954776,
			  0.0966583348777117, 0.354001508711023, 0.51329193327213,  0.303669588115648, 0.251931167506274,
			  0.392696267254173,  0.241907914777128, 0.588262144360358, 0.254236049815642, 0.0824571504279991,
			  0.465613964311862,  0.244667634977281, 1.00031335044611,  0.411367512030778, 0.233115564282051,
			  0.835467658802988});

		// set initial values
		test->alpha.set(3.0);
		test->beta.set(7.0);
	}

	void initializeExtraObs() {
		obs2 = createObservation<TGammaTest::Type, TGammaTest::NumDimObs>(
			"obs2", &test->boxOnObs, {10}, {"out"},
			{0.482959191507289, 0.153945311565638, 0.327726715894217, 0.430992696643142, 0.331417231019611,
			 0.214266334084801, 0.68914308285404, 0.600783123934697, 0.27552623762587, 1.03202270709486});
		obs3 = createObservation<TGammaTest::Type, TGammaTest::NumDimObs>(
			"obs3", &test->boxOnObs, {50}, {"out"},
			{0.212902741149737,  0.570729040735233, 0.405731608095689,  0.672042776538739, 0.392215343980774,
			 0.140589308642015,  0.352871298719718, 0.30226957617955,   0.222418887442883, 0.553353051225206,
			 0.44802091898755,   0.1396044893351,   0.10812545371654,   0.19694332340138,  0.367532812160436,
			 0.235733312122155,  0.474704785912938, 0.276512521415417,  0.337054415326772, 0.17077638626778,
			 0.956859324777371,  0.266352738746179, 0.15852707880903,   0.131542473560303, 0.516887665922852,
			 0.0615467589223317, 0.324467361429561, 0.438335045004807,  0.547681588604201, 0.354390468872564,
			 0.907481212655827,  0.27395763275716,  0.0996551147563491, 0.340290222833072, 1.16290619252044,
			 0.320252195258,     0.393942383841631, 0.559867364527359,  0.336495481805599, 0.338338840040551,
			 0.4012735378567,    0.20223911725585,  0.924927003939392,  0.429714009585099, 0.67610287870324,
			 0.0834357828408634, 0.114070093162652, 0.454158326504467,  0.517304774073869, 0.785789435208719});
	}
};

TEST_F(TGammaInferredTest, constructor) {
	EXPECT_EQ(test->boxOnObs.alpha(), 3.);
	EXPECT_EQ(test->boxOnObs.beta(), 7.);

	EXPECT_TRUE(test->alpha.isPartOfBox());
	EXPECT_TRUE(test->beta.isPartOfBox());
}

TEST_F(TGammaInferredTest, getDensity) {
	for (size_t i = 0; i < obs->size(); ++i) {
		EXPECT_FLOAT_EQ(test->boxOnObs.getDensity(obs->storage(), i),
						coretools::probdist::TGammaDistr::density((double)obs->storage()[i], test->alpha.value(),
																  test->beta.value()));
		EXPECT_FLOAT_EQ(test->boxOnObs.getLogDensity(obs->storage(), i),
						coretools::probdist::TGammaDistr::logDensity((double)obs->storage()[i], test->alpha.value(),
																	 test->beta.value()));
	}

	double sum = obs->getSumLogPriorDensity();
	EXPECT_FLOAT_EQ(sum, 3.446573);
}

TEST_F(TGammaInferredTest, getLogDensityRatio) {
	auto storage = createUpdatedStorage<TGammaTest::Type, TGammaTest::NumDimObs>(obs->storage());
	storage[10]  = 5.0;
	EXPECT_FLOAT_EQ(test->boxOnObs.getLogDensityRatio(storage, 10), -27.16281);
}

TEST_F(TGammaInferredTest, _updateAlpha) {
	test->alpha.set(3.9);

	// calculate LL
	// equivalent to: sum(dgamma(vals, 3.9, 7, log = T)) - sum(dgamma(vals, 3, 7, log = T)) in R
	auto f = test->boxOnObs.calculateLLRatio(&test->alpha, 0);
	EXPECT_FLOAT_EQ(f(&obs->storage()), -5.102216);
}

TEST_F(TGammaInferredTest, _updateAlpha_3params) {
	initializeExtraObs();
	test->alpha.set(3.9);

	// calculate LL
	// equivalent to: sum(dgamma(c(vals, vals2, vals3), 3.9, 7, log = T)) - sum(dgamma(c(vals, vals2, vals3), 3, 7, log
	// = T))
	// R
	auto f = test->boxOnObs.calculateLLRatio(&test->alpha, 0);
	EXPECT_FLOAT_EQ(f(&obs->storage()) + f(&obs2->storage()) + f(&obs3->storage()), -17.73059);
}

TEST_F(TGammaInferredTest, _updateBeta) {
	test->beta.set(5.9);

	// calculate LL
	// equivalent to: sum(dgamma(vals, 3, 5.9, log = T)) - sum(dgamma(vals, 3, 7, log = T)) in R
	auto f = test->boxOnObs.calculateLLRatio(&test->beta, 0);
	EXPECT_FLOAT_EQ(f(&obs->storage()), -1.813711);
}

TEST_F(TGammaInferredTest, _updateBeta_3params) {
	initializeExtraObs();

	test->beta.set(5.9);

	// calculate LL
	// equivalent to: sum(dgamma(c(vals, vals2, vals3), 3, 5.9, log = T)) - sum(dgamma(c(vals, vals2, vals3), 3, 7, log
	// = T))
	// R
	auto f = test->boxOnObs.calculateLLRatio(&test->beta, 0);
	EXPECT_FLOAT_EQ(f(&obs->storage()) + f(&obs2->storage()) + f(&obs3->storage()), -5.968637);
}

TEST_F(TGammaInferredTest, _setAlphaBetaToMixedTypeLogMomentEstimates) {
	test->boxOnObs.guessInitialValues();

	EXPECT_FLOAT_EQ(test->alpha.value(), 2.816228);
	EXPECT_FLOAT_EQ(test->beta.value(), 7.26332);
}

TEST_F(TGammaInferredTest, _setAlphaBetaToMixedTypeLogMomentEstimates_3params) {
	initializeExtraObs();

	test->boxOnObs.guessInitialValues();

	EXPECT_FLOAT_EQ(test->alpha.value(), 2.782495);
	EXPECT_FLOAT_EQ(test->beta.value(), 6.969123);
}
