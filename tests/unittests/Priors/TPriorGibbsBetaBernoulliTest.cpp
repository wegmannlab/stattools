#include "stattools/commonTestCases/TBernoulliTest.h"
#include "stattools/commonTestCases/commonTestingFunctions.h"
#include "gtest/gtest.h"

using namespace testing;
using namespace stattools;
using namespace stattools::prior;

//--------------------------------------
// TGibbsBetaBernoulliInferred
//--------------------------------------

class TGibbsBetaBernoulliInferredTest : public Test {
public:
	std::unique_ptr<TGibbsBetaBernoulliTest> test;

	std::unique_ptr<TGibbsBetaBernoulliTest::SpecObs> obs;
	std::unique_ptr<TGibbsBetaBernoulliTest::SpecObs> obs2;
	std::unique_ptr<TGibbsBetaBernoulliTest::SpecObs> obs3;

	void SetUp() override {
		instances::dagBuilder().clear();

		test = std::make_unique<TGibbsBetaBernoulliTest>("out");
		obs  = createObservation<TGibbsBetaBernoulliTest::Type, TGibbsBetaBernoulliTest::NumDimObs>(
			"obs", &test->boxOnObs, {21}, {"out"},
			{false, false, false, false, true,  false, false, false, true,  false, false,
			  false, true,  false, false, false, false, false, false, false, false});

		// set initial values
		test->pi.set(0.4);
		test->pi.updateTempVals(coretools::TRange(0), true);
	}

	void initializeExtraObs() {
		obs2 = createObservation<TGibbsBetaBernoulliTest::Type, TGibbsBetaBernoulliTest::NumDimObs>(
			"obs2", &test->boxOnObs, {10}, {"out"}, {false, false, true, true, false, true, true, true, true, false});
		obs3 = createObservation<TGibbsBetaBernoulliTest::Type, TGibbsBetaBernoulliTest::NumDimObs>(
			"obs3", &test->boxOnObs, {50}, {"out"},
			{false, false, false, false, false, false, false, true,  false, false, true,  false, false,
			 false, false, false, false, false, false, false, false, false, false, false, false, false,
			 false, false, false, false, false, false, false, false, false, false, false, false, false,
			 false, false, false, false, false, false, false, false, false, false, false});
	}
};

TEST_F(TGibbsBetaBernoulliInferredTest, constructor) {
	EXPECT_EQ(test->boxOnObs.pi(), 0.4);
	EXPECT_TRUE(test->pi.isPartOfBox());
}

TEST_F(TGibbsBetaBernoulliInferredTest, getDensity) {
	for (size_t i = 0; i < obs->size(); ++i) {
		if (obs->storage()[i] == 0) {
			EXPECT_FLOAT_EQ(test->boxOnObs.getDensity(obs->storage(), i), 0.6);
			EXPECT_FLOAT_EQ(test->boxOnObs.getLogDensity(obs->storage(), i), log(0.6));
		} else {
			EXPECT_FLOAT_EQ(test->boxOnObs.getDensity(obs->storage(), i), 0.4);
			EXPECT_FLOAT_EQ(test->boxOnObs.getLogDensity(obs->storage(), i), log(0.4));
		}
	}

	double sum = test->boxOnObs.getSumLogPriorDensity(obs->storage());
	EXPECT_FLOAT_EQ(sum, -11.94373);

	sum = obs->getSumLogPriorDensity() + test->pi.getSumLogPriorDensity();
	EXPECT_FLOAT_EQ(sum, -11.94373 - 0.1566568);
}

TEST_F(TGibbsBetaBernoulliInferredTest, getLogDensityRatio) {
	auto storage =
		createUpdatedStorage<TGibbsBetaBernoulliTest::Type, TGibbsBetaBernoulliTest::NumDimObs>(obs->storage());
	storage[0] = true; // update false -> true
	EXPECT_FLOAT_EQ(test->boxOnObs.getLogDensityRatio(storage, 0), -0.4054651);

	storage[4] = false; // update true -> false
	EXPECT_FLOAT_EQ(test->boxOnObs.getLogDensityRatio(storage, 4), 0.4054651);
}

TEST_F(TGibbsBetaBernoulliInferredTest, samplePi) {
	// check if sampling from posterior results approximately in posterior probabilities
	size_t numRep = 100000;
	double sum    = 0.0;
	for (size_t i = 0; i < numRep; i++) {
		test->boxOnObs.doGibbs(&test->pi, 0);
		sum += test->pi.value();
	}

	EXPECT_NEAR(sum / (double)numRep, 0.1647, 0.01);
}

TEST_F(TGibbsBetaBernoulliInferredTest, _updatePi_3params) {
	initializeExtraObs();

	size_t numRep = 100000;
	double sum    = 0.0;
	for (size_t i = 0; i < numRep; i++) {
		test->boxOnObs.doGibbs(&test->pi, 0);
		sum += test->pi.value();
	}

	EXPECT_NEAR(sum / (double)numRep, 0.1419, 0.01);
}

TEST_F(TGibbsBetaBernoulliInferredTest, _setPiToMLE_1param) {
	test->boxOnObs.guessInitialValues();

	// equivalent to: mean(vals) in R
	EXPECT_FLOAT_EQ(test->pi.value(), 0.1428571);
}

TEST_F(TGibbsBetaBernoulliInferredTest, _setPiToMLE_3params) {
	initializeExtraObs();
	test->boxOnObs.guessInitialValues();

	// equivalent to: mean(c(vals, vals2, vals3)) in R
	EXPECT_FLOAT_EQ(test->pi.value(), 0.1358025);
}
