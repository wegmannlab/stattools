#include "stattools/commonTestCases/THMMBoolGeneratingTest.h"
#include "stattools/commonTestCases/commonTestingFunctions.h"
#include "gtest/gtest.h"

using namespace testing;
using namespace stattools;
using namespace stattools::prior;

//--------------------------------------------
// THMMBoolGeneratingMatrixInferred
//--------------------------------------------

class THMMBoolGeneratingMatrixInferredTest : public Test {
public:
	std::unique_ptr<THMMBoolGeneratingTest> test;

	std::unique_ptr<genometools::TDistancesBinnedBase> distances;
	std::unique_ptr<THMMBoolGeneratingTest::SpecObs> obs;
	std::unique_ptr<THMMBoolGeneratingTest::SpecObs> obs2;

	void SetUp() override {
		instances::dagBuilder().clear();

		distances = createAndFillDistances(21, 16);
		test      = std::make_unique<THMMBoolGeneratingTest>("out", distances.get());

		obs = createObservation<THMMBoolGeneratingTest::Type, THMMBoolGeneratingTest::NumDimObs>(
			"obs", &test->boxOnObs, {21}, {"out"},
			{false, true,  true, false, false, true,  true, false, false, true, true,
			 false, false, true, true,  false, false, true, true,  false, false});

		// set initial values
		test->log_lambda_1.set(-2.302585);
		test->log_lambda_2.set(-1.609438);
		test->boxOnObs.updateTau(test->boxOnObs.getParams());
	}

	void initializeExtraObs() {
		obs2 = createObservation<THMMBoolGeneratingTest::Type, THMMBoolGeneratingTest::NumDimObs>(
			"obs2", &test->boxOnObs, {21}, {"out"},
			{true, false, false, true,  true, false, false, true,  true, false, false,
			 true, true,  false, false, true, true,  false, false, true, true});
	}

	auto getStorageForRatios() {
		auto storage =
			createUpdatedStorage<THMMBoolGeneratingTest::Type, THMMBoolGeneratingTest::NumDimObs>(obs->storage());

		bool val = true;
		for (size_t i = 0; i < 21; i++) {
			storage[i] = val;
			storage[i] = !value(storage[i]); // value is always opposite of oldValue
			obs->set(i, value(storage[i]));
			if (i % 2 == 0) // jump every 2nd element
				val = !val;
		}
		return storage;
	}
};

TEST_F(THMMBoolGeneratingMatrixInferredTest, constructor) {
	EXPECT_FLOAT_EQ(test->boxOnObs.log_lambda_1(), log(0.1));
	EXPECT_FLOAT_EQ(test->boxOnObs.log_lambda_2(), log(0.2));

	EXPECT_TRUE(test->log_lambda_1.isPartOfBox());
	EXPECT_TRUE(test->log_lambda_2.isPartOfBox());
}

TEST_F(THMMBoolGeneratingMatrixInferredTest, _fillStationaryDistribution) {
	// check stationary distribution
	EXPECT_FLOAT_EQ(test->boxOnObs.getTau(0)(0, 0), 0.6666667);
	EXPECT_FLOAT_EQ(test->boxOnObs.getTau(0)(0, 1), 0.3333333);
	EXPECT_FLOAT_EQ(test->boxOnObs.getTau(0)(1, 0), 0.6666667);
	EXPECT_FLOAT_EQ(test->boxOnObs.getTau(0)(1, 1), 0.3333333);
}

TEST_F(THMMBoolGeneratingMatrixInferredTest, calcPOfValueGivenPrevious) {
	auto storage = getStorageForRatios();

	// check calcLogPOfValueGivenPrevious
	EXPECT_NEAR(test->boxOnObs.calcPOfValueGivenPrevious(obs->storage(), 0), (0.6666667),
				0.01); // value = 0; stationary distr
	EXPECT_NEAR(test->boxOnObs.calcPOfValueGivenPrevious(obs->storage(), 1), (0.08639393),
				0.01); // value previous = 0; value this = 1; distGroup = 1
	EXPECT_NEAR(test->boxOnObs.calcPOfValueGivenPrevious(obs->storage(), 2), (0.6992078),
				0.01); // value previous = 1; value this = 1; distGroup = 2
	EXPECT_NEAR(test->boxOnObs.calcPOfValueGivenPrevious(obs->storage(), 3), (0.4658705),
				0.01); // value previous = 1; value this = 0; distGroup = 3
	EXPECT_NEAR(test->boxOnObs.calcPOfValueGivenPrevious(obs->storage(), 4), (0.7670647),
				0.01); // value previous = 0; value this = 0; distGroup = 3
	EXPECT_NEAR(test->boxOnObs.calcPOfValueGivenPrevious(obs->storage(), 5), (0.303094),
				0.01); // value previous = 0; value this = 1; distGroup = 4
	// ...
	EXPECT_NEAR(test->boxOnObs.calcPOfValueGivenPrevious(obs->storage(), 20), (0.696906),
				0.01); // value previous = 0; value this = 0; distGroup = 4

	// check calcLogPOfOldValueGivenPrevious
	EXPECT_NEAR(test->boxOnObs.calcPOfOldValueGivenPrevious(storage, 0), (0.3333333),
				0.01); // oldValue = 1; stationary distr
	EXPECT_NEAR(test->boxOnObs.calcPOfOldValueGivenPrevious(storage, 1), (0.9136061),
				0.01); // value previous = 0; oldValue this = 0; distGroup = 1
	EXPECT_NEAR(test->boxOnObs.calcPOfOldValueGivenPrevious(storage, 2), (0.3007922),
				0.01); // value previous = 1; oldValue this = 0; distGroup = 2
	EXPECT_NEAR(test->boxOnObs.calcPOfOldValueGivenPrevious(storage, 3), (0.5341295),
				0.01); // value previous = 1; oldValue this = 1; distGroup = 3
	EXPECT_NEAR(test->boxOnObs.calcPOfOldValueGivenPrevious(storage, 4), (0.2329353),
				0.01); // value previous = 0; oldValue this = 1; distGroup = 3
	EXPECT_NEAR(test->boxOnObs.calcPOfOldValueGivenPrevious(storage, 5), (0.696906),
				0.01); // value previous = 0; oldValue this = 0; distGroup = 4
	// ...
	EXPECT_NEAR(test->boxOnObs.calcPOfOldValueGivenPrevious(storage, 20), (0.303094),
				0.01); // value previous = 0; oldValue this = 1; distGroup = 4
}

TEST_F(THMMBoolGeneratingMatrixInferredTest, calcLogPOfValueGivenPrevious) {
	auto storage = getStorageForRatios();

	// check calcLogPOfValueGivenPrevious
	EXPECT_NEAR(test->boxOnObs.calcLogPOfValueGivenPrevious(obs->storage(), 0), log(0.6666667),
				0.01); // value = 0; stationary distr
	EXPECT_NEAR(test->boxOnObs.calcLogPOfValueGivenPrevious(obs->storage(), 1), log(0.08639393),
				0.01); // value previous = 0; value this = 1; distGroup = 1
	EXPECT_NEAR(test->boxOnObs.calcLogPOfValueGivenPrevious(obs->storage(), 2), log(0.6992078),
				0.01); // value previous = 1; value this = 1; distGroup = 2
	EXPECT_NEAR(test->boxOnObs.calcLogPOfValueGivenPrevious(obs->storage(), 3), log(0.4658705),
				0.01); // value previous = 1; value this = 0; distGroup = 3
	EXPECT_NEAR(test->boxOnObs.calcLogPOfValueGivenPrevious(obs->storage(), 4), log(0.7670647),
				0.01); // value previous = 0; value this = 0; distGroup = 3
	EXPECT_NEAR(test->boxOnObs.calcLogPOfValueGivenPrevious(obs->storage(), 5), log(0.303094),
				0.01); // value previous = 0; value this = 1; distGroup = 4
	// ...
	EXPECT_NEAR(test->boxOnObs.calcLogPOfValueGivenPrevious(obs->storage(), 20), log(0.696906),
				0.01); // value previous = 0; value this = 0; distGroup = 4

	// check calcLogPOfOldValueGivenPrevious
	EXPECT_NEAR(test->boxOnObs.calcLogPOfOldValueGivenPrevious(storage, 0), log(0.3333333),
				0.01); // oldValue = 1; stationary distr
	EXPECT_NEAR(test->boxOnObs.calcLogPOfOldValueGivenPrevious(storage, 1), log(0.9136061),
				0.01); // value previous = 0; oldValue this = 0; distGroup = 1
	EXPECT_NEAR(test->boxOnObs.calcLogPOfOldValueGivenPrevious(storage, 2), log(0.3007922),
				0.01); // value previous = 1; oldValue this = 0; distGroup = 2
	EXPECT_NEAR(test->boxOnObs.calcLogPOfOldValueGivenPrevious(storage, 3), log(0.5341295),
				0.01); // value previous = 1; oldValue this = 1; distGroup = 3
	EXPECT_NEAR(test->boxOnObs.calcLogPOfOldValueGivenPrevious(storage, 4), log(0.2329353),
				0.01); // value previous = 0; oldValue this = 1; distGroup = 3
	EXPECT_NEAR(test->boxOnObs.calcLogPOfOldValueGivenPrevious(storage, 5), log(0.696906),
				0.01); // value previous = 0; oldValue this = 0; distGroup = 4
	// ...
	EXPECT_NEAR(test->boxOnObs.calcLogPOfOldValueGivenPrevious(storage, 20), log(0.303094),
				0.01); // value previous = 0; oldValue this = 1; distGroup = 4
}

TEST_F(THMMBoolGeneratingMatrixInferredTest, calcPOfNextGivenValue) {
	auto storage = getStorageForRatios();

	// check calcLogPOfNextGivenValue
	EXPECT_NEAR(test->boxOnObs.calcPOfNextGivenValue(obs->storage(), 0), (0.08639393),
				0.01); // value this = 0; value next = 1, distGroup next = 1
	EXPECT_NEAR(test->boxOnObs.calcPOfNextGivenValue(obs->storage(), 1), (0.6992078),
				0.01); // value this = 1; value next = 1, distGroup next = 2
	EXPECT_NEAR(test->boxOnObs.calcPOfNextGivenValue(obs->storage(), 2), (0.4658705),
				0.01); // value this = 1; value next = 0,  distGroup next = 3
	EXPECT_NEAR(test->boxOnObs.calcPOfNextGivenValue(obs->storage(), 3), (0.7670647),
				0.01); // value this = 0; value next = 0,  distGroup next = 3
	EXPECT_NEAR(test->boxOnObs.calcPOfNextGivenValue(obs->storage(), 4), (0.303094),
				0.01); // value this = 0; value next = 1,  distGroup next = 4
	EXPECT_NEAR(test->boxOnObs.calcPOfNextGivenValue(obs->storage(), 5), (0.393812),
				0.01); // value this = 1; value next = 1,  distGroup next = 4
	// ...
	EXPECT_NEAR(test->boxOnObs.calcPOfNextGivenValue(obs->storage(), 20), 1., 0.01); // value this = 1; there is no next

	// check calcLogPOfNextGivenOldValue
	EXPECT_NEAR(test->boxOnObs.calcPOfNextGivenOldValue(storage, 0), (0.82721215),
				0.01); // oldValue this = 1; value next = 1; distGroup next = 1
	EXPECT_NEAR(test->boxOnObs.calcPOfNextGivenOldValue(storage, 1), (0.1503961),
				0.01); // oldValue this = 0; value next = 1; distGroup next = 2
	EXPECT_NEAR(test->boxOnObs.calcPOfNextGivenOldValue(storage, 2), (0.7670647),
				0.01); // oldValue this = 0; value next = 0; distGroup next = 3
	EXPECT_NEAR(test->boxOnObs.calcPOfNextGivenOldValue(storage, 3), (0.4658705),
				0.01); // oldValue this = 1; value next = 0; distGroup next = 3
	EXPECT_NEAR(test->boxOnObs.calcPOfNextGivenOldValue(storage, 4), (0.393812),
				0.01); // oldValue this = 1; value next = 1; distGroup next = 4
	EXPECT_NEAR(test->boxOnObs.calcPOfNextGivenOldValue(storage, 5), (0.303094),
				0.01); // oldValue this = 0; value next = 1; distGroup next = 4
	// ...
	EXPECT_NEAR(test->boxOnObs.calcPOfNextGivenOldValue(storage, 20), 1.,
				0.01); // oldValue this = 0; there is no next
}

TEST_F(THMMBoolGeneratingMatrixInferredTest, calcLogPOfNextGivenValue) {
	auto storage = getStorageForRatios();

	// check calcLogPOfNextGivenValue
	EXPECT_NEAR(test->boxOnObs.calcLogPOfNextGivenValue(obs->storage(), 0), log(0.08639393),
				0.01); // value this = 0; value next = 1, distGroup next = 1
	EXPECT_NEAR(test->boxOnObs.calcLogPOfNextGivenValue(obs->storage(), 1), log(0.6992078),
				0.01); // value this = 1; value next = 1, distGroup next = 2
	EXPECT_NEAR(test->boxOnObs.calcLogPOfNextGivenValue(obs->storage(), 2), log(0.4658705),
				0.01); // value this = 1; value next = 0,  distGroup next = 3
	EXPECT_NEAR(test->boxOnObs.calcLogPOfNextGivenValue(obs->storage(), 3), log(0.7670647),
				0.01); // value this = 0; value next = 0,  distGroup next = 3
	EXPECT_NEAR(test->boxOnObs.calcLogPOfNextGivenValue(obs->storage(), 4), log(0.303094),
				0.01); // value this = 0; value next = 1,  distGroup next = 4
	EXPECT_NEAR(test->boxOnObs.calcLogPOfNextGivenValue(obs->storage(), 5), log(0.393812),
				0.01); // value this = 1; value next = 1,  distGroup next = 4
	// ...
	EXPECT_NEAR(test->boxOnObs.calcLogPOfNextGivenValue(obs->storage(), 20), 0.,
				0.01); // value this = 1; there is no next

	// check calcLogPOfNextGivenOldValue
	EXPECT_NEAR(test->boxOnObs.calcLogPOfNextGivenOldValue(storage, 0), log(0.82721215),
				0.01); // oldValue this = 1; value next = 1; distGroup next = 1
	EXPECT_NEAR(test->boxOnObs.calcLogPOfNextGivenOldValue(storage, 1), log(0.1503961),
				0.01); // oldValue this = 0; value next = 1; distGroup next = 2
	EXPECT_NEAR(test->boxOnObs.calcLogPOfNextGivenOldValue(storage, 2), log(0.7670647),
				0.01); // oldValue this = 0; value next = 0; distGroup next = 3
	EXPECT_NEAR(test->boxOnObs.calcLogPOfNextGivenOldValue(storage, 3), log(0.4658705),
				0.01); // oldValue this = 1; value next = 0; distGroup next = 3
	EXPECT_NEAR(test->boxOnObs.calcLogPOfNextGivenOldValue(storage, 4), log(0.393812),
				0.01); // oldValue this = 1; value next = 1; distGroup next = 4
	EXPECT_NEAR(test->boxOnObs.calcLogPOfNextGivenOldValue(storage, 5), log(0.303094),
				0.01); // oldValue this = 0; value next = 1; distGroup next = 4
	// ...
	EXPECT_NEAR(test->boxOnObs.calcLogPOfNextGivenOldValue(storage, 20), 0.,
				0.01); // oldValue this = 0; there is no next
}

TEST_F(THMMBoolGeneratingMatrixInferredTest, getLogDensityRatio) {
	auto storage = getStorageForRatios();

	// correct logPriorDensity?
	EXPECT_NEAR(test->boxOnObs.getLogDensity(obs->storage(), 0), log(0.6666667) + log(0.08639393), 0.01);
	EXPECT_NEAR(test->boxOnObs.getLogDensity(obs->storage(), 1), log(0.08639393) + log(0.6992078), 0.01);
	EXPECT_NEAR(test->boxOnObs.getLogDensity(obs->storage(), 2), log(0.6992078) + log(0.4658705), 0.01);
	EXPECT_NEAR(test->boxOnObs.getLogDensity(obs->storage(), 3), log(0.4658705) + log(0.7670647), 0.01);
	EXPECT_NEAR(test->boxOnObs.getLogDensity(obs->storage(), 4), log(0.7670647) + log(0.303094), 0.01);
	EXPECT_NEAR(test->boxOnObs.getLogDensity(obs->storage(), 5), log(0.303094) + log(0.393812), 0.01);
	// ...
	EXPECT_NEAR(test->boxOnObs.getLogDensity(obs->storage(), 20), log(0.696906) + 0., 0.01);

	// correct priorDensity?
	EXPECT_NEAR(test->boxOnObs.getDensity(obs->storage(), 0), (0.6666667) * (0.08639393), 0.01);
	EXPECT_NEAR(test->boxOnObs.getDensity(obs->storage(), 1), (0.08639393) * (0.6992078), 0.01);
	EXPECT_NEAR(test->boxOnObs.getDensity(obs->storage(), 2), (0.6992078) * (0.4658705), 0.01);
	EXPECT_NEAR(test->boxOnObs.getDensity(obs->storage(), 3), (0.4658705) * (0.7670647), 0.01);
	EXPECT_NEAR(test->boxOnObs.getDensity(obs->storage(), 4), (0.7670647) * (0.303094), 0.01);
	EXPECT_NEAR(test->boxOnObs.getDensity(obs->storage(), 5), (0.303094) * (0.393812), 0.01);
	// ...
	EXPECT_NEAR(test->boxOnObs.getDensity(obs->storage(), 20), (0.696906) * 1., 0.01);

	// correct logPriorRatio?
	EXPECT_NEAR(test->boxOnObs.getLogDensityRatio(storage, 0),
				log(0.6666667) - log(0.3333333) + log(0.08639393) - log(0.82721215), 0.01);
	EXPECT_NEAR(test->boxOnObs.getLogDensityRatio(storage, 1),
				log(0.08639393) - log(0.9136061) + log(0.6992078) - log(0.1503961), 0.01);
	EXPECT_NEAR(test->boxOnObs.getLogDensityRatio(storage, 2),
				log(0.6992078) - log(0.3007922) + log(0.4658705) - log(0.7670647), 0.01);
	EXPECT_NEAR(test->boxOnObs.getLogDensityRatio(storage, 3),
				log(0.4658705) - log(0.5341295) + log(0.7670647) - log(0.4658705), 0.01);
	EXPECT_NEAR(test->boxOnObs.getLogDensityRatio(storage, 4), log(0.7670647) - log(0.2329353) + log(0.303094) - log(0.393812),
				0.01);
	EXPECT_NEAR(test->boxOnObs.getLogDensityRatio(storage, 5), log(0.303094) - log(0.696906) + log(0.393812) - log(0.303094),
				0.01);
	// ...
	EXPECT_NEAR(test->boxOnObs.getLogDensityRatio(storage, 20), log(0.696906) - log(0.303094) + 0. - 0., 0.01);
}

TEST_F(THMMBoolGeneratingMatrixInferredTest, _updateLogLambda1) {
	// change value of log_lambda_1, such that oldValue and newValue are not the same
	test->log_lambda_1.set(log(0.3));

	// calculate LL
	auto f = test->boxOnObs.calculateLLRatio(&test->log_lambda_1, coretools::TRange(0));
	EXPECT_NEAR(f(&obs->storage()), 0.33781, 0.01);
}

TEST_F(THMMBoolGeneratingMatrixInferredTest, _updateLogLambda2) {
	// change value of log_lambda_1, such that oldValue and newValue are not the same
	test->log_lambda_2.set(log(0.3));

	// calculate LL
	auto f = test->boxOnObs.calculateLLRatio(&test->log_lambda_2, coretools::TRange(0));
	EXPECT_NEAR(f(&obs->storage()), -1.07118, 0.01);
}

TEST_F(THMMBoolGeneratingMatrixInferredTest, densities) {
	getStorageForRatios();

	double sum = test->boxOnObs.getSumLogPriorDensity(obs->storage());
	EXPECT_NEAR(sum, -16.1902, 0.01);

	sum = obs->getSumLogPriorDensity() + test->log_lambda_1.getSumLogPriorDensity() +
		  test->log_lambda_2.getSumLogPriorDensity();
	EXPECT_NEAR(sum, -16.1902, 0.01);
}

TEST_F(THMMBoolGeneratingMatrixInferredTest, _updateLogLambda2_twoParameterArrays) {
	initializeExtraObs();

	// change value of log_lambda_2, such that oldValue and newValue are not the same
	test->log_lambda_2.set(log(0.3));

	// calculate LL
	auto f = test->boxOnObs.calculateLLRatio(&test->log_lambda_2, coretools::TRange(0));
	EXPECT_NEAR(f(&obs->storage()) + f(&obs2->storage()), -2.68629, 0.05);
}
