//
// Created by caduffm on 3/28/22.
//

#include "gtest/gtest.h"

#include "stattools/commonTestCases/commonTestingFunctions.h"
#include "coretools/Types/commonWeakTypes.h"
#include "stattools/Priors/TPriorChisq.h"

using namespace testing;
using namespace stattools;
using namespace stattools::prior;

//--------------------------------------------
// TChisqFixed
//--------------------------------------------

struct TChisqFixedTest : Test {
	std::string params = "1";

	using Type = coretools::Positive;
	TChisqFixed<TParameterBase, Type, 1> prior;

	coretools::TMultiDimensionalStorage<TValueUpdated<Type>, 1> storage;
};

TEST_F(TChisqFixedTest, initValid) {
	prior.setFixedPriorParameters(params);
	EXPECT_EQ(prior.k(), 1);
}

TEST_F(TChisqFixedTest, initZero) {
	params = "0";
	EXPECT_ANY_THROW(prior.setFixedPriorParameters(params));
}

TEST_F(TChisqFixedTest, initNegative) {
	params = "-1";
	EXPECT_ANY_THROW(prior.setFixedPriorParameters(params));
}

TEST_F(TChisqFixedTest, initNoParams) {
	params = "";
	EXPECT_ANY_THROW(prior.setFixedPriorParameters(params));
}

TEST_F(TChisqFixedTest, hastings) {
	prior.setFixedPriorParameters(params);
	storage.emplace_back(TValueUpdated<Type>(10.));
	double logDensityOld = prior.getLogDensity(storage, 0);
	storage[0]           = 5.; // update
	storage.emplace_back(TValueUpdated<Type>(1.0));

	EXPECT_FLOAT_EQ(prior.getLogDensityRatio(storage, 0), 2.846574);
	EXPECT_FLOAT_EQ(prior.getLogDensityRatio(storage, 0), prior.getLogDensity(storage, 0) - logDensityOld);
	EXPECT_FLOAT_EQ(prior.getDensity(storage, 1), 0.2419707);
}

//--------------------------------------------
// TMultivariateChisqFixed
//--------------------------------------------

class TMultivariateChisqFixedTest : public Test {
public:
	using TypeObs                     = coretools::Positive;
	static constexpr size_t NumDimObs = 1;
	using BoxType                     = TMultivariateChisqFixed<TObservationBase, TypeObs, NumDimObs>;
	using Obs                         = TObservation<TypeObs, NumDimObs, BoxType>;
	std::shared_ptr<BoxType> boxOnObs;
	std::unique_ptr<Obs> obs;

	void SetUp() override {
		instances::dagBuilder().clear();

		boxOnObs = std::make_shared<BoxType>();
		obs      = createObservation<TypeObs, NumDimObs>(
			"obs", boxOnObs.get(), {10}, {"out", "10,9,8,7,6,5,4,3,2,1"},
			{16.9913843841421, 10.3898992460316, 3.03827316667281, 6.81957212048465, 8.64789850337061, 6.5170836177272,
				  1.39898459261386, 0.593045827795861, 0.0402791057044047, 4.78553130391896});
	}
};

TEST_F(TMultivariateChisqFixedTest, _getLogDensity_vec) {
	// prior density of first element
	size_t index = 0;
	EXPECT_FLOAT_EQ(boxOnObs->getLogDensity(obs->storage(), index), -3.808656);
	EXPECT_FLOAT_EQ(boxOnObs->getDensity(obs->storage(), index), exp(-3.808656));

	// prior density of fifth element
	index = 4;
	EXPECT_FLOAT_EQ(boxOnObs->getLogDensity(obs->storage(), index), -2.781905);
	EXPECT_FLOAT_EQ(boxOnObs->getDensity(obs->storage(), index), 0.0619204187945893);

	// prior density of last element
	index = 9;
	EXPECT_FLOAT_EQ(boxOnObs->getLogDensity(obs->storage(), index), -4.094503);
	EXPECT_FLOAT_EQ(boxOnObs->getDensity(obs->storage(), index), 0.0166640310423605);
}

TEST_F(TMultivariateChisqFixedTest, getLogDensityRatio) {
	auto storage = createUpdatedStorage<TypeObs, NumDimObs>(obs->storage());
	storage[0]   = 6.34;
	EXPECT_FLOAT_EQ(boxOnObs->getLogDensityRatio(storage, 0), 1.382382);
	storage[0].reset();

	storage[4] = 5.72;
	EXPECT_FLOAT_EQ(boxOnObs->getLogDensityRatio(storage, 4), 0.6372542);
	storage[4].reset();

	storage[9] = 1.7327;
	EXPECT_FLOAT_EQ(boxOnObs->getLogDensityRatio(storage, 9), 2.034374);
}

TEST_F(TMultivariateChisqFixedTest, getLogDensityFull) {
	// log prior density full
	EXPECT_FLOAT_EQ(obs->getSumLogPriorDensity(), -24.65138);
}

//--------------------------------------------
// TMultivariateChiFixed
//--------------------------------------------

class TMultivariateChiFixedTest : public Test {
public:
	using TypeObs                     = coretools::Positive;
	static constexpr size_t NumDimObs = 1;
	using BoxType                     = TMultivariateChiFixed<TObservationBase, TypeObs, NumDimObs>;
	using Obs                         = TObservation<TypeObs, NumDimObs, BoxType>;
	std::shared_ptr<BoxType> boxOnObs;
	std::unique_ptr<Obs> obs;

	void SetUp() override {
		instances::dagBuilder().clear();

		boxOnObs = std::make_shared<BoxType>();
		obs      = createObservation<TypeObs, NumDimObs>(
			"obs", boxOnObs.get(), {10}, {"out", "10,9,8,7,6,5,4,3,2,1"},
			{16.9913843841421, 10.3898992460316, 3.03827316667281, 6.81957212048465, 8.64789850337061, 6.5170836177272,
				  1.39898459261386, 0.593045827795861, 0.0402791057044047, 4.78553130391896});
	}
};

TEST_F(TMultivariateChiFixedTest, _getLogDensity_vec) {
	// prior density of first element
	size_t index = 0;
	EXPECT_FLOAT_EQ(boxOnObs->getLogDensity(obs->storage(), index), -124.8098565);
	EXPECT_FLOAT_EQ(boxOnObs->getDensity(obs->storage(), index), exp(-124.8098565));

	// prior density of fifth element
	index = 4;
	EXPECT_FLOAT_EQ(boxOnObs->getLogDensity(obs->storage(), index), -28.68593);

	// prior density of last element
	index = 9;
	EXPECT_FLOAT_EQ(boxOnObs->getLogDensity(obs->storage(), index), -11.67645);
}

TEST_F(TMultivariateChiFixedTest, getLogDensityRatio) {
	auto storage = createUpdatedStorage<TypeObs, NumDimObs>(obs->storage());
	storage[0]   = 6.34;
	EXPECT_FLOAT_EQ(boxOnObs->getLogDensityRatio(storage, 0), 115.3833);
	storage[0].reset();

	storage[4] = 5.72;
	EXPECT_FLOAT_EQ(boxOnObs->getLogDensityRatio(storage, 4), 18.96714);
	storage[4].reset();

	storage[9] = 1.7327;
	EXPECT_FLOAT_EQ(boxOnObs->getLogDensityRatio(storage, 9), 9.94953);
}

TEST_F(TMultivariateChiFixedTest, getLogDensityFull) {
	// log prior density full
	EXPECT_FLOAT_EQ(obs->getSumLogPriorDensity(), -241.0631);
}
