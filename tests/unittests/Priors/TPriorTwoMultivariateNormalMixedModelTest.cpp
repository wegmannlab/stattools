#include "gtest/gtest.h"

#include "stattools/commonTestCases/TBernoulliTest.h"
#include "stattools/commonTestCases/commonTestingFunctions.h"
#include "stattools/commonTestCases/TTwoMultivariateNormalMixedModelsTest.h"

using namespace testing;
using namespace stattools;
using namespace stattools::prior;

//--------------------------------------------
// TTwoMultivariateNormalsMixedModelInferred
//--------------------------------------------
static std::string values_mu_1D() { return {"-0.6264538107423327"}; }
static std::string values_mu_5D() {
	return "-0.626453810742332,0.183643324222082,-0.835628612410047,1.59528080213779,0.329507771815361";
}

static std::string values_m() { return "0.2"; }

static std::string values_mrr_1D() { return "0.820468384118015"; }

static std::string values_mrr_5D() {
	return "0.820468384118015,0.487429052428485,0.738324705129217,0.575781351653492,0.305388387156356";
}

static std::string values_mrs_5D() {
	return "1.51178116845085,0.389843236411431,-0.621240580541804,-2.2146998871775,1.12493091814311,-0."
		   "0449336090152309,-0.0161902630989461,0.943836210685299,0.821221195098089,0.593901321217509";
}

static std::string values_rho_1D() { return "0.996812955243844"; }

static std::string values_rho_5D() {
	return "0.996812955243844,1.43528534337308,0.0372685263864696,0.324010152835399,1.32046792931379";
}

static std::string values_z() { return "1,0,0,0,0,0,0,1,0,1"; }

static std::string values_z_initialization() {
	return "1,0,1,1,0,1,1,0,1,1,0,0,1,1,1,1,0,1,0,1,0,1,1,1,0,0,1,1,0,1,1,0,0,0,0,1,1,1,1,1,0,1,0,1,0,0,1,1,1,0";
}

static std::vector<double> values_obs_1D() {
	return {-0.41499456329968,  -0.394289953710349, -0.0593133967111857, 1.10002537198388,  0.763175748457544,
			-0.164523596253587, -0.253361680136508, 0.696963375404737,   0.556663198673657, -0.68875569454952};
}

static std::vector<double> values_obs_5D() {
	return {-0.41499456329968,   -0.70749515696212,  1.98039989850586,    0.188792299514343,  0.291446235517463,
			-0.394289953710349,  0.36458196213683,   -0.367221476466509,  -1.80495862889104,  -0.443291873218433,
			-0.0593133967111857, 0.768532924515416,  -1.04413462631653,   1.46555486156289,   0.00110535163162413,
			1.10002537198388,    -0.112346212150228, 0.569719627442413,   0.153253338211898,  0.0743413241516641,
			0.763175748457544,   0.881107726454215,  -0.135054603880824,  2.17261167036215,   -0.589520946188072,
			-0.164523596253587,  0.398105880367068,  2.40161776050478,    0.475509528899663,  -0.568668732818502,
			-0.253361680136508,  -0.612026393250771, -0.0392400027331692, -0.709946430921815, -0.135178615123832,
			0.696963375404737,   0.341119691424425,  0.689739362450777,   0.610726353489055,  1.1780869965732,
			0.556663198673657,   -1.12936309608079,  0.0280021587806661,  -0.934097631644252, -1.52356680042976,
			-0.68875569454952,   1.43302370170104,   -0.743273208882405,  -1.2536334002391,   0.593946187628422};
}

static std::vector<double> values_obs_5D_initialization() {
	return {3.50187101272656,    -0.68559832714638,   -3.18068374543844,   -8.79362147453702,  -13.8746031014148,
			-1.5686687328185,    -0.635178615123832,  1.1780869965732,     -1.02356680042976,  1.59394618762842,
			-13.3132342155804,   9.33895570053379,    2.19924803660651,    -14.1725002909224,  6.21022742648139,
			-6.05957462114257,   12.9303882517041,    -2.14579408546869,   -1.29556530043387,  -0.00190741213561951,
			-1.16452359625359,   -0.753361680136508,  0.696963375404737,   1.05666319867366,   0.31124430545048,
			-7.20366677224124,   -0.0788412685576476, -9.10921648552445,   2.08028772404075,   -5.54584643918818,
			-18.3321840682484,   -0.478681403197304,  -6.30300333928146,   -2.90968579860405,  -10.5657236263585,
			-0.524490471100338,  -1.20994643092181,   0.610726353489055,   -0.434097631644252, -0.253633400239102,
			-1.98178744005234,   5.10820728620116,    -11.8645863857947,   11.4677704427424,   0.946559717218343,
			-4.92807929441984,   -3.69992868548507,   -2.79113302976559,   5.44188331267827,   -0.773304822696064,
			-0.811207700485657,  -2.30495862889104,   1.46555486156289,    0.653253338211898,  3.17261167036215,
			0.358679551529044,   -0.602787727342996,  0.387671611559369,   0.446194959417095,  -0.377059556828607,
			-0.398395595654848,  -6.38894486259664,   5.31496192632572,    -14.6839408178679,  4.06557860789766,
			6.12666307051405,    -1.23564404126326,   -0.376341714670479,  -6.31660478755657,  -2.24270272246319,
			-8.50819001193448,   20.3716654562835,    0.173956196932517,   -12.3630053043433,  -15.4060553441858,
			-2.64375831769667,   3.70694643254513,    -4.00246743977644,   -13.2020787754746,  10.8783826745488,
			-1.056128739529,     -0.655795506705329,  -1.47075238389927,   0.0218499448913796, 1.4179415601997,
			14.1974502549955,    -3.58740569225614,   -12.5328975560769,   6.92241305677824,   0.552908631060209,
			-1.62645381074233,   -0.316356675777918,  -0.835628612410047,  2.09528080213779,   1.32950777181536,
			6.07310667398079,    9.84107734737462,    2.23480414915304,    -8.28707612866019,  12.6296455596733,
			1.40161776050478,    -0.539240002733169,  0.689739362450777,   0.528002158780666,  0.256726791117595,
			3.25100377372448,    -2.88647100913033,   10.5848304870902,    9.36422651374936,   -5.19243048231147,
			-1.34726028311276,   7.37639605630162,    20.7524500865228,    10.7739243876377,   13.079083983867,
			23.9766158983416,    6.17066166765493,    5.413273359637,      0.366004768540913,  6.10108422952927,
			-1.41499456329968,   -0.894289953710349,  -0.0593133967111857, 1.60002537198388,   1.76317574845754,
			-1.82046838411802,   -0.0125709475715147, 0.738324705129217,   1.07578135165349,   0.694611612843644,
			17.0314190791747,    -3.81132036391221,   -16.0551341225308,   2.47193438739481,   3.63175646405474,
			-2.58754604716016,   14.145873119698,     -7.66081999604665,   -3.80211753928547,  -8.26109497377437,
			-0.441513574434696,  -1.77659220845804,   -0.573265414236886,  -0.724612614898356, 0.526599363560688,
			-11.4798441280774,   13.9115770684428,    -10.1584746530465,   4.61974712317515,   -2.8107605110892,
			16.6728726937265,    6.66707476017206,    9.10174229495227,    4.34185357826345,   17.8217608051942,
			-0.0810226283917818, 0.282136300731067,   0.0745649833651906,  -1.48935169586337,  1.61982574789471,
			-1.70749515696212,   -0.13541803786317,   0.768532924515416,   0.387653787849772,  1.88110772645421,
			0.98039989850586,    -0.867221476466509,  -1.04413462631653,   1.06971962744241,   0.864945396119176,
			-1.04493360901523,   -0.516190263098946,  0.943836210685299,   1.32122119509809,   1.59390132121751,
			-2.7710396143654,    3.52011779486338,    -7.31748173119606,   8.80373167981674,   -11.0808278630447,
			-7.35736453948977,   -5.11644730360566,   14.3228223854166,    -6.00696353310367,  -1.07380743601965,
			-16.3644982353759,   -3.50976126836611,   -5.28279904445006,   -6.02094780680999,  0.431032221526075,
			-10.8582670040929,   -29.3892067167954,   -6.40481702565115,   6.20507635920485,   0.402767239573899,
			-11.7519229661568,   9.50028803713914,    -6.21266694796823,   -13.3442684738449,  19.6929062242358,
			-1.54252003099165,   0.707867805983172,   1.16040261569495,    1.200213649515,     2.58683345454085,
			3.09401839650934,    16.3887328620405,    15.8658843344197,    -2.80907800682766,  -21.8523553529247,
			-0.667049628786482,  0.563099837276363,   -0.304183923634301,  0.870018809916288,  1.26709879077223,
			-20.1435942568001,   11.2658331201856,    -16.64972436212,     -4.13530401472386,  -10.1592010504285,
			-0.601894119632932,  -1.11202639325077,   0.341119691424425,   -0.629363096080793, 2.43302370170104,
			-0.708553764482537,  -0.943291873218433,  0.00110535163162413, 0.574341324151664,  0.410479053811928,
			-21.0016494478548,   -5.94790740001725,   -2.55670709156989,   -1.16121036765006,  11.2046390878411,
			22.0797839905936,    0.558023678937115,   4.56998805423414,    -0.27152935356531,  -2.34000842366545,
			21.0610246454047,    -3.05027030141015,   -14.2449465021281,   -0.943996019542194, 3.07538339232345,
			0.511781168450848,   -0.110156763588569,  -0.621240580541804,  -1.7146998871775,   2.12493091814311};
}

std::vector<double> values_obs_5D_initialization_2() {
	return {-4.50668131363331,   0.472611065421362,    -3.5003076616815,    1.59937736714571,    -1.76150493064403,
			-0.476379409501983,  0.51775422653797,     -0.251164588087978,  -0.92999344738861,   2.70912103210088,
			-0.549812898727345,  -0.518559832714638,   -0.318068374543844,  -0.429362147453702,  -0.487460310141485,
			-0.237413487581682,  0.611431080730634,    -0.923206952829831,  0.664341838427956,   2.15482518709727,
			0.511781168450848,   -0.110156763588569,   -0.621240580541804,  -1.7146998871775,    2.12493091814311,
			-2.04798441280774,   0.941157706844281,    -1.01584746530465,   0.911974712317515,   0.61892394889108,
			-1.39280792944198,   -0.819992868548507,   -0.279113302976559,  0.994188331267827,   0.822669517730394,
			-2.43262905201625,   6.347496439329,       3.93939687346042,    2.38313012981247,    1.01544598565683,
			-1.70749515696212,   -0.13541803786317,    0.768532924515416,   0.387653787849772,   1.88110772645421,
			-2.12987818560255,   -4.12654472301241,    -1.63622723618163,   -0.645135916240166,  8.43310543345503,
			0.51221269395063,    -0.417034266414086,   0.5672209148515,     -0.52454847953446,   1.32300650302244,
			1.49766158983416,    0.167066166765493,    0.5413273359637,     0.486600476854091,   1.51010842295293,
			-2.40885045607319,   0.416019328792574,    -0.1912789505352,    1.30328321613365,    2.8874744633086,
			-0.387781826010872,  0.1783401772227,      0.567951972471672,   -0.0725426039261257, -0.36329125627834,
			-1.54252003099165,   0.707867805983172,    1.16040261569495,    1.200213649515,      2.58683345454085,
			-1.37565742282039,   -0.866630945702358,   -0.295677452700886,  1.94182041019987,    0.302461708086959,
			-1.25893258311878,   -0.105620831778428,   -0.851857092023863,  3.14916688109488,    1.15601167566508,
			0.803141907917467,   -0.831132036391221,   -1.60551341225308,   0.697193438739481,   1.26317564640547,
			-1.63573645394898,   -0.961644730360566,   1.43228223854166,    -0.150696353310367,  0.792619256398035,
			0.324004321299602,   0.115636849302674,    1.09166895553536,    0.806604861513632,   0.889841237517143,
			-1.43383274002959,   1.2726111849785,      -0.0182597111630101, 1.35281499360053,    1.20516290324403,
			-2.91435942568001,   0.676583312018562,    -1.664972436212,     0.0364695985276144,  -0.115920105042845,
			-0.260410774425204,  -1.56345741548281,    0.2462108435364,     0.210500633435688,   -1.26488935648794,
			0.307901520114146,   0.997041009402783,    0.814702730897356,   -1.36978879020261,   1.48202950412376,
			1.40161776050478,    -0.539240002733169,   0.689739362450777,   0.528002158780666,   0.256726791117595,
			0.767287269372646,   0.216707476017206,    0.910174229495227,   0.884185357826345,   2.68217608051942,
			-2.00107218102542,   -1.16817860675339,    0.945184953373082,   0.933702149545162,   2.00515921767704,
			-1.19819542148464,   0.392008392473526,    -0.0257150709181537, -0.147660450585665,  1.64635941503464,
			-0.590598160349066,  1.18887328620405,     1.58658843344197,    0.169092199317234,   -1.28523553529247,
			-0.116583639779486,  -2.73227117010984,    1.98596461299926,    5.18084753824839,    4.4261062010146,
			-1.3887222443379,    -0.222085867549457,   -0.823081121572025,  0.431159065521535,   -0.167662326129799,
			-0.574899622627552,  -0.738647100913033,   1.05848304870902,    1.38642265137494,    0.380756951768853,
			-1.41499456329968,   -0.894289953710349,   -0.0593133967111857, 1.60002537198388,    1.76317574845754,
			0.98039989850586,    -0.867221476466509,   -1.04413462631653,   1.06971962744241,    0.864945396119176,
			-0.708553764482537,  -0.943291873218433,   0.00110535163162413, 0.574341324151664,   0.410479053811928,
			-1.16437583176967,   -0.0793053567454872,  -0.400246743977644,  -0.87020787754746,   1.98783826745488,
			-1.62036667722412,   -0.457884126855765,   -0.910921648552446,  0.658028772404075,   0.345415356081182,
			-1.056128739529,     -0.655795506705329,   -1.47075238389927,   0.0218499448913796,  1.4179415601997,
			0.473881181109138,   0.177268492312998,    0.379962686566745,   0.307201573542666,   2.5778917949044,
			-0.287333692948595,  -0.573564404126326,   -0.0376341714670479, -0.181660478755657,  0.675729727753681,
			-0.865552339066324,  0.265598999157864,    0.955136676908982,   0.44943429855773,    0.694184580233029,
			-0.267249957790898,  0.446585640197786,    0.00439870432637403, 0.147677694450151,   0.470304490866496,
			0.0691614606767963,  -0.983974930301277,   -0.121010111327444,  -0.794140003820841,  1.49431283601486,
			0.358679551529044,   -0.602787727342996,   0.387671611559369,   0.446194959417095,   -0.377059556828607,
			1.30797839905936,    -0.394197632106289,   0.456998805423414,   0.422847064643469,   0.665999157633456,
			-1.92431277312728,   1.0929137537192,      0.0450105981218803,  -0.215128400667883,  1.86522309971714,
			-1.05652142452649,   -2.62936064823465,    0.344845762099456,   -1.40495544558553,   0.188829846859783,
			-1.75081900119345,   1.58716654562835,     0.0173956196932517,  -0.786300530434326,  -0.640605534418578,
			-0.0810226283917818, 0.282136300731067,    0.0745649833651906,  -1.48935169586337,   1.61982574789471,
			0.146228357215801,   -2.90309621489187,    0.572739555245841,   0.874724406778655,   0.574732278443924,
			-1.04493360901523,   -0.516190263098946,   0.943836210685299,   1.32122119509809,    1.59390132121751,
			-0.601894119632932,  -1.11202639325077,    0.341119691424425,   -0.629363096080793,  2.43302370170104,
			-0.403765890681546,  -1.67357694087136,    -0.155642534890318,  -1.41890982026984,   0.804741153889363,
			-1.82046838411802,   -0.0125709475715147,  0.738324705129217,   1.07578135165349,    0.694611612843644,
			-0.821863093288871,  1.11076749549807,     -6.63408841737381,   -2.67607145033512,   2.69425894822931,
			1.20610246454047,    -0.755027030141015,   -1.42449465021281,   0.355600398045781,   1.20753833923234,
			-1.98582670040929,   -3.38892067167955,    -0.640481702565115,  1.07050763592048,    0.94027672395739,
			-1.3271010146531,    -2.06908218514395,    -0.367450756170482,  1.86443492906985,    0.665718635268836,
			0.0436124583561828,  -0.400921513102802,   -0.45413690915436,   -0.15578185245044,   0.964077577377489,
			-1.16452359625359,   -0.753361680136508,   0.696963375404737,   1.05666319867366,    0.31124430545048,
			-1.17710396143654,   -0.0979882205136621,  -0.731748173119606,  1.33037316798167,    -0.208082786304465,
			0.612346798201784,   -2.13728064710472,    -0.779568513201747,  -0.141176933750564,  0.318868606428844,
			-0.863778106897222,  -0.0928323965761635,  -0.0696548130129049, 0.252335658380669,   1.69555080661964,
			-1.5686687328185,    -0.635178615123832,   1.1780869965732,     -1.02356680042976,   1.59394618762842,
			-1.09817874400523,   0.0608207286201161,   -1.18645863857947,   1.59677704427424,    0.994655971721834,
			-1.50595746211426,   0.843038825170412,    -0.214579408546869,  0.320443469956613,   0.899809258786438,
			-1.00830901421561,   -0.371144598402595,   -0.145875628461003,  0.336089043263932,   2.76355200278492,
			2.04115177959897,    -2.41136199539877,    -2.38080700137769,   -4.41927578700162,   -3.59761568793944,
			-5.16980931542663,   0.652285256912088,    0.738391312153421,   4.27561804992,       0.911740918638715,
			-1.39011866405368,   -0.123629708225352,   0.244164924486494,   -0.926257342382538,  2.77842928747545,
			0.519745025499545,   -0.808740569225614,   -1.25328975560769,   1.14224130567782,    0.955290863106021,
			0.435069572311625,   -1.21037114584988,    -0.0650675736555838, -1.25946873536593,   1.5697229718191,
			-0.106326297574487,  -1.5472981490613,     1.97133738622415,    0.116367893711638,   2.65414530227499,
			-3.59232766994599,   0.814002167198053,    -0.635543001032135,  0.070021161305812,   0.830681667698037,
			-1.03472602831128,   0.287639605630162,    2.07524500865228,    1.52739243876377,    2.2079083983867,
			-0.543864396698798,  -0.853400285829911,   0.170489470947982,   -0.364035954126904,  1.67923077401566,
			-0.601869844548044,  -0.907528579269772,   1.32425863017727,    -0.20123166924692,   0.419385695759464,
			-1.15875460471602,   0.964587311969797,    -0.766081999604665,  0.0697882460714535,  0.0738905026225628,
			-4.00804859892048,   -1.86611193132898,    -0.424102260144812,  0.736803663745558,   -1.34272312035896,
			-0.939839559565485,  -1.08889448625966,    0.531496192632572,   -1.01839408178679,   1.30655786078977,
			-0.729945099062771,  -0.92218400978764,    -1.18911329485959,   0.16896702112099,    0.0601706734899786,
			-0.441513574434696,  -1.77659220845804,    -0.573265414236886,  -0.724612614898356,  0.526599363560688,
			-0.0489871924231836, -0.889237181718379,   -0.284330661799574,  1.3574097780798,     2.7196272991206,
			-2.53644982353759,   -0.800976126836611,   -0.528279904445006,  -0.152094780680999,  0.943103222152607,
			-3.00016494478548,   -1.04479074000172,    -0.255670709156989,  0.333878963234994,   2.02046390878411,
			6.73661151427699,    -1.02716205658993,    -3.30036622719055,   -5.73896806699011,   2.62753460874345,
			-2.23132342155804,   0.483895570053379,    0.219924803660651,   -0.967250029092243,  1.52102274264814,
			-2.07519229661568,   0.500028803713914,    -0.621266694796823,  -0.884426847384491,  2.86929062242358,
			-4.44869648061451,   6.7247271462643,      -2.80056866546881,   0.851349380685964,   13.0491528264719,
			0.0744409582779002,  1.39565477419858,     -0.602997303605094,  0.109132179276863,   0.583777968469808,
			-0.292689332601921,  0.534107734737462,    0.223480414915304,   -0.378707612866019,  2.16296455596733,
			-3.03328559561795,   0.000963559247906698, -1.53179813996574,   0.475002360721737,   1.59298472102529,
			-1.62645381074233,   -0.316356675777918,   -0.835628612410047,  2.09528080213779,    1.32950777181536,
			-0.667049628786482,  0.563099837276363,    -0.304183923634301,  0.870018809916288,   1.26709879077223,
			-2.73321840682484,   -0.49786814031973,    -0.630300333928146,  0.159031420139595,   -0.156572362635853,
			-0.811207700485657,  -2.30495862889104,    1.46555486156289,    0.653253338211898,   3.17261167036215,
			-3.57170414510909,   -5.61933809486353,    -0.346761768801802,  1.89421460825645,    5.2722180813707,
			-0.524490471100338,  -1.20994643092181,    0.610726353489055,   -0.434097631644252,  -0.253633400239102,
			0.130207267454943,   -2.78912397984011,    0.741001157195439,   -0.816245160451562,  1.91980367760914,
			-1.38816750592136,   0.152536452171517,    1.12477244653513,    -0.272110803023928,  0.491913783861713};
}

class TTwoMultivariateNormalsMixedModelInferredTest : public Test {
public:
	using TypeBern = TBernoulliTest<false, 1>;
	using TypeMM   = TTwoMulativariateNormalMixedModelsTest<TypeBern::BoxOnZ>;

	std::unique_ptr<TypeBern> bernoulli;
	std::unique_ptr<TypeMM> test;
	std::unique_ptr<TypeMM::SpecObs> obs;

	void SetUp() override { instances::dagBuilder().clear(); }

	void initialize(size_t NumDim, size_t Size, const std::vector<double> &ValuesObs,
					const std::string &ValuesZ = values_z(), bool SetInitialValues = true) {
		bernoulli = std::make_unique<TypeBern>("out");
		test       = std::make_unique<TypeMM>("out", &bernoulli->boxOnZ);

		// set initial values
		if (SetInitialValues) {
			test->m.getDefinition().setInitVal(values_m());
			if (NumDim == 1) {
				test->mu.getDefinition().setInitVal(values_mu_1D());
				test->Mrr.getDefinition().setInitVal(values_mrr_1D());
				test->rho.getDefinition().setInitVal(values_rho_1D());
			} else {
				test->mu.getDefinition().setInitVal(values_mu_5D());
				test->Mrr.getDefinition().setInitVal(values_mrr_5D());
				test->Mrs.getDefinition().setInitVal(values_mrs_5D());
				test->rho.getDefinition().setInitVal(values_rho_5D());
			}
		}
		if (!ValuesZ.empty()) { test->z.getDefinition().setInitVal(ValuesZ); }

		obs = createObservation<TypeMM::Type, TypeMM::NumDimObs>("obs", &test->boxOnObs, {Size, NumDim}, {"out"},
																 ValuesObs);
		bernoulli->boxOnZ.initialize();
		bernoulli->pi.set(0.3);
		bernoulli->pi.updateTempVals(bernoulli->pi.getFull(), true);
	};
};

TEST_F(TTwoMultivariateNormalsMixedModelInferredTest, initParameter_wrongDimensions) {
	EXPECT_ANY_THROW(initialize(9, 10, values_obs_5D())); // this should throw, wrong number of columns
	EXPECT_ANY_THROW(initialize(10, 5, values_obs_5D())); // this should also throw, rows - cols switched
}

TEST_F(TTwoMultivariateNormalsMixedModelInferredTest, getLogDensity_5D) {
	initialize(5, 10, values_obs_5D());

	// we want prior density of first element of first row -> z = 1
	size_t linearIndex = obs->getIndex({0, 0});
	EXPECT_FLOAT_EQ(test->boxOnObs.getLogDensity(obs->storage(), linearIndex), -229.614677302288);
	EXPECT_FLOAT_EQ(test->boxOnObs.getDensity(obs->storage(), linearIndex), exp(-229.614677302288));

	// we want prior density of third element of 6th row -> z = 0
	linearIndex = obs->getIndex({5, 2});
	EXPECT_FLOAT_EQ(test->boxOnObs.getLogDensity(obs->storage(), linearIndex), -504.058645509965);
	EXPECT_NEAR(test->boxOnObs.getDensity(obs->storage(), linearIndex), exp(-504.058645509965), 0.0001);

	// we want prior density of last element of last row -> z = 1
	linearIndex = obs->getIndex({9, 4});
	EXPECT_FLOAT_EQ(test->boxOnObs.getLogDensity(obs->storage(), linearIndex), -470.294479533199);
	EXPECT_FLOAT_EQ(test->boxOnObs.getDensity(obs->storage(), linearIndex), exp(-470.294479533199));
}

TEST_F(TTwoMultivariateNormalsMixedModelInferredTest, getLogDensity_1D) {
	initialize(1, 10, values_obs_1D());

	// we want prior density of first element of first row
	size_t linearIndex = obs->getIndex({0, 0});
	EXPECT_FLOAT_EQ(test->boxOnObs.getLogDensity(obs->storage(), linearIndex), 0.0648083011800458);
	EXPECT_FLOAT_EQ(test->boxOnObs.getDensity(obs->storage(), linearIndex), exp(0.0648083011800458));

	// we want prior density of third element of 6th row
	linearIndex = obs->getIndex({2, 0});
	EXPECT_FLOAT_EQ(test->boxOnObs.getLogDensity(obs->storage(), linearIndex), -3.33010373611332);
	EXPECT_FLOAT_EQ(test->boxOnObs.getDensity(obs->storage(), linearIndex), exp(-3.33010373611332));

	// we want prior density of last element of last row
	linearIndex = obs->getIndex({9, 0});
	EXPECT_FLOAT_EQ(test->boxOnObs.getLogDensity(obs->storage(), linearIndex), 0.32042493516794);
	EXPECT_FLOAT_EQ(test->boxOnObs.getDensity(obs->storage(), linearIndex), exp(0.32042493516794));
}

TEST_F(TTwoMultivariateNormalsMixedModelInferredTest, getLogRatio_5D) {
	initialize(5, 10, values_obs_5D());
	auto storage = createUpdatedStorage(obs->storage());

	// we want prior ratio of first element of first row
	size_t linearIndex   = obs->getIndex({0, 0});
	storage[linearIndex] = 0.8;
	EXPECT_FLOAT_EQ(test->boxOnObs.getLogDensityRatio(storage, linearIndex), -40.097848569596);
	storage[linearIndex].reset();

	// we want prior ratio of third element of 6th row
	linearIndex          = obs->getIndex({5, 2});
	storage[linearIndex] = 0.5;
	EXPECT_FLOAT_EQ(test->boxOnObs.getLogDensityRatio(storage, linearIndex), 212.017684803439);
	storage[linearIndex].reset();

	// we want prior ratio of last element of last row
	linearIndex          = obs->getIndex({9, 4});
	storage[linearIndex] = -0.5274;
	EXPECT_FLOAT_EQ(test->boxOnObs.getLogDensityRatio(storage, linearIndex), -57.6425846064016);
}

TEST_F(TTwoMultivariateNormalsMixedModelInferredTest, getLogRatio_1D) {
	initialize(1, 10, values_obs_1D());
	auto storage = createUpdatedStorage(obs->storage());

	// we want prior ratio of first element
	size_t linearIndex   = obs->getIndex({0, 0});
	storage[linearIndex] = 0.8;
	EXPECT_FLOAT_EQ(test->boxOnObs.getLogDensityRatio(storage, linearIndex), -12.457698221597);
	storage[linearIndex].reset();

	// we want prior ratio of 7th element
	linearIndex          = obs->getIndex({2, 0});
	storage[linearIndex] = 0.5;
	EXPECT_FLOAT_EQ(test->boxOnObs.getLogDensityRatio(storage, linearIndex), -11.8406242313563);
	storage[linearIndex].reset();

	// we want prior ratio of 9th element
	linearIndex          = obs->getIndex({9, 0});
	storage[linearIndex] = -0.5274;
	EXPECT_FLOAT_EQ(test->boxOnObs.getLogDensityRatio(storage, linearIndex), -0.0371224847760238);
}

TEST_F(TTwoMultivariateNormalsMixedModelInferredTest, calcLLUpdateMrr_0) {
	initialize(5, 10, values_obs_5D());

	// change r=0 of Mrr
	size_t r = 0;
	test->Mrr.set(r, 0.62);
	EXPECT_FLOAT_EQ(test->boxOnObs.calculateLLRatio(&test->Mrr, r)(&obs->storage()), 118.273990121255);
}

TEST_F(TTwoMultivariateNormalsMixedModelInferredTest, calcLLUpdateMrr_2) {
	initialize(5, 10, values_obs_5D());

	// change r=2 of Mrr
	size_t r = 2;
	test->Mrr.set(r, 0.9467);
	EXPECT_FLOAT_EQ(test->boxOnObs.calculateLLRatio(&test->Mrr, r)(&obs->storage()), -85.5747445153654);
}

TEST_F(TTwoMultivariateNormalsMixedModelInferredTest, calcLLUpdateMrr_4) {
	initialize(5, 10, values_obs_5D());

	// change r=4 of Mrr
	size_t r = 4;
	test->Mrr.set(r, 0.01);
	EXPECT_FLOAT_EQ(test->boxOnObs.calculateLLRatio(&test->Mrr, r)(&obs->storage()), -25.7886875003375);
}

TEST_F(TTwoMultivariateNormalsMixedModelInferredTest, calcLLUpdateMrs_1_0) {
	initialize(5, 10, values_obs_5D());

	// change r=1, s=0 of Mrs
	size_t r           = 1;
	size_t s           = 0;
	size_t linearIndex = stattools::prior::impl::mvn::indexMrs(r, s);
	test->Mrs.set(linearIndex, 0.62);
	EXPECT_FLOAT_EQ(test->boxOnObs.calculateLLRatio(&test->Mrs, linearIndex)(&obs->storage()), -94.5057244464356);
}

TEST_F(TTwoMultivariateNormalsMixedModelInferredTest, calcLLUpdateMrs_2_1) {
	initialize(5, 10, values_obs_5D());

	// change r=2, s=1 of Mrs
	size_t r           = 2;
	size_t s           = 1;
	size_t linearIndex = stattools::prior::impl::mvn::indexMrs(r, s);
	test->Mrs.set(linearIndex, 0.9467);
	EXPECT_FLOAT_EQ(test->boxOnObs.calculateLLRatio(&test->Mrs, linearIndex)(&obs->storage()), 789.576061482575);
}

TEST_F(TTwoMultivariateNormalsMixedModelInferredTest, calcLLUpdateMr_4_3) {
	initialize(5, 10, values_obs_5D());

	// change r=4, s=3 of Mrs
	size_t r           = 4;
	size_t s           = 3;
	size_t linearIndex = stattools::prior::impl::mvn::indexMrs(r, s);
	test->Mrs.set(linearIndex, 0.01);
	EXPECT_FLOAT_EQ(test->boxOnObs.calculateLLRatio(&test->Mrs, linearIndex)(&obs->storage()), 105.390970158065);
}

TEST_F(TTwoMultivariateNormalsMixedModelInferredTest, calcLLUpdateMu_0) {
	initialize(5, 10, values_obs_5D());

	// change r=0 of mu
	size_t r = 0;
	test->mu.set(r, -0.62);
	EXPECT_FLOAT_EQ(test->boxOnObs.calculateLLRatio(&test->mu, r)(&obs->storage()), 4.88791233064387);
	test->mu.reset(r);
}

TEST_F(TTwoMultivariateNormalsMixedModelInferredTest, calcLLUpdateMu_2) {
	initialize(5, 10, values_obs_5D());

	// change r=2 of mu
	size_t r = 2;
	test->mu.set(r, 0.9467);
	EXPECT_FLOAT_EQ(test->boxOnObs.calculateLLRatio(&test->mu, r)(&obs->storage()), 1139.0713108375);
}

TEST_F(TTwoMultivariateNormalsMixedModelInferredTest, calcLLUpdateMu_4) {
	initialize(5, 10, values_obs_5D());

	// change r=4 of mu
	size_t r = 4;
	test->mu.set(r, 0.1);
	EXPECT_FLOAT_EQ(test->boxOnObs.calculateLLRatio(&test->mu, r)(&obs->storage()), 146.872678877209);
}

TEST_F(TTwoMultivariateNormalsMixedModelInferredTest, calcLLUpdateMu_1D) {
	initialize(1, 10, values_obs_1D());

	// change r=0 of mu
	size_t r = 0;
	test->mu.set(r, -0.62);

	// calculate LL ratio and logH
	EXPECT_FLOAT_EQ(test->boxOnObs.calculateLLRatio(&test->mu, r)(&obs->storage()), 1.07190993828984);
}

TEST_F(TTwoMultivariateNormalsMixedModelInferredTest, calcLLUpdateM) {
	initialize(5, 10, values_obs_5D());

	// change m
	test->m.set(0.62);

	// calculate LL ratio
	EXPECT_FLOAT_EQ(test->boxOnObs.calculateLLRatio(&test->m, 0)(&obs->storage()), 3797.76514550288);
}

TEST_F(TTwoMultivariateNormalsMixedModelInferredTest, calcLLUpdateM_1D) {
	initialize(1, 10, values_obs_1D());

	// change m
	test->m.set(0.62);

	// calculate LL ratio
	EXPECT_FLOAT_EQ(test->boxOnObs.calculateLLRatio(&test->m, 0)(&obs->storage()), 77.6210003363649);
}

TEST_F(TTwoMultivariateNormalsMixedModelInferredTest, calcLLUpdateRho_0) {
	initialize(5, 10, values_obs_5D());

	// change r=0 of rho
	size_t r = 0;
	test->rho.set(r, 0.62);
	EXPECT_FLOAT_EQ(test->boxOnObs.calculateLLRatio(&test->rho, r)(&obs->storage()), -160.86301719483);
}

TEST_F(TTwoMultivariateNormalsMixedModelInferredTest, calcLLUpdateRho_2) {
	initialize(5, 10, values_obs_5D());

	// change r=2 of rho
	size_t r = 2;
	test->rho.set(r, 0.9467);
	EXPECT_FLOAT_EQ(test->boxOnObs.calculateLLRatio(&test->rho, r)(&obs->storage()), 61.2890047941106);
}

TEST_F(TTwoMultivariateNormalsMixedModelInferredTest, calcLLUpdateRho_4) {
	initialize(5, 10, values_obs_5D());

	// change r=4 of rho
	size_t r = 4;
	test->rho.set(r, 0.1);
	EXPECT_FLOAT_EQ(test->boxOnObs.calculateLLRatio(&test->rho, r)(&obs->storage()), 1.06426909825359);
}

TEST_F(TTwoMultivariateNormalsMixedModelInferredTest, calcLLUpdateRho_1D) {
	initialize(1, 10, values_obs_1D());

	// change r=0 of rho
	size_t r = 0;
	test->rho.set(r, 0.62);

	// calculate LL ratio and logH
	EXPECT_FLOAT_EQ(test->boxOnObs.calculateLLRatio(&test->rho, r)(&obs->storage()), -2.30728689597631);
}

TEST_F(TTwoMultivariateNormalsMixedModelInferredTest, calcLLUpdateZ) {
	initialize(5, 10, values_obs_5D());

	// n=0 of z: calculate likelihood 1 -> 0
	size_t n = 0;
	test->z.set(n, !test->z.value(n));
	EXPECT_FLOAT_EQ(test->boxOnObs.calculateLLRatio(&test->z, n)(&obs->storage()), -355.4603 - -225.01999);

	// n=3 of z: calculate likelihood 0 -> 1
	n = 3;
	test->z.set(n, !test->z.value(n));
	EXPECT_FLOAT_EQ(test->boxOnObs.calculateLLRatio(&test->z, n)(&obs->storage()), -206.36618 - -398.56232);

	// n=9 of z: calculate likelihood 1 -> 0
	n = 9;
	test->z.set(n, !test->z.value(n));
	EXPECT_FLOAT_EQ(test->boxOnObs.calculateLLRatio(&test->z, n)(&obs->storage()), -933.026 - -465.6998);
}

TEST_F(TTwoMultivariateNormalsMixedModelInferredTest, calcLikelihoodSampleZ) {
	initialize(5, 10, values_obs_5D());

	// n=0 of z: calculate likelihood
	size_t n     = 0;
	auto logDens = test->boxOnObs.calcBothLikelihoods_sampleZ(n);
	EXPECT_FLOAT_EQ(logDens[0], -355.4603);
	EXPECT_FLOAT_EQ(logDens[1], -225.01999);

	// n=3 of z: calculate likelihood
	n       = 3;
	logDens = test->boxOnObs.calcBothLikelihoods_sampleZ(n);
	EXPECT_FLOAT_EQ(logDens[0], -398.56232);
	EXPECT_FLOAT_EQ(logDens[1], -206.36618);

	// n=9 of z: calculate likelihood
	n       = 9;
	logDens = test->boxOnObs.calcBothLikelihoods_sampleZ(n);
	EXPECT_FLOAT_EQ(logDens[0], -933.026);
	EXPECT_FLOAT_EQ(logDens[1], -465.6998);
}

TEST_F(TTwoMultivariateNormalsMixedModelInferredTest, calcLLUpdateZ_1D) {
	initialize(1, 10, values_obs_1D());

	// 1,0,0,0,0,0,0,1,0,1

	// n=0 of z: calculate likelihood 1 -> 0
	size_t n = 0;
	test->z.set(n, !test->z.value(n));
	EXPECT_FLOAT_EQ(test->boxOnObs.calculateLLRatio(&test->z, n)(&obs->storage()),
					log(2.85908100371145) - log(2.67445824441536));

	// n=3 of z: calculate likelihood 0 -> 1
	n = 3;
	test->z.set(n, !test->z.value(n));
	EXPECT_FLOAT_EQ(test->boxOnObs.calculateLLRatio(&test->z, n)(&obs->storage()),
					log(2.7872172409493e-08) - log(3.29257454589664e-16));

	// n=9 of z: calculate likelihood 1 -> 0
	n = 9;
	test->z.set(n, !test->z.value(n));
	EXPECT_FLOAT_EQ(test->boxOnObs.calculateLLRatio(&test->z, n)(&obs->storage()),
					log(4.76319591356041) - log(3.45341455741161));
}

TEST_F(TTwoMultivariateNormalsMixedModelInferredTest, _calcLikelihoodSampleZ_1D) {
	initialize(1, 10, values_obs_1D());

	// n=0 of z: calculate likelihood
	size_t n     = 0;
	auto logDens = test->boxOnObs.calcBothLikelihoods_sampleZ(n);
	EXPECT_FLOAT_EQ(logDens[0], log(2.85908100371145));
	EXPECT_FLOAT_EQ(logDens[1], log(2.67445824441536));

	// n=3 of z: calculate likelihood
	n       = 3;
	logDens = test->boxOnObs.calcBothLikelihoods_sampleZ(n);
	EXPECT_FLOAT_EQ(logDens[0], log(3.29257454589664e-16));
	EXPECT_FLOAT_EQ(logDens[1], log(2.7872172409493e-08));

	// n=9 of z: calculate likelihood
	n       = 9;
	logDens = test->boxOnObs.calcBothLikelihoods_sampleZ(n);
	EXPECT_FLOAT_EQ(logDens[0], log(4.76319591356041));
	EXPECT_FLOAT_EQ(logDens[1], log(3.45341455741161));
}

TEST_F(TTwoMultivariateNormalsMixedModelInferredTest, _sampleZ) {
	initialize(5, 10, values_obs_5D());

	// n=0 of z: check if sampling from posterior results approximately in posterior probabilities
	size_t n      = 0;
	size_t numRep = 10000;
	size_t numZ0  = 0;
	size_t numZ1  = 0;
	for (size_t i = 0; i < numRep; i++) {
		test->boxOnObs.doGibbs(&test->z, n);
		if (test->z.value(n) == 0) {
			numZ0++;
		} else {
			numZ1++;
		}
	}

	EXPECT_NEAR((double)numZ0 / (double)numRep, 0.0, 0.02);
	EXPECT_NEAR((double)numZ1 / (double)numRep, 1.0, 0.02);
}

TEST_F(TTwoMultivariateNormalsMixedModelInferredTest, _sampleZ_1D) {
	initialize(1, 10, values_obs_1D());

	// n=0 of z: check if sampling from posterior results approximately in posterior probabilities
	size_t n      = 0;
	size_t numRep = 10000;
	size_t numZ0  = 0;
	size_t numZ1  = 0;
	for (size_t i = 0; i < numRep; i++) {
		test->boxOnObs.doGibbs(&test->z, n);
		if (test->z.value(n) == 0) {
			numZ0++;
		} else {
			numZ1++;
		}
	}

	EXPECT_NEAR((double)numZ0 / (double)numRep, 0.713828462385496, 0.02);
	EXPECT_NEAR((double)numZ1 / (double)numRep, 0.286171537614504, 0.02);
}

TEST_F(TTwoMultivariateNormalsMixedModelInferredTest, estimateInitialEMParameters) {
	// simulations and true values based on R script 'simulateHMM.R'
	initialize(5, 100, values_obs_5D_initialization_2(), "", false);

	test->boxOnObs.initializeEMParameters();

	std::vector<size_t> expectedZ = {1, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
									 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
									 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0,
									 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0};
	for (size_t i = 0; i < test->z.size(); i++) { EXPECT_EQ(test->z.value(i), expectedZ[i]); }

	// define expectations
	std::vector<double> mus = {-0.906038381437916, -0.377143277693602, -0.133143115400586, 0.253685694198566,
							   1.32243317540003};
	std::vector<std::vector<double>> M0 = {
		{0.965802522632954, 0, 0, 0, 0},
		{0.180166027458101, 1.03702524582431, 0, 0, 0},
		{-0.158163183928821, 0.155344922297222, 1.23593883937097, 0, 0},
		{0.126158793282841, 0.00047403522, 0.0998037300808922, 1.08524008140326, 0},
		{0.027006035849962, -0.114923013965249, -0.169824609130338, 0.049755568574062, 1.03801048996879}};
	std::vector<std::vector<double>> Sigma0 = {
		{1.14681861840939, -0.195004178062831, 0.133469539722875, -0.123319901496212, -0.014477337648415},
		{-0.195004178062831, 0.952141227668795, -0.0871152402464683, 0.00606573420950888, 0.0832537980406304},
		{0.133469539722875, -0.0871152402464683, 0.678661741520376, -0.074568620708783, 0.130962488601776},
		{-0.123319901496212, 0.00606573420950888, -0.074568620708783, 0.851030357829063, -0.0425512559451544},
		{-0.014477337648415, 0.0832537980406304, 0.130962488601776, -0.0425512559451544, 0.928103723646395}};
	std::vector<std::vector<double>> M1 = {
		{0.520860997818386, 0, 0, 0, 0},
		{0.0723031778613533, 0.283128703661081, 0, 0, 0},
		{-0.289230792791949, -0.0274446742287846, 0.457571308810008, 0, 0},
		{0.547973328600461, -0.0126362748572851, -0.275314686642822, 0.323492763847089, 0},
		{-0.0169582702999429, -0.0391761044101289, 0.0843593173120065, -0.0317872854725262, 0.2128865152413}};
	std::vector<std::vector<double>> Sigma1 = {
		{11.0064297400177, -2.65799256693268, -1.18416162390536, -7.34076444801027, -3.49720097035805},
		{-2.65799256693268, 12.9973564738482, 0.693985411313888, 1.26701451606762, 2.88199954591638},
		{-1.18416162390536, 0.693985411313888, 8.5817784634488, 5.47810988453714, -2.76341479576466},
		{-7.34076444801027, 1.26701451606762, 5.47810988453714, 9.76893372882951, 2.16816617941564},
		{-3.49720097035805, 2.88199954591638, -2.76341479576466, 2.16816617941564, 22.0649879167933}};

	// m's
	EXPECT_FLOAT_EQ(test->m.value(), 1.);

	for (size_t d = 0; d < 5; d++) {
		for (size_t e = 0; e < 5; e++) {
			if (d == e) {
				EXPECT_FLOAT_EQ(test->Mrr.value(d), M0[d][d]);
				EXPECT_FLOAT_EQ(test->mu.value(d), mus[d]);
			} else if (d > e) {
				EXPECT_FLOAT_EQ(test->Mrs.value(d * (d - 1) / 2 + e), M0[d][e]);
			}
			EXPECT_FLOAT_EQ(test->boxOnObs.getM1()(d, e), M1[d][e]);
			EXPECT_FLOAT_EQ(test->boxOnObs.getSigma1()(d, e), Sigma1[d][e]);
		}
	}
}

TEST_F(TTwoMultivariateNormalsMixedModelInferredTest, _setMToMLE) {
	// simulations and true values based on R script 'simulateHMM.R'
	initialize(5, 50, values_obs_5D_initialization(), values_z_initialization(), false);

	// calculate mean and variance based on z and param
	test->boxOnObs.setMeanAndM();
	std::vector<double> expectedMu   = {-0.9099094, 1.3820622, -1.0033525, -0.8155223, 0.4952181};
	std::vector<double> expectedMrr0 = {1.25845933372183, 0.870161135987401, 1.03309426781994, 0.724801552486324,
										0.831860778845982};
	std::vector<double> expectedMrs0 = {0.4760688,  0.1710598, 0.6714555, 0.2538392,  0.3350229,
										-0.4597982, 0.2920597, 0.2051382, -0.4922499, -0.4149332};

	// cannot check rho yet, because EM ignores rho
	// instead: check M1 and Sigma1
	std::vector<std::vector<double>> M1     = {{0.084553685, 0.00000000, 0.000000000, 0.000000000, 0.0000000},
											   {-0.003680969, 0.11107604, 0.000000000, 0.000000000, 0.0000000},
											   {-0.015334993, -0.01372092, 0.107748514, 0.000000000, 0.0000000},
											   {-0.033495583, 0.03435236, 0.004264703, 0.130916020, 0.0000000},
											   {-0.015572832, 0.01948826, -0.011338385, 0.004454872, 0.10359625}};
	std::vector<std::vector<double>> Sigma1 = {{154.383097, -3.957343, 17.039686, 21.315315, 17.090567},
											   {-3.957343, 90.270003, 9.858398, -17.848821, -14.140706},
											   {17.039686, 9.858398, 87.284334, -2.647284, 9.930593},
											   {21.315315, -17.848821, -2.647284, 58.454341, -3.170694},
											   {17.090567, -14.140706, 9.930593, -3.170694, 93.177696}};

	// m's
	EXPECT_FLOAT_EQ(test->m.value(), 1.);

	for (size_t d = 0; d < 5; d++) {
		// mean
		EXPECT_FLOAT_EQ(test->mu.value(d), expectedMu[d]);
		// Mrr0
		EXPECT_FLOAT_EQ(test->Mrr.value(d), expectedMrr0[d]);

		// M1 and Sigma1
		for (size_t e = 0; e < 5; e++) {
			EXPECT_FLOAT_EQ(test->boxOnObs.getM1()(d, e), M1[d][e]);
			EXPECT_FLOAT_EQ(test->boxOnObs.getSigma1()(d, e), Sigma1[d][e]);
		}
	}

	for (size_t d = 0; d < 10; d++) {
		// Mrs0
		EXPECT_FLOAT_EQ(test->Mrs.value(d), expectedMrs0[d]);
	}
}
