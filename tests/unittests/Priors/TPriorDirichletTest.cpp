#include "gtest/gtest.h"

#include "stattools/ParametersObservations/TParameter.h"
#include "stattools/Priors/TPriorDirichlet.h"
#include "stattools/commonTestCases/TDirichletTest.h"
#include "stattools/commonTestCases/commonTestingFunctions.h"

using namespace testing;
using namespace stattools;
using namespace stattools::prior;

class TDirichletFixedTest : public Test {
public:
	using TypeObs                     = coretools::ZeroOpenOneClosed;
	static constexpr size_t NumDimObs = 1;
	using BoxType                     = TDirichletFixed<TObservationBase, TypeObs, NumDimObs>;

	std::shared_ptr<BoxType> boxOnObs;
	std::unique_ptr<TObservation<TypeObs, NumDimObs, BoxType>> obs;

	void SetUp() override {
		instances::dagBuilder().clear();

		boxOnObs = std::make_shared<BoxType>();
		obs      = createObservation<TypeObs, NumDimObs, BoxType>(
			"obs", boxOnObs.get(), {10},
			{"out", "0.1,0.188888888888889,0.277777777777778,0.366666666666667,0.455555555555556,0."
							  "544444444444445,0.633333333333333,0.722222222222222,0.811111111111111,0.9"},
			{0.181818181818182, 0.163636363636364, 0.145454545454545, 0.127272727272727, 0.109090909090909,
				  0.0909090909090909, 0.0727272727272727, 0.0545454545454545, 0.0363636363636364, 0.0181818181818182});
	}
};

TEST_F(TDirichletFixedTest, initInvalidAlpha) {
	EXPECT_ANY_THROW(boxOnObs->setFixedPriorParameters("0.1, 0.2, 0."));
	EXPECT_ANY_THROW(boxOnObs->setFixedPriorParameters("0.1, 0.2, -0.3"));
}

TEST_F(TDirichletFixedTest, getLogDensity) {
	double sum = 0.0;
	for (size_t i = 0; i < 10; ++i) { sum += boxOnObs->getLogDensity(obs->storage(), i); }
	EXPECT_FLOAT_EQ(sum, 6.126878);

	double prod = 1.0;
	for (size_t i = 0; i < 10; ++i) { prod *= boxOnObs->getDensity(obs->storage(), i); }
	EXPECT_FLOAT_EQ(prod, 458.00382);
}

TEST_F(TDirichletFixedTest, hastings) {
	auto storage                = createUpdatedStorage<TDirichletTest::Type, TDirichletTest::NumDimObs>(obs->storage());
	std::vector<double> newVals = {0.139495918701045, 0.143278688150917,  0.0490180575439956, 0.0479300393857425,
								   0.127627647333716, 0.0912743516047731, 0.0711770008483544, 0.01044347693951,
								   0.169024252259974, 0.150730567231972};
	for (size_t k = 0; k < 10; k++) { storage[k] = newVals[k]; }

	double sum = 0.0;
	for (size_t i = 0; i < 10; ++i) { sum += boxOnObs->getLogDensityRatio(storage, i); }
	EXPECT_FLOAT_EQ(sum, 1.628368);
}

//----------------------------------------
// TDirichletVarInferred
//----------------------------------------

class TDirichletVarInferredTest : public Test {
public:
	std::unique_ptr<TDirichletTest> test;

	std::unique_ptr<TDirichletTest::SpecObs> obs;
	std::unique_ptr<TDirichletTest::SpecObs> obs2;
	std::unique_ptr<TDirichletTest::SpecObs> obs3;

	std::string alphas = "0.181818181818182,0.163636363636364,0.145454545454545,0.127272727272727,0.109090909090909,0."
						 "0909090909090909,0.0727272727272727,0.0545454545454545,0.0363636363636364,0.0181818181818182";

	void SetUp() override {
		instances::dagBuilder().clear();

		test = std::make_unique<TDirichletTest>("out");
		obs  = createObservation<TDirichletTest::Type, TDirichletTest::NumDimObs>(
			"obs", &test->boxOnObs, {10}, {"out", alphas},
			{0.158785790997813, 0.0923425684806225, 0.172584154076399, 0.0119603605861731, 0.0801798700055873,
			  0.171656150141062, 0.0676608727208207, 0.0497567640985439, 0.061046558724832, 0.134026910168146});

		// set initial values
		test->sigma.set(0.1);
	}

	void initializeExtraObs() {
		obs2 = createObservation<TDirichletTest::Type, TDirichletTest::NumDimObs>(
			"obs2", &test->boxOnObs, {10}, {"out", alphas},
			{0.0165622986043381, 0.0883544454712401, 0.185872625038421, 0.250257779986225, 0.0330534767273163,
			 0.131372603440967, 0.128465422965582, 0.0437874992550244, 0.0521919431715142, 0.0700819053393713});
		obs3 = createObservation<TDirichletTest::Type, TDirichletTest::NumDimObs>(
			"obs3", &test->boxOnObs, {10}, {"out", alphas},
			{0.0347941678907147, 0.0421810533056632, 0.0913860777218677, 0.189788744956827, 0.052017469690202,
			 0.101510273023848, 0.109190387839116, 0.180587992291653, 0.132863806948002, 0.0656800263321068});
	}
};

TEST_F(TDirichletVarInferredTest, constructor) {
	EXPECT_EQ(test->boxOnObs.sigma(), 0.1);
	EXPECT_TRUE(test->sigma.isPartOfBox());
}

TEST_F(TDirichletVarInferredTest, getDensity) {
	double lpd = 0.0;
	for (size_t i = 0; i < 10; ++i) { lpd += test->boxOnObs.getLogDensity(obs->storage(), i); }
	EXPECT_FLOAT_EQ(lpd, 10.37527);

	double pd = 1.0;
	for (size_t i = 0; i < 10; ++i) { pd *= test->boxOnObs.getDensity(obs->storage(), i); }
	EXPECT_FLOAT_EQ(pd, 32056.92);

	double sum = test->boxOnObs.getSumLogPriorDensity(obs->storage());
	EXPECT_FLOAT_EQ(sum, 10.37527);
}

TEST_F(TDirichletVarInferredTest, getLogDensityRatio) {
	auto storage = createUpdatedStorage<TDirichletTest::Type, TDirichletTest::NumDimObs>(obs->storage());

	storage[5] = 0.1;
	// normalize
	double sum = 0.0;
	for (auto i : storage) { sum += value(i); }
	for (size_t i = 0; i < storage.size(); ++i) {
		if (i == 5) {
			storage[i].setVal(value(storage[i]) / sum);
		} else {
			storage[i] = value(storage[i]) / sum;
		}
	}

	sum = 0.0;
	for (size_t i = 0; i < 10; ++i) { sum += test->boxOnObs.getLogDensityRatio(storage, i); }
	EXPECT_FLOAT_EQ(sum, 0.04912029);
}

TEST_F(TDirichletVarInferredTest, _updateSigma) {
	test->sigma.set(0.86);
	auto f = test->boxOnObs.calculateLLRatio(&test->sigma, 0);
	EXPECT_NEAR(f(&obs->storage()), -11.05857, 10e-05);
}

TEST_F(TDirichletVarInferredTest, _updateSigma_3params) {
	initializeExtraObs();
	test->boxOnObs.setScaledAlphas();

	test->sigma.set(0.86);

	auto f = test->boxOnObs.calculateLLRatio(&test->sigma, 0);
	EXPECT_FLOAT_EQ(f(&obs->storage()) + f(&obs2->storage()) + f(&obs3->storage()), -31.06685);
}

//----------------------------------------
// TDirichletMeanVarInferred
//----------------------------------------

class TDirichletMeanVarInferredTest : public Test {
public:
	std::unique_ptr<TDirichletMeanVarTest> test;

	std::unique_ptr<TDirichletMeanVarTest::SpecObs> obs;

	std::string alphasOnP = "1,2,3,4";
	size_t J              = 3;
	size_t M              = 4;

	void SetUp() override {
		instances::dagBuilder().clear();

		test = std::make_unique<TDirichletMeanVarTest>("out", alphasOnP);

		obs = createObservation<TDirichletMeanVarTest::Type, TDirichletMeanVarTest::NumDimObs>(
			"obs", &test->boxOnObs, {J, M}, {"out"},
			{0.463978594863284, 0.326444482868585, 0.179138439360291, 0.0304384829078399, 0.586639736405673,
			 0.214964419422421, 0.165960087758048, 0.0324357564138586, 0.543403345362407, 0.297352225971895,
			 0.123351348300667, 0.0358930803650305});

		// set initial values
		test->sigma.set(0.01);
		std::vector<double> trueP = {0.5, 0.3, 0.15, 0.05};
		for (size_t m = 0; m < M; ++m) { test->p.set(m, trueP[m]); }

		test->boxOnObs.setTryAlpha();
		test->boxOnObs.accept();
	}
};

TEST_F(TDirichletMeanVarInferredTest, constructor) {
	EXPECT_EQ(test->boxOnObs.sigma(), 0.01);
	EXPECT_TRUE(test->sigma.isPartOfBox());
}

TEST_F(TDirichletMeanVarInferredTest, getDensity) {
	std::vector<double> expectedLogDensity = {6.80631575560247, 5.3689858357039, 7.27767850444489};
	std::vector<double> expectedDensity    = {903.53582048272, 214.645071914376, 1447.62346977483};

	size_t linear = 0;
	for (size_t j = 0; j < J; ++j) {
		double lpd = 0.0;
		double pd  = 1.0;

		for (size_t m = 0; m < M; ++m, ++linear) {
			lpd += test->boxOnObs.getLogDensity(obs->storage(), linear);
			pd *= test->boxOnObs.getDensity(obs->storage(), linear);
		}
		EXPECT_FLOAT_EQ(lpd, expectedLogDensity[j]);
		EXPECT_FLOAT_EQ(pd, expectedDensity[j]);
	}

	double sum = test->boxOnObs.getSumLogPriorDensity(obs->storage());
	EXPECT_FLOAT_EQ(sum, coretools::containerSum(expectedLogDensity));
}

TEST_F(TDirichletMeanVarInferredTest, getLogDensityRatio) {
	auto storage = createUpdatedStorage<TDirichletMeanVarTest::Type, TDirichletMeanVarTest::NumDimObs>(obs->storage());

	storage[5]      = 0.1; // 2nd row, 2nd column
	size_t changedJ = 1;
	size_t changedM = 1;
	// normalize
	double sum      = 0.0;
	for (size_t m = 0; m < M; ++m) { sum += value(storage(changedJ, m)); }
	for (size_t m = 0; m < M; ++m) {
		if (m == changedM) {
			storage(changedJ, m).setVal(value(storage(changedJ, m)) / sum);
		} else {
			storage(changedJ, m) = value(storage(changedJ, m)) / sum;
		}
	}

	sum = 0.0;
	for (size_t m = 0; m < M; ++m) { sum += test->boxOnObs.getLogDensityRatio(storage, storage.getIndex({changedJ, m})); }
	EXPECT_FLOAT_EQ(sum, -10.46953443);
}

TEST_F(TDirichletMeanVarInferredTest, _updateSigma) {
	test->sigma.set(0.86);
	auto f = test->boxOnObs.calculateLLRatio(&test->sigma, 0);
	EXPECT_FLOAT_EQ(f(&obs->storage()), -19.61742);
}

TEST_F(TDirichletMeanVarInferredTest, _updateP) {
	std::vector<double> newPs = {0.427350427350427, 0.256410256410256, 0.273504273504274,
								 0.0427350427350427}; // sum = 1
	for (size_t m = 0; m < M; ++m) { test->p.set(m, newPs[m]); }

	coretools::TRange ixP(0, M, 1);
	auto f = test->boxOnObs.calculateLLRatio(&test->p, ixP);
	EXPECT_FLOAT_EQ(f(&obs->storage()), -13.03455);
}
