#include "gtest/gtest.h"

#include "stattools/commonTestCases/TBernoulliTest.h"
#include "stattools/commonTestCases/commonTestingFunctions.h"
#include "stattools/commonTestCases/TTwoNormalMixedModelsTest.h"

using namespace testing;
using namespace stattools;
using namespace stattools::prior;

//--------------------------------------------
// TTwoNormalsMixedModelInferred
//--------------------------------------------

static std::vector<double> values_obs() {
	return {-0.631159256727352, -0.556951795740949, -0.663316950203929, -0.443968155125421, -0.579396207387438,
			-0.701443165684603, -0.893788111063342, -0.490664477996881, -0.352142622533502, -0.631877703612212};
}

static std::vector<double> values_obs_initialization() {
	return {-0.465092740724406,  0.556663198673657,   0.768532924515416,   0.387671611559369,    -0.68875569454952,
			0.290606161243692,   1.35867955152904,    -0.0593133967111857, -0.00511981073100457, 0.182078050547539,
			1.10002537198388,    0.0235795181126569,  0.259692943931546,   -0.0177494659105969,  0.132164725907544,
			-0.102787727342996,  -0.151204323747827,  -0.0965722874379397, -0.198102089083783,   0.478067181605537,
			-0.0492668650408876, -0.394289953710349,  0.355734391167665,   -0.0538050405829051,  -0.629088242604682,
			0.36458196213683,    -0.0142092547979605, -0.70749515696212,   0.123279255747161,    0.154138600341164,
			0.233478772098053,   -1.37705955682861,   -0.264248969322179,  -0.112346212150228,   0.696963375404737,
			-0.196453520943738,  -0.700349597719884,  -0.259454884197085,  0.881107726454215,    0.504472084229583,
			-0.164523596253587,  0.104199506566357,   0.19600611157646,    0.247333215100859,    -0.41499456329968,
			0.29846721639081,    0.0580731181626549,  0.187808088043061,   0.763175748457544,    -0.253361680136508};
}

class TTwoNormalsMixedModelInferredTest : public Test {
public:
	using TypeBern = TBernoulliTest<false, 1>;
	using TypeMM   = TTwoNormalMixedModelsTest<TypeBern::BoxOnZ, true, 1>;

	std::unique_ptr<TypeBern> bernoulli;
	std::unique_ptr<TypeMM> test;
	std::unique_ptr<TypeMM::SpecObs> obs;
	std::unique_ptr<TypeMM::SpecObs> obs2;

	static std::vector<TypeMM::TypeZ> values_z() { return {1, 0, 0, 0, 0, 0, 0, 0, 1, 0}; };

	void SetUp() override { instances::dagBuilder().clear(); }

	void initialize(size_t Size, const std::vector<double> &ValuesObs,
					const std::vector<TypeMM::TypeZ> &ValuesZ = values_z(), bool SetInitialValues = true) {
		bernoulli = std::make_unique<TypeBern>("out");
		test       = std::make_unique<TypeMM>("out", &bernoulli->boxOnZ),
		obs = createObservation<TypeMM::Type, TypeMM::numDimObs>("obs", &test->boxOnObs, {Size}, {"out"}, ValuesObs);

		bernoulli->boxOnZ.initialize();
		bernoulli->pi.set(0.3);
		bernoulli->pi.updateTempVals(bernoulli->pi.getFull(), true);

		// set initial values
		if (SetInitialValues) {
			test->mu.set(-0.626453810742332);
			test->var0.set(0.0145706726703793);
			test->var1.set(0.0698976309342489);
			test->mu.updateTempVals(test->mu.getFull(), true);
			test->var0.updateTempVals(test->var0.getFull(), true);
			test->var1.updateTempVals(test->var1.getFull(), true);
		}
		if (!ValuesZ.empty()) {
			test->z.set(ValuesZ);
			test->z.updateTempVals(test->z.getFull(), true);
		}
	};
};

TEST_F(TTwoNormalsMixedModelInferredTest, initParameter_wrongDimensions) {
	EXPECT_ANY_THROW(initialize(11, values_obs())); // this throws - wrong size of parameter compared to z
}

TEST_F(TTwoNormalsMixedModelInferredTest, getLogDensity) {
	initialize(10, values_obs());

	for (size_t i = 0; i < obs->size(); ++i) {
		const double v1                 = sqrt(test->var0.value() + test->var1.value());
		const std::array<double, 2> sds = {sqrt(test->var0.value()), v1};
		EXPECT_FLOAT_EQ(test->boxOnObs.getDensity(obs->storage(), i),
						coretools::probdist::TNormalDistr::density((double)obs->storage()[i], test->mu.value(),
																   sds[test->z.value(i)]));
		EXPECT_FLOAT_EQ(test->boxOnObs.getLogDensity(obs->storage(), i),
						coretools::probdist::TNormalDistr::logDensity((double)obs->storage()[i], test->mu.value(),
																	  sds[test->z.value(i)]));
	}
}

TEST_F(TTwoNormalsMixedModelInferredTest, getLogDensityRatio) {
	initialize(10, values_obs());

	auto storage = createUpdatedStorage<TypeMM::Type, TypeMM::numDimObs>(obs->storage());

	storage[0] = -1.1;
	EXPECT_FLOAT_EQ(test->boxOnObs.getLogDensityRatio(storage, 0), -1.32726622);
	storage[0].reset();

	storage[4] = 0.125;
	EXPECT_FLOAT_EQ(test->boxOnObs.getLogDensityRatio(storage, 4), -19.30138794);
	storage[4].reset();

	storage[9] = 0.48;
	EXPECT_FLOAT_EQ(test->boxOnObs.getLogDensityRatio(storage, 9), -42.0094063);
	storage[9].reset();
}

TEST_F(TTwoNormalsMixedModelInferredTest, calcLLRatioMean) {
	initialize(10, values_obs());

	// change mean
	test->mu.set(-0.8);

	// calculate LL ratio
	EXPECT_FLOAT_EQ(test->boxOnObs.calculateLLRatio(&test->mu, 0)(&obs->storage()), -9.77687898);
}

TEST_F(TTwoNormalsMixedModelInferredTest, calcLLRatioVar0) {
	initialize(10, values_obs());

	// change var0
	test->var0.set(0.5);

	// calculate LL ratio
	EXPECT_FLOAT_EQ(test->boxOnObs.calculateLLRatio(&test->var0, 0)(&obs->storage()), -11.09894);
}

TEST_F(TTwoNormalsMixedModelInferredTest, calcLLRatioVar1) {
	initialize(10, values_obs());

	// change var1
	test->var1.set(0.5);

	// calculate LL ratio
	EXPECT_FLOAT_EQ(test->boxOnObs.calculateLLRatio(&test->var1, 0)(&obs->storage()), -1.43455);
}

TEST_F(TTwoNormalsMixedModelInferredTest, calcLLRatioZ) {
	initialize(10, values_obs());

	// change z
	size_t i = 5;
	test->z.set(i, true);

	// calculate LL ratio
	EXPECT_FLOAT_EQ(test->boxOnObs.calculateLLRatio(&test->z, i)(&obs->storage()), -0.7189999);
}

TEST_F(TTwoNormalsMixedModelInferredTest, doGibbs) {
	initialize(10, values_obs());

	// n=0 of z: check if sampling from posterior results approximately in posterior probabilities
	size_t n                     = 0;
	size_t numRep                = 10000;
	std::array<size_t, 2> counts = {0, 0};
	for (size_t i = 0; i < numRep; i++) {
		test->boxOnObs.doGibbs(&test->z, n);
		counts[test->z.value(n)]++;
	}

	EXPECT_NEAR((double)counts[0] / (double)numRep, 0.84881, 0.01);
	EXPECT_NEAR((double)counts[1] / (double)numRep, 0.15118, 0.01);
}

TEST_F(TTwoNormalsMixedModelInferredTest, estimateInitialEMParameters) {
	// simulations and true values based on R script 'simulateHMM.R'
	initialize(50, values_obs_initialization(), {}, false);

	// re-set z to have more data points (clearer pattern)
	test->boxOnObs.initializeEMParameters();

	// mean
	EXPECT_FLOAT_EQ(test->mu.value(), 0.06661872);
	// variances
	EXPECT_FLOAT_EQ(test->var0.value(), 0.06572253);
	EXPECT_FLOAT_EQ(test->boxOnObs.variance1(), 0.8230378);
	// z
	std::vector<uint8_t> expectedZ = {0, 0, 1, 0, 1, 0, 1, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1,
									  0, 0, 1, 0, 0, 0, 1, 0, 0, 1, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0};
	for (size_t i = 0; i < 50; i++) { EXPECT_EQ(test->z.value(i), expectedZ[i]); }
}
